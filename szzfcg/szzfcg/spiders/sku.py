# -*- coding: utf-8 -*-
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.util.http_util import retry_request
from zc_core.util.batch_gen import time_to_batch_no
from szzfcg.rules import *
from zc_core.spiders.base import BaseSpider


class SkuSpider(BaseSpider):
    name = 'sku'
    # 常用链接
    sku_list_index_url = 'http://www.szzfcg.cn/newmall/newGoodDisplay.do?method=searchGoods'
    sku_list_url = 'http://www.szzfcg.cn/newmall/newGoodDisplay.do'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        # 首页
        yield Request(
            url=self.sku_list_index_url,
            meta={
                'reqType': 'sku',
                'batchNo': self.batch_no,
            },
            callback=self.parse_index,
            errback=self.error_back,
            priority=100,
        )

    # 处理首页
    def parse_index(self, response):
        # 处理品类列表
        cats = parse_catalog(response)
        if cats:
            self.logger.info('品类: count[%s]' % len(cats))
            yield Box('catalog', self.batch_no, cats)

        # 处理品牌列表
        brands = parse_brand(response)
        if brands:
            self.logger.info('品牌: count[%s]' % len(brands))
            yield Box('brand', self.batch_no, brands)

        # 处理供应商列表
        suppliers = parse_supplier(response)
        if suppliers:
            self.logger.info('供应商: count[%s]' % len(suppliers))
            yield Box('supplier', self.batch_no, suppliers)

        # 总页数
        yield scrapy.FormRequest(
            url=self.sku_list_url,
            formdata={
                'method': 'listRefresh',
                'brandId': '',
                'macaId': '',
                'makeSupplyId': '',
                'priceInterval': '',
                'searchText': '',
                'sqlOrderBy': ' synth DESC',
                'showFlag': '',
            },
            meta={
                'batchNo': self.batch_no,
            },
            callback=self.build_sku_request,
            errback=self.error_back,
            priority=100,
            dont_filter=True
        )

    # 处理首页
    def build_sku_request(self, response):
        # 商品列表分页
        total = parse_total_page(response)
        if total:
            self.logger.info('总页数: total=%s' % total)
            for page_no in range(1, total + 1):
                # 商品列表分页
                yield scrapy.FormRequest(
                    url=self.sku_list_url,
                    formdata={
                        'page': str(page_no),
                        'method': 'listRefresh',
                        'brandId': '',
                        'macaId': '',
                        'makeSupplyId': '',
                        'priceInterval': '',
                        'searchText': '',
                        'sqlOrderBy': ' synth DESC',
                        'showFlag': '',
                    },
                    meta={
                        'batchNo': self.batch_no,
                        'page': page_no,
                    },
                    callback=self.parse_sku,
                    errback=self.error_back,
                    priority=10,
                    dont_filter=True
                )

    def parse_sku(self, response):
        meta = response.meta
        cur_page = meta.get('page')
        # 处理商品
        sku_list = parse_sku(response)
        if sku_list:
            self.logger.info('清单: page=%s, cnt=%s' % (cur_page, len(sku_list)))
            yield Box('sku', self.batch_no, sku_list)
        else:
            self.logger.info('分页为空: page=%s' % cur_page)
