import re


# 从链接中解析品类编码
def match_cat_id(link):
    # http://www.szzfcg.cn/newmall/newGoodDisplay.do?method=searchGoods&macaId=M01
    if link:
        arr = re.findall(r'macaId=(\w+)&*', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从js中解析品牌编码
def match_brand_id(link):
    # goodBrandRefresh('4412');
    if link:
        arr = re.findall(r'goodBrandRefresh\(\'(\w+)\'\);', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从js中解析供应商编码
def match_supplier_id(link):
    # goodSupplierRefresh('5990991');
    if link:
        arr = re.findall(r'goodSupplierRefresh\(\'(\w+)\'\);', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 解析商品编码
def match_sku_id(link):
    # /newmall/newGoodDisplay.do?method=view&id=2253322
    if link:
        arr = re.findall(r'id=(\w+)&*', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析供应商编码
def match_total_page(link):
    # goodPage('2184')
    if link:
        arr = re.findall(r'goodPage\(\'(\w+)\'\)', link.strip())
        if len(arr):
            return arr[0].strip()
    return '0'


# # 测试
if __name__ == '__main__':
    # print(match_cat_id('http://www.szzfcg.cn/newmall/newGoodDisplay.do?method=searchGoods&macaId=M01'))
    # print(match_cat_id('http://www.szzfcg.cn/newmall/newGoodDisplay.do?method=searchGoods&speciesId=A030201&macaId=M0101'))
    # print(match_brand_id("goodBrandRefresh('4412');"))
    # print(match_cat_id_from_nav('/frontBrands/getBrandsAndProductInfos.action?productTypeId=8&level=1&orderBy=normal'))
    # print(match_brand_id('/channel/0_7_0_0_0_0_0_0_0_0'))
    # print(clean_text('CANON/佳能(xx)(8116)'))
    # print(clean_text('东芝/TOSHIBA(894)'))
    # print(clean_text('Lenovo/联想(4746)'))
    # print(clean_text('东芝/TOSHIBA(894)'))
    # print(match_supplier_id('/channel/0_0_0_0_0_0_0_11111_0_0'))
    # tp = match_sku_id_from_sku('/commodities/220403?p_id=6641&target=_blank')
    # print(tp[0])
    # print(tp[1])
    pass
