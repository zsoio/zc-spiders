# -*- coding: utf-8 -*-
BOT_NAME = 'cdt'

SPIDER_MODULES = ['cdt.spiders']
NEWSPIDER_MODULE = 'cdt.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 24
# DOWNLOAD_DELAY = 1
CONCURRENT_REQUESTS_PER_DOMAIN = 16
CONCURRENT_REQUESTS_PER_IP = 16

DEFAULT_REQUEST_HEADERS = {
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 UBrowser/6.2.4094.1 Safari/537.36',
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.9',
}

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'cdt.validator.BizValidator': 500
}
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 代理积分阈值
PROXY_SCORE_LIMIT = 5
# 下载超时
DOWNLOAD_TIMEOUT = 45
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 2

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'zc_core.pipelines.catalog.CatalogCompletePipeline': 410,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
    'cdt.pipelines.ConverterPoolPipeline': 510,
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'cdt_2019'

# 日志
LOG_LEVEL = 'INFO'
# 订单采集截止天数
ORDER_DEADLINE_DAYS = 60
# 询价采集截止天数
INQUIRY_DEADLINE_DAYS = 10

# 品类白名单
CATALOG_WHITE_LIST = {
    '13137A': '办公电脑',
    '13136A': '办公用品',
    '13135A': '劳保用品',
    '13505A': 'MRO工业品',
    '13488A': '防疫物资',
}
# 登录账号集合
ACCOUNTS = [
    {'bjxygly9': 'zl88446055'},
]
