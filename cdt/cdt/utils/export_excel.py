import pandas as pd
import pymongo

# 导出商品数据
from zc_core.client.mongo_client import Mongo
from zc_core.util.common import format_time

inq_map = [
    # {'col': 'projectCode', 'title': '项目编号'},
    {'col': 'projectName', 'title': '项目名称'},
    {'col': 'buyerName', 'title': '采购员名称'},
    {'col': 'buyerPhone', 'title': '联系电话'},
    {'col': 'buyerOrg', 'title': '采购组织'},
    {'col': 'buyerDept', 'title': '采购部门'},
    {'col': 'currency', 'title': '币种'},
    {'col': 'taxType', 'title': '税项'},
    {'col': 'taxRate', 'title': '税率'},
    {'col': 'publishTime', 'title': '询价发布时间'},
    {'col': 'validityDate', 'title': '报价有效期'},
    {'col': 'deadlineTime', 'title': '报价截止时间'},
    {'col': 'address', 'title': '收货地址'},
    {'col': 'carriageIn', 'title': '是否含运费'},
    {'col': 'notes', 'title': '备注'},
    {'col': 'inquiryType', 'title': '询价方式'},
    {'col': 'quoteType', 'title': '报价方式'},
    {'col': 'payType', 'title': '付款方式'},
    {'col': 'url', 'title': '链接', 'isField': False},
]

item_map = [
    {'col': 'sortNum', 'title': '序号'},
    {'col': 'itemCode', 'title': '条目编号'},
    {'col': 'projectCode', 'title': '项目编号'},
    {'col': 'itemName', 'title': '条目名称'},
    {'col': 'itemType', 'title': '条目类型'},
    {'col': 'unit', 'title': '单位'},
    {'col': 'count', 'title': '数量'},
    {'col': 'deliveryTime', 'title': '交付时间'},
    {'col': 'notes', 'title': '行项目备注'},
    {'col': 'attachments', 'title': '附件'},
]


def export_item_data(batch_no, file):
    mongo = Mongo()
    inq_columns = list()
    inq_header = list()
    for row in inq_map:
        if row.get('isField', True):
            inq_columns.append(row.get('col'))
        inq_header.append(row.get('title'))

    item_columns = list()
    item_header = list()
    for row in item_map:
        item_columns.append(row.get('col'))
        item_header.append(row.get('title'))

    inq_columns.append('projectId')
    inqs = mongo.list('inquiry_2020', fields=inq_columns, sort=[("publishTime", pymongo.DESCENDING)])
    for inq in inqs:
        inq['publishTime'] = format_time(inq.pop('publishTime'))
        inq['deadlineTime'] = format_time(inq.pop('deadlineTime'))
        inq['validityDate'] = format_time(inq.pop('validityDate'))
        inq['url'] = 'http://mall.cdtbuy.cn/ProjectInquiryController.do?method=toProjectDetailView&readOnly=yes&projectId={}&fromDiv=yes'.format(inq.get('projectId'))

    items = mongo.list('inquiry_item_202007', fields=item_columns, sort=[("projectCode", pymongo.DESCENDING), ("sortNum", pymongo.ASCENDING)])
    for item in items:
        item['deliveryTime'] = format_time(item.pop('deliveryTime'))

    write = pd.ExcelWriter(file)
    df1 = pd.DataFrame(inqs)
    df1.to_excel(write, sheet_name='询价项目', columns=inq_columns, header=inq_header, index=False)
    df2 = pd.DataFrame(items)
    df2.to_excel(write, sheet_name='询价项目明细', columns=item_columns, header=item_header, index=False)
    write.save()


if __name__ == '__main__':
    # batch_no = time_to_batch_no(datetime.now(), delta=-1)
    batch_no = '20200717'
    export_item_data(batch_no, '/work/询价_{}.xls'.format(batch_no))
