# -*- coding: utf-8 -*-
import logging
from zc_core.client.mongo_client import Mongo

logger = logging.getLogger('converter')


class ConverterPool(object):
    """
    商品编码转换、供应商过滤
    用于订单采集
    """

    def __init__(self):
        self.sku_map = dict()
        self.suppliers = list()

        bind_list = Mongo().list('converter_pool')
        for kv in bind_list:
            sku_id = kv.get('_id')
            sp_sku_id = kv.get('supplierSkuId')
            sp_id = str(kv.get('supplierId'))

            self.sku_map['{}_{}'.format(sp_id, sp_sku_id)] = sku_id
            if sp_id not in self.suppliers:
                self.suppliers.append(sp_id)

        logger.info('编号映射: %s' % len(self.sku_map))
        logger.info('供应商数量: %s' % len(self.suppliers))

    def convert_id(self, sp_sku_id, sp_id):
        key = '{}_{}'.format(sp_id, sp_sku_id)
        return self.sku_map.get(key, '')

    def contains_supplier(self, sp_id):
        return str(sp_id) in self.suppliers


# 测试
if __name__ == '__main__':
    cov = ConverterPool()
    print(cov.convert_id('2657'))
    print(cov.contains_supplier('2657'))
