# -*- coding: utf-8 -*-
import datetime
from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError
from traceback import format_exc
from zc_core.model.items import *
from zc_core.util.batch_gen import batch_to_year


class ConverterPoolPipeline(object):

    def __init__(self, mongo_uri, bot_name):
        self.mongo_uri = mongo_uri
        self.bot_name = bot_name
        self.client = None
        self.db_map = dict()

    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings
        return cls(
            mongo_uri=settings.get('MONGODB_URI'),
            bot_name=settings.get('BOT_NAME')
        )

    def open_spider(self, spider):
        _ = spider
        self.client = MongoClient(self.mongo_uri)
        # 默认初始化当前年的库
        year = str(datetime.datetime.now().year)
        self.db_map[year] = self.client['{}_{}'.format(self.bot_name, year)]

    def get_db(self, batch_no):
        year = batch_to_year(batch_no)
        db = self.db_map.get(year)
        if not db:
            db = self.client['{}_{}'.format(self.bot_name, year)]
            self.db_map[year] = db

        return db

    def close_spider(self, spider):
        _ = spider
        self.client.close()

    def process_item(self, item, spider):
        try:
            if not item:
                raise DropItem("drop empty item： [{}]".format(self))

            # skuId -> supplierSkuId  用于订单明细中编号转换
            if isinstance(item, ItemData):
                if item and item.get('supplierSkuId', None) and item.get('skuId', None):
                    batch_no = item.get('batchNo')
                    sku_id = item.get("skuId")
                    # 保存商品数据
                    self.get_db(batch_no)['converter_pool'].update({'_id': sku_id}, {'$set': {
                        '_id': sku_id,
                        'supplierSkuId': item.get("supplierSkuId"),
                        'supplierId': item.get("supplierId"),
                    }}, upsert=True)
                    return item
        except DuplicateKeyError:
            spider.logger.debug('duplicate key error collection')
        except Exception as e:
            _ = e
            spider.logger.error(format_exc())
        return item
