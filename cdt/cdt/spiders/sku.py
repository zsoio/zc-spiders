# -*- coding: utf-8 -*-
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.util.http_util import retry_request
from cdt.rules import *
from zc_core.spiders.base import BaseSpider


class SkuSpider(BaseSpider):
    name = 'sku'

    # 常用链接
    cat_list_url = 'http://tang.cdt-ec.com/ny-mall-basedoc/ware_category/channel/0'
    sku_list_url = 'http://tang.cdt-ec.com/ny-mall-basedoc/search/mallProduct/allMallProduct?searchType=2&orgId=980001780'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        if not batchNo:
            self.batch_no = time_to_batch_no(datetime.now())
        else:
            self.batch_no = int(batchNo)

    def start_requests(self):
        yield Request(
            url=self.cat_list_url,
            meta={
                'reqType': 'catalog',
                'batchNo': self.batch_no,
            },
            callback=self.parse_catalog,
            errback=self.error_back,
        )

    # 处理分类
    def parse_catalog(self, response):
        meta = response.meta

        # 处理品类列表
        cats = parse_catalog(response)
        if cats:
            self.logger.info('品类: count[%s]' % len(cats))
            yield Box('catalog', self.batch_no, cats)

            for cat in cats:
                if cat.get('level') == 3:
                    cat3_id = cat.get('catalogId')
                    yield scrapy.Request(
                        url=self.sku_list_url,
                        method='POST',
                        body='{"cateCode":"' + cat3_id + '","channelId":0,"page":0,"pageSize":20,"isComputePS":true,"sortParam":{"isSort":false,"sortField":"","isDesc":true},"filterParam":[],"aggFilterParam":[]}',
                        headers={
                            'Referer': 'http://tang.cdt-ec.com/mall-cli-portal/static/pages/index/index.html',
                            'Content-Type': 'application/json',
                            'Accept': 'application/json, text/javascript, */*; q=0.01',
                        },
                        meta={
                            'batchNo': self.batch_no,
                            'catalog3Id': cat3_id,
                        },
                        callback=self.parse_sku_pages,
                        errback=self.error_back,
                        dont_filter=True
                    )

    # 处理首页
    def parse_sku_pages(self, response):
        meta = response.meta
        cat3_id = meta.get('catalog3Id')
        # 商品列表分页
        total = parse_sku_pages(response)
        if total:
            self.logger.info('分类页数: cat=%s, count=%s' % (cat3_id, total))
            for page_no in range(0, total):
                yield scrapy.Request(
                    method='POST',
                    url=self.sku_list_url,
                    body='{"cateCode":"' + cat3_id + '","channelId":0,"page":' + str(
                        page_no) + ',"pageSize":20,"isComputePS":true,"sortParam":{"isSort":false,"sortField":"","isDesc":true},"filterParam":[],"aggFilterParam":[]}',
                    headers={
                        'Referer': 'http://tang.cdt-ec.com/mall-cli-portal/static/pages/index/index.html',
                        'Content-Type': 'application/json',
                        'Accept': 'application/json, text/javascript, */*; q=0.01',
                    },
                    meta={
                        'batchNo': self.batch_no,
                        'catalog3Id': cat3_id,
                        'page': page_no,
                    },
                    callback=self.parse_sku,
                    errback=self.error_back,
                    priority=10,
                    dont_filter=True
                )
        else:
            self.logger.info('分类无商品: cat=%s' % cat3_id)

    def parse_sku(self, response):
        meta = response.meta
        cur_page = meta.get('page')
        cat3_id = meta.get('catalog3Id')
        # sku
        sku_list = parse_sku(response)
        if sku_list:
            self.logger.info('清单: cat=%s, page=%s, cnt=%s' % (cat3_id, cur_page, len(sku_list)))
            yield Box('sku', self.batch_no, sku_list)
        else:
            self.logger.info('分页为空: cat=%s, page=%s' % (cat3_id, cur_page))
