# # -*- coding: utf-8 -*-
# import json
#
# import scrapy
# from scrapy import Request
# from scrapy.exceptions import IgnoreRequest
#
# from cdt.rules import parse_inquiry_list, parse_inquiry_item
# from cdt.utils.login import SeleniumLogin
# from zc_core.util.http_util import retry_request
#
#
# class InquirySpider(BaseSpider):
#     name = 'inquiry'
#     custom_settings = {
#         'CONCURRENT_REQUESTS': 12,
#         'DOWNLOAD_DELAY': 0.3,
#         'CONCURRENT_REQUESTS_PER_DOMAIN': 12,
#         'CONCURRENT_REQUESTS_PER_IP': 12,
#     }
#     list_url = 'http://mall.cdtbuy.cn/ProjectInquiryController.do?method=toAcceptBidProjectDateList4S&type=WSXJ'
#     item_url = 'http://mall.cdtbuy.cn/ProjectInquiryController.do?method=toProjectDetailView&readOnly=yes&projectId={}&isAJAX=true&fromDi'
#
#     def __init__(self, *args, **kwargs):
#         super(InquirySpider, self).__init__(*args, **kwargs)
#
#     def start_requests(self):
#         # cookies = SeleniumLogin().get_cookies()
#         cookies = {
#             "JSESSIONID": "806B5E63486E4854BB7F982A7C947B97",
#             "acw_tc": "2760826316224478001334093e4ebbfb46915ee5c76771869ee0d2f1908196",
#             "yht_username": "ST-691898-vptx3OnPnzC9BrzlZB6T-euc.yonyoucloud.com__eb50f35e-e9ed-4f16-bdb4-5d02909177d8",
#             "yht_usertoken": "hJQ3%2Fqw5nlEQHVENCWbGbGf%2F2piPIIrHhZjS5XedaBxU5zm0EHfEOZqMST1GCg9BhDlolOfPbOk2rmwRxgxvRQ%3D%3D",
#             "yht_tenantinfo": "nl9dpvts",
#             "_TH_": "primary",
#             "_A_P_userId": "eb50f35e-e9ed-4f16-bdb4-5d02909177d8",
#             "ucuserid": "eb50f35e-e9ed-4f16-bdb4-5d02909177d8",
#             "_A_P_userType": "0",
#             "_A_P_userLoginName": "bjxygly9",
#             "_A_P_userAvator": "\"\"",
#             "_A_P_userName": "%25E8%25B5%25B5%25E7%25A3%258A",
#             "token": "d2ViLDM2MDAsc0x5M3dwcHNCY2dqbXROKzBvbVNtRDlpc2JOQVBBelc3S1hVRGQ5NmV5UFNFeFlTcUdqVDlteGhpbDM2VEtnWitIc0RHb1hLQnRTVXlVeTEvSCtYanc9PQ",
#             "u_usercode": "eb50f35e-e9ed-4f16-bdb4-5d02909177d8",
#             "u_logints": "1622448055592",
#             "logoImgUrl": "./logo.png",
#             "sysid": "ipu_datang",
#             "tenantid": "nl9dpvts",
#             "businessid": "ipu_datang",
#             "userType": "0",
#             "u_locale": "zh_CN",
#             "userId": "eb50f35e-e9ed-4f16-bdb4-5d02909177d8",
#             "i18next": "zh_CN"
#         }
#         if not cookies:
#             self.logger.error('init cookie failed...')
#             return
#         self.logger.info('init cookie: %s', cookies)
#
#         page = 1
#         yield scrapy.FormRequest(
#             method='POST',
#             url=self.list_url,
#             formdata={
#                 'page': str(page),
#                 'rp': '10',
#                 'sortname': '',
#                 'sortorder': '',
#                 'queryColumns': 'hasBid,objId,TENDERNO,TENDERNAME,NEG_RECORD_CODE,BUYERS_NAME,BUYERS_DEPART_NAME,RULE_SUBMIT_END_DATE,PUBLISH_TIME,HAS_BID,objId,objId,objId',
#                 'isAJAX': 'true',
#                 'qtype': '',
#                 'alias': 'hasBid,objId,TENDERNO,TENDERNAME,NEG_RECORD_CODE,BUYERS_NAME,BUYERS_DEPART_NAME,RULE_SUBMIT_END_DATE,PUBLISH_TIME,HAS_BID,objId,objId,objId',
#                 'tab': 'startNegotationRecord',
#             },
#             meta={
#                 'page': page,
#                 'cookies': cookies,
#             },
#             cookies=cookies,
#             callback=self.parse_inquiry_list,
#             errback=self.error_back,
#             priority=10,
#             dont_filter=True
#         )
#
#     def parse_inquiry_list(self, response):
#         meta = response.meta
#         page = meta.get('page')
#         cookies = meta.get('cookies')
#
#         inq_list = parse_inquiry_list(response)
#         if inq_list:
#             self.logger.info('列表: page=%s, count=%s' % (page, len(inq_list)))
#             # 下一页
#             next_page = page + 1
#             yield scrapy.FormRequest(
#                 method='POST',
#                 url=self.list_url,
#                 formdata={
#                     'page': str(next_page),
#                     'rp': '10',
#                     'sortname': '',
#                     'sortorder': '',
#                     'queryColumns': 'hasBid,objId,TENDERNO,TENDERNAME,NEG_RECORD_CODE,BUYERS_NAME,BUYERS_DEPART_NAME,RULE_SUBMIT_END_DATE,PUBLISH_TIME,HAS_BID,objId,objId,objId',
#                     'isAJAX': 'true',
#                     'qtype': '',
#                     'alias': 'hasBid,objId,TENDERNO,TENDERNAME,NEG_RECORD_CODE,BUYERS_NAME,BUYERS_DEPART_NAME,RULE_SUBMIT_END_DATE,PUBLISH_TIME,HAS_BID,objId,objId,objId',
#                     'tab': 'startNegotationRecord',
#                 },
#                 meta={
#                     'page': next_page,
#                     'cookies': cookies,
#                 },
#                 cookies=cookies,
#                 callback=self.parse_inquiry_list,
#                 errback=self.error_back,
#                 priority=10,
#                 dont_filter=True
#             )
#
#             for inq in inq_list:
#                 project_id = inq.get('projectId')
#                 project_code = inq.get('projectCode')
#                 self.logger.info('询价1: id=%s' % inq.get('projectCode'))
#                 yield inq
#
#                 yield scrapy.FormRequest(
#                     method='POST',
#                     url=self.item_url.format(project_id),
#                     meta={
#                         'projectId': project_id,
#                         'projectCode': project_code,
#                     },
#                     headers={
#                         'Host': 'mall.cdtbuy.cn',
#                         'Connection': 'keep-alive',
#                         'Accept': 'text/html, */*',
#                         'Origin': 'http://mall.cdtbuy.cn',
#                         'X-Requested-With': 'XMLHttpRequest',
#                         'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3766.400 QQBrowser/10.6.4163.400',
#                         'Content-Type': 'application/x-www-form-urlencoded',
#                         'Referer': 'http://mall.cdtbuy.cn/MngIndexController.do?viewName=loginListViewCgjy',
#                         'Accept-Encoding': 'gzip, deflate',
#                         'Accept-Language': 'zh-CN,zh;q=0.9',
#                     },
#                     cookies=response.meta.get('cookies'),
#                     callback=self.parse_inquiry_item,
#                     dont_filter=True
#                 )
#         else:
#             self.logger.info('分页完成: %s' % page)
#
#     def parse_inquiry_item(self, response):
#         inq, item_list = parse_inquiry_item(response)
#         if inq:
#             self.logger.info('询价2: id=%s' % inq.get('projectCode'))
#             inq['workStatus'] = 1
#             yield inq
#
#         if item_list:
#             self.logger.info('明细: id=%s, cnt=%s' % (inq.get('projectCode'), len(item_list)))
#             for item in item_list:
#                 yield item
#
