# # -*- coding: utf-8 -*-
# import copy
# from scrapy import Request
# from guotie.rules import *
# from zc_core.spiders.base import BaseSpider
#
#
# class SupplierSpider(BaseSpider):
#     name = 'supplier'
#     # 常用链接
#     shop_list_url = "https://mall.95306.cn/proxy/elasticsearch-service/mall/search/queryShopListByKeyword?platformId=20&accountId=1&keyword=&isBuy=false&pageNum={}&pageSize=20&orderColumn=shopSaleCount&orderType={}"
#     shop_detail_url = "https://mall.95306.cn/proxy/user/mall/shop/queryShopInfoByShopInfoId?platformId=20&shopId={}&shopInfoId={}"
#
#     def __init__(self, batchNo=None, *args, **kwargs):
#         super(SupplierSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
#         # 页数限制
#         self.page_size = 20
#
#     def _build_list_request(self, callback, page, order):
#         return Request(
#             url=self.shop_list_url.format(page, order),
#             headers={
#                 'Host': 'mall.95306.cn',
#                 'Connection': 'keep-alive',
#                 'Accept': 'application/json, text/javascript, */*; q=0.01',
#                 'X-Requested-With': 'XMLHttpRequest',
#                 'Authorization': self.authorization,
#                 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3775.400 QQBrowser/10.6.4208.400',
#                 'Referer': 'https://mall.95306.cn/mall-view/',
#                 'Accept-Encoding': 'gzip, deflate, br',
#                 'Accept-Language': 'zh-CN,zh;q=0.9',
#             },
#             meta={
#                 'reqType': 'supplier',
#                 'batchNo': self.batch_no,
#                 'page': page,
#                 'order': order,
#             },
#             cookies=self.cookies,
#             callback=callback,
#             errback=self.error_back,
#             priority=100
#         )
#
#     def _build_detail_request(self, callback, sp_id, sp):
#         return Request(
#             url=self.shop_detail_url.format(sp_id, sp_id),
#             headers={
#                 'Host': 'mall.95306.cn',
#                 'Connection': 'keep-alive',
#                 'Accept': 'application/json, text/javascript, */*; q=0.01',
#                 'X-Requested-With': 'XMLHttpRequest',
#                 'Authorization': self.authorization,
#                 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3775.400 QQBrowser/10.6.4208.400',
#                 'Referer': 'https://mall.95306.cn/mall-view/',
#                 'Accept-Encoding': 'gzip, deflate, br',
#                 'Accept-Language': 'zh-CN,zh;q=0.9',
#             },
#             meta={
#                 'reqType': 'supplier',
#                 'batchNo': self.batch_no,
#                 'supplier': sp,
#             },
#             callback=callback,
#             errback=self.error_back
#         )
#
#     def start_requests(self):
#         # AlteonPmall=0a03b7f281f2d3b61f41; cookieFinger=1604398147998; st=5139b593906bc0323be1fd12bed8914b
#         self.cookies = {'st': '0e19cec81469c2f21d52d7d490ea7bba', 'AlteonPmall': '0a03b7f8ce46a8361f41'}
#         if not self.cookies or 'st' not in self.cookies:
#             self.logger.error('init cookie failed...')
#             return
#         self.authorization = self.cookies.get('st')
#         self.logger.info('init cookie: %s', self.cookies)
#
#         yield self._build_list_request(self.parse_shop_list, 1, 'desc')
#         yield self._build_list_request(self.parse_shop_list, 1, 'asc')
#
#     def parse_shop_list(self, response):
#         meta = response.meta
#         cur_page = meta.get("page")
#         order = meta.get("order")
#
#         sp_list = parse_supplier_list(response)
#         if sp_list:
#             self.logger.info('清单: page=%s, count=%s' % (cur_page, len(sp_list)))
#             yield Box('supplier', self.batch_no, sp_list)
#
#             # # 详情页
#             # for sp in sp_list:
#             #     sp_id = sp.get('id')
#             #     new_sp = copy.copy(sp)
#             #     yield self._build_detail_request(self.parse_supplier, sp_id, new_sp)
#
#             # 列表分页
#             if cur_page == 1:
#                 total_count = parse_supplier_count(response)
#                 if total_count > self.page_size:
#                     total_page = math.ceil(total_count / self.page_size)
#                     self.logger.error('页数: total=%s' % total_page)
#                     for page in range(2, total_page + 1):
#                         yield self._build_list_request(self.parse_shop_list, page, order)
#         else:
#             self.logger.error('空页: page=%s' % cur_page)
#
#     def parse_supplier(self, response):
#         supplier = parse_supplier(response)
#         if supplier:
#             self.logger.info('供应商: sp=%s' % (supplier.get('id')))
#             yield supplier
#
#
