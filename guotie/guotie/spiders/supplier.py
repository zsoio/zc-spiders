# -*- coding: utf-8 -*-
import copy
from scrapy import Request
from guotie.rules import *
from zc_core.dao.sku_pool_dao import SkuPoolDao
from zc_core.spiders.base import BaseSpider
from zc_core.util.done_filter import DoneFilter


class SupplierSpider(BaseSpider):
    name = 'supplier'
    # 常用链接
    shop_list_url = "https://mall.95306.cn/proxy/elasticsearch-service/mall/search/queryShopListByKeyword?platformId=20&accountId=1&keyword=&isBuy=false&pageNum={}&pageSize=20&orderColumn=shopSaleCount&orderType={}"
    shop_detail_url = "https://mall.95306.cn/proxy/user/mall/shop/queryShopInfoByShopInfoId?platformId=20&shopId={}&shopInfoId={}"

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SupplierSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 页数限制
        self.page_size = 20
        # 避免重复采集
        self.done_filter = DoneFilter(coll_name='supplier_pool')

    def _build_detail_request(self, callback, sp_id):
        return Request(
            url=self.shop_detail_url.format(sp_id, sp_id),
            headers={
                'Host': 'mall.95306.cn',
                'Connection': 'keep-alive',
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization': self.authorization,
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3775.400 QQBrowser/10.6.4208.400',
                'Referer': 'https://mall.95306.cn/mall-view/',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'zh-CN,zh;q=0.9',
            },
            meta={
                'reqType': 'supplier',
                'batchNo': self.batch_no,
                'supplierId': sp_id,
            },
            callback=callback,
            errback=self.error_back
        )

    def start_requests(self):
        # AlteonPmall=0a03b7f281f2d3b61f41; cookieFinger=1604398147998; st=5139b593906bc0323be1fd12bed8914b
        self.cookies = {'st': '0e19cec81469c2f21d52d7d490ea7bba', 'AlteonPmall': '0a03b7f8ce46a8361f41'}
        if not self.cookies or 'st' not in self.cookies:
            self.logger.error('init cookie failed...')
            return
        self.authorization = self.cookies.get('st')
        self.logger.info('init cookie: %s', self.cookies)

        sp_list = SkuPoolDao().get_distinct_list(field='supplierId')

        self.logger.info('全量: %s' % (len(sp_list)))
        for sp_id in sp_list:
            # 避免重复采集
            if self.done_filter.contains(sp_id):
                self.logger.info('已采: %s', sp_id)
                continue

            yield self._build_detail_request(self.parse_supplier, sp_id)

    def parse_supplier(self, response):
        supplier = parse_supplier(response)
        if supplier:
            self.logger.info('供应商: sp=%s' % (supplier.get('id')))
            yield supplier
        else:
            self.logger.error('不存在: %s', response.meta.get('supplierId'))

