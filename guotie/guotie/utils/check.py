import os

from zc_core.client.mongo_client import Mongo
from scrapy.utils.project import get_project_settings

if __name__ == '__main__':
    fields = {'_id': 1, 'spuId': 1, 'soldCount': 1}

    rows1 = Mongo().list(
        collection='data_20210424',
        fields=fields,
        query={},
    )
    rows2 = Mongo().list(
        collection='data_20210530',
        fields=fields,
        query={},
    )

    map1 = {}
    for row1 in rows1:
        map1[row1['_id']] = row1['soldCount']

    for row2 in rows2:
        sku_id = row2.get('_id')
        spu_id = row2.get('spuId')
        sold_count2 = row2.get('soldCount')
        sold_count1 = map1.get(sku_id, -1)
        if sold_count1 > sold_count2:
            print(f'{sku_id}, {spu_id}')

    print('over~')
