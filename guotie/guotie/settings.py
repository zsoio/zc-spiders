# -*- coding: utf-8 -*-
BOT_NAME = 'guotie'

SPIDER_MODULES = ['guotie.spiders']
NEWSPIDER_MODULE = 'guotie.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 8
# DOWNLOAD_DELAY = 0.1
CONCURRENT_REQUESTS_PER_DOMAIN = 8
CONCURRENT_REQUESTS_PER_IP = 8

DEFAULT_REQUEST_HEADERS = {
    'Host': 'mall.95306.cn',
    'Connection': 'keep-alive',
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'X-Requested-With': 'XMLHttpRequest',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3775.400 QQBrowser/10.6.4208.400',
    'Referer': 'https://mall.95306.cn/mall-view/',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'zh-CN,zh;q=0.9',
    # 'Authorization': '4d491f55951926030612a8ad28c3c843',
    # 'Cookie': 'st=4d491f55951926030612a8ad28c3c843; cookieFinger=_ja1.0.2936634587; AlteonPmall=0a03b7faf8c5ddf81f41',
}

DOWNLOADER_MIDDLEWARES = {
    # 'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'zc_core.middlewares.filter.SpuFilterMiddleware': 550,
    'guotie.validator.BizValidator': 500,
}
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 下载超时
DOWNLOAD_TIMEOUT = 30
# 响应重试状态码
CUSTOM_RETRY_CODES = [405]

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'zc_core.pipelines.catalog.CatalogCompletePipeline': 400,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
    'zc_core.pipelines.spu.SpuFilterPipeline': 550
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'guotie_2020'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 2
# 订单采集截止天数
ORDER_DEADLINE_DAYS = 90
# 允许响应内容为空（兼容订单）
ALLOW_EMPTY_RESPONSE = True
# 已采商品强制覆盖重采
# FORCE_RECOVER = True

MISS_SUPPLIER_MAP = {
    '202011130946': '乌鲁木齐捷昌瑞商贸有限公司',
    '202011130938': '南京酷赠网络科技有限公司',
    '202008210301': '广州市康北环保科技有限公司',
    '202011120900': '三木高科科技（北京）有限公司',
}
# 白名单
# CATALOG_WHITE_LIST = [
#     {'catalog2Name': '打印耗材'},
#     {'catalog2Name': '办公设备'},
# ]
