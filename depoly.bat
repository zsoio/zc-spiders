
set targets=(agshop,baqc,cdt,cec,cgnpc,chdtp,chinagd,chng,chongqing,cnncmall,cpeinet,crccmall,ctaxccgp,ctg,eavic,energyahead,epec,esgcc,fjzfcg,guangdong,guangxi,henan,hsysmall,huiemall,ispacechina,jesgcc,liaoning,njsc,norincogroup,obei,plap,plap_deli,sdszfcg,suzhou,szzfcg,tjgpc,yzw)

for %%x in %targets% do (
    cd %%x
    scrapyd-deploy zso-server -p %%x
    cd ..
)