import re


# 从链接中解析品类编码
def match_cat_id(link):
    # /category/products?pmbh=MDAwNDAwMDEwMDIxMDAwMTAwMDEwMDAx
    if link:
        arr = re.findall(r'pmbh=(.+)', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析品类编码
def match_total_page(link):
    # 当前 2/4页 ,共 92 条
    if link:
        arr = re.findall(r'/(\d+)页', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析型号编码
def match_spu_id(text):
    # data["xhbh"] = "01a7a34a778740069dd276ed2f8392e4";
    if text:
        arr = re.findall(r'data\["xhbh"\]\s*=\s*"(.+)";', text.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从链接中解析型号编码
def match_price_id(text):
    # var index_num = ''+1700.0;
    if text:
        arr = re.findall(r"var\s*index_num\s*=\s*''\+(.+);", text.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从链接中解析商品编码
def match_sku_id(link):
    # /product/detail/ZmE4YjRiYjUzOTA1NDRmZWEwMDE4NmE4MWE1ZmUxODUwMDM5MDAwMV8xLDAwMzkwMDAxMDAwM18xLDAwMzkwMDAyXzEsMDAzOTAwMDIwMDA2XzEsMDAzOTAwMDNfMSwwMDM5MDAwNF8xLDAwMzkwMDA0MDAwN18xLDAwMzkwMDA1XzEsMDAzOTAwMDUwMDA0MDAwMV8xLDAwMzkwMDA2XzEsMDAzOTAwMDdfMSwwMDM5MDAwNzAwMDdfMSwwMDM5MDAwOF8xLDAwMzkwMDA5XzEsMDAzOTAwMTBfMSwwMDM5MDAxMV8xLDAwMzkwMDEyXzEsMDAzOTAwMTNfMSwwMDM5MDAxMzAwMTJfMSwwMDM5MDAxNF8xLDAwMzkwMDE0MDAwOF8xLDAwMzkwMDE1XzEsMDAzOTAwMTUwMDA2XzEsMDAzOTAwMTZfMSwwMDM5MDAxNjAwMDlfMSwwMDM5MDAxN18xLDAwMzkwMDE3MDAxMF8xLDAwMzkwMDE4XzEsMDAzOTAwMTlfMQ
    if link:
        arr = re.findall(r'detail/(.+)', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从链接中解析商品编码
def match_order_id(js):
    # yzmxq('ZjEyODUyYmMzYzI2NDIzM2I0MTZjMGYzMTZjNzcyMGM')
    if js:
        arr = re.findall(r"yzmxq\('(.+)'\)", js.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从链接中解析商品编码
def match_prices(txt):
    # var index_num = ''+3300.0;
    if txt:
        arr = re.findall(r"var\s*index_num\s*=\s*''\+(.+);", txt.strip())
        if len(arr):
            return arr

    return []


# 测试
if __name__ == '__main__':
    print(match_price_id("var index_num = ''+1700.0;"))
    pass
