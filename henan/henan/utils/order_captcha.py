# encoding=utf-8
import os
import logging
import threading
import requests
from scrapy.utils.project import get_project_settings
from zc_core.plugins.verify_code import VerifyCode
from zc_core.util.encrypt_util import short_uuid

logger = logging.getLogger('captcha')


class OrderCaptchaHelper(object):
    _instance_lock = threading.Lock()
    _biz_inited = False

    def __init__(self):
        if not self._biz_inited:
            self._biz_inited = True
            settings = get_project_settings()
            self.base_path = os.path.join(settings.get('OUTPUT_ROOT'), 'verify_code\\')
            if not os.path.exists(self.base_path):
                os.makedirs(self.base_path)
            # proxy -> code
            self.captcha_pool = dict()

    def __new__(cls, *args, **kwargs):
        if not hasattr(OrderCaptchaHelper, '_instance'):
            with OrderCaptchaHelper._instance_lock:
                if not hasattr(OrderCaptchaHelper, '_instance'):
                    OrderCaptchaHelper._instance = object.__new__(cls)
        return OrderCaptchaHelper._instance

    def get_captcha_code(self, proxy):
        code = self.captcha_pool.get(proxy, None)
        if not code:
            code = self._reload(proxy)
        return code

    def reset(self, proxy):
        self._instance_lock.acquire()
        self.captcha_pool.pop(proxy, None)
        self._instance_lock.release()

    def _reload(self, proxy):
        self._instance_lock.acquire()
        code = self.captcha_pool.get(proxy)
        if code:
            self._instance_lock.release()
            logger.info('无需加载验证码: proxy=%s, code=%s' % (proxy, code))
            return
        # 加载验证码
        proxies = dict()
        if proxy and 'NO_PROXY' not in proxy:
            proxies['http'] = proxy
        r = requests.get(
            url='http://222.143.21.205:8081/captcha-image',
            proxies=proxies,
            headers={
                'Host': '222.143.21.205:8081',
                'Connection': 'keep-alive',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 zc_core/1.70.3741.400 QQBrowser/10.5.3863.400',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'zh-CN,zh;q=0.9',
            }
        )
        # 解析保存
        img = r.content
        req_id = short_uuid()
        file = os.path.join(self.base_path, '%s.png' % req_id)
        with open(file, 'wb') as f:
            f.write(img)
        # 验证码识别
        code = VerifyCode().verify(file, code_type='1004')
        if code:
            logger.info('加载识别验证码: key={}, code={}, proxy={}'.format(req_id, code, proxy))
            self.captcha_pool[proxy] = code.lower()
        else:
            logger.info('验证码识别失败: {}.png'.format(req_id))

        self._instance_lock.release()
        return code
