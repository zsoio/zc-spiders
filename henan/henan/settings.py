# -*- coding: utf-8 -*-
BOT_NAME = 'henan'

SPIDER_MODULES = ['henan.spiders']
NEWSPIDER_MODULE = 'henan.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 16
# DOWNLOAD_DELAY = 1
CONCURRENT_REQUESTS_PER_DOMAIN = 16
CONCURRENT_REQUESTS_PER_IP = 16

DEFAULT_REQUEST_HEADERS = {
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3719.400 QQBrowser/10.5.3715.400',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.9',
}

DOWNLOADER_MIDDLEWARES = {
    # 'henan.middlewares.OrderCaptchaMiddleware': 660,
    # 'henan.middlewares.SessionMiddleware': 655,
    # 'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    # 'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    # 'henan.validator.BizValidator': 500
}
# 指定代理池类
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.mogu_pool.MoguProxyPool'
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 1
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 1
# 代理积分阈值
PROXY_SCORE_LIMIT = 5
# 下载超时
# DOWNLOAD_TIMEOUT = 60

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'zc_core.pipelines.catalog.CatalogCompletePipeline': 400,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
    'henan.pipelines.HenanOrderPipeline': 520,
}

# 列表分页数阈值
SKU_PAGE_LIMIT = 150
# 特殊价格区间阶梯
SPECIAL_LADDER = {
    'MDAwNDAwMDEwMDIxMDAwMzAwMDEwMDAx': ['0-100', '100-150', '150-200', '200-250', '250-300', '300-350', '350-400', '400-450', '450-500', '500-600', '600-700', '700-1000', '1000以上'],
    'MDAwNDAwMDEwMDIxMDAwNzAwMDEwMDAx': ['0-1', '1-2', '2-3', '3-4', '4-5', '5-10', '10-15', '15-20', '20-50', '50以上'],
    'MDAwNDAwMDEwMDIxMDAwNjAwMDEwMDAz': ['0-300', '300-500', '500-600', '600-700', '700-800', '800-900', '900-1000', '1000-1100', '1100-1200', '1200-1300', '1300-1400', '1400-1500', '1500-1700', '1700-1900', '1900-2200', '2200-2500', '2500-2800', '2800-3000', '3000-3500', '3500-4000', '4000-4500', '4500-5000', '5000-5500', '5500-6000', '6000-6500', '6500-7000', '7000-7500', '7500-8000', '8000-8500', '8500-9000', '9000-9500', '9500-10000', '10000以上'],
    'MDAwNDAwMDEwMDIxMDAwNjAwMDEwMDA0': ['0-100', '100-150', '150-200', '200-250', '250-300', '300-350', '350-400', '400-450', '450-500', '500-600', '600-700', '700-800', '800-900', '900-1000', '1000-1200', '1200-1500', '1500-2000', '2000-5000', '5000以上'],
    'MDAwNDAwMDEwMDIxMDAwNjAwMDEwMDA2': ['0-700', '700-800', '800-900', '900-1000', '1000-1200', '1200-1500', '1500-2000', '2000-3000', '3000-5000', '5000-50000', '50000以上'],
}


# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'henan_2019'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 3
# [订单] 最大采集页数
MAX_ORDER_PAGE = 100

# 输出
OUTPUT_ROOT = '/work/henan/'