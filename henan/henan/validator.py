# encoding=utf-8
"""
响应对象业务校验
"""
from scrapy.exceptions import IgnoreRequest

from zc_core.middlewares.validate import BaseValidateMiddleware
from zc_core.util.http_util import retry_request


class BizValidator(BaseValidateMiddleware):

    def validate_sku(self, request, response, spider):
        if not response.text:
            spider.logger.info('[Sku]无内容：[%s]' % request.meta.get('validate_retry_times', 0))
            return retry_request(request)
        return response

    def validate_item(self, request, response, spider):
        if not response.text:
            return retry_request(request)

        meta = request.meta
        sku_id = meta.get('skuId')
        if '无法显示此页面' in response.text:
            raise IgnoreRequest('下架: [Item][%s]' % sku_id)
        return response
