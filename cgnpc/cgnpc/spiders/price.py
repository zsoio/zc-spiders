# -*- coding: utf-8 -*-
import random
import scrapy
from scrapy import Request
from zc_core.dao.batch_dao import BatchDao
from zc_core.dao.item_data_dao import ItemDataDao
from zc_core.util.batch_gen import time_to_batch_no
from cgnpc.utils.done_filter import DoneFilter
from cgnpc.rules import *
from datetime import datetime
from zc_core.spiders.base import BaseSpider

class priceSpider(BaseSpider):
    name = 'price'
    custom_settings = {
        'CONCURRENT_REQUESTS': 32,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 32,
        'CONCURRENT_REQUESTS_PER_IP': 32,
    }
    # 获取价格链接
    # price_url = 'https://mall.cgnpc.com.cn/scm-cgn-oauth-web/obs/business/product/productDst/getProductSkuPrices?productId={}'
    price_url = 'https://mall.cgnpc.com.cn/scm-cgn-oauth-web/obs/business/product/productDst/getProductSkuPrices?productId={}'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(priceSpider, self).__init__(batchNo=batchNo,*args, **kwargs)
        if not batchNo:
            self.batch_no = time_to_batch_no(datetime.now())
        else:
            self.batch_no = int(batchNo)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)
        # 避免重复采集
        self.done_filter = DoneFilter(self.batch_no)

    def start_requests(self):

        item_list = ItemDataDao().get_batch_data_list(self.batch_no,
                                                      fields={'_id': 1, 'originPrice': 1, 'salePrice': 1, 'batchNo': 1,
                                                              'offlineTime': 1, 'spuId': 1})
        self.logger.info('目标：%s' % (len(item_list)))
        random.shuffle(item_list)
        for item in item_list:
            spuId = item.get('spuId')
            skuId = item.get('_id')
            originPrice = item.get('originPrice')
            salePrice = item.get('salePrice')
            if originPrice == 0:
                # 采集销售额
                yield Request(
                    url=self.price_url.format(spuId),
                    meta={
                        'reqType': 'item',
                        'batchNo': self.batch_no,
                        'spuId': spuId,
                        'skuId': skuId,
                        'originPrice': originPrice,
                        'salePrice': salePrice
                    },
                    callback=self.parse_sales,
                    errback=self.error_back,
                    priority=25,
                )

    # 处理ItemData
    def parse_sales(self, response):
        meta = response.meta
        sku_id = meta.get('skuId')
        spu_id = meta.get('spuId')
        data = parse_prices(response)
        if data:
            self.logger.info('销售价格: sku_id=%s,spuId=%s,spri=%s spsa=%s' % (
            sku_id, spu_id, data.get('originPrice'), data.get('salePrice')))
            yield data
        else:
            self.logger.error('异常: [%s]' % sku_id)


