# -*- coding: utf-8 -*-
BOT_NAME = 'hsysmall'

SPIDER_MODULES = ['hsysmall.spiders']
NEWSPIDER_MODULE = 'hsysmall.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 12
# DOWNLOAD_DELAY = 1
CONCURRENT_REQUESTS_PER_DOMAIN = 12
CONCURRENT_REQUESTS_PER_IP = 12

DEFAULT_REQUEST_HEADERS = {
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Upgrade-Insecure-Requests': '1',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.9',
}
DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    # 'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'hsysmall.validator.BizValidator': 543,
}
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 代理积分阈值
PROXY_SCORE_LIMIT = 5
# 指定忽略的状态码
CUSTOM_IGNORE_CODES = [500]
# 下载超时
DOWNLOAD_TIMEOUT = 60

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}
ITEM_PIPELINES = {
    'zc_core.pipelines.catalog.CatalogCompletePipeline': 300,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'hsysmall_2019'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 3
# 订单采集截止天数
ORDER_DEADLINE_DAYS = 45
# 已采商品强制覆盖重采
# FORCE_RECOVER = True

SUPPLIERS = {
    'DELI': '得力',
    'JD_1': '京东自营',
    'JD_0': '京东第三方',
    'XFS': '鑫方盛',
    'ZKH': '震坤行',
    'THIRD_1632': '领先未来',
    'THIRD_1457': '海尔',
    'SN': '苏宁',
    'THIRD_1594': '陕西星宏置业有限公司',
    'THIRD_1526': '陕西万谛电线电缆有限公司',
}