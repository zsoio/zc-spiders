# -*- coding: utf-8 -*-
import json
from zc_core.model.items import Catalog, Brand, Supplier, Sku, ItemData


# 供应商分页
def parse_catalog(response):
    cats = list()
    cat_json = json.loads(response.text).get('allList', [])
    for cat1 in cat_json:
        cat1_id = str(cat1.get('catalogId'))
        cat1_name = cat1.get('catalogName')
        entity1 = _build_catalog(cat1_id, cat1_name, '', 1)
        cats.append(entity1)
        sub_cat1 = cat1.get('children')
        if sub_cat1:
            for cat2 in sub_cat1:
                cat2_id = str(cat2.get('catalogId'))
                cat2_name = cat2.get('catalogName')
                entity2 = _build_catalog(cat2_id, cat2_name, cat1_id, 2)
                cats.append(entity2)
                sub_cat2 = cat2.get('children')
                if sub_cat2:
                    for cat3 in sub_cat2:
                        cat3_id = str(cat3.get('catalogId'))
                        cat3_name = cat3.get('catalogName')
                        entity3 = _build_catalog(cat3_id, cat3_name, cat2_id, 3)
                        cats.append(entity3)
    return cats


def _build_catalog(cat_id, cat_name, parent_id, level):
    cat = Catalog()
    cat['catalogId'] = cat_id
    cat['catalogName'] = cat_name
    cat['parentId'] = parent_id
    cat['level'] = level
    if level == 3:
        cat['leafFlag'] = 1
    else:
        cat['leafFlag'] = 0
    cat['linkable'] = 0

    return cat


# 解析brand列表
def parse_supplier(response):
    suppliers = list()
    sp_json = json.loads(response.text)
    for sp in sp_json:
        supplier = Supplier()
        supplier['id'] = str(sp.get('ecPlatformCode'))
        supplier['name'] = str(sp.get('ecPlatformName'))
        suppliers.append(supplier)

    return suppliers


# 解析stock
def parse_stock(response):
    if response.text and 'true' in response.text:
        return True

    return False


# 解析ItemData
def parse_item_data(response, cat_helper):
    data_list = list()

    meta = response.meta
    rs_json = json.loads(response.text)
    rs = rs_json.get('productList', [])
    total_page = rs.get('totalPages', 0)
    rows = rs.get('content', [])
    for data in rows:
        item = ItemData()
        item['batchNo'] = meta.get('batchNo')
        item['skuId'] = data.get('productId')
        item['spuId'] = data.get('spuId')
        item['skuCode'] = data.get('skuCode', '')
        item['skuName'] = data.get('skuName')
        item['salePrice'] = data.get('agreementPrice')
        item['originPrice'] = data.get('marketPrice', 0)
        item['catalog3Name'] = data.get('categoryName', '')
        item['skuImg'] = data.get('imagePath', '')
        item['supplierId'] = data.get('sourceFromCode')
        item['supplierName'] = data.get('sourceFrom')
        item['supplierSkuId'] = str(data.get('spu'))

        cat_helper.fill(item)

        data_list.append(item)

    return data_list, total_page
