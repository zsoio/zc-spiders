# -*- coding: utf-8 -*-
import random
from scrapy import Request
from zc_core.client.mongo_client import Mongo
from zc_core.model.items import Box
from zc_core.pipelines.helper.catalog_helper import CatalogHelper
from zc_core.spiders.base import BaseSpider
from zc_core.dao.batch_dao import BatchDao
from szecp.rules import *
from zc_core.util.common import half_split_int_range


class FullSpider(BaseSpider):
    name = 'full'
    # 常用链接
    item_url = 'https://szecp.crc.com.cn/srm/api/sads/search?page={}&size=100'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(FullSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)
        self.max_page_limit = 100
        self.cat_helper = CatalogHelper()

    def _send_request(self, callback, sp_name, sp_id, page=1, min_price=0, max_price=10000000):
        query = {
            "companyId": "1021",
            "tenantId": "968",
            "keyword": "",
            "onlyInStock": False,
            "attribute": {
                "商品来源": [
                    sp_name
                ]
            },
            "address": "",
        }
        if min_price or min_price == 0:
            query['minPrice'] = min_price
        if max_price and max_price > min_price:
            query['maxPrice'] = max_price

        req = Request(
            method='POST',
            url=self.item_url.format(page),
            meta={
                'reqType': 'item',
                'batchNo': self.batch_no,
                'spId': sp_id,
                'spName': sp_name,
                'minPrice': min_price,
                'maxPrice': max_price,
                'page': page,
            },
            headers={
                'Content-Type': 'application/json'
            },
            body=json.dumps(query),
            callback=callback,
            errback=self.error_back,
        )
        return req

    def start_requests(self):
        pool_list = Mongo().list('supplier_pool', fields={'_id': 1, 'name': 1})
        self.logger.info('全量：%s' % (len(pool_list)))
        random.shuffle(pool_list)
        for sp in pool_list:
            sp_id = sp.get('_id')
            sp_name = sp.get('name')
            # 采第一页
            yield self._send_request(self.parse_item_data, sp_name, sp_id)

    # 处理ItemData
    def parse_item_data(self, response):
        meta = response.meta
        cur_page = meta.get('page')
        sp_id = meta.get('spId')
        sp_name = meta.get('spName')
        cur_min_price = meta.get('minPrice')
        cur_max_price = meta.get('maxPrice')

        data_list, total_page = parse_item_data(response, self.cat_helper)
        if data_list:
            self.logger.info('清单: sp=%s, min=%s, max=%s, page=%s, cnt=%s' % (sp_id, cur_min_price, cur_max_price, cur_page, len(data_list)))
            yield Box('item', self.batch_no, data_list)

            if total_page and cur_page == 1 and total_page >= self.max_page_limit:
                if cur_min_price >= 0 and cur_max_price:
                    half_list = half_split_int_range(cur_min_price, cur_max_price)
                    if half_list and len(half_list) == 2:
                        # 上半部分
                        min_price_0, max_price_0 = half_list[0]
                        yield self._send_request(self.parse_item_data, sp_name, sp_id, page=1, min_price=min_price_0, max_price=max_price_0)
                        # 下半部分
                        min_price_1, max_price_1 = half_list[1]
                        yield self._send_request(self.parse_item_data, sp_name, sp_id, page=1, min_price=min_price_1, max_price=max_price_1)
                    else:
                        self.logger.info('丢失: sp=%s, min=%s, max=%s' % (sp_id, cur_min_price, cur_max_price))
                        for page in range(2, total_page + 1):
                            yield self._send_request(self.parse_item_data, sp_name, sp_id, page=page, min_price=cur_min_price, max_price=cur_max_price)
            else:
                for page in range(2, total_page + 1):
                    yield self._send_request(self.parse_item_data, sp_name, sp_id, page=page, min_price=cur_min_price, max_price=cur_max_price)
        else:
            self.logger.error('空页: sp=%s, min=%s, max=%s, page=%s' % (sp_id, cur_min_price, cur_max_price, cur_page))
