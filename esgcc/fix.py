from pymongo import UpdateOne

from zc_core.client.mongo_client import Mongo
from zc_core.util.common import parse_time

mongo = Mongo()


def fix_data_spuId():
    update_bulk = list()
    src_list = mongo.list('data_20210622')
    for item in src_list:
        data = {
            "_id": item['_id'],
            "spuId": item.get('spuId')
        }
        update_bulk.append(UpdateOne({'_id': item['_id']}, {'$set': data}, upsert=True))
    print(len(update_bulk))
    #
    # rows = [20210519, 20210518, 20210516, 20210514, 20210511, 20210509, 20210505, 20210504, 20210430, 20210427,
    #         20210425, 20210422, 20210420, 20210418, 20210415, 20210413, 20210412, 20210411, 20210409, 20210408,
    #         20210407, 20210406, 20210405, 20210404, 20210402, 20210401]
    # rows = [20210519]
    batchNo = '20210623'
    # for batch_no in rows:
    mongo.bulk_write(f'data_{batchNo}', update_bulk)
    mongo.close()
    print('任务完成~')


if __name__ == '__main__':
    fix_data_spuId()
