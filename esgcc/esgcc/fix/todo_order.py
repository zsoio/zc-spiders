# -*- coding: utf-8 -*-
from zc_core.client.mongo_client import Mongo
from zc_core.util.batch_gen import batch_to_year


class TodoOrder(object):
    """
    商品池操作简易封装（Mongo）
    """

    # 查询未采集列表
    def get_todo_list(self, batch_no, query={}):
        # 处理订单列表
        year = batch_to_year(batch_no)
        mongo = Mongo(year=year)

        all_set = set()
        done_set = set()

        all = mongo.list('order_{}'.format(year), query=query, fields={'_id': 1})
        for row in all:
            all_set.add(row.get('_id'))
        print('all: %s' % len(all_set))

        done_set = done_set.union(set(mongo.get_collection('order_item_202007').find({}).distinct('orderId')))
        print('done_set7: %s' % len(done_set))
        done_set = done_set.union(set(mongo.get_collection('order_item_202006').find({}).distinct('orderId')))
        print('done_set6: %s' % len(done_set))
        done_set = done_set.union(set(mongo.get_collection('order_item_202005').find({}).distinct('orderId')))
        print('done_set5: %s' % len(done_set))
        done_set = done_set.union(set(mongo.get_collection('order_item_202004').find({}).distinct('orderId')))
        print('done_set4: %s' % len(done_set))
        done_set = done_set.union(set(mongo.get_collection('order_item_202003').find({}).distinct('orderId')))
        print('done_set3: %s' % len(done_set))
        done_set = done_set.union(set(mongo.get_collection('order_item_202002').find({}).distinct('orderId')))
        print('done_set2: %s' % len(done_set))
        done_set = done_set.union(set(mongo.get_collection('order_item_202001').find({}).distinct('orderId')))
        print('done_set1: %s' % len(done_set))

        todo_set = all_set - done_set
        print('todo_set: %s' % len(todo_set))
        orders = mongo.get_collection('order_2020').find({'_id': {'$in': list(todo_set)}})

        return list(orders)


if __name__ == '__main__':
    todo = TodoOrder().get_todo_list('20200701')
    print(todo)
