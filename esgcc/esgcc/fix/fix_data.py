from pymongo import UpdateOne

from zc_core.client.mongo_client import Mongo
from zc_core.util.common import parse_time
from zc_core.util.encrypt_util import md5
import pandas as pd

mongo = Mongo()


def fix_data_spu():
    update_bulk = list()
    src_list = mongo.list('data_20210810', query={"spuId": {"$exists": True}}, fields={'_id': 1, 'spuId': 1})
    for item in src_list:
        _id = item.get('_id')
        spu_id = item.get('spuId')
        if spu_id:
            update_bulk.append(UpdateOne({'_id': _id}, {'$set': {
                '_id': _id,
                'spuId': spu_id,
            }}, upsert=False))

    print(len(update_bulk))
    mongo.bulk_write('data_20210811', update_bulk)
    mongo.close()
    print('任务完成~')


def fix_data_brand():
    update_bulk = list()
    src_list = mongo.list('item_data_pool', fields={'_id': 1, 'brandName': 1, 'brandModel': 1, 'brandId': 1})
    for item in src_list:
        _id = item.get('_id')
        brand_name = item.get('brandName')
        brand_id = item.get('brandId')
        brand_model = item.get('brandModel')
        if brand_name or brand_model:
            update_bulk.append(UpdateOne({'_id': _id}, {'$set': {
                '_id': _id,
                'brandId': brand_id,
                'brandName': brand_name,
                'brandModel': brand_model,
            }}, upsert=False))
    print(len(update_bulk))

    rows = [20210519, 20210518, 20210516, 20210514, 20210511, 20210509, 20210505, 20210504, 20210430, 20210427,
            20210425, 20210422, 20210420, 20210418, 20210415, 20210413, 20210412, 20210411, 20210409, 20210408,
            20210407, 20210406, 20210405, 20210404, 20210402, 20210401]
    # rows = [20210519]
    for batch_no in rows:
        mongo.bulk_write(f'data_{batch_no}', update_bulk)
    print('任务完成~')


def fix_brand_model():
    # encoding:utf-8
    # update_bulk = list()
    #
    # standard_pool = pd.read_excel(r'C:\Users\Administrator\Desktop\国网标准库产品信息_20210810(1).xls', dtype=str)
    # model_pool = list(standard_pool['型号规格'])
    # goods_code = list(standard_pool['产品编码'])
    # dict1 = {}
    # for index, data in enumerate(model_pool):
    #     dict1.update({goods_code[index]: data})
    # # print(dict1)
    #
    # data_pool = pd.read_excel(r'C:\Users\Administrator\Desktop\商品信息.xlsx', dtype=str)
    # goods_code1 = list(data_pool['商品编码'])
    # product_code = list(data_pool['产品编码'])
    # dict2 = {}
    # for index, data in enumerate(product_code):
    #     dict2.update({goods_code1[index]: data})
    # # print(dict2)
    # data_pool = Mongo().list('item_data_pool', query={
    #     "$and": [{"$or": [{"brandModel": {"$exists": False}}, {"brandModel": {"$in": [None, ""]}}]},
    #              {'supplierId': '597E4D47AE98A778'}]},
    #                          fields={"_id": 1, "supplierId": 1, "spuId": 1})
    # for data in data_pool:
    #     _id = data.get('_id')
    #     supplier_id = data.get('supplierId')
    #     spu_id = data.get('spuId')
    #     spu_pool = Mongo().list('item_data_pool', query={'spuId': spu_id},
    #                             fields={"_id": 1, "supplierId": 1, "spuId": 1})
    #     result = dict2.get(_id, '')
    #     if result == '':
    #         print()
    #     else:
    #         for data1 in spu_pool:
    #             item = {
    #                 '_id': data1.get('_id'),
    #                 'brandModel': dict1.get(result)
    #             }
    #             update_bulk.append(UpdateOne({'_id': item.get('_id')}, {'$set': item}, upsert=False))
    # print(len(update_bulk))
    # mongo.bulk_write('item_data_pool', update_bulk)

    update_bulk = list()
    src_list = mongo.list('item_data_pool', fields={'_id': 1, 'brandModel': 1})
    for item in src_list:
        _id = item.get('_id')
        brand_model = item.get('brandModel')
        if brand_model != '' and brand_model != None:
            update_bulk.append(UpdateOne({'_id': _id}, {'$set': {
                '_id': _id,
                'brandModel': brand_model,
            }}, upsert=False))
    print(len(update_bulk))
    rows = [20210101, 20210103, 20210104, 20210105, 20210106, 20210107, 20210108, 20210110, 20210111, 20210112, 20210114, 20210115, 20210117, 20210118, 20210119, 20210120, 20210121, 20210122, 20210124, 20210125, 20210126, 20210128, 20210129, 20210131, 20210201, 20210202, 20210203, 20210204, 20210205, 20210208, 20210209, 20210210, 20210217, 20210218, 20210219, 20210221, 20210222, 20210223, 20210224, 20210225, 20210226, 20210228, 20210301, 20210302, 20210303, 20210304, 20210307, 20210308, 20210309, 20210310, 20210311, 20210312, 20210314, 20210315, 20210316, 20210317, 20210318, 20210319, 20210321, 20210322, 20210323, 20210324, 20210325, 20210326, 20210328, 20210329, 20210330, 20210331, 20210401, 20210402, 20210404, 20210405, 20210406, 20210407, 20210408, 20210409, 20210411, 20210412, 20210413, 20210415, 20210418, 20210420, 20210422, 20210425, 20210427, 20210430, 20210504, 20210505, 20210509, 20210511, 20210514, 20210516, 20210518, 20210519, 20210520, 20210523, 20210524, 20210525, 20210526, 20210527, 20210528, 20210530, 20210531, 20210601, 20210602, 20210606, 20210607, 20210608, 20210609, 20210610, 20210613, 20210614, 20210615, 20210616, 20210617, 20210620, 20210621, 20210622, 20210623, 20210624, 20210627, 20210628, 20210629, 20210630, 20210701, 20210704, 20210705, 20210706, 20210707, 20210708, 20210711, 20210712, 20210713, 20210714, 20210715, 20210716, 20210718, 20210719, 20210720, 20210721, 20210722, 20210725, 20210726, 20210727, 20210728, 20210729, 20210801, 20210802]

    rows=rows[::-1]
    for batch_no in rows:
        mongo.bulk_write(f'data_{batch_no}', update_bulk)
        print(batch_no)
    print('任务完成~')


if __name__ == '__main__':
    # fix_data_spu()
    fix_brand_model()
