import pymysql
from pymongo import UpdateOne

from zc_core.client.mongo_client import Mongo
from zc_core.util import file_reader
from zc_core.util.common import parse_time
from zc_core.util.encrypt_util import md5

mongo = Mongo()


def fix_data_spu():
    update_bulk = list()
    src_list = mongo.list('data_20210330', fields={'_id': 1, 'spuId': 1})
    for item in src_list:
        _id = item.get('_id')
        spu_id = item.get('spuId')
        if spu_id:
            update_bulk.append(UpdateOne({'_id': _id}, {'$set': {
                '_id': _id,
                'spuId': spu_id,
            }}, upsert=False))

    print(len(update_bulk))
    mongo.bulk_write('data_20210331', update_bulk)
    print('任务完成~')


def fix_data_brand():
    db = pymysql.connect(host="zc-outer.mysql.rds.aliyuncs.com", port=3306, db="zc_esgcc", user="root", password="Dangerous!")
    cursor = db.cursor(pymysql.cursors.DictCursor)
    src_list = mongo.list('item_data_pool', fields={'_id': 1, 'brandName': 1, 'brandModel': 1, 'brandId': 1})
    for item in src_list:
        _id = item.get('_id')
        brand_name = item.get('brandName')
        brand_id = item.get('brandId')
        if brand_name:
            db.begin()
            sql = "update sp_sku_info set brand_name=%s, brand_id=%s where sku_id=%s"
            cursor.execute(sql, (brand_name, brand_id, _id))
            db.commit()
            print(f'update {_id}')

    print('任务完成~')


if __name__ == '__main__':
    # fix_data_spu()
    fix_data_brand()
