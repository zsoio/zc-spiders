# -*- coding: utf-8 -*-
import random
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.dao.item_data_dao import ItemDataDao
from zc_core.util.http_util import retry_request
from esgcc.rules import *
from esgcc.util.group_helper import GroupHelper
from esgcc.util.login import SeleniumLogin
from zc_core.spiders.base import BaseSpider


class GroupSpider(BaseSpider):
    name = 'group'
    # 常用链接
    # supplierId, page, startPrice, endPrice
    item_url = 'http://b.esgcc.com.cn/showDetail/{}'
    prod_detail_url = 'http://b.esgcc.com.cn/products/loadProductDetailInfomationAll?productId={}'
    prod_group_url = 'http://b.esgcc.com.cn/showDetail/getOfficeProductCompare.do?prodId={}'

    def __init__(self, batchNo=None, delta_hour=-8, *args, **kwargs):
        super(GroupSpider, self).__init__(batchNo=batchNo, hourOffset=delta_hour, *args, **kwargs)
        self.spu_helper = GroupHelper()
        self.spu_helper.init(self.batch_no)

    def start_requests(self):
        print(self.batch_no)
        cookies = SeleniumLogin().get_cookies()
        # cookies = {'JSESSIONID': '1349F12693B884C91DF4537A07CEE0B1', '__d_s_': '1349F12693B884C91DF4537A07CEE0B1', '__s_f_c_s_': '3C98C9C09BAD144C436366BE59FDA3A4', '__t_c_k_': 'c91e54176ea146ca9edaec6990596326'}
        if not cookies:
            self.logger.error('init cookie failed...')
            return
        self.logger.info('init cookie: %s', cookies)

        pool_list = ItemDataDao().get_batch_data_list(self.batch_no, query={'spuId': {'$exists': False}})
        self.logger.info('任务：%s' % (len(pool_list)))
        random.shuffle(pool_list)
        for sku in pool_list:
            sku_id = sku.get('_id')
            # 同款分组
            yield Request(
                url=self.prod_group_url.format(sku_id),
                callback=self.parse_group,
                errback=self.error_back,
                cookies=cookies,
                meta={
                    'reqType': 'group',
                    'skuId': sku_id,
                    'batchNo': self.batch_no,
                },
                priority=5,
            )

    # 处理同款商品
    def parse_group(self, response):
        meta = response.meta
        sku_id = meta.get('skuId')
        group, compare_list, done_set = parse_group(response)
        self.spu_helper.add(done_set)
        if group and len(compare_list) > 1:
            # 同款标识
            group['batchNo'] = self.batch_no
            yield group
            # 同款数据
            for compare in compare_list:
                compare['batchNo'] = self.batch_no
                yield compare
            self.logger.info(
                '同款: sku=%s, group=%s, cmp=%s' % (sku_id, len(group.get('skuIdList', [])), len(compare_list)))
        else:
            self.logger.info('无同款: sku=%s' % sku_id)
