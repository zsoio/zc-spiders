# # -*- coding: utf-8 -*-
# import scrapy
# from scrapy import Request
# from scrapy.exceptions import IgnoreRequest
# from zc_core.model.items import Order, Box, OrderStatus
# from zc_core.util.http_util import retry_request
# from esgcc.rules import *
# from esgcc.util.login import SeleniumLogin
#
#
# class OrderSpider(BaseSpider):
#     name = 'order'
#     # 常用链接
#     order_list_url = 'http://b.esgcc.com.cn/showIndex/getLastestOrdersInner.htm?pgn={}'
#     order_detail_url = 'http://buser.esgcc.com.cn/order/detail?orderId={}'
#     allowed_domains = ['esgcc.com.cn']
#
#     def __init__(self, batchNo=None, *args, **kwargs):
#         super(OrderSpider, self).__init__(*args, **kwargs)
#         if not batchNo:
#             self.batch_no = time_to_batch_no(datetime.now())
#         else:
#             self.batch_no = int(batchNo)
#
#     def start_requests(self):
#         cookies = SeleniumLogin().get_cookies()
#         # cookies = {'JSESSIONID': '01C93A63358E03D25D879BD96A90D9DD', '__d_s_': 'FB378B5340B197731D07FCAFBC390161', '__s_f_c_s_': 'A463FF84CA4C1D5B54A903F0798B08AE', '__t_c_k_': '59a9be51249c49bd94a5da8a543e19c5'}
#         if not cookies:
#             self.logger.error('init cookie failed...')
#             return
#         self.logger.info('init cookie: %s', cookies)
#
#         # 页码
#         page = 1
#         # 采集列表下一页
#         yield Request(
#             url=self.order_list_url.format(page),
#             cookies=cookies,
#             callback=self.parse_total,
#             errback=self.error_back,
#             meta={
#                 'reqType': 'order_page',
#                 'page': page,
#             },
#             priority=200,
#             dont_filter=True,
#         )
#
#     def parse_total(self, response):
#         total = parse_total_page(response)
#         if total:
#             self.logger.info('总页数: total=%s' % total)
#             for page in range(1, total):
#                 # 采集列表下一页
#                 yield Request(
#                     method='POST',
#                     url=self.order_list_url.format(page),
#                     callback=self.parse_order,
#                     errback=self.error_back,
#                     meta={
#                         'reqType': 'order_page',
#                         'page': page,
#                     },
#                     priority=20,
#                     dont_filter=True
#                 )
#
#     def parse_order(self, response):
#         # 订单列表
#         order_pages = parse_order(response)
#         cur_page = response.meta.get('page', 1)
#         if order_pages:
#             for op in order_pages:
#                 # order_user = op.get('orderUser')
#                 supplier_name = op.get('supplierName')
#                 order_dept = op.get('orderDept')
#                 order_time = op.get('orderTime')
#                 page = Order()
#                 page.update(op)
#                 yield page
#
#                 # 采集order明细页
#                 yield Request(
#                     url=self.order_detail_url.format(op.get('id')),
#                     callback=self.parse_order_item,
#                     errback=self.error_back,
#                     headers={
#                         'Referer': 'http://buser.esgcc.com.cn/order/list'
#                     },
#                     meta={
#                         'reqType': 'order',
#                         # 'orderUser': order_user,
#                         'orderId': op.get('id'),
#                         'supplierName': supplier_name,
#                         'orderDept': order_dept,
#                         'orderTime': order_time,
#                     },
#                     priority=200,
#                     dont_filter=True
#                 )
#             self.logger.info('订单列表[%s]: count[%s]' % (cur_page, len(order_pages)))
#
#     def parse_order_item(self, response):
#         meta = response.meta
#         order_id = meta.get('orderId')
#         order_time = meta.get('orderTime')
#         # 处理订单列表
#         orders = parse_order_item(response)
#         if orders:
#             yield Box('order_item', self.batch_no, orders)
#         # 标记订单已采
#         order_status = OrderStatus()
#         order_status['id'] = order_id
#         order_status['orderTime'] = order_time
#         order_status['batchNo'] = time_to_batch_no(order_time)
#         order_status['workStatus'] = 1
#         self.logger.info('完成: order=%s, cnt=%s' % (order_id, len(orders)))
#         yield order_status
#

