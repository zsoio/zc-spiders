# -*- coding: utf-8 -*-
import copy
import scrapy
from scrapy import Request
from scrapy.utils.project import get_project_settings
from scrapy.exceptions import IgnoreRequest
from zc_core.util.http_util import retry_request
from zc_core.model.items import Box
from esgcc.rules import *
from esgcc.util.login import SeleniumLogin
from zc_core.spiders.base import BaseSpider


class SkuSpider(BaseSpider):
    name = 'sku'
    custom_settings = {
        'CONCURRENT_REQUESTS': 12,
        # 'DOWNLOAD_DELAY': 0.5,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 12,
        'CONCURRENT_REQUESTS_PER_IP': 12,
    }
    # 常用链接
    index_url = 'http://b.esgcc.com.cn/showIndex/index.htm'
    # supplierId, page, startPrice, endPrice
    sku_list_url = 'http://shops.esgcc.com.cn/store/decorating/getProductViewByAjax?callback=&storeId={}&paramSTR=S2c9PQ%3D%3D&prodCat=&sortType=prod_price&currentPage={}&marketType=2&startPrice={}&endPrice={}&sortWay=up&userId=07458666&isCat=false&_=1570765869680'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        cookies = SeleniumLogin().get_cookies()
        # cookies = {'JSESSIONID': 'A3CC9662A9916C102F7C84E21FF1F034', '__d_s_': 'F7836B3AC6482A56E3DEBB9F7512ECF4', '__s_f_c_s_': 'A463FF84CA4C1D5B54A903F0798B08AE', '__t_c_k_': 'bf37ebceb6294fde965f9ac39961a13d'}
        if not cookies:
            self.logger.error('init cookie failed...')
            return
        self.logger.info('init cookie: %s', cookies)

        # 品类、品牌
        yield Request(
            url=self.index_url,
            meta={
                'reqType': 'catalog',
                'batchNo': self.batch_no,
            },
            headers={
                'X-Requested-With': 'XMLHttpRequest',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36 Edg/90.0.818.42'
            },
            cookies=cookies,
            callback=self.parse_catalog,
            errback=self.error_back,
            priority=200,
        )

    # 处理catalog列表
    def parse_catalog(self, response):
        # 品类
        print(response)
        cats = parse_catalog(response)
        if cats:
            self.logger.info('品类: count[%s]' % len(cats))
            yield Box('catalog', self.batch_no, cats)

        # sku列表第一页
        settings = get_project_settings()
        suppliers = settings.get('SUPPLIERS', {})
        ladders = settings.get('PRICE_LADDER', [])
        if ladders and suppliers:
            for sp_id in suppliers:
                for idx in range(1, len(ladders)):
                    # 采集sku列表
                    page = 1
                    start_price = ladders[idx - 1]
                    end_price = ladders[idx]
                    yield Request(
                        # supplierId, page, startPrice, endPrice
                        url=self.sku_list_url.format(sp_id, page, start_price, end_price),
                        callback=self.parse_sku_page,
                        errback=self.error_back,
                        # cookies=copy.deepcopy(response.request.cookies),
                        meta={
                            'reqType': 'sku',
                            'batchNo': self.batch_no,
                            'supplierId': sp_id,
                            'startPrice': start_price,
                            'endPrice': end_price,
                            'page': page
                        },
                        headers={
                            'Host': 'shops.esgcc.com.cn',
                            'Connection': 'keep-alive',
                            'Accept': 'text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01',
                            'X-Requested-With': 'XMLHttpRequest',
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400',
                            'Referer': 'http://shops.esgcc.com.cn/shop/{}/index.htm'.format(sp_id),
                            'Accept-Encoding': 'gzip, deflate',
                            'Accept-Language': 'zh-CN,zh;q=0.9',
                        },
                        priority=100,
                        dont_filter=True
                    )

    # 处理catalog列表
    def parse_sku_page(self, response):
        meta = response.meta
        sp_id = meta.get('supplierId')
        start_price = meta.get('startPrice')
        end_price = meta.get('endPrice')
        # 处理品类列表
        pages = parse_sku_page(response)
        if pages:
            self.logger.info('总页数: sp=%s, total=%s' % (sp_id, pages))
            if pages > 500:
                self.logger.info('分页超限: sp=%s, total=%s' % (sp_id, pages))
            for page in range(1, pages + 1):
                yield Request(
                    # supplierId, page, startPrice, endPrice
                    url=self.sku_list_url.format(sp_id, page, start_price, end_price),
                    callback=self.parse_sku,
                    errback=self.error_back,
                    # cookies=copy.deepcopy(response.request.cookies),
                    meta={
                        'reqType': 'sku',
                        'batchNo': self.batch_no,
                        'supplierId': sp_id,
                        'startPrice': start_price,
                        'endPrice': end_price,
                        'page': page
                    },
                    headers={
                        'Host': 'shops.esgcc.com.cn',
                        'Connection': 'keep-alive',
                        'Accept': 'text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01',
                        'X-Requested-With': 'XMLHttpRequest',
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400',
                        'Referer': 'http://shops.esgcc.com.cn/shop/{}/index.htm'.format(sp_id),
                        'Accept-Encoding': 'gzip, deflate',
                        'Accept-Language': 'zh-CN,zh;q=0.9',
                    },
                    priority=100,
                    dont_filter=True
                )
        else:
            self.logger.info('无分页: sp=%s, prc=<%s~%s>, total=%s' % (sp_id, start_price, end_price, pages))

    # 处理sku列表
    def parse_sku(self, response):
        meta = response.meta
        page = meta.get('page')
        sp_id = meta.get('supplierId')
        start_price = meta.get('startPrice')
        end_price = meta.get('endPrice')

        sku_list = parse_sku(response)
        if sku_list:
            self.logger.info(
                '清单: sp=%s, prc=<%s~%s>, page=%s, count=%s' % (sp_id, start_price, end_price, page, len(sku_list)))
            yield Box('sku', self.batch_no, sku_list)
        else:
            self.logger.info('末页: sp=%s, prc=<%s~%s>, page=%s' % (sp_id, start_price, end_price, page))
