# -*- coding: utf-8 -*-
import random
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from scrapy.utils.project import get_project_settings
from zc_core.spiders.base import BaseSpider
from esgcc.util.order_done_filter import OrderDoneFilter
from zc_core.dao.order_pool_dao import OrderPoolDao
from zc_core.model.items import Box, OrderStatus
from zc_core.util import file_reader
from zc_core.util.http_util import retry_request
from zc_core.util.order_deadline_filter import OrderItemFilter
from esgcc.rules import *


class OrderItemFromTxtSpider(BaseSpider):
    name = 'order_item_from_txt'
    custom_settings = {
        'CONCURRENT_REQUESTS': 3,
        'DOWNLOAD_DELAY': 0.1,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 3,
        'CONCURRENT_REQUESTS_PER_IP': 3,
    }

    # 常用链接
    api_url = 'http://m.esgcc.com.cn/common/sendPostByJsonStr'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(OrderItemFromTxtSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.order_filter = OrderDoneFilter(['202101', '202102', '202103', '202104', '202105', '202106', '202107', '202108', '202109', '202110', '202111', '202112'])

    def start_requests(self):
        month = '202109'
        token = '7755b2c24a3642769c452e9780e55570'
        if not token:
            self.logger.error('init token failed...')
            return
        self.logger.info('init token: %s', token)

        # 处理订单列表
        order_list = file_reader.read_rows(f'doc/order_list_{month}.txt')
        if order_list:
            settings = get_project_settings()
            self.logger.info('目标：%s' % (len(order_list)))
            random.shuffle(order_list)
            for order_id in order_list:
                if self.order_filter.contains(order_id) and not settings.get('FORCE_RECOVER', False):
                    self.logger.info('已采: [%s]', order_id)
                    continue

                # 采集order明细页
                yield Request(
                    method='POST',
                    url=self.api_url,
                    headers={
                        'Host': 'm.esgcc.com.cn',
                        'Accept': 'application/json',
                        'path': '/newpcenterapi/pcenter/OrderService/orderDetail',
                        'Accept-Language': 'zh-cn',
                        'Accept-Encoding': 'gzip, deflate',
                        'type': 'getUserData',
                        'token': token,
                        'User-Agent': 'okhttp/3.10.0',
                        'Connection': 'keep-alive',
                        'Content-Type': 'application/json',
                    },
                    body='{"orderId":"%s"}' % order_id,
                    meta={
                        'reqType': 'order_item',
                        'orderId': order_id,
                    },
                    callback=self.parse_order_item_ios,
                    errback=self.error_back,
                    priority=200,
                    dont_filter=True
                )

    def parse_order_item_ios(self, response):
        meta = response.meta
        order_id = meta.get('orderId')

        # 处理订单列表
        orders, order_time = parse_order_item_ios(response)
        if orders:
            yield Box('order_item', self.batch_no, orders)

        # 标记订单已采
        order_status = OrderStatus()
        order_status['id'] = order_id
        order_status['orderTime'] = order_time
        order_status['batchNo'] = time_to_batch_no(order_time)
        order_status['workStatus'] = 1
        self.logger.info('完成: order=%s, item=%s' % (order_id, len(orders)))
        yield order_status
