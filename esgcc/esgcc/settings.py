# -*- coding: utf-8 -*-
BOT_NAME = 'esgcc'

SPIDER_MODULES = ['esgcc.spiders']
NEWSPIDER_MODULE = 'esgcc.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 18
# DOWNLOAD_DELAY = 1
CONCURRENT_REQUESTS_PER_DOMAIN = 18
CONCURRENT_REQUESTS_PER_IP = 18

DEFAULT_REQUEST_HEADERS = {
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 UBrowser/6.2.4094.1 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.8',
}

DOWNLOADER_MIDDLEWARES = {
    # 'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'esgcc.middlewares.GroupFilterMiddleware': 320,
    'esgcc.validator.BizValidator': 500,
}
# 指定代理池类
PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.mogu_pool.MoguProxyPool'
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.zhima_pool.ZhimaProxyPool'
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.wandou_pool.WandouProxyPool'
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 3
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 代理积分阈值
PROXY_SCORE_LIMIT = 2
# 下载超时
DOWNLOAD_TIMEOUT = 60

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'esgcc.pipelines.EsgccMongoPipeline': 420,
    'zc_core.pipelines.catalog.CatalogCompletePipeline': 400,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
}
# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'esgcc_2019'

# 日志
LOG_LEVEL = 'INFO'
# 订单采集截止天数
ORDER_DEADLINE_DAYS = 90
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 10
# 输出
IMAGES_STORE = '/work/esgcc/'

# 供应商
SUPPLIERS = {
    '41D02A8E616E3B5C': '奥克斯空调股份有限公司',
    '48CC8465D91B6120': '东芝泰格信息系统（深圳）有限公司',
    '02ABC5161D73FEBB': '富士施乐实业发展（中国）有限公司',
    '179B0275BD9692E1': '美的空调官方店',
    '52DCBE4BC2937070': '惠普贸易（上海）有限公司',
    'CECB9456E8688439': '佳能（中国）有限公司原厂商',
    '2F5A15F0D4FD7A57': '理光（中国）投资有限公司',
    '4CAA96608D193375': '联想官方旗舰店',
    '78976423E7737152': '青岛海尔空调器有限总公司',
    '95D2C0E04E2AF15B': 'TCL空调',
    '14F67EACBCDCAEE3': '珠海奔图打印科技有限公司',
    'DDD937A6A7FB2687': '格力电器官方旗舰店',
    '00FF7CBE904BBEDB': '北京国美在线',
    '53FCBA7C5E5F9292': '成都京东世纪贸易有限公司-办公类',
    '004AC03F2C585242': '得力集团有限公司+一级采购专区',
    'AFF5E11C0284E66D': '广博集团股份有限公司',
    '84A22179768004EA': '领先未来科技集团有限公司',
    '78F557AF8605B046': '欧菲斯办公伙伴一级专区',
    '6619712258313B5A': '晨光科力普（专区店）',
    'BB11AF1D0C3A3EBE': '苏宁易购',
    '597E4D47AE98A778': '史泰博国网商城',
    '5459BCED5D6F13DE': '齐心国网采购专区',
    '9D73C604A725DC3E': '物产电商一级专区',
    '2E8047AB8C56DB9D': '联想官方旗舰店(计算机)',
    'B084F704D81C8BCB': '惠普（重庆）有限公司',
    '8AFF962D2B904B6C': '宏碁(重庆)有限公司',

    # '091DF751C77C630B': '恩益禧数码应用产品贸易（上海）有限公司',
    # '32210355A6A1F417': '夏普办公设备（常熟）有限公司',
    # '4567D4819634862F': '爱普生（中国）有限公司',
    # '60763E61DFD3225E': '京瓷办公信息系统（中国）有限公司',
    # '817BA0DA6B2D41D7': '海信（山东）空调有限公司',
    # '910F631DDEBB67F4': '深圳创维-RGB电子有限公司',
    # 'A69215603D04647D': '国美在线电子商务有限公司',
    # 'A79721BDA5632037': '青岛海信电器股份有限公司',
    # 'E1657A4FC5EA4EE1': 'TCL商用信息科技（惠州）有限责任公司',
}
# 价格区间阶梯
# PRICE_LADDER = [0, 10, 20, 30, 40, 50, 60, 80, 100, 200, 500, 1000, 10000, 10000000]
PRICE_LADDER = [0, 50, 100, 500, 1000, 10000000]
# 登录账号集合
ACCOUNTS = [
    {'13474130010': 'XXDS123456'},  # 陕西咸阳
    {'18804760096': 'ywhw960096'},
    {'13586145025': '0000aaaa'},
    {'yhb113820': '13453186180a'},  # 山西
    {'liwy7131': '0000aaa@'},  # 山西大同
    {'LN2231_P3100ZHF': 'A19910722'},  # 辽宁沈阳

    # {'jellyqgd': 'Aa19901025'},
    # {'13982849367': '2995058xd'},
    # {'13891832932': 'ZXL68003688'},
    # {'13470106886': 'TLgs0410!'},
    # {'13484854800': 'hz.123456'},
    # {'15089095100': '1234qwer'},
]
