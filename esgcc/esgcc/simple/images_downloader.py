# -*- coding: utf-8 -*-
import logging
import os
import requests
import threading
from threading import Thread
from zc_core.client.mongo_client import Mongo

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    filename='order_list_ios.log',
    filemode='a')
console = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s')
console.setFormatter(formatter)
logging.getLogger().addHandler(console)

batch_no = '20200930'

threadmax = threading.BoundedSemaphore(20)
thread_pool = list()


class Downloader(Thread):
    def __init__(self, item):
        Thread.__init__(self)
        self.item = item

    def run(self):
        sku = self.item.get('sku')
        img_type = self.item.get('img_type')
        url = self.item.get('img_url')
        path = self.item.get('img_name')
        base_path = 'G:/esgcc/sku_imgs/{}/{}/{}/'.format(batch_no, img_type, sku)
        full_path = base_path + path
        try:
            if os.path.exists(full_path):
                logging.info('Exists: path=%s, type=%s' % (path, img_type))
                return

            rs = requests.get(
                url=url,
                headers={
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400',
                },
                timeout=120
            )
            if not os.path.exists(base_path):
                os.makedirs(base_path)
            if rs.status_code == 200:
                with open(full_path, 'wb') as f:
                    f.write(rs.content)
                logging.info('OK: path=%s, type=%s' % (path, img_type))
        except Exception as ex:
            logging.info('--> retry send_req: sku_url=%s' % url)
        finally:
            threadmax.release()


if __name__ == '__main__':
    item_list = Mongo().list(collection='imgs_{}'.format(batch_no))
    for item in item_list:
        threadmax.acquire()
        ext = Downloader(item)
        thread_pool.append(ext)
        ext.start()

    for t in thread_pool:
        t.join()
    print('处理完成')
