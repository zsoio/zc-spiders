from datetime import timedelta

from scrapy import cmdline

from zc_core.util import file_reader
from zc_core.util.common import parse_time, parse_date, format_date


def gen_ids(start_date, days):
    txt = ''
    for day_offset in range(days):
        dt = parse_date(start_date, fmt='%Y%m%d') + timedelta(days=int(day_offset))
        curr_date = format_date(dt, fmt='%Y%m%d')
        start_index = int(curr_date) * 10000000000
        for idx in range(0, 8000):
            txt = txt + str(start_index + idx) + '\n'
    file_reader.write_text('D:\zc-cloud\git\zc-spiders\esgcc\esgcc\doc/order_list.txt', txt, mode='w+')

    print('gen id over~')


if __name__ == '__main__':
    # gen_ids('20210401', 23)

    cmdline.execute('scrapy crawl order_item_from_txt'.split())

    print('ok~')
