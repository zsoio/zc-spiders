import os
import random
import shutil
import threading
import time
import requests
from datetime import datetime

from zc_core.client.mongo_client import Mongo
from requests.exceptions import ProxyError
from zc_core.dao.cookie_dao import CookieDao
from zc_core.middlewares.proxies.zhima_pool import ZhimaProxyPool
from zc_core.util.batch_gen import time_to_batch_no

from esgcc.util.login import SeleniumLogin





if __name__ == '__main__':
    root_dir = 'G:/esgcc/'
    # batch_no = '20201202'
    batch_no = str(time_to_batch_no(datetime.now()))

    dist = set()
    items = Mongo().list('cert_item_{}'.format(batch_no))
    print("总共 %s" % len(items))
    random.shuffle(items)
    for item in items:
        skuId = item.get('skuId')
        url = item.get('url')
        path = item.get('path')
        full_dir = root_dir + '/cert/{}/{}'.format(batch_no, skuId)
        full_path = root_dir + path
        pool_dir = root_dir + '/cert/pool/{}'.format(skuId)
        pool_path = full_path.replace(batch_no, 'pool')

        if os.path.exists(full_path):
            print("存在 %s" % full_path)
            continue

        dist.add(skuId)

    print('任务完成 %s' % len(dist))
