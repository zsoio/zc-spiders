import shutil, os

in_base_dir = 'G:\\esgcc\\cert\\'
out_base_dir = 'G:\\esgcc\\cert4\\'

dirs = os.listdir(r'G:\\esgcc\\cert\\')
for fn in dirs:
    sku_id = fn[:fn.index('-')]
    print(sku_id)
    our_dir = out_base_dir + sku_id
    if not os.path.exists(our_dir):
        os.mkdir(our_dir)

    shutil.copy(in_base_dir + '\\' + fn, our_dir + '\\' + fn)
