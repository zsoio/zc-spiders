from scrapy.exceptions import IgnoreRequest
from esgcc.util.group_helper import GroupHelper


class GroupFilterMiddleware(object):

    def __init__(self):
        # 基于单实例，无需初始化
        self.spu_helper = GroupHelper()

    def process_request(self, request, spider):
        meta = request.meta
        req_type = meta.get('reqType', None)
        if req_type == "group":
            sku_id = meta.get('skuId', None)
            if sku_id and self.spu_helper.contains(sku_id):
                raise IgnoreRequest('同款已采: %s' % sku_id)
