# -*- coding: utf-8 -*-
from zc_core.client.mongo_client import Mongo


class OrderDoneFilter(object):
    """
    已采集商品过滤器
    用于全量商品明细重复采集时，过滤已采过的商品，避免重复采集
    """

    def __init__(self, months):
        self.item_set = set()
        if months:
            for month in months:
                items = Mongo().distinct(
                    collection='order_item_{}'.format(month),
                    dist_key='orderId',
                    fields='orderId',
                )
                for it in items:
                    self.item_set.add(it)
        else:
            raise BaseException('批次编号和集合名称至少指定一个')

    def contains(self, id):
        return id in self.item_set

    def put(self, id):
        self.item_set.add(id)


# 测试
if __name__ == '__main__':
    filter = OrderDoneFilter(['202101', '202102', '202103', '202104'])
    print(filter.contains('202101310000000021'))
