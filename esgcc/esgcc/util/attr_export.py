from datetime import datetime

import pandas as pd
from zc_core.client.mongo_client import Mongo
from zc_core.util.batch_gen import time_to_batch_no


def export_attr(collection, columns, header, file):
    db = Mongo()
    src = db.list(collection)
    exp_list = list()
    for row in src:
        attr_list = row.get('attrList', [])
        for attr in attr_list:
            exp_row = dict()
            exp_row['skuId'] = row.get('_id')
            exp_row['group'] = attr.get('group')
            exp_row['key'] = attr.get('key')
            exp_row['val'] = attr.get('val')
            exp_list.append(exp_row)

    pd.DataFrame(exp_list).to_excel(file, columns=columns, header=header, encoding='utf-8', index=False)


if __name__ == '__main__':
    # batch_no = time_to_batch_no(datetime.now())
    batch_no = '20201127'
    export_attr(
        'sku_attr_{}'.format(batch_no),
        ['skuId', 'group', 'key', 'val'],
        ['商品编码', '类别', '参数', '值'],
        'G:\\国网规格参数_{}.xlsx'.format(batch_no)
    )
