# -*- coding: utf-8 -*-
import logging
import threading

from zc_core.dao.item_data_dao import ItemDataDao
from zc_core.dao.item_pool_dao import ItemPoolDao

logger = logging.getLogger('BrandHelper')


class BrandHelper(object):
    _instance_lock = threading.Lock()
    _biz_inited = False

    def __new__(cls, *args, **kwargs):
        if not hasattr(BrandHelper, "_instance"):
            with BrandHelper._instance_lock:
                if not hasattr(BrandHelper, "_instance"):
                    BrandHelper._instance = object.__new__(cls)
        return BrandHelper._instance

    def __init__(self):
        if not self._biz_inited:
            self._biz_inited = True
            self.sku_brand_map = dict()
            self.init()

    def init(self):
        pool_list = ItemPoolDao().get_item_pool_list(fields={'_id': 1, 'brandId': 1, 'brandName': 1, 'brandModel': 1, 'onSaleTime': 1})
        for item in pool_list:
            brand_id = item.get('brandId', '')
            brand_name = item.get('brandName', '')
            brand_model = item.get('brandModel', '')
            # on_sale_time = item.get('onSaleTime', '')
            if brand_id and brand_name and brand_model:
                self.sku_brand_map[item.get('_id')] = item
        logger.info('已分组: %s' % (len(self.sku_brand_map)))

    def put(self, sku_id, item):
        self.sku_brand_map[sku_id] = item

    def get(self, sku_id):
        return self.sku_brand_map.get(sku_id, None)
