# -*- coding: utf-8 -*-
import random
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from datetime import datetime
from zc_core.dao.batch_dao import BatchDao
from zc_core.dao.item_data_dao import ItemDataDao
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from cec.utils.done_filter import DoneFilter
from cec.rules import *
from zc_core.spiders.base import BaseSpider


class priceSpider(BaseSpider):
    name = 'price'
    custom_settings = {
        'CONCURRENT_REQUESTS': 32,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 32,
        'CONCURRENT_REQUESTS_PER_IP': 32,
    }
    # 获取价格链接
    price_url = 'https://mall.cec-ec.com.cn/api/shop/goods/product-list.do?goodsid={}'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(priceSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)
        # 避免重复采集
        self.done_filter = DoneFilter(self.batch_no)

    def start_requests(self):

        item_list = ItemDataDao().get_batch_data_list(self.batch_no)
        # print(item_list)
        self.logger.info('目标：%s' % (len(item_list)))
        random.shuffle(item_list)
        for item in item_list:
            sku_id = item.get('_id')
            # 采集销售额
            yield Request(
                url=self.price_url.format(sku_id),
                meta={
                    'reqType': 'item',
                    'batchNo': self.batch_no,
                    'skuId': sku_id,
                },
                callback=self.parse_sales,
                errback=self.error_back,
                priority=25,
            )

    # 处理ItemData
    def parse_sales(self, response):
        meta = response.meta
        sku_id = meta.get('skuId')
        data = parse_prices(response)
        if data:
            self.logger.info('销售价格: sku=%s,spri=%s 库存:stock=%s' % (sku_id, data.get('salePrice'), data.get('stock')))
            yield data
        else:
            self.logger.error('异常: [%s]' % sku_id)
