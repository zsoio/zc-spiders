# -*- coding: utf-8 -*-
from zc_core.client.mongo_client import Mongo
from zc_core.dao.catalog_dao import CatalogDao


class cec_CatalogDao(CatalogDao):
    """
    重写批次商品操作简易封装（Mongo）
    """

    # 获取批次分类列表
    def get_cat_list_from_pool(self, fields={'_id': 1, 'catalogName': 1, 'parentId': 1,}, query={}):
        return Mongo().list('catalog_pool', query=query, fields=fields)
