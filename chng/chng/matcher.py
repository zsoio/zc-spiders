# -*- coding: utf-8 -*-
import re


def match_brand_with_brackets(br):
    # 史密斯（A.O.SMITH)   史密斯(A.O.SMITH)
    if br:
        br = br.replace('(', '（').replace(')', '）')
        arr = re.findall(r'(.+)（(.+)）', br.strip())
        if len(arr):
            return arr[0]

    return '', ''


def match_brand_with_no_brackets(br):
    # 史密斯（A.O.SMITH)   史密斯(A.O.SMITH)
    if br:
        cn = re.sub(u"([^\u4e00-\u9fa5\.\s])", "", br).strip('.').strip()
        en = re.sub(u"([^\u0041-\u007a\-\&\.\s])", "", br).strip()
        return cn, en

    return '', ''


def clean_text(txt):
    if txt:
        return txt.replace('(', '').replace('（', '').replace('）', '').replace(')', '').strip()
    return txt


# 测试
if __name__ == '__main__':
    print(match_brand_with_brackets('史密斯（A.O.SMITH)'))
    print(match_brand_with_brackets('史密斯(A.O.SMITH)'))
    print(match_brand_with_brackets('史密斯（A.O.SMITH）'))

    print(match_brand_with_no_brackets('史密斯 A.O.SMITH'))
    pass
