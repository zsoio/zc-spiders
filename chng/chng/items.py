# -*- coding: utf-8 -*-
import scrapy
from zc_core.model.items import BaseData


# 查询条件数据
class MetaBrand(BaseData):
    _id = scrapy.Field()
    cn = scrapy.Field()
    en = scrapy.Field()
    main_brand = scrapy.Field()
    plat = scrapy.Field()
    type = scrapy.Field()
    num = scrapy.Field()
