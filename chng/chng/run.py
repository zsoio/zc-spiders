from scrapy import cmdline

# cmdline.execute('scrapy crawl meta'.split())
# cmdline.execute('scrapy crawl meta -a batchNo=20210529'.split())
# cmdline.execute('scrapy crawl sku'.split())
# cmdline.execute('scrapy crawl sku -a batchNo=20210529'.split())
# cmdline.execute('scrapy crawl full'.split())
cmdline.execute('scrapy crawl full -a batchNo=20210829'.split())
# cmdline.execute('scrapy crawl full_cat -a batchNo=20210709'.split())


#TODO 特殊场景使用,分类池缺少分类，导致sku补不全，这个程序是补池子分类的，补完了后利用fix_data进行将池子里面的分类补到sku，进行query快速筛选抓取
# cmdline.execute('scrapy crawl fix_catalog'.split())