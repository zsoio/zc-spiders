# -*- coding: utf-8 -*-
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.client.mongo_client import Mongo
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from chng.rules import *
from zc_core.spiders.base import BaseSpider


class MetaSpider(BaseSpider):
    name = 'meta'
    # 常用链接
    sku_list_url = 'http://mall.ec.chng.com.cn/scm-hn-oauth-web/obs/business/product/ProductSearch/search'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(MetaSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 页数限制
        self.max_page_limit = 166
        self.page_size = 30

    def start_requests(self):
        br_filter = set()
        brand_list = Mongo().list('meta_brand_pool')
        for br in brand_list:
            for kw in [br.get('cn'), br.get('en'), br.get('main_brand')]:
                if kw and kw not in br_filter:
                    br_filter.add(kw)
                    yield self.build_req(kw)

    def build_req(self, kw):
        return Request(
            method='POST',
            url=self.sku_list_url,
            body=json.dumps({
                "keywords": kw,
                "limit": 30
            }),
            meta={
                'reqType': 'sku',
                'batchNo': self.batch_no,
                'kw': kw,
            },
            headers={
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'zh-CN,zh;q=0.9',
                'Cache-Control': 'no-cache',
                'Connection': 'keep-alive',
                'Cookie': '__root_domain_v=.chng.com.cn; _qddaz=QD.839827440126844',
                'Content-Type': 'application/json',
                'Host': 'mall.ec.chng.com.cn',
                'Origin': 'http://mall.ec.chng.com.cn',
                'Pragma': 'no-cache',
                'Referer': 'http://mall.ec.chng.com.cn/list?keywords={}'.format(kw),
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36'
            },
            callback=self.parse_meta_brand_list,
            errback=self.error_back
        )

    def parse_meta_brand_list(self, response):
        meta = response.meta
        kw = meta.get("kw")
        meta_info = parse_meta_brand(response)
        brand_list = meta_info.get('brandCode', [])
        if not meta_info or not brand_list:
            self.logger.info('无品牌筛选: kw=%s' % kw)
            return

        self.logger.info('清单: kw=%s' % kw)
        yield Box('meta_brand', self.batch_no, brand_list)
