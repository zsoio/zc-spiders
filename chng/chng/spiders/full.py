# -*- coding: utf-8 -*-
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from scrapy.utils.project import get_project_settings
from zc_core.dao.sku_dao import SkuDao
from zc_core.dao.item_pool_dao import ItemPoolDao
from zc_core.dao.sku_pool_dao import SkuPoolDao
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from zc_core.dao.batch_dao import BatchDao
from zc_core.util.done_filter import DoneFilter
from zc_core.spiders.base import BaseSpider
from chng.rules import *


class FullSpider(BaseSpider):
    name = "full"
    # 详情页url
    item_url = "http://mall.ec.chng.com.cn/scm-hn-oauth-web/obs/business/product/managerView/getProduct?productId={}&returnProduct=true&returnSku=true&returnSpec=true&returnSKuSpec=true"

    def __init__(self, batchNo=None, delta_day=-2, *args, **kwargs):
        super(FullSpider, self).__init__(batchNo=batchNo, delta_day=delta_day, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)
        # 避免重复采集
        self.done_filter = DoneFilter(self.batch_no)

    def start_requests(self):
        settings = get_project_settings()
        sku_list = SkuPoolDao().get_sku_pool_list(fields={"_id": 1, "offlineTime": 1, "soldCount": 1})
        # sku_list = SkuPoolDao().get_sku_pool_list(fields={"_id": 1, "offlineTime": 1, "soldCount": 1}, query={
        #     'catalog1Name': {'$in': ['办公设备', '办公耗材']}
        # })
        self.logger.info('全量：%s' % (len(sku_list)))
        for sku in sku_list:
            sku_id = sku.get("_id")
            sold_count = sku.get("soldCount")
            # 避免无效采集
            offline_time = sku.get('offlineTime', 0)
            if offline_time > settings.get('MAX_OFFLINE_TIME', 2):
                self.logger.info('忽略: [%s][%s]', sku_id, offline_time)
                continue
            # 避免重复采集
            if self.done_filter.contains(sku_id) and not settings.get('FORCE_RECOVER', False):
                self.logger.info('已采: [%s]', sku_id)
                continue

            yield Request(
                url=self.item_url.format(sku_id),
                headers={
                    'Accept': 'application/json, text/plain, */*',
                    'Accept-Encoding': 'gzip, deflate',
                    'Accept-Language': 'zh-CN,zh;q=0.9',
                    'Host': 'mall.ec.chng.com.cn',
                    'Referer': 'http://mall.ec.chng.com.cn/item?productId={}'.format(sku_id),
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36'
                },
                meta={
                    'reqType': 'full',
                    'batchNo': self.batch_no,
                    "skuId": sku_id,
                    "soldCount": sold_count,
                    "catalog1Name": sku.get('catalog1Name'),
                    "catalog2Name": sku.get('catalog2Name'),
                    "catalog3Name": sku.get('catalog3Name'),
                    "catalog1Id": sku.get('catalog1Id'),
                    "catalog2Id": sku.get('catalog2Id'),
                    "catalog3Id": sku.get('catalog3Id'),
                },
                callback=self.parse_item_data,
                errback=self.error_back
            )

    def parse_item_data(self, response):
        meta = response.meta
        sku_id = meta.get("skuId")
        item = parse_item_data(response)
        if item:
            self.logger.info('商品: [%s]' % sku_id)
            yield item
        else:
            self.logger.error('下架: sku=%s' % sku_id)
