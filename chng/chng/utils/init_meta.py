import random
import re

from pymongo import UpdateOne

from chng.matcher import match_brand_with_brackets, match_brand_with_no_brackets, clean_text
from zc_core.client.mongo_client import Mongo
from zc_core.util import file_reader

mongo = Mongo()


# def init_brand_meta(plat):
#     update_bulk = list()
#     rows = file_reader.read_rows('../doc/brand-{}.txt'.format(plat))
#     for row in rows:
#         arr = row.split('\t')
#         if arr:
#             cn_name = arr[0].strip()
#             en_name = ''
#             if len(arr) == 2:
#                 en_name = arr[1].strip()
#                 if cn_name != en_name:
#                     main_brand = '{}（{}）'.format(cn_name, en_name)
#                     item_cn_en = 'brandcode:品牌:{}:{}'.format(main_brand, main_brand)
#                     update_bulk.append(UpdateOne({'_id': item_cn_en}, {'$set': {
#                         '_id': item_cn_en,
#                         'cn': cn_name,
#                         'en': en_name,
#                         'main_brand': main_brand,
#                         'type': 'cn_en',
#                         'plat': plat,
#                     }}, upsert=True))
#
#             item_cn = 'brandcode:品牌:{}:{}'.format(cn_name, cn_name)
#             update_bulk.append(UpdateOne({'_id': item_cn}, {'$set': {
#                 '_id': item_cn,
#                 'cn': cn_name,
#                 'en': en_name,
#                 'main_brand': cn_name,
#                 'type': 'cn',
#                 'plat': plat,
#             }}, upsert=True))
#
#     print(len(update_bulk))
#     mongo.bulk_write('meta_brand_pool', update_bulk)
#     print('任务完成~')
#
#
# def init_brand_meta2(plat):
#     update_bulk = list()
#     rows = file_reader.read_rows('../doc/brand-{}.txt'.format(plat))
#     for row in rows:
#         if '\t' in row:
#             arr = row.split('\t')
#             if arr:
#                 cn_name = arr[0].strip()
#                 en_name = ''
#                 if len(arr) == 2:
#                     en_name = arr[1].strip()
#                     if cn_name != en_name:
#                         main_brand = '{}（{}）'.format(cn_name, en_name)
#                         item_cn_en = 'brandcode:品牌:{}:{}'.format(main_brand, main_brand)
#                         update_bulk.append(UpdateOne({'_id': item_cn_en}, {'$set': {
#                             '_id': item_cn_en,
#                             'cn': cn_name,
#                             'en': en_name,
#                             'main_brand': main_brand,
#                             'type': 'cn_en',
#                             'plat': plat,
#                         }}, upsert=True))
#
#                 item_cn = 'brandcode:品牌:{}:{}'.format(cn_name, cn_name)
#                 update_bulk.append(UpdateOne({'_id': item_cn}, {'$set': {
#                     '_id': item_cn,
#                     'cn': cn_name,
#                     'en': en_name,
#                     'main_brand': cn_name,
#                     'type': 'cn',
#                     'plat': plat,
#                 }}, upsert=True))
#         else:
#             cn_name, en_name = parse_brand(row)
#             if cn_name != en_name:
#                 main_brand = '{}（{}）'.format(cn_name, en_name)
#                 item_cn_en = 'brandcode:品牌:{}:{}'.format(main_brand, main_brand)
#                 update_bulk.append(UpdateOne({'_id': item_cn_en}, {'$set': {
#                     '_id': item_cn_en,
#                     'cn': cn_name,
#                     'en': en_name,
#                     'main_brand': main_brand,
#                     'type': 'cn_en',
#                     'plat': plat,
#                 }}, upsert=True))
#
#             item_cn = 'brandcode:品牌:{}:{}'.format(cn_name, cn_name)
#             update_bulk.append(UpdateOne({'_id': item_cn}, {'$set': {
#                 '_id': item_cn,
#                 'cn': cn_name,
#                 'en': en_name,
#                 'main_brand': cn_name,
#                 'type': 'cn',
#                 'plat': plat,
#             }}, upsert=True))
#
#     print(len(update_bulk))
#     mongo.bulk_write('meta_brand_poolx', update_bulk)
#     print('任务完成~')


def init_brand_meta(plat):
    update_bulk = list()
    rows = file_reader.read_rows('../doc/brand-{}.txt'.format(plat))
    for row in rows:
        cn_name = ''
        en_name = ''

        if '\t' in row:
            arr = row.split('\t')
            if arr:
                cn_name = clean_text(arr[0])
                if len(arr) == 2:
                    en_name = clean_text(arr[1])
        else:
            cn_name, en_name = parse_brand(row)

        if cn_name != en_name:
            main_brand = '{}（{}）'.format(cn_name, en_name)
            item_cn_en = 'brandcode:品牌:{}:{}'.format(main_brand, main_brand)
            update_bulk.append(UpdateOne({'_id': item_cn_en}, {'$set': {
                '_id': item_cn_en,
                'cn': cn_name,
                'en': en_name,
                'main_brand': main_brand,
                'type': 'cn_en',
                'plat': plat,
            }}, upsert=True))

        item_cn = 'brandcode:品牌:{}:{}'.format(cn_name, cn_name)
        update_bulk.append(UpdateOne({'_id': item_cn}, {'$set': {
            '_id': item_cn,
            'cn': cn_name,
            'en': en_name,
            'main_brand': cn_name,
            'type': 'cn',
            'plat': plat,
        }}, upsert=True))

    print(len(update_bulk))
    # mongo.bulk_write('meta_brand_{}_pool'.format(plat), update_bulk)
    mongo.bulk_write('meta_brand_pool', update_bulk)
    print('任务完成~')


def parse_brand(br):
    cn = ''
    en = ''
    if '(' in br or ')' in br or '（' in br or '）' in br:
        cn, en = match_brand_with_brackets(br)
    else:
        cn, en = match_brand_with_no_brackets(br)
    cn = clean_text(cn)
    en = clean_text(en)
    if cn and not en:
        en = cn
    if en and not cn:
        cn = en

    return cn, en


if __name__ == '__main__':
    init_brand_meta('staples')
    init_brand_meta('ehsy')
    init_brand_meta('deli')
    init_brand_meta('comix')
