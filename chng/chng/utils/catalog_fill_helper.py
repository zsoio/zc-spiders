# # -*- coding: utf-8 -*-
# import logging
# import threading
# from scrapy import Request
# from scrapy.exceptions import IgnoreRequest
# from zc_core.client.mongo_client import Mongo
# from zc_core.util.http_util import retry_request
# from zc_core.util.batch_gen import time_to_batch_no
# from datetime import datetime
# from chng.rules import parse_item_cat
#
# logger = logging.getLogger('CatalogFillHelper')
#
# class CatalogFillHelper(object):
#     _instance_lock = threading.Lock()
#     _biz_inited = False
#
#     # 详情页url
#     item_url = "http://ec.chng.com.cn/scm-hn-oauth-web/obs/business/product/managerView/getProductCategory?productId={}"
#
#     def __new__(cls, *args, **kwargs):
#         if not hasattr(CatalogFillHelper, "_instance"):
#             with CatalogFillHelper._instance_lock:
#                 if not hasattr(CatalogFillHelper, "_instance"):
#                     CatalogFillHelper._instance = object.__new__(cls)
#         return CatalogFillHelper._instance
#
#     def __init__(self):
#         if not self._biz_inited:
#             self._biz_inited = True
#             self.mongo = Mongo().li
#             coll = self.mongo.get_collection('item_data_pool').find({}, {"_id":1,"catalog1Id": 1,"catalog2Id": 1,"catalog3Id": 1,"catalog1Name": 1,"catalog2Name": 1,"catalog3Name": 1})
#             if coll:
#                 self._build_plain_cat(coll)
#
#     def _build_plain_cat(self, pool):
#         self.item_map = dict()
#         for row in pool:
#             if row.get("catalog1Id") and row.get("catalog2Id") and row.get("catalog3Id"):
#                 self.item_map[row.get("_id")] = row
#
#     def fill(self, item, batchNo=None):
#         if not batchNo:
#             self.batch_no = time_to_batch_no(datetime.now())
#         else:
#             self.batch_no = int(batchNo)
#         if item:
#             sku_id = item.get("_id")
#             if self.item_map.get(sku_id):
#                 cat = self.item_map.get(sku_id)
#                 # 一级分类编号
#                 item['catalog1Id'] = cat.get('catalog1Id')
#                 # 一级分类名称
#                 item['catalog1Name'] = cat.get('catalog1Name')
#                 # 二级分类编号
#                 item['catalog2Id'] = cat.get('catalog2Id')
#                 # 二级分类名称
#                 item['catalog2Name'] = cat.get('catalog2Name')
#                 # 三级分类名称
#                 item['catalog3Id'] = cat.get('catalog3Id')
#                 # 三级分类名称
#                 item['catalog3Name'] = cat.get('catalog3Name')
#             else:
#                 self.start_requests(sku_id)
#
#     def start_requests(self,sku_id):
#         yield Request(
#             url=self.item_url.format(sku_id),
#             headers={
#                 'Host': 'ec.chng.com.cn',
#                 'Connection': 'keep-alive',
#                 'Content-Type': 'application/json',
#                 'Accept': 'application/json, text/plain, */*',
#                 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3775.400 QQBrowser/10.6.4208.400',
#                 'Referer': 'http://ec.chng.com.cn/newmall',
#                 'Accept-Encoding': 'gzip, deflate',
#                 'Accept-Language': 'zh-CN,zh;q=0.9',
#             },
#             meta={
#                 'reqType': 'catalog',
#                 'batchNo': self.batch_no,
#                 "skuId": sku_id,
#             },
#             callback=self.parse_item_cat,
#             errback=self.error_back
#         )
#
#     def parse_item_cat(self, response):
#         meta = response.meta
#         sku_id = meta.get("skuId")
#         item = parse_item_cat(response)
#         if item:
#             self.logger.info('商品分类: sku=%s' % sku_id)
#             return item
#         else:
#             self.logger.error('无分类: sku=%s' % sku_id)
#
