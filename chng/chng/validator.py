# encoding=utf-8
"""
响应对象业务校验
"""
from zc_core.middlewares.validate import BaseValidateMiddleware


class BizValidator(BaseValidateMiddleware):
    pass
