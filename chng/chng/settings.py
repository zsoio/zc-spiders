# -*- coding: utf-8 -*-
BOT_NAME = 'chng'

SPIDER_MODULES = ['chng.spiders']
NEWSPIDER_MODULE = 'chng.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 12
# DOWNLOAD_DELAY = 1
CONCURRENT_REQUESTS_PER_DOMAIN = 12
CONCURRENT_REQUESTS_PER_IP = 12

DEFAULT_REQUEST_HEADERS = {
    'Host': 'ec.chng.com.cn',
    'Connection': 'keep-alive',
    'Content-Type': 'application/json',
    'Accept': 'application/json, text/plain, */*',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3775.400 QQBrowser/10.6.4208.400',
    'Referer': 'http://ec.chng.com.cn/newmall',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.9',
}

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'chng.validator.BizValidator': 500,
}
# 指定代理池类
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.mogu_pool.MoguProxyPool'
# ROXY_POOL_CLASS = 'zc_core.middlewares.proxies.wandou_pool.WandouProxyPool'
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 下载超时
DOWNLOAD_TIMEOUT = 30
# 响应重试状态码
CUSTOM_RETRY_CODES = []

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    # 'zc_core.pipelines.catalog.CatalogCompletePipeline': 400,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
    'chng.pipelines.ChngPipeline': 650
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'chng_2020'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 40
# 订单采集截止天数
ORDER_DEADLINE_DAYS = 90
# 允许响应内容为空（兼容订单）
ALLOW_EMPTY_RESPONSE = True
# 已采商品强制覆盖重采
# FORCE_RECOVER = True
