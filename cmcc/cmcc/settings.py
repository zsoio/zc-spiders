# -*- coding: utf-8 -*-
BOT_NAME = 'cmcc'

SPIDER_MODULES = ['cmcc.spiders']
NEWSPIDER_MODULE = 'cmcc.spiders'

ROBOTSTXT_OBEY = False
CONCURRENT_REQUESTS = 12
# DOWNLOAD_DELAY = 3
CONCURRENT_REQUESTS_PER_DOMAIN = 12
CONCURRENT_REQUESTS_PER_IP = 12
# COOKIES_ENABLED = False

DOWNLOADER_MIDDLEWARES = {
    # 'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    # 'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'cmcc.validator.BizValidator': 500
}
# 指定代理池类
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.mogu_pool.MoguProxyPool'
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 下载超时
DOWNLOAD_TIMEOUT = 45

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
    'zc_core.pipelines.filter.FilterPoolPipeline': 440,
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'cmcc_2019'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 3
# 标记离线商品续存批次数
RETRY_TIMES = 4
# 订单采集截止天数
ORDER_DEADLINE_DAYS = 180
# 登录重试次数
MAX_LOGIN_RETRY = 2

# 登录账号集合
ACCOUNTS = [
    # {'xusumei@gd.cmcc': 'BBbb2018'},
    # {'shareqt@hq.cmcc': 'Quantong2017'},
    # {'yuyaozhi@yn.cmcc': '871205yuboYYZH'},
    # {'duanweixian@yn.cmcc': 'Duanlj10086'},
    # {'lujianjun@gx.cmcc': 'LUlu852963'},
    # {'linyanyan@fj.cmcc': '821106Lyy'},
    # {'lianchunji@fj.cmcc': 'Lcj239718'},
    # {'xuefang@nm.cmcc': 'Xf112233'},
    # {'quxiaofang@nm.cmcc': 'Qxf965274'},
    # {'fuyan@sn.cmcc': 'hD7Ac50Z'},
    # {'kuguan1@ln.cmcc': '8Jf5NfJ6'},
    # {'tzcss@zj.cmcc': '147258Aa'},
]

# 供应商
SUPPLIERS = {
    '京东': '25a2f486759911e6aabc0894ef108494',
    '得力': '25a547ae759911e6aabc0894ef108494',
    '欧菲斯': '33766d0e96a5487a9b725d42a3b08447',
    '史泰博': '52ce53f246b05c27f214a82875ecc623',
    '齐心': '616e2e930db45d7feb79d84507b397f0',
    '晨光科力普': 'e5212022d12b422db520f899da5faba4',
    '苏宁易购': '503ccbd483be476b944e5f62a2912bf0',
}

# 输出
OUTPUT_ROOT = '/work/cmcc/'

REDIRECT_ENABLED = False