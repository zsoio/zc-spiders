import re


# 从链接中解析品类编码
def match_sku_id(link):
    # http://b2bjoy.10086.cn/oscp/goods/product/goodsDetail.html?sn=108876
    if link:
        arr = re.findall(r'sn=(\d+)&*', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析品类编码
def match_cat_id(link):
    # http://b2bjoy.10086.cn:80/oscp/goods/category/goodsList.html?id=19aa3669f52a48a3a8e393dd3efd431d
    if link:
        arr = re.findall(r'id=(\w+)&*', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 解析品牌编码
def match_brand_id(link):
    # http://b2bjoy.10086.cn/oscp/goods/category/goodsList.html?id=19aa3669f52a48a3a8e393dd3efd431d&brandId=cfd6297b0932497d92a2c8ecaaaaa6f2
    if link:
        arr = re.findall(r'brandId=(\w+)&*', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 解析物料编码
def match_material_code(link):
    # 10192375 (电商平台\办公文具\办公常用工具及耗材\笔筒)
    if link:
        arr = re.findall(r'^(\d+)\s\(.+\)', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 解析物料编码
def match_supplier_sku_id(txt):
    # 232248 (供应商商品编号：2015446)
    if txt:
        arr = re.findall(r'\(供应商商品编号：(\w+)\)', txt.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 订单明细匹配数量
def match_amount(link):
    # 300盒
    if link:
        arr = re.findall(r'(\d+)\w*', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 可见订单数量
def match_total_order(txt):
    # 当前第1条到第15条记录,共15条记录
    if txt:
        arr = re.findall(r'共(\d+)条记录', txt.strip())
        if len(arr):
            return int(arr[0])
    return 0


if __name__ == '__main__':
    print(match_supplier_sku_id('(供应商商品编号：2015446)'))
    # print(match_material_code('10192375 (电商平台\办公文具\办公常用工具及耗材\笔筒)'))
    # print(match_brand_id('http://b2bjoy.10086.cn/oscp/goods/category/goodsList.html?id=19aa3669f52a48a3a8e393dd3efd431d&brandId=cfd6297b0932497d92a2c8ecaaaaa6f2'))