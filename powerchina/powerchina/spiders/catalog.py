# -*- coding: utf-8 -*-
from datetime import datetime
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from powerchina.rules import *
from zc_core.spiders.base import BaseSpider


class CatalogSpider(BaseSpider):
    name = 'catalog'
    # 分类链接
    root_cat_url = "https://emall.powerchina.cn/eshop/loginzb.do"
    sub_cat_url = "https://emall.powerchina.cn/eshop/front/goods/categoryTreeShow.do?level=1&is_leaf=0&pid=0&id={}"

    def __init__(self, batchNo=None, *args, **kwargs):
        super(CatalogSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.page_size = 100

    def start_requests(self):
        yield Request(
            url=self.root_cat_url,
            meta={
                'reqType': 'catalog',
                'batchNo': self.batch_no
            },
            callback=self.parse_root_catalog,
            errback=self.error_back,
        )

    def parse_root_catalog(self, response):
        cats = parse_root_catalog(response)
        if cats and len(cats) > 0:
            self.logger.info(f'根分类: count={len(cats)}')
            yield Box('catalog', self.batch_no, cats)

            for cat1 in cats:
                cat1_id = cat1.get('catalogId')
                # 子菜单列表
                yield scrapy.Request(
                    url=self.sub_cat_url.format(cat1_id),
                    meta={
                        'reqType': 'catalog',
                        'catalog1Id': cat1_id,
                        'batchNo': self.batch_no,
                    },
                    callback=self.parse_sub_catalog,
                    errback=self.error_back,
                )

    def parse_sub_catalog(self, response):
        meta = response.meta
        cat1_id = meta.get('catalog1Id')

        cats = parse_sub_catalog(response)
        if cats and len(cats) > 0:
            self.logger.info(f'子分类: pid={cat1_id}, cnt={len(cats)}')
            yield Box('catalog', self.batch_no, cats)
        else:
            self.logger.info(f'无子类: pid={cat1_id}')
