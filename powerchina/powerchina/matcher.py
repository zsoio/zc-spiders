# -*- coding: utf-8 -*-
import re


# 从链接中解析品类编码
def match_catalog_id(link):
    # https://emall.powerchina.cn/eshop/front/goods/categoryTreeShow.do?level=1&is_leaf=0&pid=0&id=19206
    if link:
        arr = re.findall(r'&id=(\w+)', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


def match_sku_id(link):
    # /eshop/front/goods/detail.do?gid=2494932&pid=
    if link:
        arr = re.findall(r'gid=(\w+)', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


def match_brand_id(link):
    # https://emall.powerchina.cn:443/eshop/goodsListByBrandZb.do?id=423580
    if link:
        arr = re.findall(r'id=(\w+)', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


def match_min_buy(link):
    # 1台
    if link:
        arr = re.findall(r'^(\d+)', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 测试
if __name__ == '__main__':
    print(match_min_buy('1台'))
    pass
