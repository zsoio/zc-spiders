def create_data():
    """创建测试数据,  文件中生成200个IP"""
    with open('ips.txt', 'w') as f:
        for i in range(200):
            f.write('172.25.254.%s\n' % (i + 1))
        print("测试数据创建完成!")


from urllib.request import urlopen
from concurrent.futures import ThreadPoolExecutor


def producer(url):
    """生产测试需要的url地址http://ip:port"""
    print("生产者生产url：%s" % (url))
    return url


def consumer(future):
    # 获取producer的返回值；
    url = future.result()
    try:
        urlObj = urlopen(url)
    except Exception as e:
        print("%s不可访问" % (url))
    else:
        pageContentSize = len(urlObj.read().decode('utf-8'))
        print("%s可以访问, 页面大小为%s" % (url, pageContentSize))


def main():
    pool = ThreadPoolExecutor(max_workers=5)
    ports = [80, 443, 7001, 7002, 8000, 8080, 9000, 9001]

    with open("ips.txt") as f:
        for line in f:
            ip = line.strip()
            for port in ports:
                url = "http://%s:%s" % (ip, port)
                # producer函数的返回值会回调给consumer函数；
                res = pool.submit(producer, url).add_done_callback(consumer)


main()
