# coding:utf-8
import threading

import redis


class TokenQueue(object):
    _instance_lock = threading.Lock()
    _biz_inited = False

    queue = 'ctaxccgp:order:token'

    def __init__(self):
        if not self._biz_inited:
            self._biz_inited = True
            self.rds = redis.StrictRedis(host='192.168.1.200', port='6379', db=3, decode_responses=True)

    def __new__(cls, *args, **kwargs):
        if not hasattr(TokenQueue, "_instance"):
            with TokenQueue._instance_lock:
                if not hasattr(TokenQueue, "_instance"):
                    TokenQueue._instance = object.__new__(cls)
        return TokenQueue._instance

    def push(self, val):
        return self.rds.lpush(self.queue, val)

    def pop(self):
        return self.rds.brpop(self.queue)[1]
