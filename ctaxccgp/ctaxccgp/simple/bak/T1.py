import aiohttp
import asyncio


async def main():
    async with aiohttp.ClientSession() as session:
        async with session.get(
                url='https://ctaxccgp.zcygov.cn/items/18636828?utm=a0017.b0231.cl47.11.89aeb3101d7c11ebbc03fbf722d1e107&skuId=22660497',
                proxy='https://114.104.19.11:4265',
                verify_ssl=False
        ) as response:
            print("Status:", response.status)
            print("Content-type:", response.headers['content-type'])

            html = await response.text()
            print("Body:", html, "...")


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
