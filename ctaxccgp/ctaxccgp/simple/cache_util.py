from zc_core.client.redis_client import Redis
from zc_core.util.encrypt_util import md5


def add(key, val):
    rds_key = 'ctaxccgp:order:cache:{}'.format(md5(key))
    return Redis().client.append(rds_key, val)


def exists(key):
    rds_key = 'ctaxccgp:order:cache:{}'.format(md5(key))
    return Redis().client.exists(rds_key)


def get(key):
    rds_key = 'ctaxccgp:order:cache:{}'.format(md5(key))
    return Redis().client.get(rds_key)
