# -*- coding: utf-8 -*-
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.util.http_util import retry_request
from zc_core.spiders.base import BaseSpider
from ctaxccgp.rules import *


class AreaSpider(BaseSpider):
    name = 'area'
    # 常用链接
    area_url = 'https://ctaxccgp.zcygov.cn/front/index/common/{}/children'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(AreaSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        # 首页分类
        yield Request(
            url=self.area_url.format(0),
            meta={
                'reqType': 'area',
                'batchNo': self.batch_no,
                'pid': 0,
            },
            callback=self.parse_area,
            errback=self.error_back,
            priority=100,
        )

    def parse_area(self, response):
        meta = response.meta
        pid = meta.get('pid')
        # 处理品类列表
        areas = parse_area(response)
        if areas:
            self.logger.info('城市: pid=%s, count[%s]' % (pid, len(areas)))
            yield Box('area', self.batch_no, areas)

            for area in areas:
                area_id = area.get('areaId')
                yield Request(
                    url=self.area_url.format(area_id),
                    meta={
                        'reqType': 'area',
                        'batchNo': self.batch_no,
                        'pid': area_id,
                    },
                    callback=self.parse_area,
                    errback=self.error_back,
                    priority=100,
                )
