# -*- coding: utf-8 -*-
import random

from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.client.mongo_client import Mongo
from zc_core.util.http_util import retry_request
from ctaxccgp.rules import *
from zc_core.spiders.base import BaseSpider


class CatAttrsSpider(BaseSpider):
    name = 'cat_attrs'
    custom_settings = {
        'CONCURRENT_REQUESTS': 8,
        'DOWNLOAD_DELAY': 0.11,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 8,
        'CONCURRENT_REQUESTS_PER_IP': 8,
        'DOWNLOADER_MIDDLEWARES': {
            'ctaxccgp.middlewares.ZcyCatAttrMiddleware': 740,
            # 'zc_core.middlewares.proxy.ProxyMiddleware': 650,
            # 'zc_core.middlewares.agent.UserAgentMiddleware': 640,
            'ctaxccgp.validator.BizValidator': 500
        }
    }

    # 常用链接
    attr_url = 'https://www.zcygov.cn/api/micro/item/v1/renderPublishPage?timestamp=1604415525&layer=28&flag=0&categoryId={}&categoryType=0&spuId=&isNewBusiness=true'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(CatAttrsSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # self.cookie = {
        #     'districtCode': '001000',
        #     'districtName': '%E5%9B%BD%E5%AE%B6%E7%A8%8E%E5%8A%A1%E6%80%BB%E5%B1%80%E6%9C%AC%E7%BA%A7',
        #     'SESSION': 'ZmE3YjAxMTMtNjM3NC00YTY3LTkxMTEtMmFkM2Q5NDg4ZmNi',
        # }

    def start_requests(self):
        rows = list(Mongo().get_collection('catalog_pool').find({'level': 3}))
        self.logger.info('任务：%s' % (len(rows)))
        random.shuffle(rows)
        for row in rows:
            cat_id = row.get('_id')
            cat_name = row.get('catalogName')
            yield Request(
                url=self.attr_url.format(cat_id),
                meta={
                    'reqType': 'attr',
                    'batchNo': self.batch_no,
                    'catalogId': cat_id,
                    'catalogName': cat_name,
                },
                headers={
                    'Host': 'www.zcygov.cn',
                    'Connection': 'keep-alive',
                    'Accept': '*/*',
                    'X-Requested-With': 'XMLHttpRequest',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400',
                    'Referer': 'https://www.zcygov.cn/item-center-front/publishgoods/selectCategory?current=0&categoryType=0',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-CN,zh;q=0.9',
                },
                # cookies=self.cookie,
                callback=self.parse_attr,
                errback=self.error_back,
                priority=100,
            )

    def parse_attr(self, response):
        meta = response.meta
        cat_id = meta.get('catalogId')
        # 处理品类列表
        cat_attr = parse_attr(response)
        if cat_attr:
            self.logger.info('CatAttr: cat=%s' % cat_id)
            yield cat_attr
        else:
            self.logger.error('NO CatAttr: cat=%s' % cat_id)
