# -*- coding: utf-8 -*-
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.client.mongo_client import Mongo
from zc_core.util.http_util import retry_request
from ctaxccgp.rules import *
from zc_core.spiders.base import BaseSpider


class CatalogBrandSpider(BaseSpider):
    name = 'brand'

    # 常用链接
    back_cat_url = 'https://www.zcygov.cn/api/micro/category/backCategories/categoriesByLayer?timestamp=1571188114&layer=0&isContainFrozen=true'
    cat_brand_url = 'https://www.zcygov.cn/api/micro/spu/findBrandList?timestamp=1604324631&categoryId={}'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(CatalogBrandSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.cookie = {
            'districtCode': '001000',
            'districtName': '%E5%9B%BD%E5%AE%B6%E7%A8%8E%E5%8A%A1%E6%80%BB%E5%B1%80%E6%9C%AC%E7%BA%A7',
            'SESSION': 'NTNlMzRjODAtZWY0Ni00ZWMyLTg1ZTUtMzRhZDExNTFlODg0',
        }

    def start_requests(self):
        rows = Mongo().get_collection('cat_{}'.format(self.batch_no)).find({'level': 3})
        for row in list(rows):
            cat_id = row.get('_id')
            cat_name = row.get('catalogName')
            yield Request(
                url=self.cat_brand_url.format(cat_id),
                meta={
                    'reqType': 'brand',
                    'batchNo': self.batch_no,
                    'catalogId': cat_id,
                    'catalogName': cat_name,
                },
                headers={
                    'Host': 'www.zcygov.cn',
                    'Connection': 'keep-alive',
                    'Accept': '*/*',
                    'X-Requested-With': 'XMLHttpRequest',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400',
                    'Referer': 'https://www.zcygov.cn/item-center-front/publishgoods/selectCategory?current=0&categoryType=0',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-CN,zh;q=0.9',
                },
                cookies=self.cookie,
                callback=self.parse_cat_brand,
                errback=self.error_back,
                priority=100,
            )

    def parse_cat_brand(self, response):
        meta = response.meta
        cat_id = meta.get('catalogId')
        # 处理品类列表
        brands = parse_cat_brand(response)
        if brands:
            self.logger.info('品牌: count[%s]' % len(brands))
            yield Box('brand', self.batch_no, brands)
        else:
            self.logger.error('无品牌: cat=%s' % cat_id)
