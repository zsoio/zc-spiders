import pandas as pd
from zc_core.client.mongo_client import Mongo


def export_attr(collection, columns, header, file):
    db = Mongo()
    rows = db.list(collection)
    pd.DataFrame(rows).to_excel(file, columns=columns, header=header, encoding='utf-8', index=False)


if __name__ == '__main__':
    # batch_no = time_to_batch_no(datetime.now())
    batch_no = '20210816'
    export_attr(
        'spu_{}'.format(batch_no),
        ['_id', 'catalog3Id', 'catalog3Name', 'brandId', 'brandName', 'spec'],
        ['SPU编号', '小类编号', '小类名称', '品牌编号', '品牌名称', 'SPU'],
        'D:\\国税SPU_{}.xlsx'.format(batch_no)
    )
