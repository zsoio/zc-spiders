# -*- coding: utf-8 -*-
import logging
import random
import threading
from zc_core.client.mongo_client import Mongo

logger = logging.getLogger('OrderSkuHelper')


class AreaHelper(object):
    _instance_lock = threading.Lock()
    _biz_inited = False

    def __new__(cls, *args, **kwargs):
        if not hasattr(AreaHelper, "_instance"):
            with AreaHelper._instance_lock:
                if not hasattr(AreaHelper, "_instance"):
                    AreaHelper._instance = object.__new__(cls)
        return AreaHelper._instance

    def __init__(self):
        if not self._biz_inited:
            self._biz_inited = True
            self.area_list = list()
            item_list = Mongo().list('area_pool', query={'level': 3}, fields={'_id': 1})
            for area_id in item_list:
                self.area_list.append(area_id)
            logger.info('映射数量: %s' % len(self.area_list))

    def random_choice(self):
        if self.area_list:
            return random.choice(self.area_list).get('_id')
