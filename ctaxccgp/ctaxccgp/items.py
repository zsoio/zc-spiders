# -*- coding: utf-8 -*-
import scrapy
from scrapy.exceptions import DropItem

from zc_core.model.items import Brand, Spu, BaseData


class BackBrand(Brand):
    # 分类编号[可选]
    catId = scrapy.Field()
    # 分类名称[可选]
    catName = scrapy.Field()


class BackSpu(Spu):
    # 环保
    environ = scrapy.Field()
    # 节能/节水
    energy = scrapy.Field()
    # 分类名称[可选]
    spec = scrapy.Field()
    # 标签
    tags = scrapy.Field()


# 规格参数模型
class BackCatAttr(BaseData):
    _id = scrapy.Field()
    # 商品编号
    catId = scrapy.Field()
    # 商品编号
    catName = scrapy.Field()
    # 属性集合
    attrs = scrapy.Field()

    def validate(self):
        if not self.get('_id') and not self.get('catId'):
            raise DropItem("BackCatAttr Error [catId]")
        return True


if __name__ == '__main__':
    pass
