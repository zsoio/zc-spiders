# -*- coding: utf-8 -*-
import requests
from lxml import etree

if __name__ == '__main__':
    url = 'https://search.jd.com/s_new.php?keyword=%E8%A5%BF%E5%B1%8B&enc=utf-8&qrst=1&rt=1&stop=1&vt=2&bs=1&ev=exbrand_%E8%A5%BF%E5%B1%8B%EF%BC%88Westinghouse%EF%BC%89%5E&page=2&s=27&scrolling=y&log_id=1585459423.72302'
    headers = {
        'authority': 'search.jd.com',
        'method': 'GET',
        'path': '/s_new.php?keyword=%E8%A5%BF%E5%B1%8B&enc=utf-8&qrst=1&rt=1&stop=1&vt=2&bs=1&ev=exbrand_%E8%A5%BF%E5%B1%8B%EF%BC%88Westinghouse%EF%BC%89%5E&page=2&s=27&scrolling=y&log_id=1585459423.72302&tpl=1_M&show_items=47614079279,4551130,2896057,45288193569,100006442734,55566713240,43436664730,33221016951,61625246544,55978058631,63397718277,56110900715,100009696106,55564596567,5955224,6679492,64134711763,100003766007,57988338614,56323066782,65345321753,45878597708,54893407968,10834391943,33335904722,49239348144,100001551931,100000611919,50130684894,45081073790',
        'scheme': 'https',
        'accept': '*/*',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'zh-CN,zh;q=0.9',
        'referer': 'https://search.jd.com/Search?keyword=%E8%A5%BF%E5%B1%8B&enc=utf-8',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400',
        'x-requested-with': 'XMLHttpRequest',
    }

    rs = requests.get(
        url=url,
        headers=headers,
    )

    # print(rs.encoding)
    # print(rs.text)

    with open('search.html', 'wb') as f:
        f.write(rs.content)

    # # 打印出所请求页面返回的编码方式
    # print(rs.encoding)
    # # response.apparent_encoding是通过内容分析出的编码方式，这里是urf-8
    # print(rs.apparent_encoding)
    # # 转码
    # content = rs.text.encode(rs.encoding).decode(rs.apparent_encoding)
    # print(content)
