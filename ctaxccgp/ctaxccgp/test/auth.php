<?php
include_once './aliyun-php-sdk-core/Config.php';
use afs\Request\V20180112 as Afs;

//YOUR ACCESS_KEY、YOUR ACCESS_SECRET请替换成您的阿里云accesskey id和secret
$iClientProfile = DefaultProfile::getProfile("cn-hangzhou", "YOUR ACCESSKEY", "YOUR ACCESS_SECRET");
$client = new DefaultAcsClient($iClientProfile);
$iClientProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", "afs", "afs.aliyuncs.com");

$request = new Afs\AnalyzeNvcRequest();
$request->setData("1");// 必填参数，从前端获取getNVCVal函数的值
$request->setScoreJsonStr("{"200":"PASS","400":"NC","600":"SC","700":"LC","800":"BLOCK"}");// 根据自己需求设置，跟前端一致

$response = $client->getAcsResponse($request);
print_r($response);