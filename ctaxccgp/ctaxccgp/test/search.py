import requests
from urllib.error import HTTPError


def get_content(url):
    try:
        response = requests.get(url)
        response.raise_for_status()  # 如果状态码不是200， 引发HttpError异常
        # 从内容分析出响应内容的编码格式
        response.encoding = response.apparent_encoding
    except HTTPError as e:
        print(e)
    else:
        print(response.status_code)
        # print(response.headers)
        # return  response.text   # 返回的是字符串
        return response.content  # 返回的是bytes类型， 不进行解码


if __name__ == '__main__':
    url = 'https://item.jd.com/6789689.html'
    content = get_content(url)
    with open('item.html', 'wb') as f:
        f.write(content)
