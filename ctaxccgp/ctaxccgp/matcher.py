# -*- coding: utf-8 -*-
import re


# 从链接中解析品类编码
def match_cat_id(link):
    # http://ctaxccgp.zcygov.cn/eevees/search?fcid=237
    if link:
        arr = re.findall(r'fcid=(\d+)&*', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析订单编码
def match_order_id(link):
    # /more_notices.html?mall_show=true&no=f43035134c
    if link:
        arr = re.findall(r'&no=(.+)', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析商品编码
def match_sku_id_from_order(link):
    # /more_notices/product_show.html?back=%2Fmore_notices.html%3Fmall_show%3Dtrue%26no%3Df43035134c&item_id=455784&product_id=55709
    if link:
        arr = re.findall(r'product_id=(.+)', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从链接中解析商品编码
def match_sku_id_from_sku(link):
    # /commodities/220403?p_id=6641&target=_blank
    if link:
        arr = re.findall(r'/commodities/(\d+)\?p_id=(.+)&*', link.strip())
        if len(arr):
            return arr[0]

    return ''


# 从链接中解析品牌编码
def match_brand_id(link):
    # /channel/0_7_0_0_0_0_0_0_0_0
    if link:
        arr = re.findall(r'/channel/\d+_(\d+)_', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从链接中解析供应商编码
def match_supplier_id(link):
    # /eevees/shop?shopId=119211&searchType=1
    if link:
        arr = re.findall(r'shopId=(\d+)', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 清理文本中的括号内容
def clean_text(txt):
    # CANON/佳能(8116)
    if txt:
        arr = re.findall(r'^(.+)\(.*\)$', txt.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从链接中解析供应商编码
def match_supplier_id_from_js(js):
    # getshopUrl(201,837);
    if js:
        arr = re.findall(r'\((\d+),.+', js.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# # 测试
if __name__ == '__main__':
    # print(match_cat_id('goFirstType(2,1)'))
    # print(match_cat_id_from_nav('/frontBrands/getBrandsAndProductInfos.action?productTypeId=8&level=1&orderBy=normal'))
    # print(match_brand_id('/channel/0_7_0_0_0_0_0_0_0_0'))
    print(clean_text('CANON/佳能(xx)(8116)'))
    print(clean_text('东芝/TOSHIBA(894)'))
    print(clean_text('Lenovo/联想(4746)'))
    print(clean_text('东芝/TOSHIBA(894)'))
    # print(match_supplier_id('/channel/0_0_0_0_0_0_0_11111_0_0'))
    # tp = match_sku_id_from_sku('/commodities/220403?p_id=6641&target=_blank')
    # print(tp[0])
    # print(tp[1])