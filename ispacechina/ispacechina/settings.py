# -*- coding: utf-8 -*-
BOT_NAME = 'ispacechina'

SPIDER_MODULES = ['ispacechina.spiders']
NEWSPIDER_MODULE = 'ispacechina.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 16
# DOWNLOAD_DELAY = 1
CONCURRENT_REQUESTS_PER_DOMAIN = 16
CONCURRENT_REQUESTS_PER_IP = 16

COOKIES_ENABLED = False
DEFAULT_REQUEST_HEADERS = {
    'Connection': 'keep-alive',
    'Origin': 'https://mall.ispacechina.com',
    'X-Requested-With': 'XMLHttpRequest',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'zh-CN,zh;q=0.9',
}

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'ispacechina.validator.BizValidator': 500
}
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 代理积分阈值
# PROXY_SCORE_LIMIT = 5
# 下载超时
DOWNLOAD_TIMEOUT = 45

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'zc_core.pipelines.brand.BrandCompletePipeline': 400,
    'zc_core.pipelines.supplier.SupplierCompletePipeline': 410,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'ispacechina_2019'

# 目标一级分类限制
ROOT_CAT_LIMIT = ['110', '111', '112', '113', '114', '115', '116']
# 总页数超限的分类指定价格条件阶梯
SPECIAL_LADDERS = {
    # 其他服饰
    '11400050012': [0, 100, 200, 300, 0],
    # 其他家装及耗材
    '11300020030': [0, 10, 15, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 200, 300, 500, 1000, 3000, 10000, 0],
    # 其他办公文具
    '11000030006': [0, 5, 10, 15, 20, 25, 30, 50, 100, 0],
    # 墨粉
    '11000020007': [0, 100, 200, 300, 400, 500, 1000, 0],
    # 笔类
    '11000030001': [0, 10, 20, 50, 100, 0],
    # 硒鼓
    '11000020002': [0, 100, 200, 300, 500, 1000, 0],
    # 服务器/工作站
    '11100020007': [0, 10000, 20000, 30000, 0],
}
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 2
# 日志
LOG_LEVEL = 'INFO'
# 已采商品强制覆盖重采
# FORCE_RECOVER = True