import re


# 从链接中解析品类编码
def match_cat_id(link):
    # http://www.ispacechina.cn/newmall/newGoodDisplay.do?method=searchGoods&macaId=M01
    if link:
        arr = re.findall(r'macaId=(\w+)&*', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从js中解析品牌编码
def match_brand_id(link):
    # goodBrandRefresh('4412');
    if link:
        arr = re.findall(r'goodBrandRefresh\(\'(\w+)\'\);', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从js中解析供应商编码
def match_supplier_id(link):
    # goodSupplierRefresh('5990991');
    if link:
        arr = re.findall(r'goodSupplierRefresh\(\'(\w+)\'\);', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 解析商品编码
def match_sku_id(link):
    # /newmall/newGoodDisplay.do?method=view&id=2253322
    if link:
        arr = re.findall(r'id=(\w+)&*', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析供应商编码
def match_total_page(link):
    # goodPage('2184')
    if link:
        arr = re.findall(r'goodPage\(\'(\w+)\'\)', link.strip())
        if len(arr):
            return arr[0].strip()
    return '0'


# 从链接中解析供应商编码
def fill_img_path(sp_id, url):
    if url:
        if url.startswith('http://') or url.startswith('https://'):
            return url
        elif sp_id == '1':
            # 京东
            url = 'http://img13.360buyimg.com/n2/' + url

    return url

# # 测试
if __name__ == '__main__':
    pass
