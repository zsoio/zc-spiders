# encoding=utf-8
"""
响应对象业务校验
"""
import json
from scrapy.exceptions import IgnoreRequest

from zc_core.middlewares.validate import BaseValidateMiddleware


class BizValidator(BaseValidateMiddleware):

    def validate_sku(self, request, response, spider):
        if '"respCode":"20003"' in response.text or '20003' == json.loads(response.text).get('respCode', None):
            page = request.meta.get('pageNo')
            cat_id = request.meta.get('catalogId')
            raise IgnoreRequest('[Sku]列表为空：cat[%s], page[%s]' % (cat_id, page))

        return response
