# -*- coding: utf-8 -*-
from datetime import datetime
from scrapy import Request
from zc_core.util.http_util import retry_request
from zc_core.spiders.base import BaseSpider
from ispacechina.rules import *
from scrapy.exceptions import IgnoreRequest
from zc_core.util.batch_gen import time_to_batch_no


class CatalogSpider(BaseSpider):
    name = 'catalog'
    # 常用链接
    root_cat_url = 'https://mall.ispacechina.com/rest/service/routing/nouser/qryBusiCommodityCatalogService?upperCatalogId=0&channelId=2&pageNo=1&pageSize=1000'
    sub_cat_url = 'https://mall.ispacechina.com/rest/service/routing/nouser/qryOthLevelCommodityCatalogService?upperCatalogId={}&channelId=2&pageNo=1&pageSize=1000'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(CatalogSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        yield Request(
            url=self.root_cat_url,
            meta={
                'reqType': 'catalog',
                'batchNo': self.batch_no,
            },
            callback=self.parse_root_cat,
            errback=self.error_back,
            priority=100,
        )

    def parse_root_cat(self, response):
        cats = parse_root_cat(response)
        if cats:
            self.logger.info('主分类: count[%s]' % len(cats))
            yield Box('catalog', self.batch_no, cats)

            for cat in cats:
                yield Request(
                    url=self.sub_cat_url.format(cat.get('catalogId')),
                    meta={
                        'reqType': 'catalog',
                        'batchNo': self.batch_no,
                    },
                    callback=self.parse_sub_cat,
                    errback=self.error_back,
                    priority=100,
                )

    def parse_sub_cat(self, response):
        # 处理品类列表
        cats = parse_sub_cat(response)
        if cats:
            self.logger.info('子分类: count[%s]' % len(cats))
            yield Box('catalog', self.batch_no, cats)
