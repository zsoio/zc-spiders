# -*- coding: utf-8 -*-
BOT_NAME = 'chongqing'

SPIDER_MODULES = ['chongqing.spiders']
NEWSPIDER_MODULE = 'chongqing.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 6
# DOWNLOAD_DELAY = 0.1
CONCURRENT_REQUESTS_PER_DOMAIN = 6
CONCURRENT_REQUESTS_PER_IP = 6
# COOKIES_ENABLED = False

DEFAULT_REQUEST_HEADERS = {
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Referer': 'https://www.zcygov.cn/',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'zh-CN,zh;q=0.8',
}

DOWNLOADER_MIDDLEWARES = {
    'chongqing.middlewares.ZcySupplierMiddleware': 730,
    'chongqing.middlewares.ZcySkuMiddleware': 740,
    'chongqing.middlewares.ZcyItemMiddleware': 750,
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'chongqing.validator.BizValidator': 500
}
# 指定代理池类
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.mogu_pool.MoguProxyPool'
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 下载超时
DOWNLOAD_TIMEOUT = 30
# 响应重试状态码
CUSTOM_RETRY_CODES = [405]

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'zc_core.pipelines.catalog.CatalogCompletePipeline': 400,
    'chongqing.piplines.OrderItemWorkPipeline': 500,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'chongqing_2019'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 5
# 订单采集截止天数
ORDER_DEADLINE_DAYS = 120
# 允许响应内容为空（兼容订单）
ALLOW_EMPTY_RESPONSE = True
# 已采商品强制覆盖重采
# FORCE_RECOVER = True

# 价格区间阶梯
PRICE_LADDER = [0, 10, 20, 30, 40, 50, 60, 80, 100, 200, 500, 1000, 10000, 10000000]
# 目标供应商编号
SUPPLIERS = {
    '157066': '重庆晨光文具销售有限公司',
    '156573': '欧菲斯办公伙伴重庆有限公司',
    '158293': '成都京东世纪贸易有限公司',
    '165365': '重庆市得力商贸有限公司',
    '106031': '咸亨国际科技股份有限公司',
    '119473': '领先未来科技集团有限公司',
    '2208860': '史泰博（上海）有限公司重庆分公司',
    '2112416': '上海晨光科力普办公用品有限公司重庆分公司',
    '2104996': '重庆企事通电子商务有限公司',
    '157056': '重庆齐彩办公用品有限公司',
    '2213197': '邦威防护科技股份有限公司重庆分公司',
    '2111643': '深圳市壹办公科技股份有限公司重庆分公司',
}
