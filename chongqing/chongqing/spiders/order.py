# -*- coding: utf-8 -*-
import random
import time
import scrapy
from datetime import datetime
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.dao.item_data_dao import ItemDataDao
from zc_core.util.http_util import retry_request
from zc_core.model.items import OrderItemLog
from zc_core.util.done_filter import DoneFilter
from zc_core.util.batch_gen import time_to_batch_no
from chongqing.rules import parse_order_item
from zc_core.spiders.base import BaseSpider


class OrderSpider(BaseSpider):
    name = 'order'
    custom_settings = {
        'CONCURRENT_REQUESTS': 6,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 6,
        'CONCURRENT_REQUESTS_PER_IP': 6,
    }
    # 常用链接
    order_url = 'https://www.zcygov.cn/front/api/detail/item/dealRecord?itemId={}&pageNo={}&pageSize={}&timestamp={}'

    def __init__(self, batchNo=None, delta=-24, *args, **kwargs):
        super(OrderSpider, self).__init__(batchNo=batchNo, delta=delta, *args, **kwargs)
        self.done_filter = DoneFilter(coll_name='order_item_log_{}'.format(self.batch_no))

    def start_requests(self):
        sku_list = ItemDataDao().get_batch_data_list(self.batch_no,
                                                     fields={'_id': 1, 'supplierId': 1, 'supplierName': 1})
        self.logger.info('批次商品：%s' % len(sku_list))
        random.shuffle(sku_list)
        for sku in sku_list:
            sku_id = sku.get('_id')
            supplier_id = sku.get('supplierId')
            supplier_name = sku.get('supplierName')
            page = 1
            page_size = 10
            if self.done_filter.contains('{}_{}'.format(sku_id, page)):
                self.logger.info('已采：sku=%s, page=%s' % (sku_id, page))
                continue

            yield Request(
                url=self.order_url.format(sku_id, page, page_size, int(round(time.time()))),
                meta={
                    'reqType': 'order',
                    'batchNo': self.batch_no,
                    'skuId': sku_id,
                    'pageNo': page,
                    'pageSize': page_size,
                    'supplierId': supplier_id,
                    'supplierName': supplier_name,
                },
                headers={
                    'Connection': 'keep-alive',
                    'Upgrade-Insecure-Requests': '1',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-CN,zh;q=0.9',
                },
                callback=self.parse_order_item,
                errback=self.error_back,
            )

    # 处理order
    def parse_order_item(self, response):
        meta = response.meta
        sku_id = meta.get('skuId')
        cur_page_no = meta.get('pageNo')
        supplier_id = meta.get('supplierId')
        supplier_name = meta.get('supplierName')
        page_size = meta.get('pageSize')
        order_list = parse_order_item(response)

        log = OrderItemLog()
        log['_id'] = '{}_{}'.format(sku_id, cur_page_no)
        log['skuId'] = sku_id
        log['page'] = cur_page_no
        log['count'] = len(order_list)
        yield log

        if order_list:
            self.logger.info('订单: sku=%s, page=%s, cnt=%s' % (sku_id, cur_page_no, len(order_list)))
            for order in order_list:
                yield order

            # 下一页
            if page_size <= len(order_list):
                next_page_no = cur_page_no + 1
                # 过滤，避免单批次重采
                if not self.done_filter.contains('{}_{}'.format(sku_id, next_page_no)):
                    # 分页采集订单
                    yield Request(
                        url=self.order_url.format(sku_id, next_page_no, page_size, int(round(time.time()))),
                        meta={
                            'reqType': 'order',
                            'batchNo': self.batch_no,
                            'skuId': sku_id,
                            'pageNo': next_page_no,
                            'pageSize': page_size,
                            'supplierId': supplier_id,
                            'supplierName': supplier_name,
                        },
                        headers={
                            'Connection': 'keep-alive',
                            'Accept': 'application/json, text/plain, */*',
                            'X-Requested-With': 'XMLHttpRequest',
                            'Accept-Encoding': 'gzip, deflate, br',
                            'Accept-Language': 'zh-CN,zh;q=0.9',
                        },
                        callback=self.parse_order_item,
                        errback=self.error_back,
                    )
                else:
                    self.logger.info('已采：sku=%s, page=%s' % (sku_id, next_page_no))
        else:
            self.logger.info('无单: sku=%s, page=%s' % (sku_id, cur_page_no))
