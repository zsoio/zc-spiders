# -*- coding: utf-8 -*-
from zc_core.util.batch_gen import time_to_batch_no
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.util.http_util import retry_request
from chongqing.rules import *
from zc_core.spiders.base import BaseSpider


class SupplierSpider(BaseSpider):
    name = 'supplier'
    # 常用链接
    suppliers_url = 'https://www.zcygov.cn/pages/suppliers?pageNo={}&pageSize=10'
    supplier_sku_url = 'https://www.zcygov.cn/eevees/shop?shopId={}&searchType=1'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SupplierSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        if not batchNo:
            self.batch_no = time_to_batch_no(datetime.now())
        else:
            self.batch_no = int(batchNo)

    def start_requests(self):
        page = 1
        yield Request(
            url=self.suppliers_url.format(page),
            meta={
                'reqType': 'supplier',
                'batchNo': self.batch_no,
                'page': page,
            },
            callback=self.parse_suppliers_page,
            errback=self.error_back,
            priority=10,
            dont_filter=True
        )

    def parse_suppliers_page(self, response):
        total = parse_suppliers_page(response)
        self.logger.info('供应商总页数: total[%s]' % total)

        if total:
            # 供应商
            for page in range(1, total + 1):
                yield Request(
                    url=self.suppliers_url.format(page),
                    meta={
                        'reqType': 'supplier',
                        'batchNo': self.batch_no,
                        'page': page,
                    },
                    callback=self.parse_suppliers,
                    errback=self.error_back,
                    priority=20,
                    dont_filter=True
                )

    def parse_suppliers(self, response):
        meta = response.meta
        page = meta.get('page')
        # 处理供应商列表
        suppliers = parse_supplier(response)
        if suppliers:
            self.logger.info('供应商列表: page=%s, count=%s' % (page, len(suppliers)))
            yield Box('supplier', self.batch_no, suppliers)
        else:
            self.logger.info('无供应商: page=%s' % page)
