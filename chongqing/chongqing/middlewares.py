# encoding=utf-8
import random
import uuid
from zc_core.middlewares.agents.user_agents import agents
from zc_core.util.encrypt_util import md5
from chongqing.utils.area_helper import AreaHelper


class ZcySupplierMiddleware(object):
    """
    商品列表页/供应商列表页
    专用反爬中间件
    """
    def __init__(self):
        self.referer = 'https://www.zcygov.cn/pages/suppliers?utm=a0017.2ef5001f.cl22.1.{}&pageNo={}&pageSize=10'
        self.area_helper = AreaHelper()
        # proxy -> log_client_uuid
        self.log_client_map = dict()
        # proxy -> area_id
        self.area_map = dict()

    def process_request(self, request, spider):
        meta = request.meta
        proxy = meta.get('proxy', 'NO_PROXY')
        page = meta.get('page', 1)
        req_type = meta.get('reqType', None)
        if req_type in ['supplier']:
            request.headers['User-Agent'] = random.choice(agents)
            log_client = self.log_client_map.get(proxy)
            if not log_client:
                log_client = str(uuid.uuid4())
                self.log_client_map[proxy] = log_client
            area_id = self.area_map.get(proxy)
            if not area_id:
                area_id = self.area_helper.random_choice()
                self.area_map[proxy] = area_id
            request.headers['Cookie'] = '_zcy_log_client_uuid={}; aid={}; districtCode=509900'.format(log_client, area_id)
            request.headers['Referer'] = random.choice(self.referer).format(md5(str(uuid.uuid4())), page)

    def process_response(self, request, response, spider):
        meta = request.meta
        proxy = meta.get('proxy', 'NO_PROXY')
        page = meta.get('page', 1)
        req_type = meta.get('reqType', None)
        if req_type in ['supplier'] and response.status == 405:
            # 只观察，不处理
            spider.logger.info('触发反爬: proxy=%s, page=%s' % (proxy, page))
            self.log_client_map.pop(proxy, None)

        return response

    def process_exception(self, request, exception, spider):
        meta = request.meta
        proxy = meta.get('proxy', 'NO_PROXY')
        page = meta.get('page', 1)
        req_type = meta.get('reqType', None)
        if req_type in ['sku', 'supplier']:
            # 只观察，不处理
            spider.logger.info('反爬异常: proxy=%s, page=%s, ex=%s' % (proxy, page, str(type(exception))))


class ZcySkuMiddleware(object):
    """
    商品列表页/供应商列表页
    专用反爬中间件
    """
    def __init__(self):
        self.referer_tpl = [
            'https://www.zcygov.cn/',
            'https://www.zcygov.cn/eevees/shop?shopId={}&searchType=1&utm=a0017.2ef5001f.cl47.2.{}',
        ]
        self.area_helper = AreaHelper()
        # proxy -> log_client_uuid
        self.log_client_map = dict()
        # proxy -> area_id
        self.area_map = dict()

    def process_request(self, request, spider):
        meta = request.meta
        proxy = meta.get('proxy', 'NO_PROXY')
        req_type = meta.get('reqType', None)
        if req_type in ['sku', 'supplier']:
            request.headers['User-Agent'] = random.choice(agents)
            log_client = self.log_client_map.get(proxy)
            if not log_client:
                log_client = str(uuid.uuid4())
                self.log_client_map[proxy] = log_client
            area_id = self.area_map.get(proxy)
            if not area_id:
                area_id = self.area_helper.random_choice()
                self.area_map[proxy] = area_id
            request.headers['Cookie'] = '_zcy_log_client_uuid={}; aid={}; districtCode=509900'.format(log_client, area_id)
        if req_type in ['sku']:
            sp_id = meta.get('supplierId')
            request.headers['Referer'] = random.choice(self.referer_tpl).format(sp_id, md5(str(uuid.uuid4())))

    def process_response(self, request, response, spider):
        meta = request.meta
        proxy = meta.get('proxy', 'NO_PROXY')
        req_type = meta.get('reqType', None)
        if req_type in ['sku'] and response.status == 405:
            # 只观察，不处理
            spider.logger.info('触发反爬: proxy=%s' % proxy)
            self.log_client_map.pop(proxy, None)

        return response

    def process_exception(self, request, exception, spider):
        meta = request.meta
        proxy = meta.get('proxy', 'NO_PROXY')
        req_type = meta.get('reqType', None)
        if req_type in ['sku', 'supplier']:
            # 只观察，不处理
            spider.logger.info('反爬异常: proxy=%s, ex=%s' % (proxy, str(type(exception))))


class ZcyItemMiddleware(object):
    # 持续触发反爬而终止采集的次数阈值
    max_bind_quit = 45
    # 持续触发反爬次数记录
    bind_request_count = 0
    """
    商品详情页
    专用反爬中间件
    """
    def __init__(self):
        self.referer_tpl = [
            'https://www.zcygov.cn/items/{}?searchType=1',
            'https://www.zcygov.cn/items/{}?utm=2d017.2ef5111f.cl47.32.{}',
            'https://www.zcygov.cn/items/{}?searchType=1&utm=a0004.eevees-search.c0002.d0003.{}',
        ]
        self.area_helper = AreaHelper()
        # proxy -> log_client_uuid
        self.log_client_map = dict()
        # proxy -> area_id
        self.area_map = dict()

    def process_request(self, request, spider):
        meta = request.meta
        proxy = meta.get('proxy', 'NO_PROXY')
        sku_id = meta.get('skuId', '')
        req_type = meta.get('reqType', None)
        if sku_id and 'item' == req_type:
            request.headers['User-Agent'] = random.choice(agents)
            request.headers['Referer'] = random.choice(self.referer_tpl).format(sku_id, md5(str(uuid.uuid4())))
            log_client = self.log_client_map.get(proxy)
            if not log_client:
                log_client = str(uuid.uuid4())
                self.log_client_map[proxy] = log_client
            area_id = self.area_map.get(proxy)
            if not area_id:
                area_id = self.area_helper.random_choice()
                self.area_map[proxy] = area_id
            request.headers['Cookie'] = '_zcy_log_client_uuid={}; aid={}; districtCode=509900; districtName=%E9%87%8D%E5%BA%86%E5%B8%82%E6%9C%AC%E7%BA%A7; aid=500103'.format(log_client, area_id)
            # request.headers['Cookie'] = '_zcy_log_client_uuid=2b572860-964a-11ea-b158-3d593d1d198f; aid=110101; _dg_id.bbc15f7dfd2de351.63bf=e555b5cdd7cafa5b%7C%7C%7C1593773800%7C%7C%7C0%7C%7C%7C1593827653%7C%7C%7C1593827653%7C%7C%7C%7C%7C%7Cf419fb40d4dec007%7C%7C%7C%7C%7C%7C%7C%7C%7C0%7C%7C%7Cundefined; UM_distinctid=173295fa910b8-07741385b81539-335f487b-384000-173295fa911336; districtCode=001000; districtName=%E5%9B%BD%E5%AE%B6%E7%A8%8E%E5%8A%A1%E6%80%BB%E5%B1%80%E6%9C%AC%E7%BA%A7; acw_tc=76b20ff815988680562467027e7f69824bc54dc05e198f701edbd5c7bb10ac'

    def process_response(self, request, response, spider):
        meta = request.meta
        proxy = meta.get('proxy', 'NO_PROXY')
        sku_id = meta.get('skuId', '')
        req_type = meta.get('reqType', None)
        if sku_id and 'item' == req_type and response.status == 405:
            self.bind_request_count = self.bind_request_count + 1
            # 只观察，不处理
            spider.logger.info('触发反爬: proxy=%s, sku=%s' % (proxy, sku_id))
            self.log_client_map.pop(proxy, None)
        else:
            self.bind_request_count = 0
        if self.bind_request_count > self.max_bind_quit:
            # 终止采集
            spider.crawler.engine.close_spider(spider, '连续触发反爬次数超过阈值终止采集')

        return response

    def process_exception(self, request, exception, spider):
        meta = request.meta
        proxy = meta.get('proxy', 'NO_PROXY')
        sku_id = meta.get('skuId', '')
        req_type = meta.get('reqType', None)
        if sku_id and 'item' == req_type:
            # 只观察，不处理
            spider.logger.info('反爬异常: proxy=%s, sku=%s, ex=%s' % (proxy, sku_id, str(type(exception))))
