# encoding=utf-8
"""
agent中间件
"""
import logging
from suzhou.utils.cookie_builder import build_cookie


class CookieMiddleware(object):

    def process_request(self, request, spider):
        request.headers['Cookie'] = f'area_id=320505; _suzhou_session={build_cookie()}',
