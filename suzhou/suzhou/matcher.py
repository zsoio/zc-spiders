import re


# 解析页码
def match_last_page(link):
    # /channel/0_0_0_0_0_0_0_0_0_0?page=1256&per_page=100
    if link:
        arr = re.findall(r'page=(\d+)&', link.strip())
        if len(arr):
            return int(arr[0].strip())
    return 0


# 解析价格
def match_sale_price(link):
    # $("#price_tag").html("¥7,000.00");
    if link:
        arr = re.findall(r'\$\("#price_tag"\)\.html\("(.+?)"\);', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析品类编码
def match_cat_id(link):
    # /channel/176
    if link:
        arr = re.findall(r'/channel/(\d+)', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析订单编码
def match_order_id(link):
    # /more_notices.html?mall_show=true&no=f43035134c
    if link:
        arr = re.findall(r'&no=(.+)', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析商品编码
def match_sku_id_from_order(link):
    # /more_notices/product_show.html?back=%2Fmore_notices.html%3Fmall_show%3Dtrue%26no%3Df43035134c&item_id=455784&product_id=55709
    if link:
        arr = re.findall(r'product_id=(.+)', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从链接中解析商品编码
def match_sku_id_from_sku(link):
    # /commodities/220403?p_id=6641&target=_blank
    # /commodities/524646?p_id=489933
    if link:
        arr = re.findall(r'/commodities/(\d+)\?p_id=(.+)&*', link.strip())
        if len(arr):
            return arr[0]

    return ''


# 从链接中解析品牌编码
def match_brand_id(link):
    # /channel/0_7_0_0_0_0_0_0_0_0
    # /search.html?combo=0_143_0_0_0_0_0&k=&rp=&search%5Bis_in_stock%5D=true
    if link:
        arr = re.findall(r'/search.html\?combo=\d+_(\d+)_', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从链接中解析供应商编码
def match_supplier_id(link):
    # /emall/20309
    if link:
        arr = re.findall(r'/emall/(\w+)', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 清理文本中的括号内容
def clean_text(txt):
    # 得力（Deli）(2537)
    if txt:
        arr = re.findall(r'^(.+)\(.*?\)$', txt.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从链接中解析供应商编码
def match_supplier_id_from_js(js):
    # getshopUrl(201,837);
    if js:
        arr = re.findall(r'\((\d+),.+', js.strip())
        if len(arr):
            return arr[0].strip()
    return ''


PATTERN_LIST = [
    # 得力
    r'\.nbdeli\..+/ItemDetail_(\d+)_\w\w\.htm.*',
    # 京东
    r'\.jd\..+/(\d+)\.htm.*',
    # 晨光
    r'\.colipu\..+/itemcode-(\d+)\.htm.*',
    # 齐心
    r'\.comix\..+/(\d+)\.htm.*',
    # 史泰博
    r'\.staples\..+/product/(\w+)',
    # 办公伙伴
    r'\.officemate\..+/product-(\d+)\.htm.*',
    # 领先未来
    r'\.66123123\..+\??id=(\d+)',
    # 物产电商
    r'\.zjmi-mall\..+\??goods_code=(\d+)',
    # 广博集团
    r'\.guangbo\..+\?.*&?id=(\d+)',
]


# 从链接中解析供应商商品编码
def match_supplier_sku_id(link):
    for ptn in PATTERN_LIST:
        # p1 = r'item\.jd\..+/(\d+)\.htm.*'
        pattern = re.compile(ptn)
        if link:
            arr = pattern.findall(link.strip())
            if len(arr):
                return arr[0].strip()

    return ''


# # 测试
if __name__ == '__main__':
    # print(match_cat_id('goFirstType(2,1)'))
    # print(match_cat_id_from_nav('/frontBrands/getBrandsAndProductInfos.action?productTypeId=8&level=1&orderBy=normal'))
    # print(match_brand_id('/channel/0_7_0_0_0_0_0_0_0_0'))
    print(match_brand_id('/search.html?combo=0_143_0_0_0_0_0&k=&rp=&search%5Bis_in_stock%5D=true'))
    # print(clean_text('CANON/佳能(xx)(8116)'))
    # print(clean_text('东芝/TOSHIBA(894)'))
    # print(clean_text('Lenovo/联想(4746)'))
    # print(clean_text('东芝/TOSHIBA(894)'))
    # print(clean_text('得力（Deli）(2537)'))
    # print(clean_text('得力(Deli)(2537)'))
    # print(match_supplier_id('/channel/0_0_0_0_0_0_0_11111_0_0'))
    # tp = match_sku_id_from_sku('/commodities/220403?p_id=6641&target=_blank')
    # print(tp[0])
    # print(tp[1])
    # print(match_supplier_sku_id('https://item.jd.com/4860140.html'))
    # print(match_supplier_sku_id('https://www.66123123.com/Goods/GoodsDetail?id=142771'))
    # print(match_supplier_sku_id('http://www.officemate.cn/index.php/product-74274.html'))
    # print(match_supplier_sku_id('http://www.colipu.com/itemcode-3012988.html'))
    # print(match_supplier_sku_id('http://www.comix.com.cn/20002250.html'))
    # print(match_supplier_sku_id('http://www.staples.cn/product/1100170470EA'))
    # print(match_supplier_sku_id('http://b2b.nbdeli.com/Goods/ItemDetail_100057443_EC.htm'))
    # print(match_supplier_sku_id('https://www.zjmi-mall.com/gd/detail.do?goods_code=800539'))
    # print(match_supplier_sku_id('http://b2b.guangbo.net/?m=product&s=detail&id=358688'))

    pass
