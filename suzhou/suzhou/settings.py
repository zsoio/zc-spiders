# -*- coding: utf-8 -*-
BOT_NAME = 'suzhou'

SPIDER_MODULES = ['suzhou.spiders']
NEWSPIDER_MODULE = 'suzhou.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 8
# DOWNLOAD_DELAY = 0.2
CONCURRENT_REQUESTS_PER_DOMAIN = 8
CONCURRENT_REQUESTS_PER_IP = 8

COOKIES_ENABLED = False
DOWNLOADER_MIDDLEWARES = {
    'suzhou.middlewares.CookieMiddleware': 700,
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'suzhou.validator.BizValidator': 500
}

# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 1
# 自定义重试状态码
CUSTOM_RETRY_CODES = [403]
# 资质存在空响应
ALLOW_EMPTY_RESPONSE = True

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.wandou_pool.WandouProxyPool'
ITEM_PIPELINES = {
    'zc_core.pipelines.catalog.CatalogCompletePipeline': 420,
    'zc_core.pipelines.supplier.SupplierCompletePipeline': 420,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'suzhou_2019'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 3
# [订单] 最大采集页数
MAX_ORDER_PAGE = 500
# 输出
OUTPUT_ROOT = '/work/suzhou/'
# 已采商品强制覆盖重采
# FORCE_RECOVER = True
