# -*- coding: utf-8 -*-
from zc_core.client.mongo_client import Mongo


class Cat3Map(object):
    """
    已采集商品过滤器
    用于全量商品明细重复采集时，过滤已采过的商品，避免重复采集
    """

    def __init__(self, batch_no=None, coll_name=None, query={}, fields={'_id': 1, 'spuId': 1, 'catalog3Id': 1}, filter_key='spuId'):
        self.cat_map = dict()
        if batch_no:
            coll_name = 'sku_{}'.format(batch_no)
        elif not coll_name:
            coll_name = 'sku_pool'
        items = Mongo().list(coll_name, query=query, fields=fields)
        for it in items:
            self.cat_map[it.get('spuId')] = it.get('catalog3Id')

    def contains(self, spu_id):
        return spu_id in self.cat_map

    def put(self, spu_id, cat3_id):
        self.cat_map[spu_id] = cat3_id

    def get(self, spu_id):
        return self.cat_map.get(spu_id)


# 测试
if __name__ == '__main__':
    mapper = Cat3Map()
    print(mapper.contains('526687'))
