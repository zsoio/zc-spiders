# -*- coding=utf-8-*-
import base64

from Crypto.Cipher import AES
from binascii import b2a_hex, a2b_hex
from Crypto.Hash import MD5

"""
aes加密算法
ECB模式
"""


class AesCrypter(object):
    """
    // JS原始加密方法
    function g(e) {
        var n = t.default.enc.Utf8.parse(e)
          , i = t.default.MD5(s)
          , o = t.default.AES.encrypt(n, i, {
            mode: t.default.mode.ECB,
            padding: t.default.pad.Pkcs7
        });
        return o.toString()
    }
    // JS原始明文生成方法
    // 2aea1ch880946c94feb7a5aaeb3c5ab41628695463
    function p() {
        var e, n, i, o, t = ((new Date).getTime() + "").slice(0, 10);
        return e = (0,
        a.default)(t),
        n = e.split(""),
        n.splice(2, 1, "e"),
        n.splice(6, 1, "h"),
        n.splice(12, 1, 6),
        n.splice(25, 1, "b"),
        i = n.join("") + t,
        o = g(i),
        o
    }
    """

    def pkcs7padding(self, data):
        # AES.block_size 16位
        bs = AES.block_size
        padding = bs - len(data) % bs
        padding_text = chr(padding) * padding
        return data + padding_text

    def encrypt(self, data):
        """
        AES加密
        :param data: 普通字符串
        :return:
        """
        data = self.pkcs7padding(data)
        key = MD5.new('GvcaHhBsKa9kkHmf'.encode('utf-8')).digest()
        cipher = AES.new(key, AES.MODE_ECB)
        encrypted = cipher.encrypt(data.encode('utf-8'))
        return base64.b64encode(encrypted)


if __name__ == '__main__':
    rs = AesCrypter().encrypt('2aea1ch880946c94feb7a5aaeb3c5ab41628695463')
    print(rs)
