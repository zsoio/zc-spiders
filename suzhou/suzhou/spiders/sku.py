# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.model.items import Box
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from suzhou.rules import *
from zc_core.spiders.base import BaseSpider


class SkuSpider(BaseSpider):
    name = 'sku'
    custom_settings = {
        'CONCURRENT_REQUESTS': 6,
        # 'DOWNLOAD_DELAY': 0.1,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 6,
        'CONCURRENT_REQUESTS_PER_IP': 6,
    }
    # 常用链接
    index_url = 'http://www.zfcgwssc.suzhou.gov.cn/search.html?utf8=%E2%9C%93&k='
    sku_list_url = 'http://www.zfcgwssc.suzhou.gov.cn/channel/{}_0_0_0_0_0_0_0_0?page={}&utf8=%E2%9C%93&search%5Bis_in_stock%5D=false'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        # 品类、品牌
        yield Request(
            url=self.index_url,
            meta={
                'batchNo': self.batch_no,
            },
            headers={
                'Host': 'www.zfcgwssc.suzhou.gov.cn',
                'Connection': 'keep-alive',
                'Cache-Control': 'max-age=0',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3861.400 QQBrowser/10.7.4313.400',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'zh-CN,zh;q=0.9',
                # 'Cookie': f'area_id=320505; _suzhou_session={build_cookie()}',
            },
            callback=self.parse_catalog_brand,
            errback=self.error_back,
            dont_filter=True
        )

    def parse_catalog_brand(self, response):
        meta = response.meta
        batch_no = meta.get('batchNo')

        # 处理品牌列表
        brands = parse_brand(response)
        if brands:
            self.logger.info('品牌: count[%s]' % len(brands))
            yield Box('brand', self.batch_no, brands)

        # 处理供应商列表
        suppliers = parse_supplier(response)
        if suppliers:
            self.logger.info('供应商: count[%s]' % len(suppliers))
            yield Box('supplier', self.batch_no, suppliers)

        # 处理品类列表
        cats = parse_catalog(response)
        if cats:
            self.logger.info('品类: count[%s]' % len(cats))
            yield Box('catalog', self.batch_no, cats)

            for cat in cats:
                if cat.get('level') == 3:
                    cat3_id = cat.get('catalogId')
                    yield Request(
                        url=self.sku_list_url.format(cat3_id, 1),
                        callback=self.parse_sku_page,
                        errback=self.error_back,
                        meta={
                            'reqType': 'sku',
                            'batchNo': batch_no,
                            'catalog3Id': cat3_id,
                            'page': 1,
                        },
                        headers={
                            'Host': 'www.zfcgwssc.suzhou.gov.cn',
                            'Connection': 'keep-alive',
                            'Cache-Control': 'max-age=0',
                            'Upgrade-Insecure-Requests': '1',
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3861.400 QQBrowser/10.7.4313.400',
                            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                            'Accept-Encoding': 'gzip, deflate',
                            'Accept-Language': 'zh-CN,zh;q=0.9',
                            # 'Cookie': f'area_id=320505; _suzhou_session={build_cookie()}',
                        },
                        dont_filter=True
                    )

    # 处理sku列表
    def parse_sku_page(self, response):
        meta = response.meta
        cur_page = meta.get('page')
        cat3_id = meta.get('catalog3Id')

        # 处理商品
        sku_list = parse_sku(response)
        if sku_list:
            self.logger.info('清单: cat=%s, page=%s, cnt=%s' % (cat3_id, cur_page, len(sku_list)))
            yield Box('sku', self.batch_no, sku_list)
        else:
            self.logger.info('空页: cat=%s, page=%s' % (cat3_id, cur_page))

        # 发起分页
        if cur_page == 1:
            pages = parse_sku_page(response)
            if pages > 1:
                self.logger.info('总页数: cat=%s, pages=%s' % (cat3_id, pages))
                for page in range(2, pages + 1):
                    yield Request(
                        url=self.sku_list_url.format(cat3_id, page),
                        callback=self.parse_sku_page,
                        errback=self.error_back,
                        meta={
                            'reqType': 'sku',
                            'batchNo': self.batch_no,
                            'catalog3Id': cat3_id,
                            'page': page,
                        },
                        headers={
                            'Host': 'www.zfcgwssc.suzhou.gov.cn',
                            'Connection': 'keep-alive',
                            'Cache-Control': 'max-age=0',
                            'Upgrade-Insecure-Requests': '1',
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3861.400 QQBrowser/10.7.4313.400',
                            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                            'Accept-Encoding': 'gzip, deflate',
                            'Accept-Language': 'zh-CN,zh;q=0.9',
                            # 'Cookie': f'area_id=320505; _suzhou_session={build_cookie()}',
                        },
                        dont_filter=True
                    )
