function selectTag(e, t, n) {
    var r = document.getElementById("tags").getElementsByTagName("li"), o = r.length;
    for (i = 0; i < o; i++) r[i].className = "";
    for (t.parentNode.className = "selectTag", i = 0; j = document.getElementById("tagContent" + i); i++) j.style.display = "none";
    document.getElementById(e).style.display = "block", "tagContent2" != e && "tagContent3" != e || $.ajax({
        type: "get",
        url: "/commodities/get_commodity_info",
        data: "id=" + n + "&showContent=" + e,
        cache: !1,
        success: function (t) {
            document.getElementById("tagContent" + e.substr(e.length - 1, 1)).innerHTML = t, $(".customer_tag:first") && $(".customer_tag:first").click()
        }
    }), "tagContent4" == e && $.ajax({
        type: "get",
        url: "/commodities/get_brand_authorize",
        data: "id=" + n,
        cache: !1,
        success: function (t) {
            $("#tagContent4").html(t)
        }
    })
}

function show_tip(t) {
    $.dialog({content: document.getElementById(t), width: "500px"})
}

function login(t) {
    dialog({
        id: "login",
        title: "\u7528\u6237\u767b\u5f55",
        width: "460px",
        height: "200px",
        content: document.getElementById(t).innerHTML
    }).show()
}

function SDMenu(t) {
    if (!document.getElementById || !document.getElementsByTagName) return !1;
    this.menu = document.getElementById(t), this.submenus = this.menu.getElementsByTagName("div"), this.remember = !0, this.speed = 10, this.markCurrent = !0, this.oneSmOnly = !1
}

function jsddm_open() {
    jsddm_canceltimer(), jsddm_close(), ddmenuitem = $(this).find("ul").eq(0).css("visibility", "visible")
}

function jsddm_close() {
    ddmenuitem && ddmenuitem.css("visibility", "hidden")
}

function jsddm_timer() {
    closetimer = window.setTimeout(jsddm_close, timeout)
}

function jsddm_canceltimer() {
    closetimer && (window.clearTimeout(closetimer), closetimer = null)
}

function compare_cp(t, e, i, n) {
    $("#compare_tip").show(), insert_cp_dom($($("#compare_tip li.no_data")[0]), t, e, i, n), $.cookie("open_compare", "true", {path: "/"}), show_save_cps()
}

function compare_cp_with_div(t, e, i, n, r) {
    insert_cp_dom($(t), e, i, n, r)
}

function insert_cp_dom(t, e, i, n, r) {
    t.find(".cp img.cp_img").prop("src", i), t.find(".cp a.name").prop("href", "/cps/" + e + ".html").text(n.substring(0, 30) + "..."), t.find(".cp .price").text("\uffe5" + r), t.find(".cp").show(), t.removeClass("no_data"), t.find(".no_cp").hide();
    var o = t.prop("id");
    $.cookie(o, e + "@@@" + i + "@@@" + n + "@@@" + r, {path: "/"})
}

function remove_cp(t) {
    $("#compare_tip li#" + t + " .cp").hide(), $("#compare_tip li#" + t + " .no_cp").show(), $("#compare_tip li#" + t).addClass("no_data"), $.cookie(t, null, {path: "/"})
}

function remove_cps() {
    $("#compare_tip .cp").hide(), $("#compare_tip .no_cp").show(), $("#compare_tip li").addClass("no_data"), $.cookie("cp1", null, {path: "/"}), $.cookie("cp2", null, {path: "/"}), $.cookie("cp3", null, {path: "/"})
}

function do_compare() {
    var i = "";
    $('#compare_tip .cp[style!="display:none"]').each(function (t, e) {
        i += $(e).find("a.name").prop("href").replace("/cps/", "").replace(".html", "") + ","
    }), window.location.href = "/cps/compare?ids=" + i
}

function hide_compare_tip() {
    $("#compare_tip").hide(), $.cookie("open_compare", "false", {path: "/"})
}

function show_save_cps() {
    $(["cp1", "cp2", "cp3"]).each(function (t, e) {
        if ($.cookie(e)) {
            var i = $.cookie(e).split("@@@");
            compare_cp_with_div("#compare_tip li#" + e, i[0], i[1], i[2], i[3])
        }
    })
}

function choose_address() {
    $("#newaddress_box").hide(), 1 == $(".publicaddress").length ? ($("#choose_address_id_0").prop("checked", !0), $("#newaddress_box").show()) : (cid = $("#order_address_id").val(), $("#choose_address_id_" + cid).prop("checked", !0), $(".publicaddress").removeClass("otheraddress"), $("#choose_address_id_" + cid).parent().addClass("otheraddress")), $(".orderaddress").hide(), $(".checkaddress").show()
}

function destroy_address(t) {
    $.ajax({
        type: "delete", url: "/addresses/destroy_address", data: "id=" + t, cache: !1, success: function (t) {
            if ($("#save_address_btn").focus(), t.success) {
                $("#publicaddress_" + t.id).remove(), $(".publicaddress").removeClass("otheraddress"), $(".publicaddress").first().addClass("otheraddress");
                var e = $(".publicaddress").first().find("input:radio[name='choose_address_id']");
                e.prop("checked", !0);
                var i = e.val();
                0 == i ? ($("#order_address_id").val(""), $("#newaddress_box").show()) : ($("#order_address_id").val(i), $("#current_address_info").text(e.next().text()))
            } else alert("\u7cfb\u7edf\u7e41\u5fd9\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5")
        }, error: function () {
            alert("\u7cfb\u7edf\u7e41\u5fd9\u4e2d\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5\uff01")
        }
    })
}

function destroy_invoice(t) {
    $.ajax({
        type: "post", url: "/invoices/" + t + "/shanchu", cache: !1, success: function (t) {
            if ($("#save_invoice_btn").focus(), t.success) {
                $("#publicinvoice_" + t.id).remove(), $(".publicinvoice").removeClass("on"), $(".publicinvoice").first().addClass("on");
                var e = $(".publicinvoice").first().find("input:radio[name='choose_invoice_id']");
                e.prop("checked", !0);
                var i = e.val();
                0 == i ? $("#newinvoice_box").show() : ($("#current_invoice_info").text(e.next().text()), $("#order_invoice_id").val(i))
            } else alert("\u7cfb\u7edf\u7e41\u5fd9\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5\uff01")
        }, error: function () {
            alert("\u7cfb\u7edf\u7e41\u5fd9\u4e2d\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5\uff01")
        }
    })
}

function edit_address(t) {
    $("#edit_address_id").val(t), $("#consignee_name").val($("#hidden_consignee_name_" + t).val()), $("#street").val($("#hidden_street_" + t).val()), $("#mobile").val($("#hidden_mobile_" + t).val()), $("#phone").val($("#hidden_phone_" + t).val());
    var e = $("#hidden_area_id_" + t).val(), i = e.replace(/(\d{4}).*/, "$100"), n = e.replace(/(\d{2}).*/, "$10000");
    $("select.multi-level").first().val(n).change(), $("#value_object-parent-id_" + n).wait(function () {
        this.val(i).change()
    }), $("#value_object-parent-id_" + i).wait(function () {
        this.val(e).change()
    }), $("#newaddress_box").show()
}

function edit_invoice(t) {
    invoice_type_id = $("#hidden_invoice_type_" + t).val(), content_type_id = $("#hidden_content_type_" + t).val(), $("#newinvoice_box").show(), $("input:radio#invoice_type_" + invoice_type_id).prop("checked", !0), $("input:radio#content_type_" + invoice_type_id).prop("checked", !0), $("#invoice_title").val($("#hidden_title_" + t).val()), $("#invoice_tax_no").val($("#hidden_tax_no_" + t).val())
}

function save_ptype() {
    $("#step_ptype").show(), $("#step_ptype_current").hide()
}

function checkPtype(t) {
    var e = !1, i = null, n = null;
    return "ptype_div" == t && (isEmpty(n = $("input:radio[name='order[ptype]']:checked").val()) && (e = !0, i = "\u8bf7\u9009\u62e9\u91c7\u8d2d\u7c7b\u522b"), 0 < $("#plans_div").length && 0 == n && (plan_value = $("input:radio[name='order[plan_id]']:checked").val(), isEmpty(plan_value) && (e = !0, i = "\u91c7\u8d2d\u8bf7\u9009\u62e9\u91c7\u8d2d\u8d44\u91d1\u6838\u5b9a\u5355"))), e ? ($("#" + t + "_error").html(i), $("#" + t).addClass("errorinformation"), !1) : ($("#" + t).removeClass("errorinformation"), $("#" + t + "_error").html(""), !0)
}

function save_invoice() {
    if ($("#newinvoice_box").is(":hidden")) $("#step_invoice").show(), $("#step_invoice_current").hide(), $("#newinvoice_box").hide(); else {
        var t = !0;
        if (checkInvoice("invoice_title_div") || (t = !1), checkInvoice("invoice_type_div") || (t = !1), checkInvoice("content_type_div") || (t = !1), !t) return;
        var e = "id=" + $("#edit_invoice_id").val() + "&invoice[invoice_type]=" + $("input:radio[name='invoice[invoice_type]']:checked").val() + "&invoice[title]=" + $("#invoice_title").val() + "&invoice[tax_no]=" + $("#invoice_tax_no").val() + "&invoice[content_type]=" + $("input:radio[name='invoice[content_type]']:checked").val();
        $.ajax({
            type: "POST", url: "/invoices/save_invoice", data: e, cache: !1, beforeSend: function () {
                $("#save_invoice_btn").after('<span id="order-loading-invoice" class="ajax-state"><b></b>\u6b63\u5728\u63d0\u4ea4\u4e2d\uff0c\u8bf7\u7a0d\u5019\uff01</span>').hide()
            }, success: function (t) {
                t.success ? ($("#current_invoice_info").text(t.inf), $("#order_invoice_id").val(t.id), $("#publicinvoice_" + t.id).remove(), $("#step_invoice_current h2").after(t.html), $("#step_invoice").show(), $("#step_invoice_current").hide(), $("#newinvoice_box").hide(), $("#choose_invoice_id_" + t.id).prop("checked", !0), $(".publicinvoice").removeClass("on"), $("#choose_invoice_id_" + t.id).parent().addClass("on")) : ($("#save_invoice_btn").focus(), alert("\u7cfb\u7edf\u7e41\u5fd9\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5\uff01"))
            }, error: function () {
                $("#save_invoice_btn").focus(), alert("\u7cfb\u7edf\u7e41\u5fd9\u4e2d\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5\uff01")
            }, complete: function () {
                $("#order-loading-invoice").remove(), $("#save_invoice_btn").show()
            }
        })
    }
}

function checkInvoice(t) {
    var e = !1, i = null, n = null;
    return "invoice_title_div" == t ? isEmpty(n = $("#invoice_title").val()) && (e = !0, i = "\u8bf7\u60a8\u586b\u5199\u53d1\u7968\u62ac\u5934") : "invoice_type_div" == t ? isEmpty(n = $("input:radio[name='invoice[invoice_type]']:checked").val()) ? (e = !0, i = "\u8bf7\u60a8\u9009\u62e9\u53d1\u7968\u5f00\u5177\u65b9\u5f0f") : is_forbid(n) || (e = !0, i = "\u53d1\u7968\u5f00\u5177\u65b9\u5f0f\u4e2d\u542b\u6709\u975e\u6cd5\u5b57\u7b26") : "content_type_div" == t && (isEmpty(n = $("input:radio[name='invoice[content_type]']:checked").val()) ? (e = !0, i = "\u8bf7\u60a8\u9009\u62e9\u53d1\u7968\u5185\u5bb9") : is_forbid(n) || (e = !0, i = "\u53d1\u7968\u5185\u5bb9\u4e2d\u542b\u6709\u975e\u6cd5\u5b57\u7b26")), e ? ($("#" + t + "_error").html(i), $("#" + t).addClass("errorinformation"), !1) : ($("#" + t).removeClass("errorinformation"), $("#" + t + "_error").html(""), !0)
}

function save_address() {
    if ($("#newaddress_box").is(":hidden")) $(".orderaddress").show(), $(".checkaddress").hide(), $("#newaddress_box").hide(); else {
        var t = !0;
        if (check_Consignee("name_div") || (t = !1), check_Consignee("area_div") || (t = !1), check_Consignee("street_div") || (t = !1), check_Consignee("call_mobile_div") || (t = !1), check_Consignee("call_phone_div") || (t = !1), check_Consignee("email_div") || (t = !1), !t) return;
        var e = "id=" + $("#edit_address_id").val() + "&address[consignee_name]=" + $("#consignee_name").val() + "&address[area_id]=" + $("#area_id").val() + "&address[mobile]=" + $("#mobile").val() + "&address[street]=" + $("#street").val() + "&address[phone]=" + $("#phone").val();
        $.ajax({
            type: "POST", url: "/addresses/save_address", data: e, cache: !1, beforeSend: function () {
                $("#save_address_btn").after('<span id="order-loading-address" class="ajax-state"><b></b>\u6b63\u5728\u63d0\u4ea4\u4e2d\uff0c\u8bf7\u7a0d\u5019\uff01</span>').hide()
            }, success: function (t) {
                t.success ? ($("#order_address_id").val(t.id), $("#current_address_info").text(t.inf), $("#publicaddress_" + t.id).remove(), $(".checkaddress h2").after(t.html), $(".orderaddress").show(), $(".checkaddress").hide(), $("#newaddress_box").hide(), $("#choose_address_id_" + t.id).prop("checked", !0), $(".publicaddress").removeClass("otheraddress"), $("#choose_address_id_" + t.id).parent().addClass("otheraddress"), $("#order_address_id").change()) : ($("#save_address_btn").focus(), alert("\u7cfb\u7edf\u7e41\u5fd9\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5\uff01"))
            }, error: function () {
                $("#save_address_btn").focus(), alert("\u7cfb\u7edf\u7e41\u5fd9\u4e2d\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5\uff01")
            }, complete: function () {
                $("#order-loading-address").remove(), $("#save_address_btn").show()
            }
        })
    }
}

function calc_total() {
    sum_total = $("#item-buy-sum_total"), current_total = 0, $(".cart-item-total").each(function () {
        current_total += parseFloat($(this).text())
    }), sum_total.text(current_total.toFixed(2)).change()
}

function calc_num() {
    sum_num = $(".checked_num"), sum_num.text($(".cart_checkbox:checked").length)
}

function check_stocks() {
    $(".goodsorder_one").each(function () {
        var t = $(this).prop("pid"), e = $("#order_address_id").val(), i = $("#hidden_area_id_" + e).val();
        $.ajax({
            type: "get", url: "/products/" + t + "/check_stock?area_id=" + i, async: !1, success: function (t) {
                "true" == t ? $(this).find(".goodsorder_one").text("\u6709\u8d27") : $(this).find(".goodsorder_one").text("\u65e0\u8d27")
            }, error: function () {
                alert("\u83b7\u53d6\u5e93\u5b58\u5931\u8d25")
            }
        })
    })
}

function product_collection(t, e) {
    var i = $(t), n = "pid=" + e;
    $.ajax({
        type: "POST", url: "/user_marks", data: n, cache: !1, success: function (t) {
            var e;
            t.success ? ((e = dialog({
                title: "\u64cd\u4f5c\u6210\u529f",
                content: '<i class="show-win-i24 ok-i24"></i>&nbsp;<span>' + t.desc + "</span>"
            })).show(), i.text(t.text), "\u6536\u85cf" == t.text ? i.prev("span.label_collect").removeClass("active") : i.prev("span.label_collect").addClass("active"), setTimeout(function () {
                e.close().remove()
            }, 1500)) : (e = dialog({
                title: "\u6536\u85cf\u5931\u8d25",
                content: '<i class="tip-icon tip-error-24 fl"></i>&nbsp;<span>' + t.desc + "</span>"
            })).show()
        }, error: function () {
            alert("\u7cfb\u7edf\u7e41\u5fd9\u4e2d\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5\uff01")
        }, complete: function () {
        }
    })
}

function user_mark(t, e) {
    var i = $(t), n = "pid=" + e;
    $.ajax({
        type: "POST", url: "/user_marks", data: n, cache: !1, success: function (t) {
            t.success ? (dialog({
                title: "\u64cd\u4f5c\u6210\u529f",
                content: '<i class="show-win-i24 ok-i24"></i>&nbsp;<span>' + t.desc + "</span>"
            }).show(), i.remove()) : dialog({
                title: "\u6536\u85cf\u5931\u8d25",
                content: '<i class="tip-icon tip-error-24 fl"></i>&nbsp;<span>' + t.desc + "</span>"
            }).show()
        }, error: function () {
            alert("\u7cfb\u7edf\u7e41\u5fd9\u4e2d\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5\uff01")
        }, complete: function () {
        }
    })
}

function check_plan(t) {
    var e = !1, i = null;
    return "username_div" == t && isEmpty($("#username").val()) && (e = !0, i = "\u8bf7\u586b\u5199\u7533\u8bf7\u4eba"), "apply_on_div" == t && isEmpty($("#apply_on").val()) && (e = !0, i = "\u8bf7\u586b\u5199\u7533\u8bf7\u65e5\u671f"), "paper_div" == t && isEmpty($("#paper").val()) && (e = !0, i = "\u8bf7\u4e0a\u4f20\u91c7\u8d2d\u6279\u6587"), e ? ($("#" + t + "_error").html(i), $("#" + t).addClass("errorinformation"), !1) : ($("#" + t).removeClass("errorinformation"), $("#" + t + "_error").html(""), !0)
}

function check_Consignee(t) {
    var e = !1, i = null, n = null;
    return "name_div" == t ? (isEmpty(n = $("#consignee_name").val()) && (e = !0, i = "\u8bf7\u60a8\u586b\u5199\u6536\u8d27\u4eba\u59d3\u540d"), 25 < n.length && (e = !0, i = "\u6536\u8d27\u4eba\u59d3\u540d\u4e0d\u80fd\u5927\u4e8e25\u4f4d"), is_forbid(n) || (e = !0, i = "\u6536\u8d27\u4eba\u59d3\u540d\u4e2d\u542b\u6709\u975e\u6cd5\u5b57\u7b26")) : "area_div" == t ? (3 != $(".multi-level").length || isEmpty($(".multi-level").last().val())) && (e = !0, i = "\u8bf7\u60a8\u9009\u62e9\u5b8c\u6574\u7684\u5730\u533a\u4fe1\u606f") : "street_div" == t ? (isEmpty(n = $("#street").val()) && (e = !0, i = "\u8bf7\u60a8\u586b\u5199\u6536\u8d27\u4eba\u8be6\u7ec6\u5730\u5740"), is_forbid(n) || (e = !0, i = "\u6536\u8d27\u4eba\u8be6\u7ec6\u5730\u5740\u4e2d\u542b\u6709\u975e\u6cd5\u5b57\u7b26"), 50 < n.length && (e = !0, i = "\u6536\u8d27\u4eba\u8be6\u7ec6\u5730\u5740\u8fc7\u957f")) : "call_mobile_div" == t ? (t = "call_div", isEmpty(n = $("#mobile").val()) ? (e = !0, i = "\u8bf7\u60a8\u586b\u5199\u6536\u8d27\u4eba\u624b\u673a\u53f7\u7801") : check_mobile(n) || (e = !0, i = "\u624b\u673a\u53f7\u7801\u683c\u5f0f\u4e0d\u6b63\u786e")) : "call_phone_div" == t && (t = "call_d", isEmpty(n = $("#phone").val()) || (is_forbid(n) || (e = !0, i = "\u56fa\u5b9a\u7535\u8bdd\u53f7\u7801\u4e2d\u542b\u6709\u975e\u6cd5\u5b57\u7b26"), checkPhone(n) || (e = !0, i = "\u56fa\u5b9a\u7535\u8bdd\u53f7\u7801\u683c\u5f0f\u4e0d\u6b63\u786e"))), e ? ($("#" + t + "_error").html(i), $("#" + t).addClass("errorinformation"), !1) : ($("#" + t).removeClass("errorinformation"), $("#" + t + "_error").html(""), !0)
}

function choose_invoice() {
    $("#newinvoice_box").hide(), 1 == $(".publicinvoice").length ? ($("#choose_invoice_id_0").prop("checked", !0), $("#newinvoice_box").show()) : (cid = $("#order_invoice_id").val(), $("#choose_invoice_id_" + cid).prop("checked", !0), $(".publicinvoice").removeClass("on"), $("#choose_invoice_id_" + cid).parent().addClass("on")), $("#step_invoice").hide(), $("#step_invoice_current").show()
}

function choose_ptype() {
    $("#step_ptype").hide(), $("#step_ptype_current").show()
}

function showTitleDiv(t) {
    0 == t ? ($("#title_type_0").prop("checked", "checked"), $("#title_type_1").prop("checked", ""), $("#invoice_title").show()) : ($("#title_type_1").prop("checked", "checked"), $("#title_type_0").prop("checked", ""), $("#invoice_title").hide())
}

function art_alert(t) {
    dialog({title: "\u63d0\u793a", content: t, quickClose: !0}).show()
}

function ra_project_price_count() {
    var t = 0, e = 0;
    $("#ra_project_is_needplan_true:checked").length, isEmpty($("#catalog_limit_price").val()) || $("#catalog_limit_price").val();
    $("#ra_project_item_fields .fields:visible").each(function () {
        total_price = (parseFloat($(this).find(".ra_project_item_price").val()) * parseFloat($(this).find(".ra_project_item_num").val())).toFixed(2), t += parseFloat(total_price)
    }), $("#ra_project_pitem_price").val(t), $(".check_fitting:checked").each(function () {
        xyf_total_price = (parseFloat($(this).parent().parent().find(".xyf_price").text()) * parseFloat($(this).parent().parent().find(".xyf_num").val())).toFixed(2), t += parseFloat(xyf_total_price), e += parseInt($(this).parent().parent().find(".xyf_num").val())
    }), $("#ra_project_init_price").val(t);
    var i = parseFloat($("#ra_project_init_price").val()) - parseFloat($("#ra_project_pitem_price").val());
    $("#ra_project_fit_price").val(i), $("#ra_project_fit_num").val(e), $("#total_project_price").val(t), t > parseFloat("1000000") && art_alert("\u7ade\u4ef7\u603b\u4ef7\u4e3a" + t / 1e4 + "\u4e07\u5143;\u7ade\u4ef7\u603b\u4ef7\u5e94\u4e0d\u8d85\u8fc7100\u4e07\u5143"), $(".plan").hide(), $("#ra_project_plan_id").val(""), $(".ra_project_plan_idformError").remove(), $("#ra_project_fit_plan_id").val(""), $(".ra_project_fit_plan_idformError").remove(), $("#ra_project_init_price").val(init_price)
}

function set_select_rule(t) {
    var e;
    t = $("input[name='ra_project[ra_rule_id]']:checked").val();
    return e = $("#ra_project_init_price").val(), $.ajax({
        url: "/hero/ra/projects/get_ra_rule",
        type: "get",
        data: {ra_rule_id: t, init_price: e},
        success: function (t) {
            $("#ra_project_turn_time").html(t.turn_time), $("#ra_project_turn_count").html(t.turn_count), $("#ra_project_per_count").html(t.per_count), $("#ra_project_range").val(t.range), $("#range_min_max").text(t.range_min_max)
        }
    })
}

function open_c_detail(t) {
    $("#c_detail").is(":hidden") ? ($("#c_detail").show(), $(t).text("\u9690\u85cf\u8be6\u7ec6\u53c2\u6570")) : ($("#c_detail").hide(), $(t).text("\u67e5\u770b\u8be6\u7ec6\u53c2\u6570"))
}

function show_ra_project_product(t, e) {
    var i;
    i = dialog({id: "ra_project", title: "\u4ea7\u54c1\u4fe1\u606f", width: "800px"}), $.ajax({
        type: "get",
        url: "/raj/projects/show_ra_project_product",
        data: {id: t, t: e},
        success: function (t) {
            i.content(t).show()
        }
    })
}

function show_xy_product(t, e) {
    var i;
    i = dialog({id: "ra_project", title: "\u5546\u54c1\u4fe1\u606f", width: 700}), $.ajax({
        type: "get",
        url: "/xy_products/show_xy_product",
        data: {id: t, t: e},
        success: function (t) {
            i.content(t).show()
        }
    })
}

function add_invoice_no(e) {
    dialog({
        title: "\u8bf7\u586b\u5199\u53d1\u7968\u7f16\u53f7",
        content: '<input name="invocie_no" id="invoice_no" class="h20"/><br/><span class="red">*\u6ce8\u610f\u586b\u5199\u6210\u529f\u540e\u4e0d\u53ef\u4fee\u6539\uff01</span>',
        okValue: "\u786e\u5b9a",
        ok: function () {
            return isEmpty($("#invoice_no").val()) ? alert("\u8bf7\u8f93\u5165\u53d1\u7968\u7f16\u53f7") : (this.title("\u63d0\u4ea4\u4e2d\u2026"), $.ajax({
                type: "post",
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                url: "/hero/orders/" + e + "/add_invoice_no?invoice_no=" + $("#invoice_no").val(),
                beforeSend: function () {
                },
                success: function (t) {
                    t.success ? window.location.href = "/hero/orders/" + e + "/paper" : alert(t.msg)
                },
                complete: function () {
                },
                error: function () {
                    alert("\u7cfb\u7edf\u9519\u8bef\uff0c\u8bf7\u5237\u65b0\u9875\u9762\u91cd\u8bd5")
                }
            })), !1
        },
        cancelValue: "\u53d6\u6d88",
        cancel: function () {
        }
    }).show()
}

function check_ra_bid_form() {
    var t = $("#gysbj_price").val(), e = $("#gysbj_sent_day").val(),
        i = ($("#init_price").text(), $("#base_price").text());
    $("#quantity").text(), $("#ra_project_id").val(), $("#current_turn").val();
    if ($("#price").val(t), $("#sent_day").val(e), isEmpty(e)) return art_alert("\u60a8\u8fd8\u6ca1\u6709\u8f93\u5165\u627f\u8bfa\u9001\u8d27\u5929\u6570"), !1;
    if (60 < parseInt(e)) return $("#gysbj_sent_day").val(null), art_alert("\u627f\u8bfa\u9001\u8d27\u5929\u6570\u6709\u8bef\uff0c\u8bf7\u91cd\u65b0\u586b\u5199"), !1;
    if (isNaN(e) || 1 * e <= 0) return art_alert("\u8bf7\u8f93\u5165\u5927\u4e8e\u96f6\u7684\u6570\u5b57!"), !1;
    if (isEmpty(t)) return art_alert("\u60a8\u8fd8\u6ca1\u6709\u8f93\u5165\u62a5\u4ef7"), !1;
    if (isNaN(t) || 1 * t <= 0) return art_alert("\u8bf7\u8f93\u5165\u5927\u4e8e\u96f6\u7684\u6570\u5b57!"), !1;
    if (1 * i < 1 * t) return art_alert("\u60a8\u7684\u62a5\u4ef7\u81f3\u5c11\u8981\u5c0f\u4e8e\u7b49\u4e8e<span class='red'>" + i + "</span>\u5143"), !1;
    1 * t < .8 * i ? dialog({
        title: "\u62a5\u4ef7\u63d0\u793a",
        content: "<div class='b font18'>\u60a8\u62a5\u7684\u603b\u4ef7<span class='red'>" + t + "</span>\u5143\uff0c\u964d\u5e45\u5df2\u8d85\u8fc7\u53ef\u89c1\u6700\u4f4e\u62a5\u4ef7<span class='red'>" + i + "</span>\u5143\u7684<span class='red'>20%</span>\uff0c\u60a8\u786e\u8ba4\u7ee7\u7eed\u5417\uff1f</div>",
        okValue: "\u786e\u5b9a",
        ok: function () {
            $("#bid_form").submit()
        },
        cancelValue: "\u53d6\u6d88",
        cancel: function () {
        }
    }).show() : dialog({
        title: "\u62a5\u4ef7\u63d0\u793a",
        content: "<div class='b font18'>\u60a8\u62a5\u7684\u603b\u4ef7\uff1a<span class='red'>" + t + "</span>\u5143\uff0c\u60a8\u786e\u8ba4\u4e48\uff1f</div>",
        okValue: "\u786e\u5b9a",
        ok: function () {
            $("#bid_form").submit()
        },
        cancelValue: "\u53d6\u6d88",
        cancel: function () {
        }
    }).show()
}

function check_np_bid_form() {
    var e = $("#gysbj_price").val(), t = $("#init_price").text(), i = $("#base_price").text(),
        n = ($("#quantity").text(), $("#np_project_id").val()),
        r = ($("#current_turn").val(), parseInt($("#sy_count").text()));
    return $("#price").val(e), isEmpty(e) ? (art_alert("\u60a8\u8fd8\u6ca1\u6709\u8f93\u5165\u62a5\u4ef7"), !1) : isNaN(e) || 1 * e <= 0 ? (art_alert("\u8bf7\u8f93\u5165\u5927\u4e8e\u96f6\u7684\u6570\u5b57!"), !1) : 1 * i < 1 * e ? (art_alert("\u60a8\u7684\u62a5\u4ef7\u81f3\u5c11\u8981\u5c0f\u4e8e\u7b49\u4e8e<span class='red'>" + i + "</span>\u5143"), !1) : void (1 * e < .8 * t ? (dialog_a = dialog({
        title: "\u7b2c\u4e00\u6b21\u62a5\u4ef7\u4ef7\u683c\u63d0\u9192",
        content: "<div class='b font18'>\u60a8\u62a5\u7684\u603b\u4ef7<span class='red'>" + e + "</span>\u5143\uff0c\u964d\u5e45\u5df2\u8d85\u8fc7\u8d77\u62cd\u4ef7<span class='red'>" + t + "</span>\u5143\u7684<span class='red'>20%</span>;\u5269\u4f59\u62a5\u4ef7\u6b21\u6570\uff1a<span class='red'>" + r + "</span>\u6b21\uff0c\u60a8\u786e\u8ba4\u9879\u76ee\u62a5\u4ef7\u5417\uff1f</div>",
        cancelValue: "\u53d6\u6d88",
        cancel: function () {
            dialog_a.close(), clearTimeout(time_a)
        },
        okValue: "\u786e\u5b9a",
        ok: function () {
            clearTimeout(time_a), dialog_a.close(), dialog_b.show()
        },
        onshow: function () {
            time_a = setTimeout(function () {
                close_dialog(dialog_a, dialog_b, tag_a)
            }, 3e3)
        }
    }), dialog_b = dialog({
        title: "\u7b2c\u4e8c\u6b21\u62a5\u4ef7\u63d0\u9192",
        content: "<div class='b font18'>\u60a8\u62a5\u7684\u603b\u4ef7<span class='red'>" + e + "</span>\u5143\uff0c\u964d\u5e45\u5df2\u8d85\u8fc7\u8d77\u62cd\u4ef7<span class='red'>" + t + "</span>\u5143\u7684<span class='red'>20%</span>;\u5269\u4f59\u62a5\u4ef7\u6b21\u6570\uff1a<span class='red'>" + r + "</span>\u6b21\uff0c\u60a8\u786e\u8ba4\u9879\u76ee\u62a5\u4ef7\u5417\uff1f</div>",
        cancelValue: "\u53d6\u6d88",
        cancel: function () {
            dialog_b.close(), clearTimeout(time_b)
        },
        onshow: function () {
            time_b = setTimeout(function () {
                close_dialog(dialog_b, dialog_d, tag_b)
            }, 3e3)
        },
        okValue: "\u786e\u5b9a",
        ok: function () {
            clearTimeout(time_b), dialog_b.close(), dialog_d.show()
        }
    }), dialog_d = dialog({
        title: "\u62a5\u4ef7\u63d0\u793a",
        content: "<div class='b font18'>\u60a8\u62a5\u7684\u603b\u4ef7<span class='red'>" + e + "</span>\u5143\uff0c\u964d\u5e45\u5df2\u8d85\u8fc7\u8d77\u62cd\u4ef7<span class='red'>" + t + "</span>\u5143\u7684<span class='red'>20%</span>;\u5269\u4f59\u62a5\u4ef7\u6b21\u6570\uff1a<span class='red'>" + r + "</span>\u6b21\uff0c\u60a8\u786e\u8ba4\u7ee7\u7eed\u5417\uff1f</div>",
        okValue: "\u786e\u5b9a",
        ok: function () {
            $("#bid_form").submit()
        },
        cancelValue: "\u53d6\u6d88",
        cancel: function () {
        }
    }), dialog_a.show()) : $.ajax({
        url: "/np_projects/" + n + "/ajax_least_bid?price=" + e, success: function (t) {
            t.status ? dialog({
                title: "\u62a5\u4ef7\u63d0\u793a",
                content: "<div class='b font18'>\u60a8\u62a5\u7684\u603b\u4ef7\uff1a<span class='red'>" + e + "</span>\u5143;\u5269\u4f59\u62a5\u4ef7\u6b21\u6570\uff1a<span class='red'>" + r + "</span>\u6b21\uff0c\u60a8\u786e\u8ba4\u4e48\uff1f</div>",
                okValue: "\u786e\u5b9a",
                ok: function () {
                    $("#bid_form").submit()
                },
                cancelValue: "\u53d6\u6d88",
                cancel: function () {
                }
            }).show() : ($(".lest_bid").text(t.least_price), $(".list_least_price").text(t.least_price), $(".must_lt_price").text(t.base_price), $("#base_price").text(t.base_price), dialog({
                title: "\u62a5\u4ef7\u63d0\u793a",
                content: "<div class='b font18'>\u60a8\u62a5\u7684\u603b\u4ef7<span class='red'>" + e + "</span>\u5143\uff0c\u76ee\u524d\u6700\u4f4e\u53ef\u62a5\u4ef7<span class='red'>" + t.base_price + "</span>\u5143</div>",
                cancelValue: "\u53d6\u6d88",
                cancel: function () {
                }
            }).show())
        }, cache: !0
    }))
}

function close_dialog(t, e) {
    t.close(), e.show()
}

function time_down() {
    $(".project_time").each(function (t, e) {
        item_count_down($(e), parseInt($(e).children(".seconds").text()))
    })
}

function count_down() {
    $(".ra_project_time").each(function (t, e) {
        item_count_down($(e), parseInt($(e).children(".seconds").text()))
    })
}

function np_count_down() {
    $(".np_project_time").each(function (t, e) {
        item_count_down($(e), parseInt($(e).children(".seconds").text()))
    })
}

function item_count_down(t, e) {
    window.location.pathname;
    origin_before_begin = parseInt($("#before_begin").text()), origin_before_end = parseInt($("#before_end").text()), e <= 0 ? (t.children(".time_str").html('<span class="red b">\u6b63\u5728\u8fdb\u5165\u7ade\u4ef7\u73af\u8282......</span>'), window.location.reload()) : (e -= 1, t.children(".time_str").html('<div class="time-item">' + get_time_str(e) + "</div>"), setTimeout(function () {
        item_count_down(t, e)
    }, 1e3))
}

function get_time_str(t) {
    return msg = "", 0 < t && (days = Math.floor(t / 86400), hours = Math.floor(t / 3600) - 24 * days, minutes = Math.floor(t / 60) - 24 * days * 60 - 60 * hours, seconds = Math.floor(t) - 24 * days * 60 * 60 - 60 * hours * 60 - 60 * minutes, minutes <= 9 && (minutes = "0" + minutes), seconds <= 9 && (seconds = "0" + seconds), 0 < days && (msg += '<strong class="day_show"><span>' + days + "</span>\u5929</strong>"), msg += '<strong class="hour_show"><span>' + hours + '</span>\u65f6</strong><strong class="minute_show"><span>' + minutes + '</span>\u5206</strong><strong class="second_show"><span>' + seconds + "</span>\u79d2</strong>"), msg
}

function timer(r) {
    window.setInterval(function () {
        var t = 0, e = 0, i = 0, n = 0;
        0 < r && (t = Math.floor(r / 86400), e = Math.floor(r / 3600) - 24 * t, i = Math.floor(r / 60) - 24 * t * 60 - 60 * e, n = Math.floor(r) - 24 * t * 60 * 60 - 60 * e * 60 - 60 * i), i <= 9 && (i = "0" + i), n <= 9 && (n = "0" + n), $("#day_show").html(t + "\u5929"), $("#hour_show").html('<s id="h"></s>' + e + "\u65f6"), $("#minute_show").html("<s></s>" + i + "\u5206"), $("#second_show").html("<s></s>" + n + "\u79d2"), r--
    }, 1e3)
}

function randomString(t) {
    t = t || 32;
    var e = "ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678", n = e.length, r = "";
    for (i = 0; i < t; i++) r += e.charAt(Math.floor(Math.random() * n));
    return r
}

function pad(t, e) {
    return Array(t < e ? e - ("" + t).length + 1 : 0).join(0) + t
}

function undefind_function() {
    checkIe();
    for (var t = $(".other_temp").text(), e = (new Date).getSeconds(), i = 100 * Number(t) + e, n = parseInt(Number(i) / 1e3), r = new Array(4), o = 3; 0 <= o; o--) 3 == o ? r[3] = Number(i) % 1e3 : (r[o] = n % 1e3, n = parseInt(n / 1e3));
    var s = r.map(function (t) {
        var e, i = [1, 3, 5, 7, 9], n = [0, 2, 4, 6, 8];
        return t < 480 ? (t = 1e3 - t, e = i[Math.floor(Math.random() * i.length)]) : e = n[Math.floor(Math.random() * n.length)], (randomString(2) + t.toString(16) + e).toUpperCase()
    }).join("-"), a = parseInt(e / 10), l = e % 10, c = a * l * 100 + 10 * (a + 1) + (9 - l);
    $.cookie("_suzhou_session", s + "-" + randomString(4) + c, {path: "/"})
}

function check_cart_stocks() {
    var t = !0, r = {}, o = {};
    return $("input[name^=p-]:checked").each(function () {
        var t = $(this).attr("eid"), e = $(this).attr("sku"), i = $(this).attr("product_id"),
            n = $(this).parents(".merchandise").find("input[id^='cart_item_']").val();
        r[t] || (r[t] = []), o[i] = n, r[t].push({sku: e, num: n})
    }), $.ajax({
        type: "post",
        url: "/cart/check_cart_stocks",
        async: !0,
        data: {products: r, product_ids: o},
        success: function (t) {
            for (var e in t) $(".has_stock_" + t[e].sku_id).html(t[e].limit)
        }
    }), t
}

function replaceKey(t, i) {
    Object.keys(t).reduce(function (t, e) {
        return t[i[e] || e] = data[e], t
    })
}

function textarea_auto_height(t) {
    t.style.height = "1px", t.style.height = 25 + t.scrollHeight + "px"
}

function addToFavorite() {
    var t = -1 != navigator.userAgent.toLowerCase().indexOf("mac") ? "Command/Cmd" : "CTRL";
    document.all ? window.external.addFavorite("http://www.sinopr.org", "\u516c\u5171\u8d44\u6e90\u4ea4\u6613\u7f51") : window.sidebar ? window.sidebar.addPanel("\u516c\u5171\u8d44\u6e90\u4ea4\u6613\u7f51", "http://www.sinopr.org", "") : alert("\u6dfb\u52a0\u5931\u8d25\n\u60a8\u53ef\u4ee5\u5c1d\u8bd5\u901a\u8fc7\u5feb\u6377\u952e" + t + " + D \u52a0\u5165\u5230\u6536\u85cf\u5939~")
}

function addHomePage() {
    var t = this.href;
    try {
        this.style.behavior = "url(#default#homepage)", this.setHomePage(t)
    } catch (i) {
        if (document.all) document.body.style.behavior = "url(#default#homepage)", document.body.setHomePage(t); else if (window.sidebar) {
            if (window.netscape) try {
                netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect")
            } catch (i) {
                return alert("\u6b64\u64cd\u4f5c\u88ab\u6d4f\u89c8\u5668\u62d2\u7edd\uff01\n\u8bf7\u5728\u6d4f\u89c8\u5668\u5730\u5740\u680f\u8f93\u5165\u201cabout:config\u201d\u5e76\u56de\u8f66\n\u7136\u540e\u5c06 [signed.applets.codebase_principal_support]\u7684\u503c\u8bbe\u7f6e\u4e3a'true',\u53cc\u51fb\u5373\u53ef\u3002"), !1
            }
            var e = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);
            e.setCharPref("browser.startup.homepage", t)
        } else $("#sethomepage").href()
    }
    return !1
}

function isEmpty(t) {
    return null == t || "" == t || "undefined" == t || t == undefined || "null" == t || "" == (t = t.replace(/\s/g, ""))
}

function trimTxt(t) {
    return t.replace(/(^\s*)|(\s*$)/g, "")
}

function is_forbid(t) {
    t = (t = (t = (t = (t = (t = (t = (t = (t = (t = (t = (t = (t = (t = (t = (t = (t = trimTxt(t)).replace("*", "@")).replace("--", "@")).replace("/", "@")).replace("+", "@")).replace("'", "@")).replace("\\", "@")).replace("$", "@")).replace("^", "@")).replace(".", "@")).replace(";", "@")).replace("<", "@")).replace(">", "@")).replace('"', "@")).replace("=", "@")).replace("{", "@")).replace("}", "@");
    var e = new String("@,%,~,&"), n = new Array;
    for (n = e.split(","), i = 0; i < n.length; i++) if (-1 != t.search(new RegExp(n[i]))) return !1;
    return !0
}

function check_mobile(t) {
    return !!new RegExp(/^1[0-9]{10}$/).test(t)
}

function checkPhone(t) {
    if (20 < t.length) return !1;
    for (var e = "(0123456789-)", i = t.length, n = 0; n < i; n++) {
        var r = t.substring(n, n + 1);
        if (e.indexOf(r) < 0) return !1
    }
    return !0
}

function cutstr(t, e) {
    var i = 0, n = 0;
    str_cut = new String, n = t.length;
    for (var r = 0; r < n; r++) if (a = t.charAt(r), i++, 4 < escape(a).length && i++, str_cut = str_cut.concat(a), e <= i) return str_cut = str_cut.concat("..."), str_cut;
    if (i < e) return t + "&nbsp;&nbsp;&nbsp;&nbsp;"
}

function find_duplicates(t) {
    for (var e = t.length, i = [], n = {}, r = 0; r < e; r++) {
        n[o = t[r]] = 1 <= n[o] ? n[o] + 1 : 1
    }
    for (var o in n) 1 < n[o] && i.push(o);
    return i
}

function dealNull(t) {
    for (var e in t) null == t[e] || "null" == t[e] ? t[e] = "" : "object" == typeof t[e] && dealNull(t[e])
}

function reset_input(t) {
    var e = t.attr("id"), i = t.attr("class"), n = t.attr("name"), r = t.attr("type"),
        o = document.createElement("input");
    o.setAttribute("id", e), o.setAttribute("class", i), o.setAttribute("name", n), o.setAttribute("type", r);
    var s = t.parent()[0];
    s.removeChild(t[0]), $(s).prepend($(o))
}

function alert_warm(t) {
    var e = dialog({
        title: "\u63d0\u793a", width: 260, content: t, okValue: "\u786e\u5b9a", ok: function () {
            e.close()
        }
    });
    e.show()
}

function checkIe() {
    $.browser.msie && $.browser.version < 9 && dialog({
        width: 500,
        content: "\u7cfb\u7edf\u68c0\u6d4b\u5230\u60a8\u5f53\u524d\u4f7f\u7528\u7684IE\u6d4f\u89c8\u5668\u7248\u672c\u8fc7\u4f4e\uff0c\u4e3a\u4e86\u4e0d\u5f71\u54cd\u60a8\u7684\u5546\u57ce\u8d2d\u7269\u4f53\u9a8c\uff0c\u8bf7\u5347\u7ea7\u81f3\u6700\u65b0\u7248\u672c\u7684IE\u6d4f\u89c8\u5668\uff0c\u5347\u7ea7\u65b9\u6cd5\uff1a<a target='_blank' href='https://jingyan.baidu.com/article/0eb457e5e7797403f1a90520.html'>\u600e\u4e48\u5347\u7ea7ie\u6d4f\u89c8\u5668</a>",
        okValue: "\u786e\u5b9a"
    }).showModal()
}

function getObjectURL(t) {
    var e = null;
    return window.createObjectURL != undefined ? e = window.createObjectURL(t) : window.URL != undefined ? e = window.URL.createObjectURL(t) : window.webkitURL != undefined && (e = window.webkitURL.createObjectURL(t)), e
}

function snatch_price(url, snatch_gz, callback) {
    for (var flag = !0, i = 0; i < snatch_gz.length; i++) {
        var snatch = snatch_gz[i];
        if ("Other" != snatch.code) {
            var reg = new RegExp(snatch.reg_exp);
            if (reg.test(url)) {
                var sku = url.match(reg)[1];
                eval("get" + snatch.code + "Price")(sku, function (t) {
                    "function" == typeof callback && callback(snatch, t)
                }), flag = !1;
                break
            }
        }
    }
    if (flag && "function" == typeof callback) {
        var snatch = snatch_gz.myFind(function (t) {
            return "Other" == t.code
        });
        callback(snatch, null)
    }
}

function getJdPrice(t, r) {
    skus = t.toString(), $.ajax({
        url: "https://p.3.cn/prices/mgets?skuIds=J_" + skus + "&type=1",
        dataType: "jsonp",
        success: function (t) {
            if (!t && !t.length) return !1;
            for (var e = 0; e < t.length; e++) {
                var i = t[e].id.replace("J_", "");
                price = parseFloat(t[e].p, 10);
                var n = {sku: i, price: price};
                "function" == typeof r && r(n)
            }
        },
        error: function () {
            r(null)
        }
    })
}

function pad(t, e) {
    return Array(t < e ? e - ("" + t).length + 1 : 0).join(0) + t
}

function EmploreName_select2(t, e, i, n, r) {
    $(t).select2({
        ajax: {
            url: e, dataType: "JSON", type: "get", data: function (t) {
                return {p: t, page: 1, model: i, opts: r}
            }, results: function (t) {
                return {results: t.items}
            }, processResults: function (t) {
                var i = new Array;
                return $(t.items).each(function (t, e) {
                    i.push({id: e.id, text: e.text})
                }), {results: i}
            }, cache: !0
        }, placeholder: n, escapeMarkup: function (t) {
            return t
        }, formatResult: function o(t) {
            return t.text
        }, formatSelection: function s(t) {
            return t.text
        }
    })
}

function csv2sheet(e) {
    var n = {};
    return (e = e.split("\n")).forEach(function (t, i) {
        t = t.split(","), 0 == i && (n["!ref"] = "A1:" + String.fromCharCode(65 + t.length - 1) + (e.length - 1)), t.forEach(function (t, e) {
            n[String.fromCharCode(65 + e) + (i + 1)] = {v: t}
        })
    }), n
}

function sheet2blob(t, e) {
    function i(t) {
        for (var e = new ArrayBuffer(t.length), i = new Uint8Array(e), n = 0; n != t.length; ++n
        ) i[n] = 255 & t.charCodeAt(n);
        return e
    }

    var n = {SheetNames: [e = e || "sheet1"], Sheets: {}};
    n.Sheets[e] = t;
    var r = {bookType: "xlsx", bookSST: !1, type: "binary"}, o = XLSX.write(n, r);
    return new Blob([i(o)], {type: "application/octet-stream"})
}

function readWorkbookFromLocalFile(t, n) {
    var e = new FileReader;
    e.onload = function (t) {
        var e = t.target.result, i = XLSX.read(e, {type: "binary"});
        n && n(i)
    }, e.readAsBinaryString(t)
}

function openDownloadDialog(t, e) {
    "object" == typeof t && t instanceof Blob && (t = URL.createObjectURL(t));
    var i, n = document.createElement("a");
    n.href = t, n.download = e || "", window.MouseEvent ? i = new MouseEvent("click") : (i = document.createEvent("MouseEvents")).initMouseEvent("click", !0, !1, window, 0, 0, 0, 0, 0, !1, !1, !1, !1, 0, null), n.dispatchEvent(i)
}

function wrongMsg(t, e) {
    $(".wrongMsg").text(e), t.addClass("wrong")
}

function viewHotCities() {
    $.each(cities, function (t, e) {
        e.hotCity && $(".hotCity .city-list ul").append("<li><a><input type='button' style='background:none;border:0px;cursor: pointer;' onclick=hotCityAddrInput('" + e.provinceId + "," + e.id + "," + e.name + "') id='" + e.id + "' value='" + e.name + "'></a></li>")
    })
}

function json2str(t) {
    var e = [], i = function (t) {
        return "object" == typeof t && null != t ? json2str(t) : /^(string|number)$/.test(typeof t) ? "'" + t + "'" : t
    };
    for (var n in t) e.push("'" + n + "':" + i(t[n]));
    return "{" + e.join(",") + "}"
}

function countProvincePages() {
    return provinceTotalPage = Math.ceil(provinces.length / p_pageSize), provinceTotalPage
}

function viewProvince(t) {
    var e, i;
    $(".province .city-list ul li").remove(), 1 == t ? $(".province .pre a").removeClass("can") : $(".province .pre a").addClass("can"), $(".province .next a").addClass("can"), t == provinceTotalPage ? (i = (t - 1) * p_pageSize, e = provinces.length, $(".province .next a").removeClass("can")) : e = (i = (t - 1) * p_pageSize) + p_pageSize;
    for (var n = i; n < e; n++) {
        var r = provinces[n].id, o = provinces[n].provinceName;
        o = "\u5185\u8499\u53e4\u81ea\u6cbb\u533a" == provinces[n].provinceName ? "\u5185\u8499\u53e4" : "\u9ed1\u9f99\u6c5f\u7701" == provinces[n].provinceName ? "\u9ed1\u9f99\u6c5f" : provinces[n].provinceName.substr(0, 2);
        var s = $('<li><a style="background: none repeat scroll 0% 0% transparent; border: 0px none;" href="javascript:onclick=viewCities(' + n + ');" id="' + r + '">' + o + "</a></li>");
        $(".province .city-list ul").append(s)
    }
    $(".province .city-list #provincePage").remove(), $(".province .city-list").append("<label id='provincePage' style='display:none;'>" + t + "</label>")
}

function viewCities(t) {
    proId = provinces[t].id, $("body").data("pName", provinces[t].provinceName), $("body").data("pId", proId), citys = [];
    var i = 0;
    $.each(cities, function (t, e) {
        e.provinceId == proId && (citys[i++] = e)
    }), cityTotalPage = Math.ceil(citys.length / p_pageSize), $(".provinceCity").find(".tabs").find("a").removeClass("current"), $(".provinceCity .tabs").find("#city").addClass("current"), $(".con .province .city-list a").removeClass("current"), $(".con .province .city-list a[id='" + proId + "']").addClass("current"), $(".provinceCity").find(".con").children().hide(), $(".provinceCity").find(".con").find(".city").show(), cityPage(1)
}

function cityPage(t) {
    var e, i;
    $(".city .city-list ul li").remove(), $(".cityAll .city-list ul li").remove(), 1 == t ? $(".city .pre a").removeClass("can") : $(".city .pre a").addClass("can"), t <= 1 && (t = 1, $(".city .pre a").removeClass("can"), $(".city .next a").addClass("can")), 1 == cityTotalPage && ($(".city .next a").removeClass("can"), $(".city .pre a").removeClass("can")), t >= cityTotalPage ? (t = cityTotalPage, $(".city .next a").removeClass("can"), e = (t - 1) * p_pageSize, i = citys.length) : 1 == t ? (i = (e = (t - 1) * p_pageSize) + p_pageSize, $(".city .pre a").removeClass("can"), $(".city .next a").addClass("can")) : (i = (e = (t - 1) * p_pageSize) + p_pageSize, $(".city .next a").addClass("can"), $(".city .pre a").addClass("can"));
    for (var n = e; n < i; n++) {
        var r = citys[n].id, o = citys[n].name.substr(0, 4),
            s = $('<li><a href="javascript:onclick=viewCounties(' + n + ')" id="' + r + '">' + o + "</a></li>");
        $(".city .city-list ul").append(s)
    }
    $(".city .city-list #cityPage").remove(), $(".city .city-list").append("<label id='cityPage' style='display:none;'>" + t + "</label>")
}

function viewCounties(t) {
    cityId = citys[t].id, $("body").data("cId", cityId);
    var e = $.trim(citys[t].name);
    $("body").data("nameOfCity", e), counties = [];
    var i = 0;
    $.each(areas, function (t, e) {
        e.cityId == cityId && (counties[i++] = e)
    }), countyTotalPage = Math.ceil(counties.length / p_pageSize), $(".provinceCity").find(".tabs").find("a").removeClass("current"), $(".provinceCity .tabs").find("#county").addClass("current"), $(".con .city .city-list a").removeClass("current"), $(".con .city .city-list a[id='" + cityId + "']").addClass("current"), $(".provinceCity").find(".con").children().hide(), $(".provinceCity").find(".con").find(".county").show(), countyPage(1)
}

function countyPage(t) {
    $("input.current1").attr("name");
    var e, i, n = $("body").data("pName"), r = $("body").data("nameOfCity");
    $("input.current1").removeClass("iGrays"), $("input.current1").val(n + r), $(".county .city-list ul li").remove(), 1 == t ? $(".county .pre a").removeClass("can") : $(".county .pre a").addClass("can"), t <= 1 && (t = 1, $(".county .pre a").removeClass("can"), $(".county .next a").addClass("can")), 1 == countyTotalPage && ($(".county .next a").removeClass("can"), $(".county .pre a").removeClass("can")), t >= countyTotalPage ? (t = countyTotalPage, $(".county .next a").removeClass("can"), e = (t - 1) * p_pageSize, i = counties.length) : 1 == t ? (i = (e = (t - 1) * p_pageSize) + p_pageSize, $(".county .pre a").removeClass("can"), $(".county .next a").addClass("can")) : (i = (e = (t - 1) * p_pageSize) + p_pageSize, $(".county .next a").addClass("can"), $(".county .pre a").addClass("can"));
    for (var o = e; o < i; o++) {
        var s = counties[o].id, a = counties[o].areaName.substr(0, 4),
            l = $('<li><a href="javascript:onclick=addrInput(' + o + ')" id="' + s + '">' + a + "</a></li>");
        $(".county .city-list ul").append(l)
    }
    $(".county .city-list #countyPage").remove(), $(".county .city-list").append("<label id='countyPage' style='display:none;'>" + t + "</label>")
}

function addrInput(t) {
    var i = $.trim(counties[t].id);
    $(".con .hotCity .city-list a input").removeClass("current"), $(".con .hotCity .city-list a input[id='" + cityId + "']").addClass("current"), $(".con .county .city-list a").removeClass("current"), $(".con .county .city-list a[id='" + i + "']").addClass("current"), proId = $("body").data("pId"), cityId = $("body").data("cId");
    var n = null;
    $.each(provinces, function (t, e) {
        if (e.id == proId) return n = e.provinceName, !1
    });
    var r = null;
    $.each(cities, function (t, e) {
        if (e.id == cityId) return r = e.name, !1
    });
    var o = null;
    $.each(counties, function (t, e) {
        if (e.id == i) return o = e.areaName, !1
    }), $("input.current1").removeClass("iGrays"), $(".provinceCity").hide();
    var e = n + r + o;
    $("input.current1").val(e), $(".backifname").hide();
    var s = $("input.current1").attr("name");
    "order.sdeptProCity" == s && ($("#deptCityId").val(cityId), $("input[name='order.sdeptProCity']").trigger("change", [cityId, i])), "consignor.deptProCity" == s && $("input[name='consignor.deptProCity']").trigger("change", [cityId, i]), "template.sdeptProCity" == s && $("input[name='template.sdeptProCity']").trigger("change", [cityId, i])
}

function hotCityAddrInput(t) {
    proId = t.split(",")[0], cityId = t.split(",")[1];
    var e = t.split(",")[2];
    $("body").data("nameOfCity", e), $("body").data("pId", proId), $("body").data("cId", cityId), $.each(provinces, function (t, e) {
        e.id == proId && $("body").data("pName", e.provinceName)
    }), counties = [];
    var i = 0;
    $.each(areas, function (t, e) {
        e.cityId == cityId && (counties[i++] = e)
    }), countyTotalPage = Math.ceil(counties.length / p_pageSize), $(".provinceCity").find(".tabs").find("a").removeClass("current"), $(".provinceCity .tabs").find("#county").addClass("current"), $(".con .city .city-list a").removeClass("current"), $(".con .city .city-list a[id='" + cityId + "']").addClass("current"), $(".provinceCity").find(".con").children().hide(), $(".provinceCity").find(".con").find(".county").show(), $(".con .provinceAll .city-list a").removeClass("current"), countyPage(1)
}

function sendAllProvinceAjax() {
    $.ajax({
        type: "get", url: "/api/query_provinces.json", async: !1, dataType: "json", success: function (t) {
            allProvinces = t.provinces, $("body").data("allProvinces", allProvinces), viewAllProvince(1)
        }, error: function (t, e) {
            alert(e)
        }
    })
}

function sendAllCitiesAjax() {
    $.ajax({
        type: "get", url: "/api/query_cities.json", async: !1, dataType: "json", success: function (t) {
            allCities = t.cities, $("body").data("CitysAll", t), viewAllHotCities()
        }, error: function (t, e) {
            alert(e)
        }
    })
}

function sendAllCountiesAjax() {
    $.ajax({
        type: "get", url: "/api/query_counties.json", async: !1, dataType: "json", success: function (t) {
            allAreas = t.areas, $("body").data("allCountys", t.areas)
        }, error: function () {
            alert("\u7f51\u7edc\u7e41\u5fd9\uff0c\u8bf7\u7a0d\u540e\u518d\u8bd5\uff01")
        }
    })
}

function viewAllHotCities() {
    $.each(allCities, function (t, e) {
        e.hotCity && $(".hotCityAll .city-list ul").append("<li><a><input type='button' style='background:none;border:0px;cursor: pointer;' onclick=hotCityAddrInputAll('" + e.provinceId + "," + e.id + "," + e.name + "') id='" + e.id + "' value='" + e.name + "'></a></li>")
    })
}

function json2str(t) {
    var e = [], i = function (t) {
        return "object" == typeof t && null != t ? json2str(t) : /^(string|number)$/.test(typeof t) ? "'" + t + "'" : t
    };
    for (var n in t) e.push("'" + n + "':" + i(t[n]));
    return "{" + e.join(",") + "}"
}

function countAllProvincePages() {
    return provinceAllTotalPage = Math.ceil(allProvinces.length / pa_pageSize)
}

function viewAllProvince(t) {
    var e, i;
    $(".provinceAll .city-list ul li").remove(), 1 == t ? $(".provinceAll .pre a").removeClass("can") : $(".provinceAll .pre a").addClass("can"), $(".provinceAll .next a").addClass("can"), t == provinceAllTotalPage ? (i = (t - 1) * pa_pageSize, e = allProvinces.length, $(".provinceAll .next a").removeClass("can")) : e = (i = (t - 1) * pa_pageSize) + pa_pageSize;
    for (var n = i; n < e; n++) if (allProvinces[n]) {
        var r = allProvinces[n].id, o = allProvinces[n].provinceName,
            s = $('<li><a style="background: none repeat scroll 0% 0% transparent; border: 0px none;" href="javascript:onclick=viewAllCities(' + n + ');" id="' + r + '">' + o + "</a></li>");
        $(".provinceAll .city-list ul").append(s)
    }
    $(".provinceAll .city-list #provincePage1").remove(), $(".provinceAll .city-list").append("<label id='provincePage1' style='display:none;'>" + t + "</label>")
}

function viewAllCities(t) {
    allProId = allProvinces[t].id, $("body").data("pAllName", allProvinces[t].provinceName), $("body").data("pAllId", allProId), allCitys = [];
    var i = 0;
    $.each(allCities, function (t, e) {
        e.provinceId == allProId && (allCitys[i++] = e)
    }), allCityTotalPage = Math.ceil(allCitys.length / pa_pageSize), $(".provinceCityAll").find(".tabs").find("a").removeClass("current"), $(".provinceCityAll .tabs").find("#cityAll").addClass("current"), $(".con .provinceAll .city-list a").removeClass("current"), $(".con .provinceAll .city-list a[id='" + allProId + "']").addClass("current"), $(".provinceCityAll").find(".con").children().hide(), $(".provinceCityAll").find(".con").find(".cityAll").show(), allCityPage(1)
}

function allCityPage(t) {
    var e, i;
    $(".cityAll .city-list ul li").empty(), $(".cityAll .city-list ul li").remove(), 1 == t ? $(".cityAll .pre a").removeClass("can") : $(".cityAll .pre a").addClass("can"), t <= 1 && (t = 1, $(".cityAll .pre a").removeClass("can"), $(".cityAll .next a").addClass("can")), 1 == allCityTotalPage && ($(".cityAll .next a").removeClass("can"), $(".cityAll .pre a").removeClass("can")), t >= allCityTotalPage ? (t = allCityTotalPage, $(".cityAll .next a").removeClass("can"), e = (t - 1) * pa_pageSize, i = allCitys.length) : 1 == t ? (i = (e = (t - 1) * pa_pageSize) + pa_pageSize, $(".cityAll .pre a").removeClass("can"), $(".cityAll .next a").addClass("can")) : (i = (e = (t - 1) * pa_pageSize) + pa_pageSize, $(".cityAll .next a").addClass("can"), $(".cityAll .pre a").addClass("can"));
    for (var n = e; n < i; n++) {
        var r = allCitys[n].id, o = allCitys[n].name.substr(0, 4),
            s = $('<li><a href="javascript:onclick=viewAllCounties(' + n + ')" id="' + r + '">' + o + "</a></li>");
        $(".cityAll .city-list ul").append(s)
    }
    $(".cityAll .city-list #cityPage1").remove(), $(".cityAll .city-list").append("<label id='cityPage1' style='display:none;'>" + t + "</label>")
}

function currentCounties() {
    var t = {name: "\u82cf\u5dde\u5e02", id: 320500, provinceId: 32e4, hotCity: !1};
    cityIdAll = t.id, $("body").data("cAllId", cityIdAll);
    var e = $.trim(t.name);
    $("body").data("nameOfCityAll", e);
    var i = $("body").data("allCountys");
    countiesAll = [];
    var n = 0;
    $.each(i, function (t, e) {
        e.cityId == cityIdAll && (countiesAll[n++] = e)
    }), countyTotalPageAll = Math.ceil(countiesAll.length / pa_pageSize), $(".provinceCityAll").find(".tabs").find("a").removeClass("current"), $(".provinceCityAll .tabs").find("#countyAll").addClass("current"), $(".con .cityAll .city-list a").removeClass("current"), $(".con .cityAll .city-list a[id='" + cityIdAll + "']").addClass("current"), $(".provinceCityAll").find(".con").children().hide(), $(".provinceCityAll").find(".con").find(".countyAll").show(), allCountyPage(1)
}

function viewAllCounties(t) {
    cityIdAll = allCitys[t].id, $("body").data("cAllId", cityIdAll);
    var e = $.trim(allCitys[t].name);
    $("body").data("nameOfCityAll", e), countiesAll = [];
    var i = 0;
    $.each(allAreas, function (t, e) {
        e.cityId == cityIdAll && (countiesAll[i++] = e)
    }), countyTotalPageAll = Math.ceil(countiesAll.length / pa_pageSize), $(".provinceCityAll").find(".tabs").find("a").removeClass("current"), $(".provinceCityAll .tabs").find("#countyAll").addClass("current"), $(".con .cityAll .city-list a").removeClass("current"), $(".con .cityAll .city-list a[id='" + cityIdAll + "']").addClass("current"), $(".provinceCityAll").find(".con").children().hide(), $(".provinceCityAll").find(".con").find(".countyAll").show(), allCountyPage(1)
}

function allCountyPage(t) {
    var e, i;
    $("body").data("pAllName"), $("body").data("nameOfCityAll");
    $(".countyAll .city-list ul li").remove(), 1 == t ? $(".countyAll .pre a").removeClass("can") : $(".countyAll .pre a").addClass("can"), t <= 1 && (t = 1, $(".countyAll .pre a").removeClass("can"), $(".countyAll .next a").addClass("can")), 1 == countyTotalPageAll && ($(".countyAll .next a").removeClass("can"), $(".countyAll .pre a").removeClass("can")), t >= countyTotalPageAll ? (t = countyTotalPageAll, $(".countyAll .next a").removeClass("can"), e = (t - 1) * pa_pageSize, i = countiesAll.length) : 1 == t ? (i = (e = (t - 1) * pa_pageSize) + pa_pageSize, $(".countyAll .pre a").removeClass("can"), $(".countyAll .next a").addClass("can")) : (i = (e = (t - 1) * pa_pageSize) + pa_pageSize, $(".countyAll .next a").addClass("can"), $(".countyAll .pre a").addClass("can"));
    for (var n = e; n < i; n++) {
        var r, o = countiesAll[n].id, s = countiesAll[n].areaName.substr(0, 4);
        r = $("#countyAll").text().trim() == s ? $('<li><a class="current" href="javascript:onclick=addrInputAll(' + n + ')" id="' + o + '">' + s + "</a></li>") : $('<li><a href="javascript:onclick=addrInputAll(' + n + ')" id="' + o + '">' + s + "</a></li>"), $(".countyAll .city-list ul").append(r)
    }
    $(".countyAll .city-list #countyPage1").remove(), $(".countyAll .city-list").append("<label id='countyPage1' style='display:none;'>" + t + "</label>")
}

function addrInputAll(t) {
    var i = $.trim(countiesAll[t].id);
    $(".con .hotCityAll .city-list a input").removeClass("current"), $(".con .hotCityAll .city-list a input[id='" + cityIdAll + "']").addClass("current"), $(".con .countyAll .city-list a").removeClass("current"), $(".con .countyAll .city-list a[id='" + i + "']").addClass("current"), allProId = $("body").data("pAllId"), cityIdAll = $("body").data("cAllId");
    $.each(allProvinces, function (t, e) {
        if (e.id == allProId) return e.provinceName, !1
    });
    $.each(allCities, function (t, e) {
        if (e.id == cityIdAll) return e.name, !1
    });
    var n = null;
    $.each(countiesAll, function (t, e) {
        if (e.id == i) return n = e.areaName, !1
    }), (e = $("input.current2")).removeClass("iGrays"), $(".provinceCityAll").hide();
    var e, r = "\u6c5f\u82cf / \u82cf\u5dde\u5e02 / " + n;
    $("input#location").val(r), $("div#location_div").html(r + ' <i class="sanjiao"></i>'), $("#countyAll").text(n), $("#current_county_id").val(i), $("#current_county_id").change(), $(".backifname").hide(), "consignor.addrProCity" == (e = $("input.current2").attr("name")) && ($("#provinceId").val(allProId), $("#cityId").val(cityIdAll)), "order.caddrProCity" == e && $("input[name='order.caddrProCity']").trigger("change"), "consigneeInfo.addrProCity" == e && $("input[name='consigneeInfo.addrProCity']").trigger("change"), "template.caddrProCity" == e && $("input[name='template.caddrProCity']").trigger("change")
}

function hotCityAddrInputAll(t) {
    allProId = t.split(",")[0], cityIdAll = t.split(",")[1];
    var e = t.split(",")[2];
    $("body").data("nameOfCityAll", e), $("body").data("pAllId", allProId), $("body").data("cAllId", cityIdAll), $.each(allProvinces, function (t, e) {
        e.id == allProId && $("body").data("pAllName", e.provinceName)
    }), countiesAll = [];
    var i = 0;
    $.each(allAreas, function (t, e) {
        e.cityId == cityIdAll && (countiesAll[i++] = e)
    }), countyTotalPageAll = Math.ceil(countiesAll.length / pa_pageSize), $(".provinceCityAll").find(".tabs").find("a").removeClass("current"), $(".provinceCityAll .tabs").find("#countyAll").addClass("current"), $(".con .cityAll .city-list a").removeClass("current"), $(".con .cityAll .city-list a[id='" + cityIdAll + "']").addClass("current"), $(".provinceCityAll").find(".con").children().hide(), $(".provinceCityAll").find(".con").find(".countyAll").show(), $(".con .provinceAll .city-list a").removeClass("current"), allCountyPage(1)
}

function MouseEvent(t) {
    this.x = t.pageX, this.y = t.pageY
}

function search_agents(t) {
    return $.ajax({
        url: "/commodities/" + t + "/get_city_agents?area_id=" + $("#area").val() + "&key_word=" + encodeURI($("#key_word").val()),
        success: function (t) {
            dialog.get("select_agent").content(t)
        },
        cache: !1
    }), !1
}

function select_agents(t) {
    if (0 == $("#current_county_id").length || "" == $("#current_county_id").val()) return alert("\u8bf7\u9009\u62e9\u5730\u533a\uff01"), !1;
    var e = dialog({id: "select_agent", title: "\u9009\u62e9\u4f9b\u5e94\u5546", width: 760, height: 420, padding: 20});
    e.show(), $.ajax({
        url: "/commodities/" + t + "/get_city_agents?area_id=" + $("#area").val(), success: function (t) {
            e.content(t)
        }, cache: !1
    })
}

function after_select(t, e, i) {
    $("#agent").next("p").html(i), $("#add_to_cart").text("\u52a0\u5165\u8d2d\u7269\u8f66");
    var n = "/cart/change/" + e + "?num=" + $(".list_increase_num").prev("input").val();
    $("#add_to_cart").attr("href", n).removeClass("shopbuttonnone").addClass("orderbutton"), dialog.get("select_agent").close(), window.location.href = "/commodities/" + t + "?p_id=" + e + "&show_emall=ok", $.ajax({
        url: "/commodities/get_product_info?product_id=" + e,
        type: "GET",
        dataType: "script"
    }).done(function () {
        $("#tagContent1 .tagHtml td").removeAttr("class").addClass("p12 w100"), $("#tagContent1 .tagHtml th").removeAttr("class").addClass("p12"), $("#tagContent0 .tagHtml div").removeAttr("class").addClass("lh23"), $("#tagContent1 .tagHtml table").attr("border", 1), $("#tagContent2 .tagHtml table").attr("border", 1), $("#tagContent2 .tagHtml td").removeAttr("class").addClass("p12 w100"), $("#tagContent2 .tagHtml th").removeAttr("class").addClass("p12")
    })
}

function SDMenu(t) {
    if (!document.getElementById || !document.getElementsByTagName) return !1;
    this.menu = document.getElementById(t), this.submenus = this.menu.getElementsByTagName("div"), this.remember = !0, this.speed = 10, this.markCurrent = !0, this.oneSmOnly = !1
}

function preview(t) {
    $("#preview .jqzoom img").attr("src", $(t).attr("src")), $("#preview .jqzoom img").attr("jqimg", $(t).attr("bimg"))
}

!function (t, e) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function (t) {
        if (!t.document) throw new Error("jQuery requires a window with a document");
        return e(t)
    } : e(t)
}("undefined" != typeof window ? window : this, function (g, t) {
    function a(t) {
        var e = t.length, i = rt.type(t);
        return "function" !== i && !rt.isWindow(t) && (!(1 !== t.nodeType || !e) || ("array" === i || 0 === e || "number" == typeof e && 0 < e && e - 1 in t))
    }

    function e(t, i, n) {
        if (rt.isFunction(i)) return rt.grep(t, function (t, e) {
            return !!i.call(t, e, t) !== n
        });
        if (i.nodeType) return rt.grep(t, function (t) {
            return t === i !== n
        });
        if ("string" == typeof i) {
            if (ut.test(i)) return rt.filter(i, t, n);
            i = rt.filter(i, t)
        }
        return rt.grep(t, function (t) {
            return 0 <= rt.inArray(t, i) !== n
        })
    }

    function i(t, e) {
        for (; (t = t[e]) && 1 !== t.nodeType;) ;
        return t
    }

    function u(t) {
        var i = bt[t] = {};
        return rt.each(t.match(xt) || [], function (t, e) {
            i[e] = !0
        }), i
    }

    function r() {
        ft.addEventListener ? (ft.removeEventListener("DOMContentLoaded", o, !1), g.removeEventListener("load", o, !1)) : (ft.detachEvent("onreadystatechange", o), g.detachEvent("onload", o))
    }

    function o() {
        (ft.addEventListener || "load" === event.type || "complete" === ft.readyState) && (r(), rt.ready())
    }

    function l(t, e, i) {
        if (i === undefined && 1 === t.nodeType) {
            var n = "data-" + e.replace(kt, "-$1").toLowerCase();
            if ("string" == typeof (i = t.getAttribute(n))) {
                try {
                    i = "true" === i || "false" !== i && ("null" === i ? null : +i + "" === i ? +i : _t.test(i) ? rt.parseJSON(i) : i)
                } catch (r) {
                }
                rt.data(t, e, i)
            } else i = undefined
        }
        return i
    }

    function c(t) {
        var e;
        for (e in t) if (("data" !== e || !rt.isEmptyObject(t[e])) && "toJSON" !== e) return !1;
        return !0
    }

    function n(t, e, i, n) {
        if (rt.acceptData(t)) {
            var r, o, s = rt.expando, a = t.nodeType, l = a ? rt.cache : t, c = a ? t[s] : t[s] && s;
            if (c && l[c] && (n || l[c].data) || i !== undefined || "string" != typeof e) return c || (c = a ? t[s] = V.pop() || rt.guid++ : s), l[c] || (l[c] = a ? {} : {toJSON: rt.noop}), "object" != typeof e && "function" != typeof e || (n ? l[c] = rt.extend(l[c], e) : l[c].data = rt.extend(l[c].data, e)), o = l[c], n || (o.data || (o.data = {}), o = o.data), i !== undefined && (o[rt.camelCase(e)] = i), "string" == typeof e ? null == (r = o[e]) && (r = o[rt.camelCase(e)]) : r = o, r
        }
    }

    function s(t, e, i) {
        if (rt.acceptData(t)) {
            var n, r, o = t.nodeType, s = o ? rt.cache : t, a = o ? t[rt.expando] : rt.expando;
            if (s[a]) {
                if (e && (n = i ? s[a] : s[a].data)) {
                    r = (e = rt.isArray(e) ? e.concat(rt.map(e, rt.camelCase)) : e in n ? [e] : (e = rt.camelCase(e)) in n ? [e] : e.split(" ")).length;
                    for (; r--;) delete n[e[r]];
                    if (i ? !c(n) : !rt.isEmptyObject(n)) return
                }
                (i || (delete s[a].data, c(s[a]))) && (o ? rt.cleanData([t], !0) : it.deleteExpando || s != s.window ? delete s[a] : s[a] = null)
            }
        }
    }

    function h() {
        return !0
    }

    function d() {
        return !1
    }

    function p() {
        try {
            return ft.activeElement
        } catch (t) {
        }
    }

    function m(t) {
        var e = Dt.split("|"), i = t.createDocumentFragment();
        if (i.createElement) for (; e.length;) i.createElement(e.pop());
        return i
    }

    function v(t, e) {
        var i, n, r = 0,
            o = typeof t.getElementsByTagName !== Ct ? t.getElementsByTagName(e || "*") : typeof t.querySelectorAll !== Ct ? t.querySelectorAll(e || "*") : undefined;
        if (!o) for (o = [], i = t.childNodes || t; null != (n = i[r]); r++) !e || rt.nodeName(n, e) ? o.push(n) : rt.merge(o, v(n, e));
        return e === undefined || e && rt.nodeName(t, e) ? rt.merge([t], o) : o
    }

    function y(t) {
        Mt.test(t.type) && (t.defaultChecked = t.checked)
    }

    function f(t, e) {
        return rt.nodeName(t, "table") && rt.nodeName(11 !== e.nodeType ? e : e.firstChild, "tr") ? t.getElementsByTagName("tbody")[0] || t.appendChild(t.ownerDocument.createElement("tbody")) : t
    }

    function x(t) {
        return t.type = (null !== rt.find.attr(t, "type")) + "/" + t.type, t
    }

    function b(t) {
        var e = Xt.exec(t.type);
        return e ? t.type = e[1] : t.removeAttribute("type"), t
    }

    function w(t, e) {
        for (var i, n = 0; null != (i = t[n]); n++) rt._data(i, "globalEval", !e || rt._data(e[n], "globalEval"))
    }

    function C(t, e) {
        if (1 === e.nodeType && rt.hasData(t)) {
            var i, n, r, o = rt._data(t), s = rt._data(e, o), a = o.events;
            if (a) for (i in delete s.handle, s.events = {}, a) for (n = 0, r = a[i].length; n < r; n++) rt.event.add(e, i, a[i][n]);
            s.data && (s.data = rt.extend({}, s.data))
        }
    }

    function _(t, e) {
        var i, n, r;
        if (1 === e.nodeType) {
            if (i = e.nodeName.toLowerCase(), !it.noCloneEvent && e[rt.expando]) {
                for (n in(r = rt._data(e)).events) rt.removeEvent(e, n, r.handle);
                e.removeAttribute(rt.expando)
            }
            "script" === i && e.text !== t.text ? (x(e).text = t.text, b(e)) : "object" === i ? (e.parentNode && (e.outerHTML = t.outerHTML), it.html5Clone && t.innerHTML && !rt.trim(e.innerHTML) && (e.innerHTML = t.innerHTML)) : "input" === i && Mt.test(t.type) ? (e.defaultChecked = e.checked = t.checked, e.value !== t.value && (e.value = t.value)) : "option" === i ? e.defaultSelected = e.selected = t.defaultSelected : "input" !== i && "textarea" !== i || (e.defaultValue = t.defaultValue)
        }
    }

    function k(t, e) {
        var i, n = rt(e.createElement(t)).appendTo(e.body),
            r = g.getDefaultComputedStyle && (i = g.getDefaultComputedStyle(n[0])) ? i.display : rt.css(n[0], "display");
        return n.detach(), r
    }

    function S(t) {
        var e = ft, i = Zt[t];
        return i || ("none" !== (i = k(t, e)) && i || ((e = ((Qt = (Qt || rt("<iframe frameborder='0' width='0' height='0'/>")).appendTo(e.documentElement))[0].contentWindow || Qt[0].contentDocument).document).write(), e.close(), i = k(t, e), Qt.detach()), Zt[t] = i), i
    }

    function T(e, i) {
        return {
            get: function () {
                var t = e();
                if (null != t) {
                    if (!t) return (this.get = i).apply(this, arguments);
                    delete this.get
                }
            }
        }
    }

    function A(t, e) {
        if (e in t) return e;
        for (var i = e.charAt(0).toUpperCase() + e.slice(1), n = e, r = de.length; r--;) if ((e = de[r] + i) in t) return e;
        return n
    }

    function $(t, e) {
        for (var i, n, r, o = [], s = 0, a = t.length; s < a; s++) (n = t[s]).style && (o[s] = rt._data(n, "olddisplay"), i = n.style.display, e ? (o[s] || "none" !== i || (n.style.display = ""), "" === n.style.display && At(n) && (o[s] = rt._data(n, "olddisplay", S(n.nodeName)))) : (r = At(n), (i && "none" !== i || !r) && rt._data(n, "olddisplay", r ? i : rt.css(n, "display"))));
        for (s = 0; s < a; s++) (n = t[s]).style && (e && "none" !== n.style.display && "" !== n.style.display || (n.style.display = e ? o[s] || "" : "none"));
        return t
    }

    function M(t, e, i) {
        var n = ae.exec(e);
        return n ? Math.max(0, n[1] - (i || 0)) + (n[2] || "px") : e
    }

    function E(t, e, i, n, r) {
        for (var o = i === (n ? "border" : "content") ? 4 : "width" === e ? 1 : 0, s = 0; o < 4; o += 2) "margin" === i && (s += rt.css(t, i + Tt[o], !0, r)), n ? ("content" === i && (s -= rt.css(t, "padding" + Tt[o], !0, r)), "margin" !== i && (s -= rt.css(t, "border" + Tt[o] + "Width", !0, r))) : (s += rt.css(t, "padding" + Tt[o], !0, r), "padding" !== i && (s += rt.css(t, "border" + Tt[o] + "Width", !0, r)));
        return s
    }

    function P(t, e, i) {
        var n = !0, r = "width" === e ? t.offsetWidth : t.offsetHeight, o = Jt(t),
            s = it.boxSizing && "border-box" === rt.css(t, "boxSizing", !1, o);
        if (r <= 0 || null == r) {
            if (((r = te(t, e, o)) < 0 || null == r) && (r = t.style[e]), ie.test(r)) return r;
            n = s && (it.boxSizingReliable() || r === t.style[e]), r = parseFloat(r) || 0
        }
        return r + E(t, e, i || (s ? "border" : "content"), n, o) + "px"
    }

    function L(t, e, i, n, r) {
        return new L.prototype.init(t, e, i, n, r)
    }

    function I() {
        return setTimeout(function () {
            ue = undefined
        }), ue = rt.now()
    }

    function F(t, e) {
        var i, n = {height: t}, r = 0;
        for (e = e ? 1 : 0; r < 4; r += 2 - e) n["margin" + (i = Tt[r])] = n["padding" + i] = t;
        return e && (n.opacity = n.width = t), n
    }

    function D(t, e, i) {
        for (var n, r = (_e[e] || []).concat(_e["*"]), o = 0, s = r.length; o < s; o++) if (n = r[o].call(i, e, t)) return n
    }

    function O(e, t, i) {
        var n, r, o, s, a, l, c, h = this, d = {}, u = e.style, p = e.nodeType && At(e), f = rt._data(e, "fxshow");
        for (n in i.queue || (null == (a = rt._queueHooks(e, "fx")).unqueued && (a.unqueued = 0, l = a.empty.fire, a.empty.fire = function () {
            a.unqueued || l()
        }), a.unqueued++, h.always(function () {
            h.always(function () {
                a.unqueued--, rt.queue(e, "fx").length || a.empty.fire()
            })
        })), 1 === e.nodeType && ("height" in t || "width" in t) && (i.overflow = [u.overflow, u.overflowX, u.overflowY], "inline" === ("none" === (c = rt.css(e, "display")) ? rt._data(e, "olddisplay") || S(e.nodeName) : c) && "none" === rt.css(e, "float") && (it.inlineBlockNeedsLayout && "inline" !== S(e.nodeName) ? u.zoom = 1 : u.display = "inline-block")), i.overflow && (u.overflow = "hidden", it.shrinkWrapBlocks() || h.always(function () {
            u.overflow = i.overflow[0], u.overflowX = i.overflow[1], u.overflowY = i.overflow[2]
        })), t) if (r = t[n], xe.exec(r)) {
            if (delete t[n], o = o || "toggle" === r, r === (p ? "hide" : "show")) {
                if ("show" !== r || !f || f[n] === undefined) continue;
                p = !0
            }
            d[n] = f && f[n] || rt.style(e, n)
        } else c = undefined;
        if (rt.isEmptyObject(d)) "inline" === ("none" === c ? S(e.nodeName) : c) && (u.display = c); else for (n in f ? "hidden" in f && (p = f.hidden) : f = rt._data(e, "fxshow", {}), o && (f.hidden = !p), p ? rt(e).show() : h.done(function () {
            rt(e).hide()
        }), h.done(function () {
            var t;
            for (t in rt._removeData(e, "fxshow"), d) rt.style(e, t, d[t])
        }), d) s = D(p ? f[n] : 0, n, h), n in f || (f[n] = s.start, p && (s.end = s.start, s.start = "width" === n || "height" === n ? 1 : 0))
    }

    function N(t, e) {
        var i, n, r, o, s;
        for (i in t) if (r = e[n = rt.camelCase(i)], o = t[i], rt.isArray(o) && (r = o[1], o = t[i] = o[0]), i !== n && (t[n] = o, delete t[i]), (s = rt.cssHooks[n]) && "expand" in s) for (i in o = s.expand(o), delete t[n], o) i in t || (t[i] = o[i], e[i] = r); else e[n] = r
    }

    function j(o, t, e) {
        var i, s, n = 0, r = Ce.length, a = rt.Deferred().always(function () {
            delete l.elem
        }), l = function () {
            if (s) return !1;
            for (var t = ue || I(), e = Math.max(0, c.startTime + c.duration - t), i = 1 - (e / c.duration || 0), n = 0, r = c.tweens.length; n < r; n++) c.tweens[n].run(i);
            return a.notifyWith(o, [c, i, e]), i < 1 && r ? e : (a.resolveWith(o, [c]), !1)
        }, c = a.promise({
            elem: o,
            props: rt.extend({}, t),
            opts: rt.extend(!0, {specialEasing: {}}, e),
            originalProperties: t,
            originalOptions: e,
            startTime: ue || I(),
            duration: e.duration,
            tweens: [],
            createTween: function (t, e) {
                var i = rt.Tween(o, c.opts, t, e, c.opts.specialEasing[t] || c.opts.easing);
                return c.tweens.push(i), i
            },
            stop: function (t) {
                var e = 0, i = t ? c.tweens.length : 0;
                if (s) return this;
                for (s = !0; e < i; e++) c.tweens[e].run(1);
                return t ? a.resolveWith(o, [c, t]) : a.rejectWith(o, [c, t]), this
            }
        }), h = c.props;
        for (N(h, c.opts.specialEasing); n < r; n++) if (i = Ce[n].call(c, o, h, c.opts)) return i;
        return rt.map(h, D, c), rt.isFunction(c.opts.start) && c.opts.start.call(o, c), rt.fx.timer(rt.extend(l, {
            elem: o,
            anim: c,
            queue: c.opts.queue
        })), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
    }

    function z(o) {
        return function (t, e) {
            "string" != typeof t && (e = t, t = "*");
            var i, n = 0, r = t.toLowerCase().match(xt) || [];
            if (rt.isFunction(e)) for (; i = r[n++];) "+" === i.charAt(0) ? (i = i.slice(1) || "*", (o[i] = o[i] || []).unshift(e)) : (o[i] = o[i] || []).push(e)
        }
    }

    function R(e, r, o, s) {
        function a(t) {
            var n;
            return l[t] = !0, rt.each(e[t] || [], function (t, e) {
                var i = e(r, o, s);
                return "string" != typeof i || c || l[i] ? c ? !(n = i) : void 0 : (r.dataTypes.unshift(i), a(i), !1)
            }), n
        }

        var l = {}, c = e === Ye;
        return a(r.dataTypes[0]) || !l["*"] && a("*")
    }

    function B(t, e) {
        var i, n, r = rt.ajaxSettings.flatOptions || {};
        for (n in e) e[n] !== undefined && ((r[n] ? t : i || (i = {}))[n] = e[n]);
        return i && rt.extend(!0, t, i), t
    }

    function H(t, e, i) {
        for (var n, r, o, s, a = t.contents, l = t.dataTypes; "*" === l[0];) l.shift(), r === undefined && (r = t.mimeType || e.getResponseHeader("Content-Type"));
        if (r) for (s in a) if (a[s] && a[s].test(r)) {
            l.unshift(s);
            break
        }
        if (l[0] in i) o = l[0]; else {
            for (s in i) {
                if (!l[0] || t.converters[s + " " + l[0]]) {
                    o = s;
                    break
                }
                n || (n = s)
            }
            o = o || n
        }
        if (o) return o !== l[0] && l.unshift(o), i[o]
    }

    function W(t, e, i, n) {
        var r, o, s, a, l, c = {}, h = t.dataTypes.slice();
        if (h[1]) for (s in t.converters) c[s.toLowerCase()] = t.converters[s];
        for (o = h.shift(); o;) if (t.responseFields[o] && (i[t.responseFields[o]] = e), !l && n && t.dataFilter && (e = t.dataFilter(e, t.dataType)), l = o, o = h.shift()) if ("*" === o) o = l; else if ("*" !== l && l !== o) {
            if (!(s = c[l + " " + o] || c["* " + o])) for (r in c) if ((a = r.split(" "))[1] === o && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
                !0 === s ? s = c[r] : !0 !== c[r] && (o = a[0], h.unshift(a[1]));
                break
            }
            if (!0 !== s) if (s && t["throws"]) e = s(e); else try {
                e = s(e)
            } catch (d) {
                return {state: "parsererror", error: s ? d : "No conversion from " + l + " to " + o}
            }
        }
        return {state: "success", data: e}
    }

    function q(i, t, n, r) {
        var e;
        if (rt.isArray(t)) rt.each(t, function (t, e) {
            n || Qe.test(i) ? r(i, e) : q(i + "[" + ("object" == typeof e ? t : "") + "]", e, n, r)
        }); else if (n || "object" !== rt.type(t)) r(i, t); else for (e in t) q(i + "[" + e + "]", t[e], n, r)
    }

    function G() {
        try {
            return new g.XMLHttpRequest
        } catch (t) {
        }
    }

    function X() {
        try {
            return new g.ActiveXObject("Microsoft.XMLHTTP")
        } catch (t) {
        }
    }

    function Y(t) {
        return rt.isWindow(t) ? t : 9 === t.nodeType && (t.defaultView || t.parentWindow)
    }

    var V = [], U = V.slice, Q = V.concat, K = V.push, Z = V.indexOf, J = {}, tt = J.toString, et = J.hasOwnProperty,
        it = {}, nt = "1.11.1", rt = function (t, e) {
            return new rt.fn.init(t, e)
        }, ot = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, st = /^-ms-/, at = /-([\da-z])/gi, lt = function (t, e) {
            return e.toUpperCase()
        };
    rt.fn = rt.prototype = {
        jquery: nt, constructor: rt, selector: "", length: 0, toArray: function () {
            return U.call(this)
        }, get: function (t) {
            return null != t ? t < 0 ? this[t + this.length] : this[t] : U.call(this)
        }, pushStack: function (t) {
            var e = rt.merge(this.constructor(), t);
            return e.prevObject = this, e.context = this.context, e
        }, each: function (t, e) {
            return rt.each(this, t, e)
        }, map: function (i) {
            return this.pushStack(rt.map(this, function (t, e) {
                return i.call(t, e, t)
            }))
        }, slice: function () {
            return this.pushStack(U.apply(this, arguments))
        }, first: function () {
            return this.eq(0)
        }, last: function () {
            return this.eq(-1)
        }, eq: function (t) {
            var e = this.length, i = +t + (t < 0 ? e : 0);
            return this.pushStack(0 <= i && i < e ? [this[i]] : [])
        }, end: function () {
            return this.prevObject || this.constructor(null)
        }, push: K, sort: V.sort, splice: V.splice
    }, rt.extend = rt.fn.extend = function (t) {
        var e, i, n, r, o, s, a = t || {}, l = 1, c = arguments.length, h = !1;
        for ("boolean" == typeof a && (h = a, a = arguments[l] || {}, l++), "object" == typeof a || rt.isFunction(a) || (a = {}), l === c && (a = this, l--); l < c; l++) if (null != (o = arguments[l])) for (r in o) e = a[r], a !== (n = o[r]) && (h && n && (rt.isPlainObject(n) || (i = rt.isArray(n))) ? (i ? (i = !1, s = e && rt.isArray(e) ? e : []) : s = e && rt.isPlainObject(e) ? e : {}, a[r] = rt.extend(h, s, n)) : n !== undefined && (a[r] = n));
        return a
    }, rt.extend({
        expando: "jQuery" + (nt + Math.random()).replace(/\D/g, ""), isReady: !0, error: function (t) {
            throw new Error(t)
        }, noop: function () {
        }, isFunction: function (t) {
            return "function" === rt.type(t)
        }, isArray: Array.isArray || function (t) {
            return "array" === rt.type(t)
        }, isWindow: function (t) {
            return null != t && t == t.window
        }, isNumeric: function (t) {
            return !rt.isArray(t) && 0 <= t - parseFloat(t)
        }, isEmptyObject: function (t) {
            var e;
            for (e in t) return !1;
            return !0
        }, isPlainObject: function (t) {
            var e;
            if (!t || "object" !== rt.type(t) || t.nodeType || rt.isWindow(t)) return !1;
            try {
                if (t.constructor && !et.call(t, "constructor") && !et.call(t.constructor.prototype, "isPrototypeOf")) return !1
            } catch (i) {
                return !1
            }
            if (it.ownLast) for (e in t) return et.call(t, e);
            for (e in t) ;
            return e === undefined || et.call(t, e)
        }, type: function (t) {
            return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? J[tt.call(t)] || "object" : typeof t
        }, globalEval: function (t) {
            t && rt.trim(t) && (g.execScript || function (t) {
                g.eval.call(g, t)
            })(t)
        }, camelCase: function (t) {
            return t.replace(st, "ms-").replace(at, lt)
        }, nodeName: function (t, e) {
            return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase()
        }, each: function (t, e, i) {
            var n = 0, r = t.length, o = a(t);
            if (i) {
                if (o) for (; n < r && !1 !== e.apply(t[n], i); n++) ; else for (n in t) if (!1 === e.apply(t[n], i)) break
            } else if (o) for (; n < r && !1 !== e.call(t[n], n, t[n]); n++) ; else for (n in t) if (!1 === e.call(t[n], n, t[n])) break;
            return t
        }, trim: function (t) {
            return null == t ? "" : (t + "").replace(ot, "")
        }, makeArray: function (t, e) {
            var i = e || [];
            return null != t && (a(Object(t)) ? rt.merge(i, "string" == typeof t ? [t] : t) : K.call(i, t)), i
        }, inArray: function (t, e, i) {
            var n;
            if (e) {
                if (Z) return Z.call(e, t, i);
                for (n = e.length, i = i ? i < 0 ? Math.max(0, n + i) : i : 0; i < n; i++) if (i in e && e[i] === t) return i
            }
            return -1
        }, merge: function (t, e) {
            for (var i = +e.length, n = 0, r = t.length; n < i;) t[r++] = e[n++];
            if (i != i) for (; e[n] !== undefined;) t[r++] = e[n++];
            return t.length = r, t
        }, grep: function (t, e, i) {
            for (var n = [], r = 0, o = t.length, s = !i; r < o; r++) !e(t[r], r) !== s && n.push(t[r]);
            return n
        }, map: function (t, e, i) {
            var n, r = 0, o = t.length, s = [];
            if (a(t)) for (; r < o; r++) null != (n = e(t[r], r, i)) && s.push(n); else for (r in t) null != (n = e(t[r], r, i)) && s.push(n);
            return Q.apply([], s)
        }, guid: 1, proxy: function (t, e) {
            var i, n, r;
            return "string" == typeof e && (r = t[e], e = t, t = r), rt.isFunction(t) ? (i = U.call(arguments, 2), (n = function () {
                return t.apply(e || this, i.concat(U.call(arguments)))
            }).guid = t.guid = t.guid || rt.guid++, n) : undefined
        }, now: function () {
            return +new Date
        }, support: it
    }), rt.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (t, e) {
        J["[object " + e + "]"] = e.toLowerCase()
    });
    var ct = function (i) {
        function b(t, e, i, n) {
            var r, o, s, a, l, c, h, d, u, p;
            if ((e ? e.ownerDocument || e : R) !== L && P(e), i = i || [], !t || "string" != typeof t) return i;
            if (1 !== (a = (e = e || L).nodeType) && 9 !== a) return [];
            if (F && !n) {
                if (r = yt.exec(t)) if (s = r[1]) {
                    if (9 === a) {
                        if (!(o = e.getElementById(s)) || !o.parentNode) return i;
                        if (o.id === s) return i.push(o), i
                    } else if (e.ownerDocument && (o = e.ownerDocument.getElementById(s)) && j(e, o) && o.id === s) return i.push(o), i
                } else {
                    if (r[2]) return J.apply(i, e.getElementsByTagName(t)), i;
                    if ((s = r[3]) && v.getElementsByClassName && e.getElementsByClassName) return J.apply(i, e.getElementsByClassName(s)), i
                }
                if (v.qsa && (!D || !D.test(t))) {
                    if (d = h = z, u = e, p = 9 === a && t, 1 === a && "object" !== e.nodeName.toLowerCase()) {
                        for (c = S(t), (h = e.getAttribute("id")) ? d = h.replace(bt, "\\$&") : e.setAttribute("id", d), d = "[id='" + d + "'] ", l = c.length; l--;) c[l] = d + m(c[l]);
                        u = xt.test(t) && g(e.parentNode) || e, p = c.join(",")
                    }
                    if (p) try {
                        return J.apply(i, u.querySelectorAll(p)), i
                    } catch (f) {
                    } finally {
                        h || e.removeAttribute("id")
                    }
                }
            }
            return A(t.replace(lt, "$1"), e, i, n)
        }

        function t() {
            function i(t, e) {
                return n.push(t + " ") > C.cacheLength && delete i[n.shift()], i[t + " "] = e
            }

            var n = [];
            return i
        }

        function l(t) {
            return t[z] = !0, t
        }

        function n(t) {
            var e = L.createElement("div");
            try {
                return !!t(e)
            } catch (i) {
                return !1
            } finally {
                e.parentNode && e.parentNode.removeChild(e), e = null
            }
        }

        function e(t, e) {
            for (var i = t.split("|"), n = t.length; n--;) C.attrHandle[i[n]] = e
        }

        function c(t, e) {
            var i = e && t,
                n = i && 1 === t.nodeType && 1 === e.nodeType && (~e.sourceIndex || V) - (~t.sourceIndex || V);
            if (n) return n;
            if (i) for (; i = i.nextSibling;) if (i === e) return -1;
            return t ? 1 : -1
        }

        function r(e) {
            return function (t) {
                return "input" === t.nodeName.toLowerCase() && t.type === e
            }
        }

        function o(i) {
            return function (t) {
                var e = t.nodeName.toLowerCase();
                return ("input" === e || "button" === e) && t.type === i
            }
        }

        function s(s) {
            return l(function (o) {
                return o = +o, l(function (t, e) {
                    for (var i, n = s([], t.length, o), r = n.length; r--;) t[i = n[r]] && (t[i] = !(e[i] = t[i]))
                })
            })
        }

        function g(t) {
            return t && typeof t.getElementsByTagName !== Y && t
        }

        function a() {
        }

        function m(t) {
            for (var e = 0, i = t.length, n = ""; e < i; e++) n += t[e].value;
            return n
        }

        function d(s, t, e) {
            var a = t.dir, l = e && "parentNode" === a, c = H++;
            return t.first ? function (t, e, i) {
                for (; t = t[a];) if (1 === t.nodeType || l) return s(t, e, i)
            } : function (t, e, i) {
                var n, r, o = [B, c];
                if (i) {
                    for (; t = t[a];) if ((1 === t.nodeType || l) && s(t, e, i)) return !0
                } else for (; t = t[a];) if (1 === t.nodeType || l) {
                    if ((n = (r = t[z] || (t[z] = {}))[a]) && n[0] === B && n[1] === c) return o[2] = n[2];
                    if ((r[a] = o)[2] = s(t, e, i)) return !0
                }
            }
        }

        function u(r) {
            return 1 < r.length ? function (t, e, i) {
                for (var n = r.length; n--;) if (!r[n](t, e, i)) return !1;
                return !0
            } : r[0]
        }

        function y(t, e, i) {
            for (var n = 0, r = e.length; n < r; n++) b(t, e[n], i);
            return i
        }

        function w(t, e, i, n, r) {
            for (var o, s = [], a = 0, l = t.length, c = null != e; a < l; a++) (o = t[a]) && (i && !i(o, n, r) || (s.push(o), c && e.push(a)));
            return s
        }

        function x(p, f, g, m, v, t) {
            return m && !m[z] && (m = x(m)), v && !v[z] && (v = x(v, t)), l(function (t, e, i, n) {
                var r, o, s, a = [], l = [], c = e.length, h = t || y(f || "*", i.nodeType ? [i] : i, []),
                    d = !p || !t && f ? h : w(h, a, p, i, n), u = g ? v || (t ? p : c || m) ? [] : e : d;
                if (g && g(d, u, i, n), m) for (r = w(u, l), m(r, [], i, n), o = r.length; o--;) (s = r[o]) && (u[l[o]] = !(d[l[o]] = s));
                if (t) {
                    if (v || p) {
                        if (v) {
                            for (r = [], o = u.length; o--;) (s = u[o]) && r.push(d[o] = s);
                            v(null, u = [], r, n)
                        }
                        for (o = u.length; o--;) (s = u[o]) && -1 < (r = v ? et.call(t, s) : a[o]) && (t[r] = !(e[r] = s))
                    }
                } else u = w(u === e ? u.splice(c, u.length) : u), v ? v(null, e, u, n) : J.apply(e, u)
            })
        }

        function p(t) {
            for (var n, e, i, r = t.length, o = C.relative[t[0].type], s = o || C.relative[" "], a = o ? 1 : 0, l = d(function (t) {
                return t === n
            }, s, !0), c = d(function (t) {
                return -1 < et.call(n, t)
            }, s, !0), h = [function (t, e, i) {
                return !o && (i || e !== $) || ((n = e).nodeType ? l(t, e, i) : c(t, e, i))
            }]; a < r; a++) if (e = C.relative[t[a].type]) h = [d(u(h), e)]; else {
                if ((e = C.filter[t[a].type].apply(null, t[a].matches))[z]) {
                    for (i = ++a; i < r && !C.relative[t[i].type]; i++) ;
                    return x(1 < a && u(h), 1 < a && m(t.slice(0, a - 1).concat({value: " " === t[a - 2].type ? "*" : ""})).replace(lt, "$1"), e, a < i && p(t.slice(a, i)), i < r && p(t = t.slice(i)), i < r && m(t))
                }
                h.push(e)
            }
            return u(h)
        }

        function h(m, v) {
            var y = 0 < v.length, x = 0 < m.length, t = function (t, e, i, n, r) {
                var o, s, a, l = 0, c = "0", h = t && [], d = [], u = $, p = t || x && C.find.TAG("*", r),
                    f = B += null == u ? 1 : Math.random() || .1, g = p.length;
                for (r && ($ = e !== L && e); c !== g && null != (o = p[c]); c++) {
                    if (x && o) {
                        for (s = 0; a = m[s++];) if (a(o, e, i)) {
                            n.push(o);
                            break
                        }
                        r && (B = f)
                    }
                    y && ((o = !a && o) && l--, t && h.push(o))
                }
                if (l += c, y && c !== l) {
                    for (s = 0; a = v[s++];) a(h, d, e, i);
                    if (t) {
                        if (0 < l) for (; c--;) h[c] || d[c] || (d[c] = K.call(n));
                        d = w(d)
                    }
                    J.apply(n, d), r && !t && 0 < d.length && 1 < l + v.length && b.uniqueSort(n)
                }
                return r && (B = f, $ = u), h
            };
            return y ? l(t) : t
        }

        var f, v, C, _, k, S, T, A, $, M, E, P, L, I, F, D, O, N, j, z = "sizzle" + -new Date, R = i.document, B = 0,
            H = 0, W = t(), q = t(), G = t(), X = function (t, e) {
                return t === e && (E = !0), 0
            }, Y = typeof undefined, V = 1 << 31, U = {}.hasOwnProperty, Q = [], K = Q.pop, Z = Q.push, J = Q.push,
            tt = Q.slice, et = Q.indexOf || function (t) {
                for (var e = 0, i = this.length; e < i; e++) if (this[e] === t) return e;
                return -1
            },
            it = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            nt = "[\\x20\\t\\r\\n\\f]", rt = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", ot = rt.replace("w", "w#"),
            st = "\\[" + nt + "*(" + rt + ")(?:" + nt + "*([*^$|!~]?=)" + nt + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + ot + "))|)" + nt + "*\\]",
            at = ":(" + rt + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + st + ")*)|.*)\\)|)",
            lt = new RegExp("^" + nt + "+|((?:^|[^\\\\])(?:\\\\.)*)" + nt + "+$", "g"),
            ct = new RegExp("^" + nt + "*," + nt + "*"), ht = new RegExp("^" + nt + "*([>+~]|" + nt + ")" + nt + "*"),
            dt = new RegExp("=" + nt + "*([^\\]'\"]*?)" + nt + "*\\]", "g"), ut = new RegExp(at),
            pt = new RegExp("^" + ot + "$"), ft = {
                ID: new RegExp("^#(" + rt + ")"),
                CLASS: new RegExp("^\\.(" + rt + ")"),
                TAG: new RegExp("^(" + rt.replace("w", "w*") + ")"),
                ATTR: new RegExp("^" + st),
                PSEUDO: new RegExp("^" + at),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + nt + "*(even|odd|(([+-]|)(\\d*)n|)" + nt + "*(?:([+-]|)" + nt + "*(\\d+)|))" + nt + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + it + ")$", "i"),
                needsContext: new RegExp("^" + nt + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + nt + "*((?:-\\d)?\\d*)" + nt + "*\\)|)(?=[^-]|$)", "i")
            }, gt = /^(?:input|select|textarea|button)$/i, mt = /^h\d$/i, vt = /^[^{]+\{\s*\[native \w/,
            yt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, xt = /[+~]/, bt = /'|\\/g,
            wt = new RegExp("\\\\([\\da-f]{1,6}" + nt + "?|(" + nt + ")|.)", "ig"), Ct = function (t, e, i) {
                var n = "0x" + e - 65536;
                return n != n || i ? e : n < 0 ? String.fromCharCode(n + 65536) : String.fromCharCode(n >> 10 | 55296, 1023 & n | 56320)
            };
        try {
            J.apply(Q = tt.call(R.childNodes), R.childNodes), Q[R.childNodes.length].nodeType
        } catch (_t) {
            J = {
                apply: Q.length ? function (t, e) {
                    Z.apply(t, tt.call(e))
                } : function (t, e) {
                    for (var i = t.length, n = 0; t[i++] = e[n++];) ;
                    t.length = i - 1
                }
            }
        }
        for (f in v = b.support = {}, k = b.isXML = function (t) {
            var e = t && (t.ownerDocument || t).documentElement;
            return !!e && "HTML" !== e.nodeName
        }, P = b.setDocument = function (t) {
            var e, l = t ? t.ownerDocument || t : R, i = l.defaultView;
            return l !== L && 9 === l.nodeType && l.documentElement ? (I = (L = l).documentElement, F = !k(l), i && i !== i.top && (i.addEventListener ? i.addEventListener("unload", function () {
                P()
            }, !1) : i.attachEvent && i.attachEvent("onunload", function () {
                P()
            })), v.attributes = n(function (t) {
                return t.className = "i", !t.getAttribute("className")
            }), v.getElementsByTagName = n(function (t) {
                return t.appendChild(l.createComment("")), !t.getElementsByTagName("*").length
            }), v.getElementsByClassName = vt.test(l.getElementsByClassName) && n(function (t) {
                return t.innerHTML = "<div class='a'></div><div class='a i'></div>", t.firstChild.className = "i", 2 === t.getElementsByClassName("i").length
            }), v.getById = n(function (t) {
                return I.appendChild(t).id = z, !l.getElementsByName || !l.getElementsByName(z).length
            }), v.getById ? (C.find.ID = function (t, e) {
                if (typeof e.getElementById !== Y && F) {
                    var i = e.getElementById(t);
                    return i && i.parentNode ? [i] : []
                }
            }, C.filter.ID = function (t) {
                var e = t.replace(wt, Ct);
                return function (t) {
                    return t.getAttribute("id") === e
                }
            }) : (delete C.find.ID, C.filter.ID = function (t) {
                var i = t.replace(wt, Ct);
                return function (t) {
                    var e = typeof t.getAttributeNode !== Y && t.getAttributeNode("id");
                    return e && e.value === i
                }
            }), C.find.TAG = v.getElementsByTagName ? function (t, e) {
                if (typeof e.getElementsByTagName !== Y) return e.getElementsByTagName(t)
            } : function (t, e) {
                var i, n = [], r = 0, o = e.getElementsByTagName(t);
                if ("*" !== t) return o;
                for (; i = o[r++];) 1 === i.nodeType && n.push(i);
                return n
            }, C.find.CLASS = v.getElementsByClassName && function (t, e) {
                if (typeof e.getElementsByClassName !== Y && F) return e.getElementsByClassName(t)
            }, O = [], D = [], (v.qsa = vt.test(l.querySelectorAll)) && (n(function (t) {
                t.innerHTML = "<select msallowclip=''><option selected=''></option></select>", t.querySelectorAll("[msallowclip^='']").length && D.push("[*^$]=" + nt + "*(?:''|\"\")"), t.querySelectorAll("[selected]").length || D.push("\\[" + nt + "*(?:value|" + it + ")"), t.querySelectorAll(":checked").length || D.push(":checked")
            }), n(function (t) {
                var e = l.createElement("input");
                e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("name", "D"), t.querySelectorAll("[name=d]").length && D.push("name" + nt + "*[*^$|!~]?="), t.querySelectorAll(":enabled").length || D.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), D.push(",.*:")
            })), (v.matchesSelector = vt.test(N = I.matches || I.webkitMatchesSelector || I.mozMatchesSelector || I.oMatchesSelector || I.msMatchesSelector)) && n(function (t) {
                v.disconnectedMatch = N.call(t, "div"), N.call(t, "[s!='']:x"), O.push("!=", at)
            }), D = D.length && new RegExp(D.join("|")), O = O.length && new RegExp(O.join("|")), e = vt.test(I.compareDocumentPosition), j = e || vt.test(I.contains) ? function (t, e) {
                var i = 9 === t.nodeType ? t.documentElement : t, n = e && e.parentNode;
                return t === n || !(!n || 1 !== n.nodeType || !(i.contains ? i.contains(n) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(n)))
            } : function (t, e) {
                if (e) for (; e = e.parentNode;) if (e === t) return !0;
                return !1
            }, X = e ? function (t, e) {
                if (t === e) return E = !0, 0;
                var i = !t.compareDocumentPosition - !e.compareDocumentPosition;
                return i || (1 & (i = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1) || !v.sortDetached && e.compareDocumentPosition(t) === i ? t === l || t.ownerDocument === R && j(R, t) ? -1 : e === l || e.ownerDocument === R && j(R, e) ? 1 : M ? et.call(M, t) - et.call(M, e) : 0 : 4 & i ? -1 : 1)
            } : function (t, e) {
                if (t === e) return E = !0, 0;
                var i, n = 0, r = t.parentNode, o = e.parentNode, s = [t], a = [e];
                if (!r || !o) return t === l ? -1 : e === l ? 1 : r ? -1 : o ? 1 : M ? et.call(M, t) - et.call(M, e) : 0;
                if (r === o) return c(t, e);
                for (i = t; i = i.parentNode;) s.unshift(i);
                for (i = e; i = i.parentNode;) a.unshift(i);
                for (; s[n] === a[n];) n++;
                return n ? c(s[n], a[n]) : s[n] === R ? -1 : a[n] === R ? 1 : 0
            }, l) : L
        }, b.matches = function (t, e) {
            return b(t, null, null, e)
        }, b.matchesSelector = function (t, e) {
            if ((t.ownerDocument || t) !== L && P(t), e = e.replace(dt, "='$1']"), v.matchesSelector && F && (!O || !O.test(e)) && (!D || !D.test(e))) try {
                var i = N.call(t, e);
                if (i || v.disconnectedMatch || t.document && 11 !== t.document.nodeType) return i
            } catch (_t) {
            }
            return 0 < b(e, L, null, [t]).length
        }, b.contains = function (t, e) {
            return (t.ownerDocument || t) !== L && P(t), j(t, e)
        }, b.attr = function (t, e) {
            (t.ownerDocument || t) !== L && P(t);
            var i = C.attrHandle[e.toLowerCase()],
                n = i && U.call(C.attrHandle, e.toLowerCase()) ? i(t, e, !F) : undefined;
            return n !== undefined ? n : v.attributes || !F ? t.getAttribute(e) : (n = t.getAttributeNode(e)) && n.specified ? n.value : null
        }, b.error = function (t) {
            throw new Error("Syntax error, unrecognized expression: " + t)
        }, b.uniqueSort = function (t) {
            var e, i = [], n = 0, r = 0;
            if (E = !v.detectDuplicates, M = !v.sortStable && t.slice(0), t.sort(X), E) {
                for (; e = t[r++];) e === t[r] && (n = i.push(r));
                for (; n--;) t.splice(i[n], 1)
            }
            return M = null, t
        }, _ = b.getText = function (t) {
            var e, i = "", n = 0, r = t.nodeType;
            if (r) {
                if (1 === r || 9 === r || 11 === r) {
                    if ("string" == typeof t.textContent) return t.textContent;
                    for (t = t.firstChild; t; t = t.nextSibling) i += _(t)
                } else if (3 === r || 4 === r) return t.nodeValue
            } else for (; e = t[n++];) i += _(e);
            return i
        }, (C = b.selectors = {
            cacheLength: 50,
            createPseudo: l,
            match: ft,
            attrHandle: {},
            find: {},
            relative: {
                ">": {dir: "parentNode", first: !0},
                " ": {dir: "parentNode"},
                "+": {dir: "previousSibling", first: !0},
                "~": {dir: "previousSibling"}
            },
            preFilter: {
                ATTR: function (t) {
                    return t[1] = t[1].replace(wt, Ct), t[3] = (t[3] || t[4] || t[5] || "").replace(wt, Ct), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4)
                }, CHILD: function (t) {
                    return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || b.error(t[0]), t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && b.error(t[0]), t
                }, PSEUDO: function (t) {
                    var e, i = !t[6] && t[2];
                    return ft.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : i && ut.test(i) && (e = S(i, !0)) && (e = i.indexOf(")", i.length - e) - i.length) && (t[0] = t[0].slice(0, e), t[2] = i.slice(0, e)), t.slice(0, 3))
                }
            },
            filter: {
                TAG: function (t) {
                    var e = t.replace(wt, Ct).toLowerCase();
                    return "*" === t ? function () {
                        return !0
                    } : function (t) {
                        return t.nodeName && t.nodeName.toLowerCase() === e
                    }
                }, CLASS: function (t) {
                    var e = W[t + " "];
                    return e || (e = new RegExp("(^|" + nt + ")" + t + "(" + nt + "|$)")) && W(t, function (t) {
                        return e.test("string" == typeof t.className && t.className || typeof t.getAttribute !== Y && t.getAttribute("class") || "")
                    })
                }, ATTR: function (i, n, r) {
                    return function (t) {
                        var e = b.attr(t, i);
                        return null == e ? "!=" === n : !n || (e += "", "=" === n ? e === r : "!=" === n ? e !== r : "^=" === n ? r && 0 === e.indexOf(r) : "*=" === n ? r && -1 < e.indexOf(r) : "$=" === n ? r && e.slice(-r.length) === r : "~=" === n ? -1 < (" " + e + " ").indexOf(r) : "|=" === n && (e === r || e.slice(0, r.length + 1) === r + "-"))
                    }
                }, CHILD: function (p, t, e, f, g) {
                    var m = "nth" !== p.slice(0, 3), v = "last" !== p.slice(-4), y = "of-type" === t;
                    return 1 === f && 0 === g ? function (t) {
                        return !!t.parentNode
                    } : function (t, e, i) {
                        var n, r, o, s, a, l, c = m !== v ? "nextSibling" : "previousSibling", h = t.parentNode,
                            d = y && t.nodeName.toLowerCase(), u = !i && !y;
                        if (h) {
                            if (m) {
                                for (; c;) {
                                    for (o = t; o = o[c];) if (y ? o.nodeName.toLowerCase() === d : 1 === o.nodeType) return !1;
                                    l = c = "only" === p && !l && "nextSibling"
                                }
                                return !0
                            }
                            if (l = [v ? h.firstChild : h.lastChild], v && u) {
                                for (a = (n = (r = h[z] || (h[z] = {}))[p] || [])[0] === B && n[1], s = n[0] === B && n[2], o = a && h.childNodes[a]; o = ++a && o && o[c] || (s = a = 0) || l.pop();) if (1 === o.nodeType && ++s && o === t) {
                                    r[p] = [B, a, s];
                                    break
                                }
                            } else if (u && (n = (t[z] || (t[z] = {}))[p]) && n[0] === B) s = n[1]; else for (; (o = ++a && o && o[c] || (s = a = 0) || l.pop()) && ((y ? o.nodeName.toLowerCase() !== d : 1 !== o.nodeType) || !++s || (u && ((o[z] || (o[z] = {}))[p] = [B, s]), o !== t));) ;
                            return (s -= g) === f || s % f == 0 && 0 <= s / f
                        }
                    }
                }, PSEUDO: function (t, o) {
                    var e, s = C.pseudos[t] || C.setFilters[t.toLowerCase()] || b.error("unsupported pseudo: " + t);
                    return s[z] ? s(o) : 1 < s.length ? (e = [t, t, "", o], C.setFilters.hasOwnProperty(t.toLowerCase()) ? l(function (t, e) {
                        for (var i, n = s(t, o), r = n.length; r--;) t[i = et.call(t, n[r])] = !(e[i] = n[r])
                    }) : function (t) {
                        return s(t, 0, e)
                    }) : s
                }
            },
            pseudos: {
                not: l(function (t) {
                    var n = [], r = [], a = T(t.replace(lt, "$1"));
                    return a[z] ? l(function (t, e, i, n) {
                        for (var r, o = a(t, null, n, []), s = t.length; s--;) (r = o[s]) && (t[s] = !(e[s] = r))
                    }) : function (t, e, i) {
                        return n[0] = t, a(n, null, i, r), !r.pop()
                    }
                }), has: l(function (e) {
                    return function (t) {
                        return 0 < b(e, t).length
                    }
                }), contains: l(function (e) {
                    return function (t) {
                        return -1 < (t.textContent || t.innerText || _(t)).indexOf(e)
                    }
                }), lang: l(function (i) {
                    return pt.test(i || "") || b.error("unsupported lang: " + i), i = i.replace(wt, Ct).toLowerCase(), function (t) {
                        var e;
                        do {
                            if (e = F ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return (e = e.toLowerCase()) === i || 0 === e.indexOf(i + "-")
                        } while ((t = t.parentNode) && 1 === t.nodeType);
                        return !1
                    }
                }), target: function (t) {
                    var e = i.location && i.location.hash;
                    return e && e.slice(1) === t.id
                }, root: function (t) {
                    return t === I
                }, focus: function (t) {
                    return t === L.activeElement && (!L.hasFocus || L.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
                }, enabled: function (t) {
                    return !1 === t.disabled
                }, disabled: function (t) {
                    return !0 === t.disabled
                }, checked: function (t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && !!t.checked || "option" === e && !!t.selected
                }, selected: function (t) {
                    return t.parentNode && t.parentNode.selectedIndex, !0 === t.selected
                }, empty: function (t) {
                    for (t = t.firstChild; t; t = t.nextSibling) if (t.nodeType < 6) return !1;
                    return !0
                }, parent: function (t) {
                    return !C.pseudos.empty(t)
                }, header: function (t) {
                    return mt.test(t.nodeName)
                }, input: function (t) {
                    return gt.test(t.nodeName)
                }, button: function (t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && "button" === t.type || "button" === e
                }, text: function (t) {
                    var e;
                    return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase())
                }, first: s(function () {
                    return [0]
                }), last: s(function (t, e) {
                    return [e - 1]
                }), eq: s(function (t, e, i) {
                    return [i < 0 ? i + e : i]
                }), even: s(function (t, e) {
                    for (var i = 0; i < e; i += 2) t.push(i);
                    return t
                }), odd: s(function (t, e) {
                    for (var i = 1; i < e; i += 2) t.push(i);
                    return t
                }), lt: s(function (t, e, i) {
                    for (var n = i < 0 ? i + e : i; 0 <= --n;) t.push(n);
                    return t
                }), gt: s(function (t, e, i) {
                    for (var n = i < 0 ? i + e : i; ++n < e;) t.push(n);
                    return t
                })
            }
        }).pseudos.nth = C.pseudos.eq, {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        }) C.pseudos[f] = r(f);
        for (f in{submit: !0, reset: !0}) C.pseudos[f] = o(f);
        return a.prototype = C.filters = C.pseudos, C.setFilters = new a, S = b.tokenize = function (t, e) {
            var i, n, r, o, s, a, l, c = q[t + " "];
            if (c) return e ? 0 : c.slice(0);
            for (s = t, a = [], l = C.preFilter; s;) {
                for (o in i && !(n = ct.exec(s)) || (n && (s = s.slice(n[0].length) || s), a.push(r = [])), i = !1, (n = ht.exec(s)) && (i = n.shift(), r.push({
                    value: i,
                    type: n[0].replace(lt, " ")
                }), s = s.slice(i.length)), C.filter) !(n = ft[o].exec(s)) || l[o] && !(n = l[o](n)) || (i = n.shift(), r.push({
                    value: i,
                    type: o,
                    matches: n
                }), s = s.slice(i.length));
                if (!i) break
            }
            return e ? s.length : s ? b.error(t) : q(t, a).slice(0)
        }, T = b.compile = function (t, e) {
            var i, n = [], r = [], o = G[t + " "];
            if (!o) {
                for (e || (e = S(t)), i = e.length; i--;) (o = p(e[i]))[z] ? n.push(o) : r.push(o);
                (o = G(t, h(r, n))).selector = t
            }
            return o
        }, A = b.select = function (t, e, i, n) {
            var r, o, s, a, l, c = "function" == typeof t && t, h = !n && S(t = c.selector || t);
            if (i = i || [], 1 === h.length) {
                if (2 < (o = h[0] = h[0].slice(0)).length && "ID" === (s = o[0]).type && v.getById && 9 === e.nodeType && F && C.relative[o[1].type]) {
                    if (!(e = (C.find.ID(s.matches[0].replace(wt, Ct), e) || [])[0])) return i;
                    c && (e = e.parentNode), t = t.slice(o.shift().value.length)
                }
                for (r = ft.needsContext.test(t) ? 0 : o.length; r-- && (s = o[r], !C.relative[a = s.type]);) if ((l = C.find[a]) && (n = l(s.matches[0].replace(wt, Ct), xt.test(o[0].type) && g(e.parentNode) || e))) {
                    if (o.splice(r, 1), !(t = n.length && m(o))) return J.apply(i, n), i;
                    break
                }
            }
            return (c || T(t, h))(n, e, !F, i, xt.test(t) && g(e.parentNode) || e), i
        }, v.sortStable = z.split("").sort(X).join("") === z, v.detectDuplicates = !!E, P(), v.sortDetached = n(function (t) {
            return 1 & t.compareDocumentPosition(L.createElement("div"))
        }), n(function (t) {
            return t.innerHTML = "<a href='#'></a>", "#" === t.firstChild.getAttribute("href")
        }) || e("type|href|height|width", function (t, e, i) {
            if (!i) return t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
        }), v.attributes && n(function (t) {
            return t.innerHTML = "<input/>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value")
        }) || e("value", function (t, e, i) {
            if (!i && "input" === t.nodeName.toLowerCase()) return t.defaultValue
        }), n(function (t) {
            return null == t.getAttribute("disabled")
        }) || e(it, function (t, e, i) {
            var n;
            if (!i) return !0 === t[e] ? e.toLowerCase() : (n = t.getAttributeNode(e)) && n.specified ? n.value : null
        }), b
    }(g);
    rt.find = ct, rt.expr = ct.selectors, rt.expr[":"] = rt.expr.pseudos, rt.unique = ct.uniqueSort, rt.text = ct.getText, rt.isXMLDoc = ct.isXML, rt.contains = ct.contains;
    var ht = rt.expr.match.needsContext, dt = /^<(\w+)\s*\/?>(?:<\/\1>|)$/, ut = /^.[^:#\[\.,]*$/;
    rt.filter = function (t, e, i) {
        var n = e[0];
        return i && (t = ":not(" + t + ")"), 1 === e.length && 1 === n.nodeType ? rt.find.matchesSelector(n, t) ? [n] : [] : rt.find.matches(t, rt.grep(e, function (t) {
            return 1 === t.nodeType
        }))
    }, rt.fn.extend({
        find: function (t) {
            var e, i = [], n = this, r = n.length;
            if ("string" != typeof t) return this.pushStack(rt(t).filter(function () {
                for (e = 0; e < r; e++) if (rt.contains(n[e], this)) return !0
            }));
            for (e = 0; e < r; e++) rt.find(t, n[e], i);
            return (i = this.pushStack(1 < r ? rt.unique(i) : i)).selector = this.selector ? this.selector + " " + t : t, i
        }, filter: function (t) {
            return this.pushStack(e(this, t || [], !1))
        }, not: function (t) {
            return this.pushStack(e(this, t || [], !0))
        }, is: function (t) {
            return !!e(this, "string" == typeof t && ht.test(t) ? rt(t) : t || [], !1).length
        }
    });
    var pt, ft = g.document, gt = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/;
    (rt.fn.init = function (t, e) {
        var i, n;
        if (!t) return this;
        if ("string" != typeof t) return t.nodeType ? (this.context = this[0] = t, this.length = 1, this) : rt.isFunction(t) ? "undefined" != typeof pt.ready ? pt.ready(t) : t(rt) : (t.selector !== undefined && (this.selector = t.selector, this.context = t.context), rt.makeArray(t, this));
        if (!(i = "<" === t.charAt(0) && ">" === t.charAt(t.length - 1) && 3 <= t.length ? [null, t, null] : gt.exec(t)) || !i[1] && e) return !e || e.jquery ? (e || pt).find(t) : this.constructor(e).find(t);
        if (i[1]) {
            if (e = e instanceof rt ? e[0] : e, rt.merge(this, rt.parseHTML(i[1], e && e.nodeType ? e.ownerDocument || e : ft, !0)), dt.test(i[1]) && rt.isPlainObject(e)) for (i in e) rt.isFunction(this[i]) ? this[i](e[i]) : this.attr(i, e[i]);
            return this
        }
        if ((n = ft.getElementById(i[2])) && n.parentNode) {
            if (n.id !== i[2]) return pt.find(t);
            this.length = 1, this[0] = n
        }
        return this.context = ft, this.selector = t, this
    }).prototype = rt.fn, pt = rt(ft);
    var mt = /^(?:parents|prev(?:Until|All))/, vt = {children: !0, contents: !0, next: !0, prev: !0};
    rt.extend({
        dir: function (t, e, i) {
            for (var n = [], r = t[e]; r && 9 !== r.nodeType && (i === undefined || 1 !== r.nodeType || !rt(r).is(i));) 1 === r.nodeType && n.push(r), r = r[e];
            return n
        }, sibling: function (t, e) {
            for (var i = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && i.push(t);
            return i
        }
    }), rt.fn.extend({
        has: function (t) {
            var e, i = rt(t, this), n = i.length;
            return this.filter(function () {
                for (e = 0; e < n; e++) if (rt.contains(this, i[e])) return !0
            })
        }, closest: function (t, e) {
            for (var i, n = 0, r = this.length, o = [], s = ht.test(t) || "string" != typeof t ? rt(t, e || this.context) : 0; n < r; n++) for (i = this[n]; i && i !== e; i = i.parentNode) if (i.nodeType < 11 && (s ? -1 < s.index(i) : 1 === i.nodeType && rt.find.matchesSelector(i, t))) {
                o.push(i);
                break
            }
            return this.pushStack(1 < o.length ? rt.unique(o) : o)
        }, index: function (t) {
            return t ? "string" == typeof t ? rt.inArray(this[0], rt(t)) : rt.inArray(t.jquery ? t[0] : t, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        }, add: function (t, e) {
            return this.pushStack(rt.unique(rt.merge(this.get(), rt(t, e))))
        }, addBack: function (t) {
            return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
        }
    }), rt.each({
        parent: function (t) {
            var e = t.parentNode;
            return e && 11 !== e.nodeType ? e : null
        }, parents: function (t) {
            return rt.dir(t, "parentNode")
        }, parentsUntil: function (t, e, i) {
            return rt.dir(t, "parentNode", i)
        }, next: function (t) {
            return i(t, "nextSibling")
        }, prev: function (t) {
            return i(t, "previousSibling")
        }, nextAll: function (t) {
            return rt.dir(t, "nextSibling")
        }, prevAll: function (t) {
            return rt.dir(t, "previousSibling")
        }, nextUntil: function (t, e, i) {
            return rt.dir(t, "nextSibling", i)
        }, prevUntil: function (t, e, i) {
            return rt.dir(t, "previousSibling", i)
        }, siblings: function (t) {
            return rt.sibling((t.parentNode || {}).firstChild, t)
        }, children: function (t) {
            return rt.sibling(t.firstChild)
        }, contents: function (t) {
            return rt.nodeName(t, "iframe") ? t.contentDocument || t.contentWindow.document : rt.merge([], t.childNodes)
        }
    }, function (n, r) {
        rt.fn[n] = function (t, e) {
            var i = rt.map(this, r, t);
            return "Until" !== n.slice(-5) && (e = t), e && "string" == typeof e && (i = rt.filter(e, i)), 1 < this.length && (vt[n] || (i = rt.unique(i)), mt.test(n) && (i = i.reverse())), this.pushStack(i)
        }
    });
    var yt, xt = /\S+/g, bt = {};
    rt.Callbacks = function (r) {
        r = "string" == typeof r ? bt[r] || u(r) : rt.extend({}, r);
        var o, e, i, s, n, a, l = [], c = !r.once && [], h = function (t) {
            for (e = r.memory && t, i = !0, n = a || 0, a = 0, s = l.length, o = !0; l && n < s; n++) if (!1 === l[n].apply(t[0], t[1]) && r.stopOnFalse) {
                e = !1;
                break
            }
            o = !1, l && (c ? c.length && h(c.shift()) : e ? l = [] : d.disable())
        }, d = {
            add: function () {
                if (l) {
                    var t = l.length;
                    !function n(t) {
                        rt.each(t, function (t, e) {
                            var i = rt.type(e);
                            "function" === i ? r.unique && d.has(e) || l.push(e) : e && e.length && "string" !== i && n(e)
                        })
                    }(arguments), o ? s = l.length : e && (a = t, h(e))
                }
                return this
            }, remove: function () {
                return l && rt.each(arguments, function (t, e) {
                    for (var i; -1 < (i = rt.inArray(e, l, i));) l.splice(i, 1), o && (i <= s && s--, i <= n && n--)
                }), this
            }, has: function (t) {
                return t ? -1 < rt.inArray(t, l) : !(!l || !l.length)
            }, empty: function () {
                return l = [], s = 0, this
            }, disable: function () {
                return l = c = e = undefined, this
            }, disabled: function () {
                return !l
            }, lock: function () {
                return c = undefined, e || d.disable(), this
            }, locked: function () {
                return !c
            }, fireWith: function (t, e) {
                return !l || i && !c || (e = [t, (e = e || []).slice ? e.slice() : e], o ? c.push(e) : h(e)), this
            }, fire: function () {
                return d.fireWith(this, arguments), this
            }, fired: function () {
                return !!i
            }
        };
        return d
    }, rt.extend({
        Deferred: function (t) {
            var o = [["resolve", "done", rt.Callbacks("once memory"), "resolved"], ["reject", "fail", rt.Callbacks("once memory"), "rejected"], ["notify", "progress", rt.Callbacks("memory")]],
                r = "pending", s = {
                    state: function () {
                        return r
                    }, always: function () {
                        return a.done(arguments).fail(arguments), this
                    }, then: function () {
                        var r = arguments;
                        return rt.Deferred(function (n) {
                            rt.each(o, function (t, e) {
                                var i = rt.isFunction(r[t]) && r[t];
                                a[e[1]](function () {
                                    var t = i && i.apply(this, arguments);
                                    t && rt.isFunction(t.promise) ? t.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[e[0] + "With"](this === s ? n.promise() : this, i ? [t] : arguments)
                                })
                            }), r = null
                        }).promise()
                    }, promise: function (t) {
                        return null != t ? rt.extend(t, s) : s
                    }
                }, a = {};
            return s.pipe = s.then, rt.each(o, function (t, e) {
                var i = e[2], n = e[3];
                s[e[1]] = i.add, n && i.add(function () {
                    r = n
                }, o[1 ^ t][2].disable, o[2][2].lock), a[e[0]] = function () {
                    return a[e[0] + "With"](this === a ? s : this, arguments), this
                }, a[e[0] + "With"] = i.fireWith
            }), s.promise(a), t && t.call(a, a), a
        }, when: function (t) {
            var r, e, i, n = 0, o = U.call(arguments), s = o.length,
                a = 1 !== s || t && rt.isFunction(t.promise) ? s : 0, l = 1 === a ? t : rt.Deferred(),
                c = function (e, i, n) {
                    return function (t) {
                        i[e] = this, n[e] = 1 < arguments.length ? U.call(arguments) : t, n === r ? l.notifyWith(i, n) : --a || l.resolveWith(i, n)
                    }
                };
            if (1 < s) for (r = new Array(s), e = new Array(s), i = new Array(s); n < s; n++) o[n] && rt.isFunction(o[n].promise) ? o[n].promise().done(c(n, i, o)).fail(l.reject).progress(c(n, e, r)) : --a;
            return a || l.resolveWith(i, o), l.promise()
        }
    }), rt.fn.ready = function (t) {
        return rt.ready.promise().done(t), this
    }, rt.extend({
        isReady: !1, readyWait: 1, holdReady: function (t) {
            t ? rt.readyWait++ : rt.ready(!0)
        }, ready: function (t) {
            if (!0 === t ? !--rt.readyWait : !rt.isReady) {
                if (!ft.body) return setTimeout(rt.ready);
                (rt.isReady = !0) !== t && 0 < --rt.readyWait || (yt.resolveWith(ft, [rt]), rt.fn.triggerHandler && (rt(ft).triggerHandler("ready"), rt(ft).off("ready")))
            }
        }
    }), rt.ready.promise = function (t) {
        if (!yt) if (yt = rt.Deferred(), "complete" === ft.readyState) setTimeout(rt.ready); else if (ft.addEventListener) ft.addEventListener("DOMContentLoaded", o, !1), g.addEventListener("load", o, !1); else {
            ft.attachEvent("onreadystatechange", o), g.attachEvent("onload", o);
            var e = !1;
            try {
                e = null == g.frameElement && ft.documentElement
            } catch (i) {
            }
            e && e.doScroll && function n() {
                if (!rt.isReady) {
                    try {
                        e.doScroll("left")
                    } catch (i) {
                        return setTimeout(n, 50)
                    }
                    r(), rt.ready()
                }
            }()
        }
        return yt.promise(t)
    };
    var wt, Ct = typeof undefined;
    for (wt in rt(it)) break;
    it.ownLast = "0" !== wt, it.inlineBlockNeedsLayout = !1, rt(function () {
        var t, e, i, n;
        (i = ft.getElementsByTagName("body")[0]) && i.style && (e = ft.createElement("div"), (n = ft.createElement("div")).style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", i.appendChild(n).appendChild(e), typeof e.style.zoom !== Ct && (e.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", it.inlineBlockNeedsLayout = t = 3 === e.offsetWidth, t && (i.style.zoom = 1)), i.removeChild(n))
    }), function () {
        var t = ft.createElement("div");
        if (null == it.deleteExpando) {
            it.deleteExpando = !0;
            try {
                delete t.test
            } catch (e) {
                it.deleteExpando = !1
            }
        }
        t = null
    }(), rt.acceptData = function (t) {
        var e = rt.noData[(t.nodeName + " ").toLowerCase()], i = +t.nodeType || 1;
        return (1 === i || 9 === i) && (!e || !0 !== e && t.getAttribute("classid") === e)
    };
    var _t = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, kt = /([A-Z])/g;
    rt.extend({
        cache: {},
        noData: {"applet ": !0, "embed ": !0, "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"},
        hasData: function (t) {
            return !!(t = t.nodeType ? rt.cache[t[rt.expando]] : t[rt.expando]) && !c(t)
        },
        data: function (t, e, i) {
            return n(t, e, i)
        },
        removeData: function (t, e) {
            return s(t, e)
        },
        _data: function (t, e, i) {
            return n(t, e, i, !0)
        },
        _removeData: function (t, e) {
            return s(t, e, !0)
        }
    }), rt.fn.extend({
        data: function (t, e) {
            var i, n, r, o = this[0], s = o && o.attributes;
            if (t !== undefined) return "object" == typeof t ? this.each(function () {
                rt.data(this, t)
            }) : 1 < arguments.length ? this.each(function () {
                rt.data(this, t, e)
            }) : o ? l(o, t, rt.data(o, t)) : undefined;
            if (this.length && (r = rt.data(o), 1 === o.nodeType && !rt._data(o, "parsedAttrs"))) {
                for (i = s.length; i--;) s[i] && 0 === (n = s[i].name).indexOf("data-") && l(o, n = rt.camelCase(n.slice(5)), r[n]);
                rt._data(o, "parsedAttrs", !0)
            }
            return r
        }, removeData: function (t) {
            return this.each(function () {
                rt.removeData(this, t)
            })
        }
    }), rt.extend({
        queue: function (t, e, i) {
            var n;
            if (t) return e = (e || "fx") + "queue", n = rt._data(t, e), i && (!n || rt.isArray(i) ? n = rt._data(t, e, rt.makeArray(i)) : n.push(i)), n || []
        }, dequeue: function (t, e) {
            e = e || "fx";
            var i = rt.queue(t, e), n = i.length, r = i.shift(), o = rt._queueHooks(t, e), s = function () {
                rt.dequeue(t, e)
            };
            "inprogress" === r && (r = i.shift(), n--), r && ("fx" === e && i.unshift("inprogress"), delete o.stop, r.call(t, s, o)), !n && o && o.empty.fire()
        }, _queueHooks: function (t, e) {
            var i = e + "queueHooks";
            return rt._data(t, i) || rt._data(t, i, {
                empty: rt.Callbacks("once memory").add(function () {
                    rt._removeData(t, e + "queue"), rt._removeData(t, i)
                })
            })
        }
    }), rt.fn.extend({
        queue: function (e, i) {
            var t = 2;
            return "string" != typeof e && (i = e, e = "fx", t--), arguments.length < t ? rt.queue(this[0], e) : i === undefined ? this : this.each(function () {
                var t = rt.queue(this, e, i);
                rt._queueHooks(this, e), "fx" === e && "inprogress" !== t[0] && rt.dequeue(this, e)
            })
        }, dequeue: function (t) {
            return this.each(function () {
                rt.dequeue(this, t)
            })
        }, clearQueue: function (t) {
            return this.queue(t || "fx", [])
        }, promise: function (t, e) {
            var i, n = 1, r = rt.Deferred(), o = this, s = this.length, a = function () {
                --n || r.resolveWith(o, [o])
            };
            for ("string" != typeof t && (e = t, t = undefined), t = t || "fx"; s--;) (i = rt._data(o[s], t + "queueHooks")) && i.empty && (n++, i.empty.add(a));
            return a(), r.promise(e)
        }
    });
    var St = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, Tt = ["Top", "Right", "Bottom", "Left"],
        At = function (t, e) {
            return t = e || t, "none" === rt.css(t, "display") || !rt.contains(t.ownerDocument, t)
        }, $t = rt.access = function (t, e, i, n, r, o, s) {
            var a = 0, l = t.length, c = null == i;
            if ("object" === rt.type(i)) for (a in r = !0, i) rt.access(t, e, a, i[a], !0, o, s); else if (n !== undefined && (r = !0, rt.isFunction(n) || (s = !0), c && (s ? (e.call(t, n), e = null) : (c = e, e = function (t, e, i) {
                return c.call(rt(t), i)
            })), e)) for (; a < l; a++) e(t[a], i, s ? n : n.call(t[a], a, e(t[a], i)));
            return r ? t : c ? e.call(t) : l ? e(t[0], i) : o
        }, Mt = /^(?:checkbox|radio)$/i;
    !function () {
        var t = ft.createElement("input"), e = ft.createElement("div"), i = ft.createDocumentFragment();
        if (e.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", it.leadingWhitespace = 3 === e.firstChild.nodeType, it.tbody = !e.getElementsByTagName("tbody").length, it.htmlSerialize = !!e.getElementsByTagName("link").length, it.html5Clone = "<:nav></:nav>" !== ft.createElement("nav").cloneNode(!0).outerHTML, t.type = "checkbox", t.checked = !0, i.appendChild(t), it.appendChecked = t.checked, e.innerHTML = "<textarea>x</textarea>", it.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue, i.appendChild(e), e.innerHTML = "<input type='radio' checked='checked' name='t'/>", it.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked, it.noCloneEvent = !0, e.attachEvent && (e.attachEvent("onclick", function () {
            it.noCloneEvent = !1
        }), e.cloneNode(!0).click()), null == it.deleteExpando) {
            it.deleteExpando = !0;
            try {
                delete e.test
            } catch (n) {
                it.deleteExpando = !1
            }
        }
    }(), function () {
        var t, e, i = ft.createElement("div");
        for (t in{
            submit: !0,
            change: !0,
            focusin: !0
        }) e = "on" + t, (it[t + "Bubbles"] = e in g) || (i.setAttribute(e, "t"), it[t + "Bubbles"] = !1 === i.attributes[e].expando);
        i = null
    }();
    var Et = /^(?:input|select|textarea)$/i, Pt = /^key/, Lt = /^(?:mouse|pointer|contextmenu)|click/,
        It = /^(?:focusinfocus|focusoutblur)$/, Ft = /^([^.]*)(?:\.(.+)|)$/;
    rt.event = {
        global: {},
        add: function (t, e, i, n, r) {
            var o, s, a, l, c, h, d, u, p, f, g, m = rt._data(t);
            if (m) {
                for (i.handler && (i = (l = i).handler, r = l.selector), i.guid || (i.guid = rt.guid++), (s = m.events) || (s = m.events = {}), (h = m.handle) || ((h = m.handle = function (t) {
                    return typeof rt === Ct || t && rt.event.triggered === t.type ? undefined : rt.event.dispatch.apply(h.elem, arguments)
                }).elem = t), a = (e = (e || "").match(xt) || [""]).length; a--;) p = g = (o = Ft.exec(e[a]) || [])[1], f = (o[2] || "").split(".").sort(), p && (c = rt.event.special[p] || {}, p = (r ? c.delegateType : c.bindType) || p, c = rt.event.special[p] || {}, d = rt.extend({
                    type: p,
                    origType: g,
                    data: n,
                    handler: i,
                    guid: i.guid,
                    selector: r,
                    needsContext: r && rt.expr.match.needsContext.test(r),
                    namespace: f.join(".")
                }, l), (u = s[p]) || ((u = s[p] = []).delegateCount = 0, c.setup && !1 !== c.setup.call(t, n, f, h) || (t.addEventListener ? t.addEventListener(p, h, !1) : t.attachEvent && t.attachEvent("on" + p, h))), c.add && (c.add.call(t, d),
                d.handler.guid || (d.handler.guid = i.guid)), r ? u.splice(u.delegateCount++, 0, d) : u.push(d), rt.event.global[p] = !0);
                t = null
            }
        },
        remove: function (t, e, i, n, r) {
            var o, s, a, l, c, h, d, u, p, f, g, m = rt.hasData(t) && rt._data(t);
            if (m && (h = m.events)) {
                for (c = (e = (e || "").match(xt) || [""]).length; c--;) if (p = g = (a = Ft.exec(e[c]) || [])[1], f = (a[2] || "").split(".").sort(), p) {
                    for (d = rt.event.special[p] || {}, u = h[p = (n ? d.delegateType : d.bindType) || p] || [], a = a[2] && new RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"), l = o = u.length; o--;) s = u[o], !r && g !== s.origType || i && i.guid !== s.guid || a && !a.test(s.namespace) || n && n !== s.selector && ("**" !== n || !s.selector) || (u.splice(o, 1), s.selector && u.delegateCount--, d.remove && d.remove.call(t, s));
                    l && !u.length && (d.teardown && !1 !== d.teardown.call(t, f, m.handle) || rt.removeEvent(t, p, m.handle), delete h[p])
                } else for (p in h) rt.event.remove(t, p + e[c], i, n, !0);
                rt.isEmptyObject(h) && (delete m.handle, rt._removeData(t, "events"))
            }
        },
        trigger: function (t, e, i, n) {
            var r, o, s, a, l, c, h, d = [i || ft], u = et.call(t, "type") ? t.type : t,
                p = et.call(t, "namespace") ? t.namespace.split(".") : [];
            if (s = c = i = i || ft, 3 !== i.nodeType && 8 !== i.nodeType && !It.test(u + rt.event.triggered) && (0 <= u.indexOf(".") && (u = (p = u.split(".")).shift(), p.sort()), o = u.indexOf(":") < 0 && "on" + u, (t = t[rt.expando] ? t : new rt.Event(u, "object" == typeof t && t)).isTrigger = n ? 2 : 3, t.namespace = p.join("."), t.namespace_re = t.namespace ? new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = undefined, t.target || (t.target = i), e = null == e ? [t] : rt.makeArray(e, [t]), l = rt.event.special[u] || {}, n || !l.trigger || !1 !== l.trigger.apply(i, e))) {
                if (!n && !l.noBubble && !rt.isWindow(i)) {
                    for (a = l.delegateType || u, It.test(a + u) || (s = s.parentNode); s; s = s.parentNode) d.push(s), c = s;
                    c === (i.ownerDocument || ft) && d.push(c.defaultView || c.parentWindow || g)
                }
                for (h = 0; (s = d[h++]) && !t.isPropagationStopped();) t.type = 1 < h ? a : l.bindType || u, (r = (rt._data(s, "events") || {})[t.type] && rt._data(s, "handle")) && r.apply(s, e), (r = o && s[o]) && r.apply && rt.acceptData(s) && (t.result = r.apply(s, e), !1 === t.result && t.preventDefault());
                if (t.type = u, !n && !t.isDefaultPrevented() && (!l._default || !1 === l._default.apply(d.pop(), e)) && rt.acceptData(i) && o && i[u] && !rt.isWindow(i)) {
                    (c = i[o]) && (i[o] = null), rt.event.triggered = u;
                    try {
                        i[u]()
                    } catch (f) {
                    }
                    rt.event.triggered = undefined, c && (i[o] = c)
                }
                return t.result
            }
        },
        dispatch: function (t) {
            t = rt.event.fix(t);
            var e, i, n, r, o, s = [], a = U.call(arguments), l = (rt._data(this, "events") || {})[t.type] || [],
                c = rt.event.special[t.type] || {};
            if ((a[0] = t).delegateTarget = this, !c.preDispatch || !1 !== c.preDispatch.call(this, t)) {
                for (s = rt.event.handlers.call(this, t, l), e = 0; (r = s[e++]) && !t.isPropagationStopped();) for (t.currentTarget = r.elem, o = 0; (n = r.handlers[o++]) && !t.isImmediatePropagationStopped();) t.namespace_re && !t.namespace_re.test(n.namespace) || (t.handleObj = n, t.data = n.data, (i = ((rt.event.special[n.origType] || {}).handle || n.handler).apply(r.elem, a)) !== undefined && !1 === (t.result = i) && (t.preventDefault(), t.stopPropagation()));
                return c.postDispatch && c.postDispatch.call(this, t), t.result
            }
        },
        handlers: function (t, e) {
            var i, n, r, o, s = [], a = e.delegateCount, l = t.target;
            if (a && l.nodeType && (!t.button || "click" !== t.type)) for (; l != this; l = l.parentNode || this) if (1 === l.nodeType && (!0 !== l.disabled || "click" !== t.type)) {
                for (r = [], o = 0; o < a; o++) r[i = (n = e[o]).selector + " "] === undefined && (r[i] = n.needsContext ? 0 <= rt(i, this).index(l) : rt.find(i, this, null, [l]).length), r[i] && r.push(n);
                r.length && s.push({elem: l, handlers: r})
            }
            return a < e.length && s.push({elem: this, handlers: e.slice(a)}), s
        },
        fix: function (t) {
            if (t[rt.expando]) return t;
            var e, i, n, r = t.type, o = t, s = this.fixHooks[r];
            for (s || (this.fixHooks[r] = s = Lt.test(r) ? this.mouseHooks : Pt.test(r) ? this.keyHooks : {}), n = s.props ? this.props.concat(s.props) : this.props, t = new rt.Event(o), e = n.length; e--;) t[i = n[e]] = o[i];
            return t.target || (t.target = o.srcElement || ft), 3 === t.target.nodeType && (t.target = t.target.parentNode), t.metaKey = !!t.metaKey, s.filter ? s.filter(t, o) : t
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "), filter: function (t, e) {
                return null == t.which && (t.which = null != e.charCode ? e.charCode : e.keyCode), t
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function (t, e) {
                var i, n, r, o = e.button, s = e.fromElement;
                return null == t.pageX && null != e.clientX && (r = (n = t.target.ownerDocument || ft).documentElement, i = n.body, t.pageX = e.clientX + (r && r.scrollLeft || i && i.scrollLeft || 0) - (r && r.clientLeft || i && i.clientLeft || 0), t.pageY = e.clientY + (r && r.scrollTop || i && i.scrollTop || 0) - (r && r.clientTop || i && i.clientTop || 0)), !t.relatedTarget && s && (t.relatedTarget = s === t.target ? e.toElement : s), t.which || o === undefined || (t.which = 1 & o ? 1 : 2 & o ? 3 : 4 & o ? 2 : 0), t
            }
        },
        special: {
            load: {noBubble: !0}, focus: {
                trigger: function () {
                    if (this !== p() && this.focus) try {
                        return this.focus(), !1
                    } catch (t) {
                    }
                }, delegateType: "focusin"
            }, blur: {
                trigger: function () {
                    if (this === p() && this.blur) return this.blur(), !1
                }, delegateType: "focusout"
            }, click: {
                trigger: function () {
                    if (rt.nodeName(this, "input") && "checkbox" === this.type && this.click) return this.click(), !1
                }, _default: function (t) {
                    return rt.nodeName(t.target, "a")
                }
            }, beforeunload: {
                postDispatch: function (t) {
                    t.result !== undefined && t.originalEvent && (t.originalEvent.returnValue = t.result)
                }
            }
        },
        simulate: function (t, e, i, n) {
            var r = rt.extend(new rt.Event, i, {type: t, isSimulated: !0, originalEvent: {}});
            n ? rt.event.trigger(r, null, e) : rt.event.dispatch.call(e, r), r.isDefaultPrevented() && i.preventDefault()
        }
    }, rt.removeEvent = ft.removeEventListener ? function (t, e, i) {
        t.removeEventListener && t.removeEventListener(e, i, !1)
    } : function (t, e, i) {
        var n = "on" + e;
        t.detachEvent && (typeof t[n] === Ct && (t[n] = null), t.detachEvent(n, i))
    }, rt.Event = function (t, e) {
        if (!(this instanceof rt.Event)) return new rt.Event(t, e);
        t && t.type ? (this.originalEvent = t, this.type = t.type, this.isDefaultPrevented = t.defaultPrevented || t.defaultPrevented === undefined && !1 === t.returnValue ? h : d) : this.type = t, e && rt.extend(this, e), this.timeStamp = t && t.timeStamp || rt.now(), this[rt.expando] = !0
    }, rt.Event.prototype = {
        isDefaultPrevented: d,
        isPropagationStopped: d,
        isImmediatePropagationStopped: d,
        preventDefault: function () {
            var t = this.originalEvent;
            this.isDefaultPrevented = h, t && (t.preventDefault ? t.preventDefault() : t.returnValue = !1)
        },
        stopPropagation: function () {
            var t = this.originalEvent;
            this.isPropagationStopped = h, t && (t.stopPropagation && t.stopPropagation(), t.cancelBubble = !0)
        },
        stopImmediatePropagation: function () {
            var t = this.originalEvent;
            this.isImmediatePropagationStopped = h, t && t.stopImmediatePropagation && t.stopImmediatePropagation(), this.stopPropagation()
        }
    }, rt.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function (t, o) {
        rt.event.special[t] = {
            delegateType: o, bindType: o, handle: function (t) {
                var e, i = this, n = t.relatedTarget, r = t.handleObj;
                return n && (n === i || rt.contains(i, n)) || (t.type = r.origType, e = r.handler.apply(this, arguments), t.type = o), e
            }
        }
    }), it.submitBubbles || (rt.event.special.submit = {
        setup: function () {
            if (rt.nodeName(this, "form")) return !1;
            rt.event.add(this, "click._submit keypress._submit", function (t) {
                var e = t.target, i = rt.nodeName(e, "input") || rt.nodeName(e, "button") ? e.form : undefined;
                i && !rt._data(i, "submitBubbles") && (rt.event.add(i, "submit._submit", function (t) {
                    t._submit_bubble = !0
                }), rt._data(i, "submitBubbles", !0))
            })
        }, postDispatch: function (t) {
            t._submit_bubble && (delete t._submit_bubble, this.parentNode && !t.isTrigger && rt.event.simulate("submit", this.parentNode, t, !0))
        }, teardown: function () {
            if (rt.nodeName(this, "form")) return !1;
            rt.event.remove(this, "._submit")
        }
    }), it.changeBubbles || (rt.event.special.change = {
        setup: function () {
            if (Et.test(this.nodeName)) return "checkbox" !== this.type && "radio" !== this.type || (rt.event.add(this, "propertychange._change", function (t) {
                "checked" === t.originalEvent.propertyName && (this._just_changed = !0)
            }), rt.event.add(this, "click._change", function (t) {
                this._just_changed && !t.isTrigger && (this._just_changed = !1), rt.event.simulate("change", this, t, !0)
            })), !1;
            rt.event.add(this, "beforeactivate._change", function (t) {
                var e = t.target;
                Et.test(e.nodeName) && !rt._data(e, "changeBubbles") && (rt.event.add(e, "change._change", function (t) {
                    !this.parentNode || t.isSimulated || t.isTrigger || rt.event.simulate("change", this.parentNode, t, !0)
                }), rt._data(e, "changeBubbles", !0))
            })
        }, handle: function (t) {
            var e = t.target;
            if (this !== e || t.isSimulated || t.isTrigger || "radio" !== e.type && "checkbox" !== e.type) return t.handleObj.handler.apply(this, arguments)
        }, teardown: function () {
            return rt.event.remove(this, "._change"), !Et.test(this.nodeName)
        }
    }), it.focusinBubbles || rt.each({focus: "focusin", blur: "focusout"}, function (i, n) {
        var r = function (t) {
            rt.event.simulate(n, t.target, rt.event.fix(t), !0)
        };
        rt.event.special[n] = {
            setup: function () {
                var t = this.ownerDocument || this, e = rt._data(t, n);
                e || t.addEventListener(i, r, !0), rt._data(t, n, (e || 0) + 1)
            }, teardown: function () {
                var t = this.ownerDocument || this, e = rt._data(t, n) - 1;
                e ? rt._data(t, n, e) : (t.removeEventListener(i, r, !0), rt._removeData(t, n))
            }
        }
    }), rt.fn.extend({
        on: function (t, e, i, n, r) {
            var o, s;
            if ("object" == typeof t) {
                for (o in"string" != typeof e && (i = i || e, e = undefined), t) this.on(o, e, i, t[o], r);
                return this
            }
            if (null == i && null == n ? (n = e, i = e = undefined) : null == n && ("string" == typeof e ? (n = i, i = undefined) : (n = i, i = e, e = undefined)), !1 === n) n = d; else if (!n) return this;
            return 1 === r && (s = n, (n = function (t) {
                return rt().off(t), s.apply(this, arguments)
            }).guid = s.guid || (s.guid = rt.guid++)), this.each(function () {
                rt.event.add(this, t, n, i, e)
            })
        }, one: function (t, e, i, n) {
            return this.on(t, e, i, n, 1)
        }, off: function (t, e, i) {
            var n, r;
            if (t && t.preventDefault && t.handleObj) return n = t.handleObj, rt(t.delegateTarget).off(n.namespace ? n.origType + "." + n.namespace : n.origType, n.selector, n.handler), this;
            if ("object" != typeof t) return !1 !== e && "function" != typeof e || (i = e, e = undefined), !1 === i && (i = d), this.each(function () {
                rt.event.remove(this, t, i, e)
            });
            for (r in t) this.off(r, e, t[r]);
            return this
        }, trigger: function (t, e) {
            return this.each(function () {
                rt.event.trigger(t, e, this)
            })
        }, triggerHandler: function (t, e) {
            var i = this[0];
            if (i) return rt.event.trigger(t, e, i, !0)
        }
    });
    var Dt = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        Ot = / jQuery\d+="(?:null|\d+)"/g, Nt = new RegExp("<(?:" + Dt + ")[\\s/>]", "i"), jt = /^\s+/,
        zt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, Rt = /<([\w:]+)/,
        Bt = /<tbody/i, Ht = /<|&#?\w+;/, Wt = /<(?:script|style|link)/i, qt = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Gt = /^$|\/(?:java|ecma)script/i, Xt = /^true\/(.*)/, Yt = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, Vt = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            area: [1, "<map>", "</map>"],
            param: [1, "<object>", "</object>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: it.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
        }, Ut = m(ft).appendChild(ft.createElement("div"));
    Vt.optgroup = Vt.option, Vt.tbody = Vt.tfoot = Vt.colgroup = Vt.caption = Vt.thead, Vt.th = Vt.td, rt.extend({
        clone: function (t, e, i) {
            var n, r, o, s, a, l = rt.contains(t.ownerDocument, t);
            if (it.html5Clone || rt.isXMLDoc(t) || !Nt.test("<" + t.nodeName + ">") ? o = t.cloneNode(!0) : (Ut.innerHTML = t.outerHTML, Ut.removeChild(o = Ut.firstChild)), !(it.noCloneEvent && it.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || rt.isXMLDoc(t))) for (n = v(o), a = v(t), s = 0; null != (r = a[s]); ++s) n[s] && _(r, n[s]);
            if (e) if (i) for (a = a || v(t), n = n || v(o), s = 0; null != (r = a[s]); s++) C(r, n[s]); else C(t, o);
            return 0 < (n = v(o, "script")).length && w(n, !l && v(t, "script")), n = a = r = null, o
        }, buildFragment: function (t, e, i, n) {
            for (var r, o, s, a, l, c, h, d = t.length, u = m(e), p = [], f = 0; f < d; f++) if ((o = t[f]) || 0 === o) if ("object" === rt.type(o)) rt.merge(p, o.nodeType ? [o] : o); else if (Ht.test(o)) {
                for (a = a || u.appendChild(e.createElement("div")), l = (Rt.exec(o) || ["", ""])[1].toLowerCase(), h = Vt[l] || Vt._default, a.innerHTML = h[1] + o.replace(zt, "<$1></$2>") + h[2], r = h[0]; r--;) a = a.lastChild;
                if (!it.leadingWhitespace && jt.test(o) && p.push(e.createTextNode(jt.exec(o)[0])), !it.tbody) for (r = (o = "table" !== l || Bt.test(o) ? "<table>" !== h[1] || Bt.test(o) ? 0 : a : a.firstChild) && o.childNodes.length; r--;) rt.nodeName(c = o.childNodes[r], "tbody") && !c.childNodes.length && o.removeChild(c);
                for (rt.merge(p, a.childNodes), a.textContent = ""; a.firstChild;) a.removeChild(a.firstChild);
                a = u.lastChild
            } else p.push(e.createTextNode(o));
            for (a && u.removeChild(a), it.appendChecked || rt.grep(v(p, "input"), y), f = 0; o = p[f++];) if ((!n || -1 === rt.inArray(o, n)) && (s = rt.contains(o.ownerDocument, o), a = v(u.appendChild(o), "script"), s && w(a), i)) for (r = 0; o = a[r++];) Gt.test(o.type || "") && i.push(o);
            return a = null, u
        }, cleanData: function (t, e) {
            for (var i, n, r, o, s = 0, a = rt.expando, l = rt.cache, c = it.deleteExpando, h = rt.event.special; null != (i = t[s]); s++) if ((e || rt.acceptData(i)) && (o = (r = i[a]) && l[r])) {
                if (o.events) for (n in o.events) h[n] ? rt.event.remove(i, n) : rt.removeEvent(i, n, o.handle);
                l[r] && (delete l[r], c ? delete i[a] : typeof i.removeAttribute !== Ct ? i.removeAttribute(a) : i[a] = null, V.push(r))
            }
        }
    }), rt.fn.extend({
        text: function (t) {
            return $t(this, function (t) {
                return t === undefined ? rt.text(this) : this.empty().append((this[0] && this[0].ownerDocument || ft).createTextNode(t))
            }, null, t, arguments.length)
        }, append: function () {
            return this.domManip(arguments, function (t) {
                1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || f(this, t).appendChild(t)
            })
        }, prepend: function () {
            return this.domManip(arguments, function (t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var e = f(this, t);
                    e.insertBefore(t, e.firstChild)
                }
            })
        }, before: function () {
            return this.domManip(arguments, function (t) {
                this.parentNode && this.parentNode.insertBefore(t, this)
            })
        }, after: function () {
            return this.domManip(arguments, function (t) {
                this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
            })
        }, remove: function (t, e) {
            for (var i, n = t ? rt.filter(t, this) : this, r = 0; null != (i = n[r]); r++) e || 1 !== i.nodeType || rt.cleanData(v(i)), i.parentNode && (e && rt.contains(i.ownerDocument, i) && w(v(i, "script")), i.parentNode.removeChild(i));
            return this
        }, empty: function () {
            for (var t, e = 0; null != (t = this[e]); e++) {
                for (1 === t.nodeType && rt.cleanData(v(t, !1)); t.firstChild;) t.removeChild(t.firstChild);
                t.options && rt.nodeName(t, "select") && (t.options.length = 0)
            }
            return this
        }, clone: function (t, e) {
            return t = null != t && t, e = null == e ? t : e, this.map(function () {
                return rt.clone(this, t, e)
            })
        }, html: function (t) {
            return $t(this, function (t) {
                var e = this[0] || {}, i = 0, n = this.length;
                if (t === undefined) return 1 === e.nodeType ? e.innerHTML.replace(Ot, "") : undefined;
                if ("string" == typeof t && !Wt.test(t) && (it.htmlSerialize || !Nt.test(t)) && (it.leadingWhitespace || !jt.test(t)) && !Vt[(Rt.exec(t) || ["", ""])[1].toLowerCase()]) {
                    t = t.replace(zt, "<$1></$2>");
                    try {
                        for (; i < n; i++) 1 === (e = this[i] || {}).nodeType && (rt.cleanData(v(e, !1)), e.innerHTML = t);
                        e = 0
                    } catch (r) {
                    }
                }
                e && this.empty().append(t)
            }, null, t, arguments.length)
        }, replaceWith: function (t) {
            var e = t;
            return this.domManip(arguments, function (t) {
                e = this.parentNode, rt.cleanData(v(this)), e && e.replaceChild(t, this)
            }), e && (e.length || e.nodeType) ? this : this.remove()
        }, detach: function (t) {
            return this.remove(t, !0)
        }, domManip: function (i, n) {
            i = Q.apply([], i);
            var t, e, r, o, s, a, l = 0, c = this.length, h = this, d = c - 1, u = i[0], p = rt.isFunction(u);
            if (p || 1 < c && "string" == typeof u && !it.checkClone && qt.test(u)) return this.each(function (t) {
                var e = h.eq(t);
                p && (i[0] = u.call(this, t, e.html())), e.domManip(i, n)
            });
            if (c && (t = (a = rt.buildFragment(i, this[0].ownerDocument, !1, this)).firstChild, 1 === a.childNodes.length && (a = t), t)) {
                for (r = (o = rt.map(v(a, "script"), x)).length; l < c; l++) e = a, l !== d && (e = rt.clone(e, !0, !0), r && rt.merge(o, v(e, "script"))), n.call(this[l], e, l);
                if (r) for (s = o[o.length - 1].ownerDocument, rt.map(o, b), l = 0; l < r; l++) e = o[l], Gt.test(e.type || "") && !rt._data(e, "globalEval") && rt.contains(s, e) && (e.src ? rt._evalUrl && rt._evalUrl(e.src) : rt.globalEval((e.text || e.textContent || e.innerHTML || "").replace(Yt, "")));
                a = t = null
            }
            return this
        }
    }), rt.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (t, s) {
        rt.fn[t] = function (t) {
            for (var e, i = 0, n = [], r = rt(t), o = r.length - 1; i <= o; i++) e = i === o ? this : this.clone(!0), rt(r[i])[s](e), K.apply(n, e.get());
            return this.pushStack(n)
        }
    });
    var Qt, Kt, Zt = {};
    it.shrinkWrapBlocks = function () {
        return null != Kt ? Kt : (Kt = !1, (e = ft.getElementsByTagName("body")[0]) && e.style ? (t = ft.createElement("div"), (i = ft.createElement("div")).style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", e.appendChild(i).appendChild(t), typeof t.style.zoom !== Ct && (t.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", t.appendChild(ft.createElement("div")).style.width = "5px", Kt = 3 !== t.offsetWidth), e.removeChild(i), Kt) : void 0);
        var t, e, i
    };
    var Jt, te, ee = /^margin/, ie = new RegExp("^(" + St + ")(?!px)[a-z%]+$", "i"), ne = /^(top|right|bottom|left)$/;
    g.getComputedStyle ? (Jt = function (t) {
        return t.ownerDocument.defaultView.getComputedStyle(t, null)
    }, te = function (t, e, i) {
        var n, r, o, s, a = t.style;
        return s = (i = i || Jt(t)) ? i.getPropertyValue(e) || i[e] : undefined, i && ("" !== s || rt.contains(t.ownerDocument, t) || (s = rt.style(t, e)), ie.test(s) && ee.test(e) && (n = a.width, r = a.minWidth, o = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = i.width, a.width = n, a.minWidth = r, a.maxWidth = o)), s === undefined ? s : s + ""
    }) : ft.documentElement.currentStyle && (Jt = function (t) {
        return t.currentStyle
    }, te = function (t, e, i) {
        var n, r, o, s, a = t.style;
        return null == (s = (i = i || Jt(t)) ? i[e] : undefined) && a && a[e] && (s = a[e]), ie.test(s) && !ne.test(e) && (n = a.left, (o = (r = t.runtimeStyle) && r.left) && (r.left = t.currentStyle.left), a.left = "fontSize" === e ? "1em" : s, s = a.pixelLeft + "px", a.left = n, o && (r.left = o)), s === undefined ? s : s + "" || "auto"
    }), function () {
        function t() {
            var t, e, i, n;
            (e = ft.getElementsByTagName("body")[0]) && e.style && (t = ft.createElement("div"), (i = ft.createElement("div")).style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", e.appendChild(i).appendChild(t), t.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", r = o = !1, a = !0, g.getComputedStyle && (r = "1%" !== (g.getComputedStyle(t, null) || {}).top, o = "4px" === (g.getComputedStyle(t, null) || {width: "4px"}).width, (n = t.appendChild(ft.createElement("div"))).style.cssText = t.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", n.style.marginRight = n.style.width = "0", t.style.width = "1px", a = !parseFloat((g.getComputedStyle(n, null) || {}).marginRight)), t.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", (n = t.getElementsByTagName("td"))[0].style.cssText = "margin:0;border:0;padding:0;display:none", (s = 0 === n[0].offsetHeight) && (n[0].style.display = "", n[1].style.display = "none", s = 0 === n[0].offsetHeight), e.removeChild(i))
        }

        var e, i, n, r, o, s, a;
        (e = ft.createElement("div")).innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", (i = (n = e.getElementsByTagName("a")[0]) && n.style) && (i.cssText = "float:left;opacity:.5", it.opacity = "0.5" === i.opacity, it.cssFloat = !!i.cssFloat, e.style.backgroundClip = "content-box", e.cloneNode(!0).style.backgroundClip = "", it.clearCloneStyle = "content-box" === e.style.backgroundClip, it.boxSizing = "" === i.boxSizing || "" === i.MozBoxSizing || "" === i.WebkitBoxSizing, rt.extend(it, {
            reliableHiddenOffsets: function () {
                return null == s && t(), s
            }, boxSizingReliable: function () {
                return null == o && t(), o
            }, pixelPosition: function () {
                return null == r && t(), r
            }, reliableMarginRight: function () {
                return null == a && t(), a
            }
        }))
    }(), rt.swap = function (t, e, i, n) {
        var r, o, s = {};
        for (o in e) s[o] = t.style[o], t.style[o] = e[o];
        for (o in r = i.apply(t, n || []), e) t.style[o] = s[o];
        return r
    };
    var re = /alpha\([^)]*\)/i, oe = /opacity\s*=\s*([^)]*)/, se = /^(none|table(?!-c[ea]).+)/,
        ae = new RegExp("^(" + St + ")(.*)$", "i"), le = new RegExp("^([+-])=(" + St + ")", "i"),
        ce = {position: "absolute", visibility: "hidden", display: "block"},
        he = {letterSpacing: "0", fontWeight: "400"}, de = ["Webkit", "O", "Moz", "ms"];
    rt.extend({
        cssHooks: {
            opacity: {
                get: function (t, e) {
                    if (e) {
                        var i = te(t, "opacity");
                        return "" === i ? "1" : i
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {"float": it.cssFloat ? "cssFloat" : "styleFloat"},
        style: function (t, e, i, n) {
            if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
                var r, o, s, a = rt.camelCase(e), l = t.style;
                if (e = rt.cssProps[a] || (rt.cssProps[a] = A(l, a)), s = rt.cssHooks[e] || rt.cssHooks[a], i === undefined) return s && "get" in s && (r = s.get(t, !1, n)) !== undefined ? r : l[e];
                if ("string" === (o = typeof i) && (r = le.exec(i)) && (i = (r[1] + 1) * r[2] + parseFloat(rt.css(t, e)), o = "number"), null != i && i == i && ("number" !== o || rt.cssNumber[a] || (i += "px"), it.clearCloneStyle || "" !== i || 0 !== e.indexOf("background") || (l[e] = "inherit"), !(s && "set" in s && (i = s.set(t, i, n)) === undefined))) try {
                    l[e] = i
                } catch (c) {
                }
            }
        },
        css: function (t, e, i, n) {
            var r, o, s, a = rt.camelCase(e);
            return e = rt.cssProps[a] || (rt.cssProps[a] = A(t.style, a)), (s = rt.cssHooks[e] || rt.cssHooks[a]) && "get" in s && (o = s.get(t, !0, i)), o === undefined && (o = te(t, e, n)), "normal" === o && e in he && (o = he[e]), "" === i || i ? (r = parseFloat(o), !0 === i || rt.isNumeric(r) ? r || 0 : o) : o
        }
    }), rt.each(["height", "width"], function (t, r) {
        rt.cssHooks[r] = {
            get: function (t, e, i) {
                if (e) return se.test(rt.css(t, "display")) && 0 === t.offsetWidth ? rt.swap(t, ce, function () {
                    return P(t, r, i)
                }) : P(t, r, i)
            }, set: function (t, e, i) {
                var n = i && Jt(t);
                return M(t, e, i ? E(t, r, i, it.boxSizing && "border-box" === rt.css(t, "boxSizing", !1, n), n) : 0)
            }
        }
    }), it.opacity || (rt.cssHooks.opacity = {
        get: function (t, e) {
            return oe.test((e && t.currentStyle ? t.currentStyle.filter : t.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : e ? "1" : ""
        }, set: function (t, e) {
            var i = t.style, n = t.currentStyle, r = rt.isNumeric(e) ? "alpha(opacity=" + 100 * e + ")" : "",
                o = n && n.filter || i.filter || "";
            ((i.zoom = 1) <= e || "" === e) && "" === rt.trim(o.replace(re, "")) && i.removeAttribute && (i.removeAttribute("filter"), "" === e || n && !n.filter) || (i.filter = re.test(o) ? o.replace(re, r) : o + " " + r)
        }
    }), rt.cssHooks.marginRight = T(it.reliableMarginRight, function (t, e) {
        if (e) return rt.swap(t, {display: "inline-block"}, te, [t, "marginRight"])
    }), rt.each({margin: "", padding: "", border: "Width"}, function (r, o) {
        rt.cssHooks[r + o] = {
            expand: function (t) {
                for (var e = 0, i = {}, n = "string" == typeof t ? t.split(" ") : [t]; e < 4; e++) i[r + Tt[e] + o] = n[e] || n[e - 2] || n[0];
                return i
            }
        }, ee.test(r) || (rt.cssHooks[r + o].set = M)
    }), rt.fn.extend({
        css: function (t, e) {
            return $t(this, function (t, e, i) {
                var n, r, o = {}, s = 0;
                if (rt.isArray(e)) {
                    for (n = Jt(t), r = e.length; s < r; s++) o[e[s]] = rt.css(t, e[s], !1, n);
                    return o
                }
                return i !== undefined ? rt.style(t, e, i) : rt.css(t, e)
            }, t, e, 1 < arguments.length)
        }, show: function () {
            return $(this, !0)
        }, hide: function () {
            return $(this)
        }, toggle: function (t) {
            return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function () {
                At(this) ? rt(this).show() : rt(this).hide()
            })
        }
    }), (rt.Tween = L).prototype = {
        constructor: L, init: function (t, e, i, n, r, o) {
            this.elem = t, this.prop = i, this.easing = r || "swing", this.options = e, this.start = this.now = this.cur(), this.end = n, this.unit = o || (rt.cssNumber[i] ? "" : "px")
        }, cur: function () {
            var t = L.propHooks[this.prop];
            return t && t.get ? t.get(this) : L.propHooks._default.get(this)
        }, run: function (t) {
            var e, i = L.propHooks[this.prop];
            return this.options.duration ? this.pos = e = rt.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : this.pos = e = t, this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), i && i.set ? i.set(this) : L.propHooks._default.set(this), this
        }
    }, L.prototype.init.prototype = L.prototype, L.propHooks = {
        _default: {
            get: function (t) {
                var e;
                return null == t.elem[t.prop] || t.elem.style && null != t.elem.style[t.prop] ? (e = rt.css(t.elem, t.prop, "")) && "auto" !== e ? e : 0 : t.elem[t.prop]
            }, set: function (t) {
                rt.fx.step[t.prop] ? rt.fx.step[t.prop](t) : t.elem.style && (null != t.elem.style[rt.cssProps[t.prop]] || rt.cssHooks[t.prop]) ? rt.style(t.elem, t.prop, t.now + t.unit) : t.elem[t.prop] = t.now
            }
        }
    }, L.propHooks.scrollTop = L.propHooks.scrollLeft = {
        set: function (t) {
            t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
        }
    }, rt.easing = {
        linear: function (t) {
            return t
        }, swing: function (t) {
            return .5 - Math.cos(t * Math.PI) / 2
        }
    }, rt.fx = L.prototype.init, rt.fx.step = {};
    var ue, pe, fe, ge, me, ve, ye, xe = /^(?:toggle|show|hide)$/,
        be = new RegExp("^(?:([+-])=|)(" + St + ")([a-z%]*)$", "i"), we = /queueHooks$/, Ce = [O], _e = {
            "*": [function (t, e) {
                var i = this.createTween(t, e), n = i.cur(), r = be.exec(e), o = r && r[3] || (rt.cssNumber[t] ? "" : "px"),
                    s = (rt.cssNumber[t] || "px" !== o && +n) && be.exec(rt.css(i.elem, t)), a = 1, l = 20;
                if (s && s[3] !== o) for (o = o || s[3], r = r || [], s = +n || 1; s /= a = a || ".5", rt.style(i.elem, t, s + o), a !== (a = i.cur() / n) && 1 !== a && --l;) ;
                return r && (s = i.start = +s || +n || 0, i.unit = o, i.end = r[1] ? s + (r[1] + 1) * r[2] : +r[2]), i
            }]
        };
    rt.Animation = rt.extend(j, {
        tweener: function (t, e) {
            rt.isFunction(t) ? (e = t, t = ["*"]) : t = t.split(" ");
            for (var i, n = 0, r = t.length; n < r; n++) i = t[n], _e[i] = _e[i] || [], _e[i].unshift(e)
        }, prefilter: function (t, e) {
            e ? Ce.unshift(t) : Ce.push(t)
        }
    }), rt.speed = function (t, e, i) {
        var n = t && "object" == typeof t ? rt.extend({}, t) : {
            complete: i || !i && e || rt.isFunction(t) && t,
            duration: t,
            easing: i && e || e && !rt.isFunction(e) && e
        };
        return n.duration = rt.fx.off ? 0 : "number" == typeof n.duration ? n.duration : n.duration in rt.fx.speeds ? rt.fx.speeds[n.duration] : rt.fx.speeds._default, null != n.queue && !0 !== n.queue || (n.queue = "fx"), n.old = n.complete, n.complete = function () {
            rt.isFunction(n.old) && n.old.call(this), n.queue && rt.dequeue(this, n.queue)
        }, n
    }, rt.fn.extend({
        fadeTo: function (t, e, i, n) {
            return this.filter(At).css("opacity", 0).show().end().animate({opacity: e}, t, i, n)
        }, animate: function (e, t, i, n) {
            var r = rt.isEmptyObject(e), o = rt.speed(t, i, n), s = function () {
                var t = j(this, rt.extend({}, e), o);
                (r || rt._data(this, "finish")) && t.stop(!0)
            };
            return s.finish = s, r || !1 === o.queue ? this.each(s) : this.queue(o.queue, s)
        }, stop: function (r, t, o) {
            var s = function (t) {
                var e = t.stop;
                delete t.stop, e(o)
            };
            return "string" != typeof r && (o = t, t = r, r = undefined), t && !1 !== r && this.queue(r || "fx", []), this.each(function () {
                var t = !0, e = null != r && r + "queueHooks", i = rt.timers, n = rt._data(this);
                if (e) n[e] && n[e].stop && s(n[e]); else for (e in n) n[e] && n[e].stop && we.test(e) && s(n[e]);
                for (e = i.length; e--;) i[e].elem !== this || null != r && i[e].queue !== r || (i[e].anim.stop(o), t = !1, i.splice(e, 1));
                !t && o || rt.dequeue(this, r)
            })
        }, finish: function (s) {
            return !1 !== s && (s = s || "fx"), this.each(function () {
                var t, e = rt._data(this), i = e[s + "queue"], n = e[s + "queueHooks"], r = rt.timers,
                    o = i ? i.length : 0;
                for (e.finish = !0, rt.queue(this, s, []), n && n.stop && n.stop.call(this, !0), t = r.length; t--;) r[t].elem === this && r[t].queue === s && (r[t].anim.stop(!0), r.splice(t, 1));
                for (t = 0; t < o; t++) i[t] && i[t].finish && i[t].finish.call(this);
                delete e.finish
            })
        }
    }), rt.each(["toggle", "show", "hide"], function (t, n) {
        var r = rt.fn[n];
        rt.fn[n] = function (t, e, i) {
            return null == t || "boolean" == typeof t ? r.apply(this, arguments) : this.animate(F(n, !0), t, e, i)
        }
    }), rt.each({
        slideDown: F("show"),
        slideUp: F("hide"),
        slideToggle: F("toggle"),
        fadeIn: {opacity: "show"},
        fadeOut: {opacity: "hide"},
        fadeToggle: {opacity: "toggle"}
    }, function (t, n) {
        rt.fn[t] = function (t, e, i) {
            return this.animate(n, t, e, i)
        }
    }), rt.timers = [], rt.fx.tick = function () {
        var t, e = rt.timers, i = 0;
        for (ue = rt.now(); i < e.length; i++) (t = e[i])() || e[i] !== t || e.splice(i--, 1);
        e.length || rt.fx.stop(), ue = undefined
    }, rt.fx.timer = function (t) {
        rt.timers.push(t), t() ? rt.fx.start() : rt.timers.pop()
    }, rt.fx.interval = 13, rt.fx.start = function () {
        pe || (pe = setInterval(rt.fx.tick, rt.fx.interval))
    }, rt.fx.stop = function () {
        clearInterval(pe), pe = null
    }, rt.fx.speeds = {slow: 600, fast: 200, _default: 400}, rt.fn.delay = function (n, t) {
        return n = rt.fx && rt.fx.speeds[n] || n, t = t || "fx", this.queue(t, function (t, e) {
            var i = setTimeout(t, n);
            e.stop = function () {
                clearTimeout(i)
            }
        })
    }, (ge = ft.createElement("div")).setAttribute("className", "t"), ge.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", ve = ge.getElementsByTagName("a")[0], ye = (me = ft.createElement("select")).appendChild(ft.createElement("option")), fe = ge.getElementsByTagName("input")[0], ve.style.cssText = "top:1px", it.getSetAttribute = "t" !== ge.className, it.style = /top/.test(ve.getAttribute("style")), it.hrefNormalized = "/a" === ve.getAttribute("href"), it.checkOn = !!fe.value, it.optSelected = ye.selected, it.enctype = !!ft.createElement("form").enctype, me.disabled = !0, it.optDisabled = !ye.disabled, (fe = ft.createElement("input")).setAttribute("value", ""), it.input = "" === fe.getAttribute("value"), fe.value = "t", fe.setAttribute("type", "radio"), it.radioValue = "t" === fe.value;
    var ke = /\r/g;
    rt.fn.extend({
        val: function (i) {
            var n, t, r, e = this[0];
            return arguments.length ? (r = rt.isFunction(i), this.each(function (t) {
                var e;
                1 === this.nodeType && (null == (e = r ? i.call(this, t, rt(this).val()) : i) ? e = "" : "number" == typeof e ? e += "" : rt.isArray(e) && (e = rt.map(e, function (t) {
                    return null == t ? "" : t + ""
                })), (n = rt.valHooks[this.type] || rt.valHooks[this.nodeName.toLowerCase()]) && "set" in n && n.set(this, e, "value") !== undefined || (this.value = e))
            })) : e ? (n = rt.valHooks[e.type] || rt.valHooks[e.nodeName.toLowerCase()]) && "get" in n && (t = n.get(e, "value")) !== undefined ? t : "string" == typeof (t = e.value) ? t.replace(ke, "") : null == t ? "" : t : void 0
        }
    }), rt.extend({
        valHooks: {
            option: {
                get: function (t) {
                    var e = rt.find.attr(t, "value");
                    return null != e ? e : rt.trim(rt.text(t))
                }
            }, select: {
                get: function (t) {
                    for (var e, i, n = t.options, r = t.selectedIndex, o = "select-one" === t.type || r < 0, s = o ? null : [], a = o ? r + 1 : n.length, l = r < 0 ? a : o ? r : 0; l < a; l++) if (((i = n[l]).selected || l === r) && (it.optDisabled ? !i.disabled : null === i.getAttribute("disabled")) && (!i.parentNode.disabled || !rt.nodeName(i.parentNode, "optgroup"))) {
                        if (e = rt(i).val(), o) return e;
                        s.push(e)
                    }
                    return s
                }, set: function (t, e) {
                    for (var i, n, r = t.options, o = rt.makeArray(e), s = r.length; s--;) if (n = r[s], 0 <= rt.inArray(rt.valHooks.option.get(n), o)) try {
                        n.selected = i = !0
                    } catch (a) {
                        n.scrollHeight
                    } else n.selected = !1;
                    return i || (t.selectedIndex = -1), r
                }
            }
        }
    }), rt.each(["radio", "checkbox"], function () {
        rt.valHooks[this] = {
            set: function (t, e) {
                if (rt.isArray(e)) return t.checked = 0 <= rt.inArray(rt(t).val(), e)
            }
        }, it.checkOn || (rt.valHooks[this].get = function (t) {
            return null === t.getAttribute("value") ? "on" : t.value
        })
    });
    var Se, Te, Ae = rt.expr.attrHandle, $e = /^(?:checked|selected)$/i, Me = it.getSetAttribute, Ee = it.input;
    rt.fn.extend({
        attr: function (t, e) {
            return $t(this, rt.attr, t, e, 1 < arguments.length)
        }, removeAttr: function (t) {
            return this.each(function () {
                rt.removeAttr(this, t)
            })
        }
    }), rt.extend({
        attr: function (t, e, i) {
            var n, r, o = t.nodeType;
            if (t && 3 !== o && 8 !== o && 2 !== o) return typeof t.getAttribute === Ct ? rt.prop(t, e, i) : (1 === o && rt.isXMLDoc(t) || (e = e.toLowerCase(), n = rt.attrHooks[e] || (rt.expr.match.bool.test(e) ? Te : Se)), i === undefined ? n && "get" in n && null !== (r = n.get(t, e)) ? r : null == (r = rt.find.attr(t, e)) ? undefined : r : null !== i ? n && "set" in n && (r = n.set(t, i, e)) !== undefined ? r : (t.setAttribute(e, i + ""), i) : void rt.removeAttr(t, e))
        }, removeAttr: function (t, e) {
            var i, n, r = 0, o = e && e.match(xt);
            if (o && 1 === t.nodeType) for (; i = o[r++];) n = rt.propFix[i] || i, rt.expr.match.bool.test(i) ? Ee && Me || !$e.test(i) ? t[n] = !1 : t[rt.camelCase("default-" + i)] = t[n] = !1 : rt.attr(t, i, ""), t.removeAttribute(Me ? i : n)
        }, attrHooks: {
            type: {
                set: function (t, e) {
                    if (!it.radioValue && "radio" === e && rt.nodeName(t, "input")) {
                        var i = t.value;
                        return t.setAttribute("type", e), i && (t.value = i), e
                    }
                }
            }
        }
    }), Te = {
        set: function (t, e, i) {
            return !1 === e ? rt.removeAttr(t, i) : Ee && Me || !$e.test(i) ? t.setAttribute(!Me && rt.propFix[i] || i, i) : t[rt.camelCase("default-" + i)] = t[i] = !0, i
        }
    }, rt.each(rt.expr.match.bool.source.match(/\w+/g), function (t, e) {
        var o = Ae[e] || rt.find.attr;
        Ae[e] = Ee && Me || !$e.test(e) ? function (t, e, i) {
            var n, r;
            return i || (r = Ae[e], Ae[e] = n, n = null != o(t, e, i) ? e.toLowerCase() : null, Ae[e] = r), n
        } : function (t, e, i) {
            if (!i) return t[rt.camelCase("default-" + e)] ? e.toLowerCase() : null
        }
    }), Ee && Me || (rt.attrHooks.value = {
        set: function (t, e, i) {
            if (!rt.nodeName(t, "input")) return Se && Se.set(t, e, i);
            t.defaultValue = e
        }
    }), Me || (Se = {
        set: function (t, e, i) {
            var n = t.getAttributeNode(i);
            if (n || t.setAttributeNode(n = t.ownerDocument.createAttribute(i)), n.value = e += "", "value" === i || e === t.getAttribute(i)) return e
        }
    }, Ae.id = Ae.name = Ae.coords = function (t, e, i) {
        var n;
        if (!i) return (n = t.getAttributeNode(e)) && "" !== n.value ? n.value : null
    }, rt.valHooks.button = {
        get: function (t, e) {
            var i = t.getAttributeNode(e);
            if (i && i.specified) return i.value
        }, set: Se.set
    }, rt.attrHooks.contenteditable = {
        set: function (t, e, i) {
            Se.set(t, "" !== e && e, i)
        }
    }, rt.each(["width", "height"], function (t, i) {
        rt.attrHooks[i] = {
            set: function (t, e) {
                if ("" === e) return t.setAttribute(i, "auto"), e
            }
        }
    })), it.style || (rt.attrHooks.style = {
        get: function (t) {
            return t.style.cssText || undefined
        }, set: function (t, e) {
            return t.style.cssText = e + ""
        }
    });
    var Pe = /^(?:input|select|textarea|button|object)$/i, Le = /^(?:a|area)$/i;
    rt.fn.extend({
        prop: function (t, e) {
            return $t(this, rt.prop, t, e, 1 < arguments.length)
        }, removeProp: function (e) {
            return e = rt.propFix[e] || e, this.each(function () {
                try {
                    this[e] = undefined, delete this[e]
                } catch (t) {
                }
            })
        }
    }), rt.extend({
        propFix: {"for": "htmlFor", "class": "className"}, prop: function (t, e, i) {
            var n, r, o = t.nodeType;
            if (t && 3 !== o && 8 !== o && 2 !== o) return (1 !== o || !rt.isXMLDoc(t)) && (e = rt.propFix[e] || e, r = rt.propHooks[e]), i !== undefined ? r && "set" in r && (n = r.set(t, i, e)) !== undefined ? n : t[e] = i : r && "get" in r && null !== (n = r.get(t, e)) ? n : t[e]
        }, propHooks: {
            tabIndex: {
                get: function (t) {
                    var e = rt.find.attr(t, "tabindex");
                    return e ? parseInt(e, 10) : Pe.test(t.nodeName) || Le.test(t.nodeName) && t.href ? 0 : -1
                }
            }
        }
    }), it.hrefNormalized || rt.each(["href", "src"], function (t, e) {
        rt.propHooks[e] = {
            get: function (t) {
                return t.getAttribute(e, 4)
            }
        }
    }), it.optSelected || (rt.propHooks.selected = {
        get: function (t) {
            var e = t.parentNode;
            return e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex), null
        }
    }), rt.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
        rt.propFix[this.toLowerCase()] = this
    }), it.enctype || (rt.propFix.enctype = "encoding");
    var Ie = /[\t\r\n\f]/g;
    rt.fn.extend({
        addClass: function (e) {
            var t, i, n, r, o, s, a = 0, l = this.length, c = "string" == typeof e && e;
            if (rt.isFunction(e)) return this.each(function (t) {
                rt(this).addClass(e.call(this, t, this.className))
            });
            if (c) for (t = (e || "").match(xt) || []; a < l; a++) if (n = 1 === (i = this[a]).nodeType && (i.className ? (" " + i.className + " ").replace(Ie, " ") : " ")) {
                for (o = 0; r = t[o++];) n.indexOf(" " + r + " ") < 0 && (n += r + " ");
                s = rt.trim(n), i.className !== s && (i.className = s)
            }
            return this
        }, removeClass: function (e) {
            var t, i, n, r, o, s, a = 0, l = this.length, c = 0 === arguments.length || "string" == typeof e && e;
            if (rt.isFunction(e)) return this.each(function (t) {
                rt(this).removeClass(e.call(this, t, this.className))
            });
            if (c) for (t = (e || "").match(xt) || []; a < l; a++) if (n = 1 === (i = this[a]).nodeType && (i.className ? (" " + i.className + " ").replace(Ie, " ") : "")) {
                for (o = 0; r = t[o++];) for (; 0 <= n.indexOf(" " + r + " ");) n = n.replace(" " + r + " ", " ");
                s = e ? rt.trim(n) : "", i.className !== s && (i.className = s)
            }
            return this
        }, toggleClass: function (r, e) {
            var o = typeof r;
            return "boolean" == typeof e && "string" === o ? e ? this.addClass(r) : this.removeClass(r) : rt.isFunction(r) ? this.each(function (t) {
                rt(this).toggleClass(r.call(this, t, this.className, e), e)
            }) : this.each(function () {
                if ("string" === o) for (var t, e = 0, i = rt(this), n = r.match(xt) || []; t = n[e++];) i.hasClass(t) ? i.removeClass(t) : i.addClass(t); else o !== Ct && "boolean" !== o || (this.className && rt._data(this, "__className__", this.className), this.className = this.className || !1 === r ? "" : rt._data(this, "__className__") || "")
            })
        }, hasClass: function (t) {
            for (var e = " " + t + " ", i = 0, n = this.length; i < n; i++) if (1 === this[i].nodeType && 0 <= (" " + this[i].className + " ").replace(Ie, " ").indexOf(e)) return !0;
            return !1
        }
    }), rt.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (t, i) {
        rt.fn[i] = function (t, e) {
            return 0 < arguments.length ? this.on(i, null, t, e) : this.trigger(i)
        }
    }), rt.fn.extend({
        hover: function (t, e) {
            return this.mouseenter(t).mouseleave(e || t)
        }, bind: function (t, e, i) {
            return this.on(t, null, e, i)
        }, unbind: function (t, e) {
            return this.off(t, null, e)
        }, delegate: function (t, e, i, n) {
            return this.on(e, t, i, n)
        }, undelegate: function (t, e, i) {
            return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", i)
        }
    });
    var Fe = rt.now(), De = /\?/,
        Oe = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    rt.parseJSON = function (t) {
        if (g.JSON && g.JSON.parse) return g.JSON.parse(t + "");
        var r, o = null, e = rt.trim(t + "");
        return e && !rt.trim(e.replace(Oe, function (t, e, i, n) {
            return r && e && (o = 0), 0 === o ? t : (r = i || e, o += !n - !i, "")
        })) ? Function("return " + e)() : rt.error("Invalid JSON: " + t)
    }, rt.parseXML = function (t) {
        var e;
        if (!t || "string" != typeof t) return null;
        try {
            g.DOMParser ? e = (new DOMParser).parseFromString(t, "text/xml") : ((e = new ActiveXObject("Microsoft.XMLDOM")).async = "false", e.loadXML(t))
        } catch (i) {
            e = undefined
        }
        return e && e.documentElement && !e.getElementsByTagName("parsererror").length || rt.error("Invalid XML: " + t), e
    };
    var Ne, je, ze = /#.*$/, Re = /([?&])_=[^&]*/, Be = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
        He = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, We = /^(?:GET|HEAD)$/, qe = /^\/\//,
        Ge = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, Xe = {}, Ye = {}, Ve = "*/".concat("*");
    try {
        je = location.href
    } catch (ci) {
        (je = ft.createElement("a")).href = "", je = je.href
    }
    Ne = Ge.exec(je.toLowerCase()) || [], rt.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: je,
            type: "GET",
            isLocal: He.test(Ne[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Ve,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {xml: /xml/, html: /html/, json: /json/},
            responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
            converters: {"* text": String, "text html": !0, "text json": rt.parseJSON, "text xml": rt.parseXML},
            flatOptions: {url: !0, context: !0}
        },
        ajaxSetup: function (t, e) {
            return e ? B(B(t, rt.ajaxSettings), e) : B(rt.ajaxSettings, t)
        },
        ajaxPrefilter: z(Xe),
        ajaxTransport: z(Ye),
        ajax: function (t, e) {
            function i(t, e, i, n) {
                var r, o, s, a, l, c = e;
                2 !== w && (w = 2, u && clearTimeout(u), f = undefined, d = n || "", C.readyState = 0 < t ? 4 : 0, r = 200 <= t && t < 300 || 304 === t, i && (a = H(g, C, i)), a = W(g, a, C, r), r ? (g.ifModified && ((l = C.getResponseHeader("Last-Modified")) && (rt.lastModified[h] = l), (l = C.getResponseHeader("etag")) && (rt.etag[h] = l)), 204 === t || "HEAD" === g.type ? c = "nocontent" : 304 === t ? c = "notmodified" : (c = a.state, o = a.data, r = !(s = a.error))) : (s = c, !t && c || (c = "error", t < 0 && (t = 0))), C.status = t, C.statusText = (e || c) + "", r ? y.resolveWith(m, [o, c, C]) : y.rejectWith(m, [C, c, s]), C.statusCode(b), b = undefined, p && v.trigger(r ? "ajaxSuccess" : "ajaxError", [C, g, r ? o : s]), x.fireWith(m, [C, c]), p && (v.trigger("ajaxComplete", [C, g]), --rt.active || rt.event.trigger("ajaxStop")))
            }

            "object" == typeof t && (e = t, t = undefined), e = e || {};
            var n, r, h, d, u, p, f, o, g = rt.ajaxSetup({}, e), m = g.context || g,
                v = g.context && (m.nodeType || m.jquery) ? rt(m) : rt.event, y = rt.Deferred(),
                x = rt.Callbacks("once memory"), b = g.statusCode || {}, s = {}, a = {}, w = 0, l = "canceled", C = {
                    readyState: 0, getResponseHeader: function (t) {
                        var e;
                        if (2 === w) {
                            if (!o) for (o = {}; e = Be.exec(d);) o[e[1].toLowerCase()] = e[2];
                            e = o[t.toLowerCase()]
                        }
                        return null == e ? null : e
                    }, getAllResponseHeaders: function () {
                        return 2 === w ? d : null
                    }, setRequestHeader: function (t, e) {
                        var i = t.toLowerCase();
                        return w || (t = a[i] = a[i] || t, s[t] = e), this
                    }, overrideMimeType: function (t) {
                        return w || (g.mimeType = t), this
                    }, statusCode: function (t) {
                        var e;
                        if (t) if (w < 2) for (e in t) b[e] = [b[e], t[e]]; else C.always(t[C.status]);
                        return this
                    }, abort: function (t) {
                        var e = t || l;
                        return f && f.abort(e), i(0, e), this
                    }
                };
            if (y.promise(C).complete = x.add, C.success = C.done, C.error = C.fail, g.url = ((t || g.url || je) + "").replace(ze, "").replace(qe, Ne[1] + "//"), g.type = e.method || e.type || g.method || g.type, g.dataTypes = rt.trim(g.dataType || "*").toLowerCase().match(xt) || [""], null == g.crossDomain && (n = Ge.exec(g.url.toLowerCase()), g.crossDomain = !(!n || n[1] === Ne[1] && n[2] === Ne[2] && (n[3] || ("http:" === n[1] ? "80" : "443")) === (Ne[3] || ("http:" === Ne[1] ? "80" : "443")))), g.data && g.processData && "string" != typeof g.data && (g.data = rt.param(g.data, g.traditional)), R(Xe, g, e, C), 2 === w) return C;
            for (r in(p = g.global) && 0 == rt.active++ && rt.event.trigger("ajaxStart"), g.type = g.type.toUpperCase(), g.hasContent = !We.test(g.type), h = g.url, g.hasContent || (g.data && (h = g.url += (De.test(h) ? "&" : "?") + g.data, delete g.data), !1 === g.cache && (g.url = Re.test(h) ? h.replace(Re, "$1_=" + Fe++) : h + (De.test(h) ? "&" : "?") + "_=" + Fe++)), g.ifModified && (rt.lastModified[h] && C.setRequestHeader("If-Modified-Since", rt.lastModified[h]), rt.etag[h] && C.setRequestHeader("If-None-Match", rt.etag[h])), (g.data && g.hasContent && !1 !== g.contentType || e.contentType) && C.setRequestHeader("Content-Type", g.contentType), C.setRequestHeader("Accept", g.dataTypes[0] && g.accepts[g.dataTypes[0]] ? g.accepts[g.dataTypes[0]] + ("*" !== g.dataTypes[0] ? ", " + Ve + "; q=0.01" : "") : g.accepts["*"]), g.headers) C.setRequestHeader(r, g.headers[r]);
            if (g.beforeSend && (!1 === g.beforeSend.call(m, C, g) || 2 === w)) return C.abort();
            for (r in l = "abort", {success: 1, error: 1, complete: 1}) C[r](g[r]);
            if (f = R(Ye, g, e, C)) {
                C.readyState = 1, p && v.trigger("ajaxSend", [C, g]), g.async && 0 < g.timeout && (u = setTimeout(function () {
                    C.abort("timeout")
                }, g.timeout));
                try {
                    w = 1, f.send(s, i)
                } catch (ci) {
                    if (!(w < 2)) throw ci;
                    i(-1, ci)
                }
            } else i(-1, "No Transport");
            return C
        },
        getJSON: function (t, e, i) {
            return rt.get(t, e, i, "json")
        },
        getScript: function (t, e) {
            return rt.get(t, undefined, e, "script")
        }
    }), rt.each(["get", "post"], function (t, r) {
        rt[r] = function (t, e, i, n) {
            return rt.isFunction(e) && (n = n || i, i = e, e = undefined), rt.ajax({
                url: t,
                type: r,
                dataType: n,
                data: e,
                success: i
            })
        }
    }), rt.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (t, e) {
        rt.fn[e] = function (t) {
            return this.on(e, t)
        }
    }), rt._evalUrl = function (t) {
        return rt.ajax({url: t, type: "GET", dataType: "script", async: !1, global: !1, "throws": !0})
    }, rt.fn.extend({
        wrapAll: function (e) {
            if (rt.isFunction(e)) return this.each(function (t) {
                rt(this).wrapAll(e.call(this, t))
            });
            if (this[0]) {
                var t = rt(e, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
                    for (var t = this; t.firstChild && 1 === t.firstChild.nodeType;) t = t.firstChild;
                    return t
                }).append(this)
            }
            return this
        }, wrapInner: function (i) {
            return rt.isFunction(i) ? this.each(function (t) {
                rt(this).wrapInner(i.call(this, t))
            }) : this.each(function () {
                var t = rt(this), e = t.contents();
                e.length ? e.wrapAll(i) : t.append(i)
            })
        }, wrap: function (e) {
            var i = rt.isFunction(e);
            return this.each(function (t) {
                rt(this).wrapAll(i ? e.call(this, t) : e)
            })
        }, unwrap: function () {
            return this.parent().each(function () {
                rt.nodeName(this, "body") || rt(this).replaceWith(this.childNodes)
            }).end()
        }
    }), rt.expr.filters.hidden = function (t) {
        return t.offsetWidth <= 0 && t.offsetHeight <= 0 || !it.reliableHiddenOffsets() && "none" === (t.style && t.style.display || rt.css(t, "display"))
    }, rt.expr.filters.visible = function (t) {
        return !rt.expr.filters.hidden(t)
    };
    var Ue = /%20/g, Qe = /\[\]$/, Ke = /\r?\n/g, Ze = /^(?:submit|button|image|reset|file)$/i,
        Je = /^(?:input|select|textarea|keygen)/i;
    rt.param = function (t, e) {
        var i, n = [], r = function (t, e) {
            e = rt.isFunction(e) ? e() : null == e ? "" : e, n[n.length] = encodeURIComponent(t) + "=" + encodeURIComponent(e)
        };
        if (e === undefined && (e = rt.ajaxSettings && rt.ajaxSettings.traditional), rt.isArray(t) || t.jquery && !rt.isPlainObject(t)) rt.each(t, function () {
            r(this.name, this.value)
        }); else for (i in t) q(i, t[i], e, r);
        return n.join("&").replace(Ue, "+")
    }, rt.fn.extend({
        serialize: function () {
            return rt.param(this.serializeArray())
        }, serializeArray: function () {
            return this.map(function () {
                var t = rt.prop(this, "elements");
                return t ? rt.makeArray(t) : this
            }).filter(function () {
                var t = this.type;
                return this.name && !rt(this).is(":disabled") && Je.test(this.nodeName) && !Ze.test(t) && (this.checked || !Mt.test(t))
            }).map(function (t, e) {
                var i = rt(this).val();
                return null == i ? null : rt.isArray(i) ? rt.map(i, function (t) {
                    return {name: e.name, value: t.replace(Ke, "\r\n")}
                }) : {name: e.name, value: i.replace(Ke, "\r\n")}
            }).get()
        }
    }), rt.ajaxSettings.xhr = g.ActiveXObject !== undefined ? function () {
        return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && G() || X()
    } : G;
    var ti = 0, ei = {}, ii = rt.ajaxSettings.xhr();
    g.ActiveXObject && rt(g).on("unload", function () {
        for (var t in ei) ei[t](undefined, !0)
    }), it.cors = !!ii && "withCredentials" in ii, (ii = it.ajax = !!ii) && rt.ajaxTransport(function (l) {
        var c;
        if (!l.crossDomain || it.cors) return {
            send: function (t, o) {
                var e, s = l.xhr(), a = ++ti;
                if (s.open(l.type, l.url, l.async, l.username, l.password), l.xhrFields) for (e in l.xhrFields) s[e] = l.xhrFields[e];
                for (e in l.mimeType && s.overrideMimeType && s.overrideMimeType(l.mimeType), l.crossDomain || t["X-Requested-With"] || (t["X-Requested-With"] = "XMLHttpRequest"), t) t[e] !== undefined && s.setRequestHeader(e, t[e] + "");
                s.send(l.hasContent && l.data || null), c = function (t, e) {
                    var i, n, r;
                    if (c && (e || 4 === s.readyState)) if (delete ei[a], c = undefined, s.onreadystatechange = rt.noop, e) 4 !== s.readyState && s.abort(); else {
                        r = {}, i = s.status, "string" == typeof s.responseText && (r.text = s.responseText);
                        try {
                            n = s.statusText
                        } catch (ci) {
                            n = ""
                        }
                        i || !l.isLocal || l.crossDomain ? 1223 === i && (i = 204) : i = r.text ? 200 : 404
                    }
                    r && o(i, n, r, s.getAllResponseHeaders())
                }, l.async ? 4 === s.readyState ? setTimeout(c) : s.onreadystatechange = ei[a] = c : c()
            }, abort: function () {
                c && c(undefined, !0)
            }
        }
    }), rt.ajaxSetup({
        accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
        contents: {script: /(?:java|ecma)script/},
        converters: {
            "text script": function (t) {
                return rt.globalEval(t), t
            }
        }
    }), rt.ajaxPrefilter("script", function (t) {
        t.cache === undefined && (t.cache = !1), t.crossDomain && (t.type = "GET", t.global = !1)
    }), rt.ajaxTransport("script", function (e) {
        if (e.crossDomain) {
            var n, r = ft.head || rt("head")[0] || ft.documentElement;
            return {
                send: function (t, i) {
                    (n = ft.createElement("script")).async = !0, e.scriptCharset && (n.charset = e.scriptCharset), n.src = e.url, n.onload = n.onreadystatechange = function (t, e) {
                        (e || !n.readyState || /loaded|complete/.test(n.readyState)) && (n.onload = n.onreadystatechange = null, n.parentNode && n.parentNode.removeChild(n), n = null, e || i(200, "success"))
                    }, r.insertBefore(n, r.firstChild)
                }, abort: function () {
                    n && n.onload(undefined, !0)
                }
            }
        }
    });
    var ni = [], ri = /(=)\?(?=&|$)|\?\?/;
    rt.ajaxSetup({
        jsonp: "callback", jsonpCallback: function () {
            var t = ni.pop() || rt.expando + "_" + Fe++;
            return this[t] = !0, t
        }
    }), rt.ajaxPrefilter("json jsonp", function (t, e, i) {
        var n, r, o,
            s = !1 !== t.jsonp && (ri.test(t.url) ? "url" : "string" == typeof t.data && !(t.contentType || "").indexOf("application/x-www-form-urlencoded") && ri.test(t.data) && "data");
        if (s || "jsonp" === t.dataTypes[0]) return n = t.jsonpCallback = rt.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, s ? t[s] = t[s].replace(ri, "$1" + n) : !1 !== t.jsonp && (t.url += (De.test(t.url) ? "&" : "?") + t.jsonp + "=" + n), t.converters["script json"] = function () {
            return o || rt.error(n + " was not called"), o[0]
        }, t.dataTypes[0] = "json", r = g[n], g[n] = function () {
            o = arguments
        }, i.always(function () {
            g[n] = r, t[n] && (t.jsonpCallback = e.jsonpCallback, ni.push(n)), o && rt.isFunction(r) && r(o[0]), o = r = undefined
        }), "script"
    }), rt.parseHTML = function (t, e, i) {
        if (!t || "string" != typeof t) return null;
        "boolean" == typeof e && (i = e, e = !1), e = e || ft;
        var n = dt.exec(t), r = !i && [];
        return n ? [e.createElement(n[1])] : (n = rt.buildFragment([t], e, r), r && r.length && rt(r).remove(), rt.merge([], n.childNodes))
    };
    var oi = rt.fn.load;
    rt.fn.load = function (t, e, i) {
        if ("string" != typeof t && oi) return oi.apply(this, arguments);
        var n, r, o, s = this, a = t.indexOf(" ");
        return 0 <= a && (n = rt.trim(t.slice(a, t.length)), t = t.slice(0, a)), rt.isFunction(e) ? (i = e, e = undefined) : e && "object" == typeof e && (o = "POST"), 0 < s.length && rt.ajax({
            url: t,
            type: o,
            dataType: "html",
            data: e
        }).done(function (t) {
            r = arguments, s.html(n ? rt("<div>").append(rt.parseHTML(t)).find(n) : t)
        }).complete(i && function (t, e) {
            s.each(i, r || [t.responseText, e, t])
        }), this
    }, rt.expr.filters.animated = function (e) {
        return rt.grep(rt.timers, function (t) {
            return e === t.elem
        }).length
    };
    var si = g.document.documentElement;
    rt.offset = {
        setOffset: function (t, e, i) {
            var n, r, o, s, a, l, c = rt.css(t, "position"), h = rt(t), d = {};
            "static" === c && (t.style.position = "relative"), a = h.offset(), o = rt.css(t, "top"), l = rt.css(t, "left"), ("absolute" === c || "fixed" === c) && -1 < rt.inArray("auto", [o, l]) ? (s = (n = h.position()).top, r = n.left) : (s = parseFloat(o) || 0, r = parseFloat(l) || 0), rt.isFunction(e) && (e = e.call(t, i, a)), null != e.top && (d.top = e.top - a.top + s), null != e.left && (d.left = e.left - a.left + r), "using" in e ? e.using.call(t, d) : h.css(d)
        }
    }, rt.fn.extend({
        offset: function (e) {
            if (arguments.length) return e === undefined ? this : this.each(function (t) {
                rt.offset.setOffset(this, e, t)
            });
            var t, i, n = {top: 0, left: 0}, r = this[0], o = r && r.ownerDocument;
            return o ? (t = o.documentElement, rt.contains(t, r) ? (typeof r.getBoundingClientRect !== Ct && (n = r.getBoundingClientRect()), i = Y(o), {
                top: n.top + (i.pageYOffset || t.scrollTop) - (t.clientTop || 0),
                left: n.left + (i.pageXOffset || t.scrollLeft) - (t.clientLeft || 0)
            }) : n) : void 0
        }, position: function () {
            if (this[0]) {
                var t, e, i = {top: 0, left: 0}, n = this[0];
                return "fixed" === rt.css(n, "position") ? e = n.getBoundingClientRect() : (t = this.offsetParent(), e = this.offset(), rt.nodeName(t[0], "html") || (i = t.offset()), i.top += rt.css(t[0], "borderTopWidth", !0), i.left += rt.css(t[0], "borderLeftWidth", !0)), {
                    top: e.top - i.top - rt.css(n, "marginTop", !0),
                    left: e.left - i.left - rt.css(n, "marginLeft", !0)
                }
            }
        }, offsetParent: function () {
            return this.map(function () {
                for (var t = this.offsetParent || si; t && !rt.nodeName(t, "html") && "static" === rt.css(t, "position");) t = t.offsetParent;
                return t || si
            })
        }
    }), rt.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function (e, r) {
        var o = /Y/.test(r);
        rt.fn[e] = function (t) {
            return $t(this, function (t, e, i) {
                var n = Y(t);
                if (i === undefined) return n ? r in n ? n[r] : n.document.documentElement[e] : t[e];
                n ? n.scrollTo(o ? rt(n).scrollLeft() : i, o ? i : rt(n).scrollTop()) : t[e] = i
            }, e, t, arguments.length, null)
        }
    }), rt.each(["top", "left"], function (t, i) {
        rt.cssHooks[i] = T(it.pixelPosition, function (t, e) {
            if (e) return e = te(t, i), ie.test(e) ? rt(t).position()[i] + "px" : e
        })
    }), rt.each({Height: "height", Width: "width"}, function (o, s) {
        rt.each({padding: "inner" + o, content: s, "": "outer" + o}, function (n, t) {
            rt.fn[t] = function (t, e) {
                var i = arguments.length && (n || "boolean" != typeof t),
                    r = n || (!0 === t || !0 === e ? "margin" : "border");
                return $t(this, function (t, e, i) {
                    var n;
                    return rt.isWindow(t) ? t.document.documentElement["client" + o] : 9 === t.nodeType ? (n = t.documentElement, Math.max(t.body["scroll" + o], n["scroll" + o], t.body["offset" + o], n["offset" + o], n["client" + o])) : i === undefined ? rt.css(t, e, r) : rt.style(t, e, i, r)
                }, s, i ? t : undefined, i, null)
            }
        })
    }), rt.fn.size = function () {
        return this.length
    }, rt.fn.andSelf = rt.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function () {
        return rt
    });
    var ai = g.jQuery, li = g.$;
    return rt.noConflict = function (t) {
        return g.$ === rt && (g.$ = li), t && g.jQuery === rt && (g.jQuery = ai), rt
    }, typeof t === Ct && (g.jQuery = g.$ = rt), rt
}), function (o) {
    o.fn.hoverForIE6 = function (t) {
        var r = o.extend({current: "hover", delay: 10}, t || {});
        o.each(this, function () {
            var e = null, i = null, n = !1;
            o(this).bind("mouseover", function () {
                if (n) clearTimeout(i); else {
                    var t = o(this);
                    0 < t.find(".product-wrap").length && (e = setTimeout(function () {
                        t.addClass(r.current).find("s").hide(), t.find(".product").addClass("menu_right_border"), t.find(".product-wrap").show(), n = !0
                    }, r.delay))
                }
            }).bind("mouseout", function () {
                if (n) {
                    var t = o(this);
                    i = setTimeout(function () {
                        t.removeClass(r.current).find("s").show(), t.find(".product-wrap").hide(), t.find(".product").removeClass("menu_right_border"), n = !1
                    }, r.delay)
                } else clearTimeout(e)
            })
        })
    }
}(jQuery), function (P) {
    P.fn.changeTab = function (t) {
        t = P.extend({
            checkedArea: P.noop,
            changeArea: P.noop,
            manyTimes: !1,
            checkedClass: "active",
            callBack: P.noop
        }, t);
        var e = P(this).selector, i = t.checkedArea, n = t.checkedClass, r = t.changeArea, o = t.manyTimes,
            s = t.callBack;
        return P(document).on("click", i, function () {
            var t = P(this).index();
            P(this).siblings(i).removeClass(n), P(this).addClass(n), o ? P(this).closest(e).eq(t).show().siblings().hide() : (P(r).hide(), P(r).eq(t).show()), s && s()
        }), this
    }, P.fn.carousel = function (t) {
        var e, i, n, r = (t = P.extend({
                way: "fade",
                num: 1,
                speed: 100,
                timer: 100,
                showButton: !1,
                showArrow: !1,
                autoPlay: !1,
                imgHeight: P.noop
            }, t)).way, o = t.num, s = t.speed, a = t.timer, l = t.showButton, c = t.showArrow, h = t.autoPlay,
            d = t.imgHeight, u = P(this), p = P(this).find(".carousel_ul"), f = p.children("li"), g = f.length;
        if ("auto" === d) {
            var m = P(this).find("img");
            u.height(m.height())
        }
        if (1 < o && "scrolls_x" === r) {
            var v = u.width() / o;
            f.width(v)
        } else if (1 < o && "scrolls_y" === r) {
            var y = u.height() / o;
            f.height(y)
        }
        if (l && 1 === o) {
            var x = "<ul class='position_ul'></ul>";
            u.append(x);
            for (var b, w, C = u.find(".position_ul"), _ = 1; _ <= g; _++) C.append("<li></li>");
            C.find("li").eq(0).addClass("cur"), C.find("li").hover(function () {
                w = P(this), b = setTimeout(n, 100)
            }, function () {
                b && clearTimeout(b)
            })
        }
        if (c) {
            var k = "<span class='prev'></span>", S = "<span class='next'></span>";
            u.append(k), u.append(S);
            var T = 0;
            u.children(".next").click(function () {
                if (0 == T) {
                    T = 1;
                    var t = setInterval(function () {
                        0 == --T && clearInterval(t)
                    }, 500);
                    e()
                }
            }), u.children(".prev").click(function () {
                i()
            })
        }
        switch (r) {
            case"fade":
                f.hide(), f.eq(0).show(), p.addClass("fade_ul");
                _ = 0;
                e = function () {
                    _ = g - 1 < ++_ ? 0 : _, C.find("li").eq(_).addClass("cur").siblings().removeClass("cur"), f.eq(_).fadeIn(s).siblings().fadeOut(s)
                }, i = function () {
                    _ = --_ < 0 ? g - 1 : _, C.find("li").eq(_).addClass("cur").siblings().removeClass("cur"), f.eq(_).fadeIn(s).siblings().fadeOut(s)
                }, n = function () {
                    w.addClass("cur").siblings().removeClass("cur");
                    var t = w.index();
                    _ = t, f.eq(_).fadeIn(s).siblings().fadeOut(s)
                };
                break;
            case"scrollX":
                p.addClass("scrollX_ul");
                var A = u.width();
                f.width(A), p.width(A * g);
                _ = 0;
                e = function () {
                    g - 1 <= _ ? (_ = 0, p.animate({left: "0px"}, s)) : (_++, p.animate({left: "-=" + A}, s)), C.find("li").eq(_).addClass("cur").siblings().removeClass("cur")
                }, i = function () {
                    0 == _ ? (_ = g - 1, p.animate({left: "-=" + A * (g - 1)}, s)) : (_--, p.animate({left: "+=" + A}, s)), C.find("li").eq(_).addClass("cur").siblings().removeClass("cur")
                }, n = function () {
                    w.addClass("cur").siblings().removeClass("cur");
                    var t = w.index();
                    _ = t, p.animate({left: -A * _}, "slow")
                };
                break;
            case"scrollY":
                p.addClass("scrollY_ul");
                _ = 0;
                var $ = u.height();
                e = function () {
                    g - 1 <= _ ? (_ = 0, p.animate({top: "0px"}, s)) : (_++, p.animate({top: "-=" + $}, s)), C.find("li").eq(_).addClass("cur").siblings().removeClass("cur")
                }, i = function () {
                    0 == _ ? (_ = g - 1, p.animate({top: "-=" + $ * (g - 1)}, s)) : (_--, p.animate({top: "+=" + $}, s)), C.find("li").eq(_).addClass("cur").siblings().removeClass("cur")
                }, n = function () {
                    w.addClass("cur").siblings().removeClass("cur");
                    var t = w.index();
                    _ = t, p.animate({top: -$ * _}, "slow")
                };
                break;
            case"scrolls_x":
                p.addClass("scrolls_x_ul");
                var M = f.clone();
                p.append(M);
                A = f.outerWidth();
                p.width(A * p.find("li").length);
                _ = 0;
                e = function () {
                    g - 2 < _ ? (_ = 0, p.animate({left: "-=" + A}, s, function () {
                        p.css("left", "0px")
                    })) : (_++, p.animate({left: "-=" + A}, s))
                };
                break;
            case"scrolls_y":
                c = l = !1, p.addClass("scrolls_y_ul");
                M = f.clone();
                p.append(M);
                $ = f.outerHeight();
                p.height($ * p.find("li").length);
                _ = 0;
                e = function () {
                    g - 2 < _ ? (_ = 0, p.animate({top: "-=" + $}, s, function () {
                        p.css("top", "0px")
                    })) : (_++, p.animate({top: "-=" + $}, s))
                }
        }
        if (h) {
            var E = setInterval(e, a);
            u.hover(function () {
                clearInterval(E)
            }, function () {
                E = setInterval(e, a)
            })
        }
        return this
    }
}(window.jQuery), $(function () {
    undefind_function();
    var t;
    setInterval(function () {
        var t = $(".other_temp").text();
        $(".other_temp").text(Number(t) + 5), undefind_function()
    }, 5e3);
    $(".my_menu_tabs").each(function (i) {
        var n = $(this), r = n.next();
        n.find("li").click(function () {
            n.find("li").removeClass("selected off"), r.find(".my_menu_box").hide();
            var t = $(this).index();
            $(this).addClass("selected off");
            var e = $("#" + (i + "-" + t)).attr("catalog");
            r.find(".my_menu_box").eq(t).show(), $("#more" + i).attr("href", "/channel/" + e)
        })
    }), $(".d-tip").on("click", function () {
        dialog({align: "top", content: $(this).next(".d-tip-content").html(), quickClose: !0}).show(this)
    }), $(".all-goods .item").hoverForIE6({
        current: "active",
        delay: 150
    }), $("#left_nav").bind("mouseover", function () {
        t = setTimeout(function () {
            $("#all-goods").show()
        }, 1e4)
    }), $("#left_nav").bind("mouseout", function () {
        t && $("#all-goods").hide()
    }), $(document).on("click", ".close_tip_msg", function () {
        $("#tip_msg").hide()
    }), $(document).on("click", ".list_increase_num", function () {
        var t = $(this).prev("input");
        parseInt(t.val()) < 99999 ? t.val(parseInt(t.val()) + 1) : t.val(99999);
        var e = $("#add_to_cart").prop("href");
        $("#add_to_cart").prop("href", e.replace(/num=\d*/g, "num=" + t.val())), $("#available_quantity").change()
    }), $(document).on("click", ".list_decrease_num", function () {
        var t = $(this).next("input");
        1 != t.val() && t.val(parseInt(t.val()) - 1);
        var e = $("#add_to_cart").prop("href");
        $("#add_to_cart").prop("href", e.replace(/num=\d*/g, "num=" + t.val())), $("#available_quantity").change()
    }), $(document).on("keyup", "#available_quantity", function () {
        var t = parseInt($(this).val());
        $.isBlank(t) ? t = 1 : (t <= 1 && (t = 1), 99999 <= t && (t = 99999)), $(this).val(t);
        var e = $("#add_to_cart").prop("href");
        $("#add_to_cart").prop("href", e.replace(/num=.*/g, "num=" + t))
    }), $(document).on("click", ".decrease_num", function () {
        $num = $("#" + $(this).attr("alt")), num = $num.val(), $.isBlank(parseInt(num)) || 1 < parseInt(num) && ($num.val(parseInt(num) - 1), price = $("#item-buy-price-" + $(this).attr("alt")), total = $("#item-buy-total-" + $(this).attr("alt")), total.text((parseInt($num.val()) * parseFloat(price.text())).toFixed(2)), calc_total(), $.get("/cart/change/" + $(this).attr("alt").split("cart_item_")[1] + "?set=1&num=" + $num.val()))
    }), $(document).on("click", ".increase_num", function () {
        $num = $("#" + $(this).attr("alt")), num = $num.val(), $.isBlank(parseInt(num)) || (99999 < parseInt(num) + 1 ? $num.val(99999) : $num.val(parseInt(num) + 1), price = $("#item-buy-price-" + $(this).attr("alt")), total = $("#item-buy-total-" + $(this).attr("alt")), total.text((parseInt($num.val()) * parseFloat(price.text())).toFixed(2)), calc_total(), $.get("/cart/change/" + $(this).attr("alt").split("cart_item_")[1] + "?set=1&num=" + $num.val()))
    }), $(document).on("keyup", ".cart-item-num", function () {
        var t = parseInt($(this).val());
        $.isBlank(t) ? t = 1 : (t <= 1 && (t = 1), 99999 <= t && (t = 99999)), $(this).val(t), price = $("#item-buy-price-" + $(this).prop("id")), total = $("#item-buy-total-" + $(this).prop("id")), sum_total = $("#item-buy-sum_total"), total.text((parseInt($(this).val()) * parseFloat(price.text())).toFixed(2)), calc_total(), pid = $(this).prop("id").split("cart_item_")[1], $.get("/cart/change/" + pid + "?set=1&num=" + $(this).val() + "&plan_code=" + $("#plan_code_" + pid).val())
    }), $(".cart_checkbox").change(function () {
        var t = $(this).attr("pid"), e = $(this).attr("eid");
        0 == $(this).prop("checked") ? ($("[name='p-" + e + "']:checkbox:checked").length != $("[name='p-" + e + "']:checkbox").length && $("#all-" + $(this).attr("eid")).prop("checked", !1), $(".cart_emall_checkbox:checked").length != $(".cart_emall_checkbox").length && $(".check_all_box").prop("checked", !1), $(this).closest("div.merchandise").removeClass("bg7"), $("#item-buy-total-cart_item_" + t).removeClass("cart-item-total"), $("#plan_code_" + t).removeClass("plan_code_field"), $("#has_stock_" + t).removeClass("stock_item")) : ($("[name='p-" + e + "']:checkbox:checked").length == $("[name='p-" + e + "']:checkbox").length && $("#all-" + $(this).attr("eid")).prop("checked", !0), $(".cart_emall_checkbox:checked").length == $(".cart_emall_checkbox").length && $(".check_all_box").prop("checked", !0), $(this).closest("div.merchandise").addClass("bg7"), $("#item-buy-total-cart_item_" + t).addClass("cart-item-total"), $("#plan_code_" + t).addClass("plan_code_field"), $("#has_stock_" + t).addClass("stock_item")), $.post("/cart/dynamic?pids=" + t + "&ready=" + $(this).prop("checked")), calc_total(), calc_num()
    }), $(".cart_emall_checkbox").change(function () {
        var t = $(this).attr("eid"), e = new Array;
        0 == $(this).prop("checked") ? $("[name='p-" + t + "']:checkbox").each(function () {
            e.push($(this).attr("pid")), $(this).prop("checked", !1), $(".cart_emall_checkbox:checked").length != $(".cart_emall_checkbox").length && $(".check_all_box").prop("checked", !1), $(this).closest("div.merchandise").removeClass("bg7"), $("#item-buy-total-cart_item_" + $(this).attr("pid")).removeClass("cart-item-total"), $("#plan_code_" + $(this).attr("pid")).removeClass("plan_code_field"), $("#has_stock_" + $(this).attr("pid")).removeClass("stock_item")
        }) : $("[name='p-" + t + "']:checkbox").each(function () {
            $(this)[0].disabled || (e.push($(this).attr("pid")), $(this).prop("checked", !0), $(".cart_emall_checkbox:checked").length == $(".cart_emall_checkbox").length && $(".check_all_box").prop("checked", !0), $(this).closest("div.merchandise").addClass("bg7"), $("#item-buy-total-cart_item_" + $(this).attr("pid")).addClass("cart-item-total"), $("#plan_code_" + $(this).attr("pid")).addClass("plan_code_field"), $("#has_stock_" + $(this).attr("pid")).addClass("stock_item"))
        }), $.post("/cart/dynamic?pids=" + e.join("_") + "&ready=" + $(this).prop("checked")), calc_total(), calc_num()
    }), $("svg").find("text").last().hide(), $("img[alt='captcha']").each(function (t, e) {
        e.title = "\u770b\u4e0d\u6e05\uff1f\u70b9\u51fb\u5237\u65b0"
    }), $("img[alt='captcha']").on("click", function (t) {
        img = t.currentTarget, img.src = img.src + "?"
    }), $("#tagContent1 .tagHtml td").removeAttr("class").addClass("p12 w100"), $("#tagContent1 .tagHtml th").removeAttr("class").addClass("p12"), $("#tagContent0 .tagHtml div").removeAttr("class").addClass("lh23"), $("#tagContent1 .tagHtml table").attr("border", 1), $("#tagContent2 .tagHtml table").attr("border", 1), $("#tagContent2 .tagHtml td").removeAttr("class").addClass("p12 w100"), $("#tagContent2 .tagHtml th").removeAttr("class").addClass("p12"), $(".filtrate .name_search").bind("keyup change", function () {
        var t = $(this).val();
        t ? $(this).parent().parent("dl").addClass("mh94") : $(this).parent().parent("dl").removeClass("mh94"), $(this).parent().parent("dl").find("dd").addClass("new_hide").filter(":containsStrcmpi('" + t + "')").removeClass("new_hide")
    }), $(".rating-editor").each(function () {
        var t = {
            value: $("#" + $(this).data("target")).val(), after_click: function (t) {
                $("#" + t.target).val(t.number)
            }
        };
        $(this).rater(t)
    }), $("#ra_project_form").submit(function () {
        var t = $("#plan_title_name").val();
        return isEmpty(t) && (t = "\u8d44\u91d1\u6838\u5b9a\u5355"), !(0 < $(".formErrorContent").length) && (3 != $("select[aim_id='ra_project_area_id']").length ? (art_alert("\u8bf7\u9009\u62e9\u91c7\u8d2d\u5730\u533a\uff01"), !1) : $("#ra_project_item_fields .fields:visible").length <= 0 ? (art_alert("\u8bf7\u6dfb\u52a0\u4ea7\u54c1"), !1) : $("#ra_project_init_price").val() <= 0 ? (art_alert("\u7ade\u62cd\u603b\u4ef7\u5fc5\u987b\u5927\u4e8e0\u5143"), !1) : (ra_project_price_count(), !(1e6 < $("#ra_project_init_price").val()) && (isEmpty($("input:radio[name='ra_project[invoice_id]']").val()) ? (alert("\u8bf7\u9009\u62e9\u53d1\u7968\u4fe1\u606f"), !1) : isEmpty($("input:radio[name='ra_project[address_id]']").val()) ? (alert("\u8bf7\u9009\u62e9\u9001\u8d27\u5730\u5740\u4fe1\u606f"), !1) : void $(".project_commit").unbind().text("\u63d0\u4ea4\u4e2d..."))))
    })
}), SDMenu.prototype.init = function () {
    for (var t = this, e = 0; e < this.submenus.length; e++) this.submenus[e].getElementsByTagName("span")[0].onclick = function () {
        t.toggleMenu(this.parentNode)
    };
    if (this.markCurrent) {
        var i = this.menu.getElementsByTagName("a");
        for (e = 0; e < i.length; e++) if (i[e].href == document.location.href) {
            i[e].className = "current";
            break
        }
    }
    if (this.remember) {
        var n = new RegExp("sdmenu_" + encodeURIComponent(this.menu.id) + "=([01]+)").exec(document.cookie);
        if (n) {
            var r = n[1].split("");
            for (e = 0; e < r.length; e++) this.submenus[e].className = 0 == r[e] ? "collapsed" : ""
        }
    }
}, SDMenu.prototype.toggleMenu = function (t) {
    "collapsed" == t.className ? this.expandMenu(t) : this.collapseMenu(t)
}, SDMenu.prototype.expandMenu = function (e) {
    for (var i = e.getElementsByTagName("span")[0].offsetHeight, t = e.getElementsByTagName("a"), n = 0; n < t.length; n++) i += t[n].offsetHeight;
    var r = Math.round(this.speed * t.length), o = this, s = setInterval(function () {
        var t = e.offsetHeight + r;
        t < i ? e.style.height = t + "px" : (clearInterval(s), e.style.height = "", e.className = "", o.memorize())
    }, 30);
    this.collapseOthers(e)
}, SDMenu.prototype.collapseMenu = function (e) {
    var i = e.getElementsByTagName("span")[0].offsetHeight,
        n = Math.round(this.speed * e.getElementsByTagName("a").length), r = this, o = setInterval(function () {
            var t = e.offsetHeight - n;
            i < t ? e.style.height = t + "px" : (clearInterval(o), e.style.height = "", e.className = "collapsed", r.memorize())
        }, 30)
}, SDMenu.prototype.collapseOthers = function (t) {
    if (this.oneSmOnly) for (var e = 0; e < this.submenus.length; e++) this.submenus[e] != t && "collapsed" != this.submenus[e].className && this.collapseMenu(this.submenus[e])
}, SDMenu.prototype.expandAll = function () {
    var t = this.oneSmOnly;
    this.oneSmOnly = !1;
    for (var e = 0; e < this.submenus.length; e++) "collapsed" == this.submenus[e].className && this.expandMenu(this.submenus[e]);
    this.oneSmOnly = t
}, SDMenu.prototype.collapseAll = function () {
    for (var t = 0; t < this.submenus.length; t++) "collapsed" != this.submenus[t].className && this.collapseMenu(this.submenus[t])
}, SDMenu.prototype.memorize = function () {
    if (this.remember) {
        for (var t = new Array, e = 0; e < this.submenus.length; e++) t.push("collapsed" == this.submenus[e].className ? 0 : 1);
        var i = new Date;
        i.setTime(i.getTime() + 2592e6), document.cookie = "sdmenu_" + encodeURIComponent(this.menu.id) + "=" + t.join("") + "; expires=" + i.toGMTString() + "; path=/"
    }
};
var timeout = 500, closetimer = 0, ddmenuitem = 0;
$(document).ready(function () {
    $("#jsddm > li").bind("mouseover", jsddm_open), $("#jsddm > li").bind("mouseout", jsddm_timer)
}), document.onclick = jsddm_close, $(document).ready(function () {
    "true" == $.cookie("open_compare") && ($("#compare_tip").show(), show_save_cps()), $(document).on("click", ".publicaddress", function () {
        var t = $(this).find("input:radio[name='choose_address_id']"), e = t.val();
        $(".publicaddress").removeClass("otheraddress"), $(this).addClass("otheraddress"), $("#newaddress_box").hide(), $("#edit_address_id").val(e), "0" != e ? ($("#order_address_id").val(e), $("#current_address_info").text(t.next().text())) : $("#newaddress_box").show(), t.prop("checked", !0)
    }), $(document).on("mouseover", ".publicaddress", function () {
        $(".address_button").hide(), $(this).find(".address_button").show()
    }), $(document).on("click", "#choose_address_id_0", function () {
        setTimeout("edit_address(null)", 12)
    }), $(document).on("click", ".edit_link_address", function () {
        var t = $(this).parent().parent().find("input:radio[name='choose_address_id']").val();
        setTimeout("edit_address(" + t + ")", 12)
    }), $(document).on("click", ".edit_link_invoice", function () {
        var t = $(this).parent().parent().find("input:radio[name='choose_invoice_id']").val();
        setTimeout("edit_invoice(" + t + ")", 12)
    }), $(document).on("click", ".publicinvoice", function () {
        var t = $(this).find("input:radio[name='choose_invoice_id']"), e = t.val();
        $(".publicinvoice").removeClass("on"), $(this).addClass("on"), $("#newinvoice_box").hide(), $("#edit_invoice_id").val(e), "0" != e ? ($("#current_invoice_info").text(t.next().text()), $("#order_invoice_id").val(e)) : $("#newinvoice_box").show(), t.prop("checked", !0)
    }), $(document).on("mouseover", ".publicinvoice", function () {
        $(".invoice_button").hide(), $(this).find(".invoice_button").show()
    }), $("input:radio[name='order[ptype]']").change(function () {
        $("#current_ptype_info").text($(this).next().text()), 0 == $(this).val() ? $("#plans_div").show() : $("#plans_div").hide()
    }), $(document).on("click", ".publicplan", function () {
        var t = $(this).find("input:radio[name='order[plan_id]']");
        $(
            ".publicplan").removeClass("on"), $(this).addClass("on"), t.prop("checked", !0), $("#current_ptype_info").text($(this).children("label").text())
    }), $(document).on("blur", "#area_div", function () {
        check_Consignee("area_div")
    }), $("img[alt='captcha']").each(function (t, e) {
        e.title = "\u770b\u4e0d\u6e05\uff1f\u70b9\u51fb\u5237\u65b0"
    }), $("img[alt='captcha']").on("click", function (t) {
        img = t.currentTarget, img.src = img.src + "?"
    })
});
var dialog_a = dialog_b = dialog_c = "", tag_a = tag_b = !0, time_a = time_b = "";
$(document).on("click", ".get_products", function () {
    $t = $(this);
    var t = $t.attr("oid");
    dialog({
        id: "get_product_" + t,
        title: "\u786e\u8ba4\u6536\u8d27",
        width: "600px",
        height: "380px",
        url: "/orders/" + t + "/get_products",
        close: function () {
            $(this).remove()
        },
        cancelDisplay: !1
    }).show()
}), function (h, d) {
    var u;
    h.rails !== d && h.error("jquery-ujs has already been loaded!");
    var t = h(document);
    h.rails = u = {
        linkClickSelector: "a[data-confirm], a[data-method], a[data-remote], a[data-disable-with], a[data-disable]",
        buttonClickSelector: "button[data-remote]:not(form button), button[data-confirm]:not(form button)",
        inputChangeSelector: "select[data-remote], input[data-remote], textarea[data-remote]",
        formSubmitSelector: "form",
        formInputClickSelector: "form input[type=submit], form input[type=image], form button[type=submit], form button:not([type]), input[type=submit][form], input[type=image][form], button[type=submit][form], button[form]:not([type])",
        disableSelector: "input[data-disable-with]:enabled, button[data-disable-with]:enabled, textarea[data-disable-with]:enabled, input[data-disable]:enabled, button[data-disable]:enabled, textarea[data-disable]:enabled",
        enableSelector: "input[data-disable-with]:disabled, button[data-disable-with]:disabled, textarea[data-disable-with]:disabled, input[data-disable]:disabled, button[data-disable]:disabled, textarea[data-disable]:disabled",
        requiredInputSelector: "input[name][required]:not([disabled]),textarea[name][required]:not([disabled])",
        fileInputSelector: "input[type=file]",
        linkDisableSelector: "a[data-disable-with], a[data-disable]",
        buttonDisableSelector: "button[data-remote][data-disable-with], button[data-remote][data-disable]",
        CSRFProtection: function (t) {
            var e = h('meta[name="csrf-token"]').attr("content");
            e && t.setRequestHeader("X-CSRF-Token", e)
        },
        refreshCSRFTokens: function () {
            var t = h("meta[name=csrf-token]").attr("content"), e = h("meta[name=csrf-param]").attr("content");
            h('form input[name="' + e + '"]').val(t)
        },
        fire: function (t, e, i) {
            var n = h.Event(e);
            return t.trigger(n, i), !1 !== n.result
        },
        confirm: function (t) {
            return confirm(t)
        },
        ajax: function (t) {
            return h.ajax(t)
        },
        href: function (t) {
            return t.attr("href")
        },
        handleRemote: function (n) {
            var t, e, i, r, o, s, a, l;
            if (u.fire(n, "ajax:before")) {
                if (o = (r = n.data("cross-domain")) === d ? null : r, s = n.data("with-credentials") || null, a = n.data("type") || h.ajaxSettings && h.ajaxSettings.dataType, n.is("form")) {
                    t = n.attr("method"), e = n.attr("action"), i = n.serializeArray();
                    var c = n.data("ujs:submit-button");
                    c && (i.push(c), n.data("ujs:submit-button", null))
                } else n.is(u.inputChangeSelector) ? (t = n.data("method"), e = n.data("url"), i = n.serialize(), n.data("params") && (i = i + "&" + n.data("params"))) : n.is(u.buttonClickSelector) ? (t = n.data("method") || "get", e = n.data("url"), i = n.serialize(), n.data("params") && (i = i + "&" + n.data("params"))) : (t = n.data("method"), e = u.href(n), i = n.data("params") || null);
                return l = {
                    type: t || "GET", data: i, dataType: a, beforeSend: function (t, e) {
                        if (e.dataType === d && t.setRequestHeader("accept", "*/*;q=0.5, " + e.accepts.script), !u.fire(n, "ajax:beforeSend", [t, e])) return !1;
                        n.trigger("ajax:send", t)
                    }, success: function (t, e, i) {
                        n.trigger("ajax:success", [t, e, i])
                    }, complete: function (t, e) {
                        n.trigger("ajax:complete", [t, e])
                    }, error: function (t, e, i) {
                        n.trigger("ajax:error", [t, e, i])
                    }, crossDomain: o
                }, s && (l.xhrFields = {withCredentials: s}), e && (l.url = e), u.ajax(l)
            }
            return !1
        },
        handleMethod: function (t) {
            var e = u.href(t), i = t.data("method"), n = t.attr("target"),
                r = h("meta[name=csrf-token]").attr("content"), o = h("meta[name=csrf-param]").attr("content"),
                s = h('<form method="post" action="' + e + '"></form>'),
                a = '<input name="_method" value="' + i + '" type="hidden" />';
            o !== d && r !== d && (a += '<input name="' + o + '" value="' + r + '" type="hidden" />'), n && s.attr("target", n), s.hide().append(a).appendTo("body"), s.submit()
        },
        formElements: function (t, e) {
            return t.is("form") ? h(t[0].elements).filter(e) : t.find(e)
        },
        disableFormElements: function (t) {
            u.formElements(t, u.disableSelector).each(function () {
                u.disableFormElement(h(this))
            })
        },
        disableFormElement: function (t) {
            var e, i;
            e = t.is("button") ? "html" : "val", i = t.data("disable-with"), t.data("ujs:enable-with", t[e]()), i !== d && t[e](i), t.prop("disabled", !0)
        },
        enableFormElements: function (t) {
            u.formElements(t, u.enableSelector).each(function () {
                u.enableFormElement(h(this))
            })
        },
        enableFormElement: function (t) {
            var e = t.is("button") ? "html" : "val";
            t.data("ujs:enable-with") && t[e](t.data("ujs:enable-with")), t.prop("disabled", !1)
        },
        allowAction: function (t) {
            var e, i = t.data("confirm"), n = !1;
            return !i || (u.fire(t, "confirm") && (n = u.confirm(i), e = u.fire(t, "confirm:complete", [n])), n && e)
        },
        blankInputs: function (t, e, i) {
            var n, r = h(), o = e || "input,textarea", s = t.find(o);
            return s.each(function () {
                if (n = h(this), !(n.is("input[type=checkbox],input[type=radio]") ? n.is(":checked") : n.val()) == !i) {
                    if (n.is("input[type=radio]") && s.filter('input[type=radio]:checked[name="' + n.attr("name") + '"]').length) return !0;
                    r = r.add(n)
                }
            }), !!r.length && r
        },
        nonBlankInputs: function (t, e) {
            return u.blankInputs(t, e, !0)
        },
        stopEverything: function (t) {
            return h(t.target).trigger("ujs:everythingStopped"), t.stopImmediatePropagation(), !1
        },
        disableElement: function (t) {
            var e = t.data("disable-with");
            t.data("ujs:enable-with", t.html()), e !== d && t.html(e), t.bind("click.railsDisable", function (t) {
                return u.stopEverything(t)
            })
        },
        enableElement: function (t) {
            t.data("ujs:enable-with") !== d && (t.html(t.data("ujs:enable-with")), t.removeData("ujs:enable-with")), t.unbind("click.railsDisable")
        }
    }, u.fire(t, "rails:attachBindings") && (h.ajaxPrefilter(function (t, e, i) {
        t.crossDomain || u.CSRFProtection(i)
    }), t.delegate(u.linkDisableSelector, "ajax:complete", function () {
        u.enableElement(h(this))
    }), t.delegate(u.buttonDisableSelector, "ajax:complete", function () {
        u.enableFormElement(h(this))
    }), t.delegate(u.linkClickSelector, "click.rails", function (t) {
        var e = h(this), i = e.data("method"), n = e.data("params"), r = t.metaKey || t.ctrlKey;
        if (!u.allowAction(e)) return u.stopEverything(t);
        if (!r && e.is(u.linkDisableSelector) && u.disableElement(e), e.data("remote") === d) return e.data("method") ? (u.handleMethod(e), !1) : void 0;
        if (r && (!i || "GET" === i) && !n) return !0;
        var o = u.handleRemote(e);
        return !1 === o ? u.enableElement(e) : o.error(function () {
            u.enableElement(e)
        }), !1
    }), t.delegate(u.buttonClickSelector, "click.rails", function (t) {
        var e = h(this);
        if (!u.allowAction(e)) return u.stopEverything(t);
        e.is(u.buttonDisableSelector) && u.disableFormElement(e);
        var i = u.handleRemote(e);
        return !1 === i ? u.enableFormElement(e) : i.error(function () {
            u.enableFormElement(e)
        }), !1
    }), t.delegate(u.inputChangeSelector, "change.rails", function (t) {
        var e = h(this);
        return u.allowAction(e) ? (u.handleRemote(e), !1) : u.stopEverything(t)
    }), t.delegate(u.formSubmitSelector, "submit.rails", function (t) {
        var e, i, n = h(this), r = n.data("remote") !== d;
        if (!u.allowAction(n)) return u.stopEverything(t);
        if (n.attr("novalidate") == d && (e = u.blankInputs(n, u.requiredInputSelector)) && u.fire(n, "ajax:aborted:required", [e])) return u.stopEverything(t);
        if (r) {
            if (i = u.nonBlankInputs(n, u.fileInputSelector)) {
                setTimeout(function () {
                    u.disableFormElements(n)
                }, 13);
                var o = u.fire(n, "ajax:aborted:file", [i]);
                return o || setTimeout(function () {
                    u.enableFormElements(n)
                }, 13), o
            }
            return u.handleRemote(n), !1
        }
        setTimeout(function () {
            u.disableFormElements(n)
        }, 13)
    }), t.delegate(u.formInputClickSelector, "click.rails", function (t) {
        var e = h(this);
        if (!u.allowAction(e)) return u.stopEverything(t);
        var i = e.attr("name"), n = i ? {name: i, value: e.val()} : null;
        e.closest("form").data("ujs:submit-button", n)
    }), t.delegate(u.formSubmitSelector, "ajax:send.rails", function (t) {
        this == t.target && u.disableFormElements(h(this))
    }), t.delegate(u.formSubmitSelector, "ajax:complete.rails", function (t) {
        this == t.target && u.enableFormElements(h(this))
    }), h(function () {
        u.refreshCSRFTokens()
    }))
}(jQuery), function (t, e) {
    "use strict";
    var i, n;
    if (t.uaMatch = function (t) {
        t = t.toLowerCase();
        var e = /(opr)[\/]([\w.]+)/.exec(t) || /(chrome)[ \/]([\w.]+)/.exec(t) || /(webkit)[ \/]([\w.]+).*(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec(t) || /(webkit)[ \/]([\w.]+)/.exec(t) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(t) || /(msie) ([\w.]+)/.exec(t) || 0 <= t.indexOf("trident") && /(rv)(?::| )([\w.]+)/.exec(t) || t.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(t) || [],
            i = /(ipad)/.exec(t) || /(iphone)/.exec(t) || /(android)/.exec(t) || /(windows phone)/.exec(t) || /(win)/.exec(t) || /(mac)/.exec(t) || /(linux)/.exec(t) || /(cros)/.exec(t) || [];
        return {browser: e[3] || e[1] || "", version: e[4] || e[2], versionNumber: e[2] || "0", platform: i[0] || ""}
    }, n = {}, (i = t.uaMatch(e.navigator.userAgent)).browser && (n[i.browser] = !0, n.version = i.version, n.versionNumber = parseInt(i.versionNumber)), i.platform && (n[i.platform] = !0), (n.android || n.ipad || n.iphone || n["windows phone"]) && (n.mobile = !0), (n.cros || n.mac || n.linux || n.win) && (n.desktop = !0), (n.chrome || n.opr || n.safari) && (n.webkit = !0), n.rv) {
        var r = "msie";
        n[i.browser = r] = !0
    }
    if (n.opr) {
        var o = "opera";
        n[i.browser = o] = !0
    }
    if (n.safari && n.android) {
        var s = "android";
        n[i.browser = s] = !0
    }
    n.name = i.browser, n.platform = i.platform, t.browser = n
}(jQuery, window), function (t) {
    "function" == typeof define && define.amd ? define(["jquery"], t) : "object" == typeof exports ? t(require("jquery")) : t(jQuery)
}(function (u) {
    function p(t) {
        return v.raw ? t : encodeURIComponent(t)
    }

    function f(t) {
        return v.raw ? t : decodeURIComponent(t)
    }

    function g(t) {
        return p(v.json ? JSON.stringify(t) : String(t))
    }

    function n(t) {
        0 === t.indexOf('"') && (t = t.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, "\\"));
        try {
            return t = decodeURIComponent(t.replace(i, " ")), v.json ? JSON.parse(t) : t
        } catch (e) {
        }
    }

    function m(t, e) {
        var i = v.raw ? t : n(t);
        return u.isFunction(e) ? e(i) : i
    }

    var i = /\+/g, v = u.cookie = function (t, e, i) {
        if (1 < arguments.length && !u.isFunction(e)) {
            if ("number" == typeof (i = u.extend({}, v.defaults, i)).expires) {
                var n = i.expires, r = i.expires = new Date;
                r.setTime(+r + 864e5 * n)
            }
            return document.cookie = [p(t), "=", g(e), i.expires ? "; expires=" + i.expires.toUTCString() : "", i.path ? "; path=" + i.path : "", i.domain ? "; domain=" + i.domain : "", i.secure ? "; secure" : ""].join("")
        }
        for (var o = t ? undefined : {}, s = document.cookie ? document.cookie.split("; ") : [], a = 0, l = s.length; a < l; a++) {
            var c = s[a].split("="), h = f(c.shift()), d = c.join("=");
            if (t && t === h) {
                o = m(d, e);
                break
            }
            t || (d = m(d)) === undefined || (o[h] = d)
        }
        return o
    };
    v.defaults = {}, u.removeCookie = function (t, e) {
        return u.cookie(t) !== undefined && (u.cookie(t, "", u.extend({}, e, {expires: -1})), !u.cookie(t))
    }
}), function (r) {
    "undefined" == typeof r.fn.each2 && r.extend(r.fn, {
        each2: function (t) {
            for (var e = r([0]), i = -1, n = this.length; ++i < n && (e.context = e[0] = this[i]) && !1 !== t.call(e[0], i, e);) ;
            return this
        }
    })
}(jQuery), function (w, b) {
    "use strict";

    function s(t) {
        var e = w(document.createTextNode(""));
        t.before(e), e.before(t), e.remove()
    }

    function a(t) {
        function e(t) {
            return R[t] || t
        }

        return t.replace(/[^\u0000-\u007E]/g, e)
    }

    function h(t, e) {
        for (var i = 0, n = e.length; i < n; i += 1) if (p(t, e[i])) return i;
        return -1
    }

    function l() {
        var t = w(z);
        t.appendTo("body");
        var e = {width: t.width() - t[0].clientWidth, height: t.height() - t[0].clientHeight};
        return t.remove(), e
    }

    function p(t, e) {
        return t === e || t !== b && e !== b && (null !== t && null !== e && (t.constructor === String ? t + "" == e + "" : e.constructor === String && e + "" == t + ""))
    }

    function r(t, e) {
        var i, n, r;
        if (null === t || t.length < 1) return [];
        for (n = 0, r = (i = t.split(e)).length; n < r; n += 1) i[n] = w.trim(i[n]);
        return i
    }

    function o(t) {
        return t.outerWidth(!1) - t.width()
    }

    function c(e) {
        var i = "keyup-change-value";
        e.on("keydown", function () {
            w.data(e, i) === b && w.data(e, i, e.val())
        }), e.on("keyup", function () {
            var t = w.data(e, i);
            t !== b && e.val() !== t && (w.removeData(e, i), e.trigger("keyup-change"))
        })
    }

    function d(t) {
        t.on("mousemove", function (t) {
            var e = N;
            (e === b || e.x !== t.pageX || e.y !== t.pageY) && w(t.target).trigger("mousemove-filtered", t)
        })
    }

    function n(e, i, n) {
        var r;
        return n = n || b, function () {
            var t = arguments;
            window.clearTimeout(r), r = window.setTimeout(function () {
                i.apply(n, t)
            }, e)
        }
    }

    function u(t, e) {
        var i = n(t, function (t) {
            e.trigger("scroll-debounced", t)
        });
        e.on("scroll", function (t) {
            0 <= h(t.target, e.get()) && i(t)
        })
    }

    function t(n) {
        n[0] !== document.activeElement && window.setTimeout(function () {
            var t, e = n[0], i = n.val().length;
            n.focus(), (0 < e.offsetWidth || 0 < e.offsetHeight) && e === document.activeElement && (e.setSelectionRange ? e.setSelectionRange(i, i) : e.createTextRange && ((t = e.createTextRange()).collapse(!1), t.select()))
        }, 0)
    }

    function f(t) {
        var e = 0, i = 0;
        if ("selectionStart" in (t = w(t)[0])) e = t.selectionStart, i = t.selectionEnd - e; else if ("selection" in document) {
            t.focus();
            var n = document.selection.createRange();
            i = document.selection.createRange().text.length, n.moveStart("character", -t.value.length), e = n.text.length - i
        }
        return {offset: e, length: i}
    }

    function g(t) {
        t.preventDefault(), t.stopPropagation()
    }

    function m(t) {
        t.preventDefault(), t.stopImmediatePropagation()
    }

    function v(t) {
        if (!F) {
            var e = t[0].currentStyle || window.getComputedStyle(t[0], null);
            (F = w(document.createElement("div")).css({
                position: "absolute",
                left: "-10000px",
                top: "-10000px",
                display: "none",
                fontSize: e.fontSize,
                fontFamily: e.fontFamily,
                fontStyle: e.fontStyle,
                fontWeight: e.fontWeight,
                letterSpacing: e.letterSpacing,
                textTransform: e.textTransform,
                whiteSpace: "nowrap"
            })).attr("class", "select2-sizer"), w("body").append(F)
        }
        return F.text(t.val()), F.width()
    }

    function y(t, e, i) {
        var n, r, o = [];
        (n = w.trim(t.attr("class"))) && w((n = "" + n).split(/\s+/)).each2(function () {
            0 === this.indexOf("select2-") && o.push(this)
        }), (n = w.trim(e.attr("class"))) && w((n = "" + n).split(/\s+/)).each2(function () {
            0 !== this.indexOf("select2-") && ((r = i(this)) && o.push(r))
        }), t.attr("class", o.join(" "))
    }

    function x(t, e, i, n) {
        var r = a(t.toUpperCase()).indexOf(a(e.toUpperCase())), o = e.length;
        return r < 0 ? void i.push(n(t)) : (i.push(n(t.substring(0, r))), i.push("<span class='select2-match'>"), i.push(n(t.substring(r, r + o))), i.push("</span>"), void i.push(n(t.substring(r + o, t.length))))
    }

    function e(t) {
        var e = {"\\": "&#92;", "&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#39;", "/": "&#47;"};
        return String(t).replace(/[&<>"'\/\\]/g, function (t) {
            return e[t]
        })
    }

    function C(s) {
        var t, a = null, e = s.quietMillis || 100, l = s.url, c = this;
        return function (o) {
            window.clearTimeout(t), t = window.setTimeout(function () {
                var t = s.data, e = l, i = s.transport || w.fn.select2.ajaxDefaults.transport, n = {
                    type: s.type || "GET",
                    cache: s.cache || !1,
                    jsonpCallback: s.jsonpCallback || b,
                    dataType: s.dataType || "json"
                }, r = w.extend({}, w.fn.select2.ajaxDefaults.params, n);
                t = t ? t.call(c, o.term, o.page, o.context) : null, e = "function" == typeof e ? e.call(c, o.term, o.page, o.context) : e, a && "function" == typeof a.abort && a.abort(), s.params && (w.isFunction(s.params) ? w.extend(r, s.params.call(c)) : w.extend(r, s.params)), w.extend(r, {
                    url: e,
                    dataType: s.dataType,
                    data: t,
                    success: function (t) {
                        var e = s.results(t, o.page, o);
                        o.callback(e)
                    }
                }), a = i.call(c, r)
            }, e)
        }
    }

    function _(t) {
        var e, i, n = t, a = function (t) {
            return "" + t.text
        };
        w.isArray(n) && (n = {results: i = n}), !1 === w.isFunction(n) && (i = n, n = function () {
            return i
        });
        var r = n();
        return r.text && (a = r.text, w.isFunction(a) || (e = r.text, a = function (t) {
            return t[e]
        })), function (r) {
            var o, s = r.term, i = {results: []};
            return "" === s ? void r.callback(n()) : (o = function (t, e) {
                var i, n;
                if ((t = t[0]).children) {
                    for (n in i = {}, t) t.hasOwnProperty(n) && (i[n] = t[n]);
                    i.children = [], w(t.children).each2(function (t, e) {
                        o(e, i.children)
                    }), (i.children.length || r.matcher(s, a(i), t)) && e.push(i)
                } else r.matcher(s, a(t), t) && e.push(t)
            }, w(n().results).each2(function (t, e) {
                o(e, i.results)
            }), void r.callback(i))
        }
    }

    function k(e) {
        var o = w.isFunction(e);
        return function (i) {
            var n = i.term, r = {results: []}, t = o ? e(i) : e;
            w.isArray(t) && (w(t).each(function () {
                var t = this.text !== b, e = t ? this.text : this;
                ("" === n || i.matcher(n, e)) && r.results.push(t ? this : {id: this, text: this})
            }), i.callback(r))
        }
    }

    function S(t, e) {
        if (w.isFunction(t)) return !0;
        if (!t) return !1;
        if ("string" == typeof t) return !0;
        throw new Error(e + " must be a string, function, or falsy value")
    }

    function T(t, e) {
        if (w.isFunction(t)) {
            var i = Array.prototype.slice.call(arguments, 2);
            return t.apply(e, i)
        }
        return t
    }

    function A(t) {
        var i = 0;
        return w.each(t, function (t, e) {
            e.children ? i += A(e.children) : i++
        }), i
    }

    function i(t, e, i, n) {
        var r, o, s, a, l, c = t, h = !1;
        if (!n.createSearchChoice || !n.tokenSeparators || n.tokenSeparators.length < 1) return b;
        for (; ;) {
            for (o = -1, s = 0, a = n.tokenSeparators.length; s < a && (l = n.tokenSeparators[s], !(0 <= (o = t.indexOf(l)))); s++) ;
            if (o < 0) break;
            if (r = t.substring(0, o), t = t.substring(o + l.length), 0 < r.length && ((r = n.createSearchChoice.call(this, r, e)) !== b && null !== r && n.id(r) !== b && null !== n.id(r))) {
                for (h = !1, s = 0, a = e.length; s < a; s++) if (p(n.id(r), n.id(e[s]))) {
                    h = !0;
                    break
                }
                h || i(r)
            }
        }
        return c !== t ? t : void 0
    }

    function $() {
        var i = this;
        w.each(arguments, function (t, e) {
            i[e].remove(), i[e] = null
        })
    }

    function M(t, e) {
        var i = function () {
        };
        return i.prototype = new t, (i.prototype.constructor = i).prototype.parent = t.prototype, i.prototype = w.extend(i.prototype, e), i
    }

    if (window.Select2 === b) {
        var E, P, L, I, F, D, O, N = {x: 0, y: 0}, j = {
            TAB: 9,
            ENTER: 13,
            ESC: 27,
            SPACE: 32,
            LEFT: 37,
            UP: 38,
            RIGHT: 39,
            DOWN: 40,
            SHIFT: 16,
            CTRL: 17,
            ALT: 18,
            PAGE_UP: 33,
            PAGE_DOWN: 34,
            HOME: 36,
            END: 35,
            BACKSPACE: 8,
            DELETE: 46,
            isArrow: function (t) {
                switch (t = t.which ? t.which : t) {
                    case j.LEFT:
                    case j.RIGHT:
                    case j.UP:
                    case j.DOWN:
                        return !0
                }
                return !1
            },
            isControl: function (t) {
                switch (t.which) {
                    case j.SHIFT:
                    case j.CTRL:
                    case j.ALT:
                        return !0
                }
                return !!t.metaKey
            },
            isFunctionKey: function (t) {
                return 112 <= (t = t.which ? t.which : t) && t <= 123
            }
        }, z = "<div class='select2-measure-scrollbar'></div>", R = {
            "\u24b6": "A",
            "\uff21": "A",
            "\xc0": "A",
            "\xc1": "A",
            "\xc2": "A",
            "\u1ea6": "A",
            "\u1ea4": "A",
            "\u1eaa": "A",
            "\u1ea8": "A",
            "\xc3": "A",
            "\u0100": "A",
            "\u0102": "A",
            "\u1eb0": "A",
            "\u1eae": "A",
            "\u1eb4": "A",
            "\u1eb2": "A",
            "\u0226": "A",
            "\u01e0": "A",
            "\xc4": "A",
            "\u01de": "A",
            "\u1ea2": "A",
            "\xc5": "A",
            "\u01fa": "A",
            "\u01cd": "A",
            "\u0200": "A",
            "\u0202": "A",
            "\u1ea0": "A",
            "\u1eac": "A",
            "\u1eb6": "A",
            "\u1e00": "A",
            "\u0104": "A",
            "\u023a": "A",
            "\u2c6f": "A",
            "\ua732": "AA",
            "\xc6": "AE",
            "\u01fc": "AE",
            "\u01e2": "AE",
            "\ua734": "AO",
            "\ua736": "AU",
            "\ua738": "AV",
            "\ua73a": "AV",
            "\ua73c": "AY",
            "\u24b7": "B",
            "\uff22": "B",
            "\u1e02": "B",
            "\u1e04": "B",
            "\u1e06": "B",
            "\u0243": "B",
            "\u0182": "B",
            "\u0181": "B",
            "\u24b8": "C",
            "\uff23": "C",
            "\u0106": "C",
            "\u0108": "C",
            "\u010a": "C",
            "\u010c": "C",
            "\xc7": "C",
            "\u1e08": "C",
            "\u0187": "C",
            "\u023b": "C",
            "\ua73e": "C",
            "\u24b9": "D",
            "\uff24": "D",
            "\u1e0a": "D",
            "\u010e": "D",
            "\u1e0c": "D",
            "\u1e10": "D",
            "\u1e12": "D",
            "\u1e0e": "D",
            "\u0110": "D",
            "\u018b": "D",
            "\u018a": "D",
            "\u0189": "D",
            "\ua779": "D",
            "\u01f1": "DZ",
            "\u01c4": "DZ",
            "\u01f2": "Dz",
            "\u01c5": "Dz",
            "\u24ba": "E",
            "\uff25": "E",
            "\xc8": "E",
            "\xc9": "E",
            "\xca": "E",
            "\u1ec0": "E",
            "\u1ebe": "E",
            "\u1ec4": "E",
            "\u1ec2": "E",
            "\u1ebc": "E",
            "\u0112": "E",
            "\u1e14": "E",
            "\u1e16": "E",
            "\u0114": "E",
            "\u0116": "E",
            "\xcb": "E",
            "\u1eba": "E",
            "\u011a": "E",
            "\u0204": "E",
            "\u0206": "E",
            "\u1eb8": "E",
            "\u1ec6": "E",
            "\u0228": "E",
            "\u1e1c": "E",
            "\u0118": "E",
            "\u1e18": "E",
            "\u1e1a": "E",
            "\u0190": "E",
            "\u018e": "E",
            "\u24bb": "F",
            "\uff26": "F",
            "\u1e1e": "F",
            "\u0191": "F",
            "\ua77b": "F",
            "\u24bc": "G",
            "\uff27": "G",
            "\u01f4": "G",
            "\u011c": "G",
            "\u1e20": "G",
            "\u011e": "G",
            "\u0120": "G",
            "\u01e6": "G",
            "\u0122": "G",
            "\u01e4": "G",
            "\u0193": "G",
            "\ua7a0": "G",
            "\ua77d": "G",
            "\ua77e": "G",
            "\u24bd": "H",
            "\uff28": "H",
            "\u0124": "H",
            "\u1e22": "H",
            "\u1e26": "H",
            "\u021e": "H",
            "\u1e24": "H",
            "\u1e28": "H",
            "\u1e2a": "H",
            "\u0126": "H",
            "\u2c67": "H",
            "\u2c75": "H",
            "\ua78d": "H",
            "\u24be": "I",
            "\uff29": "I",
            "\xcc": "I",
            "\xcd": "I",
            "\xce": "I",
            "\u0128": "I",
            "\u012a": "I",
            "\u012c": "I",
            "\u0130": "I",
            "\xcf": "I",
            "\u1e2e": "I",
            "\u1ec8": "I",
            "\u01cf": "I",
            "\u0208": "I",
            "\u020a": "I",
            "\u1eca": "I",
            "\u012e": "I",
            "\u1e2c": "I",
            "\u0197": "I",
            "\u24bf": "J",
            "\uff2a": "J",
            "\u0134": "J",
            "\u0248": "J",
            "\u24c0": "K",
            "\uff2b": "K",
            "\u1e30": "K",
            "\u01e8": "K",
            "\u1e32": "K",
            "\u0136": "K",
            "\u1e34": "K",
            "\u0198": "K",
            "\u2c69": "K",
            "\ua740": "K",
            "\ua742": "K",
            "\ua744": "K",
            "\ua7a2": "K",
            "\u24c1": "L",
            "\uff2c": "L",
            "\u013f": "L",
            "\u0139": "L",
            "\u013d": "L",
            "\u1e36": "L",
            "\u1e38": "L",
            "\u013b": "L",
            "\u1e3c": "L",
            "\u1e3a": "L",
            "\u0141": "L",
            "\u023d": "L",
            "\u2c62": "L",
            "\u2c60": "L",
            "\ua748": "L",
            "\ua746": "L",
            "\ua780": "L",
            "\u01c7": "LJ",
            "\u01c8": "Lj",
            "\u24c2": "M",
            "\uff2d": "M",
            "\u1e3e": "M",
            "\u1e40": "M",
            "\u1e42": "M",
            "\u2c6e": "M",
            "\u019c": "M",
            "\u24c3": "N",
            "\uff2e": "N",
            "\u01f8": "N",
            "\u0143": "N",
            "\xd1": "N",
            "\u1e44": "N",
            "\u0147": "N",
            "\u1e46": "N",
            "\u0145": "N",
            "\u1e4a": "N",
            "\u1e48": "N",
            "\u0220": "N",
            "\u019d": "N",
            "\ua790": "N",
            "\ua7a4": "N",
            "\u01ca": "NJ",
            "\u01cb": "Nj",
            "\u24c4": "O",
            "\uff2f": "O",
            "\xd2": "O",
            "\xd3": "O",
            "\xd4": "O",
            "\u1ed2": "O",
            "\u1ed0": "O",
            "\u1ed6": "O",
            "\u1ed4": "O",
            "\xd5": "O",
            "\u1e4c": "O",
            "\u022c": "O",
            "\u1e4e": "O",
            "\u014c": "O",
            "\u1e50": "O",
            "\u1e52": "O",
            "\u014e": "O",
            "\u022e": "O",
            "\u0230": "O",
            "\xd6": "O",
            "\u022a": "O",
            "\u1ece": "O",
            "\u0150": "O",
            "\u01d1": "O",
            "\u020c": "O",
            "\u020e": "O",
            "\u01a0": "O",
            "\u1edc": "O",
            "\u1eda": "O",
            "\u1ee0": "O",
            "\u1ede": "O",
            "\u1ee2": "O",
            "\u1ecc": "O",
            "\u1ed8": "O",
            "\u01ea": "O",
            "\u01ec": "O",
            "\xd8": "O",
            "\u01fe": "O",
            "\u0186": "O",
            "\u019f": "O",
            "\ua74a": "O",
            "\ua74c": "O",
            "\u01a2": "OI",
            "\ua74e": "OO",
            "\u0222": "OU",
            "\u24c5": "P",
            "\uff30": "P",
            "\u1e54": "P",
            "\u1e56": "P",
            "\u01a4": "P",
            "\u2c63": "P",
            "\ua750": "P",
            "\ua752": "P",
            "\ua754": "P",
            "\u24c6": "Q",
            "\uff31": "Q",
            "\ua756": "Q",
            "\ua758": "Q",
            "\u024a": "Q",
            "\u24c7": "R",
            "\uff32": "R",
            "\u0154": "R",
            "\u1e58": "R",
            "\u0158": "R",
            "\u0210": "R",
            "\u0212": "R",
            "\u1e5a": "R",
            "\u1e5c": "R",
            "\u0156": "R",
            "\u1e5e": "R",
            "\u024c": "R",
            "\u2c64": "R",
            "\ua75a": "R",
            "\ua7a6": "R",
            "\ua782": "R",
            "\u24c8": "S",
            "\uff33": "S",
            "\u1e9e": "S",
            "\u015a": "S",
            "\u1e64": "S",
            "\u015c": "S",
            "\u1e60": "S",
            "\u0160": "S",
            "\u1e66": "S",
            "\u1e62": "S",
            "\u1e68": "S",
            "\u0218": "S",
            "\u015e": "S",
            "\u2c7e": "S",
            "\ua7a8": "S",
            "\ua784": "S",
            "\u24c9": "T",
            "\uff34": "T",
            "\u1e6a": "T",
            "\u0164": "T",
            "\u1e6c": "T",
            "\u021a": "T",
            "\u0162": "T",
            "\u1e70": "T",
            "\u1e6e": "T",
            "\u0166": "T",
            "\u01ac": "T",
            "\u01ae": "T",
            "\u023e": "T",
            "\ua786": "T",
            "\ua728": "TZ",
            "\u24ca": "U",
            "\uff35": "U",
            "\xd9": "U",
            "\xda": "U",
            "\xdb": "U",
            "\u0168": "U",
            "\u1e78": "U",
            "\u016a": "U",
            "\u1e7a": "U",
            "\u016c": "U",
            "\xdc": "U",
            "\u01db": "U",
            "\u01d7": "U",
            "\u01d5": "U",
            "\u01d9": "U",
            "\u1ee6": "U",
            "\u016e": "U",
            "\u0170": "U",
            "\u01d3": "U",
            "\u0214": "U",
            "\u0216": "U",
            "\u01af": "U",
            "\u1eea": "U",
            "\u1ee8": "U",
            "\u1eee": "U",
            "\u1eec": "U",
            "\u1ef0": "U",
            "\u1ee4": "U",
            "\u1e72": "U",
            "\u0172": "U",
            "\u1e76": "U",
            "\u1e74": "U",
            "\u0244": "U",
            "\u24cb": "V",
            "\uff36": "V",
            "\u1e7c": "V",
            "\u1e7e": "V",
            "\u01b2": "V",
            "\ua75e": "V",
            "\u0245": "V",
            "\ua760": "VY",
            "\u24cc": "W",
            "\uff37": "W",
            "\u1e80": "W",
            "\u1e82": "W",
            "\u0174": "W",
            "\u1e86": "W",
            "\u1e84": "W",
            "\u1e88": "W",
            "\u2c72": "W",
            "\u24cd": "X",
            "\uff38": "X",
            "\u1e8a": "X",
            "\u1e8c": "X",
            "\u24ce": "Y",
            "\uff39": "Y",
            "\u1ef2": "Y",
            "\xdd": "Y",
            "\u0176": "Y",
            "\u1ef8": "Y",
            "\u0232": "Y",
            "\u1e8e": "Y",
            "\u0178": "Y",
            "\u1ef6": "Y",
            "\u1ef4": "Y",
            "\u01b3": "Y",
            "\u024e": "Y",
            "\u1efe": "Y",
            "\u24cf": "Z",
            "\uff3a": "Z",
            "\u0179": "Z",
            "\u1e90": "Z",
            "\u017b": "Z",
            "\u017d": "Z",
            "\u1e92": "Z",
            "\u1e94": "Z",
            "\u01b5": "Z",
            "\u0224": "Z",
            "\u2c7f": "Z",
            "\u2c6b": "Z",
            "\ua762": "Z",
            "\u24d0": "a",
            "\uff41": "a",
            "\u1e9a": "a",
            "\xe0": "a",
            "\xe1": "a",
            "\xe2": "a",
            "\u1ea7": "a",
            "\u1ea5": "a",
            "\u1eab": "a",
            "\u1ea9": "a",
            "\xe3": "a",
            "\u0101": "a",
            "\u0103": "a",
            "\u1eb1": "a",
            "\u1eaf": "a",
            "\u1eb5": "a",
            "\u1eb3": "a",
            "\u0227": "a",
            "\u01e1": "a",
            "\xe4": "a",
            "\u01df": "a",
            "\u1ea3": "a",
            "\xe5": "a",
            "\u01fb": "a",
            "\u01ce": "a",
            "\u0201": "a",
            "\u0203": "a",
            "\u1ea1": "a",
            "\u1ead": "a",
            "\u1eb7": "a",
            "\u1e01": "a",
            "\u0105": "a",
            "\u2c65": "a",
            "\u0250": "a",
            "\ua733": "aa",
            "\xe6": "ae",
            "\u01fd": "ae",
            "\u01e3": "ae",
            "\ua735": "ao",
            "\ua737": "au",
            "\ua739": "av",
            "\ua73b": "av",
            "\ua73d": "ay",
            "\u24d1": "b",
            "\uff42": "b",
            "\u1e03": "b",
            "\u1e05": "b",
            "\u1e07": "b",
            "\u0180": "b",
            "\u0183": "b",
            "\u0253": "b",
            "\u24d2": "c",
            "\uff43": "c",
            "\u0107": "c",
            "\u0109": "c",
            "\u010b": "c",
            "\u010d": "c",
            "\xe7": "c",
            "\u1e09": "c",
            "\u0188": "c",
            "\u023c": "c",
            "\ua73f": "c",
            "\u2184": "c",
            "\u24d3": "d",
            "\uff44": "d",
            "\u1e0b": "d",
            "\u010f": "d",
            "\u1e0d": "d",
            "\u1e11": "d",
            "\u1e13": "d",
            "\u1e0f": "d",
            "\u0111": "d",
            "\u018c": "d",
            "\u0256": "d",
            "\u0257": "d",
            "\ua77a": "d",
            "\u01f3": "dz",
            "\u01c6": "dz",
            "\u24d4": "e",
            "\uff45": "e",
            "\xe8": "e",
            "\xe9": "e",
            "\xea": "e",
            "\u1ec1": "e",
            "\u1ebf": "e",
            "\u1ec5": "e",
            "\u1ec3": "e",
            "\u1ebd": "e",
            "\u0113": "e",
            "\u1e15": "e",
            "\u1e17": "e",
            "\u0115": "e",
            "\u0117": "e",
            "\xeb": "e",
            "\u1ebb": "e",
            "\u011b": "e",
            "\u0205": "e",
            "\u0207": "e",
            "\u1eb9": "e",
            "\u1ec7": "e",
            "\u0229": "e",
            "\u1e1d": "e",
            "\u0119": "e",
            "\u1e19": "e",
            "\u1e1b": "e",
            "\u0247": "e",
            "\u025b": "e",
            "\u01dd": "e",
            "\u24d5": "f",
            "\uff46": "f",
            "\u1e1f": "f",
            "\u0192": "f",
            "\ua77c": "f",
            "\u24d6": "g",
            "\uff47": "g",
            "\u01f5": "g",
            "\u011d": "g",
            "\u1e21": "g",
            "\u011f": "g",
            "\u0121": "g",
            "\u01e7": "g",
            "\u0123": "g",
            "\u01e5": "g",
            "\u0260": "g",
            "\ua7a1": "g",
            "\u1d79": "g",
            "\ua77f": "g",
            "\u24d7": "h",
            "\uff48": "h",
            "\u0125": "h",
            "\u1e23": "h",
            "\u1e27": "h",
            "\u021f": "h",
            "\u1e25": "h",
            "\u1e29": "h",
            "\u1e2b": "h",
            "\u1e96": "h",
            "\u0127": "h",
            "\u2c68": "h",
            "\u2c76": "h",
            "\u0265": "h",
            "\u0195": "hv",
            "\u24d8": "i",
            "\uff49": "i",
            "\xec": "i",
            "\xed": "i",
            "\xee": "i",
            "\u0129": "i",
            "\u012b": "i",
            "\u012d": "i",
            "\xef": "i",
            "\u1e2f": "i",
            "\u1ec9": "i",
            "\u01d0": "i",
            "\u0209": "i",
            "\u020b": "i",
            "\u1ecb": "i",
            "\u012f": "i",
            "\u1e2d": "i",
            "\u0268": "i",
            "\u0131": "i",
            "\u24d9": "j",
            "\uff4a": "j",
            "\u0135": "j",
            "\u01f0": "j",
            "\u0249": "j",
            "\u24da": "k",
            "\uff4b": "k",
            "\u1e31": "k",
            "\u01e9": "k",
            "\u1e33": "k",
            "\u0137": "k",
            "\u1e35": "k",
            "\u0199": "k",
            "\u2c6a": "k",
            "\ua741": "k",
            "\ua743": "k",
            "\ua745": "k",
            "\ua7a3": "k",
            "\u24db": "l",
            "\uff4c": "l",
            "\u0140": "l",
            "\u013a": "l",
            "\u013e": "l",
            "\u1e37": "l",
            "\u1e39": "l",
            "\u013c": "l",
            "\u1e3d": "l",
            "\u1e3b": "l",
            "\u017f": "l",
            "\u0142": "l",
            "\u019a": "l",
            "\u026b": "l",
            "\u2c61": "l",
            "\ua749": "l",
            "\ua781": "l",
            "\ua747": "l",
            "\u01c9": "lj",
            "\u24dc": "m",
            "\uff4d": "m",
            "\u1e3f": "m",
            "\u1e41": "m",
            "\u1e43": "m",
            "\u0271": "m",
            "\u026f": "m",
            "\u24dd": "n",
            "\uff4e": "n",
            "\u01f9": "n",
            "\u0144": "n",
            "\xf1": "n",
            "\u1e45": "n",
            "\u0148": "n",
            "\u1e47": "n",
            "\u0146": "n",
            "\u1e4b": "n",
            "\u1e49": "n",
            "\u019e": "n",
            "\u0272": "n",
            "\u0149": "n",
            "\ua791": "n",
            "\ua7a5": "n",
            "\u01cc": "nj",
            "\u24de": "o",
            "\uff4f": "o",
            "\xf2": "o",
            "\xf3": "o",
            "\xf4": "o",
            "\u1ed3": "o",
            "\u1ed1": "o",
            "\u1ed7": "o",
            "\u1ed5": "o",
            "\xf5": "o",
            "\u1e4d": "o",
            "\u022d": "o",
            "\u1e4f": "o",
            "\u014d": "o",
            "\u1e51": "o",
            "\u1e53": "o",
            "\u014f": "o",
            "\u022f": "o",
            "\u0231": "o",
            "\xf6": "o",
            "\u022b": "o",
            "\u1ecf": "o",
            "\u0151": "o",
            "\u01d2": "o",
            "\u020d": "o",
            "\u020f": "o",
            "\u01a1": "o",
            "\u1edd": "o",
            "\u1edb": "o",
            "\u1ee1": "o",
            "\u1edf": "o",
            "\u1ee3": "o",
            "\u1ecd": "o",
            "\u1ed9": "o",
            "\u01eb": "o",
            "\u01ed": "o",
            "\xf8": "o",
            "\u01ff": "o",
            "\u0254": "o",
            "\ua74b": "o",
            "\ua74d": "o",
            "\u0275": "o",
            "\u01a3": "oi",
            "\u0223": "ou",
            "\ua74f": "oo",
            "\u24df": "p",
            "\uff50": "p",
            "\u1e55": "p",
            "\u1e57": "p",
            "\u01a5": "p",
            "\u1d7d": "p",
            "\ua751": "p",
            "\ua753": "p",
            "\ua755": "p",
            "\u24e0": "q",
            "\uff51": "q",
            "\u024b": "q",
            "\ua757": "q",
            "\ua759": "q",
            "\u24e1": "r",
            "\uff52": "r",
            "\u0155": "r",
            "\u1e59": "r",
            "\u0159": "r",
            "\u0211": "r",
            "\u0213": "r",
            "\u1e5b": "r",
            "\u1e5d": "r",
            "\u0157": "r",
            "\u1e5f": "r",
            "\u024d": "r",
            "\u027d": "r",
            "\ua75b": "r",
            "\ua7a7": "r",
            "\ua783": "r",
            "\u24e2": "s",
            "\uff53": "s",
            "\xdf": "s",
            "\u015b": "s",
            "\u1e65": "s",
            "\u015d": "s",
            "\u1e61": "s",
            "\u0161": "s",
            "\u1e67": "s",
            "\u1e63": "s",
            "\u1e69": "s",
            "\u0219": "s",
            "\u015f": "s",
            "\u023f": "s",
            "\ua7a9": "s",
            "\ua785": "s",
            "\u1e9b": "s",
            "\u24e3": "t",
            "\uff54": "t",
            "\u1e6b": "t",
            "\u1e97": "t",
            "\u0165": "t",
            "\u1e6d": "t",
            "\u021b": "t",
            "\u0163": "t",
            "\u1e71": "t",
            "\u1e6f": "t",
            "\u0167": "t",
            "\u01ad": "t",
            "\u0288": "t",
            "\u2c66": "t",
            "\ua787": "t",
            "\ua729": "tz",
            "\u24e4": "u",
            "\uff55": "u",
            "\xf9": "u",
            "\xfa": "u",
            "\xfb": "u",
            "\u0169": "u",
            "\u1e79": "u",
            "\u016b": "u",
            "\u1e7b": "u",
            "\u016d": "u",
            "\xfc": "u",
            "\u01dc": "u",
            "\u01d8": "u",
            "\u01d6": "u",
            "\u01da": "u",
            "\u1ee7": "u",
            "\u016f": "u",
            "\u0171": "u",
            "\u01d4": "u",
            "\u0215": "u",
            "\u0217": "u",
            "\u01b0": "u",
            "\u1eeb": "u",
            "\u1ee9": "u",
            "\u1eef": "u",
            "\u1eed": "u",
            "\u1ef1": "u",
            "\u1ee5": "u",
            "\u1e73": "u",
            "\u0173": "u",
            "\u1e77": "u",
            "\u1e75": "u",
            "\u0289": "u",
            "\u24e5": "v",
            "\uff56": "v",
            "\u1e7d": "v",
            "\u1e7f": "v",
            "\u028b": "v",
            "\ua75f": "v",
            "\u028c": "v",
            "\ua761": "vy",
            "\u24e6": "w",
            "\uff57": "w",
            "\u1e81": "w",
            "\u1e83": "w",
            "\u0175": "w",
            "\u1e87": "w",
            "\u1e85": "w",
            "\u1e98": "w",
            "\u1e89": "w",
            "\u2c73": "w",
            "\u24e7": "x",
            "\uff58": "x",
            "\u1e8b": "x",
            "\u1e8d": "x",
            "\u24e8": "y",
            "\uff59": "y",
            "\u1ef3": "y",
            "\xfd": "y",
            "\u0177": "y",
            "\u1ef9": "y",
            "\u0233": "y",
            "\u1e8f": "y",
            "\xff": "y",
            "\u1ef7": "y",
            "\u1e99": "y",
            "\u1ef5": "y",
            "\u01b4": "y",
            "\u024f": "y",
            "\u1eff": "y",
            "\u24e9": "z",
            "\uff5a": "z",
            "\u017a": "z",
            "\u1e91": "z",
            "\u017c": "z",
            "\u017e": "z",
            "\u1e93": "z",
            "\u1e95": "z",
            "\u01b6": "z",
            "\u0225": "z",
            "\u0240": "z",
            "\u2c6c": "z",
            "\ua763": "z",
            "\u0386": "\u0391",
            "\u0388": "\u0395",
            "\u0389": "\u0397",
            "\u038a": "\u0399",
            "\u03aa": "\u0399",
            "\u038c": "\u039f",
            "\u038e": "\u03a5",
            "\u03ab": "\u03a5",
            "\u038f": "\u03a9",
            "\u03ac": "\u03b1",
            "\u03ad": "\u03b5",
            "\u03ae": "\u03b7",
            "\u03af": "\u03b9",
            "\u03ca": "\u03b9",
            "\u0390": "\u03b9",
            "\u03cc": "\u03bf",
            "\u03cd": "\u03c5",
            "\u03cb": "\u03c5",
            "\u03b0": "\u03c5",
            "\u03c9": "\u03c9",
            "\u03c2": "\u03c3"
        };
        D = w(document), B = 1, I = function () {
            return B++
        }, P = M(E = M(Object, {
            bind: function (t) {
                var e = this;
                return function () {
                    t.apply(e, arguments)
                }
            }, init: function (t) {
                var o, e, i = ".select2-results";
                this.opts = t = this.prepareOpts(t), this.id = t.id, t.element.data("select2") !== b && null !== t.element.data("select2") && t.element.data("select2").destroy(), this.container = this.createContainer(), this.liveRegion = w("<span>", {
                    role: "status",
                    "aria-live": "polite"
                }).addClass("select2-hidden-accessible").appendTo(document.body), this.containerId = "s2id_" + (t.element.attr("id") || "autogen" + I()), this.containerEventName = this.containerId.replace(/([.])/g, "_").replace(/([;&,\-\.\+\*\~':"\!\^#$%@\[\]\(\)=>\|])/g, "\\$1"), this.container.attr("id", this.containerId), this.container.attr("title", t.element.attr("title")), this.body = w("body"), y(this.container, this.opts.element, this.opts.adaptContainerCssClass), this.container.attr("style", t.element.attr("style")), this.container.css(T(t.containerCss, this.opts.element)), this.container.addClass(T(t.containerCssClass, this.opts.element)), this.elementTabIndex = this.opts.element.attr("tabindex"), this.opts.element.data("select2", this).attr("tabindex", "-1").before(this.container).on("click.select2", g), this.container.data("select2", this), this.dropdown = this.container.find(".select2-drop"), y(this.dropdown, this.opts.element, this.opts.adaptDropdownCssClass), this.dropdown.addClass(T(t.dropdownCssClass, this.opts.element)), this.dropdown.data("select2", this), this.dropdown.on("click", g), this.results = o = this.container.find(i), this.search = e = this.container.find("input.select2-input"), this.queryCount = 0, this.resultsPage = 0, this.context = null, this.initContainer(), this.container.on("click", g), d(this.results), this.dropdown.on("mousemove-filtered", i, this.bind(this.highlightUnderEvent)), this.dropdown.on("touchstart touchmove touchend", i, this.bind(function (t) {
                    this._touchEvent = !0, this.highlightUnderEvent(t)
                })), this.dropdown.on("touchmove", i, this.bind(this.touchMoved)), this.dropdown.on("touchstart touchend", i, this.bind(this.clearTouchMoved)), this.dropdown.on("click", this.bind(function () {
                    this._touchEvent && (this._touchEvent = !1, this.selectHighlighted())
                })), u(80, this.results), this.dropdown.on("scroll-debounced", i, this.bind(this.loadMoreIfNeeded)), w(this.container).on("change", ".select2-input", function (t) {
                    t.stopPropagation()
                }), w(this.dropdown).on("change", ".select2-input", function (t) {
                    t.stopPropagation()
                }), w.fn.mousewheel && o.mousewheel(function (t, e, i, n) {
                    var r = o.scrollTop();
                    0 < n && r - n <= 0 ? (o.scrollTop(0), g(t)) : n < 0 && o.get(0).scrollHeight - o.scrollTop() + n <= o.height() && (o.scrollTop(o.get(0).scrollHeight - o.height()), g(t))
                }), c(e), e.on("keyup-change input paste", this.bind(this.updateResults)), e.on("focus", function () {
                    e.addClass("select2-focused")
                }), e.on("blur", function () {
                    e.removeClass("select2-focused")
                }), this.dropdown.on("mouseup", i, this.bind(function (t) {
                    0 < w(t.target).closest(".select2-result-selectable").length && (this.highlightUnderEvent(t), this.selectHighlighted(t))
                })), this.dropdown.on("click mouseup mousedown touchstart touchend focusin", function (t) {
                    t.stopPropagation()
                }), this.nextSearchTerm = b, w.isFunction(this.opts.initSelection) && (this.initSelection(), this.monitorSource()), null !== t.maximumInputLength && this.search.attr("maxlength", t.maximumInputLength);
                var n = t.element.prop("disabled");
                n === b && (n = !1), this.enable(!n);
                var r = t.element.prop("readonly");
                r === b && (r = !1), this.readonly(r), O = O || l(), this.autofocus = t.element.prop("autofocus"), t.element.prop("autofocus", !1), this.autofocus && this.focus(), this.search.attr("placeholder", t.searchInputPlaceholder)
            }, destroy: function () {
                var t = this.opts.element, e = t.data("select2");
                this.close(), t.length && t[0].detachEvent && t.each(function () {
                    this.detachEvent("onpropertychange", this._sync)
                }), this.propertyObserver && (this.propertyObserver.disconnect(), this.propertyObserver = null), this._sync = null, e !== b && (e.container.remove(), e.liveRegion.remove(), e.dropdown.remove(), t.removeClass("select2-offscreen").removeData("select2").off(".select2").prop("autofocus", this.autofocus || !1), this.elementTabIndex ? t.attr({tabindex: this.elementTabIndex}) : t.removeAttr("tabindex"), t.show()), $.call(this, "container", "liveRegion", "dropdown", "results", "search")
            }, optionToData: function (t) {
                return t.is("option") ? {
                    id: t.prop("value"),
                    text: t.text(),
                    element: t.get(),
                    css: t.attr("class"),
                    disabled: t.prop("disabled"),
                    locked: p(t.attr("locked"), "locked") || p(t.data("locked"), !0)
                } : t.is("optgroup") ? {
                    text: t.attr("label"),
                    children: [],
                    element: t.get(),
                    css: t.attr("class")
                } : void 0
            }, prepareOpts: function (y) {
                var s, t, e, i, x = this;
                if ("select" === (s = y.element).get(0).tagName.toLowerCase() && (this.select = t = y.element), t && w.each(["id", "multiple",
                    "ajax", "query", "createSearchChoice", "initSelection", "data", "tags"], function () {
                    if (this in y) throw new Error("Option '" + this + "' is not allowed for Select2 when attached to a <select> element.")
                }), "function" != typeof (y = w.extend({}, {
                    populateResults: function (t, e, f) {
                        var g, m = this.opts.id, v = this.liveRegion;
                        (g = function (t, e, i) {
                            var n, r, o, s, a, l, c, h, d, u, p = [];
                            for (n = 0, r = (t = y.sortResults(t, e, f)).length; n < r; n += 1) s = !(a = !0 === (o = t[n]).disabled) && m(o) !== b, l = o.children && 0 < o.children.length, (c = w("<li></li>")).addClass("select2-results-dept-" + i), c.addClass("select2-result"), c.addClass(s ? "select2-result-selectable" : "select2-result-unselectable"), a && c.addClass("select2-disabled"), l && c.addClass("select2-result-with-children"), c.addClass(x.opts.formatResultCssClass(o)), c.attr("role", "presentation"), (h = w(document.createElement("div"))).addClass("select2-result-label"), h.attr("id", "select2-result-label-" + I()), h.attr("role", "option"), (u = y.formatResult(o, h, f, x.opts.escapeMarkup)) !== b && (h.html(u), c.append(h)), l && ((d = w("<ul></ul>")).addClass("select2-result-sub"), g(o.children, d, i + 1), c.append(d)), c.data("select2-data", o), p.push(c[0]);
                            e.append(p), v.text(y.formatMatches(t.length))
                        })(e, t, 0)
                    }
                }, w.fn.select2.defaults, y)).id && (e = y.id, y.id = function (t) {
                    return t[e]
                }), w.isArray(y.element.data("select2Tags"))) {
                    if ("tags" in y) throw"tags specified as both an attribute 'data-select2-tags' and in options of Select2 " + y.element.attr("id");
                    y.tags = y.element.data("select2Tags")
                }
                if (t ? (y.query = this.bind(function (n) {
                    var t, e, r, i = {results: [], more: !1}, o = n.term;
                    r = function (t, e) {
                        var i;
                        t.is("option") ? n.matcher(o, t.text(), t) && e.push(x.optionToData(t)) : t.is("optgroup") && (i = x.optionToData(t), t.children().each2(function (t, e) {
                            r(e, i.children)
                        }), 0 < i.children.length && e.push(i))
                    }, t = s.children(), this.getPlaceholder() !== b && 0 < t.length && ((e = this.getPlaceholderOption()) && (t = t.not(e))), t.each2(function (t, e) {
                        r(e, i.results)
                    }), n.callback(i)
                }), y.id = function (t) {
                    return t.id
                }) : "query" in y || ("ajax" in y ? ((i = y.element.data("ajax-url")) && 0 < i.length && (y.ajax.url = i), y.query = C.call(y.element, y.ajax)) : "data" in y ? y.query = _(y.data) : "tags" in y && (y.query = k(y.tags), y.createSearchChoice === b && (y.createSearchChoice = function (t) {
                    return {id: w.trim(t), text: w.trim(t)}
                }), y.initSelection === b && (y.initSelection = function (t, e) {
                    var i = [];
                    w(r(t.val(), y.separator)).each(function () {
                        var t = {id: this, text: this}, e = y.tags;
                        w.isFunction(e) && (e = e()), w(e).each(function () {
                            return p(this.id, t.id) ? (t = this, !1) : void 0
                        }), i.push(t)
                    }), e(i)
                }))), "function" != typeof y.query) throw"query function not defined for Select2 " + y.element.attr("id");
                if ("top" === y.createSearchChoicePosition) y.createSearchChoicePosition = function (t, e) {
                    t.unshift(e)
                }; else if ("bottom" === y.createSearchChoicePosition) y.createSearchChoicePosition = function (t, e) {
                    t.push(e)
                }; else if ("function" != typeof y.createSearchChoicePosition) throw"invalid createSearchChoicePosition option must be 'top', 'bottom' or a custom function";
                return y
            }, monitorSource: function () {
                var t, i = this.opts.element, e = this;
                i.on("change.select2", this.bind(function () {
                    !0 !== this.opts.element.data("select2-change-triggered") && this.initSelection()
                })), this._sync = this.bind(function () {
                    var t = i.prop("disabled");
                    t === b && (t = !1), this.enable(!t);
                    var e = i.prop("readonly");
                    e === b && (e = !1), this.readonly(e), y(this.container, this.opts.element, this.opts.adaptContainerCssClass), this.container.addClass(T(this.opts.containerCssClass, this.opts.element)), y(this.dropdown, this.opts.element, this.opts.adaptDropdownCssClass), this.dropdown.addClass(T(this.opts.dropdownCssClass, this.opts.element))
                }), i.length && i[0].attachEvent && i.each(function () {
                    this.attachEvent("onpropertychange", e._sync)
                }), (t = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver) !== b && (this.propertyObserver && (delete this.propertyObserver, this.propertyObserver = null), this.propertyObserver = new t(function (t) {
                    w.each(t, e._sync)
                }), this.propertyObserver.observe(i.get(0), {attributes: !0, subtree: !1}))
            }, triggerSelect: function (t) {
                var e = w.Event("select2-selecting", {val: this.id(t), object: t, choice: t});
                return this.opts.element.trigger(e), !e.isDefaultPrevented()
            }, triggerChange: function (t) {
                t = t || {}, t = w.extend({}, t, {
                    type: "change",
                    val: this.val()
                }), this.opts.element.data("select2-change-triggered", !0), this.opts.element.trigger(t), this.opts.element.data("select2-change-triggered", !1), this.opts.element.click(), this.opts.blurOnChange && this.opts.element.blur()
            }, isInterfaceEnabled: function () {
                return !0 === this.enabledInterface
            }, enableInterface: function () {
                var t = this._enabled && !this._readonly, e = !t;
                return t !== this.enabledInterface && (this.container.toggleClass("select2-container-disabled", e), this.close(), this.enabledInterface = t, !0)
            }, enable: function (t) {
                t === b && (t = !0), this._enabled !== t && (this._enabled = t, this.opts.element.prop("disabled", !t), this.enableInterface())
            }, disable: function () {
                this.enable(!1)
            }, readonly: function (t) {
                t === b && (t = !1), this._readonly !== t && (this._readonly = t, this.opts.element.prop("readonly", t), this.enableInterface())
            }, opened: function () {
                return !!this.container && this.container.hasClass("select2-dropdown-open")
            }, positionDropdown: function () {
                var t, e, i, n, r, o = this.dropdown, s = this.container.offset(), a = this.container.outerHeight(!1),
                    l = this.container.outerWidth(!1), c = o.outerHeight(!1), h = w(window), d = h.width(),
                    u = h.height(), p = h.scrollLeft() + d, f = h.scrollTop() + u, g = s.top + a, m = s.left,
                    v = g + c <= f, y = s.top - c >= h.scrollTop(), x = o.outerWidth(!1), b = m + x <= p;
                o.hasClass("select2-drop-above") ? (e = !0, !y && v && (e = !(i = !0))) : (e = !1, !v && y && (e = i = !0)), i && (o.hide(), s = this.container.offset(), a = this.container.outerHeight(!1), l = this.container.outerWidth(!1), c = o.outerHeight(!1), p = h.scrollLeft() + d, f = h.scrollTop() + u, g = s.top + a, b = (m = s.left) + (x = o.outerWidth(!1)) <= p, o.show(), this.focusSearch()), this.opts.dropdownAutoWidth ? (r = w(".select2-results", o)[0], o.addClass("select2-drop-auto-width"), o.css("width", ""), l < (x = o.outerWidth(!1) + (r.scrollHeight === r.clientHeight ? 0 : O.width)) ? l = x : x = l, c = o.outerHeight(!1), b = m + x <= p) : this.container.removeClass("select2-drop-auto-width"), "static" !== this.body.css("position") && (g -= (t = this.body.offset()).top, m -= t.left), b || (m = s.left + this.container.outerWidth(!1) - x), n = {
                    left: m,
                    width: l
                }, e ? (n.top = s.top - c, n.bottom = "auto", this.container.addClass("select2-drop-above"), o.addClass("select2-drop-above")) : (n.top = g, n.bottom = "auto", this.container.removeClass("select2-drop-above"), o.removeClass("select2-drop-above")), n = w.extend(n, T(this.opts.dropdownCss, this.opts.element)), o.css(n)
            }, shouldOpen: function () {
                var t;
                return !this.opened() && (!1 !== this._enabled && !0 !== this._readonly && (t = w.Event("select2-opening"), this.opts.element.trigger(t), !t.isDefaultPrevented()))
            }, clearDropdownAlignmentPreference: function () {
                this.container.removeClass("select2-drop-above"), this.dropdown.removeClass("select2-drop-above")
            }, open: function () {
                return !!this.shouldOpen() && (this.opening(), D.on("mousemove.select2Event", function (t) {
                    N.x = t.pageX, N.y = t.pageY
                }), !0)
            }, opening: function () {
                var n, t = this.containerEventName, e = "scroll." + t, i = "resize." + t, r = "orientationchange." + t;
                this.container.addClass("select2-dropdown-open").addClass("select2-container-active"), this.clearDropdownAlignmentPreference(), this.dropdown[0] !== this.body.children().last()[0] && this.dropdown.detach().appendTo(this.body), 0 == (n = w("#select2-drop-mask")).length && ((n = w(document.createElement("div"))).attr("id", "select2-drop-mask").attr("class", "select2-drop-mask"), n.hide(), n.appendTo(this.body), n.on("mousedown touchstart click", function (t) {
                    s(n);
                    var e, i = w("#select2-drop");
                    0 < i.length && ((e = i.data("select2")).opts.selectOnBlur && e.selectHighlighted({noFocus: !0}), e.close(), t.preventDefault(), t.stopPropagation())
                })), this.dropdown.prev()[0] !== n[0] && this.dropdown.before(n), w("#select2-drop").removeAttr("id"), this.dropdown.attr("id", "select2-drop"), n.show(), this.positionDropdown(), this.dropdown.show(), this.positionDropdown(), this.dropdown.addClass("select2-drop-active");
                var o = this;
                this.container.parents().add(window).each(function () {
                    w(this).on(i + " " + e + " " + r, function () {
                        o.opened() && o.positionDropdown()
                    })
                })
            }, close: function () {
                if (this.opened()) {
                    var t = this.containerEventName, e = "scroll." + t, i = "resize." + t, n = "orientationchange." + t;
                    this.container.parents().add(window).each(function () {
                        w(this).off(e).off(i).off(n)
                    }), this.clearDropdownAlignmentPreference(), w("#select2-drop-mask").hide(), this.dropdown.removeAttr("id"), this.dropdown.hide(), this.container.removeClass("select2-dropdown-open").removeClass("select2-container-active"), this.results.empty(), D.off("mousemove.select2Event"), this.clearSearch(), this.search.removeClass("select2-active"), this.opts.element.trigger(w.Event("select2-close"))
                }
            }, externalSearch: function (t) {
                this.open(), this.search.val(t), this.updateResults(!1)
            }, clearSearch: function () {
            }, getMaximumSelectionSize: function () {
                return T(this.opts.maximumSelectionSize, this.opts.element)
            }, ensureHighlightVisible: function () {
                var t, e, i, n, r, o, s, a, l = this.results;
                if (!((e = this.highlight()) < 0)) {
                    if (0 == e) return void l.scrollTop(0);
                    t = this.findHighlightableChoices().find(".select2-result-label"), n = (a = ((i = w(t[e])).offset() || {}).top || 0) + i.outerHeight(!0), e === t.length - 1 && (0 < (s = l.find("li.select2-more-results")).length && (n = s.offset().top + s.outerHeight(!0))), (r = l.offset().top + l.outerHeight(!0)) < n && l.scrollTop(l.scrollTop() + (n - r)), (o = a - l.offset().top) < 0 && "none" != i.css("display") && l.scrollTop(l.scrollTop() + o)
                }
            }, findHighlightableChoices: function () {
                return this.results.find(".select2-result-selectable:not(.select2-disabled):not(.select2-selected)")
            }, moveHighlight: function (t) {
                for (var e = this.findHighlightableChoices(), i = this.highlight(); -1 < i && i < e.length;) {
                    var n = w(e[i += t]);
                    if (n.hasClass("select2-result-selectable") && !n.hasClass("select2-disabled") && !n.hasClass("select2-selected")) {
                        this.highlight(i);
                        break
                    }
                }
            }, highlight: function (t) {
                var e, i, n = this.findHighlightableChoices();
                return 0 === arguments.length ? h(n.filter(".select2-highlighted")[0], n.get()) : (t >= n.length && (t = n.length - 1), t < 0 && (t = 0), this.removeHighlight(), (e = w(n[t])).addClass("select2-highlighted"), this.search.attr("aria-activedescendant", e.find(".select2-result-label").attr("id")), this.ensureHighlightVisible(), this.liveRegion.text(e.text()), void ((i = e.data("select2-data")) && this.opts.element.trigger({
                    type: "select2-highlight",
                    val: this.id(i),
                    choice: i
                })))
            }, removeHighlight: function () {
                this.results.find(".select2-highlighted").removeClass("select2-highlighted")
            }, touchMoved: function () {
                this._touchMoved = !0
            }, clearTouchMoved: function () {
                this._touchMoved = !1
            }, countSelectableResults: function () {
                return this.findHighlightableChoices().length
            }, highlightUnderEvent: function (t) {
                var e = w(t.target).closest(".select2-result-selectable");
                if (0 < e.length && !e.is(".select2-highlighted")) {
                    var i = this.findHighlightableChoices();
                    this.highlight(i.index(e))
                } else 0 == e.length && this.removeHighlight()
            }, loadMoreIfNeeded: function () {
                var e = this.results, i = e.find("li.select2-more-results"), n = this.resultsPage + 1, r = this,
                    o = this.search.val(), s = this.context;
                0 !== i.length && (i.offset().top - e.offset().top - e.height() <= this.opts.loadMorePadding && (i.addClass("select2-active"), this.opts.query({
                    element: this.opts.element,
                    term: o,
                    page: n,
                    context: s,
                    matcher: this.opts.matcher,
                    callback: this.bind(function (t) {
                        r.opened() && (r.opts.populateResults.call(this, e, t.results, {
                            term: o,
                            page: n,
                            context: s
                        }), r.postprocessResults(t, !1, !1), !0 === t.more ? (i.detach().appendTo(e).text(T(r.opts.formatLoadMore, r.opts.element, n + 1)), window.setTimeout(function () {
                            r.loadMoreIfNeeded()
                        }, 10)) : i.remove(), r.positionDropdown(), r.resultsPage = n, r.context = t.context, this.opts.element.trigger({
                            type: "select2-loaded",
                            items: t
                        }))
                    })
                })))
            }, tokenize: function () {
            }, updateResults: function (i) {
                function n() {
                    s.removeClass("select2-active"), c.positionDropdown(), a.find(".select2-no-results,.select2-selection-limit,.select2-searching").length ? c.liveRegion.text(a.text()) : c.liveRegion.text(c.opts.formatMatches(a.find(".select2-result-selectable").length))
                }

                function r(t) {
                    a.html(t), n()
                }

                var t, e, o, s = this.search, a = this.results, l = this.opts, c = this, h = s.val(),
                    d = w.data(this.container, "select2-last-term");
                if ((!0 === i || !d || !p(h, d)) && (w.data(this.container, "select2-last-term", h), !0 === i || !1 !== this.showSearchInput && this.opened())) {
                    o = ++this.queryCount;
                    var u = this.getMaximumSelectionSize();
                    if (1 <= u && (t = this.data(), w.isArray(t) && t.length >= u && S(l.formatSelectionTooBig, "formatSelectionTooBig"))) return void r("<li class='select2-selection-limit'>" + T(l.formatSelectionTooBig, l.element, u) + "</li>");
                    if (s.val().length < l.minimumInputLength) return S(l.formatInputTooShort, "formatInputTooShort") ? r("<li class='select2-no-results'>" + T(l.formatInputTooShort, l.element, s.val(), l.minimumInputLength) + "</li>") : r(""), void (i && this.showSearch && this.showSearch(!0));
                    if (l.maximumInputLength && s.val().length > l.maximumInputLength) return void (S(l.formatInputTooLong, "formatInputTooLong") ? r("<li class='select2-no-results'>" + T(l.formatInputTooLong, l.element, s.val(), l.maximumInputLength) + "</li>") : r(""));
                    l.formatSearching && 0 === this.findHighlightableChoices().length && r("<li class='select2-searching'>" + T(l.formatSearching, l.element) + "</li>"), s.addClass("select2-active"), this.removeHighlight(), (e = this.tokenize()) != b && null != e && s.val(e), this.resultsPage = 1, l.query({
                        element: l.element,
                        term: s.val(),
                        page: this.resultsPage,
                        context: null,
                        matcher: l.matcher,
                        callback: this.bind(function (t) {
                            var e;
                            if (o == this.queryCount) {
                                if (!this.opened()) return void this.search.removeClass("select2-active");
                                if (this.context = t.context === b ? null : t.context, this.opts.createSearchChoice && "" !== s.val() && ((e = this.opts.createSearchChoice.call(c, s.val(), t.results)) !== b && null !== e && c.id(e) !== b && null !== c.id(e) && 0 === w(t.results).filter(function () {
                                    return p(c.id(this), c.id(e))
                                }).length && this.opts.createSearchChoicePosition(t.results, e)), 0 === t.results.length && S(l.formatNoMatches, "formatNoMatches")) return void r("<li class='select2-no-results'>" + T(l.formatNoMatches, l.element, s.val()) + "</li>");
                                a.empty(), c.opts.populateResults.call(this, a, t.results, {
                                    term: s.val(),
                                    page: this.resultsPage,
                                    context: null
                                }), !0 === t.more && S(l.formatLoadMore, "formatLoadMore") && (a.append("<li class='select2-more-results'>" + l.escapeMarkup(T(l.formatLoadMore, l.element, this.resultsPage)) + "</li>"), window.setTimeout(function () {
                                    c.loadMoreIfNeeded()
                                }, 10)), this.postprocessResults(t, i), n(), this.opts.element.trigger({
                                    type: "select2-loaded",
                                    items: t
                                })
                            }
                        })
                    })
                }
            }, cancel: function () {
                this.close()
            }, blur: function () {
                this.opts.selectOnBlur && this.selectHighlighted({noFocus: !0}), this.close(), this.container.removeClass("select2-container-active"), this.search[0] === document.activeElement && this.search.blur(), this.clearSearch(), this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus")
            }, focusSearch: function () {
                t(this.search)
            }, selectHighlighted: function (t) {
                if (this._touchMoved) this.clearTouchMoved(); else {
                    var e = this.highlight(),
                        i = this.results.find(".select2-highlighted").closest(".select2-result").data("select2-data");
                    i ? (this.highlight(e), this.onSelect(i, t)) : t && t.noFocus && this.close()
                }
            }, getPlaceholder: function () {
                var t;
                return this.opts.element.attr("placeholder") || this.opts.element.attr("data-placeholder") || this.opts.element.data("placeholder") || this.opts.placeholder || ((t = this.getPlaceholderOption()) !== b ? t.text() : b)
            }, getPlaceholderOption: function () {
                if (this.select) {
                    var t = this.select.children("option").first();
                    if (this.opts.placeholderOption !== b) return "first" === this.opts.placeholderOption && t || "function" == typeof this.opts.placeholderOption && this.opts.placeholderOption(this.select);
                    if ("" === w.trim(t.text()) && "" === t.val()) return t
                }
            }, initContainerWidth: function () {
                function t() {
                    var t, e, i, n, r;
                    if ("off" === this.opts.width) return null;
                    if ("element" === this.opts.width) return 0 === this.opts.element.outerWidth(!1) ? "auto" : this.opts.element.outerWidth(!1) + "px";
                    if ("copy" !== this.opts.width && "resolve" !== this.opts.width) return w.isFunction(this.opts.width) ? this.opts.width() : this.opts.width;
                    if ((t = this.opts.element.attr("style")) !== b) for (n = 0, r = (e = t.split(";")).length; n < r; n += 1) if (null !== (i = e[n].replace(/\s/g, "").match(/^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i)) && 1 <= i.length) return i[1];
                    return "resolve" === this.opts.width ? 0 < (t = this.opts.element.css("width")).indexOf("%") ? t : 0 === this.opts.element.outerWidth(!1) ? "auto" : this.opts.element.outerWidth(!1) + "px" : null
                }

                var e = t.call(this);
                null !== e && this.container.css("width", e)
            }
        }), {
            createContainer: function () {
                return w(document.createElement("div")).attr({"class": "select2-container"}).html(["<a href='javascript:void(0)' class='select2-choice' tabindex='-1'>", "   <span class='select2-chosen'>&#160;</span><abbr class='select2-search-choice-close'></abbr>", "   <span class='select2-arrow' role='presentation'><b role='presentation'></b></span>", "</a>", "<label for='' class='select2-offscreen'></label>", "<input class='select2-focusser select2-offscreen' type='text' aria-haspopup='true' role='button' />", "<div class='select2-drop select2-display-none'>", "   <div class='select2-search'>", "       <label for='' class='select2-offscreen'></label>", "       <input type='text' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' class='select2-input' role='combobox' aria-expanded='true'", "       aria-autocomplete='list' />", "   </div>", "   <ul class='select2-results' role='listbox'>", "   </ul>", "</div>"].join(""))
            }, enableInterface: function () {
                this.parent.enableInterface.apply(this, arguments) && this.focusser.prop("disabled", !this.isInterfaceEnabled())
            }, opening: function () {
                var t, e, i;
                0 <= this.opts.minimumResultsForSearch && this.showSearch(!0), this.parent.opening.apply(this, arguments), !1 !== this.showSearchInput && this.search.val(this.focusser.val()), this.opts.shouldFocusInput(this) && (this.search.focus(), (t = this.search.get(0)).createTextRange ? ((e = t.createTextRange()).collapse(!1), e.select()) : t.setSelectionRange && (i = this.search.val().length, t.setSelectionRange(i, i))), "" === this.search.val() && this.nextSearchTerm != b && (this.search.val(this.nextSearchTerm), this.search.select()), this.focusser.prop("disabled", !0).val(""), this.updateResults(!0), this.opts.element.trigger(w.Event("select2-open"))
            }, close: function () {
                this.opened() && (this.parent.close.apply(this, arguments), this.focusser.prop("disabled", !1), this.opts.shouldFocusInput(this) && this.focusser.focus())
            }, focus: function () {
                this.opened() ? this.close() : (this.focusser.prop("disabled", !1), this.opts.shouldFocusInput(this) && this.focusser.focus())
            }, isFocused: function () {
                return this.container.hasClass("select2-container-active")
            }, cancel: function () {
                this.parent.cancel.apply(this, arguments), this.focusser.prop("disabled", !1), this.opts.shouldFocusInput(this) && this.focusser.focus()
            }, destroy: function () {
                w("label[for='" + this.focusser.attr("id") + "']").attr("for", this.opts.element.attr("id")), this.parent.destroy.apply(this, arguments), $.call(this, "selection", "focusser")
            }, initContainer: function () {
                var e, t, i = this.container, n = this.dropdown, r = I();
                this.opts.minimumResultsForSearch < 0 ? this.showSearch(!1) : this.showSearch(!0), this.selection = e = i.find(".select2-choice"), this.focusser = i.find(".select2-focusser"), e.find(".select2-chosen").attr("id", "select2-chosen-" + r), this.focusser.attr("aria-labelledby", "select2-chosen-" + r), this.results.attr("id", "select2-results-" + r), this.search.attr("aria-owns", "select2-results-" + r), this.focusser.attr("id", "s2id_autogen" + r), t = w("label[for='" + this.opts.element.attr("id") + "']"), this.focusser.prev().text(t.text()).attr("for", this.focusser.attr("id"));
                var o = this.opts.element.attr("title");
                this.opts.element.attr("title", o || t.text()), this.focusser.attr("tabindex", this.elementTabIndex), this.search.attr("id", this.focusser.attr("id") + "_search"), this.search.prev().text(w("label[for='" + this.focusser.attr("id") + "']").text()).attr("for", this.search.attr("id")), this.search.on("keydown", this.bind(function (t) {
                    if (this.isInterfaceEnabled()) {
                        if (t.which === j.PAGE_UP || t.which === j.PAGE_DOWN) return void g(t);
                        switch (t.which) {
                            case j.UP:
                            case j.DOWN:
                                return this.moveHighlight(t.which === j.UP ? -1 : 1), void g(t);
                            case j.ENTER:
                                return this.selectHighlighted(), void g(t);
                            case j.TAB:
                                return void this.selectHighlighted({noFocus: !0});
                            case j.ESC:
                                return this.cancel(t), void g(t)
                        }
                    }
                })), this.search.on("blur", this.bind(function () {
                    document.activeElement === this.body.get(0) && window.setTimeout(this.bind(function () {
                        this.opened() && this.search.focus()
                    }), 0)
                })), this.focusser.on("keydown", this.bind(function (t) {
                    if (this.isInterfaceEnabled() && t.which !== j.TAB && !j.isControl(t) && !j.isFunctionKey(t) && t.which !== j.ESC) {
                        if (!1 === this.opts.openOnEnter && t.which === j.ENTER) return void g(t);
                        if (t.which == j.DOWN || t.which == j.UP || t.which == j.ENTER && this.opts.openOnEnter) {
                            if (t.altKey || t.ctrlKey || t.shiftKey || t.metaKey) return;
                            return this.open(), void g(t)
                        }
                        return t.which == j.DELETE || t.which == j.BACKSPACE ? (this.opts.allowClear && this.clear(), void g(t)) : void 0
                    }
                })), c(this.focusser), this.focusser.on("keyup-change input", this.bind(function (t) {
                    if (0 <= this.opts.minimumResultsForSearch) {
                        if (t.stopPropagation(), this.opened()) return;
                        this.open()
                    }
                })), e.on("mousedown touchstart", "abbr", this.bind(function (t) {
                    this.isInterfaceEnabled() && (this.clear(), m(t), this.close(), this.selection.focus())
                })), e.on("mousedown touchstart", this.bind(function (t) {
                    s(e), this.container.hasClass("select2-container-active") || this.opts.element.trigger(w.Event("select2-focus")), this.opened() ? this.close() : this.isInterfaceEnabled() && this.open(), g(t)
                })), n.on("mousedown touchstart", this.bind(function () {
                    this.opts.shouldFocusInput(this) && this.search.focus()
                })), e.on("focus", this.bind(function (t) {
                    g(t)
                })), this.focusser.on("focus", this.bind(function () {
                    this.container.hasClass("select2-container-active") || this.opts.element.trigger(w.Event("select2-focus")), this.container.addClass("select2-container-active")
                })).on("blur", this.bind(function () {
                    this.opened() || (this.container.removeClass("select2-container-active"), this.opts.element.trigger(w.Event("select2-blur")))
                })), this.search.on("focus", this.bind(function () {
                    this.container.hasClass("select2-container-active") || this.opts.element.trigger(w.Event("select2-focus")), this.container.addClass("select2-container-active")
                })), this.initContainerWidth(), this.opts.element.addClass("select2-offscreen"), this.setPlaceholder()
            }, clear: function (t) {
                var e = this.selection.data("select2-data");
                if (e) {
                    var i = w.Event("select2-clearing");
                    if (this.opts.element.trigger(i), i.isDefaultPrevented()) return;
                    var n = this.getPlaceholderOption();
                    this.opts.element.val(n ? n.val() : ""), this.selection.find(".select2-chosen").empty(), this.selection.removeData("select2-data"), this.setPlaceholder(), !1 !== t && (this.opts.element.trigger({
                        type: "select2-removed",
                        val: this.id(e),
                        choice: e
                    }), this.triggerChange({removed: e}))
                }
            }, initSelection: function () {
                if (this.isPlaceholderOptionSelected()) this.updateSelection(null), this.close(), this.setPlaceholder(); else {
                    var e = this;
                    this.opts.initSelection.call(null, this.opts.element, function (t) {
                        t !== b && null !== t && (e.updateSelection(t), e.close(), e.setPlaceholder(), e.nextSearchTerm = e.opts.nextSearchTerm(t, e.search.val()))
                    })
                }
            }, isPlaceholderOptionSelected: function () {
                var t;
                return this.getPlaceholder() !== b && ((t = this.getPlaceholderOption()) !== b && t.prop("selected") || "" === this.opts.element.val() || this.opts.element.val() === b || null === this.opts.element.val())
            }, prepareOpts: function () {
                var s = this.parent.prepareOpts.apply(this, arguments), n = this;
                return "select" === s.element.get(0).tagName.toLowerCase() ? s.initSelection = function (t, e) {
                    var i = t.find("option").filter(function () {
                        return this.selected && !this.disabled
                    });
                    e(n.optionToData(i))
                } : "data" in s && (s.initSelection = s.initSelection || function (t, e) {
                    var r = t.val(), o = null;
                    s.query({
                        matcher: function (t, e, i) {
                            var n = p(r, s.id(i));
                            return n && (o = i), n
                        }, callback: w.isFunction(e) ? function () {
                            e(o)
                        } : w.noop
                    })
                }), s
            }, getPlaceholder: function () {
                return this.select && this.getPlaceholderOption() === b ? b : this.parent.getPlaceholder.apply(this, arguments)
            }, setPlaceholder: function () {
                var t = this.getPlaceholder();
                if (this.isPlaceholderOptionSelected() && t !== b) {
                    if (this.select && this.getPlaceholderOption() === b) return;
                    this.selection.find(".select2-chosen").html(this.opts.escapeMarkup(t)), this.selection.addClass("select2-default"), this.container.removeClass("select2-allowclear")
                }
            }, postprocessResults: function (t, e, i) {
                var n = 0, r = this;
                if (this.findHighlightableChoices().each2(function (t, e) {
                    return p(r.id(e.data("select2-data")), r.opts.element.val()) ? (n = t, !1) : void 0
                }), !1 !== i && (!0 === e && 0 <= n ? this.highlight(n) : this.highlight(0)), !0 === e) {
                    var o = this.opts.minimumResultsForSearch;
                    0 <= o && this.showSearch(A(t.results) >= o)
                }
            }, showSearch: function (t) {
                this.showSearchInput !== t && (this.showSearchInput = t, this.dropdown.find(".select2-search").toggleClass("select2-search-hidden", !t), this.dropdown.find(".select2-search").toggleClass("select2-offscreen", !t), w(this.dropdown, this.container).toggleClass("select2-with-searchbox", t))
            }, onSelect: function (t, e) {
                if (this.triggerSelect(t)) {
                    var i = this.opts.element.val(), n = this.data();
                    this.opts.element.val(this.id(t)), this.updateSelection(t), this.opts.element.trigger({
                        type: "select2-selected",
                        val: this.id(t),
                        choice: t
                    }), this.nextSearchTerm = this.opts.nextSearchTerm(t, this.search.val()), this.close(), e && e.noFocus || !this.opts.shouldFocusInput(this) || this.focusser.focus(), p(i, this.id(t)) || this.triggerChange({
                        added: t,
                        removed: n
                    })
                }
            }, updateSelection: function (t) {
                var e, i, n = this.selection.find(".select2-chosen");
                this.selection.data("select2-data", t), n.empty(), null !== t && (e = this.opts.formatSelection(t, n, this.opts.escapeMarkup)), e !== b && n.append(e), (i = this.opts.formatSelectionCssClass(t, n)) !== b && n.addClass(i), this.selection.removeClass("select2-default"), this.opts.allowClear && this.getPlaceholder() !== b && this.container.addClass("select2-allowclear")
            }, val: function (t, e) {
                var i, n = !1, r = null, o = this, s = this.data();
                if (0 === arguments.length) return this.opts.element.val();
                if (i = t, 1 < arguments.length && (n = e), this.select) this.select.val(i).find("option").filter(function () {
                    return this.selected
                }).each2(function (t, e) {
                    return r = o.optionToData(e), !1
                }), this.updateSelection(r), this.setPlaceholder(), n && this.triggerChange({
                    added: r,
                    removed: s
                }); else {
                    if (!i && 0 !== i) return void this.clear(n);
                    if (this.opts.initSelection === b) throw new Error("cannot call val() if initSelection() is not defined");
                    this.opts.element.val(i), this.opts.initSelection(this.opts.element, function (t) {
                        o.opts.element.val(t ? o.id(t) : ""), o.updateSelection(t), o.setPlaceholder(), n && o.triggerChange({
                            added: t,
                            removed: s
                        })
                    })
                }
            }, clearSearch: function () {
                this.search.val(""), this.focusser.val("")
            }, data: function (t, e) {
                var i, n = !1;
                return 0 === arguments.length ? ((i = this.selection.data("select2-data")) == b && (i = null), i) : (1 < arguments.length && (n = e), void (t ? (i = this.data(), this.opts.element.val(t ? this.id(t) : ""), this.updateSelection(t), n && this.triggerChange({
                    added: t,
                    removed: i
                })) : this.clear(n)))
            }
        }), L = M(E, {
            createContainer: function () {
                return w(document.createElement("div")).attr({"class": "select2-container select2-container-multi"}).html(["<ul class='select2-choices'>", "  <li class='select2-search-field'>", "    <label for='' class='select2-offscreen'></label>", "    <input type='text' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' class='select2-input'>", "  </li>", "</ul>", "<div class='select2-drop select2-drop-multi select2-display-none'>", "   <ul class='select2-results'>", "   </ul>", "</div>"].join(""))
            }, prepareOpts: function () {
                var l = this.parent.prepareOpts.apply(this, arguments), n = this;
                return "select" === l.element.get(0).tagName.toLowerCase() ? l.initSelection = function (t, e) {
                    var i = [];
                    t.find("option").filter(function () {
                        return this.selected && !this.disabled
                    }).each2(function (t, e) {
                        i.push(n.optionToData(e))
                    }), e(i)
                } : "data" in l && (l.initSelection = l.initSelection || function (t, o) {
                    var s = r(t.val(), l.separator), a = [];
                    l.query({
                        matcher: function (t, e, i) {
                            var n = w.grep(s, function (t) {
                                return p(t, l.id(i))
                            }).length;
                            return n && a.push(i), n
                        }, callback: w.isFunction(o) ? function () {
                            for (var t = [], e = 0; e < s.length; e++) for (var i = s[e], n = 0; n < a.length; n++) {
                                var r = a[n];
                                if (p(i, l.id(r))) {
                                    t.push(r), a.splice(n, 1);
                                    break
                                }
                            }
                            o(t)
                        } : w.noop
                    })
                }), l
            }, selectChoice: function (t) {
                var e = this.container.find(".select2-search-choice-focus");
                e.length && t && t[0] == e[0] || (e.length && this.opts.element.trigger("choice-deselected", e), e.removeClass("select2-search-choice-focus"), t && t.length && (this.close(), t.addClass("select2-search-choice-focus"), this.opts.element.trigger("choice-selected", t)))
            }, destroy: function () {
                w("label[for='" + this.search.attr("id") + "']").attr("for", this.opts.element.attr("id")), this.parent.destroy.apply(this, arguments), $.call(this, "searchContainer", "selection")
            }, initContainer: function () {
                var s, t = ".select2-choices";
                this.searchContainer = this.container.find(".select2-search-field"), this.selection = s = this.container.find(t);
                var e = this;
                this.selection.on("click", ".select2-search-choice:not(.select2-locked)", function () {
                    e.search[0].focus(), e.selectChoice(w(this))
                }), this.search.attr("id", "s2id_autogen" + I()), this.search.prev().text(w("label[for='" + this.opts.element.attr("id") + "']").text()).attr("for", this.search.attr("id")), this.search.on("input paste", this.bind(function () {
                    this.search.attr("placeholder") && 0 == this.search.val().length || this.isInterfaceEnabled() && (this.opened() || this.open())
                })), this.search.attr("tabindex", this.elementTabIndex), this.keydowns = 0, this.search.on("keydown", this.bind(function (t) {
                    if (this.isInterfaceEnabled()) {
                        ++this.keydowns;
                        var e = s.find(".select2-search-choice-focus"),
                            i = e.prev(".select2-search-choice:not(.select2-locked)"),
                            n = e.next(".select2-search-choice:not(.select2-locked)"), r = f(this.search);
                        if (e.length && (t.which == j.LEFT || t.which == j.RIGHT || t.which == j.BACKSPACE || t.which == j.DELETE || t.which == j.ENTER)) {
                            var o = e;
                            return t.which == j.LEFT && i.length ? o = i : t.which == j.RIGHT ? o = n.length ? n : null : t.which === j.BACKSPACE ? this.unselect(e.first()) && (this.search.width(10), o = i.length ? i : n) : t.which == j.DELETE ? this.unselect(e.first()) && (this.search.width(10), o = n.length ? n : null) : t.which == j.ENTER && (o = null), this.selectChoice(o), g(t), void (o && o.length || this.open())
                        }
                        if ((t.which === j.BACKSPACE && 1 == this.keydowns || t.which == j.LEFT) && 0 == r.offset && !r.length) return this.selectChoice(s.find(".select2-search-choice:not(.select2-locked)").last()), void g(t);
                        if (this.selectChoice(null), this.opened()) switch (t.which) {
                            case j.UP:
                            case j.DOWN:
                                return this.moveHighlight(t.which === j.UP ? -1 : 1), void g(t);
                            case j.ENTER:
                                return this.selectHighlighted(), void g(t);
                            case j.TAB:
                                return this.selectHighlighted({noFocus: !0}), void this.close();
                            case j.ESC:
                                return this.cancel(t), void g(t)
                        }
                        if (t.which !== j.TAB && !j.isControl(t) && !j.isFunctionKey(t) && t.which !== j.BACKSPACE && t.which !== j.ESC) {
                            if (t.which === j.ENTER) {
                                if (!1 === this.opts.openOnEnter) return;
                                if (t.altKey || t.ctrlKey || t.shiftKey || t.metaKey) return
                            }
                            this.open(), (t.which === j.PAGE_UP || t.which === j.PAGE_DOWN) && g(t), t.which === j.ENTER && g(t)
                        }
                    }
                })), this.search.on("keyup", this.bind(function () {
                    this.keydowns = 0, this.resizeSearch()
                })), this.search.on("blur", this.bind(function (t) {
                    this.container.removeClass("select2-container-active"), this.search.removeClass("select2-focused"), this.selectChoice(null), this.opened() || this.clearSearch(), t.stopImmediatePropagation(), this.opts.element.trigger(w.Event("select2-blur"))
                })), this.container.on("click", t, this.bind(function (t) {
                    this.isInterfaceEnabled() && (0 < w(t.target).closest(".select2-search-choice").length || (this.selectChoice(null), this.clearPlaceholder(), this.container.hasClass("select2-container-active") || this.opts.element.trigger(w.Event("select2-focus")), this.open(), this.focusSearch(), t.preventDefault()))
                })), this.container.on("focus", t, this.bind(function () {
                    this.isInterfaceEnabled() && (this.container.hasClass("select2-container-active") || this.opts.element.trigger(w.Event("select2-focus")), this.container.addClass("select2-container-active"), this.dropdown.addClass("select2-drop-active"), this.clearPlaceholder())
                })), this.initContainerWidth(), this.opts.element.addClass("select2-offscreen"), this.clearSearch()
            }, enableInterface: function () {
                this.parent.enableInterface.apply(this, arguments) && this.search.prop("disabled", !this.isInterfaceEnabled())
            }, initSelection: function () {
                if ("" === this.opts.element.val() && "" === this.opts.element.text() && (this.updateSelection([]), this.close(), this.clearSearch()), this.select || "" !== this.opts.element.val()) {
                    var e = this;
                    this.opts.initSelection.call(null, this.opts.element, function (t) {
                        t !== b && null !== t && (e.updateSelection(t), e.close(), e.clearSearch())
                    })
                }
            }, clearSearch: function () {
                var t = this.getPlaceholder(), e = this.getMaxSearchWidth();
                t !== b && 0 === this.getVal().length && !1 === this.search.hasClass("select2-focused") ? (this.search.val(t).addClass("select2-default"), this.search.width(0 < e ? e : this.container.css("width"))) : this.search.val("").width(10)
            }, clearPlaceholder: function () {
                this.search.hasClass("select2-default") && this.search.val("").removeClass("select2-default")
            }, opening: function () {
                this.clearPlaceholder(), this.resizeSearch(), this.parent.opening.apply(this, arguments), this.focusSearch(), "" === this.search.val() && this.nextSearchTerm != b && (this.search.val(this.nextSearchTerm), this.search.select()), this.updateResults(!0), this.opts.shouldFocusInput(this) && this.search.focus(), this.opts.element.trigger(w.Event("select2-open"))
            }, close: function () {
                this.opened() && this.parent.close.apply(this, arguments)
            }, focus: function () {
                this.close(), this.search.focus()
            }, isFocused: function () {
                return this.search.hasClass("select2-focused")
            }, updateSelection: function (t) {
                var e = [], i = [], n = this;
                w(t).each(function () {
                    h(n.id(this), e) < 0 && (e.push(n.id(this)), i.push(this))
                }), t = i, this.selection.find(".select2-search-choice").remove(), w(t).each(function () {
                    n.addSelectedChoice(this)
                }), n.postprocessResults()
            }, tokenize: function () {
                var t = this.search.val();
                null != (t = this.opts.tokenizer.call(this, t, this.data(), this.bind(this.onSelect), this.opts)) && t != b && (this.search.val(t), 0 < t.length && this.open())
            }, onSelect: function (t, e) {
                this.triggerSelect(t) && (this.addSelectedChoice(t), this.opts.element.trigger({
                    type: "selected",
                    val: this.id(t),
                    choice: t
                }), this.nextSearchTerm = this.opts.nextSearchTerm(t, this.search.val()), this.clearSearch(), this.updateResults(), (this.select || !this.opts.closeOnSelect) && this.postprocessResults(t, !1, !0 === this.opts.closeOnSelect), this.opts.closeOnSelect ? (this.close(), this.search.width(10)) : 0 < this.countSelectableResults() ? (this.search.width(10), this.resizeSearch(), 0 < this.getMaximumSelectionSize() && this.val().length >= this.getMaximumSelectionSize() ? this.updateResults(!0) : this.nextSearchTerm != b && (this.search.val(this.nextSearchTerm), this.updateResults(), this.search.select()), this.positionDropdown()) : (this.close(), this.search.width(10)), this.triggerChange({added: t}), e && e.noFocus || this.focusSearch())
            }, cancel: function () {
                this.close(), this.focusSearch()
            }, addSelectedChoice: function (t) {
                var e, i, n = !t.locked,
                    r = w("<li class='select2-search-choice'>    <div></div>    <a href='#' class='select2-search-choice-close' tabindex='-1'></a></li>"),
                    o = w("<li class='select2-search-choice select2-locked'><div></div></li>"), s = n ? r : o,
                    a = this.id(t), l = this.getVal();
                (e = this.opts.formatSelection(t, s.find("div"), this.opts.escapeMarkup)) != b && s.find("div").replaceWith("<div>" + e + "</div>"), (i = this.opts.formatSelectionCssClass(t, s.find("div"))) != b && s.addClass(i), n && s.find(".select2-search-choice-close").on("mousedown", g).on("click dblclick", this.bind(function (t) {
                    this.isInterfaceEnabled() && (this.unselect(w(t.target)), this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus"), g(t), this.close(), this.focusSearch())
                })).on("focus", this.bind(function () {
                    this.isInterfaceEnabled() && (this.container.addClass("select2-container-active"), this.dropdown.addClass("select2-drop-active"))
                })), s.data("select2-data", t), s.insertBefore(this.searchContainer), l.push(a), this.setVal(l)
            }, unselect: function (t) {
                var e, i, n = this.getVal();
                if (0 === (t = t.closest(".select2-search-choice")).length) throw"Invalid argument: " + t + ". Must be .select2-search-choice";
                if (e = t.data("select2-data")) {
                    var r = w.Event("select2-removing");
                    if (r.val = this.id(e), r.choice = e, this.opts.element.trigger(r), r.isDefaultPrevented()) return !1;
                    for (; 0 <= (i = h(this.id(e), n));) n.splice(i, 1), this.setVal(n), this.select && this.postprocessResults();
                    return t.remove(), this.opts.element.trigger({
                        type: "select2-removed",
                        val: this.id(e),
                        choice: e
                    }), this.triggerChange({removed: e}), !0
                }
            }, postprocessResults: function (t, e, i) {
                var n = this.getVal(), r = this.results.find(".select2-result"),
                    o = this.results.find(".select2-result-with-children"), s = this;
                r.each2(function (t, e) {
                    0 <= h(s.id(e.data("select2-data")), n) && (e.addClass("select2-selected"), e.find(".select2-result-selectable").addClass("select2-selected"))
                }), o.each2(function (t, e) {
                    e.is(".select2-result-selectable") || 0 !== e.find(".select2-result-selectable:not(.select2-selected)").length || e.addClass("select2-selected")
                }), -1 == this.highlight() && !1 !== i && s.highlight(0), !this.opts.createSearchChoice && 0 < !r.filter(".select2-result:not(.select2-selected)").length && (!t || t && !t.more && 0 === this.results.find(".select2-no-results").length) && S(s.opts.formatNoMatches, "formatNoMatches") && this.results.append("<li class='select2-no-results'>" + T(s.opts.formatNoMatches, s.opts.element, s.search.val()) + "</li>")
            }, getMaxSearchWidth: function () {
                return this.selection.width() - o(this.search)
            }, resizeSearch: function () {
                var t, e, i, n, r = o(this.search);
                t = v(this.search) + 10, e = this.search.offset().left, (n = (i = this.selection.width()) - (e - this.selection.offset().left) - r) < t && (n = i - r), n < 40 && (n = i - r), n <= 0 && (n = t), this.search.width(Math.floor(n))
            }, getVal: function () {
                var t;
                return this.select ? null === (t = this.select.val()) ? [] : t : r(t = this.opts.element.val(), this.opts.separator)
            }, setVal: function (t) {
                var e;
                this.select ? this.select.val(t) : (e = [], w(t).each(function () {
                    h(this, e) < 0 && e.push(this)
                }), this.opts.element.val(0 === e.length ? "" : e.join(this.opts.separator)))
            }, buildChangeDetails: function (t, e) {
                e = e.slice(0), t = t.slice(0);
                for (var i = 0; i < e.length; i++) for (var n = 0; n < t.length; n++) p(this.opts.id(e[i]), this.opts.id(t[n])) && (e.splice(i, 1), 0 < i && i--, t.splice(n, 1), n--);
                return {added: e, removed: t}
            }, val: function (t, i) {
                var n, r = this;
                if (0 === arguments.length) return this.getVal();
                if ((n = this.data()).length || (n = []), !t && 0 !== t) return this.opts.element.val(""), this.updateSelection([]), this.clearSearch(), void (i && this.triggerChange({
                    added: this.data(),
                    removed: n
                }));
                if (this.setVal(t), this.select) this.opts.initSelection(this.select, this.bind(this.updateSelection)), i && this.triggerChange(this.buildChangeDetails(n, this.data())); else {
                    if (this.opts.initSelection === b) throw new Error("val() cannot be called if initSelection() is not defined");
                    this.opts.initSelection(this.opts.element, function (t) {
                        var e = w.map(t, r.id);
                        r.setVal(e), r.updateSelection(t), r.clearSearch(), i && r.triggerChange(r.buildChangeDetails(n, r.data()))
                    })
                }
                this.clearSearch()
            }, onSortStart: function () {
                if (this.select) throw new Error("Sorting of elements is not supported when attached to <select>. Attach to <input type='hidden'/> instead.");
                this.search.width(0), this.searchContainer.hide()
            }, onSortEnd: function () {
                var t = [], e = this;
                this.searchContainer.show(), this.searchContainer.appendTo(this.searchContainer.parent()), this.resizeSearch(), this.selection.find(".select2-search-choice").each(function () {
                    t.push(e.opts.id(w(this).data("select2-data")))
                }), this.setVal(t), this.triggerChange()
            }, data: function (t, e) {
                var i, n, r = this;
                return 0 === arguments.length ? this.selection.children(".select2-search-choice").map(function () {
                    return w(this).data("select2-data")
                }).get() : (n = this.data(), t || (t = []), i = w.map(t, function (t) {
                    return r.opts.id(t)
                }), this.setVal(i), this.updateSelection(t), this.clearSearch(), void (e && this.triggerChange(this.buildChangeDetails(n, this.data()))))
            }
        }), w.fn.select2 = function () {
            var t, e, i, n, r, o = Array.prototype.slice.call(arguments, 0),
                s = ["val", "destroy", "opened", "open", "close", "focus", "isFocused", "container", "dropdown", "onSortStart", "onSortEnd", "enable", "disable", "readonly", "positionDropdown", "data", "search"],
                a = ["opened", "isFocused", "container", "dropdown"], l = ["val", "data"],
                c = {search: "externalSearch"};
            return this.each(function () {
                if (0 === o.length || "object" == typeof o[0]) (t = 0 === o.length ? {} : w.extend({}, o[0])).element = w(this), "select" === t.element.get(0).tagName.toLowerCase() ? r = t.element.prop("multiple") : (r = t.multiple || !1, "tags" in t && (t.multiple = r = !0)), (e = r ? new window.Select2["class"].multi : new window.Select2["class"].single).init(t); else {
                    if ("string" != typeof o[0]) throw"Invalid arguments to select2 plugin: " + o;
                    if (h(o[0], s) < 0) throw"Unknown method: " + o[0];
                    if (n = b, (e = w(this).data("select2")) === b) return;
                    if ("container" === (i = o[0]) ? n = e.container : "dropdown" === i ? n = e.dropdown : (c[i] && (i = c[i]), n = e[i].apply(e, o.slice(1))), 0 <= h(o[0], a) || 0 <= h(o[0], l) && 1 == o.length) return !1
                }
            }), n === b ? this : n
        }, w.fn.select2.defaults = {
            width: "copy",
            loadMorePadding: 0,
            closeOnSelect: !0,
            openOnEnter: !0,
            containerCss: {},
            dropdownCss: {},
            containerCssClass: "",
            dropdownCssClass: "",
            formatResult: function (t, e, i, n) {
                var r = [];
                return x(t.text, i.term, r, n), r.join("")
            },
            formatSelection: function (t, e, i) {
                return t ? i(t.text) : b
            },
            sortResults: function (t) {
                return t
            },
            formatResultCssClass: function (t) {
                return t.css
            },
            formatSelectionCssClass: function () {
                return b
            },
            formatMatches: function (t) {
                return 1 === t ? "One result is available, press enter to select it." : t + " results are available, use up and down arrow keys to navigate."
            },
            formatNoMatches: function () {
                return "No matches found"
            },
            formatInputTooShort: function (t, e) {
                var i = e - t.length;
                return "Please enter " + i + " or more character" + (1 == i ? "" : "s")
            },
            formatInputTooLong: function (t, e) {
                var i = t.length - e;
                return "Please delete " + i + " character" + (1 == i ? "" : "s")
            },
            formatSelectionTooBig: function (t) {
                return "You can only select " + t + " item" + (1 == t ? "" : "s")
            },
            formatLoadMore: function () {
                return "Loading more results\u2026"
            },
            formatSearching: function () {
                return "Searching\u2026"
            },
            minimumResultsForSearch: 0,
            minimumInputLength: 0,
            maximumInputLength: null,
            maximumSelectionSize: 0,
            id: function (t) {
                return t == b ? null : t.id
            },
            matcher: function (t, e) {
                return 0 <= a("" + e).toUpperCase().indexOf(a("" + t).toUpperCase())
            },
            separator: ",",
            tokenSeparators: [],
            tokenizer: i,
            escapeMarkup: e,
            blurOnChange: !1,
            selectOnBlur: !1,
            adaptContainerCssClass: function (t) {
                return t
            },
            adaptDropdownCssClass: function () {
                return null
            },
            nextSearchTerm: function () {
                return b
            },
            searchInputPlaceholder: "",
            createSearchChoicePosition: "top",
            shouldFocusInput: function (t) {
                return !("ontouchstart" in window || 0 < navigator.msMaxTouchPoints) || !(t.opts.minimumResultsForSearch < 0)
            }
        }, w.fn.select2.ajaxDefaults = {
            transport: w.ajax,
            params: {type: "GET", cache: !1, dataType: "json"}
        }, window.Select2 = {
            query: {ajax: C, local: _, tags: k},
            util: {debounce: n, markMatch: x, escapeMarkup: e, stripDiacritics: a},
            "class": {"abstract": E, single: P, multi: L}
        }
    }
    var B
}(jQuery), function (t) {
    "use strict";
    t.extend(t.fn.select2.defaults, {
        formatNoMatches: function () {
            return "\u6ca1\u6709\u627e\u5230\u5339\u914d\u9879"
        }, formatInputTooShort: function (t, e) {
            return "\u8bf7\u518d\u8f93\u5165" + (e - t.length) + "\u4e2a\u5b57\u7b26"
        }, formatInputTooLong: function (t, e) {
            return "\u8bf7\u5220\u6389" + (t.length - e) + "\u4e2a\u5b57\u7b26"
        }, formatSelectionTooBig: function (t) {
            return "\u4f60\u53ea\u80fd\u9009\u62e9\u6700\u591a" + t + "\u9879"
        }, formatLoadMore: function () {
            return "\u52a0\u8f7d\u7ed3\u679c\u4e2d\u2026"
        }, formatSearching: function () {
            return "\u641c\u7d22\u4e2d\u2026"
        }
    })
}(jQuery), $.expr[":"].containsStrcmpi = $.expr.createPseudo(function (e) {
    return function (t) {
        return 0 <= $(t).text().toUpperCase().indexOf(e.toUpperCase())
    }
}), String.prototype.replaceAll = function (t, e, i) {
    return RegExp.prototype.isPrototypeOf(t) ? this.replace(t, e) : this.replace(new RegExp(t, i ? "gi" : "g"), e)
}, Array.prototype.myFilter = function (t, e) {
    var i = this.length;
    if ("function" != typeof t) throw new TypeError;
    for (var n = new Array, r = e, o = 0; o < i; o++) if (o in this) {
        var s = this[o];
        t.call(r, s, o, this) && n.push(s)
    }
    return n
}, Array.prototype.myFind = function (t) {
    return t && (this.myFilter(t) || [])[0]
}, Array.prototype.remove = function (t) {
    var e = this.indexOf(t);
    -1 < e && this.splice(e, 1)
}, $(function () {
    $("#selectAll").click(function () {
        $(".check_list input:checkbox[name='ids[]']:not(':disabled')").prop("checked", this.checked)
    }), $(".check_list input:checkbox[name='ids[]']").click(function () {
        this.checked ? $(".check_list input:checkbox[name='ids[]']").length == $(".check_list input:checkbox[name='ids[]']:checked").length && $("#selectAll").prop("checked", !0) : $("#selectAll").prop("checked", !1)
    }), $(document).on("keyup change", ".no_emoji", function () {
        var t = /[\uD83C|\uD83D|\uD83E][\uDC00-\uDFFF][\u200D|\uFE0F]|[\uD83C|\uD83D|\uD83E][\uDC00-\uDFFF]|[0-9|*|#]\uFE0F\u20E3|[0-9|#]\u20E3|[\u203C-\u3299]\uFE0F\u200D|[\u203C-\u3299]\uFE0F|[\u2122-\u2B55]|\u303D|[\A9|\AE]\u3030|\uA9|\uAE|\u3030/gi,
            e = $(this).val();
        t.test(e) && $(this).val(e.replace(t, ""))
    }), $(document).on("keyup change", ".param_string", function () {
        if ($(this).attr("include_sym") != undefined) var t = "[\\u4E00-\\u9FFFa-zA-Z0-9" + $(this).attr("include_sym") + "]"; else t = "[\\u4E00-\\u9FFFa-zA-Z0-9]";
        var e = new RegExp(t, "g"), i = $(this).val().match(e);
        null == i && (i = []), i = i.join(""), $(this).val(i)
    }), $(document).on("keyup", ".param_float", function () {
        $(this).val($(this).val().replace(/[^\.|0-9]/g, "")), $(this).val($(this).val().match(/[0-9]*(\.){0,1}([0-9]{0,2})/)[0]);
        var t = $(this).val();
        parseFloat($(this).attr("max")) < parseFloat(t) && (t = parseFloat($(this).attr("max"))), $(this).val(t)
    }), $(document).on("keyup", ".param_integer", function () {
        $(this).val($(this).val().replace(/[^0-9]/g, ""));
        var t = $(this).val();
        parseFloat($(this).attr("max")) < parseFloat(t) && (t = parseFloat($(this).attr("max"))), $(this).val(t)
    }), $(document).on("change", ".upload_file", function () {
        var t = $(this).val();
        if ("" == t || null == t) return !1;
        var e = t.lastIndexOf("."), i = t.substring(e, t.length).toUpperCase();
        if ($(this).is(".no_image")) {
            var n = 10485760,
                r = "\u4e0a\u4f20\u6587\u4ef6\u5927\u4e8e10M\u7684\u9650\u5236\uff1b\u8bf7\u91cd\u65b0\u4e0a\u4f20";
            if (".BMP" == i || ".PNG" == i || ".GIF" == i || ".JPG" == i || ".JPEG" == i) return alert("\u672c\u6b21\u4e0a\u4f20\u4ec5\u9650\u4e8e\u975e\u56fe\u7247\u6587\u6863\uff1b\u8bf7\u91cd\u65b0\u4e0a\u4f20"), reset_input($(this)), !1
        }
        if ($(this).is(".xml")) {
            n = 10485760, r = "\u4e0a\u4f20\u6587\u4ef6\u5927\u4e8e10M\u7684\u9650\u5236\uff1b\u8bf7\u91cd\u65b0\u4e0a\u4f20";
            if (".XML" != i) return alert("\u672c\u6b21\u4e0a\u4f20\u4ec5\u9650\u4e8eXML\u6587\u6863\uff1b\u8bf7\u91cd\u65b0\u4e0a\u4f20"), reset_input($(this)), !1
        }
        if ($(this).is(".excel")) {
            n = 10485760, r = "\u4e0a\u4f20\u8868\u683c\u5927\u4e8e10M\u7684\u9650\u5236\uff1b\u8bf7\u91cd\u65b0\u4e0a\u4f20";
            if (".XLS" != i) return alert("\u672c\u6b21\u4e0a\u4f20\u4ec5\u9650\u4e8e*.xls\u6587\u6863\uff1b\u8bf7\u91cd\u65b0\u4e0a\u4f20"), reset_input($(this)), !1
        }
        if ($(this).is(".json")) {
            n = 10485760;
            if (".JSON" != i) return alert("\u672c\u6b21\u4e0a\u4f20\u4ec5\u9650\u4e8e*.json\u6587\u6863\uff1b\u8bf7\u91cd\u65b0\u4e0a\u4f20"), reset_input($(this)), !1
        }
        if ($(this).is(".image")) {
            n = 10485760, r = "\u4e0a\u4f20\u56fe\u7247\u5927\u4e8e10M\u7684\u9650\u5236\uff1b\u8bf7\u91cd\u65b0\u4e0a\u4f20";
            if (".BMP" != i && ".PNG" != i && ".GIF" != i && ".JPG" != i && ".JPEG" != i) return alert("\u672c\u6b21\u4e0a\u4f20\u9650\u4e8ebmp,png,gif,jpeg,jpg\u683c\u5f0f\u56fe\u7247\uff1b\u8bf7\u91cd\u65b0\u4e0a\u4f20"), reset_input($(this)), !1
        }
        if (".JSON" != i && ".BMP" != i && ".PNG" != i && ".GIF" != i && ".JPG" != i && ".JPEG" != i && ".XLS" != i && ".XLSX" != i && ".DOC" != i && ".ZIP" != i && ".DOCX" != i && ".PDF" != i && ".RAR" != i && ".PPT" != i && ".TXT" != i) return alert("\u8bf7\u4e0a\u4f20\u4ee5\u4e0b\u62d3\u5c55\u540d\u6587\u4ef6(jpg,png,xls,xlsx,doc,docx,pdf,jpeg,rar,zip,ppt,txt,gif\uff09"), reset_input($(this)), !1;
        if ($.browser.msie) {
            var o = new Image;
            for (o.src = t; ;) if (0 < o.fileSize) {
                if (o.fileSize > n) return alert(r), reset_input($(this)), !1;
                break
            }
        } else if (n < this.files[0].size) return alert(r), reset_input($(this)), !1;
        return !0
    }), $(document).on("keyup", ".check_price", function () {
        $(this).val($(this).val().replace(/[^\.|0-9]/g, "")), $(this).val($(this).val().match(/[0-9]*(\.){0,1}([0-9]{0,2})/)[0]);
        var t = $(this).val();
        parseFloat($(this).attr("max")) < parseFloat(t) && (t = parseFloat($(this).attr("max"))), $(this).val(t)
    }), $.isBlank = function (t) {
        return !t || "" === $.trim(t)
    }, $(".select2").select2({
        width: "resolve",
        dropdownAutoWidth: "true"
    }), $(document).on("click", ".art_tip", function () {
        if (isEmpty($(this).attr("title"))) var t = "\u63d0\u793a"; else t = $(this).attr("title");
        if (isEmpty($(this).attr("align"))) var e = "bottom"; else e = $(this).attr("align");
        dialog({title: t, align: e, quickClose: !0, content: $(this).next().clone()}).show(this)
    }), $(document).on("change", "select.multi-level", function () {
        $selector = $(this), $("#" + $selector.attr("aim_id")).val($selector.val()), $.isBlank($selector.attr("aim_id")) && ($selector.siblings("input")[0].value = $selector.val()), $.isBlank($selector.attr("text_id")) || $("#" + $selector.attr("text_id")).val($selector.find("option:selected").text()), $.ajax({
            type: "get",
            url: "/dynamic_selects?id=" + $selector.val() + "&otype=" + $selector.attr("otype") + "&aim_id=" + $selector.attr("aim_id") + "&other=" + $selector.attr("other") + "&text_id=" + $selector.attr("text_id") + "&prompt=" + $selector.attr("prompt") + "&oclass=" + $selector.attr("class"),
            beforeSend: function () {
            },
            async: !1,
            success: function (t) {
                0 < t.length && ($selector.nextAll("select").remove(), $selector.after(t))
            },
            complete: function () {
            },
            error: function () {
            }
        })
    }), $(".my97_date").click(function () {
        WdatePicker({dateFmt: "yyyy-MM-dd", readOnly: !0})
    }), $(".my97_full_time").click(function () {
        WdatePicker({dateFmt: "yyyy-MM-dd HH:mm:ss", readOnly: !0})
    }), $(".my97_start_time").click(function () {
        WdatePicker({dateFmt: "yyyy-MM-dd 00:00:00", readOnly: !0})
    }), $(".my97_end_time").click(function () {
        WdatePicker({dateFmt: "yyyy-MM-dd 23:59:59", readOnly: !0})
    }), $(".jump_check").click(function () {
        var t = $(".jump_num").val().trim();
        if ("" == t || t < 1) alert("\u8bf7\u8f93\u5165\u6709\u6548\u6570\u5b57\uff01"); else {
            var e = window.location.search;
            if ("" == e) self.location = "?page=" + t; else {
                var i = (e = (e = e.split("?"))[1]).split("&");
                page_in_there = !1;
                for (var n = 0; n < i.length; n++) i_get = i[n].split("="), "page" == i_get[0] && (i_get[1] = t, page_in_there = !0), i[n] = i_get.join("=");
                url_new = "?" + i.join("&"), page_in_there ? self.location = url_new : self.location = url_new + "&page=" + t
            }
        }
    })
}), Date.prototype.Format = function (t) {
    var e = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        S: this.getMilliseconds()
    };
    for (var i in/(y+)/.test(t) && (t = t.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length))), e) new RegExp("(" + i + ")").test(t) && (t = t.replace(RegExp.$1, 1 == RegExp.$1.length ? e[i] : ("00" + e[i]).substr(("" + e[i]).length)));
    return t
}, jQuery.fn.wait = function (t, e, i) {
    var n, r = e || -1, o = i || 20, s = this, a = this.selector;
    return this.length ? t && t.call(this) : n = setInterval(function () {
        r || clearInterval(n), r <= 0 || r--, (s = $(a)).length && (t && t.call(s), clearInterval(n))
    }, o), this
};
var getSuningPrice = function (n, r) {
    var t = n.toString(),
        e = "http://pas.suning.com/nspcsale_0_" + pad(t, "18") + "_" + pad(t, "18") + "_0000000000_100_513_5130201_20268_1000177_9177_11406_Z001___R0503002_8.7_0_.html";
    $.ajax({
        url: e, dataType: "jsonp", jsonp: "callback", jsonpCallback: "pcData", success: function (t) {
            if (!t && "1" != t.code) return !1;
            var e = t.data.price.saleInfo[0].promotionPrice, i = parseFloat(e, 10);
            t = {sku: n, price: i};
            "function" == typeof r && r(t)
        }, error: function () {
            r(null)
        }
    })
};
window.console = window.console || function () {
    var t = {};
    return t.log = t.warn = t.debug = t.info = t.error = t.time = t.dir = t.profile = t.clear = t.exception = t.trace = t.assert = function () {
    }, t
}(), $(function () {
    function e() {
        t <= i + 1 ? i = 0 : i += 1, r.find("img").hide().eq(i).fadeIn(500), o.find("a").removeClass("on").eq(i).addClass("on")
    }

    $(".proCitySelAll").click(function (t) {
        if ($(this).addClass("address-open"), $(".sanjiao").addClass("xs"), !$(".provinceCityAll").is(":hidden")) return !1;
        null == $("body").data("CitysAll") && sendAllCitiesAjax(), $(this).select(), $(".provinceCity").hide(), $(".provinceCityAll").hide(), $("#dimCityQuery").hide();
        var e = $(this).offset();
        e.left, e.top, $(this).height();
        $(".provinceCityAll").toggle(), $(".provinceCityAll").click(function (t) {
            t.stopPropagation()
        }), t.stopPropagation(), $("html").click(function () {
            $(".provinceCityAll").removeClass("address-open"), $(".sanjiao").removeClass("xs"), $(".provinceCityAll").hide()
        }), $("input.proCitySelAll").removeClass("current2"), $(this).addClass("current2"), $(".provinceCityAll").find(".tabs").find("a").removeClass("current"), $(".provinceCityAll").find(".tabs").find("a[tb=provinceAll]").addClass("current"), $(".provinceCityAll").find(".con").children().hide(), $(".provinceCityAll").find(".con").find(".provinceAll").show(), null == $("body").data("allProvinces") && sendAllProvinceAjax(), null == $("body").data("allCountys") && sendAllCountiesAjax(), currentCounties(), $(".provinceCityAll").find(".tabs").find("a").click(function () {
            if ("provinceAll" != $(this).attr("tb") && !("cityAll" == $(this).attr("tb") && null == $(".provinceAll .city-list .current").val() || "countyAll" == $(this).attr("tb") && null == $(".cityAll .city-list .current").val() && null == $(".hotCityAll .city-list .current").val())) {
                $(".provinceCityAll").find(".tabs").find("a").removeClass("current"), $(this).addClass("current");
                var t = $(this).attr("tb");
                $(".provinceCityAll").find(".con").children().hide(), $(".provinceCityAll").find(".con").find("." + t).show()
            }
        })
    }), $(".proCitySelAll_Img").click(function (t) {
        t.stopPropagation(), $(this).prev().trigger("click")
    });
    var t = $("div.flashPic img").size(), i = 0, n = [], r = $("div.flashPic"), o = $("div.picNum");
    r.find("img").hide().eq(0).show();
    for (var l = 0; l < t; l++) n[l] = "<a href='javascript:'>" + (l + 1) + "</a>";
    o.html(n.join("")), o.find("a:eq(0)").addClass("on");
    var c, h, d, u, p, f, s = setInterval(e, 4e3);
    o.find("a").click(function () {
        clearInterval(s);
        var t = $(this).index();
        o.find("a").removeClass("on").siblings().eq(t).addClass("on"), r.find("img").hide(), r.find("img").eq(t).fadeIn(500), i = t, s = setInterval(e, 4e3)
    });
    var a = "<div id='dimCityQuery'><ul></ul></div>";
    $("body").append(a), $("body").delegate(".proCityQuery,.proCityQueryAll", $.browser.opera ? "keypress" : "keyup", function (t) {
        switch (0 == $("#dimCityQuery:visible").size() && $(".backifname").hide(), $(".provinceCity").hide(), $(".provinceCityAll").hide(), $(this).hasClass("proCityQueryAll") && (null == $("body").data("allProvinces") && sendAllProvinceAjax(), null == $("body").data("CitysAll") && sendAllCitiesAjax(), null == $("body").data("allCountys") && sendAllCountiesAjax(), h = "proCityQueryAll", c = $("body").find(".proCityQueryAll").index(this), d = $("body").data("CitysAll"), u = $("body").data("allProvinces"), p = $("body").data("allCountys"), f = $(this)), $(this).hasClass("proCityQuery") && (null == $("body").data("allExistProvinces") && sendProvinceAjax(), null == $("body").data("allExistCitys") && sendCitiesAjax(), null == $("body").data("allExistCountys") && sendCountiesAjax(), h = "proCityQuery", c = $("body").find(".proCityQuery").index(this), d = $("body").data("allExistCitys"), u = $("body").data("allExistProvinces"), p = $("body").data("allExistCountys"), f = $(this)), lastKeyPressCode = t.keyCode, lastKeyPressCode) {
            case 40:
                return $("#dimCityQuery").trigger("selNext"), !1;
            case 38:
                return $("#dimCityQuery").trigger("selPrev"), !1;
            case 13:
                return $("#dimCityQuery").trigger("enter"), !1
        }
        if (v = $.trim($(this).val()), "" == v || null == v) return !1;
        $(".provinceCity").hide();
        var e, i = $(this).offset(), n = i.left, r = i.top, o = ($(this).width(), $(this).height()), s = [];
        for (l = 0; l < p.length; l++) if (v.toUpperCase() !== p[l].pinYinChar.substring(0, v.length)) if (v !== p[l].areaName.substring(0, v.length)) if (v.toLowerCase() !== p[l].pinYin.substring(0, v.length)) ; else {
            if (s[s.length] = "<li><a class='allcityClass' href='javascript:' provinceId=" + p[l].provinceId + " cityId=" + p[l].cityId + " countyId=" + p[l].id + ">" + p[l].cityName + p[l].areaName + " (<span style='color:red'>" + v.toLowerCase() + "</span>" + p[l].pinYin.substring(v.length, p[l].pinYin.length) + ")</a></li>", 9 < s.length) break;
            e = e < (p[l].cityName + p[l].areaName + p[l].pinYin).length ? (p[l].cityName + p[l].areaName + p[l].pinYin).length : e
        } else {
            if (s[s.length] = "<li><a class='allcityClass' href='javascript:' provinceId=" + p[l].provinceId + " cityId=" + p[l].cityId + " countyId=" + p[l].id + ">" + p[l].cityName + "<span style='color:red'>" + v + "</span>" + p[l].areaName.substring(v.length, p[l].areaName.length) + " (" + p[l].pinYinChar + ")</a></li>", 9 < s.length) break;
            e = e < (p[l].cityName + p[l].areaName + p[l].pinYinChar).length ? (p[l].cityName + p[l].areaName + p[l].pinYinChar).length : e
        } else {
            if (s[s.length] = "<li><a class='allcityClass' href='javascript:' provinceId=" + p[l].provinceId + " cityId=" + p[l].cityId + " countyId=" + p[l].id + ">" + p[l].cityName + p[l].areaName + " (<span style='color:red'>" + v.toUpperCase() + "</span>" + p[l].pinYinChar.substring(v.length, p[l].pinYinChar.length) + ")</a></li>", 9 < s.length) break;
            e = e < (p[l].cityName + p[l].areaName + p[l].pinYinChar).length ? (p[l].cityName + p[l].areaName + p[l].pinYinChar).length : e
        }
        for (l = 0; l < d.cities.length; l++) if (v.toUpperCase() !== d.cities[l].cityShortPY.substring(0, v.length)) if (v !== d.cities[l].name.substring(0, v.length)) if (v.toLowerCase() !== d.cities[l].cityPinyin.substring(0, v.length)) ; else {
            if (s[s.length] = "<li><a class='allcityClass' href='javascript:' provinceId=" + d.cities[l].provinceId + " cityId=" + d.cities[l].id + ">" + d.cities[l].name + " (<span style='color:red'>" + v.toLowerCase() + "</span>" + d.cities[l].cityPinyin.substring(v.length, d.cities[l].cityPinyin.length) + ")</a></li>", 9 < s.length) break;
            e = e < (d.cities[l].name + d.cities[l].cityPinyin).length ? (d.cities[l].name + d.cities[l].cityPinyin).length : e
        } else {
            if (s[s.length] = "<li><a class='allcityClass' href='javascript:' provinceId=" + d.cities[l].provinceId + " cityId=" + d.cities[l].id + "><span style='color:red'>" + v + "</span>" + d.cities[l].name.substring(v.length, d.cities[l].name.length) + " (" + d.cities[l].cityShortPY + ")</a></li>", 9 < s.length) break;
            e = e < (d.cities[l].name + d.cities[l].cityShortPY).length ? (d.cities[l].name + d.cities[l].cityShortPY).length : e
        } else {
            if (s[s.length] = "<li><a class='allcityClass' href='javascript:' provinceId=" + d.cities[l].provinceId + " cityId=" + d.cities[l].id + ">" + d.cities[l].name + " (<span style='color:red'>" + v.toUpperCase() + "</span>" + d.cities[l].cityShortPY.substring(v.length, d.cities[l].cityShortPY.length) + ")</a></li>", 9 < s.length) break;
            e = e < (d.cities[l].name + d.cities[l].cityShortPY).length ? (d.cities[l].name + d.cities[l].cityShortPY).length : e
        }
        if ("" == s || null == s) return $("#dimCityQuery ul").html("<li class='none'>\u5bf9\u4e0d\u8d77,\u6ca1\u6709\u627e\u5230\u8be5\u57ce\u5e02</li>"), !1;
        $("#dimCityQuery ul").html(s.join("")).find("li:first").addClass("current"), e < 200 && (e = 200), $("#dimCityQuery").css("width", e).css("top", r + o - 1).css("left", n).show(), $(".backifname").show(), $("html").click(function () {
            $("#dimCityQuery").hide(), $(".backifname").hide()
        })
    }), $("body").delegate("#dimCityQuery li", "hover", function () {
        $(this).addClass("current").siblings().removeClass("current")
    }, function () {
        $(this).removeClass("current")
    }), $("#dimCityQuery").delegate("", "selNext", function () {
        var t = $(this).find("li.current").next();
        0 < t.size() ? t.addClass("current").siblings().removeClass("current") : $("#dimCityQuery li").removeClass("current").first().addClass("current")
    }), $("#dimCityQuery").delegate("", "selPrev", function () {
        var t = $(this).find("li.current").prev();
        0 < t.size() ? t.addClass("current").siblings().removeClass("current") : $("#dimCityQuery li").removeClass("current").last().addClass("current")
    }), $("#dimCityQuery").delegate("", "enter", function () {
        var t = $(this).find("li.current");
        0 < t.size() && t.find("a").trigger("click")
    }), $("body").delegate("#dimCityQuery li a.allcityClass", "click", function () {
        var t, i, e, n = $(this).text(), r = $(this).attr("provinceId"), o = $(this).attr("cityId"),
            s = $(this).attr("countyId");
        for (l = 0; l < u.length; l++) u[l].id == r && (t = u[l].provinceName);
        for (l = 0; l < d.cities.length; l++) d.cities[l].id == o && (i = d.cities[l].name);
        if ("proCityQueryAll" == h && ($("body").data("pAllId", r), $("body").data("cAllId", o), $("body").data("aAllId", s), $("body").data("pAllName", t), $("body").data("nameOfCityAll", i)), "proCityQuery" == h && ($("body").data("pId", r), $("body").data("cId", o), $("body").data("aId", s), $("body").data("pName", t), $("body").data("nameOfCity", i)), n = n.split("("), countyName = $.trim(n[0]), null == s || countyName == i) {
            if ("proCityQuery" == h) {
                f.trigger("click"), counties = [];
                var a = 0;
                $.each(p, function (t, e) {
                    e.cityId == o && (counties[a++] = e)
                }), countyTotalPage = Math.ceil(counties.length / p_pageSize), $(".provinceCity").find(".tabs").find("a").removeClass("current"), $(".provinceCity .tabs").find("#county").addClass("current"), $(".con .city .city-list a").removeClass("current"), $(".provinceCity").find(".con").children().hide(), $(".provinceCity").find(".con").find(".county").show(), $(".con .provinceAll .city-list a").removeClass("current"), countyPage(1)
            } else if ("proCityQueryAll" == h) {
                f.trigger("click"), countiesAll = [];
                a = 0;
                $.each(p, function (t, e) {
                    e.cityId == o && e.areaName != i && (countiesAll[a++] = e)
                }), countyTotalPageAll = Math.ceil(countiesAll.length / p_pageSize), $(".provinceCityAll").find(".tabs").find("a").removeClass("current"), $(".provinceCityAll .tabs").find("#countyAll").addClass("current"), $(".con .cityAll .city-list a").removeClass("current"), $(".provinceCityAll").find(".con").children().hide(), $(".provinceCityAll").find(".con").find(".countyAll").show(), $(".con .provinceAll .city-list a").removeClass("current"), allCountyPage(1)
            }
        } else e = t + countyName, "proCityQueryAll" == h && ($("body").find(".proCityQueryAll").eq(c).val(e), $("body").find(".proCityQueryAll").eq(c).trigger("change"), $(".provinceCityAll").find(".tabs").find("a").removeClass("current"), $(".provinceCityAll").find(".tabs").find("a[tb=hotCityAll]").addClass("current"), $(".provinceCityAll .con .city-list a").removeClass("current"), $(".provinceCityAll .con .city-list a input").removeClass("current")), "proCityQuery" == h && ($("body").find(".proCityQuery").eq(c).val(e), $("body").find(".proCityQuery").eq(c).trigger("change", [o, s]), $(".provinceCity").find(".tabs").find("a").removeClass("current"), $(".provinceCity").find(".tabs").find("a[tb=hotCity]").addClass("current"), $(".provinceCity .con .city-list a").removeClass("current"), $(".provinceCity .con .city-list a input").removeClass("current"));
        return $("#dimCityQuery").hide(), $(".backifname").hide(), !1
    }), $(".nomarl").on("focus", function () {
        var t = $.trim($(this).attr("ov")), e = $.trim($(this).val());
        $(this).css({color: "#000"}), e == t && $(this).val("")
    }), $(".nomarl").on("blur", function () {
        var t = $.trim($(this).attr("ov")), e = $.trim($(this).val());
        "" != e && e != t || $(this).val(t).css({color: "#aaa"})
    })
}), $(".province .pre a").bind("click", function () {
    var t = parseInt($("#provincePage").html());
    1 != t && viewProvince(t - 1)
}), $(".city .pre a").bind("click", function () {
    var t = parseInt($("#cityPage").html());
    1 != t && cityPage(t - 1)
}), $(".county .pre a").bind("click", function () {
    var t = parseInt($("#countyPage").html());
    1 != t && countyPage(t - 1)
}), $(".province .next a").bind("click", function () {
    var t = parseInt($("#provincePage").html());
    provinceTotalPage = countProvincePages(), t != provinceTotalPage && viewProvince(t + 1)
}), $(".city .next a").bind("click", function () {
    $(this).hasClass("can") && cityPage(parseInt($("#cityPage").html()) + 1)
}), $(".county .next a").bind("click", function () {
    $(this).hasClass("can") && countyPage(parseInt($("#countyPage").html()) + 1)
});
var allProvinces = null, allCities = null, allAreas = null, allProId = null, cityIdAll = null,
    provinceAllTotalPage = null, pa_pageSize = 33, pa_currentPage = 1;
$(".provinceAll .pre a").bind("click", function () {
    var t = parseInt($("#provincePage1").html());
    1 != t && viewAllProvince(t - 1)
}), $(".cityAll .pre a").bind("click", function () {
    var t = parseInt($("#cityPage1").html());
    1 != t && allCityPage(t - 1)
}), $(".countyAll .pre a").bind("click", function () {
    var t = parseInt($("#countyPage1").html());
    1 != t && allCountyPage(t - 1)
}), $(".provinceAll .next a").bind("click", function () {
    var t = parseInt($("#provincePage1").html());
    (provinceAllTotalPage = countAllProvincePages()) <= t || viewAllProvince(t + 1)
}), $(".cityAll .next a").bind("click", function () {
    $(this).hasClass("can") && allCityPage(parseInt($("#cityPage1").html()) + 1)
}), $(".countyAll .next a").bind("click", function () {
    $(this).hasClass("can") && allCountyPage(parseInt($("#countyPage1").html()) + 1)
}), function (u) {
    u.fn.extend({
        yx_rotaion: function (d) {
            var t = {during: 3e3, btn: !0, focus: !0, title: !0, auto: !0};
            d = u.extend(t, d);
            return this.each(function () {
                var t = d, e = 0, n = u(this), r = n.find("li"), o = r.length;
                n.css({
                    position: "relative",
                    overflow: "hidden",
                    width: r.find("img").width(),
                    height: r.find("img").height()
                }), n.find("li").css({
                    position: "relative",
                    left: 0,
                    top: 0
                }).hide(), r.first().show(), n.append('<div class="yx-rotaion-btn"><span class="left_btn"></span><span class="right_btn"></span></div>'), t.btn || u(".yx-rotaion-btn").css({visibility: "hidden"}), t.title && n.append(' <div class="yx-rotation-title"></div><a href="" class="yx-rotation-t"></a>'), t.focus && n.append('<div class="yx-rotation-focus"></div>');
                var s = u(".yx-rotaion-btn span"), a = u(".yx-rotation-t"),
                    l = (u(".yx-rotation-title"), u(".yx-rotation-focus"));
                if (t.auto) var c = setInterval(function () {
                    s.last().click()
                }, t.during);
                for (a.text(r.first().find("img").attr("alt")), a.attr("href", r.first().find("a").attr("href")), i = 1; i <= o; i++) l.append("<span>" + i + "</span>");
                var h = l.children("span");
                h.first().addClass("hover"), s.hover(function () {
                    u(this).addClass("hover")
                }, function () {
                    u(this).removeClass("hover")
                }), s.add(r).add(h).hover(function () {
                    c && clearInterval(c)
                }, function () {
                    t.auto && (c = setInterval(function () {
                        s.last().click()
                    }, t.during))
                }), h.bind("mouseover", function () {
                    var t = u(this).index();
                    u(this).addClass("hover"), l.children("span").not(u(this)).removeClass("hover"), r.eq(t).fadeIn(300), r.not(r.eq(t)).fadeOut(300), a.text(r.eq(t).find("img").attr("alt")), e = t
                }), s.bind("click", function () {
                    1 == u(this).index() ? e++ : e--, o <= e && (e = 0), e < 0 && (e = o - 1), r.eq(e).fadeIn(300), r.not(r.eq(e)).fadeOut(300), h.eq(e).addClass("hover"), h.not(h.eq(e)).removeClass("hover"), a.text(r.eq(e).find("img").attr("alt")), a.attr("href", r.eq(e).find("a").attr("href"))
                })
            })
        }
    })
}(jQuery), function () {
    function n(t) {
        var e = r[t], i = "exports";
        return "object" == typeof e ? e : (e[i] || (e[i] = {}, e[i] = e.call(e[i], n, e[i], e) || e[i]), e[i])
    }

    function t(t, e) {
        r[t] = e
    }

    var r = {};
    t("jquery", function () {
        return jQuery
    }), t("popup", function (t) {
        function o() {
            this.destroyed = !1, this.__popup = I("<div />").css({
                display: "none",
                position: "absolute",
                outline: 0
            }).attr("tabindex", "-1").html(this.innerHTML).appendTo("body"), this.__backdrop = this.__mask = I("<div />").css({
                opacity: .7,
                background: "#000"
            }), this.node = this.__popup[0], this.backdrop = this.__backdrop[0], 0
        }

        var I = t("jquery"), r = !("minWidth" in I("html")[0].style), s = !r;
        return I.extend(o.prototype, {
            node: null,
            backdrop: null,
            fixed: !1,
            destroyed: !0,
            open: !1,
            returnValue: "",
            autofocus: !0,
            align: "bottom left",
            innerHTML: "",
            className: "ui-popup",
            show: function (t) {
                if (this.destroyed) return this;
                var e = this.__popup, i = this.__backdrop;
                if (this.__activeElement = this.__getActive(), this.open = !0, this.follow = t || this.follow, !this.__ready) {
                    if (e.addClass(this.className).attr("role", this.modal ? "alertdialog" : "dialog").css("position", this.fixed ? "fixed" : "absolute"), r || I(window).on("resize", I.proxy(this.reset, this)), this.modal) {
                        var n = {
                            position: "fixed",
                            left: 0,
                            top: 0,
                            width: "100%",
                            height: "100%",
                            overflow: "hidden",
                            userSelect: "none",
                            zIndex: this.zIndex || o.zIndex
                        };
                        e.addClass(this.className + "-modal"), s || I.extend(n, {
                            position: "absolute",
                            width: I(window).width() + "px",
                            height: I(document).height() + "px"
                        }), i.css(n).attr({tabindex: "0"}).on("focus", I.proxy(this.focus, this)), this.__mask = i.clone(!0).attr("style", "").insertAfter(e), i.addClass(this.className + "-backdrop").insertBefore(e), this.__ready = !0
                    }
                    e.html() || e.html(this.innerHTML)
                }
                return e.addClass(this.className + "-show").show(), i.show(), this.reset().focus(), this.__dispatchEvent("show"), this
            },
            showModal: function () {
                return this.modal = !0, this.show.apply(this, arguments)
            },
            close: function (t) {
                return !this.destroyed && this.open && (void 0 !== t && (this.returnValue = t), this.__popup.hide().removeClass(this.className + "-show"), this.__backdrop.hide(), this.open = !1, this.blur(), this.__dispatchEvent("close")), this
            },
            remove: function () {
                if (this.destroyed) return this;
                for (var t in this.__dispatchEvent("beforeremove"), o.current === this && (o.current = null), this.__popup.remove(), this.__backdrop.remove(), this.__mask.remove(), r || I(window).off("resize", this.reset), this.__dispatchEvent("remove"), this) delete this[t];
                return this
            },
            reset: function () {
                var t = this.follow;
                return t ? this.__follow(t) : this.__center(), this.__dispatchEvent("reset"), this
            },
            focus: function () {
                var t = this.node, e = this.__popup, i = o.current, n = this.zIndex = o.zIndex++;
                if (i && i !== this && i.blur(!1), !I.contains(t, this.__getActive())) {
                    var r = e.find("[autofocus]")[0];
                    !this._autofocus && r ? this._autofocus = !0 : r = t, this.__focus(r)
                }
                return e.css("zIndex", n), o.current = this, e.addClass(this.className + "-focus"), this.__dispatchEvent("focus"), this
            },
            blur: function (t) {
                var e = this.__activeElement;
                return !1 !== t && this.__focus(e), this._autofocus = !1, this.__popup.removeClass(this.className + "-focus"), this.__dispatchEvent("blur"), this
            },
            addEventListener: function (t, e) {
                return this.__getEventListener(t).push(e), this
            },
            removeEventListener: function (t, e) {
                for (var i = this.__getEventListener(t), n = 0; n < i.length; n++) e === i[n] && i.splice(n--, 1);
                return this
            },
            __getEventListener: function (t) {
                var e = this.__listener;
                return e || (e = this.__listener = {}), e[t] || (e[t] = []), e[t]
            },
            __dispatchEvent: function (t) {
                var e = this.__getEventListener(t);
                this["on" + t] && this["on" + t]();
                for (var i = 0; i < e.length; i++) e[i].call(this)
            },
            __focus: function (t) {
                try {
                    this.autofocus && !/^iframe$/i.test(t.nodeName) && t.focus()
                } catch (o) {
                }
            },
            __getActive: function () {
                try {
                    var t = document.activeElement, e = t.contentDocument;
                    return e && e.activeElement || t
                } catch (i) {
                }
            },
            __center: function () {
                var t = this.__popup, e = I(window), i = I(document), n = this.fixed, r = n ? 0 : i.scrollLeft(),
                    o = n ? 0 : i.scrollTop(), s = e.width(), a = e.height(), l = (s - t.width()) / 2 + r,
                    c = 382 * (a - t.height()) / 1e3 + o, h = t[0].style;
                h.left = Math.max(parseInt(l), r) + "px", h.top = Math.max(parseInt(c), o) + "px"
            },
            __follow: function (t) {
                var e = t.parentNode && I(t), i = this.__popup;
                if (this.__followSkin && i.removeClass(this.__followSkin), e) {
                    var n = e.offset();
                    if (n.left * n.top < 0) return this.__center()
                }
                var r = this, o = this.fixed, s = I(window), a = I(document), l = s.width(), c = s.height(),
                    h = a.scrollLeft(), d = a.scrollTop(), u = i.width(), p = i.height(), f = e ? e.outerWidth() : 0,
                    g = e ? e.outerHeight() : 0, m = this.__offset(t), v = m.left, y = m.top, x = o ? v - h : v,
                    b = o ? y - d : y, w = o ? 0 : h, C = o ? 0 : d, _ = w + l - u, k = C + c - p, S = {},
                    T = this.align.split(" "), A = this.className + "-",
                    $ = {top: "bottom", bottom: "top", left: "right", right: "left"},
                    M = {top: "top", bottom: "top", left: "left", right: "left"},
                    E = [{top: b - p, bottom: b + g, left: x - u, right: x + f}, {
                        top: b,
                        bottom: b - p + g,
                        left: x,
                        right: x - u + f
                    }], P = {left: x + f / 2 - u / 2, top: b + g / 2 - p / 2}, L = {left: [w, _], top: [C, k]};
                I.each(T, function (t, e) {
                    E[t][e] > L[M[e]][1] && (e = T[t] = $[e]), E[t][e] < L[M[e]][0] && (T[t] = $[e])
                }), T[1] || (M[T[1]] = "left" === M[T[0]] ? "top" : "left", E[1][T[1]] = P[M[T[1]]]), A += T.join("-") + " " + this.className + "-follow", r.__followSkin = A, e && i.addClass(A), S[M[T[0]]] = parseInt(E[0][T[0]]), S[M[T[1]]] = parseInt(E[1][T[1]]), i.css(S)
            },
            __offset: function (t) {
                var e = t.parentNode, i = e ? I(t).offset() : {left: t.pageX, top: t.pageY},
                    n = (t = e ? t : t.target).ownerDocument, r = n.defaultView || n.parentWindow;
                if (r == window) return i;
                var o = r.frameElement, s = I(n), a = s.scrollLeft(), l = s.scrollTop(), c = I(o).offset(), h = c.left,
                    d = c.top;
                return {left: i.left + h - a, top: i.top + d - l}
            }
        }), o.zIndex = 1024, o.current = null, o
    }), t("dialog-config", {
        backdropBackground: "#000",
        backdropOpacity: .7,
        content: '<span class="ui-dialog-loading">Loading..</span>',
        title: "",
        statusbar: "",
        button: null,
        ok: null,
        cancel: null,
        okValue: "ok",
        cancelValue: "cancel",
        cancelDisplay: !0,
        width: "",
        height: "",
        padding: "",
        skin: "",
        quickClose: !1,
        cssUri: "../css/ui-dialog.css",
        innerHTML: '<div i="dialog" class="ui-dialog"><div class="ui-dialog-arrow-a"></div><div class="ui-dialog-arrow-b"></div><table class="ui-dialog-grid"><tr><td i="header" class="ui-dialog-header"><button i="close" class="ui-dialog-close">&#215;</button><div i="title" class="ui-dialog-title"></div></td></tr><tr><td i="body" class="ui-dialog-body"><div i="content" class="ui-dialog-content"></div></td></tr><tr><td i="footer" class="ui-dialog-footer"><div i="statusbar" class="ui-dialog-statusbar"></div><div i="button" class="ui-dialog-button"></div></td></tr></table></div>'
    }), t("dialog", function (t) {
        var a = t("jquery"), l = t("popup"), e = t("dialog-config"), i = e.cssUri;
        if (i) {
            var n = t[t.toUrl ? "toUrl" : "resolve"];
            n && (i = '<link rel="stylesheet" href="' + (i = n(i)) + '" />', a("base")[0] ? a("base").before(i) : a("head").append(i))
        }
        var c = 0, s = new Date - 0, r = !("minWidth" in a("html")[0].style),
            h = "createTouch" in document && !("onmousemove" in document) || /(iPhone|iPad|iPod)/i.test(navigator.userAgent),
            d = !r && !h, u = function (t, e, i) {
                var n = t = t || {};
                ("string" == typeof t || 1 === t.nodeType) && (t = {
                    content: t,
                    fixed: !h
                }), (t = a.extend(!0, {}, u.defaults, t)).original = n;
                var r = t.id = t.id || s + c, o = u.get(r);
                return o ? o.focus() : (d || (t.fixed = !1), t.quickClose && (t.modal = !0, t.backdropOpacity = 0), a.isArray(t.button) || (t.button = []), void 0 !== i && (t.cancel = i), t.cancel && t.button.push({
                    id: "cancel",
                    value: t.cancelValue,
                    callback: t.cancel,
                    display: t.cancelDisplay
                }), void 0 !== e && (t.ok = e), t.ok && t.button.push({
                    id: "ok",
                    value: t.okValue,
                    callback: t.ok,
                    autofocus: !0
                }), u.list[r] = new u.create(t))
            }, o = function () {
            };
        o.prototype = l.prototype;
        var p = u.prototype = new o;
        return u.create = function (t) {
            var s = this;
            a.extend(this, new l);
            var e = (t.original, a(this.node).html(t.innerHTML)), i = a(this.backdrop);
            return this.options = t, this._popup = e, a.each(t, function (t, e) {
                "function" == typeof s[t] ? s[t](e) : s[t] = e
            }), t.zIndex && (l.zIndex = t.zIndex), e.attr({
                "aria-labelledby": this._$("title").attr("id", "title:" + this.id).attr("id"),
                "aria-describedby": this._$("content").attr("id", "content:" + this.id).attr("id")
            }), this._$("close").css("display", !1 === this.cancel ? "none" : "").attr("title", this.cancelValue).on("click", function (t) {
                s._trigger("cancel"), t.preventDefault()
            }), this._$("dialog").addClass(this.skin), this._$("body").css("padding", this.padding), t.quickClose && i.on("onmousedown" in document ? "mousedown" : "click", function () {
                return s._trigger("cancel"), !1
            }), this.addEventListener("show", function () {
                i.css({opacity: 0, background: t.backdropBackground}).animate({opacity: t.backdropOpacity}, 150)
            }), this._esc = function (t) {
                var e = t.target, i = e.nodeName, n = /^input|textarea$/i, r = l.current === s, o = t.keyCode;
                !r || n.test(i) && "button" !== e.type || 27 === o && s._trigger("cancel")
            }, a(document).on("keydown", this._esc), this.addEventListener("remove", function () {
                a(document).off("keydown", this._esc), delete u.list[this.id]
            }), c++, u.oncreate(this), this
        }, u.create.prototype = p, a.extend(p, {
            content: function (t) {
                var e = this._$("content");
                return "object" == typeof t ? (t = a(t), e.empty("").append(t.show()), this.addEventListener("beforeremove", function () {
                    a("body").append(t.hide())
                })) : e.html(t), this.reset()
            }, title: function (t) {
                return this._$("title").text(t), this._$("header")[t ? "show" : "hide"](), this
            }, width: function (t) {
                return this._$("content").css("width", t), this.reset()
            }, height: function (t) {
                return this._$("content").css("height", t), this.reset()
            }, button: function (t) {
                t = t || [];
                var r = this, o = "", s = 0;
                return this.callbacks = {}, "string" == typeof t ? (o = t, s++) : a.each(t, function (t, e) {
                    var i = e.id = e.id || e.value, n = "";
                    r.callbacks[i] = e.callback, !1 === e.display ? n = ' style="display:none"' : s++, o += '<button type="button" i-id="' + i + '"' + n + (e.disabled ? " disabled" : "") + (e.autofocus ? ' autofocus class="ui-dialog-autofocus"' : "") + ">" + e.value + "</button>", r._$("button").on("click", "[i-id=" + i + "]", function (t) {
                        a(this).attr("disabled") || r._trigger(i), t.preventDefault()
                    })
                }), this._$("button").html(o), this._$("footer")[s ? "show" : "hide"](), this
            }, statusbar: function (t) {
                return this._$("statusbar").html(t)[t ? "show" : "hide"](), this
            }, _$: function (t) {
                return this._popup.find("[i=" + t + "]")
            }, _trigger: function (t) {
                var e = this.callbacks[t];
                return "function" != typeof e || !1 !== e.call(this) ? this.close().remove() : this
            }
        }), u.oncreate = a.noop, u.getCurrent = function () {
            return l.current
        }, u.get = function (t) {
            return void 0 === t ? u.list : u.list[t]
        }, u.list = {}, u.defaults = e, u
    }), t("drag", function (t) {
        var r = t("jquery"), v = r(window), y = r(document), e = "createTouch" in document,
            i = document.documentElement, n = !!("minWidth" in i.style) && "onlosecapture" in i, o = "setCapture" in i,
            s = {
                start: e ? "touchstart" : "mousedown",
                over: e ? "touchmove" : "mousemove",
                end: e ? "touchend" : "mouseup"
            }, a = e ? function (t) {
                return t.touches || (t = t.originalEvent.touches.item(0)), t
            } : function (t) {
                return t
            }, l = function () {
                this.start = r.proxy(this.start, this), this.over = r.proxy(this.over, this), this.end = r.proxy(this.end, this), this.onstart = this.onover = this.onend = r.noop
            };
        return l.types = s, l.prototype = {
            start: function (t) {
                return t = this.startFix(t), y.on(s.over, this.over).on(s.end, this.end), this.onstart(t), !1
            }, over: function (t) {
                return t = this.overFix(t), this.onover(t), !1
            }, end: function (t) {
                return t = this.endFix(t), y.off(s.over, this.over).off(s.end, this.end), this.onend(t), !1
            }, startFix: function (t) {
                return t = a(t), this.target = r(t.target), this.selectstart = function () {
                    return !1
                }, y.on("selectstart", this.selectstart).on("dblclick", this.end), n ? this.target.on("losecapture", this.end) : v.on("blur", this.end), o && this.target[0].setCapture(), t
            }, overFix: function (t) {
                return a(t)
            }, endFix: function (t) {
                return t = a(t), y.off("selectstart", this.selectstart).off("dblclick", this.end), n ? this.target.off("losecapture", this.end) : v.off("blur", this.end), o && this.target[0].releaseCapture(), t
            }
        }, l.create = function (c, t) {
            var h, d, u, p, f = r(c), e = new l, i = l.types.start, n = function () {
            }, g = c.className.replace(/^\s|\s.*/g, "") + "-drag-start", m = {
                onstart: n, onover: n, onend: n, off: function () {
                    f.off(i, e.start)
                }
            };
            return e.onstart = function (t) {
                var e = "fixed" === f.css("position"), i = y.scrollLeft(), n = y.scrollTop(), r = f.width(),
                    o = f.height();
                d = h = 0, u = e ? v.width() - r + h : y.width() - r, p = e ? v.height() - o + d : y.height() - o;
                var s = f.offset(), a = this.startLeft = e ? s.left - i : s.left,
                    l = this.startTop = e ? s.top - n : s.top;
                this.clientX = t.clientX, this.clientY = t.clientY, f.addClass(g), m.onstart.call(c, t, a, l)
            }, e.onover = function (t) {
                var e = t.clientX - this.clientX + this.startLeft, i = t.clientY - this.clientY + this.startTop,
                    n = f[0].style;
                e = Math.max(h, Math.min(u, e)), i = Math.max(d, Math.min(p, i)), n.left = e + "px", n.top = i + "px", m.onover.call(c, t, e, i)
            }, e.onend = function (t) {
                var e = f.position(), i = e.left, n = e.top;
                f.removeClass(g), m.onend.call(c, t, i, n)
            }, e.off = function () {
                f.off(i, e.start)
            }, t ? e.start(t) : f.on(i, e.start), m
        }, l
    }), t("dialog-plus", function (t) {
        var c = t("jquery"), o = t("dialog"), h = t("drag");
        return o.oncreate = function (e) {
            var i, n = e.options, t = n.original, r = n.url, o = n.oniframeload;
            if (r && (this.padding = n.padding = 0, (i = c("<iframe />")).attr({
                src: r,
                name: e.id,
                width: "100%",
                height: "100%",
                allowtransparency: "yes",
                frameborder: "no",
                scrolling: "no"
            }).on("load", function () {
                var t;
                try {
                    t = i[0].contentWindow.frameElement
                } catch (h) {
                }
                t && (n.width || e.width(i.contents().width()), n.height || e.height(i.contents().height())), o && o.call(e)
            }), e.addEventListener("beforeremove", function () {
                i.attr("src", "about:blank").remove()
            }, !1), e.content(i[0]), e.iframeNode = i[0]), !(t instanceof Object)) for (var s = function () {
                e.close().remove()
            }, a = 0; a < frames.length; a++) try {
                if (t instanceof frames[a].Object) {
                    c(frames[a]).one("unload", s);
                    break
                }
            } catch (l) {
            }
            c(e.node).on(h.types.start, "[i=title]", function (t) {
                e.follow || (e.focus(), h.create(e.node, t))
            })
        }, o.get = function (t) {
            if (t && t.frameElement) {
                var e, i = t.frameElement, n = o.list;
                for (var r in n) if ((e = n[r]).node.getElementsByTagName("iframe")[0] === i) return e
            } else if (t) return o.list[t]
        }, o
    }), window.dialog = n("dialog-plus")
}(), function (h) {
    h.fn.jqueryzoom = function (t) {
        var c = {xzoom: 200, yzoom: 200, offset: 10, position: "right", lens: 1, preload: 1};
        t && h.extend(c, t);
        var e = "";
        h(this).hover(function () {
            var o = h(this).offset().left, s = h(this).offset().top, a = h(this).children("img").get(0).offsetWidth,
                l = h(this).children("img").get(0).offsetHeight;
            e = h(this).children("img").attr("alt");
            var t = h(this).children("img").attr("jqimg");
            h(this).children("img").attr("alt", ""), 0 == h("div.zoomdiv").get().length && (h(this).after("<div class='zoomdiv'><img class='bigimg' src='" + t + "'/></div>"), h(this).append("<div class='jqZoomPup'>&nbsp;</div>")), "right" == c.position ? o + a + c.offset + c.xzoom > screen.width ? leftpos = o - c.offset - c.xzoom : leftpos = o + a + c.offset : (leftpos = o - c.xzoom - c.offset, leftpos < 0 && (leftpos = o + a + c.offset)), h("div.zoomdiv").css({
                top: s - s,
                left: leftpos - 148
            }), h("div.zoomdiv").width(c.xzoom), h("div.zoomdiv").height(c.yzoom), h("div.zoomdiv").show(), c.lens || h(this).css("cursor", "crosshair"), h(document.body).mousemove(function (t) {
                mouse = new MouseEvent(t);
                var e = h(".bigimg").get(0).offsetWidth, i = h(".bigimg").get(0).offsetHeight, n = "x", r = "y";
                if (isNaN(r) | isNaN(n)) {
                    r = e / a, n = i / l;
                    h("div.jqZoomPup").width(c.xzoom / r), h("div.jqZoomPup").height(c.yzoom / n), c.lens && h("div.jqZoomPup").css("visibility", "visible")
                }
                xpos = mouse.x - h("div.jqZoomPup").width() / 2 - o, ypos = mouse.y - h("div.jqZoomPup").height() / 2 - s, c.lens && (xpos = mouse.x - h("div.jqZoomPup").width() / 2 < o ? 0 : mouse.x + h("div.jqZoomPup").width() / 2 > a + o ? a - h("div.jqZoomPup").width() - 2 : xpos, ypos = mouse.y - h("div.jqZoomPup").height() / 2 < s ? 0 : mouse.y + h("div.jqZoomPup").height() / 2 > l + s ? l - h("div.jqZoomPup").height() - 2 : ypos), c.lens && h("div.jqZoomPup").css({
                    top: ypos,
                    left: xpos
                }), scrolly = ypos, h("div.zoomdiv").get(0).scrollTop = scrolly * n, scrollx = xpos, h("div.zoomdiv").get(0).scrollLeft = scrollx * r
            })
        }, function () {
            h(this).children("img").attr("alt", e), h(document.body).unbind("mousemove"), c.lens && h("div.jqZoomPup").remove(), h("div.zoomdiv").remove()
        }), count = 0, c.preload && (h("body").append("<div style='display:none;' class='jqPreload" + count + "'>sdsdssdsd</div>"), h(this).each(function () {
            var t = h(this).children("img").attr("jqimg"), e = jQuery("div.jqPreload" + count).html();
            jQuery("div.jqPreload" + count).html(e + '<img src="' + t + '">')
        }))
    }
}(jQuery), function (d) {
    window.NestedFormEvents = function () {
        this.addFields = d.proxy(this.addFields, this), this.removeFields = d.proxy(this.removeFields, this)
    }, NestedFormEvents.prototype = {
        addFields: function (t) {
            var e = t.currentTarget, i = d(e).data("association"),
                n = d("#" + d(e).data("blueprint-id")).data("blueprint"),
                r = (d(e).closest(".fields").closestChild("input, textarea, select").eq(0).attr("name") || "").replace(new RegExp("[[a-z_]+]$"), "");
            if (r) for (var o = r.match(/[a-z_]+_attributes(?=\]\[(new_)?\d+\])/g) || [], s = r.match(/[0-9]+/g) || [], a = 0; a < o.length; a++) s[a] && (n = (n = n.replace(new RegExp("(_" + o[a] + ")_.+?_", "g"), "$1_" + s[a] + "_")).replace(new RegExp("(\\[" + o[a] + "\\])\\[.+?\\]", "g"), "$1[" + s[a] + "]"));
            var l = new RegExp("new_" + i, "g"), c = this.newId();
            n = d.trim(n.replace(l, c));
            var h = this.insertFields(n, i, e);
            return h.trigger({type: "nested:fieldAdded", field: h}).trigger({
                type: "nested:fieldAdded:" + i,
                field: h
            }), !1
        }, newId: function () {
            return (new Date).getTime()
        }, insertFields: function (t, e, i) {
            var n = d(i).data("target");
            return n ? d(t).appendTo(d(n)) : d(t).insertBefore(i)
        }, removeFields: function (t) {
            var e = d(t.currentTarget), i = e.data("association");
            e.prev("input[type=hidden]").val("1");
            var n = e.closest(".fields");
            return n.hide(), n.trigger({
                type: "nested:fieldRemoved",
                field: n
            }).trigger({type: "nested:fieldRemoved:" + i, field: n}), !1
        }
    }, window.nestedFormEvents = new NestedFormEvents, d(document).delegate("form a.add_nested_fields", "click", nestedFormEvents.addFields).delegate("form a.remove_nested_fields", "click", nestedFormEvents.removeFields)
}(jQuery), function (o) {
    o.fn.closestChild = function (t) {
        if (t && "" != t) {
            var e = [];
            for (e.push(this); 0 < e.length;) for (var i = e.shift().children(), n = 0; n < i.length; ++n) {
                var r = o(i[n]);
                if (r.is(t)) return r;
                e.push(r)
            }
        }
        return o()
    }
}(jQuery), function () {
    var t;
    (t = "undefined" != typeof exports && null !== exports ? exports : this).selectAll = function (t) {
        return $("#check_box_all_" + t).prop("checked") ? $(".check_box_" + t).prop("checked", !0) : $(".check_box_" + t).prop("checked", !1)
    }, t.nice_comment_all = function () {
        var t, e, i;
        return e = $("#check_box_all_review").attr("oid"), 0 === $(".check_box_review:checked").size() ? alert("\u8bf7\u5148\u52fe\u9009\u9700\u8981\u8bc4\u4ef7\u7684\u5546\u54c1!") : confirm("\u786e\u5b9a\u5168\u90e8\u597d\u8bc4\u4e48?") ? (t = [], $(".check_box_review:checked").each(function () {
            return t.push($(this).attr("item_id"))
        }), i = "/orders/" + e + "/nice_comment_all?item_ids=" + t.join("_"), $.post(i, function () {
            return location.reload()
        })) : void 0
    }, jQuery(function () {
        return $(document).on("change", "#check_box_all_review", function () {
            return selectAll("review")
        })
    })
}.call(this), $(function () {
    $(document).on("click", ".supplier-quote .pagination a", function (t) {
        t.preventDefault();
        var e = $(this).attr("href");
        return "#" != e && $.ajax({
            url: e, success: function (t) {
                dialog.get("select_agent").content(t)
            }, cache: !1
        }), !1
    })
}), function (t, e) {
    "object" == typeof module && module.exports ? module.exports = t.document ? e(t) : e : t.Highcharts = e(t)
}("undefined" != typeof window ? window : this, function (t) {
    var e, i, n, r, o, s, a, l, v, c, h, d, u, p, f, g, m, y, x, b, L, w, C, _, M, k, S, E, T, I, A, $, P, F, D, O, N,
        j, z, R, B, H, W, q, G, X, Y, V, U, Q, K, Z, J, tt, et, it, nt, rt, ot, st, at, lt, ct, ht, dt, ut, pt, ft, gt,
        mt, vt, yt, xt, bt, wt, Ct, _t, kt, St, Tt, At, $t, Mt, Et, Pt, Lt, It, Ft, Dt, Ot, Nt, jt, zt, Rt, Bt, Ht, Wt,
        qt, Gt, Xt, Yt, Vt, Ut, Qt, Kt, Zt, Jt, te, ee, ie, ne, re, oe, se, ae, le, ce, he, de, ue, pe, fe, ge, me, ve,
        ye, xe, be, we, Ce, _e, ke, Se, Te, Ae, $e, Me, Ee, Pe, Le, Ie, Fe, De, Oe, Ne, je, ze, Re, Be, He, We, qe, Ge,
        Xe, Ye, Ve, Ue, Qe, Ke, Ze, Je, ti, ei, ii, ni, ri, oi, si, ai, li, ci, hi, di, ui, pi, fi, gi, mi, vi, yi, xi,
        bi, wi, Ci, _i, ki, Si, Ti, Ai, $i, Mi, Ei, Pi, Li, Ii, Fi, Di, Oi, Ni, ji, zi, Ri, Bi, Hi, Wi, qi, Gi, Xi, Yi,
        Vi, Ui, Qi, Ki, Zi, Ji, tn, en, nn, rn, on, sn, an, ln, cn, hn, dn, un, pn, fn, gn, mn, vn, yn, xn, bn, wn, Cn,
        _n, kn, Sn, Tn, An, $n, Mn, En, Pn, Ln, In, Fn, Dn, On, Nn, jn, zn, Rn, Bn, Hn, Wn, qn, Gn, Xn, Yn, Vn, Un, Qn,
        Kn, Zn, Jn, tr, er, ir, nr, rr, or, sr, ar, lr, cr, hr, dr, ur, pr, fr, gr, mr, vr, yr, xr, br, wr, Cr, _r, kr,
        Sr, Tr, Ar, $r, Mr, Er, Pr, Lr, Ir, Fr, Dr, Or, Nr, jr, zr, Rr, Br, Hr, Wr, qr, Gr, Xr, Yr, Vr, Ur, Qr, Kr, Zr,
        Jr, to, eo, io, no, ro, oo, so, ao, lo, co, ho, uo, po, fo, go, mo, vo, yo, xo, bo, wo, Co, _o, ko, So, To, Ao,
        $o, Mo, Eo, Po, Lo, Io, Fo, Do, Oo, No, jo, zo, Ro, Bo, Ho, Wo, qo, Go, Xo, Yo, Vo, Uo, Qo, Ko, Zo, Jo, ts;
    return e = window, i = e.document, n = e.navigator && e.navigator.userAgent || "", r = i && i.createElementNS && !!i.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect, o = /(edge|msie|trident)/i.test(n) && !window.opera, s = !r, a = /Firefox/.test(n), l = a && parseInt(n.split("Firefox/")[1], 10) < 4, t = e.Highcharts ? e.Highcharts.error(16, !0) : {
        product: "Highcharts",
        version: "5.0.2",
        deg2rad: 2 * Math.PI / 360,
        doc: i,
        hasBidiBug: l,
        hasTouch: i && void 0 !== i.documentElement.ontouchstart,
        isMS: o,
        isWebKit: /AppleWebKit/.test(n),
        isFirefox: a,
        isTouchDevice: /(Mobile|Android|Windows Phone)/.test(n),
        SVG_NS: "http://www.w3.org/2000/svg",
        idCounter: 0,
        chartCount: 0,
        seriesTypes: {},
        symbolSizes: {},
        svg: r,
        vml: s,
        win: e,
        charts: [],
        marginNames: ["plotTop", "marginRight", "marginBottom", "plotLeft"],
        noop: function () {
        }
    }, c = [], h = (v = t).charts, d = v.doc, u = v.win, v.error = function (t, e) {
        if (t = "Highcharts error #" + t + ": www.highcharts.com/errors/" + t, e) throw Error(t);
        u.console && console.log(t)
    }, v.Fx = function (t, e, i) {
        this.options = e, this.elem = t, this.prop = i
    }, v.Fx.prototype = {
        dSetter: function () {
            var t, e = this.paths[0], i = this.paths[1], n = [], r = this.now, o = e.length;
            if (1 === r) n = this.toD; else if (o === i.length && r < 1) for (; o--;) t = parseFloat(e[o]), n[o] = isNaN(t) ? e[o] : r * parseFloat(i[o] - t) + t; else n = i;
            this.elem.attr("d", n)
        }, update: function () {
            var t = this.elem, e = this.prop, i = this.now, n = this.options.step;
            this[e + "Setter"] ? this[e + "Setter"]() : t.attr ? t.element && t.attr(e, i) : t.style[e] = i + this.unit, n && n.call(t, i, this)
        }, run: function (t, e, i) {
            var n, r = this, o = function (t) {
                return !o.stopped && r.step(t)
            };
            this.startTime = +new Date, this.start = t, this.end = e, this.unit = i, this.now = this.start, this.pos = 0, o.elem = this.elem, o() && 1 === c.push(o) && (o.timerId = setInterval(function () {
                for (n = 0; n < c.length; n++) c[n]() || c.splice(n--, 1);
                c.length || clearInterval(o.timerId)
            }, 13))
        }, step: function (t) {
            var e, i = +new Date, n = this.options;
            e = this.elem;
            var r, o = n.complete, s = n.duration, a = n.curAnim;
            if (e.attr && !e.element) e = !1; else if (t || i >= s + this.startTime) {
                for (r in this.now = this.end, this.pos = 1, this.update(), t = a[this.prop] = !0, a) !0 !== a[r] && (t = !1);
                t && o && o.call(e), e = !1
            } else this.pos = n.easing((i - this.startTime) / s), this.now = this.start + (this.end - this.start) * this.pos, this.update(), e = !0;
            return e
        }, initPath: function (t, e, i) {
            function n(t) {
                for (c = t.length; c--;) "M" !== t[c] && "L" !== t[c] || t.splice(c + 1, 0, t[c + 1], t[c + 2], t[c + 1], t[c + 2])
            }

            function r(t, e) {
                for (; t.length < a;) {
                    t[0] = e[a - t.length];
                    var i = t.slice(0, p);
                    [].splice.apply(t, [0, 0].concat(i)), g && (i = t.slice(t.length - p), [].splice.apply(t, [t.length, 0].concat(i)), c--)
                }
                t[0] = "M"
            }

            function o(t, e) {
                for (var i = (a - t.length) / p; 0 < i && i--;) (l = t.slice().splice(t.length / m - p, p * m))[0] = e[a - p - i * p], u && (l[p - 6] = l[p - 2], l[p - 5] = l[p - 1]), [].splice.apply(t, [t.length / m, 0].concat(l)), g && i--
            }

            e = e || "";
            var s, a, l, c, h = t.startX, d = t.endX, u = -1 < e.indexOf("C"), p = u ? 7 : 3;
            e = e.split(" "), i = i.slice();
            var f, g = t.isArea, m = g ? 2 : 1;
            if (u && (n(e), n(i)), h && d) {
                for (c = 0; c < h.length; c++) {
                    if (h[c] === d[0]) {
                        s = c;
                        break
                    }
                    if (h[0] === d[d.length - h.length + c]) {
                        s = c, f = !0;
                        break
                    }
                }
                void 0 === s && (e = [])
            }
            return e.length && v.isNumber(s) && (a = i.length + s * m * p, f ? (r(e, i), o(i, e)) : (r(i, e), o(e, i))), [e, i]
        }
    }, v.extend = function (t, e) {
        var i;
        for (i in t || (t = {}), e) t[i] = e[i];
        return t
    }, v.merge = function () {
        var t, e, i = arguments, n = {}, r = function (t, e) {
            var i, n;
            for (n in"object" != typeof t && (t = {}), e) e.hasOwnProperty(n) && (i = e[n], v.isObject(i, !0) && "renderTo" !== n && "number" != typeof i.nodeType ? t[n] = r(t[n] || {}, i) : t[n] = e[n]);
            return t
        };
        for (!0 === i[0] && (n = i[1], i = Array.prototype.slice.call(i, 2)), e = i.length, t = 0; t < e; t++) n = r(n, i[t]);
        return n
    }, v.pInt = function (t, e) {
        return parseInt(t, e || 10)
    }, v.isString = function (t) {
        return "string" == typeof t
    }, v.isArray = function (t) {
        return "[object Array]" === (t = Object.prototype.toString.call(t)) || "[object Array Iterator]" === t
    }, v.isObject = function (t, e) {
        return t && "object" == typeof t && (!e || !v.isArray(t))
    }, v.isNumber = function (t) {
        return "number" == typeof t && !isNaN(t)
    }, v.erase = function (t, e) {
        for (var i = t.length; i--;) if (t[i] === e) {
            t.splice(i, 1);
            break
        }
    }, v.defined = function (t) {
        return null != t
    }, v.attr = function (t, e, i) {
        var n, r;
        if (v.isString(e)) v.defined(i) ? t.setAttribute(e, i) : t && t.getAttribute && (r = t.getAttribute(e)); else if (v.defined(e) && v.isObject(e)) for (n in e) t.setAttribute(n, e[n]);
        return r
    }, v.splat = function (t) {
        return v.isArray(t) ? t : [t]
    }, v.syncTimeout = function (t, e, i) {
        if (e) return setTimeout(t, e, i);
        t.call(0, i)
    }, v.pick = function () {
        var t, e, i = arguments, n = i.length;
        for (t = 0; t < n; t++) if (null != (e = i[t])) return e
    }, v.css = function (t, e) {
        v.isMS && !v.svg && e && void 0 !== e.opacity && (e.filter = "alpha(opacity=" + 100 * e.opacity + ")"), v.extend(t.style, e)
    }, v.createElement = function (t, e, i, n, r) {
        t = d.createElement(t);
        var o = v.css;
        return e && v.extend(t, e), r && o(t, {
            padding: 0,
            border: "none",
            margin: 0
        }), i && o(t, i), n && n.appendChild(t), t
    }, v.extendClass = function (t, e) {
        var i = function () {
        };
        return i.prototype = new t, v.extend(i.prototype, e), i
    }, v.pad = function (t, e, i) {
        return Array((e || 2) + 1 - String(t).length).join(i || 0) + t
    }, v.relativeLength = function (t, e) {
        return /%$/.test(t) ? e * parseFloat(t) / 100 : parseFloat(t)
    }, v.wrap = function (t, e, i) {
        var n = t[e];
        t[e] = function () {
            var t = Array.prototype.slice.call(arguments);
            return t.unshift(n), i.apply(this, t)
        }
    }, v.getTZOffset = function (t) {
        var e = v.Date;
        return 6e4 * (e.hcGetTimezoneOffset && e.hcGetTimezoneOffset(t) || e.hcTimezoneOffset || 0)
    }, v.dateFormat = function (t, e, i) {
        if (!v.defined(e) || isNaN(e)) return v.defaultOptions.lang.invalidDate || "";
        t = v.pick(t, "%Y-%m-%d %H:%M:%S");
        var n, r = new (f = v.Date)(e - v.getTZOffset(e)), o = r[f.hcGetHours](), s = r[f.hcGetDay](),
            a = r[f.hcGetDate](), l = r[f.hcGetMonth](), c = r[f.hcGetFullYear](), h = v.defaultOptions.lang,
            d = h.weekdays, u = h.shortWeekdays, p = v.pad, f = v.extend({
                a: u ? u[s] : d[s].substr(0, 3),
                A: d[s],
                d: p(a),
                e: p(a, 2, " "),
                w: s,
                b: h.shortMonths[l],
                B: h.months[l],
                m: p(l + 1),
                y: c.toString().substr(2, 2),
                Y: c,
                H: p(o),
                k: o,
                I: p(o % 12 || 12),
                l: o % 12 || 12,
                M: p(r[f.hcGetMinutes]()),
                p: o < 12 ? "AM" : "PM",
                P: o < 12 ? "am" : "pm",
                S: p(r.getSeconds()),
                L: p(Math.round(e % 1e3), 3)
            }, v.dateFormats);
        for (n in f) for (; -1 !== t.indexOf("%" + n);) t = t.replace("%" + n, "function" == typeof f[n] ? f[n](e) : f[n]);
        return i ? t.substr(0, 1).toUpperCase() + t.substr(1) : t
    }, v.formatSingle = function (t, e) {
        var i = /\.([0-9])/, n = v.defaultOptions.lang;
        return /f$/.test(t) ? (i = (i = t.match(i)) ? i[1] : -1, null !== e && (e = v.numberFormat(e, i, n.decimalPoint, -1 < t.indexOf(",") ? n.thousandsSep : ""))) : e = v.dateFormat(t, e), e
    }, v.format = function (t, e) {
        for (var i, n, r, o, s, a = "{", l = !1, c = []; t && -1 !== (a = t.indexOf(a));) {
            if (i = t.slice(0, a), l) {
                for (o = (n = (i = i.split(":")).shift().split(".")).length, s = e, r = 0; r < o; r++) s = s[n[r]];
                i.length && (s = v.formatSingle(i.join(":"), s)), c.push(s)
            } else c.push(i);
            t = t.slice(a + 1), a = (l = !l) ? "}" : "{"
        }
        return c.push(t), c.join("")
    }, v.getMagnitude = function (t) {
        return Math.pow(10, Math.floor(Math.log(t) / Math.LN10))
    }, v.normalizeTickInterval = function (t, e, i, n, r) {
        var o, s = t;
        for (o = t / (i = v.pick(i, 1)), e || !(e = [1, 2, 2.5, 5, 10]) === n && (1 === i ? e = [1, 2, 5, 10] : i <= .1 && (e = [1 / i])), n = 0; n < e.length && (s = e[n], !(r && t <= s * i || !r && o <= (e[n] + (e[n + 1] || e[n])) / 2)); n++) ;
        return s * i
    }, v.stableSort = function (t, i) {
        var n, e, r = t.length;
        for (e = 0; e < r; e++) t[e].safeI = e;
        for (t.sort(function (t, e) {
            return 0 === (n = i(t, e)) ? t.safeI - e.safeI : n
        }), e = 0; e < r; e++) delete t[e].safeI
    }, v.arrayMin = function (t) {
        for (var e = t.length, i = t[0]; e--;) t[e] < i && (i = t[e]);
        return i
    }, v.arrayMax = function (t) {
        for (var e = t.length, i = t[0]; e--;) t[e] > i && (i = t[e]);
        return i
    }, v.destroyObjectProperties = function (t, e) {
        for (var i in t) t[i] && t[i] !== e && t[i].destroy && t[i].destroy(), delete t[i]
    }, v.discardElement = function (t) {
        var e = v.garbageBin;
        e || (e = v.createElement("div")), t && e.appendChild(t), e.innerHTML = ""
    }, v.correctFloat = function (t, e) {
        return parseFloat(t.toPrecision(e || 14))
    }, v.setAnimation = function (t, e) {
        e.renderer.globalAnimation = v.pick(t, e.options.chart.animation, !0)
    }, v.animObject = function (t) {
        return v.isObject(t) ? v.merge(t) : {duration: t ? 500 : 0}
    }, v.timeUnits = {
        millisecond: 1,
        second: 1e3,
        minute: 6e4,
        hour: 36e5,
        day: 864e5,
        week: 6048e5,
        month: 24192e5,
        year: 314496e5
    }, v.numberFormat = function (t, e, i, n) {
        t = +t || 0, e = +e;
        var r, o, s = v.defaultOptions.lang, a = (t.toString().split(".")[1] || "").length, l = Math.abs(t);
        return -1 === e ? e = Math.min(a, 20) : v.isNumber(e) || (e = 2), o = 3 < (r = String(v.pInt(l.toFixed(e)))).length ? r.length % 3 : 0, i = v.pick(i, s.decimalPoint), n = v.pick(n, s.thousandsSep), t = (t < 0 ? "-" : "") + (o ? r.substr(0, o) + n : ""), t += r.substr(o).replace(/(\d{3})(?=\d)/g, "$1" + n), e && (t += i + (n = Math.abs(l - r + Math.pow(10, -Math.max(e, a) - 1))).toFixed(e).slice(2)), t
    }, Math.easeInOutSine = function (t) {
        return -.5 * (Math.cos(Math.PI * t) - 1)
    }, v.getStyle = function (t, e) {
        return "width" === e ? Math.min(t.offsetWidth, t.scrollWidth) - v.getStyle(t, "padding-left") - v.getStyle(t, "padding-right") : "height" === e ? Math.min(t.offsetHeight, t.scrollHeight) - v.getStyle(t, "padding-top") - v.getStyle(t, "padding-bottom") : (t = u.getComputedStyle(t, void 0)) && v.pInt(t.getPropertyValue(e))
    }, v.inArray = function (t, e) {
        return e.indexOf ? e.indexOf(t) : [].indexOf.call(e, t)
    }, v.grep = function (t, e) {
        return [].filter.call(t, e)
    }, v.map = function (t, e) {
        for (var i = [], n = 0, r = t.length; n < r; n++) i[n] = e.call(t[n], t[n], n, t);
        return i
    }, v.offset = function (t) {
        var e = d.documentElement;
        return {
            top: (t = t.getBoundingClientRect()).top + (u.pageYOffset || e.scrollTop) - (e.clientTop || 0),
            left: t.left + (u.pageXOffset || e.scrollLeft) - (e.clientLeft || 0)
        }
    }, v.stop = function (t) {
        for (var e = c.length; e--;) c[e].elem === t && (c[e].stopped = !0)
    }, v.each = function (t, e, i) {
        return Array.prototype.forEach.call(t, e, i)
    }, v.addEvent = function (e, t, i) {
        function n(t) {
            t.target = t.srcElement || u, i.call(e, t)
        }

        var r = e.hcEvents = e.hcEvents || {};
        e.addEventListener ? e.addEventListener(t, i, !1) : e.attachEvent && (e.hcEventsIE || (e.hcEventsIE = {}), e.hcEventsIE[i.toString()] = n, e.attachEvent("on" + t, n)), r[t] || (r[t] = []), r[t].push(i)
    }, v.removeEvent = function (i, n, t) {
        function r(t, e) {
            i.removeEventListener ? i.removeEventListener(t, e, !1) : i.attachEvent && (e = i.hcEventsIE[e.toString()], i.detachEvent("on" + t, e))
        }

        function e() {
            var t, e;
            if (i.nodeName) for (e in n ? (t = {})[n] = !0 : t = a, t) if (a[e]) for (t = a[e].length; t--;) r(e, a[e][t])
        }

        var o, s, a = i.hcEvents;
        a && (n ? (o = a[n] || [], t ? (-1 < (s = v.inArray(t, o)) && (o.splice(s, 1), a[n] = o), r(n, t)) : (e(), a[n] = [])) : (e(), i.hcEvents = {}))
    }, v.fireEvent = function (t, e, i, n) {
        var r, o, s;
        if (r = t.hcEvents, i = i || {}, d.createEvent && (t.dispatchEvent || t.fireEvent)) (r = d.createEvent("Events")).initEvent(e, !0, !0), v.extend(r, i), t.dispatchEvent ? t.dispatchEvent(r) : t.fireEvent(e, r); else if (r) for (o = (r = r[e] || []).length, i.target || v.extend(i, {
            preventDefault: function () {
                i.defaultPrevented = !0
            }, target: t, type: e
        }), e = 0; e < o; e++) (s = r[e]) && !1 === s.call(t, i) && i.preventDefault();
        n && !i.defaultPrevented && n(i)
    }, v.animate = function (t, e, i) {
        var n, r, o, s, a = "";
        for (s in v.isObject(i) || (i = {
            duration: (n = arguments)[2],
            easing: n[3],
            complete: n[4]
        }), v.isNumber(i.duration) || (i.duration = 400), i.easing = "function" == typeof i.easing ? i.easing : Math[i.easing] || Math.easeInOutSine, i.curAnim = v.merge(e), e) o = new v.Fx(t, i, s), r = null, "d" === s ? (o.paths = o.initPath(t, t.d, e.d), o.toD = e.d, n = 0, r = 1) : t.attr ? n = t.attr(s) : (n = parseFloat(v.getStyle(t, s)) || 0, "opacity" !== s && (a = "px")), r || (r = e[s]), r.match && r.match("px") && (r = r.replace(/px/g, "")), o.run(n, r, a)
    }, v.seriesType = function (t, e, i, n, r) {
        var o = v.getOptions(), s = v.seriesTypes;
        return o.plotOptions[t] = v.merge(o.plotOptions[e], i), s[t] = v.extendClass(s[e] || function () {
        }, n), s[t].prototype.type = t, r && (s[t].prototype.pointClass = v.extendClass(v.Point, r)), s[t]
    }, u.jQuery && (u.jQuery.fn.highcharts = function () {
        var t = [].slice.call(arguments);
        if (this[0]) return t[0] ? (new (v[v.isString(t[0]) ? t.shift() : "Chart"])(this[0], t[0], t[1]), this) : h[v.attr(this[0], "data-highcharts-chart")]
    }), d && !d.defaultView && (v.getStyle = function (t, e) {
        var i = {width: "clientWidth", height: "clientHeight"}[e];
        return t.style[e] ? v.pInt(t.style[e]) : ("opacity" === e && (e = "filter"), i ? (t.style.zoom = 1, Math.max(t[i] - 2 * v.getStyle(t, "padding"), 0)) : (t = t.currentStyle[e.replace(/\-(\w)/g, function (t, e) {
            return e.toUpperCase()
        })], "filter" === e && (t = t.replace(/alpha\(opacity=([0-9]+)\)/, function (t, e) {
            return e / 100
        })), "" === t ? 1 : v.pInt(t)))
    }), Array.prototype.forEach || (v.each = function (t, e, i) {
        for (var n = 0, r = t.length; n < r; n++) if (!1 === e.call(i, t[n], n, t)) return n
    }), Array.prototype.indexOf || (v.inArray = function (t, e) {
        var i, n = 0;
        if (e) for (i = e.length; n < i; n++) if (e[n] === t) return n;
        return -1
    }), Array.prototype.filter || (v.grep = function (t, e) {
        for (var i = [], n = 0, r = t.length; n < r; n++) e(t[n], n) && i.push(t[n]);
        return i
    }), f = (p = t).each, g = p.isNumber, m = p.map, y = p.merge, x = p.pInt, p.Color = function (t) {
        if (!(this instanceof p.Color)) return new p.Color(t);
        this.init(t)
    }, p.Color.prototype = {
        parsers: [{
            regex: /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]?(?:\.[0-9]+)?)\s*\)/,
            parse: function (t) {
                return [x(t[1]), x(t[2]), x(t[3]), parseFloat(t[4], 10)]
            }
        }, {
            regex: /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/, parse: function (t) {
                return [x(t[1], 16), x(t[2], 16), x(t[3], 16), 1]
            }
        }, {
            regex: /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/, parse: function (t) {
                return [x(t[1]), x(t[2]), x(t[3]), 1]
            }
        }], names: {white: "#ffffff", black: "#000000"}, init: function (t) {
            var e, i, n, r;
            if ((this.input = t = this.names[t] || t) && t.stops) this.stops = m(t.stops, function (t) {
                return new p.Color(t[1])
            }); else for (n = this.parsers.length; n-- && !i;) (e = (r = this.parsers[n]).regex.exec(t)) && (i = r.parse(e));
            this.rgba = i || []
        }, get: function (i) {
            var n, t = this.input, e = this.rgba;
            return this.stops ? ((n = y(t)).stops = [].concat(n.stops), f(this.stops, function (t, e) {
                n.stops[e] = [n.stops[e][0], t.get(i)]
            })) : n = e && g(e[0]) ? "rgb" === i || !i && 1 === e[3] ? "rgb(" + e[0] + "," + e[1] + "," + e[2] + ")" : "a" === i ? e[3] : "rgba(" + e.join(",") + ")" : t, n
        }, brighten: function (e) {
            var t, i = this.rgba;
            if (this.stops) f(this.stops, function (t) {
                t.brighten(e)
            }); else if (g(e) && 0 !== e) for (t = 0; t < 3; t++) i[t] += x(255 * e), i[t] < 0 && (i[t] = 0), 255 < i[t] && (i[t] = 255);
            return this
        }, setOpacity: function (t) {
            return this.rgba[3] = t, this
        }
    }, p.color = function (t) {
        return new p.Color(t)
    }, C = (b = t).addEvent, _ = b.animate, M = b.attr, k = b.charts, S = b.color, E = b.css, T = b.createElement, I = b.defined, A = b.deg2rad, $ = b.destroyObjectProperties, P = b.doc, F = b.each, D = b.extend, O = b.erase, N = b.grep, j = b.hasTouch, z = b.isArray, R = b.isFirefox, B = b.isMS, H = b.isObject, W = b.isString, q = b.isWebKit, G = b.merge, X = b.noop,Y = b.pick,V = b.pInt,U = b.removeEvent,Q = b.stop,K = b.svg,Z = b.SVG_NS,J = b.symbolSizes,tt = b.win,(L = b.SVGElement = function () {
        return this
    }).prototype = {
        opacity: 1,
        SVG_NS: Z,
        textProps: "direction fontSize fontWeight fontFamily fontStyle color lineHeight width textDecoration textOverflow textShadow".split(" "),
        init: function (t, e) {
            this.element = "span" === e ? T(e) : P.createElementNS(this.SVG_NS, e), this.renderer = t
        },
        animate: function (t, e, i) {
            return e = Y(e, this.renderer.globalAnimation, !0), Q(this), e ? (i && (e.complete = i), _(this, t, e)) : this.attr(t, null, i), this
        },
        colorGradient: function (t, e, i) {
            var n, r, o, s, a, l, c, h, d, u, p, f, g = this.renderer, m = [];
            if (t.linearGradient ? r = "linearGradient" : t.radialGradient && (r = "radialGradient"), r) {
                for (p in o = t[r], a = g.gradients, c = t.stops, u = i.radialReference, z(o) && (t[r] = o = {
                    x1: o[0],
                    y1: o[1],
                    x2: o[2],
                    y2: o[3],
                    gradientUnits: "userSpaceOnUse"
                }), "radialGradient" === r && u && !I(o.gradientUnits) && (o = G(s = o, g.getRadialAttr(u, s), {gradientUnits: "userSpaceOnUse"})), o) "id" !== p && m.push(p, o[p]);
                for (p in c) m.push(c[p]);
                a[m = m.join(",")] ? u = a[m].attr("id") : (o.id = u = "highcharts-" + b.idCounter++, a[m] = l = g.createElement(r).attr(o).add(g.defs), l.radAttr = s, l.stops = [], F(c, function (t) {
                    0 === t[1].indexOf("rgba") ? (n = b.color(t[1]), h = n.get("rgb"), d = n.get("a")) : (h = t[1], d = 1), t = g.createElement("stop").attr({
                        offset: t[0],
                        "stop-color": h,
                        "stop-opacity": d
                    }).add(l), l.stops.push(t)
                })), f = "url(" + g.url + "#" + u + ")", i.setAttribute(e, f), i.gradient = m, t.toString = function () {
                    return f
                }
            }
        },
        applyTextShadow: function (t) {
            var e, o = this.element, i = -1 !== t.indexOf("contrast"), n = {}, r = this.renderer.forExport,
                s = this.renderer.forExport || void 0 !== o.style.textShadow && !B;
            i && (n.textShadow = t = t.replace(/contrast/g, this.renderer.getContrast(o.style.fill))), (q || r) && (n.textRendering = "geometricPrecision"), s ? this.css(n) : (this.fakeTS = !0, this.ySetter = this.xSetter, e = [].slice.call(o.getElementsByTagName("tspan")), F(t.split(/\s?,\s?/g), function (t) {
                var i, n, r = o.firstChild;
                t = t.split(" "), i = t[t.length - 1], (n = t[t.length - 2]) && F(e, function (t, e) {
                    0 === e && (t.setAttribute("x", o.getAttribute("x")), e = o.getAttribute("y"), t.setAttribute("y", e || 0), null === e && o.setAttribute("y", 0)), t = t.cloneNode(1), M(t, {
                        "class": "highcharts-text-shadow",
                        fill: i,
                        stroke: i,
                        "stroke-opacity": 1 / Math.max(V(n), 3),
                        "stroke-width": n,
                        "stroke-linejoin": "round"
                    }), o.insertBefore(t, r)
                })
            }))
        },
        attr: function (t, e, i) {
            var n, r, o, s = this.element, a = this;
            if ("string" == typeof t && void 0 !== e && (n = t, (t = {})[n] = e), "string" == typeof t) a = (this[t + "Getter"] || this._defaultGetter).call(this, t, s); else {
                for (n in t) e = t[n], o = !1, this.symbolName && /^(x|y|width|height|r|start|end|innerR|anchorX|anchorY)/.test(n) && (r || (this.symbolAttr(t), r = !0), o = !0), !this.rotation || "x" !== n && "y" !== n || (this.doTransform = !0), o || ((o = this[n + "Setter"] || this._defaultSetter).call(this, e, n, s), this.shadows && /^(width|height|visibility|x|y|d|transform|cx|cy|r)$/.test(n) && this.updateShadows(n, e, o));
                this.doTransform && (this.updateTransform(), this.doTransform = !1)
            }
            return i && i(), a
        },
        updateShadows: function (t, e, i) {
            for (var n = this.shadows, r = n.length; r--;) i.call(n[r], "height" === t ? Math.max(e - (n[r].cutHeight || 0), 0) : "d" === t ? this.d : e, t, n[r])
        },
        addClass: function (t, e) {
            var i = this.attr("class") || "";
            return -1 === i.indexOf(t) && (e || (t = (i + (i ? " " : "") + t).replace("  ", " ")), this.attr("class", t)), this
        },
        hasClass: function (t) {
            return -1 !== M(this.element, "class").indexOf(t)
        },
        removeClass: function (t) {
            return M(this.element, "class", (M(this.element, "class") || "").replace(t, "")), this
        },
        symbolAttr: function (e) {
            var i = this;
            F("x y r start end width height innerR anchorX anchorY".split(" "), function (t) {
                i[t] = Y(e[t], i[t])
            }), i.attr({d: i.renderer.symbols[i.symbolName](i.x, i.y, i.width, i.height, i)})
        },
        clip: function (t) {
            return this.attr("clip-path", t ? "url(" + this.renderer.url + "#" + t.id + ")" : "none")
        },
        crisp: function (t, e) {
            var i, n, r = {};
            for (i in e = e || t.strokeWidth || 0, n = Math.round(e) % 2 / 2, t.x = Math.floor(t.x || this.x || 0) + n, t.y = Math.floor(t.y || this.y || 0) + n, t.width = Math.floor((t.width || this.width || 0) - 2 * n), t.height = Math.floor((t.height || this.height || 0) - 2 * n), I(t.strokeWidth) && (t.strokeWidth = e), t) this[i] !== t[i] && (this[i] = r[i] = t[i]);
            return r
        },
        css: function (t) {
            var e, i, n = this.styles, r = {}, o = this.element, s = "";
            if (e = !n, t && t.color && (t.fill = t.color), n) for (i in t) t[i] !== n[i] && (r[i] = t[i], e = !0);
            if (e) {
                if (e = this.textWidth = t && t.width && "text" === o.nodeName.toLowerCase() && V(t.width) || this.textWidth, n && (t = D(n, r)), this.styles = t, e && !K && this.renderer.forExport && delete t.width, B && !K) E(this.element, t); else {
                    for (i in n = function (t, e) {
                        return "-" + e.toLowerCase()
                    }, t) s += i.replace(/([A-Z])/g, n) + ":" + t[i] + ";";
                    M(o, "style", s)
                }
                this.added && e && this.renderer.buildText(this)
            }
            return this
        },
        strokeWidth: function () {
            return this["stroke-width"] || 0
        },
        on: function (t, e) {
            var i = this, n = i.element;
            return j && "click" === t ? (n.ontouchstart = function (t) {
                i.touchEventFired = Date.now(), t.preventDefault(), e.call(n, t)
            }, n.onclick = function (t) {
                (-1 === tt.navigator.userAgent.indexOf("Android") || 1100 < Date.now() - (i.touchEventFired || 0)) && e.call(n, t)
            }) : n["on" + t] = e, this
        },
        setRadialReference: function (t) {
            var e = this.renderer.gradients[this.element.gradient];
            return this.element.radialReference = t, e && e.radAttr && e.animate(this.renderer.getRadialAttr(t, e.radAttr)), this
        },
        translate: function (t, e) {
            return this.attr({translateX: t, translateY: e})
        },
        invert: function (t) {
            return this.inverted = t, this.updateTransform(), this
        },
        updateTransform: function () {
            var t = this.translateX || 0, e = this.translateY || 0, i = this.scaleX, n = this.scaleY, r = this.inverted,
                o = this.rotation, s = this.element;
            r && (t += this.attr("width"), e += this.attr("height")), t = ["translate(" + t + "," + e + ")"], r ? t.push("rotate(90) scale(-1,1)") : o && t.push("rotate(" + o + " " + (s.getAttribute("x") || 0) + " " + (s.getAttribute("y") || 0) + ")"), (I(i) || I(n)) && t.push("scale(" + Y(i, 1) + " " + Y(n, 1) + ")"), t.length && s.setAttribute("transform", t.join(" "))
        },
        toFront: function () {
            var t = this.element;
            return t.parentNode.appendChild(t), this
        },
        align: function (t, e, i) {
            var n, r, o, s, a, l, c = {};
            return o = (r = this.renderer).alignedObjects, t ? (this.alignOptions = t, this.alignByTranslate = e, (!i || W(i)) && (this.alignTo = n = i || "renderer", O(o, this), o.push(this), i = null)) : (t = this.alignOptions, e = this.alignByTranslate, n = this.alignTo), i = Y(i, r[n], r), n = t.align, r = t.verticalAlign, o = (i.x || 0) + (t.x || 0), s = (i.y || 0) + (t.y || 0), "right" === n ? a = 1 : "center" === n && (a = 2), a && (o += (i.width - (t.width || 0)) / a), c[e ? "translateX" : "x"] = Math.round(o), "bottom" === r ? l = 1 : "middle" === r && (l = 2), l && (s += (i.height - (t.height || 0)) / l), c[e ? "translateY" : "y"] = Math.round(s), this[this.placed ? "animate" : "attr"](c), this.placed = !0, this.alignAttr = c, this
        },
        getBBox: function (t, e) {
            var i, n, r, o, s, a, l = this.renderer, c = this.element, h = this.styles, d = this.textStr, u = c.style,
                p = l.cache, f = l.cacheKeys;
            if (n = (e = Y(e, this.rotation)) * A, r = h && h.fontSize, void 0 !== d && (a = d.toString().replace(/[0-9]/g, "0") + ["", e || 0, r, c.style.width].join()), a && !t && (i = p[a]), !i) {
                if (c.namespaceURI === this.SVG_NS || l.forExport) {
                    try {
                        s = this.fakeTS && function (e) {
                            F(c.querySelectorAll(".highcharts-text-shadow"), function (t) {
                                t.style.display = e
                            })
                        }, R && u.textShadow ? (o = u.textShadow, u.textShadow = "") : s && s("none"), i = c.getBBox ? D({}, c.getBBox()) : {
                            width: c.offsetWidth,
                            height: c.offsetHeight
                        }, o ? u.textShadow = o : s && s("")
                    } catch (g) {
                    }
                    (!i || i.width < 0) && (i = {width: 0, height: 0})
                } else i = this.htmlGetBBox();
                if (l.isSVG && (t = i.width, l = i.height, B && h && "11px" === h.fontSize && "16.9" === l.toPrecision(3) && (i.height = l = 14), e && (i.width = Math.abs(l * Math.sin(n)) + Math.abs(t * Math.cos(n)), i.height = Math.abs(l * Math.cos(n)) + Math.abs(t * Math.sin(n)))), a && 0 < i.height) {
                    for (; 250 < f.length;) delete p[f.shift()];
                    p[a] || f.push(a), p[a] = i
                }
            }
            return i
        },
        show: function (t) {
            return this.attr({visibility: t ? "inherit" : "visible"})
        },
        hide: function () {
            return this.attr({visibility: "hidden"})
        },
        fadeOut: function (t) {
            var e = this;
            e.animate({opacity: 0}, {
                duration: t || 150, complete: function () {
                    e.attr({y: -9999})
                }
            })
        },
        add: function (t) {
            var e, i = this.renderer, n = this.element;
            return t && (this.parentGroup = t), this.parentInverted = t && t.inverted, void 0 !== this.textStr && i.buildText(this), this.added = !0, (!t || t.handleZ || this.zIndex) && (e = this.zIndexSetter()), e || (t ? t.element : i.box).appendChild(n), this.onAdd && this.onAdd(), this
        },
        safeRemoveChild: function (t) {
            var e = t.parentNode;
            e && e.removeChild(t)
        },
        destroy: function () {
            var t, e, i = this.element || {}, n = this.renderer.isSVG && "SPAN" === i.nodeName && this.parentGroup;
            if (i.onclick = i.onmouseout = i.onmouseover = i.onmousemove = i.point = null, Q(this), this.clipPath && (this.clipPath = this.clipPath.destroy()), this.stops) {
                for (e = 0; e < this.stops.length; e++) this.stops[e] = this.stops[e].destroy();
                this.stops = null
            }
            for (this.safeRemoveChild(i), this.destroyShadows(); n && n.div && 0 === n.div.childNodes.length;) i = n.parentGroup, this.safeRemoveChild(n.div), delete n.div, n = i;
            for (t in this.alignTo && O(this.renderer.alignedObjects, this), this) delete this[t];
            return null
        },
        shadow: function (t, e, i) {
            var n, r, o, s, a, l, c = [], h = this.element;
            if (t) {
                if (!this.shadows) {
                    for (s = Y(t.width, 3), a = (t.opacity || .15) / s, l = this.parentInverted ? "(-1,-1)" : "(" + Y(t.offsetX, 1) + ", " + Y(t.offsetY, 1) + ")", n = 1; n <= s; n++) r = h.cloneNode(0), o = 2 * s + 1 - 2 * n, M(r, {
                        isShadow: "true",
                        stroke: t.color || "#000000",
                        "stroke-opacity": a * n,
                        "stroke-width": o,
                        transform: "translate" + l,
                        fill: "none"
                    }), i && (M(r, "height", Math.max(M(r, "height") - o, 0)), r.cutHeight = o), e ? e.element.appendChild(r) : h.parentNode.insertBefore(r, h), c.push(r);
                    this.shadows = c
                }
            } else this.destroyShadows();
            return this
        },
        destroyShadows: function () {
            F(this.shadows || [], function (t) {
                this.safeRemoveChild(t)
            }, this), this.shadows = void 0
        },
        xGetter: function (t) {
            return "circle" === this.element.nodeName && ("x" === t ? t = "cx" : "y" === t && (t = "cy")), this._defaultGetter(t)
        },
        _defaultGetter: function (t) {
            return t = Y(this[t], this.element ? this.element.getAttribute(t) : null, 0), /^[\-0-9\.]+$/.test(t) && (t = parseFloat(t)), t
        },
        dSetter: function (t, e, i) {
            t && t.join && (t = t.join(" ")), /(NaN| {2}|^$)/.test(t) && (t = "M 0 0"), i.setAttribute(e, t), this[e] = t
        },
        dashstyleSetter: function (t) {
            var e, i = this["stroke-width"];
            if ("inherit" === i && (i = 1), t = t && t.toLowerCase()) {
                for (e = (t = t.replace("shortdashdotdot", "3,1,1,1,1,1,").replace("shortdashdot", "3,1,1,1").replace("shortdot", "1,1,").replace("shortdash", "3,1,").replace("longdash", "8,3,").replace(/dot/g, "1,3,").replace("dash", "4,3,").replace(/,$/, "").split(",")).length; e--;) t[e] = V(t[e]) * i;
                t = t.join(",").replace(/NaN/g, "none"), this.element.setAttribute("stroke-dasharray", t)
            }
        },
        alignSetter: function (t) {
            this.element.setAttribute("text-anchor", {left: "start", center: "middle", right: "end"}[t])
        },
        opacitySetter: function (t, e, i) {
            this[e] = t, i.setAttribute(e, t)
        },
        titleSetter: function (t) {
            var e = this.element.getElementsByTagName("title")[0];
            e || (e = P.createElementNS(this.SVG_NS, "title"), this.element.appendChild(e)), e.firstChild && e.removeChild(e.firstChild), e.appendChild(P.createTextNode(String(Y(t), "").replace(/<[^>]*>/g, "")))
        },
        textSetter: function (t) {
            t !== this.textStr && (delete this.bBox, this.textStr = t, this.added && this.renderer.buildText(this))
        },
        fillSetter: function (t, e, i) {
            "string" == typeof t ? i.setAttribute(e, t) : t && this.colorGradient(t, e, i)
        },
        visibilitySetter: function (t, e, i) {
            "inherit" === t ? i.removeAttribute(e) : i.setAttribute(e, t)
        },
        zIndexSetter: function (t, e) {
            var i, n, r, o = this.renderer, s = this.parentGroup, a = (s || o).element || o.box, l = this.element;
            if (i = this.added, I(t) && (t = +(l.zIndex = t), this[e] === t && (i = !1), this[e] = t), i) {
                for ((t = this.zIndex) && s && (s.handleZ = !0), e = a.childNodes, r = 0; r < e.length && !n; r++) i = (s = e[r]).zIndex, s !== l && (V(i) > t || !I(t) && I(i) || t < 0 && !I(i) && a !== o.box) && (a.insertBefore(l, s), n = !0);
                n || a.appendChild(l)
            }
            return n
        },
        _defaultSetter: function (t, e, i) {
            i.setAttribute(e, t)
        }
    },L.prototype.yGetter = L.prototype.xGetter,L.prototype.translateXSetter = L.prototype.translateYSetter = L.prototype.rotationSetter = L.prototype.verticalAlignSetter = L.prototype.scaleXSetter = L.prototype.scaleYSetter = function (t, e) {
        this[e] = t, this.doTransform = !0
    },L.prototype["stroke-widthSetter"] = L.prototype.strokeSetter = function (t, e, i) {
        this[e] = t, this.stroke && this["stroke-width"] ? (L.prototype.fillSetter.call(this, this.stroke, "stroke", i), i.setAttribute("stroke-width", this["stroke-width"]), this.hasStroke = !0) : "stroke-width" === e && 0 === t && this.hasStroke && (i.removeAttribute("stroke"), this.hasStroke = !1)
    },(w = b.SVGRenderer = function () {
        this.init.apply(this, arguments)
    }).prototype = {
        Element: L, SVG_NS: Z, init: function (t, e, i, n, r, o) {
            var s, a;
            s = (n = this.createElement("svg").attr({
                version: "1.1",
                "class": "highcharts-root"
            }).css(this.getStyle(n))).element, t.appendChild(s), -1 === t.innerHTML.indexOf("xmlns") && M(s, "xmlns", this.SVG_NS), this.isSVG = !0, this.box = s, this.boxWrapper = n, this.alignedObjects = [], this.url = (R || q) && P.getElementsByTagName("base").length ? tt.location.href.replace(/#.*?$/, "").replace(/([\('\)])/g, "\\$1").replace(/ /g, "%20") : "", this.createElement("desc").add().element.appendChild(P.createTextNode("Created with Highcharts 5.0.2")), this.defs = this.createElement("defs").add(), this.allowHTML = o, this.forExport = r, this.gradients = {}, this.cache = {}, this.cacheKeys = [], this.imgCount = 0, this.setSize(e, i, !1), R && t.getBoundingClientRect && (this.subPixelFix = e = function () {
                E(t, {left: 0, top: 0}), a = t.getBoundingClientRect(), E(t, {
                    left: Math.ceil(a.left) - a.left + "px",
                    top: Math.ceil(a.top) - a.top + "px"
                })
            }, e(), C(tt, "resize", e))
        }, getStyle: function (t) {
            return this.style = D({
                fontFamily: '"Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif',
                fontSize: "12px"
            }, t)
        }, setStyle: function (t) {
            this.boxWrapper.css(this.getStyle(t))
        }, isHidden: function () {
            return !this.boxWrapper.getBBox().width
        }, destroy: function () {
            var t = this.defs;
            return this.box = null, this.boxWrapper = this.boxWrapper.destroy(), $(this.gradients || {}), this.gradients = null, t && (this.defs = t.destroy()), this.subPixelFix && U(tt, "resize", this.subPixelFix), this.alignedObjects = null
        }, createElement: function (t) {
            var e = new this.Element;
            return e.init(this, t), e
        }, draw: X, getRadialAttr: function (t, e) {
            return {cx: t[0] - t[2] / 2 + e.cx * t[2], cy: t[1] - t[2] / 2 + e.cy * t[2], r: e.r * t[2]}
        }, buildText: function (m) {
            for (var v, y, x, b, w = m.element, C = this, _ = C.forExport, t = Y(m.textStr, "").toString(), e = -1 !== t.indexOf("<"), i = w.childNodes, k = M(w, "x"), S = m.styles, T = m.textWidth, n = S && S.lineHeight, r = S && S.textShadow, A = S && "ellipsis" === S.textOverflow, o = i.length, s = T && !m.added && this.box, $ = function (t) {
                var e;
                return e = /(px|em)$/.test(t && t.style.fontSize) ? t.style.fontSize : S && S.fontSize || C.style.fontSize || 12, n ? V(n) : C.fontMetrics(e, t).h
            }; o--;) w.removeChild(i[o]);
            e || r || A || T || -1 !== t.indexOf(" ") ? (v = /<.*class="([^"]+)".*>/, y = /<.*style="([^"]+)".*>/, x = /<.*href="(http[^"]+)".*>/, s && s.appendChild(w), t = e ? t.replace(/<(b|strong)>/g, '<span style="font-weight:bold">').replace(/<(i|em)>/g, '<span style="font-style:italic">').replace(/<a/g, "<span").replace(/<\/(b|strong|i|em|a)>/g, "</span>").split(/<br.*?>/g) : [t], t = N(t, function (t) {
                return "" !== t
            }), F(t, function (t, p) {
                var f, g = 0;
                t = t.replace(/^\s+|\s+$/g, "").replace(/<span/g, "|||<span").replace(/<\/span>/g, "</span>|||"), f = t.split("|||"), F(f, function (t) {
                    if ("" !== t || 1 === f.length) {
                        var e, i, n = {}, r = P.createElementNS(C.SVG_NS, "tspan");
                        if (v.test(t) && (e = t.match(v)[1], M(r, "class", e)), y.test(t) && (i = t.match(y)[1].replace(/(;| |^)color([ :])/, "$1fill$2"), M(r, "style", i)), x.test(t) && !_ && (M(r, "onclick", 'location.href="' + t.match(x)[1] + '"'), E(r, {cursor: "pointer"})), " " !== (t = (t.replace(/<(.|\n)*?>/g, "") || " ").replace(/&lt;/g, "<").replace(/&gt;/g, ">"))) {
                            if (r.appendChild(P.createTextNode(t)), g ? n.dx = 0 : p && null !== k && (n.x = k), M(r, n), w.appendChild(r), !g && p && (!K && _ && E(r, {display: "block"}), M(r, "dy", $(r))), T) {
                                n = t.replace(/([^\^])-/g, "$1- ").split(" "), e = "nowrap" === S.whiteSpace;
                                for (var o, s, a = 1 < f.length || p || 1 < n.length && !e, l = [], c = $(r), h = m.rotation, d = t, u = d.length; (a || A) && (n.length || l.length);) m.rotation = 0, s = (o = m.getBBox(!0)).width, !K && C.forExport && (s = C.measureSpanWidth(r.firstChild.data, m.styles)), o = T < s, void 0 === b && (b = o), A && b ? (u /= 2, "" === d || !o && u < .5 ? n = [] : (n = [(d = t.substring(0, d.length + (o ? -1 : 1) * Math.ceil(u))) + (3 < T ? "\u2026" : "")], r.removeChild(r.firstChild))) : o && 1 !== n.length ? (r.removeChild(r.firstChild), l.unshift(n.pop())) : (n = l, l = [], n.length && !e && (r = P.createElementNS(Z, "tspan"), M(r, {
                                    dy: c,
                                    x: k
                                }), i && M(r, "style", i), w.appendChild(r)), T < s && (T = s)), n.length && r.appendChild(P.createTextNode(n.join(" ").replace(/- /g, "-")));
                                m.rotation = h
                            }
                            g++
                        }
                    }
                })
            }), b && m.attr("title", m.textStr), s && s.removeChild(w), r && m.applyTextShadow && m.applyTextShadow(r)) : w.appendChild(P.createTextNode(t.replace(/&lt;/g, "<").replace(/&gt;/g, ">")))
        }, getContrast: function (t) {
            return 510 < (t = S(t).rgba)[0] + t[1] + t[2] ? "#000000" : "#FFFFFF"
        }, button: function (t, e, i, n, r, o, s, a, l) {
            var c, h, d, u, p = this.label(t, e, i, l, null, null, null, null, "button"), f = 0;
            return p.attr(G({padding: 8, r: 2}, r)), r = G({
                fill: "#f7f7f7",
                stroke: "#cccccc",
                "stroke-width": 1,
                style: {color: "#333333", cursor: "pointer", fontWeight: "normal"}
            }, r), c = r.style, delete r.style, o = G(r, {fill: "#e6e6e6"}, o), h = o.style, delete o.style, s = G(r, {
                fill: "#e6ebf5",
                style: {color: "#000000", fontWeight: "bold"}
            }, s), d = s.style, delete s.style, a = G(r, {style: {color: "#cccccc"}}, a), u = a.style, delete a.style, C(p.element, B ? "mouseover" : "mouseenter", function () {
                3 !== f && p.setState(1)
            }), C(p.element, B ? "mouseout" : "mouseleave", function () {
                3 !== f && p.setState(f)
            }), p.setState = function (t) {
                1 !== t && (p.state = f = t), p.removeClass(/highcharts-button-(normal|hover|pressed|disabled)/).addClass("highcharts-button-" + ["normal", "hover", "pressed", "disabled"][t || 0]), p.attr([r, o, s, a][t || 0]).css([c, h, d, u][t || 0])
            }, p.attr(r).css(D({cursor: "default"}, c)), p.on("click", function (t) {
                3 !== f && n.call(p, t)
            })
        }, crispLine: function (t, e) {
            return t[1] === t[4] && (t[1] = t[4] = Math.round(t[1]) - e % 2 / 2), t[2] === t[5] && (t[2] = t[5] = Math.round(t[2]) + e % 2 / 2), t
        }, path: function (t) {
            var e = {fill: "none"};
            return z(t) ? e.d = t : H(t) && D(e, t), this.createElement("path").attr(e)
        }, circle: function (t, e, i) {
            return t = H(t) ? t : {
                x: t,
                y: e,
                r: i
            }, (e = this.createElement("circle")).xSetter = e.ySetter = function (t, e, i) {
                i.setAttribute("c" + e, t)
            }, e.attr(t)
        }, arc: function (t, e, i, n, r, o) {
            return H(t) && (e = t.y, i = t.r, n = t.innerR, r = t.start, o = t.end, t = t.x), (t = this.symbol("arc", t || 0, e || 0, i || 0, i || 0, {
                innerR: n || 0,
                start: r || 0,
                end: o || 0
            })).r = i, t
        }, rect: function (t, e, i, n, r, o) {
            r = H(t) ? t.r : r;
            var s = this.createElement("rect");
            return t = H(t) ? t : void 0 === t ? {} : {
                x: t,
                y: e,
                width: Math.max(i, 0),
                height: Math.max(n, 0)
            }, void 0 !== o && (t.strokeWidth = o, t = s.crisp(t)), t.fill = "none", r && (t.r = r), s.rSetter = function (t, e, i) {
                M(i, {rx: t, ry: t})
            }, s.attr(t)
        }, setSize: function (t, e, i) {
            var n = this.alignedObjects, r = n.length;
            for (this.width = t, this.height = e, this.boxWrapper.animate({width: t, height: e}, {
                step: function () {
                    this.attr({viewBox: "0 0 " + this.attr("width") + " " + this.attr("height")})
                }, duration: Y(i, !0) ? void 0 : 0
            }); r--;) n[r].align()
        }, g: function (t) {
            var e = this.createElement("g");
            return t ? e.attr({"class": "highcharts-" + t}) : e
        }, image: function (t, e, i, n, r) {
            var o = {preserveAspectRatio: "none"};
            return 1 < arguments.length && D(o, {
                x: e,
                y: i,
                width: n,
                height: r
            }), (o = this.createElement("image").attr(o)).element.setAttributeNS ? o.element.setAttributeNS("http://www.w3.org/1999/xlink", "href", t) : o.element.setAttribute("hc-svg-href", t), o
        }, symbol: function (t, e, i, n, r, o) {
            var s, a, l, c = this, h = this.symbols[t], d = I(e) && h && h(Math.round(e), Math.round(i), n, r, o),
                u = /^url\((.*?)\)$/;
            return h ? ((s = this.path(d)).attr("fill", "none"), D(s, {
                symbolName: t,
                x: e,
                y: i,
                width: n,
                height: r
            }), o && D(s, o)) : u.test(t) && (a = t.match(u)[1], (s = this.image(a)).imgwidth = Y(J[a] && J[a].width, o && o.width), s.imgheight = Y(J[a] && J[a].height, o && o.height), l = function () {
                s.attr({width: s.width, height: s.height})
            }, F(["width", "height"], function (t) {
                s[t + "Setter"] = function (t, e) {
                    var i = {}, n = this["img" + e], r = "width" === e ? "translateX" : "translateY";
                    this[e] = t, I(n) && (this.element && this.element.setAttribute(e, n), this.alignByTranslate || (i[r] = ((this[e] || 0) - n) / 2, this.attr(i)))
                }
            }), I(e) && s.attr({x: e, y: i}), s.isImg = !0, I(s.imgwidth) && I(s.imgheight) ? l() : (s.attr({
                width: 0,
                height: 0
            }), T("img", {
                onload: function () {
                    var t = k[c.chartIndex];
                    0 === this.width && (E(this, {
                        position: "absolute",
                        top: "-999em"
                    }), P.body.appendChild(this)), J[a] = {
                        width: this.width,
                        height: this.height
                    }, s.imgwidth = this.width, s.imgheight = this.height, s.element && l(), this.parentNode && this.parentNode.removeChild(this), c.imgCount--, !c.imgCount && t && t.onload && t.onload()
                }, src: a
            }), this.imgCount++)), s
        }, symbols: {
            circle: function (t, e, i, n) {
                var r = .166 * i;
                return ["M", t + i / 2, e, "C", t + i + r, e, t + i + r, e + n, t + i / 2, e + n, "C", t - r, e + n, t - r, e, t + i / 2, e, "Z"]
            }, square: function (t, e, i, n) {
                return ["M", t, e, "L", t + i, e, t + i, e + n, t, e + n, "Z"]
            }, triangle: function (t, e, i, n) {
                return ["M", t + i / 2, e, "L", t + i, e + n, t, e + n, "Z"]
            }, "triangle-down": function (t, e, i, n) {
                return ["M", t, e, "L", t + i, e, t + i / 2, e + n, "Z"]
            }, diamond: function (t, e, i, n) {
                return ["M", t + i / 2, e, "L", t + i, e + n / 2, t + i / 2, e + n, t, e + n / 2, "Z"]
            }, arc: function (t, e, i, n, r) {
                var o = r.start;
                i = r.r || i || n;
                var s = r.end - .001;
                n = r.innerR;
                var a = r.open, l = Math.cos(o), c = Math.sin(o), h = Math.cos(s);
                return s = Math.sin(s), ["M", t + i * l, e + i * c, "A", i, i, 0, r = r.end - o < Math.PI ? 0 : 1, 1, t + i * h, e + i * s, a ? "M" : "L", t + n * h, e + n * s, "A", n, n, 0, r, 0, t + n * l, e + n * c, a ? "" : "Z"]
            }, callout: function (t, e, i, n, r) {
                var o, s = Math.min(r && r.r || 0, i, n), a = s + 6, l = r && r.anchorX;
                return r = r && r.anchorY, o = ["M", t + s, e, "L", t + i - s, e, "C", t + i, e, t + i, e, t + i, e + s, "L", t + i, e + n - s, "C", t + i, e + n, t + i, e + n, t + i - s, e + n, "L", t + s, e + n, "C", t, e + n, t, e + n, t, e + n - s, "L", t, e + s, "C", t, e, t, e, t + s, e], l && i < l && e + a < r && r < e + n - a ? o.splice(13, 3, "L", t + i, r - 6, t + i + 6, r, t + i, r + 6, t + i, e + n - s) : l && l < 0 && e + a < r && r < e + n - a ? o.splice(33, 3, "L", t, r + 6, t - 6, r, t, r - 6, t, e + s) : r && n < r && t + a < l && l < t + i - a ? o.splice(23, 3, "L", l + 6, e + n, l, e + n + 6, l - 6, e + n, t + s, e + n) : r && r < 0 && t + a < l && l < t + i - a && o.splice(3, 3, "L", l - 6, e, l, e - 6, l + 6, e, i - s, e), o
            }
        }, clipRect: function (t, e, i, n) {
            var r = "highcharts-" + b.idCounter++, o = this.createElement("clipPath").attr({id: r}).add(this.defs);
            return (t = this.rect(t, e, i, n, 0).add(o)).id = r, t.clipPath = o, t.count = 0, t
        }, text: function (t, e, i, n) {
            var r = !K && this.forExport, o = {};
            return !n || !this.allowHTML && this.forExport ? (o.x = Math.round(e || 0), i && (o.y = Math.round(i)), (t || 0 === t) && (o.text = t), t = this.createElement("text").attr(o), r && t.css({position: "absolute"}), n || (t.xSetter = function (t, e, i) {
                var n, r, o = i.getElementsByTagName("tspan"), s = i.getAttribute(e);
                for (r = 0; r < o.length; r++) (n = o[r]).getAttribute(e) === s && n.setAttribute(e, t);
                i.setAttribute(e, t)
            }), t) : this.html(t, e, i)
        }, fontMetrics: function (t, e) {
            return t = t || this.style && this.style.fontSize, {
                h: e = (t = /px/.test(t) ? V(t) : /em/.test(t) ? 12 * parseFloat(t) : 12) < 24 ? t + 3 : Math.round(1.2 * t),
                b: Math.round(.8 * e),
                f: t
            }
        }, rotCorr: function (t, e, i) {
            var n = t;
            return e && i && (n = Math.max(n * Math.cos(e * A), 4)), {x: -t / 3 * Math.sin(e * A), y: n}
        }, label: function (t, e, i, n, r, o, s, a, l) {
            var c, h, d, u, p, f, g, m, v, y, x, b, w, C = this, _ = C.g("button" !== l && "label"),
                k = _.text = C.text("", 0, 0, s).attr({zIndex: 1}), S = 0, T = 3, A = 0, $ = {},
                M = /^url\((.*?)\)$/.test(n), E = M;
            l && _.addClass("highcharts-" + l), E = M, y = function () {
                return (m || 0) % 2 / 2
            }, x = function () {
                var t = k.element.style, e = {};
                h = (void 0 === d || void 0 === u || g) && I(k.textStr) && k.getBBox(), _.width = (d || h.width || 0) + 2 * T + A, _.height = (u || h.height || 0) + 2 * T, v = T + C.fontMetrics(t && t.fontSize, k).b, E && (c || (_.box = c = C.symbols[n] || M ? C.symbol(n) : C.rect(), c.addClass(("button" === l ? "" : "highcharts-label-box") + (l ? " highcharts-" + l + "-box" : "")), c.add(_), t = y(), e.x = t, e.y = (a ? -v : 0) + t), e.width = Math.round(_.width), e.height = Math.round(_.height), c.attr(D(e, $)), $ = {})
            }, b = function () {
                var t, e = A + T;
                t = a ? 0 : v, I(d) && h && ("center" === g || "right" === g) && (e += {
                    center: .5,
                    right: 1
                }[g] * (d - h.width)), e === k.x && t === k.y || (k.attr("x", e), void 0 !== t && k.attr("y", t)), k.x = e, k.y = t
            }, w = function (t, e) {
                c ? c.attr(t, e) : $[t] = e
            }, _.onAdd = function () {
                k.add(_), _.attr({text: t || 0 === t ? t : "", x: e, y: i}), c && I(r) && _.attr({
                    anchorX: r,
                    anchorY: o
                })
            }, _.widthSetter = function (t) {
                d = t
            }, _.heightSetter = function (t) {
                u = t
            }, _["text-alignSetter"] = function (t) {
                g = t
            }, _.paddingSetter = function (t) {
                I(t) && t !== T && (T = _.padding = t, b())
            }, _.paddingLeftSetter = function (t) {
                I(t) && t !== A && (A = t, b())
            }, _.alignSetter = function (t) {
                (t = {left: 0, center: .5, right: 1}[t]) !== S && (S = t, h && _.attr({x: p}))
            }, _.textSetter = function (t) {
                void 0 !== t && k.textSetter(t), x(), b()
            }, _["stroke-widthSetter"] = function (t, e) {
                t && (E = !0), m = this["stroke-width"] = t, w(e, t)
            }, _.strokeSetter = _.fillSetter = _.rSetter = function (t, e) {
                "fill" === e && t && (E = !0), w(e, t)
            }, _.anchorXSetter = function (t, e) {
                r = t, w(e, Math.round(t) - y() - p)
            }, _.anchorYSetter = function (t, e) {
                w(e, (o = t) - f)
            }, _.xSetter = function (t) {
                _.x = t, S && (t -= S * ((d || h.width) + 2 * T)), p = Math.round(t), _.attr("translateX", p)
            }, _.ySetter = function (t) {
                f = _.y = Math.round(t), _.attr("translateY", f)
            };
            var P = _.css;
            return D(_, {
                css: function (e) {
                    if (e) {
                        var i = {};
                        e = G(e), F(_.textProps, function (t) {
                            void 0 !== e[t] && (i[t] = e[t], delete e[t])
                        }), k.css(i)
                    }
                    return P.call(_, e)
                }, getBBox: function () {
                    return {width: h.width + 2 * T, height: h.height + 2 * T, x: h.x - T, y: h.y - T}
                }, shadow: function (t) {
                    return t && (x(), c && c.shadow(t)), _
                }, destroy: function () {
                    U(_.element, "mouseenter"), U(_.element, "mouseleave"), k && (k = k.destroy()), c && (c = c.destroy()), L.prototype.destroy.call(_), _ = C = x = b = w = null
                }
            })
        }
    },b.Renderer = w,it = (et = t).attr,nt = et.createElement,rt = et.css,ot = et.defined,st = et.each,at = et.extend,lt = et.isFirefox,ct = et.isMS,ht = et.isWebKit,dt = et.pInt,ut = et.SVGRenderer,pt = et.win,ft = et.wrap,at(et.SVGElement.prototype, {
        htmlCss: function (t) {
            var e = this.element;
            return (e = t && "SPAN" === e.tagName && t.width) && (delete t.width, this.textWidth = e, this.updateTransform()), t && "ellipsis" === t.textOverflow && (t.whiteSpace = "nowrap", t.overflow = "hidden"), this.styles = at(this.styles, t), rt(this.element, t), this
        }, htmlGetBBox: function () {
            var t = this.element;
            return "text" === t.nodeName && (t.style.position = "absolute"), {
                x: t.offsetLeft,
                y: t.offsetTop,
                width: t.offsetWidth,
                height: t.offsetHeight
            }
        }, htmlUpdateTransform: function () {
            if (this.added) {
                var e = this.renderer, i = this.element, n = this.translateX || 0, r = this.translateY || 0,
                    t = this.x || 0, o = this.y || 0, s = this.textAlign || "left",
                    a = {left: 0, center: .5, right: 1}[s], l = this.styles;
                if (rt(i, {marginLeft: n, marginTop: r}), this.shadows && st(this.shadows, function (t) {
                    rt(t, {marginLeft: n + 1, marginTop: r + 1})
                }), this.inverted && st(i.childNodes, function (t) {
                    e.invertChild(t, i)
                }), "SPAN" === i.tagName) {
                    var c = this.rotation, h = dt(this.textWidth), d = l && l.whiteSpace,
                        u = [c, s, i.innerHTML, this.textWidth, this.textAlign].join();
                    u !== this.cTT && (l = e.fontMetrics(i.style.fontSize).b, ot(c) && this.setSpanRotation(c, a, l), rt(i, {
                        width: "",
                        whiteSpace: d || "nowrap"
                    }), i.offsetWidth > h && /[ \-]/.test(i.textContent || i.innerText) && rt(i, {
                        width: h + "px",
                        display: "block",
                        whiteSpace: d || "normal"
                    }), this.getSpanCorrection(i.offsetWidth, l, a, c, s)), rt(i, {
                        left: t + (this.xCorr || 0) + "px",
                        top: o + (this.yCorr || 0) + "px"
                    }), ht && (l = i.offsetHeight), this.cTT = u
                }
            } else this.alignOnAdd = !0
        }, setSpanRotation: function (t, e, i) {
            var n = {},
                r = ct ? "-ms-transform" : ht ? "-webkit-transform" : lt ? "MozTransform" : pt.opera ? "-o-transform" : "";
            n[r] = n.transform = "rotate(" + t + "deg)", n[r + (lt ? "Origin" : "-origin")] = n.transformOrigin = 100 * e + "% " + i + "px", rt(this.element, n)
        }, getSpanCorrection: function (t, e, i) {
            this.xCorr = -t * i, this.yCorr = -e
        }
    }),at(ut.prototype, {
        html: function (t, e, i) {
            var n = this.createElement("span"), o = n.element, s = n.renderer, r = s.isSVG, a = function (e, r) {
                st(["opacity", "visibility"], function (t) {
                    ft(e, t + "Setter", function (t, e, i, n) {
                        t.call(this, e, i, n), r[i] = e
                    })
                })
            };
            return n.textSetter = function (t) {
                t !== o.innerHTML && delete this.bBox, o.innerHTML = this.textStr = t, n.htmlUpdateTransform()
            }, r && a(n, n.element.style), n.xSetter = n.ySetter = n.alignSetter = n.rotationSetter = function (t, e) {
                "align" === e && (e = "textAlign"), n[e] = t, n.htmlUpdateTransform()
            }, n.attr({text: t, x: Math.round(e), y: Math.round(i)}).css({
                fontFamily: this.style.fontFamily,
                fontSize: this.style.fontSize,
                position: "absolute"
            }), o.style.whiteSpace = "nowrap", n.css = n.htmlCss, r && (n.add = function (t) {
                var e, r = s.box.parentNode, i = [];
                if (this.parentGroup = t) {
                    if (!(e = t.div)) {
                        for (; t;) i.push(t), t = t.parentGroup;
                        st(i.reverse(), function (i) {
                            var n, t = it(i.element, "class");
                            t && (t = {className: t}), e = i.div = i.div || nt("div", t, {
                                position: "absolute",
                                left: (i.translateX || 0) + "px",
                                top: (i.translateY || 0) + "px",
                                display: i.display,
                                opacity: i.opacity,
                                pointerEvents: i.styles && i.styles.pointerEvents
                            }, e || r), n = e.style, at(i, {
                                translateXSetter: function (t, e) {
                                    n.left = t + "px", i[e] = t, i.doTransform = !0
                                }, translateYSetter: function (t, e) {
                                    n.top = t + "px", i[e] = t, i.doTransform = !0
                                }
                            }), a(i, n)
                        })
                    }
                } else e = r;
                return e.appendChild(o), n.added = !0, n.alignOnAdd && n.htmlUpdateTransform(), n
            }), n
        }
    }),function ($) {
        var t, e, M = $.createElement, c = $.css, o = $.defined, h = $.deg2rad, n = $.discardElement, s = $.doc,
            E = $.each, r = $.erase, a = $.extend;
        t = $.extendClass;
        var i = $.isArray, l = $.isNumber, d = $.isObject, u = $.merge;
        e = $.noop;
        var g = $.pick, m = $.pInt, p = $.SVGElement, f = $.SVGRenderer, v = $.win;
        $.svg || ((e = {
            docMode8: s && 8 === s.documentMode, init: function (t, e) {
                var i = ["<", e, ' filled="f" stroked="f"'], n = ["position: ", "absolute", ";"], r = "div" === e;
                ("shape" === e || r) && n.push("left:0;top:0;width:1px;height:1px;"), n.push("visibility: ", r ? "hidden" : "visible"), i.push(' style="', n.join(""), '"/>'), e && (i = r || "span" === e || "img" === e ? i.join("") : t.prepVML(i), this.element = M(i)), this.renderer = t
            }, add: function (t) {
                var e = this.renderer, i = this.element, n = e.box, r = t && t.inverted;
                n = t ? t.element || t : n;
                return t && (this.parentGroup = t), r && e.invertChild(i, n), n.appendChild(i), this.added = !0, this.alignOnAdd && !this.deferUpdateTransform && this.updateTransform(), this.onAdd && this.onAdd(), this.className && this.attr("class", this.className), this
            }, updateTransform: p.prototype.htmlUpdateTransform, setSpanRotation: function () {
                var t = this.rotation, e = Math.cos(t * h), i = Math.sin(t * h);
                c(this.element, {filter: t ? ["progid:DXImageTransform.Microsoft.Matrix(M11=", e, ", M12=", -i, ", M21=", i, ", M22=", e, ", sizingMethod='auto expand')"].join("") : "none"})
            }, getSpanCorrection: function (t, e, i, n, r) {
                var o, s = n ? Math.cos(n * h) : 1, a = n ? Math.sin(n * h) : 0,
                    l = g(this.elemHeight, this.element.offsetHeight);
                this.xCorr = s < 0 && -t, this.yCorr = a < 0 && -l, o = s * a < 0, this.xCorr += a * e * (o ? 1 - i : i), this.yCorr -= s * e * (n ? o ? i : 1 - i : 1), r && "left" !== r && (this.xCorr -= t * i * (s < 0 ? -1 : 1), n && (this.yCorr -= l * i * (a < 0 ? -1 : 1)), c(this.element, {textAlign: r}))
            }, pathToVML: function (t) {
                for (var e = t.length, i = []; e--;) l(t[e]) ? i[e] = Math.round(10 * t[e]) - 5 : "Z" === t[e] ? i[e] = "x" : (i[e] = t[e], !t.isArc || "wa" !== t[e] && "at" !== t[e] || (i[e + 5] === i[e + 7] && (i[e + 7] += t[e + 7] > t[e + 5] ? 1 : -1), i[e + 6] === i[e + 8] && (i[e + 8] += t[e + 8] > t[e + 6] ? 1 : -1)));
                return i.join(" ") || "x"
            }, clip: function (t) {
                var e, i = this;
                return t ? (e = t.members, r(e, i), e.push(i), i.destroyClip = function () {
                    r(e, i)
                }, t = t.getCSS(i)) : (i.destroyClip && i.destroyClip(), t = {clip: i.docMode8 ? "inherit" : "rect(auto)"}), i.css(t)
            }, css: p.prototype.htmlCss, safeRemoveChild: function (t) {
                t.parentNode && n(t)
            }, destroy: function () {
                return this.destroyClip && this.destroyClip(), p.prototype.destroy.apply(this)
            }, on: function (t, e) {
                return this.element["on" + t] = function () {
                    var t = v.event;
                    t.target = t.srcElement, e(t)
                }, this
            }, cutOffPath: function (t, e) {
                var i;
                return 9 !== (i = (t = t.split(/[ ,]/)).length) && 11 !== i || (t[i - 4] = t[i - 2] = m(t[i - 2]) - 10 * e), t.join(" ")
            }, shadow: function (t, e, i) {
                var n, r, o, s, a, l, c, h = [], d = this.element, u = this.renderer, p = d.style, f = d.path;
                if (f && "string" != typeof f.value && (f = "x"), a = f, t) {
                    for (l = g(t.width, 3), c = (t.opacity || .15) / l, n = 1; n <= 3; n++) s = 2 * l + 1 - 2 * n, i && (a = this.cutOffPath(f.value, s + .5)), o = ['<shape isShadow="true" strokeweight="', s, '" filled="false" path="', a, '" coordsize="10 10" style="', d.style.cssText, '" />'], r = M(u.prepVML(o), null, {
                        left: m(p.left) + g(t.offsetX, 1),
                        top: m(p.top) + g(t.offsetY, 1)
                    }), i && (r.cutOff = s + 1), o = ['<stroke color="', t.color || "#000000", '" opacity="', c * n, '"/>'], M(u.prepVML(o), null, null, r), e ? e.element.appendChild(r) : d.parentNode.insertBefore(r, d), h.push(r);
                    this.shadows = h
                }
                return this
            }, updateShadows: e, setAttr: function (t, e) {
                this.docMode8 ? this.element[t] = e : this.element.setAttribute(t, e)
            }, classSetter: function (t) {
                (this.added ? this.element : this).className = t
            }, dashstyleSetter: function (t, e, i) {
                (i.getElementsByTagName("stroke")[0] || M(this.renderer.prepVML(["<stroke/>"]), null, null, i))[e] = t || "solid", this[e] = t
            }, dSetter: function (t, e, i) {
                var n = this.shadows;
                if (t = t || [], this.d = t.join && t.join(" "), i.path = t = this.pathToVML(t), n) for (i = n.length; i--;) n[i].path = n[i].cutOff ? this.cutOffPath(t, n[i].cutOff) : t;
                this.setAttr(e, t)
            }, fillSetter: function (t, e, i) {
                var n = i.nodeName;
                "SPAN" === n ? i.style.color = t : "IMG" !== n && (i.filled = "none" !== t, this.setAttr("fillcolor", this.renderer.color(t, i, e, this)))
            }, "fill-opacitySetter": function (t, e, i) {
                M(this.renderer.prepVML(["<", e.split("-")[0], ' opacity="', t, '"/>']), null, null, i)
            }, opacitySetter: e, rotationSetter: function (t, e, i) {
                i = i.style, this[e] = i[e] = t, i.left = -Math.round(Math.sin(t * h) + 1) + "px", i.top = Math.round(Math.cos(t * h)) + "px"
            }, strokeSetter: function (t, e, i) {
                this.setAttr("strokecolor", this.renderer.color(t, i, e, this))
            }, "stroke-widthSetter": function (t, e, i) {
                i.stroked = !!t, this[e] = t, l(t) && (t += "px"), this.setAttr("strokeweight", t)
            }, titleSetter: function (t, e) {
                this.setAttr(e, t)
            }, visibilitySetter: function (e, i, t) {
                "inherit" === e && (e = "visible"), this.shadows && E(this.shadows, function (t) {
                    t.style[i] = e
                }), "DIV" === t.nodeName && (e = "hidden" === e ? "-999em" : 0, this.docMode8 || (t.style[i] = e ? "visible" : "hidden"), i = "top"), t.style[i] = e
            }, xSetter: function (t, e, i) {
                this[e] = t, "x" === e ? e = "left" : "y" === e && (e = "top"), this.updateClipping ? (this[e] = t, this.updateClipping()) : i.style[e] = t
            }, zIndexSetter: function (t, e, i) {
                i.style[e] = t
            }
        })["stroke-opacitySetter"] = e["fill-opacitySetter"], $.VMLElement = e = t(p, e), e.prototype.ySetter = e.prototype.widthSetter = e.prototype.heightSetter = e.prototype.xSetter, e = {
            Element: e, isIE8: -1 < v.navigator.userAgent.indexOf("MSIE 8.0"), init: function (t, e, i) {
                var n, r;
                if (this.alignedObjects = [], r = (n = this.createElement("div").css({position: "relative"})).element, t.appendChild(n.element), this.isVML = !0, this.box = r, this.boxWrapper = n, this.gradients = {}, this.cache = {}, this.cacheKeys = [], this.imgCount = 0, this.setSize(e, i, !1), !s.namespaces.hcv) {
                    s.namespaces.add("hcv", "urn:schemas-microsoft-com:vml");
                    try {
                        s.createStyleSheet().cssText = "hcv\\:fill, hcv\\:path, hcv\\:shape, hcv\\:stroke{ behavior:url(#default#VML); display: inline-block; } "
                    } catch (o) {
                        s.styleSheets[0].cssText += "hcv\\:fill, hcv\\:path, hcv\\:shape, hcv\\:stroke{ behavior:url(#default#VML); display: inline-block; } "
                    }
                }
            }, isHidden: function () {
                return !this.box.offsetWidth
            }, clipRect: function (t, e, i, n) {
                var r = this.createElement(), o = d(t);
                return a(r, {
                    members: [],
                    count: 0,
                    left: (
                        o ? t.x : t) + 1,
                    top: (o ? t.y : e) + 1,
                    width: (o ? t.width : i) - 1,
                    height: (o ? t.height : n) - 1,
                    getCSS: function (t) {
                        var e = (o = t.element).nodeName, i = t.inverted,
                            n = this.top - ("shape" === e ? o.offsetTop : 0), r = this.left, o = r + this.width,
                            s = n + this.height;
                        n = {clip: "rect(" + Math.round(i ? r : n) + "px," + Math.round(i ? s : o) + "px," + Math.round(i ? o : s) + "px," + Math.round(i ? n : r) + "px)"};
                        return !i && t.docMode8 && "DIV" === e && a(n, {width: o + "px", height: s + "px"}), n
                    },
                    updateClipping: function () {
                        E(r.members, function (t) {
                            t.element && t.css(r.getCSS(t))
                        })
                    }
                })
            }, color: function (t, e, i, n) {
                var r, o, s, a = this, l = /^rgba/, c = "none";
                if (t && t.linearGradient ? s = "gradient" : t && t.radialGradient && (s = "pattern"), s) {
                    var h, d, u, p, f, g, m, v = t.linearGradient || t.radialGradient, y = "";
                    t = t.stops;
                    var x, b = [], w = function () {
                        o = ['<fill colors="' + b.join(",") + '" opacity="', f, '" o:opacity2="', p, '" type="', s, '" ', y, 'focus="100%" method="any" />'], M(a.prepVML(o), null, null, e)
                    };
                    if (u = t[0], x = t[t.length - 1], 0 < u[0] && t.unshift([0, u[1]]), x[0] < 1 && t.push([1, x[1]]), E(t, function (t, e) {
                        l.test(t[1]) ? (r = $.color(t[1]), h = r.get("rgb"), d = r.get("a")) : (h = t[1], d = 1), b.push(100 * t[0] + "% " + h), e ? (f = d, g = h) : (p = d, m = h)
                    }), "fill" === i) if ("gradient" === s) i = v.x1 || v[0] || 0, t = v.y1 || v[1] || 0, u = v.x2 || v[2] || 0, v = v.y2 || v[3] || 0, y = 'angle="' + (90 - 180 * Math.atan((v - t) / (u - i)) / Math.PI) + '"', w(); else {
                        var C, _ = 2 * (c = v.r), k = 2 * c, S = v.cx, T = v.cy, A = e.radialReference;
                        c = function () {
                            A && (C = n.getBBox(), S += (A[0] - C.x) / C.width - .5, T += (A[1] - C.y) / C.height - .5, _ *= A[2] / C.width, k *= A[2] / C.height), y = 'src="' + $.getOptions().global.VMLRadialGradientURL + '" size="' + _ + "," + k + '" origin="0.5,0.5" position="' + S + "," + T + '" color2="' + m + '" ', w()
                        };
                        n.added ? c() : n.onAdd = c, c = g
                    } else c = h
                } else l.test(t) && "IMG" !== e.tagName ? (r = $.color(t), n[i + "-opacitySetter"](r.get("a"), i, e), c = r.get("rgb")) : ((c = e.getElementsByTagName(i)).length && (c[0].opacity = 1, c[0].type = "solid"), c = t);
                return c
            }, prepVML: function (t) {
                var e = this.isIE8;
                return t = t.join(""), e ? t = -1 === (t = t.replace("/>", ' xmlns="urn:schemas-microsoft-com:vml" />')).indexOf('style="') ? t.replace("/>", ' style="display:inline-block;behavior:url(#default#VML);" />') : t.replace('style="', 'style="display:inline-block;behavior:url(#default#VML);') : t = t.replace("<", "<hcv:"), t
            }, text: f.prototype.html, path: function (t) {
                var e = {coordsize: "10 10"};
                return i(t) ? e.d = t : d(t) && a(e, t), this.createElement("shape").attr(e)
            }, circle: function (t, e, i) {
                var n = this.symbol("circle");
                return d(t) && (i = t.r, e = t.y, t = t.x), n.isCircle = !0, n.r = i, n.attr({x: t, y: e})
            }, g: function (t) {
                var e;
                return t && (e = {
                    className: "highcharts-" + t,
                    "class": "highcharts-" + t
                }), this.createElement("div").attr(e)
            }, image: function (t, e, i, n, r) {
                var o = this.createElement("img").attr({src: t});
                return 1 < arguments.length && o.attr({x: e, y: i, width: n, height: r}), o
            }, createElement: function (t) {
                return "rect" === t ? this.symbol(t) : f.prototype.createElement.call(this, t)
            }, invertChild: function (e, t) {
                var i = this;
                t = t.style;
                var n = "IMG" === e.tagName && e.style;
                c(e, {
                    flip: "x",
                    left: m(t.width) - (n ? m(n.top) : 1),
                    top: m(t.height) - (n ? m(n.left) : 1),
                    rotation: -90
                }), E(e.childNodes, function (t) {
                    i.invertChild(t, e)
                })
            }, symbols: {
                arc: function (t, e, i, n, r) {
                    var o = r.start, s = r.end, a = r.r || i || n;
                    i = r.innerR, n = Math.cos(o);
                    var l = Math.sin(o), c = Math.cos(s), h = Math.sin(s);
                    return 0 == s - o ? ["x"] : (o = ["wa", t - a, e - a, t + a, e + a, t + a * n, e + a * l, t + a * c, e + a * h], r.open && !i && o.push("e", "M", t, e), o.push("at", t - i, e - i, t + i, e + i, t + i * c, e + i * h, t + i * n, e + i * l, "x", "e"), o.isArc = !0, o)
                }, circle: function (t, e, i, n, r) {
                    return r && o(r.r) && (i = n = 2 * r.r), r && r.isCircle && (t -= i / 2, e -= n / 2), ["wa", t, e, t + i, e + n, t + i, e + n / 2, t + i, e + n / 2, "e"]
                }, rect: function (t, e, i, n, r) {
                    return f.prototype.symbols[o(r) && r.r ? "callout" : "square"].call(0, t, e, i, n, r)
                }
            }
        }, $.VMLRenderer = t = function () {
            this.init.apply(this, arguments)
        }, t.prototype = u(f.prototype, e), $.Renderer = t), f.prototype.measureSpanWidth = function (t, e) {
            var i = s.createElement("span");
            return t = s.createTextNode(t), i.appendChild(t), c(i, e), this.box.appendChild(i), e = i.offsetWidth, n(i), e
        }
    }(t),function (n) {
        function e() {
            var a, t = n.defaultOptions.global, l = t.useUTC, e = l ? "getUTC" : "get", i = l ? "setUTC" : "set";
            n.Date = a = t.Date || o.Date, a.hcTimezoneOffset = l && t.timezoneOffset, a.hcGetTimezoneOffset = l && t.getTimezoneOffset, a.hcMakeTime = function (t, e, i, n, r, o) {
                var s;
                return l ? (s = a.UTC.apply(0, arguments), s += c(s)) : s = new a(t, e, h(i, 1), h(n, 0), h(r, 0), h(o, 0)).getTime(), s
            }, r("Minutes Hours Day Date Month FullYear".split(" "), function (t) {
                a["hcGet" + t] = e + t
            }), r("Milliseconds Seconds Minutes Hours Date Month FullYear".split(" "), function (t) {
                a["hcSet" + t] = i + t
            })
        }

        var t = n.color, r = n.each, c = n.getTZOffset, i = n.merge, h = n.pick, o = n.win;
        n.defaultOptions = {
            colors: "#7cb5ec #434348 #90ed7d #f7a35c #8085e9 #f15c80 #e4d354 #2b908f #f45b5b #91e8e1".split(" "),
            symbols: ["circle", "diamond", "square", "triangle", "triangle-down"],
            lang: {
                loading: "Loading...",
                months: "January February March April May June July August September October November December".split(" "),
                shortMonths: "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),
                weekdays: "Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),
                decimalPoint: ".",
                numericSymbols: "kMGTPE".split(""),
                resetZoom: "Reset zoom",
                resetZoomTitle: "Reset zoom level 1:1",
                thousandsSep: " "
            },
            global: {useUTC: !0, VMLRadialGradientURL: "http://code.highcharts.com/5.0.2/gfx/vml-radial-gradient.png"},
            chart: {
                borderRadius: 0,
                defaultSeriesType: "line",
                ignoreHiddenSeries: !0,
                spacing: [10, 10, 15, 10],
                resetZoomButton: {theme: {zIndex: 20}, position: {align: "right", x: -10, y: 10}},
                width: null,
                height: null,
                borderColor: "#335cad",
                backgroundColor: "#ffffff",
                plotBorderColor: "#cccccc"
            },
            title: {
                text: "Chart title",
                align: "center",
                margin: 15,
                style: {color: "#333333", fontSize: "18px"},
                widthAdjust: -44
            },
            subtitle: {text: "", align: "center", style: {color: "#666666"}, widthAdjust: -44},
            plotOptions: {},
            labels: {style: {position: "absolute", color: "#333333"}},
            legend: {
                enabled: !0,
                align: "center",
                layout: "horizontal",
                labelFormatter: function () {
                    return this.name
                },
                borderColor: "#999999",
                borderRadius: 0,
                navigation: {activeColor: "#003399", inactiveColor: "#cccccc"},
                itemStyle: {color: "#333333", fontSize: "12px", fontWeight: "bold"},
                itemHoverStyle: {color: "#000000"},
                itemHiddenStyle: {color: "#cccccc"},
                shadow: !1,
                itemCheckboxStyle: {position: "absolute", width: "13px", height: "13px"},
                squareSymbol: !0,
                symbolPadding: 5,
                verticalAlign: "bottom",
                x: 0,
                y: 0,
                title: {style: {fontWeight: "bold"}}
            },
            loading: {
                labelStyle: {fontWeight: "bold", position: "relative", top: "45%"},
                style: {position: "absolute", backgroundColor: "#ffffff", opacity: .5, textAlign: "center"}
            },
            tooltip: {
                enabled: !0,
                animation: n.svg,
                borderRadius: 3,
                dateTimeLabelFormats: {
                    millisecond: "%A, %b %e, %H:%M:%S.%L",
                    second: "%A, %b %e, %H:%M:%S",
                    minute: "%A, %b %e, %H:%M",
                    hour: "%A, %b %e, %H:%M",
                    day: "%A, %b %e, %Y",
                    week: "Week from %A, %b %e, %Y",
                    month: "%B %Y",
                    year: "%Y"
                },
                footerFormat: "",
                padding: 8,
                snap: n.isTouchDevice ? 25 : 10,
                backgroundColor: t("#f7f7f7").setOpacity(.85).get(),
                borderWidth: 1,
                headerFormat: '<span style="font-size: 10px">{point.key}</span><br/>',
                pointFormat: '<span style="color:{point.color}">\u25cf</span> {series.name}: <b>{point.y}</b><br/>',
                shadow: !0,
                style: {
                    color: "#333333",
                    cursor: "default",
                    fontSize: "12px",
                    pointerEvents: "none",
                    whiteSpace: "nowrap"
                }
            },
            credits: {
                enabled: !0,
                href: "http://www.highcharts.com",
                position: {align: "right", x: -10, verticalAlign: "bottom", y: -5},
                style: {cursor: "pointer", color: "#999999", fontSize: "9px"},
                text: "Highcharts.com"
            }
        }, n.setOptions = function (t) {
            return n.defaultOptions = i(!0, n.defaultOptions, t), e(), n.defaultOptions
        }, n.getOptions = function () {
            return n.defaultOptions
        }, n.defaultPlotOptions = n.defaultOptions.plotOptions, e()
    }(t),mt = (gt = t).arrayMax,vt = gt.arrayMin,yt = gt.defined,xt = gt.destroyObjectProperties,bt = gt.each,wt = gt.erase,Ct = gt.merge,_t = gt.pick,gt.PlotLineOrBand = function (t, e) {
        this.axis = t, e && (this.options = e, this.id = e.id)
    },gt.PlotLineOrBand.prototype = {
        render: function () {
            var t, i = this, e = i.axis, n = e.horiz, r = i.options, o = r.label, s = i.label, a = r.to, l = r.from,
                c = r.value, h = yt(l) && yt(a), d = yt(c), u = i.svgElem, p = !u, f = [], g = r.color,
                m = _t(r.zIndex, 0), v = r.events,
                y = (f = {"class": "highcharts-plot-" + (h ? "band " : "line ") + (r.className || "")}, {}),
                x = e.chart.renderer, b = h ? "bands" : "lines", w = e.log2lin;
            if (e.isLog && (l = w(l), a = w(a), c = w(c)), d ? (f = {
                stroke: g,
                "stroke-width": r.width
            }, r.dashStyle && (f.dashstyle = r.dashStyle)) : h && (g && (f.fill = g), r.borderWidth && (f.stroke = r.borderColor, f["stroke-width"] = r.borderWidth)), (g = e[b += "-" + (y.zIndex = m)]) || (e[b] = g = x.g("plot-" + b).attr(y).add()), p && (i.svgElem = u = x.path().attr(f).add(g)), d) f = e.getPlotLinePath(c, u.strokeWidth()); else {
                if (!h) return;
                f = e.getPlotBandPath(l, a, r)
            }
            if (p && f && f.length) {
                if (u.attr({d: f}), v) for (t in r = function (e) {
                    u.on(e, function (t) {
                        v[e].apply(i, [t])
                    })
                }, v) r(t)
            } else u && (f ? (u.show(), u.animate({d: f})) : (u.hide(), s && (i.label = s = s.destroy())));
            return o && yt(o.text) && f && f.length && 0 < e.width && 0 < e.height && !f.flat ? (o = Ct({
                align: n && h && "center",
                x: n ? !h && 4 : 10,
                verticalAlign: !n && h && "middle",
                y: n ? h ? 16 : 10 : h ? 6 : -4,
                rotation: n && !h && 90
            }, o), this.renderLabel(o, f, h, m)) : s && s.hide(), i
        }, renderLabel: function (t, e, i, n) {
            var r = this.label, o = this.axis.chart.renderer;
            r || ((r = {
                align: t.textAlign || t.align,
                rotation: t.rotation,
                "class": "highcharts-plot-" + (i ? "band" : "line") + "-label " + (t.className || "")
            }).zIndex = n, this.label = r = o.text(t.text, 0, 0, t.useHTML).attr(r).add(), r.css(t.style)), n = [e[1], e[4], i ? e[6] : e[1]], e = [e[2], e[5], i ? e[7] : e[2]], i = vt(n), o = vt(e), r.align(t, !1, {
                x: i,
                y: o,
                width: mt(n) - i,
                height: mt(e) - o
            }), r.show()
        }, destroy: function () {
            wt(this.axis.plotLinesAndBands, this), delete this.axis, xt(this)
        }
    },gt.AxisPlotLineOrBandExtension = {
        getPlotBandPath: function (t, e) {
            return e = this.getPlotLinePath(e, null, null, !0), (t = this.getPlotLinePath(t, null, null, !0)) && e ? (t.flat = t.toString() === e.toString(), t.push(e[4], e[5], e[1], e[2])) : t = null, t
        }, addPlotBand: function (t) {
            return this.addPlotBandOrLine(t, "plotBands")
        }, addPlotLine: function (t) {
            return this.addPlotBandOrLine(t, "plotLines")
        }, addPlotBandOrLine: function (t, e) {
            var i = new gt.PlotLineOrBand(this, t).render(), n = this.userOptions;
            return i && (e && (n[e] = n[e] || [], n[e].push(t)), this.plotLinesAndBands.push(i)), i
        }, removePlotBandOrLine: function (e) {
            for (var t = this.plotLinesAndBands, i = this.options, n = this.userOptions, r = t.length; r--;) t[r].id === e && t[r].destroy();
            bt([i.plotLines || [], n.plotLines || [], i.plotBands || [], n.plotBands || []], function (t) {
                for (r = t.length; r--;) t[r].id === e && wt(t, t[r])
            })
        }
    },St = (kt = t).correctFloat,Tt = kt.defined,At = kt.destroyObjectProperties,$t = kt.isNumber,Mt = kt.merge,Et = kt.pick,Pt = kt.stop,Lt = kt.deg2rad,kt.Tick = function (t, e, i, n) {
        this.axis = t, this.pos = e, this.type = i || "", this.isNew = !0, i || n || this.addLabel()
    },kt.Tick.prototype = {
        addLabel: function () {
            var t, e = this.axis, i = e.options, n = e.chart, r = e.categories, o = e.names, s = this.pos, a = i.labels,
                l = s === (h = e.tickPositions)[0], c = s === h[h.length - 1],
                h = (o = r ? Et(r[s], o[s], s) : s, r = this.label, h.info);
            e.isDatetimeAxis && h && (t = i.dateTimeLabelFormats[h.higherRanks[s] || h.unitName]), this.isFirst = l, this.isLast = c, i = e.labelFormatter.call({
                axis: e,
                chart: n,
                isFirst: l,
                isLast: c,
                dateTimeLabelFormat: t,
                value: e.isLog ? St(e.lin2log(o)) : o
            }), Tt(r) ? r && r.attr({text: i}) : (this.labelLength = (this.label = r = Tt(i) && a.enabled ? n.renderer.text(i, 0, 0, a.useHTML).css(Mt(a.style)).add(e.labelGroup) : null) && r.getBBox().width, this.rotation = 0)
        }, getLabelSize: function () {
            return this.label ? this.label.getBBox()[this.axis.horiz ? "height" : "width"] : 0
        }, handleOverflow: function (t) {
            var e, i = this.axis, n = t.x, r = i.chart.chartWidth, o = i.chart.spacing,
                s = Et(i.labelLeft, Math.min(i.pos, o[3])),
                a = (o = Et(i.labelRight, Math.max(i.pos + i.len, r - o[1])), this.label), l = this.rotation,
                c = {left: 0, center: .5, right: 1}[i.labelAlign], h = a.getBBox().width, d = i.getSlotWidth(), u = d,
                p = 1, f = {};
            l ? l < 0 && n - c * h < s ? e = Math.round(n / Math.cos(l * Lt) - s) : 0 < l && o < n + c * h && (e = Math.round((r - n) / Math.cos(l * Lt))) : (r = n + (1 - c) * h, n - c * h < s ? u = t.x + u * (1 - c) - s : o < r && (u = o - t.x + u * c, p = -1), (u = Math.min(d, u)) < d && "center" === i.labelAlign && (t.x += p * (d - u - c * (d - Math.min(h, u)))), (u < h || i.autoRotation && (a.styles || {}).width) && (e = u)), e && (f.width = e, (i.options.labels.style || {}).textOverflow || (f.textOverflow = "ellipsis"), a.css(f))
        }, getPosition: function (t, e, i, n) {
            var r = this.axis, o = r.chart, s = n && o.oldChartHeight || o.chartHeight;
            return {
                x: t ? r.translate(e + i, null, null, n) + r.transB : r.left + r.offset + (r.opposite ? (n && o.oldChartWidth || o.chartWidth) - r.right - r.left : 0),
                y: t ? s - r.bottom + r.offset - (r.opposite ? r.height : 0) : s - r.translate(e + i, null, null, n) - r.transB
            }
        }, getLabelPosition: function (t, e, i, n, r, o, s, a) {
            var l = this.axis, c = l.transA, h = l.reversed, d = l.staggerLines, u = l.tickRotCorr || {x: 0, y: 0},
                p = r.y;
            return Tt(p) || (p = 0 === l.side ? i.rotation ? -8 : -i.getBBox().height : 2 === l.side ? u.y + 8 : Math.cos(i.rotation * Lt) * (u.y - i.getBBox(!1, 0).height / 2)), t = t + r.x + u.x - (o && n ? o * c * (h ? -1 : 1) : 0), e = e + p - (o && !n ? o * c * (h ? 1 : -1) : 0), d && (i = s / (a || 1) % d, l.opposite && (i = d - i - 1), e += l.labelOffset / d * i), {
                x: t,
                y: Math.round(e)
            }
        }, getMarkPath: function (t, e, i, n, r, o) {
            return o.crispLine(["M", t, e, "L", t + (r ? 0 : -i), e + (r ? i : 0)], n)
        }, render: function (t, e, i) {
            var n = this.axis, r = n.options, o = n.chart.renderer, s = n.horiz, a = this.type, l = this.label,
                c = this.pos, h = r.labels, d = this.gridLine, u = a ? a + "Tick" : "tick", p = n.tickSize(u),
                f = this.mark, g = !f, m = h.step, v = {}, y = !0, x = n.tickmarkOffset,
                b = (w = this.getPosition(s, c, x, e)).x, w = w.y,
                C = s && b === n.pos + n.len || !s && w === n.pos ? -1 : 1,
                _ = r[(T = a ? a + "Grid" : "grid") + "LineWidth"], k = r[T + "LineColor"], S = r[T + "LineDashStyle"],
                T = Et(r[u + "Width"], !a && n.isXAxis ? 1 : 0);
            u = r[u + "Color"], i = Et(i, 1), this.isActive = !0, d || (v.stroke = k, v["stroke-width"] = _, S && (v.dashstyle = S), a || (v.zIndex = 1), e && (v.opacity = 0), this.gridLine = d = o.path().attr(v).addClass("highcharts-" + (a ? a + "-" : "") + "grid-line").add(n.gridGroup)), !e && d && (c = n.getPlotLinePath(c + x, d.strokeWidth() * C, e, !0)) && d[this.isNew ? "attr" : "animate"]({
                d: c,
                opacity: i
            }), p && (n.opposite && (p[0] = -p[0]), g && (this.mark = f = o.path().addClass("highcharts-" + (a ? a + "-" : "") + "tick").add(n.axisGroup), f.attr({
                stroke: u,
                "stroke-width": T
            })), f[g ? "attr" : "animate"]({
                d: this.getMarkPath(b, w, p[0], f.strokeWidth() * C, s, o),
                opacity: i
            })), l && $t(b) && (l.xy = w = this.getLabelPosition(b, w, l, s, h, x, t, m), this.isFirst && !this.isLast && !Et(r.showFirstLabel, 1) || this.isLast && !this.isFirst && !Et(r.showLastLabel, 1) ? y = !1 : !s || n.isRadial || h.step || h.rotation || e || 0 === i || this.handleOverflow(w), m && t % m && (y = !1), y && $t(w.y) ? (w.opacity = i, l[this.isNew ? "attr" : "animate"](w)) : (Pt(l), l.attr("y", -9999)), this.isNew = !1)
        }, destroy: function () {
            At(this, this.axis)
        }
    },Ft = (It = t).addEvent,Dt = It.animObject,Ot = It.arrayMax,Nt = It.arrayMin,jt = It.AxisPlotLineOrBandExtension,zt = It.color,Rt = It.correctFloat,Bt = It.defaultOptions,Ht = It.defined,Wt = It.deg2rad,qt = It.destroyObjectProperties,Gt = It.each,Xt = It.error,Yt = It.extend,Vt = It.fireEvent,Ut = It.format,Qt = It.getMagnitude,Kt = It.grep,Zt = It.inArray,Jt = It.isArray,te = It.isNumber,ee = It.isString,ie = It.merge,ne = It.normalizeTickInterval,re = It.pick,oe = It.PlotLineOrBand,se = It.removeEvent,ae = It.splat,le = It.syncTimeout,ce = It.Tick,It.Axis = function () {
        this.init.apply(this, arguments)
    },It.Axis.prototype = {
        defaultOptions: {
            dateTimeLabelFormats: {
                millisecond: "%H:%M:%S.%L",
                second: "%H:%M:%S",
                minute: "%H:%M",
                hour: "%H:%M",
                day: "%e. %b",
                week: "%e. %b",
                month: "%b '%y",
                year: "%Y"
            },
            endOnTick: !1,
            labels: {enabled: !0, style: {color: "#666666", cursor: "default", fontSize: "11px"}, x: 0},
            minPadding: .01,
            maxPadding: .01,
            minorTickLength: 2,
            minorTickPosition: "outside",
            startOfWeek: 1,
            startOnTick: !1,
            tickLength: 10,
            tickmarkPlacement: "between",
            tickPixelInterval: 100,
            tickPosition: "outside",
            title: {align: "middle", style: {color: "#666666"}},
            type: "linear",
            minorGridLineColor: "#f2f2f2",
            minorGridLineWidth: 1,
            minorTickColor: "#999999",
            lineColor: "#ccd6eb",
            lineWidth: 1,
            gridLineColor: "#e6e6e6",
            tickColor: "#ccd6eb"
        },
        defaultYAxisOptions: {
            endOnTick: !0,
            tickPixelInterval: 72,
            showLastLabel: !0,
            labels: {x: -8},
            maxPadding: .05,
            minPadding: .05,
            startOnTick: !0,
            title: {rotation: 270, text: "Values"},
            stackLabels: {
                enabled: !1,
                formatter: function () {
                    return It.numberFormat(this.total, -1)
                },
                style: {
                    fontSize: "11px",
                    fontWeight: "bold",
                    color: "#000000",
                    textShadow: "1px 1px contrast, -1px -1px contrast, -1px 1px contrast, 1px -1px contrast"
                }
            },
            gridLineWidth: 1,
            lineWidth: 0
        },
        defaultLeftAxisOptions: {labels: {x: -15}, title: {rotation: 270}},
        defaultRightAxisOptions: {labels: {x: 15}, title: {rotation: 90}},
        defaultBottomAxisOptions: {labels: {autoRotation: [-45], x: 0}, title: {rotation: 0}},
        defaultTopAxisOptions: {labels: {autoRotation: [-45], x: 0}, title: {rotation: 0}},
        init: function (t, e) {
            var i = e.isX;
            this.chart = t, this.horiz = t.inverted ? !i : i, this.isXAxis = i, this.coll = this.coll || (i ? "xAxis" : "yAxis"), this.opposite = e.opposite, this.side = e.side || (this.horiz ? this.opposite ? 0 : 2 : this.opposite ? 1 : 3), this.setOptions(e);
            var n, r = this.options, o = r.type;
            for (n in this.labelFormatter = r.labels.formatter || this.defaultLabelFormatter, this.userOptions = e, this.minPixelPadding = 0, this.reversed = r.reversed, this.visible = !1 !== r.visible, this.zoomEnabled = !1 !== r.zoomEnabled, this.hasNames = "category" === o || !0 === r.categories, this.categories = r.categories || this.hasNames, this.names = this.names || [], this.isLog = "logarithmic" === o, this.isDatetimeAxis = "datetime" === o, this.isLinked = Ht(r.linkedTo), this.ticks = {}, this.labelEdge = [], this.minorTicks = {}, this.plotLinesAndBands = [], this.alternateBands = {}, this.len = 0, this.minRange = this.userMinRange = r.minRange || r.maxZoom, this.range = r.range, this.offset = r.offset || 0, this.stacks = {}, this.oldStacks = {}, this.stacksTouched = 0, this.min = this.max = null, this.crosshair = re(r.crosshair, ae(t.options.tooltip.crosshairs)[i ? 0 : 1], !1), e = this.options.events, -1 === Zt(this, t.axes) && (i ? t.axes.splice(t.xAxis.length, 0, this) : t.axes.push(this), t[this.coll].push(this)), this.series = this.series || [], t.inverted && i && void 0 === this.reversed && (this.reversed = !0), this.removePlotLine = this.removePlotBand = this.removePlotBandOrLine, e) Ft(this, n, e[n]);
            this.isLog && (this.val2lin = this.log2lin, this.lin2val = this.lin2log)
        },
        setOptions: function (t) {
            this.options = ie(this.defaultOptions, "yAxis" === this.coll && this.defaultYAxisOptions, [this.defaultTopAxisOptions, this.defaultRightAxisOptions, this.defaultBottomAxisOptions, this.defaultLeftAxisOptions][this.side], ie(Bt[this.coll], t))
        },
        defaultLabelFormatter: function () {
            var t, e = this.axis, i = this.value, n = e.categories, r = this.dateTimeLabelFormat,
                o = Bt.lang.numericSymbols, s = o && o.length, a = e.options.labels.format;
            if (e = e.isLog ? i : e.tickInterval, a) t = Ut(a, this); else if (n) t = i; else if (r) t = It.dateFormat(r, i); else if (s && 1e3 <= e) for (; s-- && void 0 === t;) (n = Math.pow(1e3, s + 1)) <= e && 0 == 10 * i % n && null !== o[s] && 0 !== i && (t = It.numberFormat(i / n, -1) + o[s]);
            return void 0 === t && (t = 1e4 <= Math.abs(i) ? It.numberFormat(i, -1) : It.numberFormat(i, -1, void 0, "")), t
        },
        getSeriesExtremes: function () {
            var r = this, o = r.chart;
            r.hasVisibleSeries = !1, r.dataMin = r.dataMax = r.threshold = null, r.softThreshold = !r.isXAxis, r.buildStacks && r.buildStacks(), Gt(r.series, function (t) {
                if (t.visible || !o.options.chart.ignoreHiddenSeries) {
                    var e, i = t.options, n = i.threshold;
                    r.hasVisibleSeries = !0, r.isLog && n <= 0 && (n = null), r.isXAxis ? (i = t.xData).length && (t = Nt(i), te(t) || t instanceof Date || (i = Kt(i, function (t) {
                        return te(t)
                    }), t = Nt(i)), r.dataMin = Math.min(re(r.dataMin, i[0]), t), r.dataMax = Math.max(re(r.dataMax, i[0]), Ot(i))) : (t.getExtremes(), e = t.dataMax, t = t.dataMin, Ht(t) && Ht(e) && (r.dataMin = Math.min(re(r.dataMin, t), t), r.dataMax = Math.max(re(r.dataMax, e), e)), Ht(n) && (r.threshold = n), (!i.softThreshold || r.isLog) && (r.softThreshold = !1))
                }
            })
        },
        translate: function (t, e, i, n, r, o) {
            var s = this.linkedParent || this, a = 1, l = 0, c = n ? s.oldTransA : s.transA;
            n = n ? s.oldMin : s.min;
            var h = s.minPixelPadding;
            return r = (s.isOrdinal || s.isBroken || s.isLog && r) && s.lin2val, c || (c = s.transA), i && (a *= -1, l = s.len), s.reversed && (l -= (a *= -1) * (s.sector || s.len)), e ? (t = (t * a + l - h) / c + n, r && (t = s.lin2val(t))) : (r && (t = s.val2lin(t)), "between" === o && (o = .5), t = a * (t - n) * c + l + a * h + (te(o) ? c * o * s.pointRange : 0)), t
        },
        toPixels: function (t, e) {
            return this.translate(t, !1, !this.horiz, null, !0) + (e ? 0 : this.pos)
        },
        toValue: function (t, e) {
            return this.translate(t - (e ? 0 : this.pos), !0, !this.horiz, null, !0)
        },
        getPlotLinePath: function (t, e, i, n, r) {
            var o, s, a, l = this.chart, c = this.left, h = this.top, d = i && l.oldChartHeight || l.chartHeight,
                u = i && l.oldChartWidth || l.chartWidth;
            o = this.transB;
            var p = function (t, e, i) {
                return (t < e || i < t) && (n ? t = Math.min(Math.max(e, t), i) : a = !0), t
            };
            return r = re(r, this.translate(t, null, null, i)), t = i = Math.round(r + o), o = s = Math.round(d - r - o), te(r) ? this.horiz ? (o = h, s = d - this.bottom, t = i = p(t, c, c + this.width)) : (t = c, i = u - this.right, o = s = p(o, h, h + this.height)) : a = !0, a && !n ? null : l.renderer.crispLine(["M", t, o, "L", i, s], e || 1)
        },
        getLinearTickPositions: function (t, e, i) {
            var n, r = Rt(Math.floor(e / t) * t), o = Rt(Math.ceil(i / t) * t), s = [];
            if (e === i && te(e)) return [e];
            for (e = r; e <= o && (s.push(e), (e = Rt(e + t)) !== n);) n = e;
            return s
        },
        getMinorTickPositions: function () {
            var t, e = this.options, i = this.tickPositions, n = this.minorTickInterval, r = [],
                o = this.pointRangePadding || 0;
            t = this.min - o;
            var s = (o = this.max + o) - t;
            if (s && s / n < this.len / 3) if (this.isLog) for (o = i.length, t = 1; t < o; t++) r = r.concat(this.getLogTickPositions(n, i[t - 1], i[t], !0)); else if (this.isDatetimeAxis && "auto" === e.minorTickInterval) r = r.concat(this.getTimeTicks(this.normalizeTimeTickInterval(n), t, o, e.startOfWeek)); else for (i = t + (i[0] - t) % n; i <= o; i += n) r.push(i);
            return 0 !== r.length && this.trimTicks(r, e.startOnTick, e.endOnTick), r
        },
        adjustForMinRange: function () {
            var t, e, i, n, r, o, s = this.options, a = this.min, l = this.max,
                c = this.dataMax - this.dataMin >= this.minRange;
            this.isXAxis && void 0 === this.minRange && !this.isLog && (Ht(s.min) || Ht(s.max) ? this.minRange = null : (Gt(this.series, function (t) {
                for (r = t.xData, i = t.xIncrement ? 1 : r.length - 1; 0 < i; i--) n = r[i] - r[i - 1], (void 0 === e || n < e) && (e = n)
            }), this.minRange = Math.min(5 * e, this.dataMax - this.dataMin))), l - a < this.minRange && (t = [a - (t = ((o = this.minRange) - l + a) / 2), re(s.min, a - t)], c && (t[2] = this.isLog ? this.log2lin(this.dataMin) : this.dataMin), l = [(a = Ot(t)) + o, re(s.max, a + o)], c && (l[2] = this.isLog ? this.log2lin(this.dataMax) : this.dataMax), (l = Nt(l)) - a < o && (t[0] = l - o, t[1] = re(s.min, l - o), a = Ot(t))), this.min = a, this.max = l
        },
        getClosest: function () {
            var i;
            return this.categories ? i = 1 : Gt(this.series, function (t) {
                var e = t.closestPointRange;
                !t.noSharedTooltip && Ht(e) && (i = Ht(i) ? Math.min(i, e) : e)
            }), i
        },
        nameToX: function (t) {
            var e, i = Jt(this.categories), n = i ? this.categories : this.names, r = t.options.x;
            return t.series.requireSorting = !1, Ht(r) || (r = !1 === this.options.uniqueNames ? t.series.autoIncrement() : Zt(t.name, n)), -1 === r ? i || (e = n.length) : e = r, this.names[e] = t.name, e
        },
        updateNames: function () {
            var r = this;
            0 < this.names.length && (this.names.length = 0, this.minRange = void 0, Gt(this.series || [], function (n) {
                n.points && !n.isDirtyData || (n.processData(), n.generatePoints()), Gt(n.points, function (t, e) {
                    var i;
                    t.options && void 0 === t.options.x && (i = r.nameToX(t)) !== t.x && (t.x = i, n.xData[e] = i)
                })
            }))
        },
        setAxisTranslation: function (t) {
            var i, n = this, e = n.max - n.min, r = n.axisPointRange || 0, o = 0, s = 0, a = n.linkedParent,
                l = !!n.categories, c = n.transA, h = n.isXAxis;
            (h || l || r) && (a ? (o = a.minPointOffset, s = a.pointRangePadding) : (i = n.getClosest(), Gt(n.series, function (t) {
                var e = l ? 1 : h ? re(t.options.pointRange, i, 0) : n.axisPointRange || 0;
                t = t.options.pointPlacement, r = Math.max(r, e), n.single || (o = Math.max(o, ee(t) ? 0 : e / 2), s = Math.max(s, "on" === t ? 0 : e))
            })), a = n.ordinalSlope && i ? n.ordinalSlope / i : 1, n.minPointOffset = o *= a, n.pointRangePadding = s *= a, n.pointRange = Math.min(r, e), h && (n.closestPointRange = i)), t && (n.oldTransA = c), n.translationSlope = n.transA = c = n.len / (e + s || 1), n.transB = n.horiz ? n.left : n.bottom, n.minPixelPadding = c * o
        },
        minFromRange: function () {
            return this.max - this.range
        },
        setTickInterval: function (t) {
            var e, i, n, r, o = this, s = o.chart, a = o.options, l = o.isLog, c = o.log2lin, h = o.isDatetimeAxis,
                d = o.isXAxis, u = o.isLinked, p = a.maxPadding, f = a.minPadding, g = a.tickInterval,
                m = a.tickPixelInterval, v = o.categories, y = o.threshold, x = o.softThreshold;
            h || v || u || this.getTickAmount(), n = re(o.userMin, a.min), r = re(o.userMax, a.max), u ? (o.linkedParent = s[o.coll][a.linkedTo], s = o.linkedParent.getExtremes(), o.min = re(s.min, s.dataMin), o.max = re(s.max, s.dataMax), a.type !== o.linkedParent.options.type && Xt(11, 1)) : (!x && Ht(y) && (o.dataMin >= y ? (e = y, f = 0) : o.dataMax <= y && (i = y, p = 0)), o.min = re(n, e, o.dataMin), o.max = re(r, i, o.dataMax)), l && (!t && Math.min(o.min, re(o.dataMin, o.min)) <= 0 && Xt(10, 1), o.min = Rt(c(o.min), 15), o.max = Rt(c(o.max), 15)), o.range && Ht(o.max) && (o.userMin = o.min = n = Math.max(o.min, o.minFromRange()), o.userMax = r = o.max, o.range = null), Vt(o, "foundExtremes"), o.beforePadding && o.beforePadding(), o.adjustForMinRange(), !(v || o.axisPointRange || o.usePercentage || u) && Ht(o.min) && Ht(o.max) && (c = o.max - o.min) && (!Ht(n) && f && (o.min -= c * f), !Ht(r) && p && (o.max += c * p)), te(a.floor) ? o.min = Math.max(o.min, a.floor) : te(a.softMin) && (o.min = Math.min(o.min, a.softMin)), te(a.ceiling) ? o.max = Math.min(o.max, a.ceiling) : te(a.softMax) && (o.max = Math.max(o.max, a.softMax)), x && Ht(o.dataMin) && (y = y || 0, !Ht(n) && o.min < y && o.dataMin >= y ? o.min = y : !Ht(r) && o.max > y && o.dataMax <= y && (o.max = y)), o.tickInterval = o.min === o.max || void 0 === o.min || void 0 === o.max ? 1 : u && !g && m === o.linkedParent.options.tickPixelInterval ? g = o.linkedParent.tickInterval : re(g, this.tickAmount ? (o.max - o.min) / Math.max(this.tickAmount - 1, 1) : void 0, v ? 1 : (o.max - o.min) * m / Math.max(o.len, m)), d && !t && Gt(o.series, function (t) {
                t.processData(o.min !== o.oldMin || o.max !== o.oldMax)
            }), o.setAxisTranslation(!0), o.beforeSetTickPositions && o.beforeSetTickPositions(), o.postProcessTickInterval && (o.tickInterval = o.postProcessTickInterval(o.tickInterval)), o.pointRange && !g && (o.tickInterval = Math.max(o.pointRange, o.tickInterval)), t = re(a.minTickInterval, o.isDatetimeAxis && o.closestPointRange), !g && o.tickInterval < t && (o.tickInterval = t), h || l || g || (o.tickInterval = ne(o.tickInterval, null, Qt(o.tickInterval), re(a.allowDecimals, !(.5 < o.tickInterval && o.tickInterval < 5 && 1e3 < o.max && o.max < 9999)), !!this.tickAmount)), this.tickAmount || (o.tickInterval = o.unsquish()), this.setTickPositions()
        },
        setTickPositions: function () {
            var t, e, i = this.options, n = i.tickPositions, r = i.tickPositioner, o = i.startOnTick, s = i.endOnTick;
            this.tickmarkOffset = this.categories && "between" === i.tickmarkPlacement && 1 === this.tickInterval ? .5 : 0, this.minorTickInterval = "auto" === i.minorTickInterval && this.tickInterval ? this.tickInterval / 5 : i.minorTickInterval, this.tickPositions = t = n && n.slice(), !t && ((t = this.isDatetimeAxis ? this.getTimeTicks(this.normalizeTimeTickInterval(this.tickInterval, i.units), this.min, this.max, i.startOfWeek, this.ordinalPositions, this.closestPointRange, !0) : this.isLog ? this.getLogTickPositions(this.tickInterval, this.min, this.max) : this.getLinearTickPositions(this.tickInterval, this.min, this.max)).length > this.len && (t = [t[0], t.pop()]), this.tickPositions = t, r && (r = r.apply(this, [this.min, this.max]))) && (this.tickPositions = t = r), this.isLinked || (this.trimTicks(t, o, s), this.min === this.max && Ht(this.min) && !this.tickAmount && (e = !0, this.min -= .5, this.max += .5), this.single = e, n || r || this.adjustTickAmount())
        },
        trimTicks: function (t, e, i) {
            var n = t[0], r = t[t.length - 1], o = this.minPointOffset || 0;
            if (e) this.min = n; else for (; this.min - o > t[0];) t.shift();
            if (i) this.max = r; else for (; this.max + o < t[t.length - 1];) t.pop();
            0 === t.length && Ht(n) && t.push((r + n) / 2)
        },
        alignToOthers: function () {
            var i, n = {}, t = this.options;
            return !1 !== this.chart.options.chart.alignTicks && !1 !== t.alignTicks && Gt(this.chart[this.coll], function (t) {
                var e = t.options;
                e = [t.horiz ? e.left : e.top, e.width, e.height, e.pane].join(), t.series.length && (n[e] ? i = !0 : n[e] = 1)
            }), i
        },
        getTickAmount: function () {
            var t = this.options, e = t.tickAmount, i = t.tickPixelInterval;
            !Ht(t.tickInterval) && this.len < i && !this.isRadial && !this.isLog && t.startOnTick && t.endOnTick && (e = 2), !e && this.alignToOthers() && (e = Math.ceil(this.len / i) + 1), e < 4 && (this.finalTickAmt = e, e = 5), this.tickAmount = e
        },
        adjustTickAmount: function () {
            var t = this.tickInterval, e = this.tickPositions, i = this.tickAmount, n = this.finalTickAmt,
                r = e && e.length;
            if (r < i) {
                for (; e.length < i;) e.push(Rt(e[e.length - 1] + t));
                this.transA *= (r - 1) / (i - 1), this.max = e[e.length - 1]
            } else i < r && (this.tickInterval *= 2, this.setTickPositions());
            if (Ht(n)) {
                for (t = i = e.length; t--;) (3 === n && 1 == t % 2 || n <= 2 && 0 < t && t < i - 1) && e.splice(t, 1);
                this.finalTickAmt = void 0
            }
        },
        setScale: function () {
            var e, t;
            this.oldMin = this.min, this.oldMax = this.max, this.oldAxisLength = this.len, this.setAxisSize(), t = this.len !== this.oldAxisLength, Gt(this.series, function (t) {
                (t.isDirtyData || t.isDirty || t.xAxis.isDirty) && (e = !0)
            }), t || e || this.isLinked || this.forceRedraw || this.userMin !== this.oldUserMin || this.userMax !== this.oldUserMax || this.alignToOthers() ? (this.resetStacks && this.resetStacks(), this.forceRedraw = !1, this.getSeriesExtremes(), this.setTickInterval(), this.oldUserMin = this.userMin, this.oldUserMax = this.userMax, this.isDirty || (this.isDirty = t || this.min !== this.oldMin || this.max !== this.oldMax)) : this.cleanStacks && this.cleanStacks()
        },
        setExtremes: function (t, e, i, n, r) {
            var o = this, s = o.chart;
            i = re(i, !0), Gt(o.series, function (t) {
                delete t.kdTree
            }), r = Yt(r, {min: t, max: e}), Vt(o, "setExtremes", r, function () {
                o.userMin = t, o.userMax = e, o.eventArgs = r, i && s.redraw(n)
            })
        },
        zoom: function (t, e) {
            var i = this.dataMin, n = this.dataMax, r = this.options, o = Math.min(i, re(r.min, i));
            return r = Math.max(n, re(r.max, n)), t === this.min && e === this.max || (this.allowZoomOutside || (Ht(i) && t <= o && (t = o), Ht(n) && r <= e && (e = r)), this.displayBtn = void 0 !== t || void 0 !== e, this.setExtremes(t, e, !1, void 0, {trigger: "zoom"})), !0
        },
        setAxisSize: function () {
            var t = this.chart, e = (s = this.options).offsetLeft || 0, i = this.horiz,
                n = re(s.width, t.plotWidth - e + (s.offsetRight || 0)), r = re(s.height, t.plotHeight),
                o = re(s.top, t.plotTop), s = re(s.left, t.plotLeft + e);
            (e = /%$/).test(r) && (r = Math.round(parseFloat(r) / 100 * t.plotHeight)), e.test(o) && (o = Math.round(parseFloat(o) / 100 * t.plotHeight + t.plotTop)), this.left = s, this.top = o, this.width = n, this.height = r, this.bottom = t.chartHeight - r - o, this.right = t.chartWidth - n - s, this.len = Math.max(i ? n : r, 0), this.pos = i ? s : o
        },
        getExtremes: function () {
            var t = this.isLog, e = this.lin2log;
            return {
                min: t ? Rt(e(this.min)) : this.min,
                max: t ? Rt(e(this.max)) : this.max,
                dataMin: this.dataMin,
                dataMax: this.dataMax,
                userMin: this.userMin,
                userMax: this.userMax
            }
        },
        getThreshold: function (t) {
            var e = this.isLog, i = this.lin2log, n = e ? i(this.min) : this.min;
            return e = e ? i(this.max) : this.max, null === t ? t = n : t < n ? t = n : e < t && (t = e), this.translate(t, 0, 1, 0, 1)
        },
        autoLabelAlign: function (t) {
            return 15 < (t = (re(t, 0) - 90 * this.side + 720) % 360) && t < 165 ? "right" : 195 < t && t < 345 ? "left" : "center"
        },
        tickSize: function (t) {
            var e = this.options, i = e[t + "Length"], n = re(e[t + "Width"], "tick" === t && this.isXAxis ? 1 : 0);
            if (n && i) return "inside" === e[t + "Position"] && (i = -i), [i, n]
        },
        labelMetrics: function () {
            return this.chart.renderer.fontMetrics(this.options.labels.style && this.options.labels.style.fontSize, this.ticks[0] && this.ticks[0].label)
        },
        unsquish: function () {
            var i, n, t, e = this.options.labels, r = this.horiz, o = this.tickInterval, s = o,
                a = this.len / (((this.categories ? 1 : 0) + this.max - this.min) / o), l = e.rotation,
                c = this.labelMetrics(), h = Number.MAX_VALUE, d = function (t) {
                    return (t = 1 < (t /= a || 1) ? Math.ceil(t) : 1) * o
                };
            return r ? (t = !e.staggerLines && !e.step && (Ht(l) ? [l] : a < re(e.autoRotationLimit, 80) && e.autoRotation)) && Gt(t, function (t) {
                var e;
                (t === l || t && -90 <= t && t <= 90) && (e = (n = d(Math.abs(c.h / Math.sin(Wt * t)))) + Math.abs(t / 360)) < h && (h = e, i = t, s = n)
            }) : e.step || (s = d(c.h)), this.autoRotation = t, this.labelRotation = re(i, l), s
        },
        getSlotWidth: function () {
            var t = this.chart, e = this.horiz, i = this.options.labels,
                n = Math.max(this.tickPositions.length - (this.categories ? 0 : 1), 1), r = t.margin[3];
            return e && (i.step || 0) < 2 && !i.rotation && (this.staggerLines || 1) * t.plotWidth / n || !e && (r && r - t.spacing[3] || .33 * t.chartWidth)
        },
        renderUnsquish: function () {
            var i, t, e, n = this.chart, r = n.renderer, o = this.tickPositions, s = this.ticks,
                a = this.options.labels, l = this.horiz, c = this.getSlotWidth(),
                h = Math.max(1, Math.round(c - 2 * (a.padding || 5))), d = {}, u = this.labelMetrics(),
                p = a.style && a.style.textOverflow, f = 0;
            if (ee(a.rotation) || (d.rotation = a.rotation || 0), Gt(o, function (t) {
                (t = s[t]) && t.labelLength > f && (f = t.labelLength)
            }), this.maxLabelLength = f, this.autoRotation) h < f && f > u.h ? d.rotation = this.labelRotation : this.labelRotation = 0; else if (c && (i = {width: h + "px"}, !p)) for (i.textOverflow = "clip", t = o.length; !l && t--;) e = o[t], (h = s[e].label) && (h.styles && "ellipsis" === h.styles.textOverflow ? h.css({textOverflow: "clip"}) : s[e].labelLength > c && h.css({width: c + "px"}), h.getBBox().height > this.len / o.length - (u.h - u.f) && (h.specCss = {textOverflow: "ellipsis"}));
            d.rotation && (i = {width: (f > .5 * n.chartHeight ? .33 * n.chartHeight : n.chartHeight) + "px"}, p || (i.textOverflow = "ellipsis")), (this.labelAlign = a.align || this.autoLabelAlign(this.labelRotation)) && (d.align = this.labelAlign), Gt(o, function (t) {
                var e = (t = s[t]) && t.label;
                e && (e.attr(d), i && e.css(ie(i, e.specCss)), delete e.specCss, t.rotation = d.rotation)
            }), this.tickRotCorr = r.rotCorr(u.b, this.labelRotation || 0, 0 !== this.side)
        },
        hasData: function () {
            return this.hasVisibleSeries || Ht(this.min) && Ht(this.max) && !!this.tickPositions
        },
        getOffset: function () {
            var t, e, i, n, r = this, o = (x = r.chart).renderer, s = r.options, a = r.tickPositions, l = r.ticks,
                c = r.horiz, h = r.side, d = x.inverted ? [1, 0, 3, 2][h] : h, u = 0, p = 0, f = s.title, g = s.labels,
                m = 0, v = r.opposite, y = x.axisOffset, x = x.clipOffset, b = [-1, 1, 1, -1][h], w = s.className,
                C = r.axisParent, _ = this.tickSize("tick");
            if (t = r.hasData(), r.showAxis = e = t || re(s.showEmpty, !0), r.staggerLines = r.horiz && g.staggerLines, r.axisGroup || (r.gridGroup = o.g("grid").attr({zIndex: s.gridZIndex || 1}).addClass("highcharts-" + this.coll.toLowerCase() + "-grid " + (w || "")).add(C), r.axisGroup = o.g("axis").attr({zIndex: s.zIndex || 2}).addClass("highcharts-" + this.coll.toLowerCase() + " " + (w || "")).add(C), r.labelGroup = o.g("axis-labels").attr({zIndex: g.zIndex || 7}).addClass("highcharts-" + r.coll.toLowerCase() + "-labels " + (w || "")).add(C)), t || r.isLinked) Gt(a, function (t) {
                l[t] ? l[t].addLabel() : l[t] = new ce(r, t)
            }), r.renderUnsquish(), !1 === g.reserveSpace || 0 !== h && 2 !== h && {
                1: "left",
                3: "right"
            }[h] !== r.labelAlign && "center" !== r.labelAlign || Gt(a, function (t) {
                m = Math.max(l[t].getLabelSize(), m)
            }), r.staggerLines && (m *= r.staggerLines, r.labelOffset = m * (r.opposite ? -1 : 1)); else for (n in l) l[n].destroy(), delete l[n];
            f && f.text && !1 !== f.enabled && (r.axisTitle || ((n = f.textAlign) || (n = (c ? {
                low: "left",
                middle: "center", high: "right"
            } : {
                low: v ? "right" : "left",
                middle: "center",
                high: v ? "left" : "right"
            })[f.align]), r.axisTitle = o.text(f.text, 0, 0, f.useHTML).attr({
                zIndex: 7,
                rotation: f.rotation || 0,
                align: n
            }).addClass("highcharts-axis-title").css(f.style).add(r.axisGroup), r.axisTitle.isNew = !0), e && (u = r.axisTitle.getBBox()[c ? "height" : "width"], i = f.offset, p = Ht(i) ? 0 : re(f.margin, c ? 5 : 10)), r.axisTitle[e ? "show" : "hide"](!0)), r.renderLine(), r.offset = b * re(s.offset, y[h]), r.tickRotCorr = r.tickRotCorr || {
                x: 0,
                y: 0
            }, o = 0 === h ? -r.labelMetrics().h : 2 === h ? r.tickRotCorr.y : 0, p = Math.abs(m) + p, m && (p = p - o + b * (c ? re(g.y, r.tickRotCorr.y + 8 * b) : g.x)), r.axisTitleMargin = re(i, p), y[h] = Math.max(y[h], r.axisTitleMargin + u + b * r.offset, p, t && a.length && _ ? _[0] : 0), s = s.offset ? 0 : 2 * Math.floor(r.axisLine.strokeWidth() / 2), x[d] = Math.max(x[d], s)
        },
        getLinePath: function (t) {
            var e = this.chart, i = this.opposite, n = this.offset, r = this.horiz,
                o = this.left + (i ? this.width : 0) + n;
            return n = e.chartHeight - this.bottom - (i ? this.height : 0) + n, i && (t *= -1), e.renderer.crispLine(["M", r ? this.left : o, r ? n : this.top, "L", r ? e.chartWidth - this.right : o, r ? n : e.chartHeight - this.bottom], t)
        },
        renderLine: function () {
            this.axisLine || (this.axisLine = this.chart.renderer.path().addClass("highcharts-axis-line").add(this.axisGroup), this.axisLine.attr({
                stroke: this.options.lineColor,
                "stroke-width": this.options.lineWidth,
                zIndex: 7
            }))
        },
        getTitlePosition: function () {
            var t = this.horiz, e = this.left, i = this.top, n = this.len, r = this.options.title, o = t ? e : i,
                s = this.opposite, a = this.offset, l = r.x || 0, c = r.y || 0,
                h = this.chart.renderer.fontMetrics(r.style && r.style.fontSize, this.axisTitle).f;
            return n = {
                low: o + (t ? 0 : n),
                middle: o + n / 2,
                high: o + (t ? n : 0)
            }[r.align], e = (t ? i + this.height : e) + (t ? 1 : -1) * (s ? -1 : 1) * this.axisTitleMargin + (2 === this.side ? h : 0), {
                x: t ? n + l : e + (s ? this.width : 0) + a + l,
                y: t ? e + c - (s ? this.height : 0) + a : n + c
            }
        },
        render: function () {
            var i, n, r = this, o = r.chart, t = o.renderer, e = r.options, s = r.isLog, a = r.lin2log, l = r.isLinked,
                c = r.tickPositions, h = r.axisTitle, d = r.ticks, u = r.minorTicks, p = r.alternateBands,
                f = e.stackLabels, g = e.alternateGridColor, m = r.tickmarkOffset, v = r.axisLine,
                y = o.hasRendered && te(r.oldMin), x = r.showAxis, b = Dt(t.globalAnimation);
            r.labelEdge.length = 0, r.overlap = !1, Gt([d, u, p], function (t) {
                for (var e in t) t[e].isActive = !1
            }), (r.hasData() || l) && (r.minorTickInterval && !r.categories && Gt(r.getMinorTickPositions(), function (t) {
                u[t] || (u[t] = new ce(r, t, "minor")), y && u[t].isNew && u[t].render(null, !0), u[t].render(null, !1, 1)
            }), c.length && (Gt(c, function (t, e) {
                (!l || t >= r.min && t <= r.max) && (d[t] || (d[t] = new ce(r, t)), y && d[t].isNew && d[t].render(e, !0, .1), d[t].render(e))
            }), m && (0 === r.min || r.single) && (d[-1] || (d[-1] = new ce(r, -1, null, !0)), d[-1].render(-1))), g && Gt(c, function (t, e) {
                n = void 0 !== c[e + 1] ? c[e + 1] + m : r.max - m, 0 == e % 2 && t < r.max && n <= r.max + (o.polar ? -m : m) && (p[t] || (p[t] = new oe(r)), i = t + m, p[t].options = {
                    from: s ? a(i) : i,
                    to: s ? a(n) : n,
                    color: g
                }, p[t].render(), p[t].isActive = !0)
            }), r._addedPlotLB || (Gt((e.plotLines || []).concat(e.plotBands || []), function (t) {
                r.addPlotBandOrLine(t)
            }), r._addedPlotLB = !0)), Gt([d, u, p], function (t) {
                var e, i, n = [], r = b.duration;
                for (e in t) t[e].isActive || (t[e].render(e, !1, 0), t[e].isActive = !1, n.push(e));
                le(function () {
                    for (i = n.length; i--;) t[n[i]] && !t[n[i]].isActive && (t[n[i]].destroy(), delete t[n[i]])
                }, t !== p && o.hasRendered && r ? r : 0)
            }), v && (v[v.isPlaced ? "animate" : "attr"]({d: this.getLinePath(v.strokeWidth())}), v.isPlaced = !0, v[x ? "show" : "hide"](!0)), h && x && (h[h.isNew ? "attr" : "animate"](r.getTitlePosition()), h.isNew = !1), f && f.enabled && r.renderStackTotals(), r.isDirty = !1
        },
        redraw: function () {
            this.visible && (this.render(), Gt(this.plotLinesAndBands, function (t) {
                t.render()
            })), Gt(this.series, function (t) {
                t.isDirty = !0
            })
        },
        destroy: function (t) {
            var e, i, n = this, r = n.stacks, o = n.plotLinesAndBands;
            for (e in t || se(n), r) qt(r[e]), r[e] = null;
            if (Gt([n.ticks, n.minorTicks, n.alternateBands], function (t) {
                qt(t)
            }), o) for (t = o.length; t--;) o[t].destroy();
            for (i in Gt("stackTotalGroup axisLine axisTitle axisGroup gridGroup labelGroup cross".split(" "), function (t) {
                n[t] && (n[t] = n[t].destroy())
            }), o = "extKey hcEvents names series userMax userMin".split(" "), n) n.hasOwnProperty(i) && -1 === Zt(i, o) && delete n[i]
        },
        drawCrosshair: function (t, e) {
            var i, n, r = this.crosshair, o = re(r.snap, !0), s = this.cross;
            t || (t = this.cross && this.cross.e), this.crosshair && !1 !== (Ht(e) || !o) ? (o ? Ht(e) && (n = this.isXAxis ? e.plotX : this.len - e.plotY) : n = t && (this.horiz ? t.chartX - this.pos : this.len - t.chartY + this.pos), Ht(n) && (i = this.getPlotLinePath(e && (this.isXAxis ? e.x : re(e.stackY, e.y)), null, null, null, n) || null), Ht(i) ? (e = this.categories && !this.isRadial, s || (this.cross = s = this.chart.renderer.path().addClass("highcharts-crosshair highcharts-crosshair-" + (e ? "category " : "thin ") + r.className).attr({zIndex: re(r.zIndex, 2)}).add(), s.attr({
                stroke: r.color || (e ? zt("#ccd6eb").setOpacity(.25).get() : "#cccccc"),
                "stroke-width": re(r.width, 1)
            }), r.dashStyle && s.attr({dashstyle: r.dashStyle})), s.show().attr({d: i}), e && !r.width && s.attr({"stroke-width": this.transA}), this.cross.e = t) : this.hideCrosshair()) : this.hideCrosshair()
        },
        hideCrosshair: function () {
            this.cross && this.cross.hide()
        }
    },Yt(It.Axis.prototype, jt),de = (he = t).Axis,ue = he.Date,pe = he.dateFormat,fe = he.defaultOptions,ge = he.defined,me = he.each,ve = he.extend,ye = he.getMagnitude,xe = he.getTZOffset,be = he.normalizeTickInterval,we = he.pick,Ce = he.timeUnits,de.prototype.getTimeTicks = function (t, e, i, n) {
        var r, o, s, a = [], l = {}, c = fe.global.useUTC, h = new ue(e - xe(e)), d = ue.hcMakeTime, u = t.unitRange,
            p = t.count;
        if (ge(e)) {
            for (h[ue.hcSetMilliseconds](u >= Ce.second ? 0 : p * Math.floor(h.getMilliseconds() / p)), u >= Ce.second && h[ue.hcSetSeconds](u >= Ce.minute ? 0 : p * Math.floor(h.getSeconds() / p)), u >= Ce.minute && h[ue.hcSetMinutes](u >= Ce.hour ? 0 : p * Math.floor(h[ue.hcGetMinutes]() / p)), u >= Ce.hour && (h[ue.hcSetHours](u >= Ce.day ? 0 : p * Math.floor(h[ue.hcGetHours]() / p)), o = h[ue.hcGetHours]()), u >= Ce.day && h[ue.hcSetDate](u >= Ce.month ? 1 : p * Math.floor(h[ue.hcGetDate]() / p)), u >= Ce.month && (h[ue.hcSetMonth](u >= Ce.year ? 0 : p * Math.floor(h[ue.hcGetMonth]() / p)), r = h[ue.hcGetFullYear]()), u >= Ce.year && h[ue.hcSetFullYear](r - r % p), u === Ce.week && h[ue.hcSetDate](h[ue.hcGetDate]() - h[ue.hcGetDay]() + we(n, 1)), n = 1, (ue.hcTimezoneOffset || ue.hcGetTimezoneOffset) && (s = (!c || !!ue.hcGetTimezoneOffset) && (i - e > 4 * Ce.month || xe(e) !== xe(i)), h = h.getTime(), h = new ue(h + xe(h))), r = h[ue.hcGetFullYear](), e = h.getTime(), c = h[ue.hcGetMonth](), h = h[ue.hcGetDate](); e < i;) a.push(e), e = u === Ce.year ? d(r + n * p, 0) : u === Ce.month ? d(r, c + n * p) : !s || u !== Ce.day && u !== Ce.week ? s && u === Ce.hour ? d(r, c, h, o + n * p) : e + u * p : d(r, c, h + n * p * (u === Ce.day ? 1 : 7)), n++;
            a.push(e), u <= Ce.hour && me(a, function (t) {
                "000000000" === pe("%H%M%S%L", t) && (l[t] = "day")
            })
        }
        return a.info = ve(t, {higherRanks: l, totalRange: u * p}), a
    },de.prototype.normalizeTimeTickInterval = function (t, e) {
        var i = e || [["millisecond", [1, 2, 5, 10, 20, 25, 50, 100, 200, 500]], ["second", [1, 2, 5, 10, 15, 30]], ["minute", [1, 2, 5, 10, 15, 30]], ["hour", [1, 2, 3, 4, 6, 8, 12]], ["day", [1, 2]], ["week", [1, 2]], ["month", [1, 2, 3, 4, 6]], ["year", null]];
        e = i[i.length - 1];
        var n, r = Ce[e[0]], o = e[1];
        for (n = 0; n < i.length && (e = i[n], r = Ce[e[0]], o = e[1], !(i[n + 1] && t <= (r * o[o.length - 1] + Ce[i[n + 1][0]]) / 2)); n++) ;
        return r === Ce.year && t < 5 * r && (o = [1, 2, 5]), {
            unitRange: r,
            count: t = be(t / r, o, "year" === e[0] ? Math.max(ye(t / r), 1) : 1),
            unitName: e[0]
        }
    },ke = (_e = t).Axis,Se = _e.getMagnitude,Te = _e.map,Ae = _e.normalizeTickInterval,$e = _e.pick,ke.prototype.getLogTickPositions = function (t, e, i, n) {
        var r = this.options, o = this.len, s = this.lin2log, a = this.log2lin, l = [];
        if (n || (this._minorAutoInterval = null), .5 <= t) t = Math.round(t), l = this.getLinearTickPositions(t, e, i); else if (.08 <= t) {
            var c, h, d, u, p;
            for (o = Math.floor(e), r = .3 < t ? [1, 2, 4] : .15 < t ? [1, 2, 4, 6, 8] : [1, 2, 3, 4, 5, 6, 7, 8, 9]; o < i + 1 && !p; o++) for (h = r.length, c = 0; c < h && !p; c++) e < (d = a(s(o) * r[c])) && (!n || u <= i) && void 0 !== u && l.push(u), i < u && (p = !0), u = d
        } else e = s(e), i = s(i), t = r[n ? "minorTickInterval" : "tickInterval"], t = $e("auto" === t ? null : t, this._minorAutoInterval, r.tickPixelInterval / (n ? 5 : 1) * (i - e) / ((n ? o / this.tickPositions.length : o) || 1)), t = Ae(t, null, Se(t)), l = Te(this.getLinearTickPositions(t, e, i), a), n || (this._minorAutoInterval = t / 5);
        return n || (this.tickInterval = t), l
    },ke.prototype.log2lin = function (t) {
        return Math.log(t) / Math.LN10
    },ke.prototype.lin2log = function (t) {
        return Math.pow(10, t)
    },Ee = (Me = t).dateFormat,Pe = Me.each,Le = Me.extend,Ie = Me.format,Fe = Me.isNumber,De = Me.map,Oe = Me.merge,Ne = Me.pick,je = Me.splat,ze = Me.stop,Re = Me.syncTimeout,Be = Me.timeUnits,Me.Tooltip = function () {
        this.init.apply(this, arguments)
    },Me.Tooltip.prototype = {
        init: function (t, e) {
            this.chart = t, this.options = e, this.crosshairs = [], this.now = {
                x: 0,
                y: 0
            }, this.isHidden = !0, this.split = e.split && !t.inverted, this.shared = e.shared || this.split
        }, cleanSplit: function (i) {
            Pe(this.chart.series, function (t) {
                var e = t && t.tt;
                e && (!e.isActive || i ? t.tt = e.destroy() : e.isActive = !1)
            })
        }, getLabel: function () {
            var t = this.chart.renderer, e = this.options;
            return this.label || (this.split ? this.label = t.g("tooltip") : (this.label = t.label("", 0, 0, e.shape || "callout", null, null, e.useHTML, null, "tooltip").attr({
                padding: e.padding,
                r: e.borderRadius
            }), this.label.attr({
                fill: e.backgroundColor,
                "stroke-width": e.borderWidth
            }).css(e.style).shadow(e.shadow)), this.label.attr({zIndex: 8}).add()), this.label
        }, update: function (t) {
            this.destroy(), this.init(this.chart, Oe(!0, this.options, t))
        }, destroy: function () {
            this.label && (this.label = this.label.destroy()), this.split && this.tt && (this.cleanSplit(this.chart, !0), this.tt = this.tt.destroy()), clearTimeout(this.hideTimer), clearTimeout(this.tooltipTimeout)
        }, move: function (t, e, i, n) {
            var r = this, o = r.now,
                s = !1 !== r.options.animation && !r.isHidden && (1 < Math.abs(t - o.x) || 1 < Math.abs(e - o.y)),
                a = r.followPointer || 1 < r.len;
            Le(o, {
                x: s ? (2 * o.x + t) / 3 : t,
                y: s ? (o.y + e) / 2 : e,
                anchorX: a ? void 0 : s ? (2 * o.anchorX + i) / 3 : i,
                anchorY: a ? void 0 : s ? (o.anchorY + n) / 2 : n
            }), r.getLabel().attr(o), s && (clearTimeout(this.tooltipTimeout), this.tooltipTimeout = setTimeout(function () {
                r && r.move(t, e, i, n)
            }, 32))
        }, hide: function (t) {
            var e = this;
            clearTimeout(this.hideTimer), t = Ne(t, this.options.hideDelay, 500), this.isHidden || (this.hideTimer = Re(function () {
                e.getLabel()[t ? "fadeOut" : "hide"](), e.isHidden = !0
            }, t))
        }, getAnchor: function (t, e) {
            var i, n, r, o = this.chart, s = o.inverted, a = o.plotTop, l = o.plotLeft, c = 0, h = 0;
            return i = (t = je(t))[0].tooltipPos, this.followPointer && e && (void 0 === e.chartX && (e = o.pointer.normalize(e)), i = [e.chartX - o.plotLeft, e.chartY - a]), i || (Pe(t, function (t) {
                n = t.series.yAxis, r = t.series.xAxis, c += t.plotX + (!s && r ? r.left - l : 0), h += (t.plotLow ? (t.plotLow + t.plotHigh) / 2 : t.plotY) + (!s && n ? n.top - a : 0)
            }), c /= t.length, h /= t.length, i = [s ? o.plotWidth - h : c, this.shared && !s && 1 < t.length && e ? e.chartY - a : s ? o.plotHeight - c : h]), De(i, Math.round)
        }, getPosition: function (t, e, i) {
            var n, r = this.chart, c = this.distance, h = {}, d = i.h || 0,
                o = ["y", r.chartHeight, e, i.plotY + r.plotTop, r.plotTop, r.plotTop + r.plotHeight],
                s = ["x", r.chartWidth, t, i.plotX + r.plotLeft, r.plotLeft, r.plotLeft + r.plotWidth],
                u = !this.followPointer && Ne(i.ttBelow, !r.inverted == !!i.negative), a = function (t, e, i, n, r, o) {
                    var s = i < n - c, a = n + c + i < e, l = n - c - i;
                    if (n += c, u && a) h[t] = n; else if (!u && s) h[t] = l; else if (s) h[t] = Math.min(o - i, l - d < 0 ? l : l - d); else {
                        if (!a) return !1;
                        h[t] = Math.max(r, e < n + d + i ? n : n + d)
                    }
                }, l = function (t, e, i, n) {
                    var r;
                    return n < c || e - c < n ? r = !1 : h[t] = n < i / 2 ? 1 : e - i / 2 < n ? e - i - 2 : n - i / 2, r
                }, p = function (t) {
                    var e = o;
                    o = s, s = e, n = t
                }, f = function () {
                    !1 !== a.apply(0, o) ? !1 !== l.apply(0, s) || n || (p(!0), f()) : n ? h.x = h.y = 0 : (p(!0), f())
                };
            return (r.inverted || 1 < this.len) && p(), f(), h
        }, defaultFormatter: function (t) {
            var e, i = this.points || je(this);
            return (e = (e = [t.tooltipFooterHeaderFormatter(i[0])]).concat(t.bodyFormatter(i))).push(t.tooltipFooterHeaderFormatter(i[0], !0)), e
        }, refresh: function (t, e) {
            var i, n, r, o = this.chart, s = this.getLabel(), a = this.options, l = {}, c = [];
            r = a.formatter || this.defaultFormatter, l = o.hoverPoints;
            var h = this.shared;
            clearTimeout(this.hideTimer), this.followPointer = je(t)[0].series.tooltipOptions.followPointer, e = (n = this.getAnchor(t, e))[0], i = n[1], !h || t.series && t.series.noSharedTooltip ? l = t.getLabelConfig() : (o.hoverPoints = t, l && Pe(l, function (t) {
                t.setState()
            }), Pe(t, function (t) {
                t.setState("hover"), c.push(t.getLabelConfig())
            }), (l = {
                x: t[0].category,
                y: t[0].y
            }).points = c, this.len = c.length, t = t[0]), r = r.call(l, this), l = t.series, this.distance = Ne(l.tooltipOptions.distance, 16), !1 === r ? this.hide() : (this.isHidden && (ze(s), s.attr({opacity: 1}).show()), this.split ? this.renderSplit(r, o.hoverPoints) : (s.attr({text: r.join ? r.join("") : r}), s.removeClass(/highcharts-color-[\d]+/g).addClass("highcharts-color-" + Ne(t.colorIndex, l.colorIndex)), s.attr({stroke: a.borderColor || t.color || l.color || "#666666"}), this.updatePosition({
                plotX: e,
                plotY: i,
                negative: t.negative,
                ttBelow: t.ttBelow,
                h: n[2] || 0
            })), this.isHidden = !1)
        }, renderSplit: function (t, s) {
            var a, l = this, c = [], h = this.chart, d = h.renderer, u = !0, p = this.options, f = this.getLabel();
            Pe(t.slice(0, t.length - 1), function (t, e) {
                var i = (e = s[e - 1] || {isHeader: !0, plotX: s[0].plotX}).series || l, n = i.tt, r = e.series || {},
                    o = "highcharts-color-" + Ne(e.colorIndex, r.colorIndex, "none");
                n || (i.tt = n = d.label(null, null, null, e.isHeader && "callout").addClass("highcharts-tooltip-box " + o).attr({
                    padding: p.padding,
                    r: p.borderRadius,
                    fill: p.backgroundColor,
                    stroke: e.color || r.color || "#333333",
                    "stroke-width": p.borderWidth
                }).add(f), e.series && (n.connector = d.path().addClass("highcharts-tooltip-connector " + o).attr({
                    "stroke-width": r.options.lineWidth || 2,
                    stroke: e.color || r.color || "#666666"
                }).add(n))), n.isActive = !0, n.attr({text: t}), r = (t = n.getBBox()).width + n.strokeWidth(), e.isHeader ? (a = t.height, r = Math.max(0, Math.min(e.plotX + h.plotLeft - r / 2, h.chartWidth - r))) : r = e.plotX + h.plotLeft - Ne(p.distance, 16) - r, r < 0 && (u = !1), t = (e.series && e.series.yAxis && e.series.yAxis.pos) + (e.plotY || 0), t -= h.plotTop, c.push({
                    target: e.isHeader ? h.plotHeight + a : t,
                    rank: e.isHeader ? 1 : 0,
                    size: i.tt.getBBox().height + 1,
                    point: e,
                    x: r,
                    tt: n
                })
            }), this.cleanSplit(), Me.distribute(c, h.plotHeight + a), Pe(c, function (t) {
                var e, i = t.point, n = t.tt;
                e = {
                    visibility: void 0 === t.pos ? "hidden" : "inherit",
                    x: u || i.isHeader ? t.x : i.plotX + h.plotLeft + Ne(p.distance, 16),
                    y: t.pos + h.plotTop
                }, i.isHeader && (e.anchorX = i.plotX + h.plotLeft, e.anchorY = e.y - 100), n.attr(e), i.isHeader || n.connector.attr({d: ["M", i.plotX + h.plotLeft - e.x, i.plotY + i.series.yAxis.pos - e.y, "L", (u ? -1 : 1) * Ne(p.distance, 16) + i.plotX + h.plotLeft - e.x, t.pos + h.plotTop + n.getBBox().height / 2 - e.y]})
            })
        }, updatePosition: function (t) {
            var e = this.chart, i = this.getLabel();
            i = (this.options.positioner || this.getPosition).call(this, i.width, i.height, t), this.move(Math.round(i.x), Math.round(i.y || 0), t.plotX + e.plotLeft, t.plotY + e.plotTop)
        }, getXDateFormat: function (t, e, i) {
            var n;
            e = e.dateTimeLabelFormats;
            var r, o, s = i && i.closestPointRange, a = {millisecond: 15, second: 12, minute: 9, hour: 6, day: 3},
                l = "millisecond";
            if (s) {
                for (r in o = Ee("%m-%d %H:%M:%S.%L", t.x), Be) {
                    if (s === Be.week && +Ee("%w", t.x) === i.options.startOfWeek && "00:00:00.000" === o.substr(6)) {
                        r = "week";
                        break
                    }
                    if (Be[r] > s) {
                        r = l;
                        break
                    }
                    if (a[r] && o.substr(a[r]) !== "01-01 00:00:00.000".substr(a[r])) break;
                    "week" !== r && (l = r)
                }
                r && (n = e[r])
            } else n = e.day;
            return n || e.year
        }, tooltipFooterHeaderFormatter: function (t, e) {
            var i = e ? "footer" : "header", n = (e = t.series).tooltipOptions, r = n.xDateFormat, o = e.xAxis,
                s = o && "datetime" === o.options.type && Fe(t.key);
            return i = n[i + "Format"], s && !r && (r = this.getXDateFormat(t, n, o)), s && r && (i = i.replace("{point.key}", "{point.key:" + r + "}")), Ie(i, {
                point: t,
                series: e
            })
        }, bodyFormatter: function (t) {
            return De(t, function (t) {
                var e = t.series.tooltipOptions;
                return (e.pointFormatter || t.point.tooltipFormatter).call(t.point, e.pointFormat)
            })
        }
    },We = (He = t).addEvent,qe = He.attr,Ge = He.charts,Xe = He.color,Ye = He.css,Ve = He.defined,Ue = He.doc,Qe = He.each,Ke = He.extend,Ze = He.fireEvent,Je = He.offset,ti = He.pick,ei = He.removeEvent,ii = He.splat,ni = He.Tooltip,ri = He.win,He.Pointer = function (t, e) {
        this.init(t, e)
    },He.Pointer.prototype = {
        init: function (t, e) {
            this.options = e, this.chart = t, this.runChartClick = e.chart.events && !!e.chart.events.click, this.pinchDown = [], this.lastValidTouch = {}, ni && e.tooltip.enabled && (t.tooltip = new ni(t, e.tooltip), this.followTouchMove = ti(e.tooltip.followTouchMove, !0)), this.setDOMEvents()
        }, zoomOption: function () {
            var t = (i = this.chart).options.chart.zoomType, e = /x/.test(t), i = (t = /y/.test(t), i.inverted);
            this.zoomX = e, this.zoomY = t, this.zoomHor = e && !i || t && i, this.zoomVert = t && !i || e && i, this.hasZoom = e || t
        }, normalize: function (t, e) {
            var i, n;
            return (t = t || ri.event).target || (t.target = t.srcElement), n = t.touches ? t.touches.length ? t.touches.item(0) : t.changedTouches[0] : t, e || (this.chartPosition = e = Je(this.chart.container)), void 0 === n.pageX ? (i = Math.max(t.x, t.clientX - e.left), e = t.y) : (i = n.pageX - e.left, e = n.pageY - e.top), Ke(t, {
                chartX: Math.round(i),
                chartY: Math.round(e)
            })
        }, getCoordinates: function (e) {
            var i = {xAxis: [], yAxis: []};
            return Qe(this.chart.axes, function (t) {
                i[t.isXAxis ? "xAxis" : "yAxis"].push({axis: t, value: t.toValue(e[t.horiz ? "chartX" : "chartY"])})
            }), i
        }, runPointActions: function (i) {
            var t, e, n, r, o = this.chart, s = o.series, a = o.tooltip, l = !!a && a.shared, c = !0, h = o.hoverPoint,
                d = o.hoverSeries, u = [];
            if (!l && !d) for (t = 0; t < s.length; t++) !s[t].directTouch && s[t].options.stickyTracking || (s = []);
            if (d && (l ? d.noSharedTooltip : d.directTouch) && h ? u = [h] : (l || !d || d.options.stickyTracking || (s = [d]), Qe(s, function (t) {
                e = t.noSharedTooltip && l, n = !l && t.directTouch, t.visible && !e && !n && ti(t.options.enableMouseTracking, !0) && (r = t.searchPoint(i, !e && 1 === t.kdDimensions)) && r.series && u.push(r)
            }), u.sort(function (t, e) {
                var i = t.distX - e.distX, n = t.dist - e.dist;
                return 0 !== i && l ? i : 0 !== n ? n : t.series.group.zIndex > e.series.group.zIndex ? -1 : 1
            })), l) for (t = u.length; t--;) (u[t].x !== u[0].x || u[t].series.noSharedTooltip) && u.splice(t, 1);
            if (u[0] && (u[0] !== this.prevKDPoint || a && a.isHidden)) {
                if (l && !u[0].series.noSharedTooltip) {
                    for (t = 0; t < u.length; t++) u[t].onMouseOver(i, u[t] !== (d && d.directTouch && h || u[0]));
                    u.length && a && a.refresh(u.sort(function (t, e) {
                        return t.series.index - e.series.index
                    }), i)
                } else a && a.refresh(u[0], i), d && d.directTouch || u[0].onMouseOver(i);
                this.prevKDPoint = u[0], c = !1
            }
            c && (s = d && d.tooltipOptions.followPointer, a && s && !a.isHidden && (s = a.getAnchor([{}], i), a.updatePosition({
                plotX: s[0],
                plotY: s[1]
            }))), this._onDocumentMouseMove || (this._onDocumentMouseMove = function (t) {
                Ge[He.hoverChartIndex] && Ge[He.hoverChartIndex].pointer.onDocumentMouseMove(t)
            }, We(Ue, "mousemove", this._onDocumentMouseMove)), Qe(l ? u : [ti(h, u[0])], function (e) {
                Qe(o.axes, function (t) {
                    (!e || e.series && e.series[t.coll] === t) && t.drawCrosshair(i, e)
                })
            })
        }, reset: function (e, t) {
            var i = this.chart, n = i.hoverSeries, r = i.hoverPoint, o = i.hoverPoints, s = i.tooltip,
                a = s && s.shared ? o : r;
            e && a && Qe(ii(a), function (t) {
                t.series.isCartesian && void 0 === t.plotX && (e = !1)
            }), e ? s && a && (s.refresh(a), r && (r.setState(r.state, !0), Qe(i.axes, function (t) {
                t.crosshair && t.drawCrosshair(null, r)
            }))) : (r && r.onMouseOut(), o && Qe(o, function (t) {
                t.setState()
            }), n && n.onMouseOut(), s && s.hide(t), this._onDocumentMouseMove && (ei(Ue, "mousemove", this._onDocumentMouseMove), this._onDocumentMouseMove = null), Qe(i.axes, function (t) {
                t.hideCrosshair()
            }), this.hoverX = this.prevKDPoint = i.hoverPoints = i.hoverPoint = null)
        }, scaleGroups: function (e, i) {
            var n, r = this.chart;
            Qe(r.series, function (t) {
                n = e || t.getPlotBox(), t.xAxis && t.xAxis.zoomEnabled && t.group && (t.group.attr(n), t.markerGroup && (t.markerGroup.attr(n), t.markerGroup.clip(i ? r.clipRect : null)), t.dataLabelsGroup && t.dataLabelsGroup.attr(n))
            }), r.clipRect.attr(i || r.clipBox)
        }, dragStart: function (t) {
            var e = this.chart;
            e.mouseIsDown = t.type, e.cancelClick = !1, e.mouseDownX = this.mouseDownX = t.chartX, e.mouseDownY = this.mouseDownY = t.chartY
        }, drag: function (t) {
            var e, i = this.chart, n = i.options.chart, r = t.chartX, o = t.chartY, s = this.zoomHor, a = this.zoomVert,
                l = i.plotLeft, c = i.plotTop, h = i.plotWidth, d = i.plotHeight, u = this.selectionMarker,
                p = this.mouseDownX, f = this.mouseDownY, g = n.panKey && t[n.panKey + "Key"];
            u && u.touch || (r < l ? r = l : l + h < r && (r = l + h), o < c ? o = c : c + d < o && (o = c + d), this.hasDragged = Math.sqrt(Math.pow(p - r, 2) + Math.pow(f - o, 2)), 10 < this.hasDragged && (e = i.isInsidePlot(p - l, f - c), i.hasCartesianSeries && (this.zoomX || this.zoomY) && e && !g && !u && (this.selectionMarker = u = i.renderer.rect(l, c, s ? 1 : h, a ? 1 : d, 0).attr({
                fill: n.selectionMarkerFill || Xe("#335cad").setOpacity(.25).get(),
                "class": "highcharts-selection-marker",
                zIndex: 7
            }).add()), u && s && (r -= p, u.attr({
                width: Math.abs(r),
                x: (0 < r ? 0 : r) + p
            })), u && a && (r = o - f, u.attr({
                height: Math.abs(r),
                y: (0 < r ? 0 : r) + f
            })), e && !u && n.panning && i.pan(t, n.panning)))
        }, drop: function (r) {
            var o = this, e = this.chart, s = this.hasPinched;
            if (this.selectionMarker) {
                var a, l = {originalEvent: r, xAxis: [], yAxis: []}, t = this.selectionMarker,
                    c = t.attr ? t.attr("x") : t.x, h = t.attr ? t.attr("y") : t.y,
                    d = t.attr ? t.attr("width") : t.width, u = t.attr ? t.attr("height") : t.height;
                (this.hasDragged || s) && (Qe(e.axes, function (t) {
                    if (t.zoomEnabled && Ve(t.min) && (s || o[{xAxis: "zoomX", yAxis: "zoomY"}[t.coll]])) {
                        var e = t.horiz, i = "touchend" === r.type ? t.minPixelPadding : 0,
                            n = t.toValue((e ? c : h) + i);
                        e = t.toValue((e ? c + d : h + u) - i), l[t.coll].push({
                            axis: t,
                            min: Math.min(n, e),
                            max: Math.max(n, e)
                        }), a = !0
                    }
                }), a && Ze(e, "selection", l, function (t) {
                    e.zoom(Ke(t, s ? {animation: !1} : null))
                })), this.selectionMarker = this.selectionMarker.destroy(), s && this.scaleGroups()
            }
            e && (Ye(e.container, {cursor: e._cursor}), e.cancelClick = 10 < this.hasDragged, e.mouseIsDown = this.hasDragged = this.hasPinched = !1, this.pinchDown = [])
        }, onContainerMouseDown: function (t) {
            t = this.normalize(t), this.zoomOption(), t.preventDefault && t.preventDefault(), this.dragStart(t)
        }, onDocumentMouseUp: function (t) {
            Ge[He.hoverChartIndex] && Ge[He.hoverChartIndex].pointer.drop(t)
        }, onDocumentMouseMove: function (t) {
            var e = this.chart, i = this.chartPosition;
            t = this.normalize(t, i), !i || this.inClass(t.target, "highcharts-tracker") || e.isInsidePlot(t.chartX - e.plotLeft, t.chartY - e.plotTop) || this.reset()
        }, onContainerMouseLeave: function (t) {
            var e = Ge[He.hoverChartIndex];
            e && (t.relatedTarget || t.toElement) && (e.pointer.reset(), e.pointer.chartPosition = null)
        }, onContainerMouseMove: function (t) {
            var e = this.chart;
            Ve(He.hoverChartIndex) && Ge[He.hoverChartIndex] && Ge[He.hoverChartIndex].mouseIsDown || (He.hoverChartIndex = e.index), (t = this.normalize(t)).returnValue = !1, "mousedown" === e.mouseIsDown && this.drag(t), !this.inClass(t.target, "highcharts-tracker") && !e.isInsidePlot(t.chartX - e.plotLeft, t.chartY - e.plotTop) || e.openMenu || this.runPointActions(t)
        }, inClass: function (t, e) {
            for (var i; t;) {
                if (i = qe(t, "class")) {
                    if (-1 !== i.indexOf(e)) return !0;
                    if (-1 !== i.indexOf("highcharts-container")) return !1
                }
                t = t.parentNode
            }
        }, onTrackerMouseOut: function (t) {
            var e = this.chart.hoverSeries;
            t = t.relatedTarget || t.toElement, !e || !t || e.options.stickyTracking || this.inClass(t, "highcharts-tooltip") || this.inClass(t, "highcharts-series-" + e.index) && this.inClass(t, "highcharts-tracker") || e.onMouseOut()
        }, onContainerClick: function (t) {
            var e = this.chart, i = e.hoverPoint, n = e.plotLeft, r = e.plotTop;
            t = this.normalize(t), e.cancelClick || (i && this.inClass(t.target, "highcharts-tracker") ? (Ze(i.series, "click", Ke(t, {point: i})), e.hoverPoint && i.firePointEvent("click", t)) : (Ke(t, this.getCoordinates(t)), e.isInsidePlot(t.chartX - n, t.chartY - r) && Ze(e, "click", t)))
        }, setDOMEvents: function () {
            var e = this, t = e.chart.container;
            t.onmousedown = function (t) {
                e.onContainerMouseDown(t)
            }, t.onmousemove = function (t) {
                e.onContainerMouseMove(t)
            }, t.onclick = function (t) {
                e.onContainerClick(t)
            }, We(t, "mouseleave", e.onContainerMouseLeave), 1 === He.chartCount && We(Ue, "mouseup", e.onDocumentMouseUp), He.hasTouch && (t.ontouchstart = function (t) {
                e.onContainerTouchStart(t)
            }, t.ontouchmove = function (t) {
                e.onContainerTouchMove(t)
            }, 1 === He.chartCount && We(Ue, "touchend", e.onDocumentTouchEnd))
        }, destroy: function () {
            var t;
            for (t in ei(this.chart.container, "mouseleave", this.onContainerMouseLeave), He.chartCount || (ei(Ue, "mouseup", this.onDocumentMouseUp), ei(Ue, "touchend", this.onDocumentTouchEnd)), clearInterval(this.tooltipTimeout), this) this[t] = null
        }
    },si = (oi = t).charts,ai = oi.each,li = oi.extend,ci = oi.map,hi = oi.noop,di = oi.pick,li(oi.Pointer.prototype, {
        pinchTranslate: function (t, e, i, n, r, o) {
            (this.zoomHor || this.pinchHor) && this.pinchTranslateDirection(!0, t, e, i, n, r, o), (this.zoomVert || this.pinchVert) && this.pinchTranslateDirection(!1, t, e, i, n, r, o)
        }, pinchTranslateDirection: function (t, e, i, n, r, o, s, a) {
            var l, c, h, d = this.chart, u = t ? "x" : "y", p = t ? "X" : "Y", f = "chart" + p,
                g = t ? "width" : "height", m = d["plot" + (t ? "Left" : "Top")], v = a || 1, y = d.inverted,
                x = d.bounds[t ? "h" : "v"], b = 1 === e.length, w = e[0][f], C = i[0][f], _ = !b && e[1][f],
                k = !b && i[1][f];
            (i = function () {
                !b && 20 < Math.abs(w - _) && (v = a || Math.abs(C - k) / Math.abs(w - _)), c = (m - C) / v + w, l = d["plot" + (t ? "Width" : "Height")] / v
            })(), (e = c) < x.min ? (e = x.min, h = !0) : e + l > x.max && (e = x.max - l, h = !0), h ? (C -= .8 * (C - s[u][0]), b || (k -= .8 * (k - s[u][1])), i()) : s[u] = [C, k], y || (o[u] = c - m, o[g] = l), o = y ? 1 / v : v, r[g] = l, r[u] = e, n[y ? t ? "scaleY" : "scaleX" : "scale" + p] = v, n["translate" + p] = o * m + (C - o * w)
        }, pinch: function (t) {
            var e = this, s = e.chart, i = e.pinchDown, n = t.touches, r = n.length, o = e.lastValidTouch,
                a = e.hasZoom, l = e.selectionMarker, c = {},
                h = 1 === r && (e.inClass(t.target, "highcharts-tracker") && s.runTrackerClick || e.runChartClick),
                d = {};
            1 < r && (e.initiated = !0), a && e.initiated && !h && t.preventDefault(), ci(n, function (t) {
                return e.normalize(t)
            }), "touchstart" === t.type ? (ai(n, function (t, e) {
                i[e] = {chartX: t.chartX, chartY: t.chartY}
            }), o.x = [i[0].chartX, i[1] && i[1].chartX], o.y = [i[0].chartY, i[1] && i[1].chartY], ai(s.axes, function (t) {
                if (t.zoomEnabled) {
                    var e = s.bounds[t.horiz ? "h" : "v"], i = t.minPixelPadding,
                        n = t.toPixels(di(t.options.min, t.dataMin)), r = t.toPixels(di(t.options.max, t.dataMax)),
                        o = Math.max(n, r);
                    e.min = Math.min(t.pos, Math.min(n, r) - i), e.max = Math.max(t.pos + t.len, o + i)
                }
            }), e.res = !0) : i.length && (l || (e.selectionMarker = l = li({
                destroy: hi,
                touch: !0
            }, s.plotBox)), e.pinchTranslate(i, n, c, l, d, o), e.hasPinched = a, e.scaleGroups(c, d), !a && e.followTouchMove && 1 === r ? this.runPointActions(e.normalize(t)) : e.res && (e.res = !1, this.reset(!1, 0)))
        }, touch: function (t, e) {
            var i, n = this.chart;
            oi.hoverChartIndex = n.index, 1 === t.touches.length ? (t = this.normalize(t), n.isInsidePlot(t.chartX - n.plotLeft, t.chartY - n.plotTop) && !n.openMenu ? (e && this.runPointActions(t), "touchmove" === t.type && (i = !!(e = this.pinchDown)[0] && 4 <= Math.sqrt(Math.pow(e[0].chartX - t.chartX, 2) + Math.pow(e[0].chartY - t.chartY, 2))), di(i, !0) && this.pinch(t)) : e && this.reset()) : 2 === t.touches.length && this.pinch(t)
        }, onContainerTouchStart: function (t) {
            this.zoomOption(), this.touch(t, !0)
        }, onContainerTouchMove: function (t) {
            this.touch(t)
        }, onDocumentTouchEnd: function (t) {
            si[oi.hoverChartIndex] && si[oi.hoverChartIndex].pointer.drop(t)
        }
    }),function (r) {
        var e = r.addEvent, o = r.charts, n = r.css, i = r.doc, t = r.extend, s = r.noop, a = r.Pointer,
            l = r.removeEvent, c = r.win, h = r.wrap;
        if (c.PointerEvent || c.MSPointerEvent) {
            var d = {}, u = !!c.PointerEvent, p = function () {
                var t, e = [];
                for (t in e.item = function (t) {
                    return this[t]
                }, d) d.hasOwnProperty(t) && e.push({pageX: d[t].pageX, pageY: d[t].pageY, target: d[t].target});
                return e
            }, f = function (t, e, i, n) {
                "touch" !== t.pointerType && t.pointerType !== t.MSPOINTER_TYPE_TOUCH || !o[r.hoverChartIndex] || (n(t), (n = o[r.hoverChartIndex].pointer)[e]({
                    type: i,
                    target: t.currentTarget,
                    preventDefault: s,
                    touches: p()
                }))
            };
            t(a.prototype, {
                onContainerPointerDown: function (t) {
                    f(t, "onContainerTouchStart", "touchstart", function (t) {
                        d[t.pointerId] = {pageX: t.pageX, pageY: t.pageY, target: t.currentTarget}
                    })
                }, onContainerPointerMove: function (t) {
                    f(t, "onContainerTouchMove", "touchmove", function (t) {
                        d[t.pointerId] = {
                            pageX: t.pageX,
                            pageY: t.pageY
                        }, d[t.pointerId].target || (d[t.pointerId].target = t.currentTarget)
                    })
                }, onDocumentPointerUp: function (t) {
                    f(t, "onDocumentTouchEnd", "touchend", function (t) {
                        delete d[t.pointerId]
                    })
                }, batchMSEvents: function (t) {
                    t(this.chart.container, u ? "pointerdown" : "MSPointerDown", this.onContainerPointerDown), t(this.chart.container, u ? "pointermove" : "MSPointerMove", this.onContainerPointerMove), t(i, u ? "pointerup" : "MSPointerUp", this.onDocumentPointerUp)
                }
            }), h(a.prototype, "init", function (t, e, i) {
                t.call(this, e, i), this.hasZoom && n(e.container, {"-ms-touch-action": "none", "touch-action": "none"})
            }), h(a.prototype, "setDOMEvents", function (t) {
                t.apply(this), (this.hasZoom || this.followTouchMove) && this.batchMSEvents(e)
            }), h(a.prototype, "destroy", function (t) {
                this.batchMSEvents(l), t.call(this)
            })
        }
    }(t),fi = (ui = t).addEvent,gi = ui.css,mi = ui.discardElement,vi = ui.defined,yi = ui.each,xi = ui.extend,bi = ui.isFirefox,wi = ui.marginNames,Ci = ui.merge,_i = ui.pick,ki = ui.setAnimation,Si = ui.stableSort,Ti = ui.win,Ai = ui.wrap,(pi = ui.Legend = function (t, e) {
        this.init(t, e)
    }).prototype = {
        init: function (t, e) {
            this.chart = t, this.setOptions(e), e.enabled && (this.render(), fi(this.chart, "endResize", function () {
                this.legend.positionCheckboxes()
            }))
        }, setOptions: function (t) {
            var e = _i(t.padding, 8);
            this.options = t, this.itemStyle = t.itemStyle, this.itemHiddenStyle = Ci(this.itemStyle, t.itemHiddenStyle), this.itemMarginTop = t.itemMarginTop || 0, this.initialItemX = this.padding = e, this.initialItemY = e - 5, this.itemHeight = this.maxItemWidth = 0, this.symbolWidth = _i(t.symbolWidth, 16), this.pages = []
        }, update: function (t, e) {
            var i = this.chart;
            this.setOptions(Ci(!0, this.options, t)), this.destroy(), i.isDirtyLegend = i.isDirtyBox = !0, _i(e, !0) && i.redraw()
        }, colorizeItem: function (t, e) {
            t.legendGroup[e ? "removeClass" : "addClass"]("highcharts-legend-item-hidden");
            var i, n = this.options, r = t.legendItem, o = t.legendLine, s = t.legendSymbol,
                a = this.itemHiddenStyle.color, l = (n = e ? n.itemStyle.color : a, e && t.color || a),
                c = t.options && t.options.marker, h = {fill: l};
            if (r && r.css({fill: n, color: n}), o && o.attr({stroke: l}), s) {
                if (c && s.isMarker && (h = t.pointAttribs(), !e)) for (i in h) h[i] = a;
                s.attr(h)
            }
        }, positionItem: function (t) {
            var e = (i = this.options).symbolPadding, i = !i.rtl, n = (r = t._legendItemPos)[0], r = r[1],
                o = t.checkbox;
            (t = t.legendGroup) && t.element && t.translate(i ? n : this.legendWidth - n - 2 * e - 4, r), o && (o.x = n, o.y = r)
        }, destroyItem: function (e) {
            var t = e.checkbox;
            yi(["legendItem", "legendLine", "legendSymbol", "legendGroup"], function (t) {
                e[t] && (e[t] = e[t].destroy())
            }), t && mi(e.checkbox)
        }, destroy: function () {
            var t = this.group, e = this.box;
            e && (this.box = e.destroy()), yi(this.getAllItems(), function (e) {
                yi(["legendItem", "legendGroup"], function (t) {
                    e[t] && (e[t] = e[t].destroy())
                })
            }), t && (this.group = t.destroy())
        }, positionCheckboxes: function (n) {
            var r, o = this.group.alignAttr, s = this.clipHeight || this.legendHeight, a = this.titleHeight;
            o && (r = o.translateY, yi(this.allItems, function (t) {
                var e, i = t.checkbox;
                i && (e = r + a + i.y + (n || 0) + 3, gi(i, {
                    left: o.translateX + t.checkboxOffset + i.x - 20 + "px",
                    top: e + "px",
                    display: r - 6 < e && e < r + s - 6 ? "" : "none"
                }))
            }))
        }, renderTitle: function () {
            var t = this.padding, e = this.options.title, i = 0;
            e.text && (this.title || (this.title = this.chart.renderer.label(e.text, t - 3, t - 4, null, null, null, null, null, "legend-title").attr({zIndex: 1}).css(e.style).add(this.group)), i = (t = this.title.getBBox()).height, this.offsetWidth = t.width, this.contentGroup.attr({translateY: i})), this.titleHeight = i
        }, setText: function (t) {
            var e = this.options;
            t.legendItem.attr({text: e.labelFormat ? ui.format(e.labelFormat, t) : e.labelFormatter.call(t)})
        }, renderItem: function (t) {
            var e = this.chart, i = e.renderer, n = this.options, r = "horizontal" === n.layout, o = this.symbolWidth,
                s = n.symbolPadding, a = this.itemStyle, l = this.itemHiddenStyle, c = this.padding,
                h = r ? _i(n.itemDistance, 20) : 0, d = !n.rtl, u = n.width, p = n.itemMarginBottom || 0,
                f = this.itemMarginTop, g = this.initialItemX, m = t.legendItem, v = !t.series,
                y = !v && t.series.drawLegendSymbol ? t.series : t, x = y.options,
                b = (x = this.createCheckboxForItem && x && x.showCheckbox, n.useHTML);
            m || (t.legendGroup = i.g("legend-item").addClass("highcharts-" + y.type + "-series highcharts-color-" + t.colorIndex + " " + (t.options.className || "") + (v ? "highcharts-series-" + t.index : "")).attr({zIndex: 1}).add(this.scrollGroup), t.legendItem = m = i.text("", d ? o + s : -s, this.baseline || 0, b).css(Ci(t.visible ? a : l)).attr({
                align: d ? "left" : "right",
                zIndex: 2
            }).add(t.legendGroup), this.baseline || (a = a.fontSize, this.fontMetrics = i.fontMetrics(a, m), this.baseline = this.fontMetrics.f + 3 + f, m.attr("y", this.baseline)), y.drawLegendSymbol(this, t), this.setItemEvents && this.setItemEvents(t, m, b), x && this.createCheckboxForItem(t)), this.colorizeItem(t, t.visible), this.setText(t), i = m.getBBox(), o = t.checkboxOffset = n.itemWidth || t.legendItemWidth || o + s + i.width + h + (x ? 20 : 0), this.itemHeight = s = Math.round(t.legendItemHeight || i.height), r && this.itemX - g + o > (u || e.chartWidth - 2 * c - g - n.x) && (this.itemX = g, this.itemY += f + this.lastLineHeight + p, this.lastLineHeight = 0), this.maxItemWidth = Math.max(this.maxItemWidth, o), this.lastItemY = f + this.itemY + p, this.lastLineHeight = Math.max(s, this.lastLineHeight), t._legendItemPos = [this.itemX, this.itemY], r ? this.itemX += o : (this.itemY += f + s + p, this.lastLineHeight = s), this.offsetWidth = u || Math.max((r ? this.itemX - g - h : o) + c, this.offsetWidth)
        }, getAllItems: function () {
            var i = [];
            return yi(this.chart.series, function (t) {
                var e = t && t.options;
                t && _i(e.showInLegend, !vi(e.linkedTo) && void 0, !0) && (i = i.concat(t.legendItems || ("point" === e.legendType ? t.data : t)))
            }), i
        }, adjustMargins: function (i, n) {
            var r = this.chart, o = this.options,
                s = o.align.charAt(0) + o.verticalAlign.charAt(0) + o.layout.charAt(0);
            o.floating || yi([/(lth|ct|rth)/, /(rtv|rm|rbv)/, /(rbh|cb|lbh)/, /(lbv|lm|ltv)/], function (t, e) {
                t.test(s) && !vi(i[e]) && (r[wi[e]] = Math.max(r[wi[e]], r.legend[(e + 1) % 2 ? "legendHeight" : "legendWidth"] + [1, -1, -1, 1][e] * o[e % 2 ? "x" : "y"] + _i(o.margin, 12) + n[e]))
            })
        }, render: function () {
            var t, e, i, n, r = this, o = r.chart, s = o.renderer, a = r.group, l = r.box, c = r.options, h = r.padding;
            r.itemX = r.initialItemX, r.itemY = r.initialItemY, r.offsetWidth = 0, r.lastItemY = 0, a || (r.group = a = s.g("legend").attr({zIndex: 7}).add(), r.contentGroup = s.g().attr({zIndex: 1}).add(a), r.scrollGroup = s.g().add(r.contentGroup)), r.renderTitle(), t = r.getAllItems(), Si(t, function (t, e) {
                return (t.options && t.options.legendIndex || 0) - (e.options && e.options.legendIndex || 0)
            }), c.reversed && t.reverse(), r.allItems = t, r.display = e = !!t.length, r.lastLineHeight = 0, yi(t, function (t) {
                r.renderItem(t)
            }), i = (c.width || r.offsetWidth) + h, n = r.lastItemY + r.lastLineHeight + r.titleHeight, n = r.handleOverflow(n), n += h, l || (r.box = l = s.rect().addClass("highcharts-legend-box").attr({r: c.borderRadius}).add(a), l.isNew = !0), l.attr({
                stroke: c.borderColor,
                "stroke-width": c.borderWidth || 0,
                fill: c.backgroundColor || "none"
            }).shadow(c.shadow), 0 < i && 0 < n && (
                l[l.isNew ? "attr" : "animate"](l.crisp({
                    x: 0,
                    y: 0,
                    width: i,
                    height: n
                }, l.strokeWidth())), l.isNew = !1), l[e ? "show" : "hide"](), r.legendWidth = i, r.legendHeight = n, yi(t, function (t) {
                r.positionItem(t)
            }), e && a.align(xi({width: i, height: n}, c), !0, "spacingBox"), o.isResizing || this.positionCheckboxes()
        }, handleOverflow: function (t) {
            var r, o, e = this, i = this.chart, n = i.renderer, s = this.options, a = s.y,
                l = (a = i.spacingBox.height + ("top" === s.verticalAlign ? -a : a) - this.padding, s.maxHeight),
                c = this.clipRect, h = s.navigation, d = _i(h.animation, !0), u = h.arrowSize || 12, p = this.nav,
                f = this.pages, g = this.padding, m = this.allItems, v = function (t) {
                    c.attr({height: t}), e.contentGroup.div && (e.contentGroup.div.style.clip = "rect(" + g + "px,9999px," + (g + t) + "px,0)")
                };
            return "horizontal" === s.layout && (a /= 2), l && (a = Math.min(a, l)), f.length = 0, a < t && !1 !== h.enabled ? (this.clipHeight = r = Math.max(a - 20 - this.titleHeight - g, 0), this.currentPage = _i(this.currentPage, 1), this.fullHeight = t, yi(m, function (t, e) {
                var i = t._legendItemPos[1];
                t = Math.round(t.legendItem.getBBox().height);
                var n = f.length;
                (!n || i - f[n - 1] > r && (o || i) !== f[n - 1]) && (f.push(o || i), n++), e === m.length - 1 && i + t - f[n - 1] > r && f.push(i), i !== o && (o = i)
            }), c || (c = e.clipRect = n.clipRect(0, g, 9999, 0), e.contentGroup.clip(c)), v(r), p || (this.nav = p = n.g().attr({zIndex: 1}).add(this.group), this.up = n.symbol("triangle", 0, 0, u, u).on("click", function () {
                e.scroll(-1, d)
            }).add(p), this.pager = n.text("", 15, 10).addClass("highcharts-legend-navigation").css(h.style).add(p), this.down = n.symbol("triangle-down", 0, 0, u, u).on("click", function () {
                e.scroll(1, d)
            }).add(p)), e.scroll(0), t = a) : p && (v(i.chartHeight), p.hide(), this.scrollGroup.attr({translateY: 1}), this.clipHeight = 0), t
        }, scroll: function (t, e) {
            var i = this.pages, n = i.length;
            t = this.currentPage + t;
            var r = this.clipHeight, o = this.options.navigation, s = this.pager, a = this.padding;
            n < t && (t = n), 0 < t && (void 0 !== e && ki(e, this.chart), this.nav.attr({
                translateX: a,
                translateY: r + this.padding + 7 + this.titleHeight,
                visibility: "visible"
            }), this.up.attr({"class": 1 === t ? "highcharts-legend-nav-inactive" : "highcharts-legend-nav-active"}), s.attr({text: t + "/" + n}), this.down.attr({
                x: 18 + this.pager.getBBox().width,
                "class": t === n ? "highcharts-legend-nav-inactive" : "highcharts-legend-nav-active"
            }), this.up.attr({fill: 1 === t ? o.inactiveColor : o.activeColor}).css({cursor: 1 === t ? "default" : "pointer"}), this.down.attr({fill: t === n ? o.inactiveColor : o.activeColor}).css({cursor: t === n ? "default" : "pointer"}), e = -i[t - 1] + this.initialItemY, this.scrollGroup.animate({translateY: e}), this.currentPage = t, this.positionCheckboxes(e))
        }
    },ui.LegendSymbolMixin = {
        drawRectangle: function (t, e) {
            var i = (n = t.options).symbolHeight || t.fontMetrics.f, n = n.squareSymbol;
            e.legendSymbol = this.chart.renderer.rect(n ? (t.symbolWidth - i) / 2 : 0, t.baseline - i + 1, n ? i : t.symbolWidth, i, _i(t.options.symbolRadius, i / 2)).addClass("highcharts-point").attr({zIndex: 3}).add(e.legendGroup)
        }, drawLineMarker: function (t) {
            var e, i = this.options, n = i.marker, r = t.symbolWidth, o = this.chart.renderer, s = this.legendGroup;
            t = t.baseline - Math.round(.3 * t.fontMetrics.b), e = {"stroke-width": i.lineWidth || 0}, i.dashStyle && (e.dashstyle = i.dashStyle), this.legendLine = o.path(["M", 0, t, "L", r, t]).addClass("highcharts-graph").attr(e).add(s), n && !1 !== n.enabled && (i = 0 === this.symbol.indexOf("url") ? 0 : n.radius, this.legendSymbol = n = o.symbol(this.symbol, r / 2 - i, t - i, 2 * i, 2 * i, n).addClass("highcharts-point").add(s), n.isMarker = !0)
        }
    },(/Trident\/7\.0/.test(Ti.navigator.userAgent) || bi) && Ai(pi.prototype, "positionItem", function (t, e) {
        var i = this, n = function () {
            e._legendItemPos && t.call(i, e)
        };
        n(), setTimeout(n)
    }),Mi = ($i = t).addEvent,Ei = $i.animate,Pi = $i.animObject,Li = $i.attr,Ii = $i.doc,Fi = $i.Axis,Di = $i.createElement,Oi = $i.defaultOptions,Ni = $i.discardElement,ji = $i.charts,zi = $i.css,Ri = $i.defined,Bi = $i.each,Hi = $i.error,Wi = $i.extend,qi = $i.fireEvent,Gi = $i.getStyle,Xi = $i.grep,Yi = $i.isNumber,Vi = $i.isObject,Ui = $i.isString,Qi = $i.Legend,Ki = $i.marginNames,Zi = $i.merge,Ji = $i.Pointer,tn = $i.pick,en = $i.pInt,nn = $i.removeEvent,rn = $i.seriesTypes,on = $i.splat,sn = $i.svg,an = $i.syncTimeout,ln = $i.win,cn = $i.Renderer,hn = $i.Chart = function () {
        this.getArgs.apply(this, arguments)
    },$i.chart = function (t, e, i) {
        return new hn(t, e, i)
    },hn.prototype = {
        callbacks: [], getArgs: function () {
            var t = [].slice.call(arguments);
            (Ui(t[0]) || t[0].nodeName) && (this.renderTo = t.shift()), this.init(t[0], t[1])
        }, init: function (t, e) {
            var i, n, r = t.series;
            if (t.series = null, (i = Zi(Oi, t)).series = t.series = r, this.userOptions = t, this.respRules = [], r = (t = i.chart).events, this.margin = [], this.spacing = [], this.bounds = {
                h: {},
                v: {}
            }, this.callback = e, this.isResizing = 0, this.options = i, this.axes = [], this.series = [], this.hasCartesianSeries = t.showAxes, this.index = ji.length, ji.push(this), $i.chartCount++, r) for (n in r) Mi(this, n, r[n]);
            this.xAxis = [], this.yAxis = [], this.pointCount = this.colorCounter = this.symbolCounter = 0, this.firstRender()
        }, initSeries: function (t) {
            var e = this.options.chart;
            return (e = rn[t.type || e.type || e.defaultSeriesType]) || Hi(17, !0), (e = new e).init(this, t), e
        }, isInsidePlot: function (t, e, i) {
            var n = i ? e : t;
            return t = i ? t : e, 0 <= n && n <= this.plotWidth && 0 <= t && t <= this.plotHeight
        }, redraw: function (t) {
            var i, e, n = this.axes, r = this.series, o = this.pointer, s = this.legend, a = this.isDirtyLegend,
                l = this.hasCartesianSeries, c = this.isDirtyBox, h = r.length, d = h, u = this.renderer,
                p = u.isHidden(), f = [];
            for ($i.setAnimation(t, this), p && this.cloneRenderTo(), this.layOutTitles(); d--;) if ((t = r[d]).options.stacking && (i = !0, t.isDirty)) {
                e = !0;
                break
            }
            if (e) for (d = h; d--;) (t = r[d]).options.stacking && (t.isDirty = !0);
            Bi(r, function (t) {
                t.isDirty && "point" === t.options.legendType && (t.updateTotals && t.updateTotals(), a = !0), t.isDirtyData && qi(t, "updatedData")
            }), a && s.options.enabled && (s.render(), this.isDirtyLegend = !1), i && this.getStacks(), l && Bi(n, function (t) {
                t.updateNames(), t.setScale()
            }), this.getMargins(), l && (Bi(n, function (t) {
                t.isDirty && (c = !0)
            }), Bi(n, function (t) {
                var e = t.min + "," + t.max;
                t.extKey !== e && (t.extKey = e, f.push(function () {
                    qi(t, "afterSetExtremes", Wi(t.eventArgs, t.getExtremes())), delete t.eventArgs
                })), (c || i) && t.redraw()
            })), c && this.drawChartBox(), Bi(r, function (t) {
                (c || t.isDirty) && t.visible && t.redraw()
            }), o && o.reset(!0), u.draw(), qi(this, "redraw"), p && this.cloneRenderTo(!0), Bi(f, function (t) {
                t.call()
            })
        }, get: function (t) {
            var e, i, n = this.axes, r = this.series;
            for (e = 0; e < n.length; e++) if (n[e].options.id === t) return n[e];
            for (e = 0; e < r.length; e++) if (r[e].options.id === t) return r[e];
            for (e = 0; e < r.length; e++) for (i = r[e].points || [], n = 0; n < i.length; n++) if (i[n].id === t) return i[n];
            return null
        }, getAxes: function () {
            var e = this, t = (i = this.options).xAxis = on(i.xAxis || {}), i = i.yAxis = on(i.yAxis || {});
            Bi(t, function (t, e) {
                t.index = e, t.isX = !0
            }), Bi(i, function (t, e) {
                t.index = e
            }), t = t.concat(i), Bi(t, function (t) {
                new Fi(e, t)
            })
        }, getSelectedPoints: function () {
            var e = [];
            return Bi(this.series, function (t) {
                e = e.concat(Xi(t.points || [], function (t) {
                    return t.selected
                }))
            }), e
        }, getSelectedSeries: function () {
            return Xi(this.series, function (t) {
                return t.selected
            })
        }, setTitle: function (t, e, i) {
            var n, o = this, r = o.options;
            n = r.title = Zi(r.title, t), r = r.subtitle = Zi(r.subtitle, e), Bi([["title", t, n], ["subtitle", e, r]], function (t, e) {
                var i = t[0], n = o[i], r = t[1];
                t = t[2], n && r && (o[i] = n = n.destroy()), t && t.text && !n && (o[i] = o.renderer.text(t.text, 0, 0, t.useHTML).attr({
                    align: t.align,
                    "class": "highcharts-" + i,
                    zIndex: t.zIndex || 4
                }).add(), o[i].update = function (t) {
                    o.setTitle(!e && t, e && t)
                }, o[i].css(t.style))
            }), o.layOutTitles(i)
        }, layOutTitles: function (t) {
            var e, r = 0, o = this.renderer, s = this.spacingBox;
            Bi(["title", "subtitle"], function (t) {
                var e, i = this[t], n = this.options[t];
                i && (e = n.style.fontSize, e = o.fontMetrics(e, i).b, i.css({width: (n.width || s.width + n.widthAdjust) + "px"}).align(Wi({y: r + e + ("title" === t ? -3 : 2)}, n), !1, "spacingBox"), n.floating || n.verticalAlign || (r = Math.ceil(r + i.getBBox().height)))
            }, this), e = this.titleOffset !== r, this.titleOffset = r, !this.isDirtyBox && e && (this.isDirtyBox = e, this.hasRendered && tn(t, !0) && this.isDirtyBox && this.redraw())
        }, getChartSize: function () {
            var t = (e = this.options.chart).width, e = e.height, i = this.renderToClone || this.renderTo;
            Ri(t) || (this.containerWidth = Gi(i, "width")), Ri(e) || (this.containerHeight = Gi(i, "height")), this.chartWidth = Math.max(0, t || this.containerWidth || 600), this.chartHeight = Math.max(0, tn(e, 19 < this.containerHeight ? this.containerHeight : 400))
        }, cloneRenderTo: function (t) {
            var e = this.renderToClone, i = this.container;
            if (t) {
                if (e) {
                    for (; e.childNodes.length;) this.renderTo.appendChild(e.firstChild);
                    Ni(e), delete this.renderToClone
                }
            } else i && i.parentNode === this.renderTo && this.renderTo.removeChild(i), this.renderToClone = e = this.renderTo.cloneNode(0), zi(e, {
                position: "absolute",
                top: "-9999px",
                display: "block"
            }), e.style.setProperty && e.style.setProperty("display", "block", "important"), Ii.body.appendChild(e), i && e.appendChild(i)
        }, setClassName: function (t) {
            this.container.className = "highcharts-container " + (t || "")
        }, getContainer: function () {
            var t, e, i, n = this.options, r = n.chart;
            t = this.renderTo;
            var o, s = "highcharts-" + $i.idCounter++;
            t || (this.renderTo = t = r.renderTo), Ui(t) && (this.renderTo = t = Ii.getElementById(t)), t || Hi(13, !0), e = en(Li(t, "data-highcharts-chart")), Yi(e) && ji[e] && ji[e].hasRendered && ji[e].destroy(), Li(t, "data-highcharts-chart", this.index), t.innerHTML = "", r.skipClone || t.offsetWidth || this.cloneRenderTo(), this.getChartSize(), e = this.chartWidth, i = this.chartHeight, o = Wi({
                position: "relative",
                overflow: "hidden",
                width: e + "px",
                height: i + "px",
                textAlign: "left",
                lineHeight: "normal",
                zIndex: 0,
                "-webkit-tap-highlight-color": "rgba(0,0,0,0)"
            }, r.style), this.container = t = Di("div", {id: s}, o, this.renderToClone || t), this._cursor = t.style.cursor, this.renderer = new ($i[r.renderer] || cn)(t, e, i, null, r.forExport, n.exporting && n.exporting.allowHTML), this.setClassName(r.className), this.renderer.setStyle(r.style), this.renderer.chartIndex = this.index
        }, getMargins: function (t) {
            var e = this.spacing, i = this.margin, n = this.titleOffset;
            this.resetMargins(), n && !Ri(i[0]) && (this.plotTop = Math.max(this.plotTop, n + this.options.title.margin + e[0])), this.legend.display && this.legend.adjustMargins(i, e), this.extraBottomMargin && (this.marginBottom += this.extraBottomMargin), this.extraTopMargin && (this.plotTop += this.extraTopMargin), t || this.getAxisMargins()
        }, getAxisMargins: function () {
            var i = this, n = i.axisOffset = [0, 0, 0, 0], r = i.margin;
            i.hasCartesianSeries && Bi(i.axes, function (t) {
                t.visible && t.getOffset()
            }), Bi(Ki, function (t, e) {
                Ri(r[e]) || (i[t] += n[e])
            }), i.setChartSize()
        }, reflow: function (t) {
            var e = this, i = e.options.chart, n = e.renderTo, r = Ri(i.width), o = i.width || Gi(n, "width");
            i = i.height || Gi(n, "height"), n = t ? t.target : ln, r || e.isPrinting || !o || !i || n !== ln && n !== Ii || (o === e.containerWidth && i === e.containerHeight || (clearTimeout(e.reflowTimeout), e.reflowTimeout = an(function () {
                e.container && e.setSize(void 0, void 0, !1)
            }, t ? 100 : 0)), e.containerWidth = o, e.containerHeight = i)
        }, initReflow: function () {
            var e = this, t = function (t) {
                e.reflow(t)
            };
            Mi(ln, "resize", t), Mi(e, "destroy", function () {
                nn(ln, "resize", t)
            })
        }, setSize: function (t, e, i) {
            var n = this, r = n.renderer;
            n.isResizing += 1, $i.setAnimation(i, n), n.oldChartHeight = n.chartHeight, n.oldChartWidth = n.chartWidth, void 0 !== t && (n.options.chart.width = t), void 0 !== e && (n.options.chart.height = e), n.getChartSize(), ((t = r.globalAnimation) ? Ei : zi)(n.container, {
                width: n.chartWidth + "px",
                height: n.chartHeight + "px"
            }, t), n.setChartSize(!0), r.setSize(n.chartWidth, n.chartHeight, i), Bi(n.axes, function (t) {
                t.isDirty = !0, t.setScale()
            }), n.isDirtyLegend = !0, n.isDirtyBox = !0, n.layOutTitles(), n.getMargins(), n.setResponsive && n.setResponsive(!1), n.redraw(i), n.oldChartHeight = null, qi(n, "resize"), an(function () {
                n && qi(n, "endResize", null, function () {
                    --n.isResizing
                })
            }, Pi(t).duration)
        }, setChartSize: function (t) {
            var e, i, n, r, o = this.inverted, s = this.renderer, a = this.chartWidth, l = this.chartHeight,
                c = this.options.chart, h = this.spacing, d = this.clipOffset;
            this.plotLeft = e = Math.round(this.plotLeft), this.plotTop = i = Math.round(this.plotTop), this.plotWidth = n = Math.max(0, Math.round(a - e - this.marginRight)), this.plotHeight = r = Math.max(0, Math.round(l - i - this.marginBottom)), this.plotSizeX = o ? r : n, this.plotSizeY = o ? n : r, this.plotBorderWidth = c.plotBorderWidth || 0, this.spacingBox = s.spacingBox = {
                x: h[3],
                y: h[0],
                width: a - h[3] - h[1],
                height: l - h[0] - h[2]
            }, this.plotBox = s.plotBox = {
                x: e,
                y: i,
                width: n,
                height: r
            }, a = 2 * Math.floor(this.plotBorderWidth / 2), o = Math.ceil(Math.max(a, d[3]) / 2), s = Math.ceil(Math.max(a, d[0]) / 2), this.clipBox = {
                x: o,
                y: s,
                width: Math.floor(this.plotSizeX - Math.max(a, d[1]) / 2 - o),
                height: Math.max(0, Math.floor(this.plotSizeY - Math.max(a, d[2]) / 2 - s))
            }, t || Bi(this.axes, function (t) {
                t.setAxisSize(), t.setAxisTranslation()
            })
        }, resetMargins: function () {
            var r = this, o = r.options.chart;
            Bi(["margin", "spacing"], function (i) {
                var t = o[i], n = Vi(t) ? t : [t, t, t, t];
                Bi(["Top", "Right", "Bottom", "Left"], function (t, e) {
                    r[i][e] = tn(o[i + t], n[e])
                })
            }), Bi(Ki, function (t, e) {
                r[t] = tn(r.margin[e], r.spacing[e])
            }), r.axisOffset = [0, 0, 0, 0], r.clipOffset = [0, 0, 0, 0]
        }, drawChartBox: function () {
            var t, e, i = this.options.chart, n = this.renderer, r = this.chartWidth, o = this.chartHeight,
                s = this.chartBackground, a = this.plotBackground, l = this.plotBorder, c = this.plotBGImage,
                h = i.backgroundColor, d = i.plotBackgroundColor, u = i.plotBackgroundImage, p = this.plotLeft,
                f = this.plotTop, g = this.plotWidth, m = this.plotHeight, v = this.plotBox, y = this.clipRect,
                x = this.clipBox, b = "animate";
            s || (this.chartBackground = s = n.rect().addClass("highcharts-background").add(), b = "attr"), e = (t = i.borderWidth || 0) + (i.shadow ? 8 : 0), h = {fill: h || "none"}, (t || s["stroke-width"]) && (h.stroke = i.borderColor, h["stroke-width"] = t), s.attr(h).shadow(i.shadow), s[b]({
                x: e / 2,
                y: e / 2,
                width: r - e - t % 2,
                height: o - e - t % 2,
                r: i.borderRadius
            }), b = "animate", a || (b = "attr", this.plotBackground = a = n.rect().addClass("highcharts-plot-background").add()), a[b](v), a.attr({fill: d || "none"}).shadow(i.plotShadow), u && (c ? c.animate(v) : this.plotBGImage = n.image(u, p, f, g, m).add()), y ? y.animate({
                width: x.width,
                height: x.height
            }) : this.clipRect = n.clipRect(x), b = "animate", l || (b = "attr", this.plotBorder = l = n.rect().addClass("highcharts-plot-border").attr({zIndex: 1}).add()), l.attr({
                stroke: i.plotBorderColor,
                "stroke-width": i.plotBorderWidth || 0,
                fill: "none"
            }), l[b](l.crisp({x: p, y: f, width: g, height: m}, -l.strokeWidth())), this.isDirtyBox = !1
        }, propFromSeries: function () {
            var e, i, n, r = this, o = r.options.chart, s = r.options.series;
            Bi(["inverted", "angular", "polar"], function (t) {
                for (e = rn[o.type || o.defaultSeriesType], n = o[t] || e && e.prototype[t], i = s && s.length; !n && i--;) (e = rn[s[i].type]) && e.prototype[t] && (n = !0);
                r[t] = n
            })
        }, linkSeries: function () {
            var i = this, t = i.series;
            Bi(t, function (t) {
                t.linkedSeries.length = 0
            }), Bi(t, function (t) {
                var e = t.options.linkedTo;
                Ui(e) && (e = ":previous" === e ? i.series[t.index - 1] : i.get(e)) && e.linkedParent !== t && (e.linkedSeries.push(t), t.linkedParent = e, t.visible = tn(t.options.visible, e.options.visible, t.visible))
            })
        }, renderSeries: function () {
            Bi(this.series, function (t) {
                t.translate(), t.render()
            })
        }, renderLabels: function () {
            var r = this, o = r.options.labels;
            o.items && Bi(o.items, function (t) {
                var e = Wi(o.style, t.style), i = en(e.left) + r.plotLeft, n = en(e.top) + r.plotTop + 12;
                delete e.left, delete e.top, r.renderer.text(t.html, i, n).attr({zIndex: 2}).css(e).add()
            })
        }, render: function () {
            var t, e, i, n = this.axes, r = this.renderer, o = this.options;
            this.setTitle(), this.legend = new Qi(this, o.legend), this.getStacks && this.getStacks(), this.getMargins(!0), this.setChartSize(), o = this.plotWidth, t = this.plotHeight -= 21, Bi(n, function (t) {
                t.setScale()
            }), this.getAxisMargins(), e = 1.1 < o / this.plotWidth, i = 1.05 < t / this.plotHeight, (e || i) && (Bi(n, function (t) {
                (t.horiz && e || !t.horiz && i) && t.setTickInterval(!0)
            }), this.getMargins()), this.drawChartBox(), this.hasCartesianSeries && Bi(n, function (t) {
                t.visible && t.render()
            }), this.seriesGroup || (this.seriesGroup = r.g("series-group").attr({zIndex: 3}).add()), this.renderSeries(), this.renderLabels(), this.addCredits(), this.setResponsive && this.setResponsive(), this.hasRendered = !0
        }, addCredits: function (t) {
            var e = this;
            (t = Zi(!0, this.options.credits, t)).enabled && !this.credits && (this.credits = this.renderer.text(t.text + (this.mapCredits || ""), 0, 0).addClass("highcharts-credits").on("click", function () {
                t.href && (ln.location.href = t.href)
            }).attr({
                align: t.position.align,
                zIndex: 8
            }).css(t.style).add().align(t.position), this.credits.update = function (t) {
                e.credits = e.credits.destroy(), e.addCredits(t)
            })
        }, destroy: function () {
            var t, i = this, e = i.axes, n = i.series, r = i.container, o = r && r.parentNode;
            for (qi(i, "destroy"), ji[i.index] = void 0, $i.chartCount--, i.renderTo.removeAttribute("data-highcharts-chart"), nn(i), t = e.length; t--;) e[t] = e[t].destroy();
            for (this.scroller && this.scroller.destroy && this.scroller.destroy(), t = n.length; t--;) n[t] = n[t].destroy();
            for (t in Bi("title subtitle chartBackground plotBackground plotBGImage plotBorder seriesGroup clipRect credits pointer rangeSelector legend resetZoomButton tooltip renderer".split(" "), function (t) {
                var e = i[t];
                e && e.destroy && (i[t] = e.destroy())
            }), r && (r.innerHTML = "", nn(r), o && Ni(r)), i) delete i[t]
        }, isReadyToRender: function () {
            var t = this;
            return !(!sn && ln == ln.top && "complete" !== Ii.readyState && (Ii.attachEvent("onreadystatechange", function () {
                Ii.detachEvent("onreadystatechange", t.firstRender), "complete" === Ii.readyState && t.firstRender()
            }), 1))
        }, firstRender: function () {
            var e = this, t = e.options;
            e.isReadyToRender() && (e.getContainer(), qi(e, "init"), e.resetMargins(), e.setChartSize(), e.propFromSeries(), e.getAxes(), Bi(t.series || [], function (t) {
                e.initSeries(t)
            }), e.linkSeries(), qi(e, "beforeRender"), Ji && (e.pointer = new Ji(e, t)), e.render(), e.renderer.draw(), !e.renderer.imgCount && e.onload && e.onload(), e.cloneRenderTo(!0))
        }, onload: function () {
            Bi([this.callback].concat(this.callbacks), function (t) {
                t && void 0 !== this.index && t.apply(this, [this])
            }, this), qi(this, "load"), !1 !== this.options.chart.reflow && this.initReflow(), this.onload = null
        }
    },pn = (dn = t).each,fn = dn.extend,gn = dn.erase,mn = dn.fireEvent,vn = dn.format,yn = dn.isArray,xn = dn.isNumber,bn = dn.pick,wn = dn.removeEvent,(un = dn.Point = function () {
    }).prototype = {
        init: function (t, e, i) {
            return this.series = t, this.color = t.color, this.applyOptions(e, i), t.options.colorByPoint ? (e = t.options.colors || t.chart.options.colors, this.color = this.color || e[t.colorCounter], e = e.length, i = t.colorCounter, t.colorCounter++, t.colorCounter === e && (t.colorCounter = 0)) : i = t.colorIndex, this.colorIndex = bn(this.colorIndex, i), t.chart.pointCount++, this
        }, applyOptions: function (t, e) {
            var i = this.series, n = i.options.pointValKey || i.pointValKey;
            return t = un.prototype.optionsToObject.call(this, t), fn(this, t), this.options = this.options ? fn(this.options, t) : t, t.group && delete this.group, n && (this.y = this[n]), this.isNull = bn(this.isValid && !this.isValid(), null === this.x || !xn(this.y, !0)), this.selected && (this.state = "select"), "name" in this && void 0 === e && i.xAxis && i.xAxis.hasNames && (this.x = i.xAxis.nameToX(this)), void 0 === this.x && i && (this.x = void 0 === e ? i.autoIncrement(this) : e), this
        }, optionsToObject: function (t) {
            var e = {}, i = this.series, n = i.options.keys, r = n || i.pointArrayMap || ["y"], o = r.length, s = 0,
                a = 0;
            if (xn(t) || null === t) e[r[0]] = t; else if (yn(t)) for (!n && t.length > o && ("string" == (i = typeof t[0]) ? e.name = t[0] : "number" === i && (e.x = t[0]), s++); a < o;) n && void 0 === t[s] || (e[r[a]] = t[s]), s++, a++; else "object" == typeof t && ((e = t).dataLabels && (i._hasPointLabels = !0), t.marker && (i._hasPointMarkers = !0));
            return e
        }, getClassName: function () {
            return "highcharts-point" + (this.selected ? " highcharts-point-select" : "") + (this.negative ? " highcharts-negative" : "") + (this.isNull ? " highcharts-null-point" : "") + (void 0 !== this.colorIndex ? " highcharts-color-" + this.colorIndex : "") + (this.options.className ? " " + this.options.className : "")
        }, getZone: function () {
            var t, e = (i = this.series).zones, i = i.zoneAxis || "y", n = 0;
            for (t = e[n]; this[i] >= t.value;) t = e[++n];
            return t && t.color && !this.options.color && (this.color = t.color), t
        }, destroy: function () {
            var t, e = this.series.chart, i = e.hoverPoints;
            for (t in e.pointCount--, i && (this.setState(), gn(i, this), i.length || (e.hoverPoints = null)), this === e.hoverPoint && this.onMouseOut(), (this.graphic || this.dataLabel) && (wn(this), this.destroyElements()), this.legendItem && e.legend.destroyItem(this), this) this[t] = null
        }, destroyElements: function () {
            for (var t, e = ["graphic", "dataLabel", "dataLabelUpper", "connector", "shadowGroup"], i = 6; i--;) this[t = e[i]] && (this[t] = this[t].destroy())
        }, getLabelConfig: function () {
            return {
                x: this.category,
                y: this.y,
                color: this.color,
                key: this.name || this.category,
                series: this.series,
                point: this,
                percentage: this.percentage,
                total: this.total || this.stackTotal
            }
        }, tooltipFormatter: function (e) {
            var t = this.series, i = t.tooltipOptions, n = bn(i.valueDecimals, ""), r = i.valuePrefix || "",
                o = i.valueSuffix || "";
            return pn(t.pointArrayMap || ["y"], function (t) {
                t = "{point." + t, (r || o) && (e = e.replace(t + "}", r + t + "}" + o)), e = e.replace(t + "}", t + ":,." + n + "f}")
            }), vn(e, {point: this, series: this.series})
        }, firePointEvent: function (t, e, i) {
            var n = this, r = this.series.options;
            (r.point.events[t] || n.options && n.options.events && n.options.events[t]) && this.importEvents(), "click" === t && r.allowPointSelect && (i = function (t) {
                n.select && n.select(null, t.ctrlKey || t.metaKey || t.shiftKey)
            }), mn(this, t, e, i)
        }, visible: !0
    },_n = (Cn = t).addEvent,kn = Cn.animObject,Sn = Cn.arrayMax,Tn = Cn.arrayMin,An = Cn.correctFloat,$n = Cn.Date,Mn = Cn.defaultOptions,En = Cn.defaultPlotOptions,Pn = Cn.defined,Ln = Cn.each,In = Cn.erase,Fn = Cn.error,Dn = Cn.extend,On = Cn.fireEvent,Nn = Cn.grep,jn = Cn.isArray,zn = Cn.isNumber,Rn = Cn.isString,Bn = Cn.merge,Hn = Cn.pick,Wn = Cn.removeEvent,qn = Cn.splat,Gn = Cn.stableSort,Xn = Cn.SVGElement,Yn = Cn.syncTimeout,Vn = Cn.win,Cn.Series = Cn.seriesType("line", null, {
        lineWidth: 2,
        allowPointSelect: !1,
        showCheckbox: !1,
        animation: {duration: 1e3},
        events: {},
        marker: {
            lineWidth: 0,
            lineColor: "#ffffff",
            radius: 4,
            states: {
                hover: {animation: {duration: 50}, enabled: !0, radiusPlus: 2, lineWidthPlus: 1},
                select: {fillColor: "#cccccc", lineColor: "#000000", lineWidth: 2}
            }
        },
        point: {events: {}},
        dataLabels: {
            align: "center",
            formatter: function () {
                return null === this.y ? "" : Cn.numberFormat(this.y, -1)
            },
            style: {
                fontSize: "11px",
                fontWeight: "bold",
                color: "contrast",
                textShadow: "1px 1px contrast, -1px -1px contrast, -1px 1px contrast, 1px -1px contrast"
            },
            verticalAlign: "bottom",
            x: 0,
            y: 0,
            padding: 5
        },
        cropThreshold: 300,
        pointRange: 0,
        softThreshold: !0,
        states: {hover: {lineWidthPlus: 1, marker: {}, halo: {size: 10, opacity: .25}}, select: {marker: {}}},
        stickyTracking: !0,
        turboThreshold: 1e3
    }, {
        isCartesian: !0,
        pointClass: Cn.Point,
        sorted: !0,
        requireSorting: !0,
        directTouch: !1,
        axisTypes: ["xAxis", "yAxis"],
        colorCounter: 0,
        parallelArrays: ["x", "y"],
        coll: "series",
        init: function (t, e) {
            var i, n, r = this, o = t.series, s = function (t, e) {
                return Hn(t.options.index, t._i) - Hn(e.options.index, e._i)
            };
            for (i in r.chart = t, r.options = e = r.setOptions(e), r.linkedSeries = [], r.bindAxes(), Dn(r, {
                name: e.name,
                state: "",
                visible: !1 !== e.visible,
                selected: !0 === e.selected
            }), n = e.events) _n(r, i, n[i]);
            (n && n.click || e.point && e.point.events && e.point.events.click || e.allowPointSelect) && (t.runTrackerClick = !0), r.getColor(), r.getSymbol(), Ln(r.parallelArrays, function (t) {
                r[t + "Data"] = []
            }), r.setData(e.data, !1), r.isCartesian && (t.hasCartesianSeries = !0), o.push(r), r._i = o.length - 1, Gn(o, s), this.yAxis && Gn(this.yAxis.series, s), Ln(o, function (t, e) {
                t.index = e, t.name = t.name || "Series " + (e + 1)
            })
        },
        bindAxes: function () {
            var i, n = this, r = n.options, t = n.chart;
            Ln(n.axisTypes || [], function (e) {
                Ln(t[e], function (t) {
                    i = t.options, (r[e] === i.index || void 0 !== r[e] && r[e] === i.id || void 0 === r[e] && 0 === i.index) && (t.series.push(n), (n[e] = t).isDirty = !0)
                }), n[e] || n.optionalAxis === e || Fn(18, !0)
            })
        },
        updateParallelArrays: function (i, n) {
            var r = i.series, e = arguments, t = zn(n) ? function (t) {
                var e = "y" === t && r.toYData ? r.toYData(i) : i[t];
                r[t + "Data"][n] = e
            } : function (t) {
                Array.prototype[n].apply(r[t + "Data"], Array.prototype.slice.call(e, 2))
            };
            Ln(r.parallelArrays, t)
        },
        autoIncrement: function () {
            var t, e = this.options, i = this.xIncrement, n = e.pointIntervalUnit;
            return i = Hn(i, e.pointStart, 0), this.pointInterval = t = Hn(this.pointInterval, e.pointInterval, 1), n && (e = new $n(i), "day" === n ? e = +e[$n.hcSetDate](e[$n.hcGetDate]() + t) : "month" === n ? e = +e[$n.hcSetMonth](e[$n.hcGetMonth]() + t) : "year" === n && (e = +e[$n.hcSetFullYear](e[$n.hcGetFullYear]() + t)), t = e - i), this.xIncrement = i + t, i
        },
        setOptions: function (t) {
            var e, i = (e = this.chart).options.plotOptions, n = (e = e.userOptions || {}).plotOptions || {},
                r = i[this.type];
            return this.userOptions = t, i = Bn(r, i.series, t), this.tooltipOptions = Bn(Mn.tooltip, Mn.plotOptions[this.type].tooltip, e.tooltip, n.series && n.series.tooltip, n[this.type] && n[this.type].tooltip, t.tooltip), null === r.marker && delete i.marker, this.zoneAxis = i.zoneAxis, t = this.zones = (i.zones || []).slice(), !i.negativeColor && !i.negativeFillColor || i.zones || t.push({
                value: i[this.zoneAxis + "Threshold"] || i.threshold || 0,
                className: "highcharts-negative",
                color: i.negativeColor,
                fillColor: i.negativeFillColor
            }), t.length && Pn(t[t.length - 1].value) && t.push({color: this.color, fillColor: this.fillColor}), i
        },
        getCyclic: function (t, e, i) {
            var n, r = this.userOptions, o = t + "Index", s = t + "Counter",
                a = i ? i.length : Hn(this.chart.options.chart[t + "Count"], this.chart[t + "Count"]);
            e || (n = Hn(r[o], r["_" + o]), Pn(n) || (r["_" + o] = n = this.chart[s] % a, this.chart[s] += 1), i && (e = i[n])), void 0 !== n && (this[o] = n), this[t] = e
        },
        getColor: function () {
            this.options.colorByPoint ? this.options.color = null : this.getCyclic("color", this.options.color || En[this.type].color, this.chart.options.colors)
        },
        getSymbol: function () {
            this.getCyclic("symbol", this.options.marker.symbol, this.chart.options.symbols)
        },
        drawLegendSymbol: Cn.LegendSymbolMixin.drawLineMarker,
        setData: function (t, e, i, n) {
            var r, o = this, s = o.points, a = s && s.length || 0, l = o.options, c = o.chart, h = null, d = o.xAxis,
                u = l.turboThreshold, p = this.xData, f = this.yData, g = (r = o.pointArrayMap) && r.length;
            if (r = (t = t || []).length, e = Hn(e, !0), !1 !== n && r && a === r && !o.cropped && !o.hasGroupedData && o.visible) Ln(t, function (t, e) {
                s[e].update && t !== l.data[e] && s[e].update(t, !1, null, !1)
            }); else {
                if (o.xIncrement = null, o.colorCounter = 0, Ln(this.parallelArrays, function (t) {
                    o[t + "Data"].length = 0
                }), u && u < r) {
                    for (i = 0; null === h && i < r;) h = t[i], i++;
                    if (zn(h)) for (i = 0; i < r; i++) p[i] = this.autoIncrement(), f[i] = t[i]; else if (jn(h)) if (g) for (i = 0; i < r; i++) h = t[i], p[i] = h[0], f[i] = h.slice(1, g + 1); else for (i = 0; i < r; i++) h = t[i], p[i] = h[0], f[i] = h[1]; else Fn(12)
                } else for (i = 0; i < r; i++) void 0 !== t[i] && (h = {series: o}, o.pointClass.prototype.applyOptions.apply(h, [t[i]]), o.updateParallelArrays(h, i));
                for (Rn(f[0]) && Fn(14, !0), o.data = [], o.options.data = o.userOptions.data = t, i = a; i--;) s[i] && s[i].destroy && s[i].destroy();
                d && (d.minRange = d.userMinRange), o.isDirty = c.isDirtyBox = !0, o.isDirtyData = !!s, i = !1
            }
            "point" === l.legendType && (this.processData(), this.generatePoints()), e && c.redraw(i)
        },
        processData: function (t) {
            var e, i = this.xData, n = this.yData, r = i.length;
            e = 0;
            var o, s, a, l = this.xAxis;
            a = (p = this.options).cropThreshold;
            var c, h, d = this.getExtremesFromAll || p.getExtremesFromAll, u = this.isCartesian, p = l && l.val2lin,
                f = l && l.isLog;
            if (u && !this.isDirty && !l.isDirty && !this.yAxis.isDirty && !t) return !1;
            for (l && (c = (t = l.getExtremes()).min, h = t.max), u && this.sorted && !d && (!a || a < r || this.forceCrop) && (i[r - 1] < c || i[0] > h ? (i = [], n = []) : (i[0] < c || i[r - 1] > h) && (i = (e = this.cropData(this.xData, this.yData, c, h)).xData, n = e.yData, e = e.start, o = !0)), a = i.length || 1; --a;) 0 < (r = f ? p(i[a]) - p(i[a - 1]) : i[a] - i[a - 1]) && (void 0 === s || r < s) ? s = r : r < 0 && this.requireSorting && Fn(15);
            this.cropped = o, this.cropStart = e, this.processedXData = i, this.processedYData = n, this.closestPointRange = s
        },
        cropData: function (t, e, i, n) {
            var r, o = t.length, s = 0, a = o, l = Hn(this.cropShoulder, 1);
            for (r = 0; r < o; r++) if (t[r] >= i) {
                s = Math.max(0, r - l);
                break
            }
            for (i = r; i < o; i++) if (t[i] > n) {
                a = i + l;
                break
            }
            return {xData: t.slice(s, a), yData: e.slice(s, a), start: s, end: a}
        },
        generatePoints: function () {
            var t, e, i, n, r = this.options.data, o = this.data, s = this.processedXData, a = this.processedYData,
                l = this.pointClass, c = s.length, h = this.cropStart || 0, d = this.hasGroupedData, u = [];
            for (o || d || ((o = []).length = r.length, o = this.data = o), n = 0; n < c; n++) e = h + n, d ? (u[n] = (new l).init(this, [s[n]].concat(qn(a[n]))), u[n].dataGroup = this.groupMap[n]) : (o[e] ? i = o[e] : void 0 !== r[e] && (o[e] = i = (new l).init(this, r[e], s[n])), u[n] = i), u[n].index = e;
            if (o && (c !== (t = o.length) || d)) for (n = 0; n < t; n++) n !== h || d || (n += c), o[n] && (o[n].destroyElements(), o[n].plotX = void 0);
            this.data = o, this.points = u
        },
        getExtremes: function (t) {
            var e, i, n, r, o, s = this.yAxis, a = this.processedXData, l = [], c = 0,
                h = (e = this.xAxis.getExtremes()).min, d = e.max;
            for (e = (t = t || this.stackedYData || this.processedYData || []).length, o = 0; o < e; o++) if (n = a[o], r = t[o], i = (zn(r, !0) || jn(r)) && (!s.isLog || r.length || 0 < r), n = this.getExtremesFromAll || this.options.getExtremesFromAll || this.cropped || (a[o + 1] || n) >= h && (a[o - 1] || n) <= d, i && n) if (i = r.length) for (; i--;) null !== r[i] && (l[c++] = r[i]); else l[c++] = r;
            this.dataMin = Tn(l), this.dataMax = Sn(l)
        },
        translate: function () {
            this.processedXData || this.processData(), this.generatePoints();
            for (var t, e, i, n, r = (m = this.options).stacking, o = this.xAxis, s = o.categories, a = this.yAxis, l = this.points, c = l.length, h = !!this.modifyValue, d = m.pointPlacement, u = "between" === d || zn(d), p = m.threshold, f = m.startFromThreshold ? p : 0, g = Number.MAX_VALUE, m = 0; m < c; m++) {
                var v = l[m], y = v.x, x = v.y;
                e = v.low;
                var b, w = r && a.stacks[(this.negStacks && x < (f ? 0 : p) ? "-" : "") + this.stackKey];
                a.isLog && null !== x && x <= 0 && (v.isNull = !0), v.plotX = t = An(Math.min(Math.max(-1e5, o.translate(y, 0, 0, 0, 1, d, "flags" === this.type)), 1e5)), r && this.visible && !v.isNull && w && w[y] && (n = this.getStackIndicator(n, y, this.index), e = (x = (b = w[y]).points[n.key])[0], x = x[1], e === f && n.key === w[y].base && (e = Hn(p, a.min)), a.isLog && e <= 0 && (e = null), v.total = v.stackTotal = b.total, v.percentage = b.total && v.y / b.total * 100, v.stackY = x, b.setOffset(this.pointXOffset || 0, this.barW || 0)), v.yBottom = Pn(e) ? a.translate(e, 0, 1, 0, 1) : null, h && (x = this.modifyValue(x, v)), v.plotY = e = "number" == typeof x && Infinity !== x ? Math.min(Math.max(-1e5, a.translate(x, 0, 1, 0, 1)), 1e5) : void 0, v.isInside = void 0 !== e && 0 <= e && e <= a.len && 0 <= t && t <= o.len, v.clientX = u ? An(o.translate(y, 0, 0, 0, 1, d)) : t, v.negative = v.y < (p || 0), v.category = s && void 0 !== s[v.x] ? s[v.x] : v.x, v.isNull || (void 0 !== i && (g = Math.min(g, Math.abs(t - i))), i = t)
            }
            this.closestPointRangePx = g
        },
        getValidPoints: function (t, e) {
            var i = this.chart;
            return Nn(t || this.points || [], function (t) {
                return !(e && !i.isInsidePlot(t.plotX, t.plotY, i.inverted) || t.isNull)
            })
        },
        setClip: function (t) {
            var e = this.chart, i = this.options, n = e.renderer, r = e.inverted, o = this.clipBox, s = o || e.clipBox,
                a = this.sharedClipKey || ["_sharedClip", t && t.duration, t && t.easing, s.height, i.xAxis, i.yAxis].join(),
                l = e[a], c = e[a + "m"];
            l || (t && (s.width = 0, e[a + "m"] = c = n.clipRect(-99, r ? -e.plotLeft : -e.plotTop, 99, r ? e.chartWidth : e.chartHeight)), e[a] = l = n.clipRect(s), l.count = {length: 0}), t && !l.count[this.index] && (l.count[this.index] = !0, l.count.length += 1), !1 !== i.clip && (this.group.clip(t || o ? l : e.clipRect), this.markerGroup.clip(c), this.sharedClipKey = a), t || (l.count[this.index] && (delete l.count[this.index], --l.count.length), 0 === l.count.length && a && e[a] && (o || (e[a] = e[a].destroy()), e[a + "m"] && (e[a + "m"] = e[a + "m"].destroy())))
        },
        animate: function (t) {
            var e, i = this.chart, n = kn(this.options.animation);
            t ? this.setClip(n) : ((t = i[e = this.sharedClipKey]) && t.animate({width: i.plotSizeX}, n), i[e + "m"] && i[e + "m"].animate({width: i.plotSizeX + 99}, n), this.animate = null)
        },
        afterAnimate: function () {
            this.setClip(), On(this, "afterAnimate")
        },
        drawPoints: function () {
            var t, e, i, n, r, o, s, a, l = this.points, c = this.chart, h = this.options.marker, d = this.markerGroup,
                u = Hn(h.enabled, !!this.xAxis.isRadial || null, this.closestPointRangePx > 2 * h.radius);
            if (!1 !== h.enabled || this._hasPointMarkers) for (e = l.length; e--;) t = (i = l[e]).plotY, n = i.graphic, r = i.marker || {}, o = !!i.marker, s = u && void 0 === r.enabled || r.enabled, a = i.isInside, s && zn(t) && null !== i.y ? (t = Hn(r.symbol, this.symbol), i.hasImage = 0 === t.indexOf("url"), s = this.markerAttribs(i, i.selected && "select"), n ? n[a ? "show" : "hide"](!0).animate(s) : a && (0 < s.width || i.hasImage) && (i.graphic = n = c.renderer.symbol(t, s.x, s.y, s.width, s.height, o ? r : h).add(d)), n && n.attr(this.pointAttribs(i, i.selected && "select")), n && n.addClass(i.getClassName(), !0)) : n && (i.graphic = n.destroy())
        },
        markerAttribs: function (t, e) {
            var i = this.options.marker, n = (r = t && t.options) && r.marker || {}, r = Hn(n.radius, i.radius);
            return e && (i = i.states[e], e = n.states && n.states[e], r = Hn(e && e.radius, i && i.radius, r + (i && i.radiusPlus || 0))), t.hasImage && (r = 0), t = {
                x: Math.floor(t.plotX) - r,
                y: t.plotY - r
            }, r && (t.width = t.height = 2 * r), t
        },
        pointAttribs: function (t, e) {
            var i, n = this.options.marker, r = (a = t && t.options) && a.marker || {}, o = n.lineWidth, s = this.color,
                a = a && a.color, l = t && t.color;
            return t && this.zones.length && (t = t.getZone()) && t.color && (i = t.color), s = a || i || l || s, i = r.fillColor || n.fillColor || s, s = r.lineColor || n.lineColor || s, e && (n = n.states[e], e = r.states && r.states[e] || {}, o = n.lineWidth || o + n.lineWidthPlus, i = e.fillColor || n.fillColor || i, s = e.lineColor || n.lineColor || s), {
                stroke: s,
                "stroke-width": o,
                fill: i
            }
        },
        destroy: function () {
            var t, e, i, n, r = this, o = r.chart, s = /AppleWebKit\/533/.test(Vn.navigator.userAgent),
                a = r.data || [];
            for (On(r, "destroy"), Wn(r), Ln(r.axisTypes || [], function (t) {
                (n = r[t]) && n.series && (In(n.series, r), n.isDirty = n.forceRedraw = !0)
            }), r.legendItem && r.chart.legend.destroyItem(r), t = a.length; t--;) (e = a[t]) && e.destroy && e.destroy();
            for (i in r.points = null, clearTimeout(r.animationTimeout), r) r[i] instanceof Xn && !r[i].survive && (t = s && "group" === i ? "hide" : "destroy", r[i][t]());
            for (i in o.hoverSeries === r && (o.hoverSeries = null), In(o.series, r), r) delete r[i]
        },
        getGraphPath: function (o, s, a) {
            var t, l, c = this, h = c.options, d = h.step, u = [], p = [];
            return (t = (o = o || c.points).reversed) && o.reverse(), (d = {
                right: 1,
                center: 2
            }[d] || d && 3) && t && (d = 4 - d), !h.connectNulls || s || a || (o = this.getValidPoints(o)), Ln(o, function (t, e) {
                var i = t.plotX, n = t.plotY, r = o[e - 1];
                (t.leftCliff || r && r.rightCliff) && !a && (l = !0), t.isNull && !Pn(s) && 0 < e ? l = !h.connectNulls : t.isNull && !s ? l = !0 : (0 === e || l ? e = ["M", t.plotX, t.plotY] : c.getPointSpline ? e = c.getPointSpline(o, t, e) : d ? (e = 1 === d ? ["L", r.plotX, n] : 2 === d ? ["L", (r.plotX + i) / 2, r.plotY, "L", (r.plotX + i) / 2, n] : ["L", i, r.plotY]).push("L", i, n) : e = ["L", i, n], p.push(t.x), d && p.push(t.x), u.push.apply(u, e), l = !1)
            }), u.xMap = p, c.graphPath = u
        },
        drawGraph: function () {
            var r = this, o = this.options, s = (this.gappedPath || this.getGraphPath).call(this),
                i = [["graph", "highcharts-graph", o.lineColor || this.color, o.dashStyle]];
            Ln(this.zones, function (t, e) {
                i.push(["zone-graph-" + e, "highcharts-graph highcharts-zone-graph-" + e + " " + (t.className || ""), t.color || r.color, t.dashStyle || o.dashStyle])
            }), Ln(i, function (t, e) {
                var i = t[0], n = r[i];
                n ? (n.endX = s.xMap, n.animate({d: s})) : s.length && (r[i] = r.chart.renderer.path(s).addClass(t[1]).attr({zIndex: 1}).add(r.group), n = {
                    stroke: t[2],
                    "stroke-width": o.lineWidth,
                    fill: r.fillGraph && r.color || "none"
                }, t[3] ? n.dashstyle = t[3] : "square" !== o.linecap && (n["stroke-linecap"] = n["stroke-linejoin"] = "round"), n = r[i].attr(n).shadow(e < 2 && o.shadow)), n && (n.startX = s.xMap, n.isArea = s.isArea)
            })
        },
        applyZones: function () {
            var i, n, r, o, s, a, l, c, h, d = this, u = this.chart, p = u.renderer, t = this.zones,
                f = this.clips || [], g = this.graph, m = this.area, v = Math.max(u.chartWidth, u.chartHeight),
                y = this[(this.zoneAxis || "y") + "Axis"], x = u.inverted, b = !1;
            t.length && (g || m) && y && void 0 !== y.min && (s = y.reversed, a = y.horiz, g && g.hide(), m && m.hide(), o = y.getExtremes(), Ln(t, function (t, e) {
                i = s ? a ? u.plotWidth : 0 : a ? 0 : y.toPixels(o.min), i = Math.min(Math.max(Hn(n, i), 0), v), n = Math.min(Math.max(Math.round(y.toPixels(Hn(t.value, o.max), !0)), 0), v), b && (i = n = y.toPixels(o.max)), l = Math.abs(i - n), c = Math.min(i, n), h = Math.max(i, n), y.isXAxis ? (r = {
                    x: x ? h : c,
                    y: 0,
                    width: l,
                    height: v
                }, a || (r.x = u.plotHeight - r.x)) : (r = {
                    x: 0,
                    y: x ? h : c,
                    width: v,
                    height: l
                }, a && (r.y = u.plotWidth - r.y)), x && p.isVML && (r = y.isXAxis ? {
                    x: 0,
                    y: s ? c : h,
                    height: r.width,
                    width: u.chartWidth
                } : {
                    x: r.y - u.plotLeft - u.spacingBox.x,
                    y: 0,
                    width: r.height,
                    height: u.chartHeight
                }), f[e] ? f[e].animate(r) : (f[e] = p.clipRect(r), g && d["zone-graph-" + e].clip(f[e]), m && d["zone-area-" + e].clip(f[e])), b = t.value > o.max
            }), this.clips = f)
        },
        invertGroups: function (i) {
            function t() {
                var e = {width: n.yAxis.len, height: n.xAxis.len};
                Ln(["group", "markerGroup"], function (t) {
                    n[t] && n[t].attr(e).invert(i)
                })
            }

            var n = this, e = n.chart;
            n.xAxis && (_n(e, "resize", t), _n(n, "destroy", function () {
                Wn(e, "resize", t)
            }), t(i), n.invertGroups = t)
        },
        plotGroup: function (t, e, i, n, r) {
            var o = this[t], s = !o;
            return s && (this[t] = o = this.chart.renderer.g(e).attr({zIndex: n || .1}).add(r), o.addClass("highcharts-series-" + this.index + " highcharts-" + this.type + "-series highcharts-color-" + this.colorIndex + " " + (this.options.className || ""))), o.attr({visibility: i})[s ? "attr" : "animate"](this.getPlotBox()), o
        },
        getPlotBox: function () {
            var t = this.chart, e = this.xAxis, i = this.yAxis;
            return t.inverted && (e = i, i = this.xAxis), {
                translateX: e ? e.left : t.plotLeft,
                translateY: i ? i.top : t.plotTop,
                scaleX: 1,
                scaleY: 1
            }
        },
        render: function () {
            var t, e = this, i = e.chart, n = e.options,
                r = !!e.animate && i.renderer.isSVG && kn(n.animation).duration, o = e.visible ? "inherit" : "hidden",
                s = n.zIndex, a = e.hasRendered, l = i.seriesGroup, c = i.inverted;
            t = e.plotGroup("group", "series", o, s, l), e.markerGroup = e.plotGroup("markerGroup", "markers", o, s, l), r && e.animate(!0), t.inverted = !!e.isCartesian && c, e.drawGraph && (e.drawGraph(), e.applyZones()), e.drawDataLabels && e.drawDataLabels(), e.visible && e.drawPoints(), e.drawTracker && !1 !== e.options.enableMouseTracking && e.drawTracker(), e.invertGroups(c), !1 === n.clip || e.sharedClipKey || a || t.clip(i.clipRect), r && e.animate(), a || (e.animationTimeout = Yn(function () {
                e.afterAnimate()
            }, r)), e.isDirty = e.isDirtyData = !1, e.hasRendered = !0
        },
        redraw: function () {
            var t = this.chart, e = this.isDirty || this.isDirtyData, i = this.group, n = this.xAxis, r = this.yAxis;
            i && (t.inverted && i.attr({
                width: t.plotWidth,
                height: t.plotHeight
            }), i.animate({
                translateX: Hn(n && n.left, t.plotLeft),
                translateY: Hn(r && r.top, t.plotTop)
            })), this.translate(), this.render(), e && delete this.kdTree
        },
        kdDimensions: 1,
        kdAxisArray: ["clientX", "plotY"],
        searchPoint: function (t, e) {
            var i = this.xAxis, n = this.yAxis, r = this.chart.inverted;
            return this.searchKDTree({
                clientX: r ? i.len - t.chartY + i.pos : t.chartX - i.pos,
                plotY: r ? n.len - t.chartX + n.pos : t.chartY - n.pos
            }, e)
        },
        buildKDTree: function () {
            function o(t, e, i) {
                var n, r;
                if (r = t && t.length) return n = s.kdAxisArray[e % i], t.sort(function (t, e) {
                    return t[n] - e[n]
                }), {
                    point: t[r = Math.floor(r / 2)],
                    left: o(t.slice(0, r), e + 1, i),
                    right: o(t.slice(r + 1), e + 1, i)
                }
            }

            var s = this, t = s.kdDimensions;
            delete s.kdTree, Yn(function () {
                s.kdTree = o(s.getValidPoints(null, !s.directTouch), t, t)
            }, s.options.kdNow ? 0 : 1)
        },
        searchKDTree: function (t, e) {
            function c(t, e, i, n) {
                var r, o, s = e.point, a = h.kdAxisArray[i % n], l = s;
                return r = ((o = Pn(t[d]) && Pn(s[d]) ? Math.pow(t[d] - s[d], 2) : null) || 0) + ((r = Pn(t[u]) && Pn(s[u]) ? Math.pow(t[u] - s[u], 2) : null) || 0), s.dist = Pn(r) ? Math.sqrt(r) : Number.MAX_VALUE, s.distX = Pn(o) ? Math.sqrt(o) : Number.MAX_VALUE, o = (a = t[a] - s[a]) < 0 ? "right" : "left", e[r = a < 0 ? "left" : "right"] && (l = (r = c(t, e[r], i + 1, n))[p] < l[p] ? r : s), e[o] && Math.sqrt(a * a) < l[p] && (l = (t = c(t, e[o], i + 1, n))[p] < l[p] ? t : l), l
            }

            var h = this, d = this.kdAxisArray[0], u = this.kdAxisArray[1], p = e ? "distX" : "dist";
            if (this.kdTree || this.buildKDTree(), this.kdTree) return c(t, this.kdTree, this.kdDimensions, this.kdDimensions)
        }
    }),function (t) {
        function w(t, e, i, n, r) {
            var o = t.chart.inverted;
            this.axis = t, this.isNegative = i, this.options = e, this.x = n, this.total = null, this.points = {}, this.stack = r, this.rightCliff = this.leftCliff = 0, this.alignOptions = {
                align: e.align || (o ? i ? "left" : "right" : "center"),
                verticalAlign: e.verticalAlign || (o ? "middle" : i ? "bottom" : "top"),
                y: k(e.y, o ? 4 : i ? 14 : -6),
                x: k(e.x, o ? i ? -6 : 6 : 0)
            }, this.textAlign = e.textAlign || (o ? i ? "right" : "left" : "center")
        }

        var e = t.Axis, i = t.Chart, C = t.correctFloat, _ = t.defined, n = t.destroyObjectProperties, l = t.each,
            r = t.format, k = t.pick;
        t = t.Series, w.prototype = {
            destroy: function () {
                n(this, this.axis)
            }, render: function (t) {
                var e = this.options, i = (i = e.format) ? r(i, this) : e.formatter.call(this);
                this.label ? this.label.attr({
                    text: i,
                    visibility: "hidden"
                }) : this.label = this.axis.chart.renderer.text(i, null, null, e.useHTML).css(e.style).attr({
                    align: this.textAlign,
                    rotation: e.rotation,
                    visibility: "hidden"
                }).add(t)
            }, setOffset: function (t, e) {
                var i = (s = this.axis).chart, n = i.inverted, r = s.reversed,
                    o = (r = this.isNegative && !r || !this.isNegative && r, s.translate(s.usePercentage ? 100 : this.total, 0, 0, 0, 1)),
                    s = s.translate(0);
                s = Math.abs(o - s);
                t = i.xAxis[0].translate(this.x) + t;
                var a = i.plotHeight;
                n = {
                    x: n ? r ? o : o - s : t,
                    y: n ? a - t - e : r ? a - o - s : a - o,
                    width: n ? s : e,
                    height: n ? e : s
                };
                (e = this.label) && (e.align(this.alignOptions, null, n), n = e.alignAttr, e[!1 === this.options.crop || i.isInsidePlot(n.x, n.y) ? "show" : "hide"](!0))
            }
        }, i.prototype.getStacks = function () {
            var e = this;
            l(e.yAxis, function (t) {
                t.stacks && t.hasVisibleSeries && (t.oldStacks = t.stacks)
            }), l(e.series, function (t) {
                !t.options.stacking || !0 !== t.visible && !1 !== e.options.chart.ignoreHiddenSeries || (t.stackKey = t.type + k(t.options.stack, ""))
            })
        }, e.prototype.buildStacks = function () {
            var t, e, i = this.series, n = k(this.options.reversedStacks, !0), r = i.length;
            if (!this.isXAxis) {
                for (this.usePercentage = !1, e = r; e--;) i[n ? e : r - e - 1].setStackedPoints();
                for (e = r; e--;) (t = i[n ? e : r - e - 1]).setStackCliffs && t.setStackCliffs();
                if (this.usePercentage) for (e = 0; e < r; e++) i[e].setPercentStacks()
            }
        }, e.prototype.renderStackTotals = function () {
            var t, e, i = this.chart, n = i.renderer, r = this.stacks, o = this.stackTotalGroup;
            for (t in o || (this.stackTotalGroup = o = n.g("stack-labels").attr({
                visibility: "visible",
                zIndex: 6
            }).add()), o.translate(i.plotLeft, i.plotTop), r) for (e in i = r[t]) i[e].render(o)
        }, e.prototype.resetStacks = function () {
            var t, e, i = this.stacks;
            if (!this.isXAxis) for (t in i) for (e in i[t]) i[t][e].touched < this.stacksTouched ? (i[t][e].destroy(), delete i[t][e]) : (i[t][e].total = null, i[t][e].cum = 0)
        }, e.prototype.cleanStacks = function () {
            var t, e, i;
            if (!this.isXAxis) for (e in this.oldStacks && (t = this.stacks = this.oldStacks), t) for (i in t[e]) t[e][i].cum = t[e][i].total
        }, t.prototype.setStackedPoints = function () {
            if (this.options.stacking && (!0 === this.visible || !1 === this.chart.options.chart.ignoreHiddenSeries)) {
                var t, e, i, n, r, o, s, a = this.processedXData, l = this.processedYData, c = [], h = l.length,
                    d = (f = this.options).threshold, u = f.startFromThreshold ? d : 0, p = f.stack, f = f.stacking,
                    g = this.stackKey, m = "-" + g, v = this.negStacks, y = this.yAxis, x = y.stacks, b = y.oldStacks;
                for (y.stacksTouched += 1, r = 0; r < h; r++) o = a[r], s = l[r], n = (t = this.getStackIndicator(t, o, this.index)).key, x[i = (e = v && s < (u ? 0 : d)) ? m : g] || (x[i] = {}), x[i][o] || (b[i] && b[i][o] ? (x[i][o] = b[i][o], x[i][o].total = null) : x[i][o] = new w(y, y.options.stackLabels, e, o, p)), i = x[i][o], null !== s && (i.points[n] = i.points[this.index] = [k(i.cum, u)], _(i.cum) || (i.base = n), i.touched = y.stacksTouched, 0 < t.index && !1 === this.singleStacks && (i.points[n][0] = i.points[this.index + "," + o + ",0"][0])), "percent" === f ? (e = e ? g : m, v && x[e] && x[e][o] ? (e = x[e][o], i.total = e.total = Math.max(e.total, i.total) + Math.abs(s) || 0) : i.total = C(i.total + (Math.abs(s) || 0))) : i.total = C(i.total + (s || 0)), i.cum = k(i.cum, u) + (s || 0), null !== s && (i.points[n].push(i.cum), c[r] = i.cum);
                "percent" === f && (y.usePercentage = !0), this.stackedYData = c, y.oldStacks = {}
            }
        }, t.prototype.setPercentStacks = function () {
            var r, o = this, t = o.stackKey, s = o.yAxis.stacks, a = o.processedXData;
            l([t, "-" + t], function (t) {
                for (var e, i, n = a.length; n--;) e = a[n], r = o.getStackIndicator(r, e, o.index, t), (e = (i = s[t] && s[t][e]) && i.points[r.key]) && (i = i.total ? 100 / i.total : 0, e[0] = C(e[0] * i), e[1] = C(e[1] * i), o.stackedYData[n] = e[1])
            })
        }, t.prototype.getStackIndicator = function (t, e, i, n) {
            return !_(t) || t.x !== e || n && t.key !== n ? t = {
                x: e,
                index: 0,
                key: n
            } : t.index++, t.key = [i, e, t.index].join(), t
        }
    }(t),Qn = (Un = t).addEvent,Kn = Un.animate,Zn = Un.Axis,Jn = Un.createElement,tr = Un.css,er = Un.defined,ir = Un.each,nr = Un.erase,rr = Un.extend,or = Un.fireEvent,sr = Un.inArray,ar = Un.isNumber,lr = Un.isObject,cr = Un.merge,hr = Un.pick,dr = Un.Point,ur = Un.Series,pr = Un.seriesTypes,fr = Un.setAnimation,gr = Un.splat,rr(Un.Chart.prototype, {
        addSeries: function (t, e, i) {
            var n, r = this;
            return t && (e = hr(e, !0), or(r, "addSeries", {options: t}, function () {
                n = r.initSeries(t), r.isDirtyLegend = !0, r.linkSeries(), e && r.redraw(i)
            })), n
        },
        addAxis: function (t, e, i, n) {
            var r = e ? "xAxis" : "yAxis", o = this.options;
            t = cr(t, {
                index: this[r].length,
                isX: e
            }), new Zn(this, t), o[r] = gr(o[r] || {}), o[r].push(t), hr(i, !0) && this.redraw(n)
        },
        showLoading: function (t) {
            var e = this, i = e.options, n = e.loadingDiv, r = i.loading, o = function () {
                n && tr(n, {
                    left: e.plotLeft + "px",
                    top: e.plotTop + "px",
                    width: e.plotWidth + "px",
                    height: e.plotHeight + "px"
                })
            };
            n || (e.loadingDiv = n = Jn("div", {className: "highcharts-loading highcharts-loading-hidden"}, null, e.container), e.loadingSpan = Jn("span", {className: "highcharts-loading-inner"}, null, n), Qn(e, "redraw", o)), n.className = "highcharts-loading", e.loadingSpan.innerHTML = t || i.lang.loading, tr(n, rr(r.style, {zIndex: 10})), tr(e.loadingSpan, r.labelStyle), e.loadingShown || (tr(n, {
                opacity: 0,
                display: ""
            }), Kn(n, {opacity: r.style.opacity || .5}, {duration: r.showDuration || 0})), e.loadingShown = !0, o()
        },
        hideLoading: function () {
            var t = this.options, e = this.loadingDiv;
            e && (e.className = "highcharts-loading highcharts-loading-hidden", Kn(e, {opacity: 0}, {
                duration: t.loading.hideDuration || 100,
                complete: function () {
                    tr(e, {display: "none"})
                }
            })), this.loadingShown = !1
        },
        propsRequireDirtyBox: "backgroundColor borderColor borderWidth margin marginTop marginRight marginBottom marginLeft spacing spacingTop spacingRight spacingBottom spacingLeft borderRadius plotBackgroundColor plotBackgroundImage plotBorderColor plotBorderWidth plotShadow shadow".split(" "),
        propsRequireUpdateSeries: ["chart.polar", "chart.ignoreHiddenSeries", "chart.type", "colors", "plotOptions"],
        update: function (t, e) {
            var i, n, r, o = {credits: "addCredits", title: "setTitle", subtitle: "setSubtitle"}, s = t.chart;
            if (s) {
                for (i in cr(!0, this.options.chart, s), "className" in s && this.setClassName(s.className), ("inverted" in s || "polar" in s) && (this.propFromSeries(), n = !0), s) s.hasOwnProperty(i) && (-1 !== sr("chart." + i, this.propsRequireUpdateSeries) && (r = !0), -1 !== sr(i, this.propsRequireDirtyBox) && (this.isDirtyBox = !0));
                "style" in s && this.renderer.setStyle(s.style)
            }
            for (i in t) this[i] && "function" == typeof this[i].update ? this[i].update(t[i], !1) : "function" == typeof this[o[i]] && this[o[i]](t[i]), "chart" !== i && -1 !== sr(i, this.propsRequireUpdateSeries) && (r = !0);
            t.colors && (this.options.colors = t.colors), t.plotOptions && cr(!0, this.options.plotOptions, t.plotOptions), ir(["xAxis", "yAxis", "series"], function (i) {
                t[i] && ir(gr(t[i]), function (t) {
                    var e = er(t.id) && this.get(t.id) || this[i][0];
                    e && e.coll === i && e.update(t, !1)
                }, this)
            }, this), n && ir(this.axes, function (t) {
                t.update({}, !1)
            }), r && ir(this.series, function (t) {
                t.update({}, !1)
            }), t.loading && cr(!0, this.options.loading, t.loading), i = s && s.width, s = s && s.height, ar(i) && i !== this.chartWidth || ar(s) && s !== this.chartHeight ? this.setSize(i, s) : hr(e, !0) && this.redraw()
        },
        setSubtitle: function (t) {
            this.setTitle(void 0, t)
        }
    }),rr(dr.prototype, {
        update: function (t, e, i, n) {
            function r() {
                s.applyOptions(t), null === s.y && l && (s.graphic = l.destroy()), lr(t, !0) && (l && l.element && t && t.marker && t.marker.symbol && (s.graphic = l.destroy()), t && t.dataLabels && s.dataLabel && (s.dataLabel = s.dataLabel.destroy())), o = s.index, a.updateParallelArrays(s, o), h.data[o] = lr(h.data[o], !0) ? s.options : t, a.isDirty = a.isDirtyData = !0, !a.fixedBox && a.hasCartesianSeries && (c.isDirtyBox = !0), "point" === h.legendType && (c.isDirtyLegend = !0), e && c.redraw(i)
            }

            var o, s = this, a = s.series, l = s.graphic, c = a.chart, h = a.options;
            e = hr(e, !0), !1 === n ? r() : s.firePointEvent("update", {options: t}, r)
        }, remove: function (t, e) {
            this.series.removePoint(sr(this, this.series.data), t, e)
        }
    }),rr(ur.prototype, {
        addPoint: function (t, e, i, n) {
            var r, o, s, a, l = this.options, c = this.data, h = this.chart, d = this.xAxis && this.xAxis.names,
                u = l.data, p = this.xData;
            if (e = hr(e, !0), r = {series: this}, this.pointClass.prototype.applyOptions.apply(r, [t]), a = r.x, s = p.length, this.requireSorting && a < p[s - 1]) for (o = !0; s && p[s - 1] > a;) s--;
            this.updateParallelArrays(r, "splice", s, 0, 0), this.updateParallelArrays(r, s), d && r.name && (d[a] = r.name), u.splice(s, 0, t), o && (this.data.splice(s, 0, null), this.processData()), "point" === l.legendType && this.generatePoints(), i && (c[0] && c[0].remove ? c[0].remove(!1) : (c.shift(), this.updateParallelArrays(r, "shift"), u.shift())), this.isDirtyData = this.isDirty = !0, e && h.redraw(n)
        }, removePoint: function (t, e, i) {
            var n = this, r = n.data, o = r[t], s = n.points, a = n.chart, l = function () {
                s && s.length === r.length && s.splice(t, 1), r.splice(t, 1), n.options.data.splice(t, 1), n.updateParallelArrays(o || {series: n}, "splice", t, 1), o && o.destroy(), n.isDirty = !0, n.isDirtyData = !0, e && a.redraw()
            };
            fr(i, a), e = hr(e, !0), o ? o.firePointEvent("remove", null, l) : l()
        }, remove: function (t, e, i) {
            function n() {
                r.destroy(), o.isDirtyLegend = o.isDirtyBox = !0, o.linkSeries(), hr(t, !0) && o.redraw(e)
            }

            var r = this, o = r.chart;
            !1 !== i ? or(r, "remove", null, n) : n()
        }, update: function (t, e) {
            var i, n = this, r = this.chart, o = this.userOptions, s = this.type,
                a = t.type || o.type || r.options.chart.type, l = pr[s].prototype,
                c = ["group", "markerGroup", "dataLabelsGroup"];
            for (i in(a && a !== s || void 0 !== t.zIndex) && (c.length = 0), ir(c, function (t) {
                c[t] = n[t], delete n[t]
            }), t = cr(o, {
                animation: !1,
                index: this.index,
                pointStart: this.xData[0]
            }, {data: this.options.data}, t), this.remove(!1, null, !1), l) this[i] = void 0;
            rr(this, pr[a || s].prototype), ir(c, function (t) {
                n[t] = c[t]
            }), this.init(r, t), r.linkSeries(), hr(e, !0) && r.redraw(!1)
        }
    }),rr(Zn.prototype, {
        update: function (t, e) {
            var i = this.chart;
            t = i.options[this.coll][this.options.index] = cr(this.userOptions, t), this.destroy(!0), this.init(i, rr(t, {events: void 0})), i.isDirtyBox = !0, hr(e, !0) && i.redraw()
        }, remove: function (t) {
            for (var e = this.chart, i = this.coll, n = this.series, r = n.length; r--;) n[r] && n[r].remove(!1);
            nr(e.axes, this), nr(e[i], this), e.options[i].splice(this.options.index, 1), ir(e[i], function (t, e) {
                t.options.index = e
            }), this.destroy(), e.isDirtyBox = !0, hr(t, !0) && e.redraw()
        }, setTitle: function (t, e) {
            this.update({title: t}, e)
        }, setCategories: function (t, e) {
            this.update({categories: t}, e)
        }
    }),vr = (mr = t).color,yr = mr.each,xr = mr.map,br = mr.pick,wr = mr.Series,(0, mr.seriesType)("area", "line", {
        softThreshold: !1,
        threshold: 0
    }, {
        singleStacks: !1, getStackPoints: function () {
            var l, c, t, e = [], h = [], i = this.xAxis, n = this.yAxis, d = n.stacks[this.stackKey], u = {},
                r = this.points, p = this.index, o = n.series, f = o.length,
                g = br(n.options.reversedStacks, !0) ? 1 : -1;
            if (this.options.stacking) {
                for (c = 0; c < r.length; c++) u[r[c].x] = r[c];
                for (t in d) null !== d[t].total && h.push(t);
                h.sort(function (t, e) {
                    return t - e
                }), l = xr(o, function () {
                    return this.visible
                }), yr(h, function (r, o) {
                    var s, a, t = 0;
                    if (u[r] && !u[r].isNull) e.push(u[r]), yr([-1, 1], function (t) {
                        var e = 1 === t ? "rightNull" : "leftNull", i = 0, n = d[h[o + t]];
                        if (n) for (c = p; 0 <= c && c < f;) (s = n.points[c]) || (c === p ? u[r][e] = !0 : l[c] && (a = d[r].points[c]) && (i -= a[1] - a[0])), c += g;
                        u[r][1 === t ? "rightCliff" : "leftCliff"] = i
                    }); else {
                        for (c = p; 0 <= c && c < f;) {
                            if (s = d[r].points[c]) {
                                t = s[1];
                                break
                            }
                            c += g
                        }
                        t = n.toPixels(t, !0), e.push({isNull: !0, plotX: i.toPixels(r, !0), plotY: t, yBottom: t})
                    }
                })
            }
            return e
        }, getGraphPath: function (a) {
            var t, e, l, i, n = wr.prototype.getGraphPath, c = (r = this.options).stacking, h = this.yAxis, d = [],
                u = [], p = this.index, f = h.stacks[this.stackKey], g = r.threshold, m = h.getThreshold(r.threshold),
                r = r.connectNulls || "percent" === c, o = function (t, e, i) {
                    var n = a[t];
                    t = c && f[n.x].points[p];
                    var r, o, s = n[i + "Null"] || 0;
                    i = n[i + "Cliff"] || 0, n = !0, i || s ? (r = (s ? t[0] : t[1]) + i, o = t[0] + i, n = !!s) : !c && a[e] && a[e].isNull && (r = o = g), void 0 !== r && (u.push({
                        plotX: l,
                        plotY: null === r ? m : h.getThreshold(r),
                        isNull: n
                    }), d.push({plotX: l, plotY: null === o ? m : h.getThreshold(o), doCurve: !1}))
                };
            for (a = a || this.points, c && (a = this.getStackPoints()), t = 0; t < a.length; t++) e = a[t].isNull, l = br(a[t].rectPlotX, a[t].plotX), i = br(a[t].yBottom, m), (!e || r) && (r || o(t, t - 1, "left"), e && !c && r || (u.push(a[t]), d.push({
                x: t,
                plotX: l,
                plotY: i
            })), r || o(t, t + 1, "right"));
            return t = n.call(this, u, !0, !0), d.reversed = !0, (e = n.call(this, d, !0, !0)).length && (e[0] = "L"), e = t.concat(e), n = n.call(this, u, !1, r), e.xMap = t.xMap, this.areaPath = e, n
        }, drawGraph: function () {
            this.areaPath = [], wr.prototype.drawGraph.apply(this);
            var n = this, r = this.areaPath, o = this.options,
                i = [["area", "highcharts-area", this.color, o.fillColor]];
            yr(this.zones, function (t, e) {
                i.push(["zone-area-" + e, "highcharts-area highcharts-zone-area-" + e + " " + t.className, t.color || n.color, t.fillColor || o.fillColor])
            }), yr(i, function (t) {
                var e = t[0], i = n[e];
                i ? (i.endX = r.xMap, i.animate({d: r})) : (i = n[e] = n.chart.renderer.path(r).addClass(t[1]).attr({
                    fill: br(t[3], vr(t[2]).setOpacity(br(o.fillOpacity, .75)).get()),
                    zIndex: 0
                }).add(n.group)).isArea = !0, i.startX = r.xMap, i.shiftUnit = o.step ? 2 : 1
            })
        }, drawLegendSymbol: mr.LegendSymbolMixin.drawRectangle
    }),_r = (Cr = t).extendClass,kr = Cr.merge,Sr = Cr.pick,Tr = Cr.Series,Ar = Cr.seriesTypes,Cr.defaultPlotOptions.spline = kr(Cr.defaultPlotOptions.line),Ar.spline = _r(Tr, {
        type: "spline",
        getPointSpline: function (t, e, i) {
            var n, r, o, s, a = e.plotX, l = e.plotY, c = t[i - 1];
            if (i = t[i + 1], c && !c.isNull && !1 !== c.doCurve && i && !i.isNull && !1 !== i.doCurve) {
                t = c.plotY, o = i.plotX;
                var h = 0;
                r = (1.5 * l + t) / 2.5, s = (1.5 * l + (i = i.plotY)) / 2.5, (o = (1.5 * a + o) / 2.5) != (n = (1.5 * a + c.plotX) / 2.5) && (h = (s - r) * (o - a) / (o - n) + l - s), s += h, t < (r += h) && l < r ? s = 2 * l - (r = Math.max(t, l)) : r < t && r < l && (s = 2 * l - (r = Math.min(t, l))), i < s && l < s ? r = 2 * l - (s = Math.max(i, l)) : s < i && s < l && (r = 2 * l - (s = Math.min(i, l))), e.rightContX = o, e.rightContY = s
            }
            return e = ["C", Sr(c.rightContX, c.plotX), Sr(c.rightContY, c.plotY), Sr(n, a), Sr(r, l), a, l], c.rightContX = c.rightContY = null, e
        }
    }),Mr = ($r = t).seriesTypes.area.prototype,(0, $r.seriesType)("areaspline", "spline", $r.defaultPlotOptions.area, {
        getStackPoints: Mr.getStackPoints,
        getGraphPath: Mr.getGraphPath,
        setStackCliffs: Mr.setStackCliffs,
        drawGraph: Mr.drawGraph,
        drawLegendSymbol: $r.LegendSymbolMixin.drawRectangle
    }),Pr = (Er = t).animObject,Lr = Er.color,Ir = Er.each,Fr = Er.extend,Dr = Er.isNumber,Or = Er.merge,Nr = Er.pick,jr = Er.Series,zr = Er.seriesType,Rr = Er.stop,Br = Er.svg,zr("column", "line", {
        borderRadius: 0,
        groupPadding: .2,
        marker: null,
        pointPadding: .1,
        minPointLength: 0,
        cropThreshold: 50,
        pointRange: null,
        states: {
            hover: {halo: !1, brightness: .1, shadow: !1},
            select: {color: "#cccccc", borderColor: "#000000", shadow: !1}
        },
        dataLabels: {align: null, verticalAlign: null, y: null},
        softThreshold: !1,
        startFromThreshold: !0,
        stickyTracking: !1,
        tooltip: {distance: 6},
        threshold: 0,
        borderColor: "#ffffff"
    }, {
        cropShoulder: 0,
        directTouch: !0,
        trackerGroups: ["group", "dataLabelsGroup"],
        negStacks: !0,
        init: function () {
            jr.prototype.init.apply(this, arguments);
            var e = this, t = e.chart;
            t.hasRendered && Ir(t.series, function (t) {
                t.type === e.type && (t.isDirty = !0)
            })
        },
        getColumnMetrics: function () {
            var r, o = this, t = o.options, e = o.xAxis, s = o.yAxis, i = e.reversed, a = {}, l = 0;
            !1 === t.grouping ? l = 1 : Ir(o.chart.series, function (t) {
                var e, i = t.options, n = t.yAxis;
                t.type === o.type && t.visible && s.len === n.len && s.pos === n.pos && (i.stacking ? (r = t.stackKey, void 0 === a[r] && (a[r] = l++), e = a[r]) : !1 !== i.grouping && (e = l++), t.columnIndex = e)
            });
            var n = Math.min(Math.abs(e.transA) * (e.ordinalSlope || t.pointRange || e.closestPointRange || e.tickInterval || 1), e.len),
                c = n * t.groupPadding, h = (n - 2 * c) / l;
            return t = Math.min(t.maxPointWidth || e.len, Nr(t.pointWidth, h * (1 - 2 * t.pointPadding))), o.columnMetrics = {
                width: t,
                offset: (h - t) / 2 + (c + ((o.columnIndex || 0) + (i ? 1 : 0)) * h - n / 2) * (i ? -1 : 1)
            }, o.columnMetrics
        },
        crispCol: function (t, e, i, n) {
            var r = this.chart, o = -((s = this.borderWidth) % 2 ? .5 : 0), s = s % 2 ? .5 : 1;
            return r.inverted && r.renderer.isVML && (s += 1), i = Math.round(t + i) + o, t = Math.round(t) + o, n = Math.round(e + n) + s, o = Math.abs(e) <= .5 && .5 < n, n -= e = Math.round(e) + s, o && n && (--e, n += 1), {
                x: t,
                y: e,
                width: i - t,
                height: n
            }
        },
        translate: function () {
            var l = this, c = l.chart, t = l.options, e = l.dense = l.closestPointRange * l.xAxis.transA < 2,
                h = (e = l.borderWidth = Nr(t.borderWidth, e ? 0 : 1), l.yAxis),
                d = l.translatedThreshold = h.getThreshold(t.threshold), u = Nr(t.minPointLength, 5),
                i = l.getColumnMetrics(), p = i.width, f = l.barW = Math.max(p, 1 + 2 * e),
                g = l.pointXOffset = i.offset;
            c.inverted && (d -= .5), t.pointPadding && (f = Math.ceil(f)), jr.prototype.translate.apply(l), Ir(l.points, function (t) {
                var e, i = Nr(t.yBottom, d), n = 999 + Math.abs(i),
                    r = (n = Math.min(Math.max(-n, t.plotY), h.len + n), t.plotX + g), o = f, s = Math.min(n, i),
                    a = Math.max(n, i) - s;
                Math.abs(a) < u && u && (a = u, e = !h.reversed && !t.negative || h.reversed && t.negative, s = Math.abs(s - d) > u ? i - u : d - (e ? u : 0)), t.barX = r, t.pointWidth = p, t.tooltipPos = c.inverted ? [h.len + h.pos - c.plotLeft - n, l.xAxis.len - r - o / 2, a] : [r + o / 2, n + h.pos - c.plotTop, a], t.shapeType = "rect", t.shapeArgs = l.crispCol.apply(l, t.isNull ? [t.plotX, h.len / 2, 0, 0] : [r, s, o, a])
            })
        },
        getSymbol: Er.noop,
        drawLegendSymbol: Er.LegendSymbolMixin.drawRectangle,
        drawGraph: function () {
            this.group[this.dense ? "addClass" : "removeClass"]("highcharts-dense-data")
        },
        pointAttribs: function (t, e) {
            var i, n = this.options, r = (l = this.pointAttrToOptions || {}).stroke || "borderColor",
                o = l["stroke-width"] || "borderWidth", s = t && t.color || this.color,
                a = t[r] || n[r] || this.color || s, l = n.dashStyle;
            return t && this.zones.length && (s = (s = t.getZone()) && s.color || t.options.color || this.color), e && (i = (e = n.states[e]).brightness, s = e.color || void 0 !== i && Lr(s).brighten(e.brightness).get() || s, a = e[r] || a, l = e.dashStyle || l), t = {
                fill: s,
                stroke: a,
                "stroke-width": t[o] || n[o] || this[o] || 0
            }, n.borderRadius && (t.r = n.borderRadius), l && (t.dashstyle = l), t
        },
        drawPoints: function () {
            var i, n = this, r = this.chart, o = n.options, s = r.renderer, a = o.animationLimit || 250;
            Ir(n.points, function (t) {
                var e = t.graphic;
                Dr(t.plotY) && null !== t.y ? (i = t.shapeArgs, e ? (Rr(e), e[r.pointCount < a ? "animate" : "attr"](Or(i))) : t.graphic = e = s[t.shapeType](i).attr({"class": t.getClassName()}).add(t.group || n.group), e.attr(n.pointAttribs(t, t.selected && "select")).shadow(o.shadow, null, o.stacking && !o.borderRadius)) : e && (t.graphic = e.destroy())
            })
        },
        animate: function (t) {
            var i = this, e = this.yAxis, n = i.options, r = this.chart.inverted, o = {};
            Br && (t ? (o.scaleY = .001, t = Math.min(e.pos + e.len, Math.max(e.pos, e.toPixels(n.threshold))), r ? o.translateX = t - e.len : o.translateY = t, i.group.attr(o)) : (o[r ? "translateX" : "translateY"] = e.pos, i.group.animate(o, Fr(Pr(i.options.animation), {
                step: function (t, e) {
                    i.group.attr({scaleY: Math.max(.001, e.pos)})
                }
            })), i.animate = null))
        },
        remove: function () {
            var e = this, t = e.chart;
            t.hasRendered && Ir(t.series, function (t) {
                t.type === e.type && (t.isDirty = !0)
            }), jr.prototype.remove.apply(e, arguments)
        }
    }),(0, t.seriesType)("bar", "column", null, {inverted: !0}),Wr = (Hr = t).Series,(Hr = Hr.seriesType)("scatter", "line", {
        lineWidth: 0,
        marker: {enabled: !0},
        tooltip: {
            headerFormat: '<span style="color:{point.color}">\u25cf</span> <span style="font-size: 0.85em"> {series.name}</span><br/>',
            pointFormat: "x: <b>{point.x}</b><br/>y: <b>{point.y}</b><br/>"
        }
    }, {
        sorted: !1,
        requireSorting: !1,
        noSharedTooltip: !0,
        trackerGroups: ["group", "markerGroup", "dataLabelsGroup"],
        takeOrdinalPosition: !1,
        kdDimensions: 2,
        drawGraph: function () {
            this.options.lineWidth && Wr.prototype.drawGraph.call(this)
        }
    }),Gr = (qr = t).pick,Xr = qr.relativeLength,qr.CenteredSeriesMixin = {
        getCenter: function () {
            var t, e, i = this.options, n = this.chart, r = 2 * (i.slicedOffset || 0), o = n.plotWidth - 2 * r,
                s = (n = n.plotHeight - 2 * r, i.center),
                a = (s = [Gr(s[0], "50%"), Gr(s[1], "50%"), i.size || "100%", i.innerSize || 0], Math.min(o, n));
            for (t = 0; t < 4; ++t) e = s[t], i = t < 2 || 2 === t && /%$/.test(e), s[t] = Xr(e, [o, n, a, s[2]][t]) + (i ? r : 0);
            return s[3] > s[2] && (s[3] = s[2]), s
        }
    },Vr = (Yr = t).addEvent,Ur = Yr.defined,Qr = Yr.each,Kr = Yr.extend,Zr = Yr.inArray,Jr = Yr.noop,to = Yr.pick,eo = Yr.Point,io = Yr.Series,no = Yr.seriesType,ro = Yr.setAnimation,no("pie", "line", {
        center: [null, null],
        clip: !1,
        colorByPoint: !0,
        dataLabels: {
            distance: 30, enabled: !0, formatter: function () {
                return null === this.y ? void 0 : this.point.name
            }, x: 0
        },
        ignoreHiddenPoint: !0,
        legendType: "point",
        marker: null,
        size: null,
        showInLegend: !1,
        slicedOffset: 10,
        stickyTracking: !1,
        tooltip: {followPointer: !0},
        borderColor: "#ffffff",
        borderWidth: 1,
        states: {hover: {brightness: .1, shadow: !1}}
    }, {
        isCartesian: !1,
        requireSorting: !1,
        directTouch: !0,
        noSharedTooltip: !0,
        trackerGroups: ["group", "dataLabelsGroup"],
        axisTypes: [],
        pointAttribs: Yr.seriesTypes.column.prototype.pointAttribs,
        animate: function (t) {
            var n = this, e = n.points, r = n.startAngleRad;
            t || (Qr(e, function (t) {
                var e = t.graphic, i = t.shapeArgs;
                e && (e.attr({r: t.startR || n.center[3] / 2, start: r, end: r}), e.animate({
                    r: i.r,
                    start: i.start,
                    end: i.end
                }, n.options.animation))
            }), n.animate = null)
        },
        updateTotals: function () {
            var t, e, i = 0, n = this.points, r = n.length, o = this.options.ignoreHiddenPoint;
            for (t = 0; t < r; t++) (e = n[t]).y < 0 && (e.y = null), i += o && !e.visible ? 0 : e.y;
            for (this.total = i, t = 0; t < r; t++) (e = n[t]).percentage = 0 < i && (e.visible || !o) ? e.y / i * 100 : 0, e.total = i
        },
        generatePoints: function () {
            io.prototype.generatePoints.call(this), this.updateTotals()
        },
        translate: function (i) {
            this.generatePoints();
            var t, e, n, r, o, s = 0, a = (p = this.options).slicedOffset, l = a + (p.borderWidth || 0),
                c = p.startAngle || 0, h = this.startAngleRad = Math.PI / 180 * (c - 90),
                d = (c = (this.endAngleRad = Math.PI / 180 * (to(p.endAngle, c + 360) - 90)) - h, this.points),
                u = p.dataLabels.distance, p = p.ignoreHiddenPoint, f = d.length;
            for (i || (this.center = i = this.getCenter()), this.getX = function (t, e) {
                return n = Math.asin(Math.min((t - i[1]) / (i[2] / 2 + u), 1)), i[0] + (e ? -1 : 1) * Math.cos(n) * (i[2] / 2 + u)
            }, r = 0; r < f; r++) o = d[r], t = h + s * c, p && !o.visible || (s += o.percentage / 100), e = h + s * c, o.shapeType = "arc", o.shapeArgs = {
                x: i[0],
                y: i[1],
                r: i[2] / 2,
                innerR: i[3] / 2,
                start: Math.round(1e3 * t) / 1e3,
                end: Math.round(1e3 * e) / 1e3
            }, (n = (e + t) / 2) > 1.5 * Math.PI ? n -= 2 * Math.PI : n < -Math.PI / 2 && (n += 2 * Math.PI), o.slicedTranslation = {
                translateX: Math.round(Math.cos(n) * a),
                translateY: Math.round(Math.sin(n) * a)
            }, t = Math.cos(n) * i[2] / 2, e = Math.sin(n) * i[2] / 2, o.tooltipPos = [i[0] + .7 * t, i[1] + .7 * e], o.half = n < -Math.PI / 2 || n > Math.PI / 2 ? 1 : 0, o.angle = n, l = Math.min(l, u / 5), o.labelPos = [i[0] + t + Math.cos(n) * u, i[1] + e + Math.sin(n) * u, i[0] + t + Math.cos(n) * l, i[1] + e + Math.sin(n) * l, i[0] + t, i[1] + e, u < 0 ? "center" : o.half ? "right" : "left", n]
        },
        drawGraph: null,
        drawPoints: function () {
            var i, n, r, o, s = this, a = s.chart.renderer, l = s.options.shadow;
            l && !s.shadowGroup && (s.shadowGroup = a.g("shadow").add(s.group)), Qr(s.points, function (t) {
                if (null !== t.y) {
                    n = t.graphic, o = t.shapeArgs, i = t.sliced ? t.slicedTranslation : {};
                    var e = t.shadowGroup;
                    l && !e && (e = t.shadowGroup = a.g("shadow").add(s.shadowGroup)), e && e.attr(i), r = s.pointAttribs(t, t.selected && "select"), n ? n.setRadialReference(s.center).attr(r).animate(Kr(o, i)) : (t.graphic = n = a[t.shapeType](o).addClass(t.getClassName()).setRadialReference(s.center).attr(i).add(s.group), t.visible || n.attr({visibility: "hidden"}), n.attr(r).attr({"stroke-linejoin": "round"}).shadow(l, e))
                }
            })
        },
        searchPoint: Jr,
        sortByAngle: function (t, i) {
            t.sort(function (t, e) {
                return void 0 !== t.angle && (e.angle - t.angle) * i
            })
        },
        drawLegendSymbol: Yr.LegendSymbolMixin.drawRectangle,
        getCenter: Yr.CenteredSeriesMixin.getCenter,
        getSymbol: Jr
    }, {
        init: function () {
            eo.prototype.init.apply(this, arguments);
            var t, e = this;
            return e.name = to(e.name, "Slice"), Vr(e, "select", t = function (t) {
                e.slice("select" === t.type)
            }), Vr(e, "unselect", t), e
        }, setVisible: function (e, t) {
            var i = this, n = i.series, r = n.chart, o = n.options.ignoreHiddenPoint;
            t = to(t, o), e !== i.visible && (i.visible = i.options.visible = e = void 0 === e ? !i.visible : e, n.options.data[Zr(i, n.data)] = i.options, Qr(["graphic", "dataLabel", "connector", "shadowGroup"], function (t) {
                i[t] && i[t][e ? "show" : "hide"](!0)
            }), i.legendItem && r.legend.colorizeItem(i, e), e || "hover" !== i.state || i.setState(""), o && (n.isDirty = !0), t && r.redraw())
        }, slice: function (t, e, i) {
            var n = this.series;
            ro(i, n.chart), to(e, !0), this.sliced = this.options.sliced = t = Ur(t) ? t : !this.sliced, n.options.data[Zr(this, n.data)] = this.options, t = t ? this.slicedTranslation : {
                translateX: 0,
                translateY: 0
            }, this.graphic.animate(t), this.shadowGroup && this.shadowGroup.animate(t)
        }, haloPath: function (t) {
            var e = this.shapeArgs;
            return this.sliced || !this.visible ? [] : this.series.chart.renderer.symbols.arc(e.x, e.y, e.r + t, e.r + t, {
                innerR: this.shapeArgs.r,
                start: e.start,
                end: e.end
            })
        }
    }),so = (oo = t).addEvent,ao = oo.arrayMax,lo = oo.defined,co = oo.each,ho = oo.extend,uo = oo.format,po = oo.map,fo = oo.merge,go = oo.noop,mo = oo.pick,vo = oo.relativeLength,yo = oo.Series,xo = oo.seriesTypes,bo = oo.stableSort,wo = oo.stop,oo.distribute = function (t, e) {
        function i(t, e) {
            return t.target - e.target
        }

        var n, r, o = !0, s = t, a = [];
        for (r = 0, n = t.length; n--;) r += t[n].size;
        if (e < r) {
            for (bo(t, function (t, e) {
                return (e.rank || 0) - (t.rank || 0)
            }), r = n = 0; r <= e;) r += t[n].size, n++;
            a = t.splice(n - 1, t.length)
        }
        for (bo(t, i), t = po(t, function (t) {
            return {size: t.size, targets: [t.target]}
        }); o;) {
            for (n = t.length; n--;) o = t[n], r = (Math.min.apply(0, o.targets) + Math.max.apply(0, o.targets)) / 2, o.pos = Math.min(Math.max(0, r - o.size / 2), e - o.size);
            for (n = t.length, o = !1; n--;) 0 < n && t[n - 1].pos + t[n - 1].size > t[n].pos && (t[n - 1].size += t[n].size, t[n - 1].targets = t[n - 1].targets.concat(t[n].targets), t[n - 1].pos + t[n - 1].size > e && (t[n - 1].pos = e - t[n - 1].size), t.splice(n, 1), o = !0)
        }
        n = 0, co(t, function (t) {
            var e = 0;
            co(t.targets, function () {
                s[n].pos = t.pos + e, e += s[n].size, n++
            })
        }), s.push.apply(s, a), bo(s, i)
    },yo.prototype.drawDataLabels = function () {
        var c, h, d, u, p = this, f = p.options, g = f.dataLabels, t = p.points, e = p.hasRendered || 0,
            i = mo(g.defer, !0), m = p.chart.renderer;
        (g.enabled || p._hasPointLabels) && (p.dlProcessOptions && p.dlProcessOptions(g), u = p.plotGroup("dataLabelsGroup", "data-labels", i && !e ? "hidden" : "visible", g.zIndex || 6), i && (u.attr({opacity: +e}), e || so(p, "afterAnimate", function () {
            p.visible && u.show(!0), u[f.animation ? "animate" : "attr"]({opacity: 1}, {duration: 200})
        })), h = g, co(t, function (t) {
            var e, i, n, r, o = t.dataLabel, s = t.connector, a = !0, l = {};
            if (c = t.dlOptions || t.options && t.options.dataLabels, e = mo(c && c.enabled, h.enabled) && null !== t.y, o && !e) t.dataLabel = o.destroy(); else if (e) {
                if (r = (g = fo(h, c)).style, e = g.rotation, i = t.getLabelConfig(), d = g.format ? uo(g.format, i) : g.formatter.call(i, g), r.color = mo(g.color, r.color, p.color, "#000000"), o) lo(d) ? (o.attr({text: d}), a = !1) : (t.dataLabel = o = o.destroy(), s && (t.connector = s.destroy())); else if (lo(d)) {
                    for (n in o = {
                        fill: g.backgroundColor,
                        stroke: g.borderColor,
                        "stroke-width": g.borderWidth,
                        r: g.borderRadius || 0,
                        rotation: e,
                        padding: g.padding,
                        zIndex: 1
                    }, "contrast" === r.color && (l.color = g.inside || g.distance < 0 || f.stacking ? m.getContrast(t.color || p.color) : "#000000"), f.cursor && (l.cursor = f.cursor), o) void 0 === o[n] && delete o[n];
                    (o = t.dataLabel = m[e ? "text" : "label"](d, 0, -9999, g.shape, null, null, g.useHTML, null, "data-label").attr(o)).addClass("highcharts-data-label-color-" + t.colorIndex + " " + (g.className || "")), o.css(ho(r, l)), o.add(u), o.shadow(g.shadow)
                }
                o && p.alignDataLabel(t, o, g, null, a)
            }
        }))
    },yo.prototype.alignDataLabel = function (t, e, i, n, r) {
        var o, s = this.chart, a = s.inverted, l = mo(t.plotX, -9999), c = mo(t.plotY, -9999), h = e.getBBox(),
            d = i.rotation, u = i.align,
            p = this.visible && (t.series.forceDL || s.isInsidePlot(l, Math.round(c), a) || n && s.isInsidePlot(l, a ? n.x + 1 : n.y + n.height - 1, a)),
            f = "justify" === mo(i.overflow, "justify");
        p && (o = i.style.fontSize, o = s.renderer.fontMetrics(o, e).b, n = ho({
            x: a ? s.plotWidth - c : l,
            y: Math.round(a ? s.plotHeight - l : c),
            width: 0,
            height: 0
        }, n), ho(i, {
            width: h.width,
            height: h.height
        }), d ? (f = !1, a = s.renderer.rotCorr(o, d), a = {
            x: n.x + i.x + n.width / 2 + a.x,
            y: n.y + i.y + {top: 0, middle: .5, bottom: 1}[i.verticalAlign] * n.height
        }, e[r ? "attr" : "animate"](a).attr({align: u}), l = 180 < (l = (d + 720) % 360) && l < 360, "left" === u ? a.y -= l ? h.height : 0 : "center" === u ? (a.x -= h.width / 2, a.y -= h.height / 2) : "right" === u && (a.x -= h.width, a.y -= l ? 0 : h.height)) : (e.align(i, null, n), a = e.alignAttr), f ? this.justifyDataLabel(e, i, a, h, n, r) : mo(i.crop, !0) && (p = s.isInsidePlot(a.x, a.y) && s.isInsidePlot(a.x + h.width, a.y + h.height)), i.shape && !d && e.attr({
            anchorX: t.plotX,
            anchorY: t.plotY
        })), p || (wo(e), e.attr({y: -9999}), e.placed = !1)
    },yo.prototype.justifyDataLabel = function (t, e, i, n, r, o) {
        var s, a, l = this.chart, c = e.align, h = e.verticalAlign, d = t.box ? 0 : t.padding || 0;
        (s = i.x + d) < 0 && ("right" === c ? e.align = "left" : e.x = -s, a = !0), (s = i.x + n.width - d) > l.plotWidth && ("left" === c ? e.align = "right" : e.x = l.plotWidth - s, a = !0), (s = i.y + d) < 0 && ("bottom" === h ? e.verticalAlign = "top" : e.y = -s, a = !0), (s = i.y + n.height - d) > l.plotHeight && ("top" === h ? e.verticalAlign = "bottom" : e.y = l.plotHeight - s, a = !0), a && (t.placed = !o, t.align(e, null, r))
    },xo.pie && (xo.pie.prototype.drawDataLabels = function () {
        var l, i, c, h, d, u, p, f, g, m, v = this, t = v.data, y = v.chart, x = v.options.dataLabels,
            b = mo(x.connectorPadding, 10), n = mo(x.connectorWidth, 1), w = y.plotWidth, C = y.plotHeight,
            _ = x.distance, k = v.center, S = k[2] / 2, T = k[1], e = 0 < _, r = [[], []], A = [0, 0, 0, 0];
        v.visible && (x.enabled || v._hasPointLabels) && (yo.prototype.drawDataLabels.apply(v), co(t, function (t) {
            t.dataLabel && t.visible && (r[t.half].push(t), t.dataLabel._pos = null)
        }), co(r, function (t, e) {
            var i, n, r, o, s, a = t.length;
            if (a) for (v.sortByAngle(t, e - .5), 0 < _ && (i = Math.max(0, T - S - _), n = Math.min(T + S + _,
                y.plotHeight), r = po(t, function (t) {
                if (t.dataLabel) return s = t.dataLabel.getBBox().height || 21, {
                    target: t.labelPos[1] - i + s / 2,
                    size: s,
                    rank: t.y
                }
            }), oo.distribute(r, n + s - i)), m = 0; m < a; m++) l = t[m], d = l.labelPos, c = l.dataLabel, g = !1 === l.visible ? "hidden" : "inherit", o = d[1], r ? void 0 === r[m].pos ? g = "hidden" : (u = r[m].size, f = i + r[m].pos) : f = o, p = x.justify ? k[0] + (e ? -1 : 1) * (S + _) : v.getX(f < i + 2 || n - 2 < f ? o : f, e), c._attr = {
                visibility: g,
                align: d[6]
            }, c._pos = {
                x: p + x.x + ({left: b, right: -b}[d[6]] || 0),
                y: f + x.y - 10
            }, d.x = p, d.y = f, null === v.options.size && (h = c.width, p - h < b ? A[3] = Math.max(Math.round(h - p + b), A[3]) : w - b < p + h && (A[1] = Math.max(Math.round(p + h - w + b), A[1])), f - u / 2 < 0 ? A[0] = Math.max(Math.round(u / 2 - f), A[0]) : C < f + u / 2 && (A[2] = Math.max(Math.round(f + u / 2 - C), A[2])))
        }), 0 === ao(A) || this.verifyDataLabelOverflow(A)) && (this.placeDataLabels(), e && n && co(this.points, function (t) {
            var e;
            i = t.connector, (c = t.dataLabel) && c._pos && t.visible ? (g = c._attr.visibility, (e = !i) && (t.connector = i = y.renderer.path().addClass("highcharts-data-label-connector highcharts-color-" + t.colorIndex).add(v.dataLabelsGroup), i.attr({
                "stroke-width": n,
                stroke: x.connectorColor || t.color || "#666666"
            })), i[e ? "attr" : "animate"]({d: v.connectorPath(t.labelPos)}), i.attr("visibility", g)) : i && (t.connector = i.destroy())
        }))
    }, xo.pie.prototype.connectorPath = function (t) {
        var e = t.x, i = t.y;
        return mo(this.options.softConnector, !0) ? ["M", e + ("left" === t[6] ? 5 : -5), i, "C", e, i, 2 * t[2] - t[4], 2 * t[3] - t[5], t[2], t[3], "L", t[4], t[5]] : ["M", e + ("left" === t[6] ? 5 : -5), i, "L", t[2], t[3], "L", t[4], t[5]]
    }, xo.pie.prototype.placeDataLabels = function () {
        co(this.points, function (t) {
            var e = t.dataLabel;
            e && t.visible && ((t = e._pos) ? (e.attr(e._attr), e[e.moved ? "animate" : "attr"](t), e.moved = !0) : e && e.attr({y: -9999}))
        })
    }, xo.pie.prototype.alignDataLabel = go, xo.pie.prototype.verifyDataLabelOverflow = function (t) {
        var e, i, n = this.center, r = this.options, o = r.center, s = r.minSize || 80;
        return null !== o[0] ? e = Math.max(n[2] - Math.max(t[1], t[3]), s) : (e = Math.max(n[2] - t[1] - t[3], s), n[0] += (t[3] - t[1]) / 2), null !== o[1] ? e = Math.max(Math.min(e, n[2] - Math.max(t[0], t[2])), s) : (e = Math.max(Math.min(e, n[2] - t[0] - t[2]), s), n[1] += (t[0] - t[2]) / 2), e < n[2] ? (n[2] = e, n[3] = Math.min(vo(r.innerSize || 0, e), e), this.translate(n), this.drawDataLabels && this.drawDataLabels()) : i = !0, i
    }),xo.column && (xo.column.prototype.alignDataLabel = function (t, e, i, n, r) {
        var o = this.chart.inverted, s = t.series, a = t.dlBox || t.shapeArgs,
            l = mo(t.below, t.plotY > mo(this.translatedThreshold, s.yAxis.len)),
            c = mo(i.inside, !!this.options.stacking);
        a && ((n = fo(a)).y < 0 && (n.height += n.y, n.y = 0), 0 < (a = n.y + n.height - s.yAxis.len) && (n.height -= a), o && (n = {
            x: s.yAxis.len - n.y - n.height,
            y: s.xAxis.len - n.x - n.width,
            width: n.height,
            height: n.width
        }), c || (o ? (n.x += l ? 0 : n.width, n.width = 0) : (n.y += l ? n.height : 0, n.height = 0))), i.align = mo(i.align, !o || c ? "center" : l ? "right" : "left"), i.verticalAlign = mo(i.verticalAlign, o || c ? "middle" : l ? "top" : "bottom"), yo.prototype.alignDataLabel.call(this, t, e, i, n, r)
    }),_o = (Co = t).Chart,ko = Co.each,So = Co.pick,To = Co.addEvent,_o.prototype.callbacks.push(function (t) {
        function e() {
            var n = [];
            ko(t.series, function (t) {
                var e = t.options.dataLabels, i = t.dataLabelCollections || ["dataLabel"];
                (e.enabled || t._hasPointLabels) && !e.allowOverlap && t.visible && ko(i, function (e) {
                    ko(t.points, function (t) {
                        t[e] && (t[e].labelrank = So(t.labelrank, t.shapeArgs && t.shapeArgs.height), n.push(t[e]))
                    })
                })
            }), t.hideOverlappingLabels(n)
        }

        e(), To(t, "redraw", e)
    }),_o.prototype.hideOverlappingLabels = function (t) {
        var e, i, n, r, o, s, a, l, c, h = t.length, d = function (t, e, i, n, r, o, s, a) {
            return !(t + i < r || r + s < t || e + n < o || o + a < e)
        };
        for (i = 0; i < h; i++) (e = t[i]) && (e.oldOpacity = e.opacity, e.newOpacity = 1);
        for (t.sort(function (t, e) {
            return (e.labelrank || 0) - (t.labelrank || 0)
        }), i = 0; i < h; i++) for (n = t[i], e = i + 1; e < h; ++e) r = t[e], n && r && n.placed && r.placed && 0 !== n.newOpacity && 0 !== r.newOpacity && (o = n.alignAttr, s = r.alignAttr, a = n.parentGroup, l = r.parentGroup, c = 2 * (n.box ? 0 : n.padding), o = d(o.x + a.translateX, o.y + a.translateY, n.width - c, n.height - c, s.x + l.translateX, s.y + l.translateY, r.width - c, r.height - c)) && ((n.labelrank < r.labelrank ? n : r).newOpacity = 0);
        ko(t, function (t) {
            var e, i;
            t && (i = t.newOpacity, t.oldOpacity !== i && t.placed && (i ? t.show(!0) : e = function () {
                t.hide()
            }, t.alignAttr.opacity = i, t[t.isOld ? "animate" : "attr"](t.alignAttr, null, e)), t.isOld = !0)
        })
    },Mo = (Ao = t).addEvent,Eo = Ao.Chart,Po = Ao.createElement,Lo = Ao.css,Io = Ao.defaultOptions,Fo = Ao.defaultPlotOptions,Do = Ao.each,Oo = Ao.extend,No = Ao.fireEvent,jo = Ao.hasTouch,zo = Ao.inArray,Ro = Ao.isObject,Bo = Ao.Legend,Ho = Ao.merge,Wo = Ao.pick,qo = Ao.Point,Go = Ao.Series,Xo = Ao.seriesTypes,Yo = Ao.svg,$o = Ao.TrackerMixin = {
        drawTrackerPoint: function () {
            var e = this, n = e.chart, i = n.pointer, r = function (t) {
                for (var e, i = t.target; i && !e;) e = i.point, i = i.parentNode;
                void 0 !== e && e !== n.hoverPoint && e.onMouseOver(t)
            };
            Do(e.points, function (t) {
                t.graphic && (t.graphic.element.point = t), t.dataLabel && (t.dataLabel.element.point = t)
            }), e._hasTracking || (Do(e.trackerGroups, function (t) {
                e[t] && (e[t].addClass("highcharts-tracker").on("mouseover", r).on("mouseout", function (t) {
                    i.onTrackerMouseOut(t)
                }), jo && e[t].on("touchstart", r), e.options.cursor && e[t].css(Lo).css({cursor: e.options.cursor}))
            }), e._hasTracking = !0)
        }, drawTrackerGraph: function () {
            var t, e = this, i = e.options, n = i.trackByArea, r = [].concat(n ? e.areaPath : e.graphPath),
                o = r.length, s = e.chart, a = s.pointer, l = s.renderer, c = s.options.tooltip.snap, h = e.tracker,
                d = function () {
                    s.hoverSeries !== e && e.onMouseOver()
                }, u = "rgba(192,192,192," + (Yo ? 1e-4 : .002) + ")";
            if (o && !n) for (t = o + 1; t--;) "M" === r[t] && r.splice(t + 1, 0, r[t + 1] - c, r[t + 2], "L"), (t && "M" === r[t] || t === o) && r.splice(t, 0, "L", r[t - 2] + c, r[t - 1]);
            h ? h.attr({d: r}) : e.graph && (e.tracker = l.path(r).attr({
                "stroke-linejoin": "round",
                visibility: e.visible ? "visible" : "hidden",
                stroke: u,
                fill: n ? u : "none",
                "stroke-width": e.graph.strokeWidth() + (n ? 0 : 2 * c),
                zIndex: 2
            }).add(e.group), Do([e.tracker, e.markerGroup], function (t) {
                t.addClass("highcharts-tracker").on("mouseover", d).on("mouseout", function (t) {
                    a.onTrackerMouseOut(t)
                }), i.cursor && t.css({cursor: i.cursor}), jo && t.on("touchstart", d)
            }))
        }
    },Xo.column && (Xo.column.prototype.drawTracker = $o.drawTrackerPoint),Xo.pie && (Xo.pie.prototype.drawTracker = $o.drawTrackerPoint),Xo.scatter && (Xo.scatter.prototype.drawTracker = $o.drawTrackerPoint),Oo(Bo.prototype, {
        setItemEvents: function (i, t, e) {
            var n = this, r = n.chart, o = "highcharts-legend-" + (i.series ? "point" : "series") + "-active";
            (e ? t : i.legendGroup).on("mouseover", function () {
                i.setState("hover"), r.seriesGroup.addClass(o), t.css(n.options.itemHoverStyle)
            }).on("mouseout", function () {
                t.css(i.visible ? n.itemStyle : n.itemHiddenStyle), r.seriesGroup.removeClass(o), i.setState()
            }).on("click", function (t) {
                var e = function () {
                    i.setVisible && i.setVisible()
                };
                t = {browserEvent: t}, i.firePointEvent ? i.firePointEvent("legendItemClick", t, e) : No(i, "legendItemClick", t, e)
            })
        }, createCheckboxForItem: function (e) {
            e.checkbox = Po("input", {
                type: "checkbox",
                checked: e.selected,
                defaultChecked: e.selected
            }, this.options.itemCheckboxStyle, this.chart.container), Mo(e.checkbox, "click", function (t) {
                No(e.series || e, "checkboxClick", {checked: t.target.checked, item: e}, function () {
                    e.select()
                })
            })
        }
    }),Io.legend.itemStyle.cursor = "pointer",Oo(Eo.prototype, {
        showResetZoom: function () {
            var t = this, e = Io.lang, i = t.options.chart.resetZoomButton, n = i.theme, r = n.states,
                o = "chart" === i.relativeTo ? null : "plotBox";
            this.resetZoomButton = t.renderer.button(e.resetZoom, null, null, function () {
                t.zoomOut()
            }, n, r && r.hover).attr({
                align: i.position.align,
                title: e.resetZoomTitle
            }).addClass("highcharts-reset-zoom").add().align(i.position, !1, o)
        }, zoomOut: function () {
            var t = this;
            No(t, "selection", {resetSelection: !0}, function () {
                t.zoom()
            })
        }, zoom: function (t) {
            var n, e, r = this.pointer, o = !1;
            !t || t.resetSelection ? Do(this.axes, function (t) {
                n = t.zoom()
            }) : Do(t.xAxis.concat(t.yAxis), function (t) {
                var e = t.axis, i = e.isXAxis;
                (r[i ? "zoomX" : "zoomY"] || r[i ? "pinchX" : "pinchY"]) && (n = e.zoom(t.min, t.max), e.displayBtn && (o = !0))
            }), e = this.resetZoomButton, o && !e ? this.showResetZoom() : !o && Ro(e) && (this.resetZoomButton = e.destroy()), n && this.redraw(Wo(this.options.chart.animation, t && t.animation, this.pointCount < 100))
        }, pan: function (a, t) {
            var l, c = this, e = c.hoverPoints;
            e && Do(e, function (t) {
                t.setState()
            }), Do("xy" === t ? [1, 0] : [1], function (t) {
                var e = (t = c[t ? "xAxis" : "yAxis"][0]).horiz, i = a[e ? "chartX" : "chartY"],
                    n = c[e = e ? "mouseDownX" : "mouseDownY"], r = (t.pointRange || 0) / 2, o = t.getExtremes(),
                    s = t.toValue(n - i, !0) + r;
                r = t.toValue(n + t.len - i, !0) - r, n = i < n, t.series.length && (n || s > Math.min(o.dataMin, o.min)) && (!n || r < Math.max(o.dataMax, o.max)) && (t.setExtremes(s, r, !1, !1, {trigger: "pan"}), l = !0), c[e] = i
            }), l && c.redraw(!1), Lo(c.container, {cursor: "move"})
        }
    }),Oo(qo.prototype, {
        select: function (t, e) {
            var i = this, n = i.series, r = n.chart;
            t = Wo(t, !i.selected), i.firePointEvent(t ? "select" : "unselect", {accumulate: e}, function () {
                i.selected = i.options.selected = t, n.options.data[zo(i, n.data)] = i.options, i.setState(t && "select"), e || Do(r.getSelectedPoints(), function (t) {
                    t.selected && t !== i && (t.selected = t.options.selected = !1, n.options.data[zo(t, n.data)] = t.options, t.setState(""), t.firePointEvent("unselect"))
                })
            })
        }, onMouseOver: function (t, e) {
            var i = this.series, n = i.chart, r = n.tooltip, o = n.hoverPoint;
            this.series && (e || (o && o !== this && o.onMouseOut(), n.hoverSeries !== i && i.onMouseOver(), n.hoverPoint = this), !r || r.shared && !i.noSharedTooltip ? r || this.setState("hover") : (this.setState("hover"), r.refresh(this, t)), this.firePointEvent("mouseOver"))
        }, onMouseOut: function () {
            var t = this.series.chart, e = t.hoverPoints;
            this.firePointEvent("mouseOut"), e && -1 !== zo(this, e) || (this.setState(), t.hoverPoint = null)
        }, importEvents: function () {
            if (!this.hasImportedEvents) {
                var t, e = Ho(this.series.options.point, this.options).events;
                for (t in this.events = e) Mo(this, t, e[t]);
                this.hasImportedEvents = !0
            }
        }, setState: function (t, e) {
            var i, n = Math.floor(this.plotX), r = this.plotY, o = this.series, s = o.options.states[t] || {},
                a = Fo[o.type].marker && o.options.marker, l = a && !1 === a.enabled,
                c = a && a.states && a.states[t] || {}, h = !1 === c.enabled, d = o.stateMarkerGraphic,
                u = this.marker || {}, p = o.chart, f = o.halo;
            (t = t || "") === this.state && !e || this.selected && "select" !== t || !1 === s.enabled || t && (h || l && !1 === c.enabled) || t && u.states && u.states[t] && !1 === u.states[t].enabled || (a && o.markerAttribs && (i = o.markerAttribs(this, t)), this.graphic ? (this.state && this.graphic.removeClass("highcharts-point-" + this.state), t && this.graphic.addClass("highcharts-point-" + t), this.graphic.attr(o.pointAttribs(this, t)), i && this.graphic.animate(i, Wo(p.options.chart.animation, c.animation, a.animation)), d && d.hide()) : (t && c && (a = u.symbol || o.symbol, d && d.currentSymbol !== a && (d = d.destroy()), d ? d[e ? "animate" : "attr"]({
                x: i.x,
                y: i.y
            }) : a && (o.stateMarkerGraphic = d = p.renderer.symbol(a, i.x, i.y, i.width, i.height).add(o.markerGroup), d.currentSymbol = a), d && d.attr(o.pointAttribs(this, t))), d && (d[t && p.isInsidePlot(n, r, p.inverted) ? "show" : "hide"](), d.element.point = this)), (n = s.halo) && n.size ? (f || (o.halo = f = p.renderer.path().add(o.markerGroup || o.group)), Ao.stop(f), f[e ? "animate" : "attr"]({d: this.haloPath(n.size)}), f.attr({"class": "highcharts-halo highcharts-color-" + Wo(this.colorIndex, o.colorIndex)}), f.attr(Oo({
                fill: this.color || o.color,
                "fill-opacity": n.opacity,
                zIndex: -1
            }, n.attributes))) : f && f.animate({d: this.haloPath(0)}), this.state = t)
        }, haloPath: function (t) {
            return this.series.chart.renderer.symbols.circle(Math.floor(this.plotX) - t, this.plotY - t, 2 * t, 2 * t)
        }
    }),Oo(Go.prototype, {
        onMouseOver: function () {
            var t = this.chart, e = t.hoverSeries;
            e && e !== this && e.onMouseOut(), this.options.events.mouseOver && No(this, "mouseOver"), this.setState("hover"), t.hoverSeries = this
        }, onMouseOut: function () {
            var t = this.options, e = this.chart, i = e.tooltip, n = e.hoverPoint;
            e.hoverSeries = null, n && n.onMouseOut(), this && t.events.mouseOut && No(this, "mouseOut"), !i || t.stickyTracking || i.shared && !this.noSharedTooltip || i.hide(), this.setState()
        }, setState: function (e) {
            var i = this, t = i.options, n = i.graph, r = t.states, o = t.lineWidth;
            if (t = 0, e = e || "", i.state !== e && (Do([i.group, i.markerGroup], function (t) {
                t && (i.state && t.removeClass("highcharts-series-" + i.state), e && t.addClass("highcharts-series-" + e))
            }), !r[i.state = e] || !1 !== r[e].enabled) && (e && (o = r[e].lineWidth || o + (r[e].lineWidthPlus || 0)), n && !n.dashstyle)) for (r = {"stroke-width": o}, n.attr(r); i["zone-graph-" + t];) i["zone-graph-" + t].attr(r), t += 1
        }, setVisible: function (e, t) {
            var i, n = this, r = n.chart, o = n.legendItem, s = r.options.chart.ignoreHiddenSeries, a = n.visible;
            i = (n.visible = e = n.options.visible = n.userOptions.visible = void 0 === e ? !a : e) ? "show" : "hide", Do(["group", "dataLabelsGroup", "markerGroup", "tracker", "tt"], function (t) {
                n[t] && n[t][i]()
            }), r.hoverSeries !== n && (r.hoverPoint && r.hoverPoint.series) !== n || n.onMouseOut(), o && r.legend.colorizeItem(n, e), n.isDirty = !0, n.options.stacking && Do(r.series, function (t) {
                t.options.stacking && t.visible && (t.isDirty = !0)
            }), Do(n.linkedSeries, function (t) {
                t.setVisible(e, !1)
            }), s && (r.isDirtyBox = !0), !1 !== t && r.redraw(), No(n, i)
        }, show: function () {
            this.setVisible(!0)
        }, hide: function () {
            this.setVisible(!1)
        }, select: function (t) {
            this.selected = t = void 0 === t ? !this.selected : t, this.checkbox && (this.checkbox.checked = t), No(this, t ? "select" : "unselect")
        }, drawTracker: $o.drawTrackerGraph
    }),Uo = (Vo = t).Chart,Qo = Vo.each,Ko = Vo.inArray,Zo = Vo.isObject,Jo = Vo.pick,ts = Vo.splat,Uo.prototype.setResponsive = function (e) {
        var t = this.options.responsive;
        t && t.rules && Qo(t.rules, function (t) {
            this.matchResponsiveRule(t, e)
        }, this)
    },Uo.prototype.matchResponsiveRule = function (t, e) {
        var i, n = this.respRules, r = t.condition;
        i = t.callback || function () {
            return this.chartWidth <= Jo(r.maxWidth, Number.MAX_VALUE) && this.chartHeight <= Jo(r.maxHeight, Number.MAX_VALUE) && this.chartWidth >= Jo(r.minWidth, 0) && this.chartHeight >= Jo(r.minHeight, 0)
        }, void 0 === t._id && (t._id = Vo.idCounter++), i = i.call(this), !n[t._id] && i ? t.chartOptions && (n[t._id] = this.currentOptions(t.chartOptions), this.update(t.chartOptions, e)) : n[t._id] && !i && (this.update(n[t._id], e), delete n[t._id])
    },Uo.prototype.currentOptions = function (t) {
        function o(t, e, i) {
            var n, r;
            for (n in t) if (-1 < Ko(n, ["series", "xAxis", "yAxis"])) for (t[n] = ts(t[n]), i[n] = [], r = 0; r < t[n].length; r++) i[n][r] = {}, o(t[n][r], e[n][r], i[n][r]); else Zo(t[n]) ? (i[n] = {}, o(t[n], e[n] || {}, i[n])) : i[n] = e[n] || null
        }

        var e = {};
        return o(t, this.options, e), e
    },t
}), Highcharts.theme = {
    colors: ["#058DC7", "#50B432", "#ED561B", "#DDDF00", "#24CBE5", "#64E572", "#FF9655", "#FFF263", "#6AF9C4"],
    title: {style: {color: "#000", font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'}},
    subtitle: {style: {color: "#666666", font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'}},
    xAxis: {
        gridLineWidth: 1,
        lineColor: "#000",
        tickColor: "#000",
        labels: {style: {color: "#000", font: "11px Trebuchet MS, Verdana, sans-serif"}},
        title: {
            style: {
                color: "#333",
                fontWeight: "bold",
                fontSize: "12px",
                fontFamily: "Trebuchet MS, Verdana, sans-serif"
            }
        }
    },
    yAxis: {
        minorTickInterval: "auto",
        lineColor: "#000",
        lineWidth: 1,
        tickWidth: 1,
        tickColor: "#000",
        labels: {style: {color: "#000", font: "11px Trebuchet MS, Verdana, sans-serif"}},
        title: {
            style: {
                color: "#333",
                fontWeight: "bold",
                fontSize: "12px",
                fontFamily: "Trebuchet MS, Verdana, sans-serif"
            }
        }
    },
    legend: {
        itemStyle: {font: "9pt Trebuchet MS, Verdana, sans-serif", color: "black"},
        itemHoverStyle: {color: "#039"},
        itemHiddenStyle: {color: "gray"}
    },
    labels: {style: {color: "#99b"}},
    toolbar: {itemStyle: {color: "#CCC"}},
    navigation: {buttonOptions: {theme: {stroke: "#CCCCCC"}}},
    rangeSelector: {
        buttonTheme: {
            fill: {
                linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                stops: [[.4, "#888"], [.6, "#555"]]
            },
            stroke: "#000000",
            style: {color: "#CCC", fontWeight: "bold"},
            states: {
                hover: {
                    fill: {linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1}, stops: [[.4, "#BBB"], [.6, "#888"]]},
                    stroke: "#000000",
                    style: {color: "white"}
                },
                select: {
                    fill: {linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1}, stops: [[.1, "#000"], [.3, "#333"]]},
                    stroke: "#000000",
                    style: {color: "yellow"}
                }
            }
        }, inputStyle: {backgroundColor: "#333", color: "silver"}, labelStyle: {color: "silver"}
    },
    navigator: {
        handles: {backgroundColor: "#666", borderColor: "#AAA"},
        outlineColor: "#CCC",
        maskFill: "rgba(16, 16, 16, 0.5)",
        series: {color: "#7798BF", lineColor: "#A6C7ED"}
    },
    scrollbar: {
        barBackgroundColor: {linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1}, stops: [[.4, "#888"], [.6, "#555"]]},
        barBorderColor: "#CCC",
        buttonArrowColor: "#CCC",
        buttonBackgroundColor: {linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1}, stops: [[.4, "#888"], [.6, "#555"]]},
        buttonBorderColor: "#CCC",
        rifleColor: "#FFF",
        trackBackgroundColor: {linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1}, stops: [[0, "#000"], [1, "#333"]]},
        trackBorderColor: "#666"
    },
    legendBackgroundColor: "rgba(48, 48, 48, 0.8)",
    legendBackgroundColorSolid: "rgb(70, 70, 70)",
    dataLabelsColor: "#444",
    textColor: "#E0E0E0",
    maskColor: "rgba(255,255,255,0.3)"
}, $(document).ready(function () {
    $('a[data-toggle="tab"]').on("shown.bs.tab", function () {
        $(".tab-content .tab-pane.active .statistic_highchart").each(function (t, e) {
            $(e).highcharts().setSize($(".tab-pane").width())
        })
    })
}), function (ct) {
    ct.fn.slide = function (lt) {
        return ct.fn.slide.defaults = {
            type: "slide",
            effect: "fade",
            autoPlay: !1,
            delayTime: 500,
            interTime: 2500,
            triggerTime: 150,
            defaultIndex: 0,
            titCell: ".hd li",
            mainCell: ".bd",
            targetCell: null,
            trigger: "mouseover",
            scroll: 1,
            vis: 1,
            titOnClassName: "on",
            autoPage: !1,
            prevCell: ".prev",
            nextCell: ".next",
            pageStateCell: ".pageState",
            opp: !1,
            pnLoop: !0,
            easing: "swing",
            startFun: null,
            endFun: null,
            switchLoad: null,
            playStateCell: ".playState",
            mouseOverStop: !0,
            defaultPlay: !0,
            returnDefault: !1
        }, this.each(function () {
            var e = ct.extend({}, ct.fn.slide.defaults, lt), t = ct(this), s = e.effect, r = ct(e.prevCell, t),
                o = ct(e.nextCell, t), a = ct(e.pageStateCell, t), i = ct(e.playStateCell, t),
                l = (X = ct(e.titCell, t)).size(), c = ct(e.mainCell, t), h = c.children().size(), d = e.switchLoad,
                u = ct(e.targetCell, t), p = parseInt(e.defaultIndex), f = parseInt(e.delayTime),
                n = parseInt(e.interTime);
            parseInt(e.triggerTime);
            var g, m = parseInt(e.scroll), v = parseInt(e.vis), y = "false" != e.autoPlay && 0 != e.autoPlay,
                x = "false" != e.opp && 0 != e.opp, b = "false" != e.autoPage && 0 != e.autoPage,
                w = "false" != e.pnLoop && 0 != e.pnLoop, C = "false" != e.mouseOverStop && 0 != e.mouseOverStop,
                _ = "false" != e.defaultPlay && 0 != e.defaultPlay,
                k = "false" != e.returnDefault && 0 != e.returnDefault, S = 0, T = 0, A = 0, $ = 0, M = e.easing,
                E = null, P = null, L = null, I = e.titOnClassName, F = X.index(t.find("." + I)),
                D = p = -1 == F ? p : F, O = p, N = p, j = v <= h ? 0 != h % m ? h % m : m : 0,
                z = "leftMarquee" == s || "topMarquee" == s, R = function () {
                    ct.isFunction(e.startFun) && e.startFun(p, l, t, ct(e.titCell, t), c, u, r, o)
                }, B = function () {
                    ct.isFunction(e.endFun) && e.endFun(p, l, t, ct(e.titCell, t), c, u, r, o)
                }, H = function () {
                    X.removeClass(I), _ && X.eq(O).addClass(I)
                };
            if ("menu" == e.type) return _ && X.removeClass(I).eq(p).addClass(I), X.hover(function () {
                g = ct(this).find(e.targetCell);
                var t = X.index(ct(this));
                P = setTimeout(function () {
                    switch (p = t, X.removeClass(I).eq(p).addClass(I), R(), s) {
                        case"fade":
                            g.stop(!0, !0).animate({opacity: "show"}, f, M, B);
                            break;
                        case"slideDown":
                            g.stop(!0, !0).animate({height: "show"}, f, M, B)
                    }
                }, e.triggerTime)
            }, function () {
                switch (clearTimeout(P), s) {
                    case"fade":
                        g.animate({opacity: "hide"}, f, M);
                        break;
                    case"slideDown":
                        g.animate({height: "hide"}, f, M)
                }
            }), void (k && t.hover(function () {
                clearTimeout(L)
            }, function () {
                L = setTimeout(H, f)
            }));
            if (0 == l && (l = h), z && (l = 2), b) {
                if (v <= h) if ("leftLoop" == s || "topLoop" == s) l = 0 != h % m ? 1 + (0 ^ h / m) : h / m; else {
                    var W = h - v;
                    (l = 1 + parseInt(0 != W % m ? W / m + 1 : W / m)) <= 0 && (l = 1)
                } else l = 1;
                X.html("");
                var q = "";
                if (1 == e.autoPage || "true" == e.autoPage) for (var G = 0; G < l; G++) q += "<li>" + (G + 1) + "</li>"; else for (G = 0; G < l; G++) q += e.autoPage.replace("$", G + 1);
                X.html(q);
                var X = X.children()
            }
            if (v <= h) {
                c.children().each(function () {
                    ct(this).width() > A && (A = ct(this).width(), T = ct(this).outerWidth(!0)), ct(this).height() > $ && ($ = ct(this).height(), S = ct(this).outerHeight(!0))
                });
                var Y = c.children(), V = function () {
                    for (var t = 0; t < v; t++) Y.eq(t).clone().addClass("clone").appendTo(c);
                    for (t = 0; t < j; t++) Y.eq(h - t - 1).clone().addClass("clone").prependTo(c)
                };
                switch (s) {
                    case"fold":
                        c.css({position: "relative", width: T, height: S}).children().css({
                            position: "absolute",
                            width: A,
                            left: 0,
                            top: 0,
                            display: "none"
                        });
                        break;
                    case"top":
                        c.wrap('<div class="tempWrap" style="overflow:hidden; position:relative; height:' + v * S + 'px"></div>').css({
                            top: -p * m * S,
                            position: "relative",
                            padding: "0",
                            margin: "0"
                        }).children().css({height: $});
                        break;
                    case"left":
                        c.wrap('<div class="tempWrap" style="overflow:hidden; position:relative; width:' + v * T + 'px"></div>').css({
                            width: h * T,
                            left: -p * m * T,
                            position: "relative",
                            overflow: "hidden",
                            padding: "0",
                            margin: "0"
                        }).children().css({"float": "left", width: A});
                        break;
                    case"leftLoop":
                    case"leftMarquee":
                        V(), c.wrap('<div class="tempWrap" style="overflow:hidden; position:relative; width:' + v * T + 'px"></div>').css({
                            width: (h + v + j) * T,
                            position: "relative",
                            overflow: "hidden",
                            padding: "0",
                            margin: "0",
                            left: -(j + p * m) * T
                        }).children().css({"float": "left", width: A});
                        break;
                    case"topLoop":
                    case"topMarquee":
                        V(), c.wrap('<div class="tempWrap" style="overflow:hidden; position:relative; height:' + v * S + 'px"></div>').css({
                            height: (h + v + j) * S,
                            position: "relative",
                            padding: "0",
                            margin: "0",
                            top: -(j + p * m) * S
                        }).children().css({height: $})
                }
            }
            var U = function (t) {
                var e = t * m;
                return t == l ? e = h : -1 == t && 0 != h % m && (e = -h % m), e
            }, Q = function (i) {
                var t = function (t) {
                    for (var e = t; e < v + t; e++) i.eq(e).find("img[" + d + "]").each(function () {
                        var t = ct(this);
                        if (t.attr("src", t.attr(d)).removeAttr(d), c.find(".clone")[0]) for (var e = c.children(), i = 0; i < e.size(); i++) e.eq(i).find("img[" + d + "]").each(function () {
                            ct(this).attr(d) == t.attr("src") && ct(this).attr("src", ct(this).attr(d)).removeAttr(d)
                        })
                    })
                };
                switch (s) {
                    case"fade":
                    case"fold":
                    case"top":
                    case"left":
                    case"slideDown":
                        t(p * m);
                        break;
                    case"leftLoop":
                    case"topLoop":
                        t(j + U(N));
                        break;
                    case"leftMarquee":
                    case"topMarquee":
                        var e = "leftMarquee" == s ? c.css("left").replace("px", "") : c.css("top").replace("px", ""),
                            n = "leftMarquee" == s ? T : S, r = j;
                        if (0 != e % n) {
                            var o = Math.abs(0 ^ e / n);
                            r = 1 == p ? j + o : j + o - 1
                        }
                        t(r)
                }
            }, K = function (t) {
                if (!_ || D != p || t || z) {
                    if (z ? 1 <= p ? p = 1 : p <= 0 && (p = 0) : l <= (N = p) ? p = 0 : p < 0 && (p = l - 1), R(), null != d && Q(c.children()), u[0] && (g = u.eq(p), null != d && Q(u), "slideDown" == s ? (u.not(g).stop(!0, !0).slideUp(f), g.slideDown(f, M, function () {
                        c[0] || B()
                    })) : (u.not(g).stop(!0, !0).hide(), g.animate({opacity: "show"}, f, function () {
                        c[0] || B()
                    }))), v <= h) switch (s) {
                        case"fade":
                            c.children().stop(!0, !0).eq(p).animate({opacity: "show"}, f, M, function () {
                                B()
                            }).siblings().hide();
                            break;
                        case"fold":
                            c.children().stop(!0, !0).eq(p).animate({opacity: "show"}, f, M, function () {
                                B()
                            }).siblings().animate({opacity: "hide"}, f, M);
                            break;
                        case"top":
                            c.stop(!0, !1).animate({top: -p * m * S}, f, M, function () {
                                B()
                            });
                            break;
                        case"left":
                            c.stop(!0, !1).animate({left: -p * m * T}, f, M, function () {
                                B()
                            });
                            break;
                        case"leftLoop":
                            var e = N;
                            c.stop(!0, !0).animate({left: -(U(N) + j) * T}, f, M, function () {
                                e <= -1 ? c.css("left", -(j + (l - 1) * m) * T) : l <= e && c.css("left", -j * T), B()
                            });
                            break;
                        case"topLoop":
                            e = N;
                            c.stop(!0, !0).animate({top: -(U(N) + j) * S}, f, M, function () {
                                e <= -1 ? c.css("top", -(j + (l - 1) * m) * S) : l <= e && c.css("top", -j * S), B()
                            });
                            break;
                        case"leftMarquee":
                            var i = c.css("left").replace("px", "");
                            0 == p ? c.animate({left: ++i}, 0, function () {
                                0 <= c.css("left").replace("px", "") && c.css("left", -h * T)
                            }) : c.animate({left: --i}, 0, function () {
                                c.css("left").replace("px", "") <= -(h + j) * T && c.css("left", -j * T)
                            });
                            break;
                        case"topMarquee":
                            var n = c.css("top").replace("px", "");
                            0 == p ? c.animate({top: ++n}, 0, function () {
                                0 <= c.css("top").replace("px", "") && c.css("top", -h * S)
                            }) : c.animate({top: --n}, 0, function () {
                                c.css("top").replace("px", "") <= -(h + j) * S && c.css("top", -j * S)
                            })
                    }
                    X.removeClass(I).eq(p).addClass(I), D = p, w || (o.removeClass("nextStop"), r.removeClass("prevStop"), 0 == p && r.addClass("prevStop"), p == l - 1 && o.addClass("nextStop")), a.html("<span>" + (p + 1) + "</span>/" + l)
                }
            };
            _ && K(!0), k && t.hover(function () {
                clearTimeout(L)
            }, function () {
                L = setTimeout(function () {
                    p = O, _ ? K() : "slideDown" == s ? g.slideUp(f, H) : g.animate({opacity: "hide"}, f, H), D = p
                }, 300)
            });
            var Z = function (t) {
                E = setInterval(function () {
                    x ? p-- : p++, K()
                }, t || n)
            }, J = function (t) {
                E = setInterval(K, t || n)
            }, tt = function () {
                C || (clearInterval(E), Z())
            }, et = function () {
                (w || p != l - 1) && (p++, K(), z || tt())
            }, it = function () {
                (w || 0 != p) && (p--, K(), z || tt())
            }, nt = function () {
                clearInterval(E), z ? J() : Z(), i.removeClass("pauseState")
            }, rt = function () {
                clearInterval(E), i.addClass("pauseState")
            };
            if (y ? z ? (x ? p-- : p++, J(), C && c.hover(rt, nt)) : (Z(), C && t.hover(rt, nt)) : (z && (x ? p-- : p++), i.addClass("pauseState")), i.click(function () {
                i.hasClass("pauseState") ? nt() : rt()
            }), "mouseover" == e.trigger ? X.hover(function () {
                var t = X.index(this);
                P = setTimeout(function () {
                    p = t, K(), tt()
                }, e.triggerTime)
            }, function () {
                clearTimeout(P)
            }) : X.click(function () {
                p = X.index(this), K(), tt()
            }), z) {
                if (o.mousedown(et), r.mousedown(it), w) {
                    var ot, st = function () {
                        ot = setTimeout(function () {
                            clearInterval(E), J(0 ^ n / 10)
                        }, 150)
                    }, at = function () {
                        clearTimeout(ot), clearInterval(E), J()
                    };
                    o.mousedown(st), o.mouseup(at), r.mousedown(st), r.mouseup(at)
                }
                "mouseover" == e.trigger && (o.hover(et, function () {
                }), r.hover(it, function () {
                }))
            } else o.click(et), r.click(it)
        })
    }
}(jQuery), jQuery.easing.jswing = jQuery.easing.swing, jQuery.extend(jQuery.easing, {
    def: "easeOutQuad", swing: function (t, e, i, n, r) {
        return jQuery.easing[jQuery.easing.def](t, e, i, n, r)
    }, easeInQuad: function (t, e, i, n, r) {
        return n * (e /= r) * e + i
    }, easeOutQuad: function (t, e, i, n, r) {
        return -n * (e /= r) * (e - 2) + i
    }, easeInOutQuad: function (t, e, i, n, r) {
        return (e /= r / 2) < 1 ? n / 2 * e * e + i : -n / 2 * (--e * (e - 2) - 1) + i
    }, easeInCubic: function (t, e, i, n, r) {
        return n * (e /= r) * e * e + i
    }, easeOutCubic: function (t, e, i, n, r) {
        return n * ((e = e / r - 1) * e * e + 1) + i
    }, easeInOutCubic: function (t, e, i, n, r) {
        return (e /= r / 2) < 1 ? n / 2 * e * e * e + i : n / 2 * ((e -= 2) * e * e + 2) + i
    }, easeInQuart: function (t, e, i, n, r) {
        return n * (e /= r) * e * e * e + i
    }, easeOutQuart: function (t, e, i, n, r) {
        return -n * ((e = e / r - 1) * e * e * e - 1) + i
    }, easeInOutQuart: function (t, e, i, n, r) {
        return (e /= r / 2) < 1 ? n / 2 * e * e * e * e + i : -n / 2 * ((e -= 2) * e * e * e - 2) + i
    }, easeInQuint: function (t, e, i, n, r) {
        return n * (e /= r) * e * e * e * e + i
    }, easeOutQuint: function (t, e, i, n, r) {
        return n * ((e = e / r - 1) * e * e * e * e + 1) + i
    }, easeInOutQuint: function (t, e, i, n, r) {
        return (e /= r / 2) < 1 ? n / 2 * e * e * e * e * e + i : n / 2 * ((e -= 2) * e * e * e * e + 2) + i
    }, easeInSine: function (t, e, i, n, r) {
        return -n * Math.cos(e / r * (Math.PI / 2)) + n + i
    }, easeOutSine: function (t, e, i, n, r) {
        return n * Math.sin(e / r * (Math.PI / 2)) + i
    }, easeInOutSine: function (t, e, i, n, r) {
        return -n / 2 * (Math.cos(Math.PI * e / r) - 1) + i
    }, easeInExpo: function (t, e, i, n, r) {
        return 0 == e ? i : n * Math.pow(2, 10 * (e / r - 1)) + i
    }, easeOutExpo: function (t, e, i, n, r) {
        return e == r ? i + n : n * (1 - Math.pow(2, -10 * e / r)) + i
    }, easeInOutExpo: function (t, e, i, n, r) {
        return 0 == e ? i : e == r ? i + n : (e /= r / 2) < 1 ? n / 2 * Math.pow(2, 10 * (e - 1)) + i : n / 2 * (2 - Math.pow(2, -10 * --e)) + i
    }, easeInCirc: function (t, e, i, n, r) {
        return -n * (Math.sqrt(1 - (e /= r) * e) - 1) + i
    }, easeOutCirc: function (t, e, i, n, r) {
        return n * Math.sqrt(1 - (e = e / r - 1) * e) + i
    }, easeInOutCirc: function (t, e, i, n, r) {
        return (e /= r / 2) < 1 ? -n / 2 * (Math.sqrt(1 - e * e) - 1) + i : n / 2 * (Math.sqrt(1 - (e -= 2) * e) + 1) + i
    }, easeInElastic: function (t, e, i, n, r) {
        var o = 1.70158, s = 0, a = n;
        if (0 == e) return i;
        if (1 == (e /= r)) return i + n;
        if (s || (s = .3 * r), a < Math.abs(n)) {
            a = n;
            o = s / 4
        } else o = s / (2 * Math.PI) * Math.asin(n / a);
        return -a * Math.pow(2, 10 * (e -= 1)) * Math.sin(2 * (e * r - o) * Math.PI / s) + i
    }, easeOutElastic: function (t, e, i, n, r) {
        var o = 1.70158, s = 0, a = n;
        if (0 == e) return i;
        if (1 == (e /= r)) return i + n;
        if (s || (s = .3 * r), a < Math.abs(n)) {
            a = n;
            o = s / 4
        } else o = s / (2 * Math.PI) * Math.asin(n / a);
        return a * Math.pow(2, -10 * e) * Math.sin(2 * (e * r - o) * Math.PI / s) + n + i
    }, easeInOutElastic: function (t, e, i, n, r) {
        var o = 1.70158, s = 0, a = n;
        if (0 == e) return i;
        if (2 == (e /= r / 2)) return i + n;
        if (s || (s = .3 * r * 1.5), a < Math.abs(n)) {
            a = n;
            o = s / 4
        } else o = s / (2 * Math.PI) * Math.asin(n / a);
        return e < 1 ? -.5 * a * Math.pow(2, 10 * (e -= 1)) * Math.sin(2 * (e * r - o) * Math.PI / s) + i : .5 * a * Math.pow(2, -10 * (e -= 1)) * Math.sin(2 * (e * r - o) * Math.PI / s) + n + i
    }, easeInBack: function (t, e, i, n, r, o) {
        return null == o && (o = 1.70158), n * (e /= r) * e * ((o + 1) * e - o) + i
    }, easeOutBack: function (t, e, i, n, r, o) {
        return null == o && (o = 1.70158), n * ((e = e / r - 1) * e * ((o + 1) * e + o) + 1) + i
    }, easeInOutBack: function (t, e, i, n, r, o) {
        return null == o && (o = 1.70158), (e /= r / 2) < 1 ? n / 2 * e * e * ((1 + (o *= 1.525)) * e - o) + i : n / 2 * ((e -= 2) * e * ((1 + (o *= 1.525)) * e + o) + 2) + i
    }, easeInBounce: function (t, e, i, n, r) {
        return n - jQuery.easing.easeOutBounce(t, r - e, 0, n, r) + i
    }, easeOutBounce: function (t, e, i, n, r) {
        return (e /= r) < 1 / 2.75 ? 7.5625 * n * e * e + i : e < 2 / 2.75 ? n * (7.5625 * (e -= 1.5 / 2.75) * e + .75) + i : e < 2.5 / 2.75 ? n * (7.5625 * (e -= 2.25 / 2.75) * e + .9375) + i : n * (7.5625 * (e -= 2.625 / 2.75) * e + .984375) + i
    }, easeInOutBounce: function (t, e, i, n, r) {
        return e < r / 2 ? .5 * jQuery.easing.easeInBounce(t, 2 * e, 0, n, r) + i : .5 * jQuery.easing.easeOutBounce(t, 2 * e - r, 0, n, r) + .5 * n + i
    }
}), function (t) {
    t.fn.validationEngineLanguage = function () {
    }, t.validationEngineLanguage = {
        newLang: function () {
            t.validationEngineLanguage.allRules = {
                required: {
                    regex: "none",
                    alertText: "* \u4e0d\u80fd\u4e3a\u7a7a",
                    alertTextCheckboxMultiple: "* \u8bf7\u9009\u62e9\u4e00\u4e2a\u9879\u76ee",
                    alertTextCheckboxe: "* \u60a8\u5fc5\u987b\u94a9\u9009\u6b64\u680f",
                    alertTextDateRange: "* \u65e5\u671f\u8303\u56f4\u4e0d\u53ef\u7a7a\u767d"
                },
                requiredInFunction: {
                    func: function (t) {
                        return "test" == t.val()
                    }, alertText: "* Field must equal test"
                },
                dateRange: {regex: "none", alertText: "* \u65e0\u6548\u7684 ", alertText2: " \u65e5\u671f\u8303\u56f4"},
                dateTimeRange: {
                    regex: "none",
                    alertText: "* \u65e0\u6548\u7684 ",
                    alertText2: " \u65f6\u95f4\u8303\u56f4"
                },
                minSize: {regex: "none", alertText: "* \u6700\u5c11 ", alertText2: " \u4e2a\u5b57\u7b26"},
                maxSize: {regex: "none", alertText: "* \u6700\u591a ", alertText2: " \u4e2a\u5b57\u7b26"},
                groupRequired: {
                    regex: "none",
                    alertText: "* \u4f60\u5fc5\u9700\u9009\u586b\u5176\u4e2d\u4e00\u4e2a\u680f\u4f4d"
                },
                min: {regex: "none", alertText: "* \u6700\u5c0f\u503c\u4e3a "},
                max: {regex: "none", alertText: "* \u6700\u5927\u503c\u4e3a "},
                past: {regex: "none", alertText: "* \u65e5\u671f\u5fc5\u9700\u65e9\u4e8e "},
                future: {regex: "none", alertText: "* \u65e5\u671f\u5fc5\u9700\u665a\u4e8e "},
                maxCheckbox: {
                    regex: "none",
                    alertText: "* \u6700\u591a\u9009\u53d6 ",
                    alertText2: " \u4e2a\u9879\u76ee"
                },
                minCheckbox: {regex: "none", alertText: "* \u8bf7\u9009\u62e9 ", alertText2: " \u4e2a\u9879\u76ee"},
                equals: {
                    regex: "none",
                    alertText: "* \u8bf7\u8f93\u5165\u4e0e\u4e0a\u9762\u76f8\u540c\u7684\u5bc6\u7801"
                },
                creditCard: {regex: "none", alertText: "* \u65e0\u6548\u7684\u4fe1\u7528\u5361\u53f7\u7801"},
                phone: {
                    regex: /^([\+][0-9]{1,3}([ \.\-])?)?([\(][0-9]{1,6}[\)])?([0-9 \.\-]{1,32})(([A-Za-z \:]{1,11})?[0-9]{1,4}?)$/,
                    alertText: "* \u65e0\u6548\u7684\u7535\u8bdd\u53f7\u7801"
                },
                mobile: {regex: /^1[0-9]\d{9}$/, alertText: "* \u65e0\u6548\u7684\u624b\u673a\u53f7\u7801"},
                email: {
                    regex: /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i,
                    alertText: "* \u90ae\u4ef6\u5730\u5740\u65e0\u6548"
                },
                integer: {regex: /^[\-\+]?\d+$/, alertText: "* \u4e0d\u662f\u6709\u6548\u7684\u6574\u6570"},
                number: {
                    regex: /^[\-\+]?((([0-9]{1,3})([,][0-9]{3})*)|([0-9]+))?([\.]([0-9]+))?$/,
                    alertText: "* \u65e0\u6548\u7684\u6570\u5b57"
                },
                date: {
                    regex: /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/,
                    alertText: "* \u65e0\u6548\u7684\u65e5\u671f\uff0c\u683c\u5f0f\u5fc5\u9700\u4e3a YYYY-MM-DD"
                },
                ipv4: {
                    regex: /^((([01]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))[.]){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))$/,
                    alertText: "* \u65e0\u6548\u7684 IP \u5730\u5740"
                },
                url: {
                    regex: /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i,
                    alertText: "* Invalid URL"
                },
                onlyNumberSp: {regex: /^[0-9\ ]+$/, alertText: "* \u53ea\u80fd\u586b\u6570\u5b57"},
                onlyLetterSp: {
                    regex: /^[a-zA-Z\ \']+$/,
                    alertText: "* \u53ea\u63a5\u53d7\u82f1\u6587\u5b57\u6bcd\u5927\u5c0f\u5199"
                },
                onlyLetterNumber: {regex: /^[0-9a-zA-Z]+$/, alertText: "* \u4e0d\u63a5\u53d7\u7279\u6b8a\u5b57\u7b26"},
                password_complexity: {
                    regex: /(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[^a-zA-Z0-9]).{8}/,
                    alertText: "\u957f\u5ea6\u81f3\u5c118\u4e2a\u5b57\u7b26\uff0c\u5fc5\u987b\u5305\u542b\u5927\u5c0f\u5199\u5b57\u6bcd\u3001\u6570\u5b57\u548c\u7b26\u53f7\u5728\u5185\u7684\u7ec4\u5408"
                },
                ajaxUserCall: {
                    url: "ajaxValidateFieldUser",
                    extraData: "name=eric",
                    alertText: "* \u6b64\u540d\u79f0\u5df2\u88ab\u5176\u4ed6\u4eba\u4f7f\u7528",
                    alertTextLoad: "* \u6b63\u5728\u786e\u8ba4\u540d\u79f0\u662f\u5426\u6709\u5176\u4ed6\u4eba\u4f7f\u7528\uff0c\u8bf7\u7a0d\u7b49\u3002"
                },
                ajaxUserCallPhp: {
                    url: "phpajax/ajaxValidateFieldUser.php",
                    extraData: "name=eric",
                    alertTextOk: "* \u6b64\u5e10\u53f7\u540d\u79f0\u53ef\u4ee5\u4f7f\u7528",
                    alertText: "* \u6b64\u540d\u79f0\u5df2\u88ab\u5176\u4ed6\u4eba\u4f7f\u7528",
                    alertTextLoad: "* \u6b63\u5728\u786e\u8ba4\u5e10\u53f7\u540d\u79f0\u662f\u5426\u6709\u5176\u4ed6\u4eba\u4f7f\u7528\uff0c\u8bf7\u7a0d\u7b49\u3002"
                },
                ajaxNameCall: {
                    url: "ajaxValidateFieldName",
                    alertText: "* \u6b64\u540d\u79f0\u53ef\u4ee5\u4f7f\u7528",
                    alertTextOk: "* \u6b64\u540d\u79f0\u5df2\u88ab\u5176\u4ed6\u4eba\u4f7f\u7528",
                    alertTextLoad: "* \u6b63\u5728\u786e\u8ba4\u540d\u79f0\u662f\u5426\u6709\u5176\u4ed6\u4eba\u4f7f\u7528\uff0c\u8bf7\u7a0d\u7b49\u3002"
                },
                ajaxNameCallPhp: {
                    url: "phpajax/ajaxValidateFieldName.php",
                    alertText: "* \u6b64\u540d\u79f0\u5df2\u88ab\u5176\u4ed6\u4eba\u4f7f\u7528",
                    alertTextLoad: "* \u6b63\u5728\u786e\u8ba4\u540d\u79f0\u662f\u5426\u6709\u5176\u4ed6\u4eba\u4f7f\u7528\uff0c\u8bf7\u7a0d\u7b49\u3002"
                },
                validate2fields: {alertText: "* \u8bf7\u8f93\u5165 HELLO"},
                dateFormat: {
                    regex: /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(?:(?:0?[1-9]|1[0-2])(\/|-)(?:0?[1-9]|1\d|2[0-8]))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(0?2(\/|-)29)(\/|-)(?:(?:0[48]00|[13579][26]00|[2468][048]00)|(?:\d\d)?(?:0[48]|[2468][048]|[13579][26]))$/,
                    alertText: "* \u65e0\u6548\u7684\u65e5\u671f\u683c\u5f0f"
                },
                dateTimeFormat: {
                    regex: /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1}$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^((1[012]|0?[1-9]){1}\/(0?[1-9]|[12][0-9]|3[01]){1}\/\d{2,4}\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1})$/,
                    alertText: "* \u65e0\u6548\u7684\u65e5\u671f\u6216\u65f6\u95f4\u683c\u5f0f",
                    alertText2: "\u53ef\u63a5\u53d7\u7684\u683c\u5f0f\uff1a ",
                    alertText3: "mm/dd/yyyy hh:mm:ss AM|PM \u6216 ",
                    alertText4: "yyyy-mm-dd hh:mm:ss AM|PM"
                }
            }
        }
    }, t.validationEngineLanguage.newLang()
}(jQuery), function (k) {
    "use strict";
    var S = {
        init: function (t) {
            var e = this;
            return e.data("jqv") && null != e.data("jqv") || (t = S._saveOptions(e, t), k(document).on("click", ".formError", function () {
                k(this).fadeOut(150, function () {
                    k(this).parent(".formErrorOuter").remove(), k(this).remove()
                })
            })), this
        },
        attach: function (t) {
            var e, i = this;
            return (e = t ? S._saveOptions(i, t) : i.data("jqv")).validateAttribute = i.find("[data-validation-engine*=validate]").length ? "data-validation-engine" : "class", e.binded && (i.on(e.validationEventTrigger, "[" + e.validateAttribute + "*=validate]:not([type=checkbox]):not([type=radio]):not(.datepicker)", S._onFieldEvent), i.on("click", "[" + e.validateAttribute + "*=validate][type=checkbox],[" + e.validateAttribute + "*=validate][type=radio]", S._onFieldEvent), i.on(e.validationEventTrigger, "[" + e.validateAttribute + "*=validate][class*=datepicker]", {delay: 300}, S._onFieldEvent)), e.autoPositionUpdate && k(window).bind("resize", {
                noAnimation: !0,
                formElem: i
            }, S.updatePromptsPosition), i.on("click", "a[data-validation-engine-skip], a[class*='validate-skip'], button[data-validation-engine-skip], button[class*='validate-skip'], input[data-validation-engine-skip], input[class*='validate-skip']", S._submitButtonClick), i.removeData("jqv_submitButton"), i.on("submit", S._onSubmitEvent), this
        },
        detach: function () {
            var t = this, e = t.data("jqv");
            return t.off(e.validationEventTrigger, "[" + e.validateAttribute + "*=validate]:not([type=checkbox]):not([type=radio]):not(.datepicker)", S._onFieldEvent), t.off("click", "[" + e.validateAttribute + "*=validate][type=checkbox],[" + e.validateAttribute + "*=validate][type=radio]", S._onFieldEvent), t.off(e.validationEventTrigger, "[" + e.validateAttribute + "*=validate][class*=datepicker]", S._onFieldEvent), t.off("submit", S._onSubmitEvent), t.removeData("jqv"), t.off("click", "a[data-validation-engine-skip], a[class*='validate-skip'], button[data-validation-engine-skip], button[class*='validate-skip'], input[data-validation-engine-skip], input[class*='validate-skip']", S._submitButtonClick), t.removeData("jqv_submitButton"), e.autoPositionUpdate && k(window).off("resize", S.updatePromptsPosition), this
        },
        validate: function () {
            var t = k(this), e = null;
            if (t.is("form") || t.hasClass("validationEngineContainer")) {
                if (t.hasClass("validating")) return !1;
                t.addClass("validating");
                var i = t.data("jqv");
                e = S._validateFields(this);
                setTimeout(function () {
                    t.removeClass("validating")
                }, 100), e && i.onSuccess ? i.onSuccess() : !e && i.onFailure && i.onFailure()
            } else if (t.is("form") || t.hasClass("validationEngineContainer")) t.removeClass("validating"); else {
                var n = t.closest("form, .validationEngineContainer");
                i = n.data("jqv") ? n.data("jqv") : k.validationEngine.defaults, e = S._validateField(t, i)
            }
            return i.onValidationComplete ? !!i.onValidationComplete(n, e) : e
        },
        updatePromptsPosition: function (t) {
            if (t && this == window) var n = t.data.formElem,
                r = t.data.noAnimation; else n = k(this.closest("form, .validationEngineContainer"));
            var o = n.data("jqv");
            return o || (o = S._saveOptions(n, o)), n.find("[" + o.validateAttribute + "*=validate]").not(":disabled").each(function () {
                var t = k(this);
                o.prettySelect && t.is(":hidden") && (t = n.find("#" + o.usePrefix + t.attr("id") + o.useSuffix));
                var e = S._getPrompt(t), i = k(e).find(".formErrorContent").html();
                e && S._updatePrompt(t, k(e), i, undefined, !1, o, r)
            }), this
        },
        showPrompt: function (t, e, i, n) {
            var r = this.closest("form, .validationEngineContainer").data("jqv");
            return r || (r = S._saveOptions(this, r)), i && (r.promptPosition = i), r.showArrow = 1 == n, S._showPrompt(this, t, e, !1, r), this
        },
        hide: function () {
            var t = k(this).closest("form, .validationEngineContainer"), e = t.data("jqv");
            e || (e = S._saveOptions(t, e));
            var i, n = e && e.fadeDuration ? e.fadeDuration : .3;
            return i = k(this).is("form") || k(this).hasClass("validationEngineContainer") ? "parentForm" + S._getClassName(k(this).attr("id")) : S._getClassName(k(this).attr("id")) + "formError", k("." + i).fadeTo(n, .3, function () {
                k(this).parent(".formErrorOuter").remove(), k(this).remove()
            }), this
        },
        hideAll: function () {
            var t = this.data("jqv"), e = t ? t.fadeDuration : 300;
            return k(".formError").fadeTo(e, 300, function () {
                k(this).parent(".formErrorOuter").remove(), k(this).remove()
            }), this
        },
        _onFieldEvent: function (t) {
            var e = k(this), i = e.closest("form, .validationEngineContainer"), n = i.data("jqv");
            n || (n = S._saveOptions(i, n)), n.eventTrigger = "field", window.setTimeout(function () {
                S._validateField(e, n)
            }, t.data ? t.data.delay : 0)
        },
        _onSubmitEvent: function () {
            var t = k(this), e = t.data("jqv");
            if (t.data("jqv_submitButton")) {
                var i = k("#" + t.data("jqv_submitButton"));
                if (i && 0 < i.length && (i.hasClass("validate-skip") || "true" == i.attr("data-validation-engine-skip"))) return !0
            }
            e.eventTrigger = "submit";
            var n = S._validateFields(t);
            return n && e.ajaxFormValidation ? (S._validateFormWithAjax(t, e), !1) : e.onValidationComplete ? !!e.onValidationComplete(t, n) : n
        },
        _checkAjaxStatus: function (t) {
            var i = !0;
            return k.each(t.ajaxValidCache, function (t, e) {
                if (!e) return i = !1
            }), i
        },
        _checkAjaxFieldStatus: function (t, e) {
            return 1 == e.ajaxValidCache[t]
        },
        _validateFields: function (i) {
            var n = i.data("jqv"), r = !1;
            i.trigger("jqv.form.validating");
            var o = null;
            if (i.find("[" + n.validateAttribute + "*=validate]").not(":disabled").each(function () {
                var t = k(this), e = [];
                if (k.inArray(t.attr("name"), e) < 0) {
                    if ((r |= S._validateField(t, n)) && null == o && (t.is(":hidden") && n.prettySelect ? o = t = i.find("#" + n.usePrefix + S._jqSelector(t.attr("id")) + n.useSuffix) : (t.data("jqv-prompt-at") instanceof jQuery ? t = t.data("jqv-prompt-at") : t.data("jqv-prompt-at") && (t = k(t.data("jqv-prompt-at"))), o = t)), n.doNotShowAllErrosOnSubmit) return !1;
                    if (e.push(t.attr("name")), 1 == n.showOneMessage && r) return !1
                }
            }), i.trigger("jqv.form.result", [r]), r) {
                if (n.scroll) {
                    var t = o.offset().top, e = o.offset().left, s = n.promptPosition;
                    if ("string" == typeof s && -1 != s.indexOf(":") && (s = s.substring(0, s.indexOf(":"))), "bottomRight" != s && "bottomLeft" != s) {
                        var a = S._getPrompt(o);
                        a && (t = a.offset().top)
                    }
                    if (n.scrollOffset && (t -= n.scrollOffset), n.isOverflown) {
                        var l = k(n.overflownDIV);
                        if (!l.length) return !1;
                        t += l.scrollTop() + -parseInt(l.offset().top) - 5, k(n.overflownDIV + ":not(:animated)").animate({scrollTop: t}, 1100, function () {
                            n.focusFirstField && o.focus()
                        })
                    } else k("html, body").animate({scrollTop: t}, 1100, function () {
                        n.focusFirstField && o.focus()
                    }), k("html, body").animate({scrollLeft: e}, 1100)
                } else n.focusFirstField && o.focus();
                return !1
            }
            return !0
        },
        _validateFormWithAjax: function (l, c) {
            var t = l.serialize(), e = c.ajaxFormValidationMethod ? c.ajaxFormValidationMethod : "GET",
                i = c.ajaxFormValidationURL ? c.ajaxFormValidationURL : l.attr("action"),
                h = c.dataType ? c.dataType : "json";
            k.ajax({
                type: e,
                url: i,
                cache: !1,
                dataType: h,
                data: t,
                form: l,
                methods: S,
                options: c,
                beforeSend: function () {
                    return c.onBeforeAjaxFormValidation(l, c)
                },
                error: function (t, e) {
                    c.onFailure ? c.onFailure(t, e) : S._ajaxError(t, e)
                },
                success: function (t) {
                    if ("json" == h && !0 !== t) {
                        for (var e = !1, i = 0; i < t.length; i++) {
                            var n = t[i], r = n[0], o = k(k("#" + r)[0]);
                            if (1 == o.length) {
                                var s = n[2];
                                if (1 == n[1]) if ("" != s && s) {
                                    if (c.allrules[s]) (a = c.allrules[s].alertTextOk) && (s = a);
                                    c.showPrompts && S._showPrompt(o, s, "pass", !1, c, !0)
                                } else S._closePrompt(o); else {
                                    var a;
                                    if (e |= !0, c.allrules[s]) (a = c.allrules[s].alertText) && (s = a);
                                    c.showPrompts && S._showPrompt(o, s, "", !1, c, !0)
                                }
                            }
                        }
                        c.onAjaxFormComplete(!e, l, t, c)
                    } else c.onAjaxFormComplete(!0, l, t, c)
                }
            })
        },
        _validateField: function (t, e, i) {
            if (t.attr("id") || (t.attr("id", "form-validation-field-" + k.validationEngine.fieldIdCounter), ++k.validationEngine.fieldIdCounter), !e.validateNonVisibleFields && (t.is(":hidden") && !e.prettySelect || t.parent().is(":hidden"))) return !1;
            var n = t.attr(e.validateAttribute), r = /validate\[(.*)\]/.exec(n);
            if (!r) return !1;
            var o = r[1], s = o.split(/\[|,|\]/), a = !1, l = t.attr("name"), c = "", h = "", d = !1, u = !1;
            e.isError = !1, e.showArrow = !0, 0 < e.maxErrorsPerField && (u = !0);
            for (var p = k(t.closest("form, .validationEngineContainer")), f = 0; f < s.length; f++) s[f] = s[f].replace(" ", ""), "" === s[f] && delete s[f];
            f = 0;
            for (var g = 0; f < s.length; f++) {
                if (u && g >= e.maxErrorsPerField) {
                    if (!d) {
                        var m = k.inArray("required", s);
                        d = -1 != m && f <= m
                    }
                    break
                }
                var v = undefined;
                switch (s[f]) {
                    case"required":
                        d = !0, v = S._getErrorMessage(p, t, s[f], s, f, e, S._required);
                        break;
                    case"custom":
                        v = S._getErrorMessage(p, t, s[f], s, f, e, S._custom);
                        break;
                    case"groupRequired":
                        var y = "[" + e.validateAttribute + "*=" + s[f + 1] + "]", x = p.find(y).eq(0);
                        x[0] != t[0] && (S._validateField(x, e, i), e.showArrow = !0), (v = S._getErrorMessage(p, t, s[f], s, f, e, S._groupRequired)) && (d = !0), e.showArrow = !1;
                        break;
                    case"ajax":
                        (v = S._ajax(t, s, f, e)) && (h = "load");
                        break;
                    case"minSize":
                        v = S._getErrorMessage(p, t, s[f], s, f, e, S._minSize);
                        break;
                    case"maxSize":
                        v = S._getErrorMessage(p, t, s[f], s, f, e, S._maxSize);
                        break;
                    case"min":
                        v = S._getErrorMessage(p, t, s[f], s, f, e, S._min);
                        break;
                    case"max":
                        v = S._getErrorMessage(p, t, s[f], s, f, e, S._max);
                        break;
                    case"past":
                        v = S._getErrorMessage(p, t, s[f], s, f, e, S._past);
                        break;
                    case"future":
                        v = S._getErrorMessage(p, t, s[f], s, f, e, S._future);
                        break;
                    case"dateRange":
                        y = "[" + e.validateAttribute + "*=" + s[f + 1] + "]";
                        e.firstOfGroup = p.find(y).eq(0), e.secondOfGroup = p.find(y).eq(1), (e.firstOfGroup[0].value || e.secondOfGroup[0].value) && (v = S._getErrorMessage(p, t, s[f], s, f, e, S._dateRange)), v && (d = !0), e.showArrow = !1;
                        break;
                    case"dateTimeRange":
                        y = "[" + e.validateAttribute + "*=" + s[f + 1] + "]";
                        e.firstOfGroup = p.find(y).eq(0), e.secondOfGroup = p.find(y).eq(1), (e.firstOfGroup[0].value || e.secondOfGroup[0].value) && (v = S._getErrorMessage(p, t, s[f], s, f, e, S._dateTimeRange)), v && (d = !0), e.showArrow = !1;
                        break;
                    case"maxCheckbox":
                        t = k(p.find("input[name='" + l + "']")), v = S._getErrorMessage(p, t, s[f], s, f, e, S._maxCheckbox);
                        break;
                    case"minCheckbox":
                        t = k(p.find("input[name='" + l + "']")), v = S._getErrorMessage(p, t, s[f], s, f, e, S._minCheckbox);
                        break;
                    case"equals":
                        v = S._getErrorMessage(p, t, s[f], s, f, e, S._equals);
                        break;
                    case"funcCall":
                        v = S._getErrorMessage(p, t, s[f], s, f, e, S._funcCall);
                        break;
                    case"creditCard":
                        v = S._getErrorMessage(p, t, s[f], s, f, e, S._creditCard);
                        break;
                    case"condRequired":
                        (v = S._getErrorMessage(p, t, s[f], s, f, e, S._condRequired)) !== undefined && (d = !0);
                        break;
                    case"funcCallRequired":
                        (v = S._getErrorMessage(p, t, s[f], s, f, e, S._funcCallRequired)) !== undefined && (d = !0)
                }
                var b = !1;
                if ("object" == typeof v) switch (v.status) {
                    case"_break":
                        b = !0;
                        break;
                    case"_error":
                        v = v.message;
                        break;
                    case"_error_no_prompt":
                        return !0
                }
                if (0 == f && 0 == o.indexOf("funcCallRequired") && v !== undefined && (c += v + "<br/>", g++, b = e.isError = !0), b) break;
                "string" == typeof v && (c += v + "<br/>", e.isError = !0, g++)
            }
            !d && !t.val() && t.val().length < 1 && k.inArray("equals", s) < 0 && (e.isError = !1);
            var w = t.prop("type"), C = t.data("promptPosition") || e.promptPosition;
            ("radio" == w || "checkbox" == w) && 1 < p.find("input[name='" + l + "']").size() && (t = k("inline" === C ? p.find("input[name='" + l + "'][type!=hidden]:last") : p.find("input[name='" + l + "'][type!=hidden]:first")), e.showArrow = e.showArrowOnRadioAndCheckbox), t.is(":hidden") && e.prettySelect && (t = p.find("#" + e.usePrefix + S._jqSelector(t.attr("id")) + e.useSuffix)), e.isError && e.showPrompts ? S._showPrompt(t, c, h, !1, e) : a || S._closePrompt(t), a || t.trigger("jqv.field.result", [t, e.isError, c]);
            var _ = k.inArray(t[0], e.InvalidFields);
            return -1 == _ ? e.isError && e.InvalidFields.push(t[0]) : e.isError || e.InvalidFields.splice(_, 1), S._handleStatusCssClasses(t, e), e.isError && e.onFieldFailure && e.onFieldFailure(t), !e.isError && e.onFieldSuccess && e.onFieldSuccess(t), e.isError
        },
        _handleStatusCssClasses: function (t, e) {
            e.addSuccessCssClassToField && t.removeClass(e.addSuccessCssClassToField), e.addFailureCssClassToField && t.removeClass(e.addFailureCssClassToField), e.addSuccessCssClassToField && !e.isError && t.addClass(e.addSuccessCssClassToField), e.addFailureCssClassToField && e.isError && t.addClass(e.addFailureCssClassToField)
        },
        _getErrorMessage: function (t, e, i, n, r, o, s) {
            var a = jQuery.inArray(i, n);
            "custom" !== i && "funcCall" !== i && "funcCallRequired" !== i || (i = i + "[" + n[a + 1] + "]", delete n[a]);
            var l, c = i,
                h = (e.attr("data-validation-engine") ? e.attr("data-validation-engine") : e.attr("class")).split(" ");
            if ((l = "future" == i || "past" == i || "maxCheckbox" == i || "minCheckbox" == i ? s(t, e, n, r, o) : s(e, n, r, o)) != undefined) {
                var d = S._getCustomErrorMessage(k(e), h, c, o);
                d && (l = d)
            }
            return l
        },
        _getCustomErrorMessage: function (t, e, i, n) {
            var r = !1, o = /^custom\[.*\]$/.test(i) ? S._validityProp.custom : S._validityProp[i];
            if (o != undefined && (r = t.attr("data-errormessage-" + o)) != undefined) return r;
            if ((r = t.attr("data-errormessage")) != undefined) return r;
            var s = "#" + t.attr("id");
            if ("undefined" != typeof n.custom_error_messages[s] && "undefined" != typeof n.custom_error_messages[s][i]) r = n.custom_error_messages[s][i].message; else if (0 < e.length) for (var a = 0; a < e.length && 0 < e.length; a++) {
                var l = "." + e[a];
                if ("undefined" != typeof n.custom_error_messages[l] && "undefined" != typeof n.custom_error_messages[l][i]) {
                    r = n.custom_error_messages[l][i].message;
                    break
                }
            }
            return r || "undefined" == typeof n.custom_error_messages[i] || "undefined" == typeof n.custom_error_messages[i].message || (r = n.custom_error_messages[i].message), r
        },
        _validityProp: {
            required: "value-missing",
            custom: "custom-error",
            groupRequired: "value-missing",
            ajax: "custom-error",
            minSize: "range-underflow",
            maxSize: "range-overflow",
            min: "range-underflow",
            max: "range-overflow",
            past: "type-mismatch",
            future: "type-mismatch",
            dateRange: "type-mismatch",
            dateTimeRange: "type-mismatch",
            maxCheckbox: "range-overflow",
            minCheckbox: "range-underflow",
            equals: "pattern-mismatch",
            funcCall: "custom-error",
            funcCallRequired: "custom-error",
            creditCard: "pattern-mismatch",
            condRequired: "value-missing"
        },
        _required: function (t, e, i, n, r) {
            switch (t.prop("type")) {
                case"radio":
                case"checkbox":
                    if (r) {
                        if (!t.prop("checked")) return n.allrules[e[i]].alertTextCheckboxMultiple;
                        break
                    }
                    var o = t.closest("form, .validationEngineContainer"), s = t.attr("name");
                    if (0 == o.find("input[name='" + s + "']:checked").size()) return 1 == o.find("input[name='" + s + "']:visible").size() ? n.allrules[e[i]].alertTextCheckboxe : n.allrules[e[i]].alertTextCheckboxMultiple;
                    break;
                case"text":
                case"password":
                case"textarea":
                case"file":
                case"select-one":
                case"select-multiple":
                default:
                    var a = k.trim(t.val()), l = k.trim(t.attr("data-validation-placeholder")),
                        c = k.trim(t.attr("placeholder"));
                    if (!a || l && a == l || c && a == c) return n.allrules[e[i]].alertText
            }
        },
        _groupRequired: function (t, e, i, n) {
            var r = "[" + n.validateAttribute + "*=" + e[i + 1] + "]", o = !1;
            if (t.closest("form, .validationEngineContainer").find(r).each(function () {
                if (!S._required(k(this), e, i, n)) return !(o = !0)
            }), !o) return n.allrules[e[i]].alertText
        },
        _custom: function (t, e, i, n) {
            var r, o = e[i + 1], s = n.allrules[o];
            if (s) if (s.regex) {
                var a = s.regex;
                if (!a) return void alert("jqv:custom regex not found - " + o);
                if (!new RegExp(a).test(t.val())) return n.allrules[o].alertText
            } else {
                if (!s.func) return void alert("jqv:custom type not allowed " + o);
                if ("function" != typeof (r = s.func)) return void alert("jqv:custom parameter 'function' is no function - " + o);
                if (!r(t, e, i, n)) return n.allrules[o].alertText
            } else alert("jqv:custom rule not found - " + o)
        },
        _funcCall: function (t, e, i, n) {
            var r, o = e[i + 1];
            if (-1 < o.indexOf(".")) {
                for (var s = o.split("."), a = window; s.length;) a = a[s.shift()];
                r = a
            } else r = window[o] || n.customFunctions[o];
            if ("function" == typeof r) return r(t, e, i, n)
        },
        _funcCallRequired: function (t, e, i, n) {
            return S._funcCall(t, e, i, n)
        },
        _equals: function (t, e, i, n) {
            var r = e[i + 1];
            if (t.val() != k("#" + r).val()) return n.allrules.equals.alertText
        },
        _maxSize: function (t, e, i, n) {
            var r = e[i + 1];
            if (r < t.val().length) {
                var o = n.allrules.maxSize;
                return o.alertText + r + o.alertText2
            }
        },
        _minSize: function (t, e, i, n) {
            var r = e[i + 1];
            if (t.val().length < r) {
                var o = n.allrules.minSize;
                return o.alertText + r + o.alertText2
            }
        },
        _min: function (t, e, i, n) {
            var r = parseFloat(e[i + 1]);
            if (parseFloat(t.val()) < r) {
                var o = n.allrules.min;
                return o.alertText2 ? o.alertText + r + o.alertText2 : o.alertText + r
            }
        },
        _max: function (t, e, i, n) {
            var r = parseFloat(e[i + 1]);
            if (r < parseFloat(t.val())) {
                var o = n.allrules.max;
                return o.alertText2 ? o.alertText + r + o.alertText2 : o.alertText + r
            }
        },
        _past: function (t, e, i, n, r) {
            var o, s = i[n + 1], a = k(t.find("*[name='" + s.replace(/^#+/, "") + "']"));
            if ("now" == s.toLowerCase()) o = new Date; else if (undefined != a.val()) {
                if (a.is(":disabled")) return;
                o = S._parseDate(a.val())
            } else o = S._parseDate(s);
            if (o < S._parseDate(e.val())) {
                var l = r.allrules.past;
                return l.alertText2 ? l.alertText + S._dateToString(o) + l.alertText2 : l.alertText + S._dateToString(o)
            }
        },
        _future: function (t, e, i, n, r) {
            var o, s = i[n + 1], a = k(t.find("*[name='" + s.replace(/^#+/, "") + "']"));
            if ("now" == s.toLowerCase()) o = new Date; else if (undefined != a.val()) {
                if (a.is(":disabled")) return;
                o = S._parseDate(a.val())
            } else o = S._parseDate(s);
            if (S._parseDate(e.val()) < o) {
                var l = r.allrules.future;
                return l.alertText2 ? l.alertText + S._dateToString(o) + l.alertText2 : l.alertText + S._dateToString(o)
            }
        },
        _isDate: function (t) {
            return new RegExp(/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(?:(?:0?[1-9]|1[0-2])(\/|-)(?:0?[1-9]|1\d|2[0-8]))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(0?2(\/|-)29)(\/|-)(?:(?:0[48]00|[13579][26]00|[2468][048]00)|(?:\d\d)?(?:0[48]|[2468][048]|[13579][26]))$/).test(t)
        },
        _isDateTime: function (t) {
            return new RegExp(/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1}$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^((1[012]|0?[1-9]){1}\/(0?[1-9]|[12][0-9]|3[01]){1}\/\d{2,4}\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1})$/).test(t)
        },
        _dateCompare: function (t, e) {
            return new Date(t.toString()) < new Date(e.toString())
        },
        _dateRange: function (t, e, i, n) {
            return !n.firstOfGroup[0].value && n.secondOfGroup[0].value || n.firstOfGroup[0].value && !n.secondOfGroup[0].value ? n.allrules[e[i]].alertText + n.allrules[e[i]].alertText2 : S._isDate(n.firstOfGroup[0].value) && S._isDate(n.secondOfGroup[0].value) && S._dateCompare(n.firstOfGroup[0].value, n.secondOfGroup[0].value) ? void 0 : n.allrules[e[i]].alertText + n.allrules[e[i]].alertText2
        },
        _dateTimeRange: function (t, e, i, n) {
            return !n.firstOfGroup[0].value && n.secondOfGroup[0].value || n.firstOfGroup[0].value && !n.secondOfGroup[0].value ? n.allrules[e[i]].alertText + n.allrules[e[i]].alertText2 : S._isDateTime(n.firstOfGroup[0].value) && S._isDateTime(n.secondOfGroup[0].value) && S._dateCompare(n.firstOfGroup[0].value, n.secondOfGroup[0].value) ? void 0 : n.allrules[e[i]].alertText + n.allrules[e[i]].alertText2
        },
        _maxCheckbox: function (t, e, i, n, r) {
            var o = i[n + 1], s = e.attr("name");
            if (o < t.find("input[name='" + s + "']:checked").size()) return r.showArrow = !1, r.allrules.maxCheckbox.alertText2 ? r.allrules.maxCheckbox.alertText + " " + o + " " + r.allrules.maxCheckbox.alertText2 : r.allrules.maxCheckbox.alertText
        },
        _minCheckbox: function (t, e, i, n, r) {
            var o = i[n + 1], s = e.attr("name");
            if (t.find("input[name='" + s + "']:checked").size() < o) return r.showArrow = !1, r.allrules.minCheckbox.alertText + " " + o + " " + r.allrules.minCheckbox.alertText2
        },
        _creditCard: function (t, e, i, n) {
            var r = !1, o = t.val().replace(/ +/g, "").replace(/-+/g, ""), s = o.length;
            if (14 <= s && s <= 16 && 0 < parseInt(o)) {
                for (var a, l = 0, c = (i = s - 1, 1), h = new String; a = parseInt(o.charAt(i)), h += c++ % 2 == 0 ? 2 * a : a, 0 <= --i;) ;
                for (i = 0; i < h.length; i++) l += parseInt(h.charAt(i));
                r = l % 10 == 0
            }
            if (!r) return n.allrules.creditCard.alertText
        },
        _ajax: function (s, t, e, a) {
            var i = t[e + 1], l = a.allrules[i], n = l.extraData, r = l.extraDataDynamic,
                o = {fieldId: s.attr("id"), fieldValue: s.val()};
            if ("object" == typeof n) k.extend(o, n); else if ("string" == typeof n) {
                var c = n.split("&");
                for (e = 0; e < c.length; e++) {
                    var h = c[e].split("=");
                    h[0] && h[0] && (o[h[0]] = h[1])
                }
            }
            if (r) {
                var d = String(r).split(",");
                for (e = 0; e < d.length; e++) {
                    var u = d[e];
                    if (k(u).length) {
                        var p = s.closest("form, .validationEngineContainer").find(u).val();
                        u.replace("#", ""), escape(p);
                        o[u.replace("#", "")] = p
                    }
                }
            }
            if ("field" == a.eventTrigger && delete a.ajaxValidCache[s.attr("id")], !a.isError && !S._checkAjaxFieldStatus(s.attr("id"), a)) return k.ajax({
                type: a.ajaxFormValidationMethod,
                url: l.url,
                cache: !1,
                dataType: "json",
                data: o,
                field: s,
                rule: l,
                methods: S,
                options: a,
                beforeSend: function () {
                },
                error: function (t, e) {
                    a.onFailure ? a.onFailure(t, e) : S._ajaxError(t, e)
                },
                success: function (t) {
                    var e = t[0], i = k("#" + e).eq(0);
                    if (1 == i.length) {
                        var n = t[1], r = t[2];
                        if (n) {
                            if (a.ajaxValidCache[e] = !0, r) {
                                if (a.allrules[r]) (o = a.allrules[r].alertTextOk) && (r = o)
                            } else r = l.alertTextOk;
                            a.showPrompts && (r ? S._showPrompt(i, r, "pass", !0, a) : S._closePrompt(i)), "submit" == a.eventTrigger && s.closest("form").submit()
                        } else {
                            var o;
                            if (a.ajaxValidCache[e] = !1, a.isError = !0, r) {
                                if (a.allrules[r]) (o = a.allrules[r].alertText) && (r = o)
                            } else r = l.alertText;
                            a.showPrompts && S._showPrompt(i, r, "", !0, a)
                        }
                    }
                    i.trigger("jqv.field.result", [i, a.isError, r])
                }
            }), l.alertTextLoad
        },
        _ajaxError: function (t, e) {
            0 == t.status && null == e && alert("The page is not served from a server! ajax call failed")
        },
        _dateToString: function (t) {
            return t.getFullYear() + "-" + (t.getMonth() + 1) + "-" + t.getDate()
        },
        _parseDate: function (t) {
            var e = t.split("-");
            return e == t && (e = t.split("/")), e == t ? (e = t.split("."), new Date(e[2], e[1] - 1, e[0])) : new Date(e[0], e[1] - 1, e[2])
        },
        _showPrompt: function (t, e, i, n, r, o) {
            t.data("jqv-prompt-at") instanceof jQuery ? t = t.data("jqv-prompt-at") : t.data("jqv-prompt-at") && (t = k(t.data("jqv-prompt-at")));
            var s = S._getPrompt(t);
            o && (s = !1), k.trim(e) && (s ? S._updatePrompt(t, s, e, i, n, r) : S._buildPrompt(t, e, i, n, r))
        },
        _buildPrompt: function (t, e, i, n, r) {
            var o = k("<div>");
            switch (o.addClass(S._getClassName(t.attr("id")) + "formError"), o.addClass("parentForm" + S._getClassName(t.closest("form, .validationEngineContainer").attr("id"))), o.addClass("formError"), i) {
                case"pass":
                    o.addClass("greenPopup");
                    break;
                case"load":
                    o.addClass("blackPopup")
            }
            n && o.addClass("ajaxed");
            k("<div>").addClass("formErrorContent").html(e).appendTo(o);
            var s = t.data("promptPosition") || r.promptPosition;
            if (r.showArrow) {
                var a = k("<div>").addClass("formErrorArrow");
                if ("string" == typeof s) -1 != (h = s.indexOf(":")) && (s = s.substring(0, h));
                switch (s) {
                    case"bottomLeft":
                    case"bottomRight":
                        o.find(".formErrorContent").before(a), a.addClass("formErrorArrowBottom").html('<div class="line1"><!-- --></div><div class="line2"><!-- --></div><div class="line3"><!-- --></div><div class="line4"><!-- --></div><div class="line5"><!-- --></div><div class="line6"><!-- --></div><div class="line7"><!-- --></div><div class="line8"><!-- --></div><div class="line9"><!-- --></div><div class="line10"><!-- --></div>');
                        break;
                    case"topLeft":
                    case"topRight":
                        a.html('<div class="line10"><!-- --></div><div class="line9"><!-- --></div><div class="line8"><!-- --></div><div class="line7"><!-- --></div><div class="line6"><!-- --></div><div class="line5"><!-- --></div><div class="line4"><!-- --></div><div class="line3"><!-- --></div><div class="line2"><!-- --></div><div class="line1"><!-- --></div>'), o.append(a)
                }
            }
            r.addPromptClass && o.addClass(r.addPromptClass);
            var l = t.attr("data-required-class");
            if (l !== undefined) o.addClass(l); else if (r.prettySelect && k("#" + t.attr("id")).next().is("select")) {
                var c = k("#" + t.attr("id").substr(r.usePrefix.length).substring(r.useSuffix.length)).attr("data-required-class");
                c !== undefined && o.addClass(c)
            }
            o.css({opacity: 0}), "inline" === s ? (o.addClass("inline"), void 0 !== t.attr("data-prompt-target") && 0 < k("#" + t.attr("data-prompt-target")).length ? o.appendTo(k("#" + t.attr("data-prompt-target"))) : t.after(o)) : t.before(o);
            var h = S._calculatePosition(t, o, r);
            return o.css({
                position: "inline" === s ? "relative" : "absolute",
                top: h.callerTopPosition,
                left: h.callerleftPosition,
                marginTop: h.marginTopSize,
                opacity: 0
            }).data("callerField", t), r.autoHidePrompt && setTimeout(function () {
                o.animate({opacity: 0}, function () {
                    o.closest(".formErrorOuter").remove(), o.remove()
                })
            }, r.autoHideDelay), o.animate({opacity: .87})
        },
        _updatePrompt: function (t, e, i, n, r, o, s) {
            if (e) {
                void 0 !== n && ("pass" == n ? e.addClass("greenPopup") : e.removeClass("greenPopup"), "load" == n ? e.addClass("blackPopup") : e.removeClass("blackPopup")), r ? e.addClass("ajaxed") : e.removeClass("ajaxed"), e.find(".formErrorContent").html(i);
                var a = S._calculatePosition(t, e, o),
                    l = {top: a.callerTopPosition, left: a.callerleftPosition, marginTop: a.marginTopSize};
                s ? e.css(l) : e.animate(l)
            }
        },
        _closePrompt: function (t) {
            var e = S._getPrompt(t);
            e && e.fadeTo("fast", 0, function () {
                e.parent(".formErrorOuter").remove(), e.remove()
            })
        },
        closePrompt: function (t) {
            return S._closePrompt(t)
        },
        _getPrompt: function (t) {
            var e = k(t).closest("form, .validationEngineContainer").attr("id"),
                i = S._getClassName(t.attr("id")) + "formError",
                n = k("." + S._escapeExpression(i) + ".parentForm" + S._getClassName(e))[0];
            if (n) return k(n)
        },
        _escapeExpression: function (t) {
            return t.replace(/([#;&,\.\+\*\~':"\!\^$\[\]\(\)=>\|])/g, "\\$1")
        },
        isRTL: function (t) {
            var e = k(document), i = k("body"),
                n = t && t.hasClass("rtl") || t && "rtl" === (t.attr("dir") || "").toLowerCase() || e.hasClass("rtl") || "rtl" === (e.attr("dir") || "").toLowerCase() || i.hasClass("rtl") || "rtl" === (i.attr("dir") || "").toLowerCase();
            return Boolean(n)
        },
        _calculatePosition: function (t, e, i) {
            var n, r, o, s = t.width(), a = t.position().left, l = t.position().top;
            t.height();
            n = r = 0, o = -e.height();
            var c = t.data("promptPosition") || i.promptPosition, h = "", d = "", u = 0, p = 0;
            switch ("string" == typeof c && -1 != c.indexOf(":") && (h = c.substring(c.indexOf(":") + 1), c = c.substring(0, c.indexOf(":")), -1 != h.indexOf(",") && (d = h.substring(h.indexOf(",") + 1), h = h.substring(0, h.indexOf(",")), p = parseInt(d), isNaN(p) && (p = 0)), u = parseInt(h), isNaN(h) && (h = 0)), c) {
                default:
                case"topRight":
                    r += a + s - 27, n += l;
                    break;
                case"topLeft":
                    n += l, r += a;
                    break;
                case"centerRight":
                    n = l + 4, o = 0, r = a + t.outerWidth(!0) + 5;
                    break;
                case"centerLeft":
                    r = a - (e.width() + 2), n = l + 4, o = 0;
                    break;
                case"bottomLeft":
                    n = l + t.height() + 5, o = 0, r = a;
                    break;
                case"bottomRight":
                    r = a + s - 27, n = l + t.height() + 5, o = 0;
                    break;
                case"inline":
                    o = n = r = 0
            }
            return {callerTopPosition: (n += p) + "px", callerleftPosition: (r += u) + "px", marginTopSize: o + "px"}
        },
        _saveOptions: function (t, e) {
            if (k.validationEngineLanguage) var i = k.validationEngineLanguage.allRules; else k.error("jQuery.validationEngine rules are not loaded, plz add localization files to the page");
            k.validationEngine.defaults.allrules = i;
            var n = k.extend(!0, {}, k.validationEngine.defaults, e);
            return t.data("jqv", n), n
        },
        _getClassName: function (t) {
            if (t) return t.replace(/:/g, "_").replace(/\./g, "_")
        },
        _jqSelector: function (t) {
            return t.replace(/([;&,\.\+\*\~':"\!\^#$%@\[\]\(\)=>\|])/g, "\\$1")
        },
        _condRequired: function (t, e, i, n) {
            var r, o;
            for (r = i + 1; r < e.length; r++) if ((o = jQuery("#" + e[r]).first()).length && S._required(o, ["required"], 0, n, !0) == undefined) return S._required(t, ["required"], 0, n)
        },
        _submitButtonClick: function () {
            var t = k(this);
            t.closest("form, .validationEngineContainer").data("jqv_submitButton", t.attr("id"))
        }
    };
    k.fn.validationEngine = function (t) {
        var e = k(this);
        return e[0] ? "string" == typeof t && "_" != t.charAt(0) && S[t] ? ("showPrompt" != t && "hide" != t && "hideAll" != t && S.init.apply(e), S[t].apply(e, Array.prototype.slice.call(arguments, 1))) : "object" != typeof t && t ? void k.error("Method " + t + " does not exist in jQuery.validationEngine") : (S.init.apply(e, arguments), S.attach.apply(e)) : e
    }, k.validationEngine = {
        fieldIdCounter: 0,
        defaults: {
            validationEventTrigger: "blur",
            scroll: !0,
            focusFirstField: !0,
            showPrompts: !0,
            validateNonVisibleFields: !1,
            promptPosition: "topRight",
            bindMethod: "bind",
            inlineAjax: !1,
            ajaxFormValidation: !1,
            ajaxFormValidationURL: !1,
            ajaxFormValidationMethod: "get",
            onAjaxFormComplete: k.noop,
            onBeforeAjaxFormValidation: k.noop,
            onValidationComplete: !1,
            doNotShowAllErrosOnSubmit: !1,
            custom_error_messages: {},
            binded: !0,
            showArrow: !0,
            showArrowOnRadioAndCheckbox: !1,
            isError: !1,
            maxErrorsPerField: !1,
            ajaxValidCache: {},
            autoPositionUpdate: !1,
            InvalidFields: [],
            onFieldSuccess: !1,
            onFieldFailure: !1,
            onSuccess: !1,
            onFailure: !1,
            validateAttribute: "class",
            addSuccessCssClassToField: "",
            addFailureCssClassToField: "",
            autoHidePrompt: !1,
            autoHideDelay: 1e4,
            fadeDuration: .3,
            prettySelect: !1,
            addPromptClass: "",
            usePrefix: "",
            useSuffix: "",
            showOneMessage: !1
        }
    }, k(function () {
        k.validationEngine.defaults.promptPosition = S.isRTL() ? "topLeft" : "topRight"
    })
}(jQuery), jQuery(document).ready(function () {
    jQuery(".valid_engine").validationEngine("attach", {promptPosition: "topLeft"})
}), function () {
    function t(a) {
        function l(t, e, i, n, r, o) {
            for (; 0 <= r && r < o; r += a) {
                var s = n ? n[r] : r;
                i = e(i, t[s], s, t)
            }
            return i
        }

        return function (t, e, i, n) {
            e = x(e, n, 4);
            var r = !T(t) && y.keys(t), o = (r || t).length, s = 0 < a ? 0 : o - 1;
            return arguments.length < 3 && (i = t[r ? r[s] : s], s += a), l(t, e, i, r, s, o)
        }
    }

    function e(o) {
        return function (t, e, i) {
            e = b(e, i);
            for (var n = S(t), r = 0 < o ? 0 : n - 1; 0 <= r && r < n; r += o) if (e(t[r], r, t)) return r;
            return -1
        }
    }

    function i(o, s, a) {
        return function (t, e, i) {
            var n = 0, r = S(t);
            if ("number" == typeof i) 0 < o ? n = 0 <= i ? i : Math.max(i + r, n) : r = 0 <= i ? Math.min(i + 1, r) : i + r + 1; else if (a && i && r) return t[i = a(t, e)] === e ? i : -1;
            if (e != e) return 0 <= (i = s(h.call(t, n, r), y.isNaN)) ? i + n : -1;
            for (i = 0 < o ? n : r - 1; 0 <= i && i < r; i += o) if (t[i] === e) return i;
            return -1
        }
    }

    function n(t, e) {
        var i = P.length, n = t.constructor, r = y.isFunction(n) && n.prototype || a, o = "constructor";
        for (y.has(t, o) && !y.contains(e, o) && e.push(o); i--;) (o = P[i]) in t && t[o] !== r[o] && !y.contains(e, o) && e.push(o)
    }

    var r = this, o = r._, s = Array.prototype, a = Object.prototype, l = Function.prototype, c = s.push, h = s.slice,
        d = a.toString, u = a.hasOwnProperty, p = Array.isArray, f = Object.keys, g = l.bind, m = Object.create,
        v = function () {
        }, y = function (t) {
            return t instanceof y ? t : this instanceof y ? void (this._wrapped = t) : new y(t)
        };
    "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = y), exports._ = y) : r._ = y, y.VERSION = "1.8.3";
    var x = function (r, o, t) {
        if (void 0 === o) return r;
        switch (null == t ? 3 : t) {
            case 1:
                return function (t) {
                    return r.call(o, t)
                };
            case 2:
                return function (t, e) {
                    return r.call(o, t, e)
                };
            case 3:
                return function (t, e, i) {
                    return r.call(o, t, e, i)
                };
            case 4:
                return function (t, e, i, n) {
                    return r.call(o, t, e, i, n)
                }
        }
        return function () {
            return r.apply(o, arguments)
        }
    }, b = function (t, e, i) {
        return null == t ? y.identity : y.isFunction(t) ? x(t, e, i) : y.isObject(t) ? y.matcher(t) : y.property(t)
    };
    y.iteratee = function (t, e) {
        return b(t, e, Infinity)
    };
    var w = function (l, c) {
        return function (t) {
            var e = arguments.length;
            if (e < 2 || null == t) return t;
            for (var i = 1; i < e; i++) for (var n = arguments[i], r = l(n), o = r.length, s = 0; s < o; s++) {
                var a = r[s];
                c && void 0 !== t[a] || (t[a] = n[a])
            }
            return t
        }
    }, C = function (t) {
        if (!y.isObject(t)) return {};
        if (m) return m(t);
        v.prototype = t;
        var e = new v;
        return v.prototype = null, e
    }, _ = function (e) {
        return function (t) {
            return null == t ? void 0 : t[e]
        }
    }, k = Math.pow(2, 53) - 1, S = _("length"), T = function (t) {
        var e = S(t);
        return "number" == typeof e && 0 <= e && e <= k
    };
    y.each = y.forEach = function (t, e, i) {
        var n, r;
        if (e = x(e, i), T(t)) for (n = 0, r = t.length; n < r; n++) e(t[n], n, t); else {
            var o = y.keys(t);
            for (n = 0, r = o.length; n < r; n++) e(t[o[n]], o[n], t)
        }
        return t
    }, y.map = y.collect = function (t, e, i) {
        e = b(e, i);
        for (var n = !T(t) && y.keys(t), r = (n || t).length, o = Array(r), s = 0; s < r; s++) {
            var a = n ? n[s] : s;
            o[s] = e(t[a], a, t)
        }
        return o
    }, y.reduce = y.foldl = y.inject = t(1), y.reduceRight = y.foldr = t(-1), y.find = y.detect = function (t, e, i) {
        var n;
        if (void 0 !== (n = T(t) ? y.findIndex(t, e, i) : y.findKey(t, e, i)) && -1 !== n) return t[n]
    }, y.filter = y.select = function (t, n, e) {
        var r = [];
        return n = b(n, e), y.each(t, function (t, e, i) {
            n(t, e, i) && r.push(t)
        }), r
    }, y.reject = function (t, e, i) {
        return y.filter(t, y.negate(b(e)), i)
    }, y.every = y.all = function (t, e, i) {
        e = b(e, i);
        for (var n = !T(t) && y.keys(t), r = (n || t).length, o = 0; o < r; o++) {
            var s = n ? n[o] : o;
            if (!e(t[s], s, t)) return !1
        }
        return !0
    }, y.some = y.any = function (t, e, i) {
        e = b(e, i);
        for (var n = !T(t) && y.keys(t), r = (n || t).length, o = 0; o < r; o++) {
            var s = n ? n[o] : o;
            if (e(t[s], s, t)) return !0
        }
        return !1
    }, y.contains = y.includes = y.include = function (t, e, i, n) {
        return T(t) || (t = y.values(t)), ("number" != typeof i || n) && (i = 0), 0 <= y.indexOf(t, e, i)
    }, y.invoke = function (t, i) {
        var n = h.call(arguments, 2), r = y.isFunction(i);
        return y.map(t, function (t) {
            var e = r ? i : t[i]
            ;
            return null == e ? e : e.apply(t, n)
        })
    }, y.pluck = function (t, e) {
        return y.map(t, y.property(e))
    }, y.where = function (t, e) {
        return y.filter(t, y.matcher(e))
    }, y.findWhere = function (t, e) {
        return y.find(t, y.matcher(e))
    }, y.max = function (t, n, e) {
        var i, r, o = -Infinity, s = -Infinity;
        if (null == n && null != t) for (var a = 0, l = (t = T(t) ? t : y.values(t)).length; a < l; a++) i = t[a], o < i && (o = i); else n = b(n, e), y.each(t, function (t, e, i) {
            r = n(t, e, i), (s < r || r === -Infinity && o === -Infinity) && (o = t, s = r)
        });
        return o
    }, y.min = function (t, n, e) {
        var i, r, o = Infinity, s = Infinity;
        if (null == n && null != t) for (var a = 0, l = (t = T(t) ? t : y.values(t)).length; a < l; a++) (i = t[a]) < o && (o = i); else n = b(n, e), y.each(t, function (t, e, i) {
            ((r = n(t, e, i)) < s || r === Infinity && o === Infinity) && (o = t, s = r)
        });
        return o
    }, y.shuffle = function (t) {
        for (var e, i = T(t) ? t : y.values(t), n = i.length, r = Array(n), o = 0; o < n; o++) (e = y.random(0, o)) !== o && (r[o] = r[e]), r[e] = i[o];
        return r
    }, y.sample = function (t, e, i) {
        return null == e || i ? (T(t) || (t = y.values(t)), t[y.random(t.length - 1)]) : y.shuffle(t).slice(0, Math.max(0, e))
    }, y.sortBy = function (t, n, e) {
        return n = b(n, e), y.pluck(y.map(t, function (t, e, i) {
            return {value: t, index: e, criteria: n(t, e, i)}
        }).sort(function (t, e) {
            var i = t.criteria, n = e.criteria;
            if (i !== n) {
                if (n < i || void 0 === i) return 1;
                if (i < n || void 0 === n) return -1
            }
            return t.index - e.index
        }), "value")
    };
    var A = function (s) {
        return function (n, r, t) {
            var o = {};
            return r = b(r, t), y.each(n, function (t, e) {
                var i = r(t, e, n);
                s(o, t, i)
            }), o
        }
    };
    y.groupBy = A(function (t, e, i) {
        y.has(t, i) ? t[i].push(e) : t[i] = [e]
    }), y.indexBy = A(function (t, e, i) {
        t[i] = e
    }), y.countBy = A(function (t, e, i) {
        y.has(t, i) ? t[i]++ : t[i] = 1
    }), y.toArray = function (t) {
        return t ? y.isArray(t) ? h.call(t) : T(t) ? y.map(t, y.identity) : y.values(t) : []
    }, y.size = function (t) {
        return null == t ? 0 : T(t) ? t.length : y.keys(t).length
    }, y.partition = function (t, n, e) {
        n = b(n, e);
        var r = [], o = [];
        return y.each(t, function (t, e, i) {
            (n(t, e, i) ? r : o).push(t)
        }), [r, o]
    }, y.first = y.head = y.take = function (t, e, i) {
        if (null != t) return null == e || i ? t[0] : y.initial(t, t.length - e)
    }, y.initial = function (t, e, i) {
        return h.call(t, 0, Math.max(0, t.length - (null == e || i ? 1 : e)))
    }, y.last = function (t, e, i) {
        if (null != t) return null == e || i ? t[t.length - 1] : y.rest(t, Math.max(0, t.length - e))
    }, y.rest = y.tail = y.drop = function (t, e, i) {
        return h.call(t, null == e || i ? 1 : e)
    }, y.compact = function (t) {
        return y.filter(t, y.identity)
    };
    var $ = function (t, e, i, n) {
        for (var r = [], o = 0, s = n || 0, a = S(t); s < a; s++) {
            var l = t[s];
            if (T(l) && (y.isArray(l) || y.isArguments(l))) {
                e || (l = $(l, e, i));
                var c = 0, h = l.length;
                for (r.length += h; c < h;) r[o++] = l[c++]
            } else i || (r[o++] = l)
        }
        return r
    };
    y.flatten = function (t, e) {
        return $(t, e, !1)
    }, y.without = function (t) {
        return y.difference(t, h.call(arguments, 1))
    }, y.uniq = y.unique = function (t, e, i, n) {
        y.isBoolean(e) || (n = i, i = e, e = !1), null != i && (i = b(i, n));
        for (var r = [], o = [], s = 0, a = S(t); s < a; s++) {
            var l = t[s], c = i ? i(l, s, t) : l;
            e ? (s && o === c || r.push(l), o = c) : i ? y.contains(o, c) || (o.push(c), r.push(l)) : y.contains(r, l) || r.push(l)
        }
        return r
    }, y.union = function () {
        return y.uniq($(arguments, !0, !0))
    }, y.intersection = function (t) {
        for (var e = [], i = arguments.length, n = 0, r = S(t); n < r; n++) {
            var o = t[n];
            if (!y.contains(e, o)) {
                for (var s = 1; s < i && y.contains(arguments[s], o); s++) ;
                s === i && e.push(o)
            }
        }
        return e
    }, y.difference = function (t) {
        var e = $(arguments, !0, !0, 1);
        return y.filter(t, function (t) {
            return !y.contains(e, t)
        })
    }, y.zip = function () {
        return y.unzip(arguments)
    }, y.unzip = function (t) {
        for (var e = t && y.max(t, S).length || 0, i = Array(e), n = 0; n < e; n++) i[n] = y.pluck(t, n);
        return i
    }, y.object = function (t, e) {
        for (var i = {}, n = 0, r = S(t); n < r; n++) e ? i[t[n]] = e[n] : i[t[n][0]] = t[n][1];
        return i
    }, y.findIndex = e(1), y.findLastIndex = e(-1), y.sortedIndex = function (t, e, i, n) {
        for (var r = (i = b(i, n, 1))(e), o = 0, s = S(t); o < s;) {
            var a = Math.floor((o + s) / 2);
            i(t[a]) < r ? o = a + 1 : s = a
        }
        return o
    }, y.indexOf = i(1, y.findIndex, y.sortedIndex), y.lastIndexOf = i(-1, y.findLastIndex), y.range = function (t, e, i) {
        null == e && (e = t || 0, t = 0), i = i || 1;
        for (var n = Math.max(Math.ceil((e - t) / i), 0), r = Array(n), o = 0; o < n; o++, t += i) r[o] = t;
        return r
    };
    var M = function (t, e, i, n, r) {
        if (!(n instanceof e)) return t.apply(i, r);
        var o = C(t.prototype), s = t.apply(o, r);
        return y.isObject(s) ? s : o
    };
    y.bind = function (t, e) {
        if (g && t.bind === g) return g.apply(t, h.call(arguments, 1));
        if (!y.isFunction(t)) throw new TypeError("Bind must be called on a function");
        var i = h.call(arguments, 2), n = function () {
            return M(t, n, e, this, i.concat(h.call(arguments)))
        };
        return n
    }, y.partial = function (r) {
        var o = h.call(arguments, 1), s = function () {
            for (var t = 0, e = o.length, i = Array(e), n = 0; n < e; n++) i[n] = o[n] === y ? arguments[t++] : o[n];
            for (; t < arguments.length;) i.push(arguments[t++]);
            return M(r, s, this, this, i)
        };
        return s
    }, y.bindAll = function (t) {
        var e, i, n = arguments.length;
        if (n <= 1) throw new Error("bindAll must be passed function names");
        for (e = 1; e < n; e++) t[i = arguments[e]] = y.bind(t[i], t);
        return t
    }, y.memoize = function (n, r) {
        var o = function (t) {
            var e = o.cache, i = "" + (r ? r.apply(this, arguments) : t);
            return y.has(e, i) || (e[i] = n.apply(this, arguments)), e[i]
        };
        return o.cache = {}, o
    }, y.delay = function (t, e) {
        var i = h.call(arguments, 2);
        return setTimeout(function () {
            return t.apply(null, i)
        }, e)
    }, y.defer = y.partial(y.delay, y, 1), y.throttle = function (i, n, r) {
        var o, s, a, l = null, c = 0;
        r || (r = {});
        var h = function () {
            c = !1 === r.leading ? 0 : y.now(), l = null, a = i.apply(o, s), l || (o = s = null)
        };
        return function () {
            var t = y.now();
            c || !1 !== r.leading || (c = t);
            var e = n - (t - c);
            return o = this, s = arguments, e <= 0 || n < e ? (l && (clearTimeout(l), l = null), c = t, a = i.apply(o, s), l || (o = s = null)) : l || !1 === r.trailing || (l = setTimeout(h, e)), a
        }
    }, y.debounce = function (e, i, n) {
        var r, o, s, a, l, c = function () {
            var t = y.now() - a;
            t < i && 0 <= t ? r = setTimeout(c, i - t) : (r = null, n || (l = e.apply(s, o), r || (s = o = null)))
        };
        return function () {
            s = this, o = arguments, a = y.now();
            var t = n && !r;
            return r || (r = setTimeout(c, i)), t && (l = e.apply(s, o), s = o = null), l
        }
    }, y.wrap = function (t, e) {
        return y.partial(e, t)
    }, y.negate = function (t) {
        return function () {
            return !t.apply(this, arguments)
        }
    }, y.compose = function () {
        var i = arguments, n = i.length - 1;
        return function () {
            for (var t = n, e = i[n].apply(this, arguments); t--;) e = i[t].call(this, e);
            return e
        }
    }, y.after = function (t, e) {
        return function () {
            if (--t < 1) return e.apply(this, arguments)
        }
    }, y.before = function (t, e) {
        var i;
        return function () {
            return 0 < --t && (i = e.apply(this, arguments)), t <= 1 && (e = null), i
        }
    }, y.once = y.partial(y.before, 2);
    var E = !{toString: null}.propertyIsEnumerable("toString"),
        P = ["valueOf", "isPrototypeOf", "toString", "propertyIsEnumerable", "hasOwnProperty", "toLocaleString"];
    y.keys = function (t) {
        if (!y.isObject(t)) return [];
        if (f) return f(t);
        var e = [];
        for (var i in t) y.has(t, i) && e.push(i);
        return E && n(t, e), e
    }, y.allKeys = function (t) {
        if (!y.isObject(t)) return [];
        var e = [];
        for (var i in t) e.push(i);
        return E && n(t, e), e
    }, y.values = function (t) {
        for (var e = y.keys(t), i = e.length, n = Array(i), r = 0; r < i; r++) n[r] = t[e[r]];
        return n
    }, y.mapObject = function (t, e, i) {
        e = b(e, i);
        for (var n, r = y.keys(t), o = r.length, s = {}, a = 0; a < o; a++) s[n = r[a]] = e(t[n], n, t);
        return s
    }, y.pairs = function (t) {
        for (var e = y.keys(t), i = e.length, n = Array(i), r = 0; r < i; r++) n[r] = [e[r], t[e[r]]];
        return n
    }, y.invert = function (t) {
        for (var e = {}, i = y.keys(t), n = 0, r = i.length; n < r; n++) e[t[i[n]]] = i[n];
        return e
    }, y.functions = y.methods = function (t) {
        var e = [];
        for (var i in t) y.isFunction(t[i]) && e.push(i);
        return e.sort()
    }, y.extend = w(y.allKeys), y.extendOwn = y.assign = w(y.keys), y.findKey = function (t, e, i) {
        e = b(e, i);
        for (var n, r = y.keys(t), o = 0, s = r.length; o < s; o++) if (e(t[n = r[o]], n, t)) return n
    }, y.pick = function (t, e, i) {
        var n, r, o = {}, s = t;
        if (null == s) return o;
        y.isFunction(e) ? (r = y.allKeys(s), n = x(e, i)) : (r = $(arguments, !1, !1, 1), n = function (t, e, i) {
            return e in i
        }, s = Object(s));
        for (var a = 0, l = r.length; a < l; a++) {
            var c = r[a], h = s[c];
            n(h, c, s) && (o[c] = h)
        }
        return o
    }, y.omit = function (t, e, i) {
        if (y.isFunction(e)) e = y.negate(e); else {
            var n = y.map($(arguments, !1, !1, 1), String);
            e = function (t, e) {
                return !y.contains(n, e)
            }
        }
        return y.pick(t, e, i)
    }, y.defaults = w(y.allKeys, !0), y.create = function (t, e) {
        var i = C(t);
        return e && y.extendOwn(i, e), i
    }, y.clone = function (t) {
        return y.isObject(t) ? y.isArray(t) ? t.slice() : y.extend({}, t) : t
    }, y.tap = function (t, e) {
        return e(t), t
    }, y.isMatch = function (t, e) {
        var i = y.keys(e), n = i.length;
        if (null == t) return !n;
        for (var r = Object(t), o = 0; o < n; o++) {
            var s = i[o];
            if (e[s] !== r[s] || !(s in r)) return !1
        }
        return !0
    };
    var L = function (t, e, i, n) {
        if (t === e) return 0 !== t || 1 / t == 1 / e;
        if (null == t || null == e) return t === e;
        t instanceof y && (t = t._wrapped), e instanceof y && (e = e._wrapped);
        var r = d.call(t);
        if (r !== d.call(e)) return !1;
        switch (r) {
            case"[object RegExp]":
            case"[object String]":
                return "" + t == "" + e;
            case"[object Number]":
                return +t != +t ? +e != +e : 0 == +t ? 1 / +t == 1 / e : +t == +e;
            case"[object Date]":
            case"[object Boolean]":
                return +t == +e
        }
        var o = "[object Array]" === r;
        if (!o) {
            if ("object" != typeof t || "object" != typeof e) return !1;
            var s = t.constructor, a = e.constructor;
            if (s !== a && !(y.isFunction(s) && s instanceof s && y.isFunction(a) && a instanceof a) && "constructor" in t && "constructor" in e) return !1
        }
        n = n || [];
        for (var l = (i = i || []).length; l--;) if (i[l] === t) return n[l] === e;
        if (i.push(t), n.push(e), o) {
            if ((l = t.length) !== e.length) return !1;
            for (; l--;) if (!L(t[l], e[l], i, n)) return !1
        } else {
            var c, h = y.keys(t);
            if (l = h.length, y.keys(e).length !== l) return !1;
            for (; l--;) if (c = h[l], !y.has(e, c) || !L(t[c], e[c], i, n)) return !1
        }
        return i.pop(), n.pop(), !0
    };
    y.isEqual = function (t, e) {
        return L(t, e)
    }, y.isEmpty = function (t) {
        return null == t || (T(t) && (y.isArray(t) || y.isString(t) || y.isArguments(t)) ? 0 === t.length : 0 === y.keys(t).length)
    }, y.isElement = function (t) {
        return !(!t || 1 !== t.nodeType)
    }, y.isArray = p || function (t) {
        return "[object Array]" === d.call(t)
    }, y.isObject = function (t) {
        var e = typeof t;
        return "function" === e || "object" === e && !!t
    }, y.each(["Arguments", "Function", "String", "Number", "Date", "RegExp", "Error"], function (e) {
        y["is" + e] = function (t) {
            return d.call(t) === "[object " + e + "]"
        }
    }), y.isArguments(arguments) || (y.isArguments = function (t) {
        return y.has(t, "callee")
    }), "function" != typeof /./ && "object" != typeof Int8Array && (y.isFunction = function (t) {
        return "function" == typeof t || !1
    }), y.isFinite = function (t) {
        return isFinite(t) && !isNaN(parseFloat(t))
    }, y.isNaN = function (t) {
        return y.isNumber(t) && t !== +t
    }, y.isBoolean = function (t) {
        return !0 === t || !1 === t || "[object Boolean]" === d.call(t)
    }, y.isNull = function (t) {
        return null === t
    }, y.isUndefined = function (t) {
        return void 0 === t
    }, y.has = function (t, e) {
        return null != t && u.call(t, e)
    }, y.noConflict = function () {
        return r._ = o, this
    }, y.identity = function (t) {
        return t
    }, y.constant = function (t) {
        return function () {
            return t
        }
    }, y.noop = function () {
    }, y.property = _, y.propertyOf = function (e) {
        return null == e ? function () {
        } : function (t) {
            return e[t]
        }
    }, y.matcher = y.matches = function (e) {
        return e = y.extendOwn({}, e), function (t) {
            return y.isMatch(t, e)
        }
    }, y.times = function (t, e, i) {
        var n = Array(Math.max(0, t));
        e = x(e, i, 1);
        for (var r = 0; r < t; r++) n[r] = e(r);
        return n
    }, y.random = function (t, e) {
        return null == e && (e = t, t = 0), t + Math.floor(Math.random() * (e - t + 1))
    }, y.now = Date.now || function () {
        return (new Date).getTime()
    };
    var I = {"&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#x27;", "`": "&#x60;"}, F = y.invert(I),
        D = function (e) {
            var i = function (t) {
                return e[t]
            }, t = "(?:" + y.keys(e).join("|") + ")", n = RegExp(t), r = RegExp(t, "g");
            return function (t) {
                return t = null == t ? "" : "" + t, n.test(t) ? t.replace(r, i) : t
            }
        };
    y.escape = D(I), y.unescape = D(F), y.result = function (t, e, i) {
        var n = null == t ? void 0 : t[e];
        return void 0 === n && (n = i), y.isFunction(n) ? n.call(t) : n
    };
    var O = 0;
    y.uniqueId = function (t) {
        var e = ++O + "";
        return t ? t + e : e
    }, y.templateSettings = {evaluate: /<%([\s\S]+?)%>/g, interpolate: /<%=([\s\S]+?)%>/g, escape: /<%-([\s\S]+?)%>/g};
    var N = /(.)^/, j = {"'": "'", "\\": "\\", "\r": "r", "\n": "n", "\u2028": "u2028", "\u2029": "u2029"},
        z = /\\|'|\r|\n|\u2028|\u2029/g, R = function (t) {
            return "\\" + j[t]
        };
    y.template = function (o, t, e) {
        !t && e && (t = e), t = y.defaults({}, t, y.templateSettings);
        var i = RegExp([(t.escape || N).source, (t.interpolate || N).source, (t.evaluate || N).source].join("|") + "|$", "g"),
            s = 0, a = "__p+='";
        o.replace(i, function (t, e, i, n, r) {
            return a += o.slice(s, r).replace(z, R), s = r + t.length, e ? a += "'+\n((__t=(" + e + "))==null?'':_.escape(__t))+\n'" : i ? a += "'+\n((__t=(" + i + "))==null?'':__t)+\n'" : n && (a += "';\n" + n + "\n__p+='"), t
        }), a += "';\n", t.variable || (a = "with(obj||{}){\n" + a + "}\n"), a = "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" + a + "return __p;\n";
        try {
            var n = new Function(t.variable || "obj", "_", a)
        } catch (c) {
            throw c.source = a, c
        }
        var r = function (t) {
            return n.call(this, t, y)
        }, l = t.variable || "obj";
        return r.source = "function(" + l + "){\n" + a + "}", r
    }, y.chain = function (t) {
        var e = y(t);
        return e._chain = !0, e
    };
    var B = function (t, e) {
        return t._chain ? y(e).chain() : e
    };
    y.mixin = function (i) {
        y.each(y.functions(i), function (t) {
            var e = y[t] = i[t];
            y.prototype[t] = function () {
                var t = [this._wrapped];
                return c.apply(t, arguments), B(this, e.apply(y, t))
            }
        })
    }, y.mixin(y), y.each(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function (e) {
        var i = s[e];
        y.prototype[e] = function () {
            var t = this._wrapped;
            return i.apply(t, arguments), "shift" !== e && "splice" !== e || 0 !== t.length || delete t[0], B(this, t)
        }
    }), y.each(["concat", "join", "slice"], function (t) {
        var e = s[t];
        y.prototype[t] = function () {
            return B(this, e.apply(this._wrapped, arguments))
        }
    }), y.prototype.value = function () {
        return this._wrapped
    }, y.prototype.valueOf = y.prototype.toJSON = y.prototype.value, y.prototype.toString = function () {
        return "" + this._wrapped
    }, "function" == typeof define && define.amd && define("underscore", [], function () {
        return y
    })
}.call(this), function () {
    var t;
    t = "undefined" != typeof exports && null !== exports ? exports : this, jQuery(function () {
        if ("true" === $.cookie("wuxi_open_compare")) return $("#compare_tip").show(), show_save_cps()
    }), t.compare_cp = function (t, e, i, n, r) {
        return $("#compare_tip").show(), insert_cp_dom($($("#compare_tip li.no_data")[0]), t, e, i, n, r), $.cookie("wuxi_open_compare", "true", {path: "/"}), show_save_cps()
    }, t.compare_cp_with_div = function (t, e, i, n, r, o) {
        return insert_cp_dom($(t), e, i, n, r, o)
    }, t.insert_cp_dom = function (t, e, i, n, r, o) {
        var s;
        return t.find(".cp img.cp_img").attr("src", n), t.find(".cp a.name").attr("href", "/commodities/" + e + "?p_id=" + i + ".html").text(r.substring(0, 30) + "..."), t.find(".cp .price").text("\uffe5" + o), t.find(".cp").show(), t.removeClass("no_data"), t.find(".no_cp").hide(), s = t.attr("id"), $.cookie(s, e + "@@@" + i + "@@@" + n + "@@@" + r + "@@@" + o, {path: "/"})
    }, t.remove_cp = function (t) {
        return $("#compare_tip li#" + t + " .cp").hide(), $("#compare_tip li#" + t + " .no_cp").show(), $("#compare_tip li#" + t).addClass("no_data"), $.cookie(t, "", {path: "/"})
    }, t.remove_cps = function () {
        return $("#compare_tip .cp").hide(), $("#compare_tip .no_cp").show(), $("#compare_tip li").addClass("no_data"), $.cookie("wuxi_cp1", "", {path: "/"}), $.cookie("wuxi_cp2", "", {path: "/"}), $.cookie("wuxi_cp3", "", {path: "/"})
    }, t.do_compare = function () {
        var i;
        return i = "", $('#compare_tip li[class!="no_data"] .cp').each(function (t, e) {
            return i += $(e).find("a.name").attr("href").split("=")[1].replace(".html", "") + ","
        }), window.location.href = "/commodities/compare?ids=" + i
    }, t.hide_compare_tip = function () {
        return $("#compare_tip").hide(), $.cookie("wuxi_open_compare", "false", {path: "/"})
    }, t.show_save_cps = function () {
        return $(["wuxi_cp1", "wuxi_cp2", "wuxi_cp3"]).each(function (t, e) {
            var i;
            if ($.cookie(e)) return compare_cp_with_div("#compare_tip li#" + e, (i = $.cookie(e).split("@@@"))[0], i[1], i[2], i[3], i[4])
        })
    }
}.call(this), function () {
    var t;
    (t = "undefined" != typeof exports && null !== exports ? exports : this).disabled_add_to_cart = function (t) {
        return $("#add_to_cart").text(t), $("#add_to_cart").attr("href", "javascript:void(0);").removeClass("orderbutton").addClass("shopbuttonnone"), $("#hidden_no_stock_for").val("1")
    }, t.enable_add_to_cart = function (t) {
        var e;
        return $("#add_to_cart").text("\u52a0\u5165\u8d2d\u7269\u8f66"), e = "/cart/change/" + t + "?num=" + $("#available_quantity").val(), $("#add_to_cart").attr("href", e).removeClass("shopbuttonnone").addClass("orderbutton")
    }, t.disabled_order_button = function (t) {
        return $("#new_order_link").text(t).attr("href", "javascript:void(0);").removeClass("orderbutton").addClass("shopbuttonnone")
    }, t.enable_order_button = function () {
        return $("#new_order_link").text("\u8ba2\u5355\u786e\u8ba4").attr("href", "/orders/new.html").removeClass("shopbuttonnone").addClass("orderbutton")
    }, t.toggle_no_stock_class = function (t) {
        return t ? $("#new_order_link").removeClass("no_stock") : $("#new_order_link").addClass("no_stock")
    }, jQuery(function () {
        return $(document).on("click", ".binding_check_stock", function (t) {
            var e, i, n;
            return t.preventDefault(), (i = $(this)).text("\u7b49\u5f85"), n = i.data("product-id"), (e = i.data("area-id")) || (e = 320505), $.ajax({
                type: "post",
                url: "/products/" + n + "/check_stock?area_id=" + e,
                async: !1,
                success: function (t) {
                    return "true" === t ? location.href = i.attr("href") : (i.text("\u65e0\u8d27"), i.attr("href", "javascript:void(0);").removeClass("binding_check_stock"))
                },
                error: function () {
                    return i.text("\u65e0\u8d27"), i.attr("href", "javascript:void(0);").removeClass("binding_check_stock")
                }
            })
        }), $(document).on("click", ".increase_num, .decrease_num", function () {
            var t, e, i, n, r;
            return n = $(this).attr("alt").split("cart_item_")[1], !!$(".checkbox_" + n).is(":checked") && (t = $("#" + $(this).attr("alt")), i = parseInt(t.val()), e = null != (r = $.cookie("area_id")) ? r : 440106, $.ajax({
                type: "post",
                url: "/products/" + n + "/check_num_stock?area_id=" + e + "&num=" + i,
                async: !1,
                success: function (t) {
                    return t.info ? ($("#has_stock_" + n).html(t.limit).removeClass("no_stock"), enable_order_button()) : ($("#has_stock_" + n).html("\u65e0\u8d27").addClass("no_stock"), disabled_order_button("\u5546\u54c1\u65e0\u8d27"))
                },
                error: function () {
                    return $("#has_stock_" + n).html("\u65e0\u8d27").addClass("no_stock"), disabled_order_button("\u5546\u54c1\u65e0\u8d27")
                }
            }))
        }), $(document).on("keyup", ".cart-item-num", function () {
            var t, e, i, n;
            return i = this.id.split("cart_item_")[1], !!$(".checkbox_" + i).is(":checked") && (e = parseInt(this.value), t = null != (n = $.cookie("area_id")) ? n : 440106, $.ajax({
                type: "post",
                url: "/products/" + i + "/check_num_stock?area_id=" + t + "&num=" + e,
                async: !1,
                success: function (t) {
                    return t.info ? ($("#has_stock_" + i).html(t.limit).removeClass("no_stock"), enable_order_button()) : ($("#has_stock_" + i).html("\u65e0\u8d27").addClass("no_stock"), disabled_order_button("\u5546\u54c1\u65e0\u8d27"))
                },
                error: function () {
                    return $("#has_stock_" + i).html("\u65e0\u8d27").addClass("no_stock"), disabled_order_button("\u5546\u54c1\u65e0\u8d27")
                }
            }))
        }), $(".cart_checkbox").change(function () {
            var t;
            return t = $(this).attr("pid"), $("#cart_item_" + t).trigger("keyup")
        })
    })
}.call(this), SDMenu.prototype.init = function () {
    for (var t = this, e = 0; e < this.submenus.length; e++) this.submenus[e].getElementsByTagName("span")[0].onclick = function () {
        t.toggleMenu(this.parentNode)
    };
    if (this.markCurrent) {
        var i = this.menu.getElementsByTagName("a");
        for (e = 0; e < i.length; e++) if (i[e].href == document.location.href) {
            i[e].className = "current";
            break
        }
    }
    if (this.remember) {
        var n = new RegExp("sdmenu_" + encodeURIComponent(this.menu.id) + "=([01]+)").exec(document.cookie);
        if (n) {
            var r = n[1].split("");
            for (e = 0; e < r.length; e++) this.submenus[e].className = 0 == r[e] ? "collapsed" : ""
        }
    }
}, SDMenu.prototype.toggleMenu = function (t) {
    "collapsed" == t.className ? this.expandMenu(t) : this.collapseMenu(t)
}, SDMenu.prototype.expandMenu = function (e) {
    for (var i = e.getElementsByTagName("span")[0].offsetHeight, t = e.getElementsByTagName("a"), n = 0; n < t.length; n++) i += t[n].offsetHeight;
    var r = Math.round(this.speed * t.length), o = this, s = setInterval(function () {
        var t = e.offsetHeight + r;
        t < i ? e.style.height = t + "px" : (clearInterval(s), e.style.height = "", e.className = "", o.memorize())
    }, 30);
    this.collapseOthers(e)
}, SDMenu.prototype.collapseMenu = function (e) {
    var i = e.getElementsByTagName("span")[0].offsetHeight,
        n = Math.round(this.speed * e.getElementsByTagName("a").length), r = this, o = setInterval(function () {
            var t = e.offsetHeight - n;
            i < t ? e.style.height = t + "px" : (clearInterval(o), e.style.height = "", e.className = "collapsed", r.memorize())
        }, 30)
}, SDMenu.prototype.collapseOthers = function (t) {
    if (this.oneSmOnly) for (var e = 0; e < this.submenus.length; e++) this.submenus[e] != t && "collapsed" != this.submenus[e].className && this.collapseMenu(this.submenus[e])
}, SDMenu.prototype.expandAll = function () {
    var t = this.oneSmOnly;
    this.oneSmOnly = !1;
    for (var e = 0; e < this.submenus.length; e++) "collapsed" == this.submenus[e].className && this.expandMenu(this.submenus[e]);
    this.oneSmOnly = t
}, SDMenu.prototype.collapseAll = function () {
    for (var t = 0; t < this.submenus.length; t++) "collapsed" != this.submenus[t].className && this.collapseMenu(this.submenus[t])
}, SDMenu.prototype.memorize = function () {
    if (this.remember) {
        for (var t = new Array, e = 0; e < this.submenus.length; e++) t.push("collapsed" == this.submenus[e].className ? 0 : 1);
        var i = new Date;
        i.setTime(i.getTime() + 2592e6), document.cookie = "sdmenu_" + encodeURIComponent(this.menu.id) + "=" + t.join("") + "; expires=" + i.toGMTString() + "; path=/"
    }
}, function (a) {
    a.alerts = {
        verticalOffset: -75,
        horizontalOffset: 0,
        repositionOnResize: !0,
        overlayOpacity: .01,
        overlayColor: "#FFF",
        draggable: !0,
        okButton: "&nbsp;\u786e\u5b9a&nbsp;",
        cancelButton: "&nbsp;\u53d6\u6d88&nbsp;",
        dialogClass: null,
        alert: function (t, e, i) {
            null == e && (e = "Alert"), a.alerts._show(e, t, null, "alert", function (t) {
                i && i(t)
            })
        },
        confirm: function (t, e, i) {
            null == e && (e = "Confirm"), a.alerts._show(e, t, null, "confirm", function (t) {
                i && i(t)
            })
        },
        prompt: function (t, e, i, n) {
            null == i && (i = "Prompt"), a.alerts._show(i, t, e, "prompt", function (t) {
                n && n(t)
            })
        },
        _show: function (t, e, i, n, r) {
            a.alerts._hide(), a.alerts._overlay("show"), a("BODY").append('<div id="popup_container"><h1 id="popup_title"></h1><div id="popup_content"><div id="popup_message"></div></div></div>'), a.alerts.dialogClass && a("#popup_container").addClass(a.alerts.dialogClass);
            var o = "undefined" == typeof document.body.style.maxHeight ? "absolute" : "fixed";
            switch (a("#popup_container").css({
                position: o,
                zIndex: 99999,
                padding: 0,
                margin: 0
            }), a("#popup_title").text(t), a("#popup_content").addClass(n), a("#popup_message").text(e), a("#popup_message").html(a("#popup_message").text().replace(/\n/g, "<br />")), a("#popup_container").css({
                minWidth: a("#popup_container").outerWidth(),
                maxWidth: a("#popup_container").outerWidth()
            }), a.alerts._reposition(), a.alerts._maintainPosition(!0), n) {
                case"alert":
                    a("#popup_message").after('<div id="popup_panel"><input type="button" value="' + a.alerts.okButton + '" id="popup_ok" /></div>'), a("#popup_ok").click(function () {
                        a.alerts._hide(), r(!0)
                    }), a("#popup_ok").focus().keypress(function (t) {
                        13 != t.keyCode && 27 != t.keyCode || a("#popup_ok").trigger("click")
                    });
                    break;
                case"confirm":
                    a("#popup_message").after('<div id="popup_panel"><input type="button" value="' + a.alerts.okButton + '" id="popup_ok" /> <input type="button" value="' + a.alerts.cancelButton + '" id="popup_cancel" /></div>'), a("#popup_ok").click(function () {
                        a.alerts._hide(), r && r(!0)
                    }), a("#popup_cancel").click(function () {
                        a.alerts._hide(), r && r(!1)
                    }), a("#popup_ok").focus(), a("#popup_ok, #popup_cancel").keypress(function (t) {
                        13 == t.keyCode && a("#popup_ok").trigger("click"), 27 == t.keyCode && a("#popup_cancel").trigger("click")
                    });
                    break;
                case"prompt":
                    a("#popup_message").append('<br /><input type="text" size="30" id="popup_prompt" />').after('<div id="popup_panel"><input type="button" value="' + a.alerts.okButton + '" id="popup_ok" /> <input type="button" value="' + a.alerts.cancelButton + '" id="popup_cancel" /></div>'), a("#popup_prompt").width(a("#popup_message").width()), a("#popup_ok").click(function () {
                        var t = a("#popup_prompt").val();
                        a.alerts._hide(), r && r(t)
                    }), a("#popup_cancel").click(function () {
                        a.alerts._hide(), r && r(null)
                    }), a("#popup_prompt, #popup_ok, #popup_cancel").keypress(function (t) {
                        13 == t.keyCode && a("#popup_ok").trigger("click"), 27 == t.keyCode && a("#popup_cancel").trigger("click")
                    }), i && a("#popup_prompt").val(i), a("#popup_prompt").focus().select()
            }
            if (a.alerts.draggable) try {
                a("#popup_container").draggable({handle: a("#popup_title")}), a("#popup_title").css({cursor: "move"})
            } catch (s) {
            }
        },
        _hide: function () {
            a("#popup_container").remove(), a.alerts._overlay("hide"), a.alerts._maintainPosition(!1)
        },
        _overlay: function (t) {
            switch (t) {
                case"show":
                    a.alerts._overlay("hide"), a("BODY").append('<div id="popup_overlay"></div>'), a("#popup_overlay").css({
                        position: "absolute",
                        zIndex: 99998,
                        top: "0px",
                        left: "0px",
                        width: "100%",
                        height: a(document).height(),
                        background: a.alerts.overlayColor,
                        opacity: a.alerts.overlayOpacity
                    });
                    break;
                case"hide":
                    a("#popup_overlay").remove()
            }
        },
        _reposition: function () {
            var t = a(window).height() / 2 - a("#popup_container").outerHeight() / 2 + a.alerts.verticalOffset,
                e = a(window).width() / 2 - a("#popup_container").outerWidth() / 2 + a.alerts.horizontalOffset;
            t < 0 && (t = 0), e < 0 && (e = 0), "undefined" == typeof document.body.style.maxHeight && (t += a(window).scrollTop()), a("#popup_container").css({
                top: t + "px",
                left: e + "px"
            }), a("#popup_overlay").height(a(document).height())
        },
        _maintainPosition: function (t) {
            if (a.alerts.repositionOnResize) switch (t) {
                case!0:
                    a(window).bind("resize", function () {
                        a.alerts._reposition()
                    });
                    break;
                case!1:
                    a(window).unbind("resize")
            }
        }
    }, jAlert = function (t, e, i) {
        a.alerts.alert(t, e, i)
    }, jConfirm = function (t, e, i) {
        a.alerts.confirm(t, e, i)
    }, jPrompt = function (t, e, i, n) {
        a.alerts.prompt(t, e, i, n)
    }
}(jQuery), $(function () {
    $(".jqzoom").jqueryzoom({xzoom: 520, yzoom: 520})
}), $(function () {
    var t = 0, e = 3, i = 2, n = 300, r = $(".spec-scroll .items ul"), o = $(".spec-scroll .items ul li"),
        s = o.eq(0).width() * i, a = (o.length - e) * o.eq(0).width();
    $(".spec-scroll .next").bind("click", function () {
        t < a && (s < a - t ? (r.animate({left: "-=" + s + "px"}, n), t += s) : (r.animate({left: "-=" + (a - t) + "px"}, n), t += a - t))
    }), $(".spec-scroll .prev").bind("click", function () {
        0 < t && (s < t ? (r.animate({left: "+=" + s + "px"}, n), t -= s) : (r.animate({left: "+=" + t + "px"}, n), t = 0))
    })
}), $(function () {
    function t() {
        for (var t = 0; t <= n; t++) l += '<li class="pointer"></li>';
        $("#slides").after(a + l + c)
    }

    function e() {
        var t = $(this).index();
        $("#slides li").eq(r).css("z-index", "1"), $("#slides li").eq(t).css({"z-index": "1"}).show(), h.eq(t).addClass("current").siblings("li").removeClass("current"), $("#slides li").eq(r).fadeOut(400, function () {
            $("#slides li").eq(t).fadeIn(500)
        }), r = t
    }

    function i() {
        var t = r + 1;
        1 == o || (r < n ? ($("#slides li").eq(r).css("z-index", "1"), $("#slides li").eq(t).css({"z-index": "1"}).show(), h.eq(t).addClass("current").siblings("li").removeClass("current"), $("#slides li").eq(r).fadeOut(400, function () {
            $("#slides li").eq(t).fadeIn(500)
        }), r += 1) : (t = 0, $("#slides li").eq(r).css("z-index", "1"), $("#slides li").eq(t).stop(!0, !0).css({"z-index": "1"}).show(), $("#slides li").eq(r).fadeOut(400, function () {
            $("#slides li").eq(0).fadeIn(500)
        }), h.eq(t).addClass("current").siblings("li").removeClass("current"), r = 0)), setTimeout(i, s)
    }

    var n = $("#slides li").size() - 1, r = 0, o = 0, s = 8e3;
    $("#slides li").eq(0).siblings("li").css({display: "none"});
    var a = '<ul id="pagination">', l = "", c = "</ul>";
    t();
    var h = $("#pagination li"), d = $("#pagination").width();
    $("#pagination").css("margin-left", 470 - d), h.eq(0).addClass("current"), h.on("click", e), h.mouseenter(function () {
        o = 1
    }), h.mouseleave(function () {
        o = 0
    }), setTimeout(i, s)
}), function (a, n, r, l) {
    var c = a(n);
    a.fn.lazyload = function (t) {
        function e() {
            var e = 0;
            o.each(function () {
                var t = a(this);
                if (!s.skip_invisible || t.is(":visible")) if (a.abovethetop(this, s) || a.leftofbegin(this, s)) ; else if (a.belowthefold(this, s) || a.rightoffold(this, s)) {
                    if (++e > s.failure_limit) return !1
                } else t.trigger("appear"), e = 0
            })
        }

        var i, o = this, s = {
            threshold: 0,
            failure_limit: 0,
            event: "scroll",
            effect: "show",
            container: n,
            data_attribute: "original",
            skip_invisible: !0,
            appear: null,
            load: null,
            placeholder: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
        };
        return t && (l !== t.failurelimit && (t.failure_limit = t.failurelimit, delete t.failurelimit), l !== t.effectspeed && (t.effect_speed = t.effectspeed, delete t.effectspeed), a.extend(s, t)), i = s.container === l || s.container === n ? c : a(s.container), 0 === s.event.indexOf("scroll") && i.bind(s.event, function () {
            return e()
        }), this.each(function () {
            var n = this, r = a(n);
            n.loaded = !1, (r.attr("src") === l || !1 === r.attr("src")) && r.is("img") && r.attr("src", s.placeholder), r.one("appear", function () {
                if (!this.loaded) {
                    if (s.appear) {
                        var t = o.length;
                        s.appear.call(n, t, s)
                    }
                    a("<img />").bind("load", function () {
                        var t = r.attr("data-" + s.data_attribute);
                        r.hide(), r.is("img") ? r.attr("src", t) : r.css("background-image", "url('" + t + "')"), r[s.effect](s.effect_speed), n.loaded = !0;
                        var e = a.grep(o, function (t) {
                            return !t.loaded
                        });
                        if (o = a(e), s.load) {
                            var i = o.length;
                            s.load.call(n, i, s)
                        }
                    }).attr("src", r.attr("data-" + s.data_attribute))
                }
            }), 0 !== s.event.indexOf("scroll") && r.bind(s.event, function () {
                n.loaded || r.trigger("appear")
            })
        }), c.bind("resize", function () {
            e()
        }), /(?:iphone|ipod|ipad).*os 5/gi.test(navigator.appVersion) && c.bind("pageshow", function (t) {
            t.originalEvent && t.originalEvent.persisted && o.each(function () {
                a(this).trigger("appear")
            })
        }), a(r).ready(function () {
            e()
        }), this
    }, a.belowthefold = function (t, e) {
        return (e.container === l || e.container === n ? (n.innerHeight ? n.innerHeight : c.height()) + c.scrollTop() : a(e.container).offset().top + a(e.container).height()) <= a(t).offset().top - e.threshold
    }, a.rightoffold = function (t, e) {
        return (e.container === l || e.container === n ? c.width() + c.scrollLeft() : a(e.container).offset().left + a(e.container).width()) <= a(t).offset().left - e.threshold
    }, a.abovethetop = function (t, e) {
        return (e.container === l || e.container === n ? c.scrollTop() : a(e.container).offset().top) >= a(t).offset().top + e.threshold + a(t).height()
    }, a.leftofbegin = function (t, e) {
        return (e.container === l || e.container === n ? c.scrollLeft() : a(e.container).offset().left) >= a(t).offset().left + e.threshold + a(t).width()
    }, a.inviewport = function (t, e) {
        return !(a.rightoffold(t, e) || a.leftofbegin(t, e) || a.belowthefold(t, e) || a.abovethetop(t, e))
    }, a.extend(a.expr[":"], {
        "below-the-fold": function (t) {
            return a.belowthefold(t, {threshold: 0})
        }, "above-the-top": function (t) {
            return !a.belowthefold(t, {threshold: 0})
        }, "right-of-screen": function (t) {
            return a.rightoffold(t, {threshold: 0})
        }, "left-of-screen": function (t) {
            return !a.rightoffold(t, {threshold: 0})
        }, "in-viewport": function (t) {
            return a.inviewport(t, {threshold: 0})
        }, "above-the-fold": function (t) {
            return !a.belowthefold(t, {threshold: 0})
        }, "right-of-fold": function (t) {
            return a.rightoffold(t, {threshold: 0})
        }, "left-of-fold": function (t) {
            return !a.rightoffold(t, {threshold: 0})
        }
    })
}(jQuery, window, document), function (f) {
    f.fn.qrcode = function (c) {
        function e(t) {
            this.mode = i, this.data = t
        }

        function h(t, e) {
            this.typeNumber = t, this.errorCorrectLevel = e, this.modules = null, this.moduleCount = 0, this.dataCache = null, this.dataList = []
        }

        function d(t, e) {
            if (null == t.length) throw Error(t.length + "/" + e);
            for (var i = 0; i < t.length && 0 == t[i];) i++;
            this.num = Array(t.length - i + e);
            for (var n = 0; n < t.length - i; n++) this.num[n] = t[n + i]
        }

        function u(t, e) {
            this.totalCount = t, this.dataCount = e
        }

        function s() {
            this.buffer = [], this.length = 0
        }

        var i;
        e.prototype = {
            getLength: function () {
                return this.data.length
            }, write: function (t) {
                for (var e = 0; e < this.data.length; e++) t.put(this.data.charCodeAt(e), 8)
            }
        }, h.prototype = {
            addData: function (t) {
                this.dataList.push(new e(t)), this.dataCache = null
            }, isDark: function (t, e) {
                if (t < 0 || this.moduleCount <= t || e < 0 || this.moduleCount <= e) throw Error(t + "," + e);
                return this.modules[t][e]
            }, getModuleCount: function () {
                return this.moduleCount
            }, make: function () {
                if (this.typeNumber < 1) {
                    var t = 1;
                    for (t = 1; t < 40; t++) {
                        for (var e = u.getRSBlocks(t, this.errorCorrectLevel), i = new s, n = 0, r = 0; r < e.length; r++) n += e[r].dataCount;
                        for (r = 0; r < this.dataList.length; r++) e = this.dataList[r], i.put(e.mode, 4), i.put(e.getLength(), p.getLengthInBits(e.mode, t)), e.write(i);
                        if (i.getLengthInBits() <= 8 * n) break
                    }
                    this.typeNumber = t
                }
                this.makeImpl(!1, this.getBestMaskPattern())
            }, makeImpl: function (t, e) {
                this.moduleCount = 4 * this.typeNumber + 17, this.modules = Array(this.moduleCount);
                for (var i = 0; i < this.moduleCount; i++) {
                    this.modules[i] = Array(this.moduleCount);
                    for (var n = 0; n < this.moduleCount; n++) this.modules[i][n] = null
                }
                this.setupPositionProbePattern(0, 0), this.setupPositionProbePattern(this.moduleCount - 7, 0), this.setupPositionProbePattern(0, this.moduleCount - 7), this.setupPositionAdjustPattern(), this.setupTimingPattern(), this.setupTypeInfo(t, e), 7 <= this.typeNumber && this.setupTypeNumber(t), null == this.dataCache && (this.dataCache = h.createData(this.typeNumber, this.errorCorrectLevel, this.dataList)), this.mapData(this.dataCache, e)
            }, setupPositionProbePattern: function (t, e) {
                for (var i = -1; i <= 7; i++) if (!(t + i <= -1 || this.moduleCount <= t + i)) for (var n = -1; n <= 7; n++) e + n <= -1 || this.moduleCount <= e + n || (this.modules[t + i][e + n] = 0 <= i && i <= 6 && (0 == n || 6 == n) || 0 <= n && n <= 6 && (0 == i || 6 == i) || 2 <= i && i <= 4 && 2 <= n && n <= 4)
            }, getBestMaskPattern: function () {
                for (var t = 0, e = 0, i = 0; i < 8; i++) {
                    this.makeImpl(!0, i);
                    var n = p.getLostPoint(this);
                    (0 == i || n < t) && (t = n, e = i)
                }
                return e
            }, createMovieClip: function (t, e, i) {
                for (t = t.createEmptyMovieClip(e, i), this.make(), e = 0; e < this.modules.length; e++) {
                    i = 1 * e;
                    for (var n = 0; n < this.modules[e].length; n++) {
                        var r = 1 * n;
                        this.modules[e][n] && (t.beginFill(0, 100), t.moveTo(r, i), t.lineTo(r + 1, i), t.lineTo(r + 1, i + 1), t.lineTo(r, i + 1), t.endFill())
                    }
                }
                return t
            }, setupTimingPattern: function () {
                for (var t = 8; t < this.moduleCount - 8; t++) null == this.modules[t][6] && (this.modules[t][6] = 0 == t % 2);
                for (t = 8; t < this.moduleCount - 8; t++) null == this.modules[6][t] && (this.modules[6][t] = 0 == t % 2)
            }, setupPositionAdjustPattern: function () {
                for (var t = p.getPatternPosition(this.typeNumber), e = 0; e < t.length; e++) for (var i = 0; i < t.length; i++) {
                    var n = t[e], r = t[i];
                    if (null == this.modules[n][r]) for (var o = -2; o <= 2; o++) for (var s = -2; s <= 2; s++) this.modules[n + o][r + s] = -2 == o || 2 == o || -2 == s || 2 == s || 0 == o && 0 == s
                }
            }, setupTypeNumber: function (t) {
                for (var e = p.getBCHTypeNumber(this.typeNumber), i = 0; i < 18; i++) {
                    var n = !t && 1 == (e >> i & 1);
                    this.modules[Math.floor(i / 3)][i % 3 + this.moduleCount - 8 - 3] = n
                }
                for (i = 0; i < 18; i++) n = !t && 1 == (e >> i & 1), this.modules[i % 3 + this.moduleCount - 8 - 3][Math.floor(i / 3)] = n
            }, setupTypeInfo: function (t, e) {
                for (var i = p.getBCHTypeInfo(this.errorCorrectLevel << 3 | e), n = 0; n < 15; n++) {
                    var r = !t && 1 == (i >> n & 1);
                    n < 6 ? this.modules[n][8] = r : n < 8 ? this.modules[n + 1][8] = r : this.modules[this.moduleCount - 15 + n][8] = r
                }
                for (n = 0; n < 15; n++) r = !t && 1 == (i >> n & 1),
                    n < 8 ? this.modules[8][this.moduleCount - n - 1] = r : n < 9 ? this.modules[8][15 - n - 1 + 1] = r : this.modules[8][15 - n - 1] = r;
                this.modules[this.moduleCount - 8][8] = !t
            }, mapData: function (t, e) {
                for (var i = -1, n = this.moduleCount - 1, r = 7, o = 0, s = this.moduleCount - 1; 0 < s; s -= 2) for (6 == s && s--; ;) {
                    for (var a = 0; a < 2; a++) if (null == this.modules[n][s - a]) {
                        var l = !1;
                        o < t.length && (l = 1 == (t[o] >>> r & 1)), p.getMask(e, n, s - a) && (l = !l), this.modules[n][s - a] = l, -1 == --r && (o++, r = 7)
                    }
                    if ((n += i) < 0 || this.moduleCount <= n) {
                        n -= i, i = -i;
                        break
                    }
                }
            }
        }, h.PAD0 = 236, h.PAD1 = 17, h.createData = function (t, e, i) {
            e = u.getRSBlocks(t, e);
            for (var n = new s, r = 0; r < i.length; r++) {
                var o = i[r];
                n.put(o.mode, 4), n.put(o.getLength(), p.getLengthInBits(o.mode, t)), o.write(n)
            }
            for (r = t = 0; r < e.length; r++) t += e[r].dataCount;
            if (n.getLengthInBits() > 8 * t) throw Error("code length overflow. (" + n.getLengthInBits() + ">" + 8 * t + ")");
            for (n.getLengthInBits() + 4 <= 8 * t && n.put(0, 4); 0 != n.getLengthInBits() % 8;) n.putBit(!1);
            for (; !(n.getLengthInBits() >= 8 * t) && (n.put(h.PAD0, 8), !(n.getLengthInBits() >= 8 * t));) n.put(h.PAD1, 8);
            return h.createBytes(n, e)
        }, h.createBytes = function (t, e) {
            for (var i = 0, n = 0, r = 0, o = Array(e.length), s = Array(e.length), a = 0; a < e.length; a++) {
                var l = e[a].dataCount, c = e[a].totalCount - l;
                n = Math.max(n, l), r = Math.max(r, c);
                o[a] = Array(l);
                for (var h = 0; h < o[a].length; h++) o[a][h] = 255 & t.buffer[h + i];
                for (i += l, h = p.getErrorCorrectPolynomial(c), l = new d(o[a], h.getLength() - 1).mod(h), s[a] = Array(h.getLength() - 1), h = 0; h < s[a].length; h++) c = h + l.getLength() - s[a].length, s[a][h] = 0 <= c ? l.get(c) : 0
            }
            for (h = a = 0; h < e.length; h++) a += e[h].totalCount;
            for (i = Array(a), h = l = 0; h < n; h++) for (a = 0; a < e.length; a++) h < o[a].length && (i[l++] = o[a][h]);
            for (h = 0; h < r; h++) for (a = 0; a < e.length; a++) h < s[a].length && (i[l++] = s[a][h]);
            return i
        }, i = 4;
        for (var p = {
            PATTERN_POSITION_TABLE: [[], [6, 18], [6, 22], [6, 26], [6, 30], [6, 34], [6, 22, 38], [6, 24, 42], [6, 26, 46], [6, 28, 50], [6, 30, 54], [6, 32, 58], [6, 34, 62], [6, 26, 46, 66], [6, 26, 48, 70], [6, 26, 50, 74], [6, 30, 54, 78], [6, 30, 56, 82], [6, 30, 58, 86], [6, 34, 62, 90], [6, 28, 50, 72, 94], [6, 26, 50, 74, 98], [6, 30, 54, 78, 102], [6, 28, 54, 80, 106], [6, 32, 58, 84, 110], [6, 30, 58, 86, 114], [6, 34, 62, 90, 118], [6, 26, 50, 74, 98, 122], [6, 30, 54, 78, 102, 126], [6, 26, 52, 78, 104, 130], [6, 30, 56, 82, 108, 134], [6, 34, 60, 86, 112, 138], [6, 30, 58, 86, 114, 142], [6, 34, 62, 90, 118, 146], [6, 30, 54, 78, 102, 126, 150], [6, 24, 50, 76, 102, 128, 154], [6, 28, 54, 80, 106, 132, 158], [6, 32, 58, 84, 110, 136, 162], [6, 26, 54, 82, 110, 138, 166], [6, 30, 58, 86, 114, 142, 170]],
            G15: 1335,
            G18: 7973,
            G15_MASK: 21522,
            getBCHTypeInfo: function (t) {
                for (var e = t << 10; 0 <= p.getBCHDigit(e) - p.getBCHDigit(p.G15);) e ^= p.G15 << p.getBCHDigit(e) - p.getBCHDigit(p.G15);
                return (t << 10 | e) ^ p.G15_MASK
            },
            getBCHTypeNumber: function (t) {
                for (var e = t << 12; 0 <= p.getBCHDigit(e) - p.getBCHDigit(p.G18);) e ^= p.G18 << p.getBCHDigit(e) - p.getBCHDigit(p.G18);
                return t << 12 | e
            },
            getBCHDigit: function (t) {
                for (var e = 0; 0 != t;) e++, t >>>= 1;
                return e
            },
            getPatternPosition: function (t) {
                return p.PATTERN_POSITION_TABLE[t - 1]
            },
            getMask: function (t, e, i) {
                switch (t) {
                    case 0:
                        return 0 == (e + i) % 2;
                    case 1:
                        return 0 == e % 2;
                    case 2:
                        return 0 == i % 3;
                    case 3:
                        return 0 == (e + i) % 3;
                    case 4:
                        return 0 == (Math.floor(e / 2) + Math.floor(i / 3)) % 2;
                    case 5:
                        return 0 == e * i % 2 + e * i % 3;
                    case 6:
                        return 0 == (e * i % 2 + e * i % 3) % 2;
                    case 7:
                        return 0 == (e * i % 3 + (e + i) % 2) % 2;
                    default:
                        throw Error("bad maskPattern:" + t)
                }
            },
            getErrorCorrectPolynomial: function (t) {
                for (var e = new d([1], 0), i = 0; i < t; i++) e = e.multiply(new d([1, r.gexp(i)], 0));
                return e
            },
            getLengthInBits: function (t, e) {
                if (1 <= e && e < 10) switch (t) {
                    case 1:
                        return 10;
                    case 2:
                        return 9;
                    case i:
                    case 8:
                        return 8;
                    default:
                        throw Error("mode:" + t)
                } else if (e < 27) switch (t) {
                    case 1:
                        return 12;
                    case 2:
                        return 11;
                    case i:
                        return 16;
                    case 8:
                        return 10;
                    default:
                        throw Error("mode:" + t)
                } else {
                    if (!(e < 41)) throw Error("type:" + e);
                    switch (t) {
                        case 1:
                            return 14;
                        case 2:
                            return 13;
                        case i:
                            return 16;
                        case 8:
                            return 12;
                        default:
                            throw Error("mode:" + t)
                    }
                }
            },
            getLostPoint: function (t) {
                for (var e = t.getModuleCount(), i = 0, n = 0; n < e; n++) for (var r = 0; r < e; r++) {
                    for (var o = 0, s = t.isDark(n, r), a = -1; a <= 1; a++) if (!(n + a < 0 || e <= n + a)) for (var l = -1; l <= 1; l++) r + l < 0 || e <= r + l || 0 == a && 0 == l || s == t.isDark(n + a, r + l) && o++;
                    5 < o && (i += 3 + o - 5)
                }
                for (n = 0; n < e - 1; n++) for (r = 0; r < e - 1; r++) o = 0, t.isDark(n, r) && o++, t.isDark(n + 1, r) && o++, t.isDark(n, r + 1) && o++, t.isDark(n + 1, r + 1) && o++, (0 == o || 4 == o) && (i += 3);
                for (n = 0; n < e; n++) for (r = 0; r < e - 6; r++) t.isDark(n, r) && !t.isDark(n, r + 1) && t.isDark(n, r + 2) && t.isDark(n, r + 3) && t.isDark(n, r + 4) && !t.isDark(n, r + 5) && t.isDark(n, r + 6) && (i += 40);
                for (r = 0; r < e; r++) for (n = 0; n < e - 6; n++) t.isDark(n, r) && !t.isDark(n + 1, r) && t.isDark(n + 2, r) && t.isDark(n + 3, r) && t.isDark(n + 4, r) && !t.isDark(n + 5, r) && t.isDark(n + 6, r) && (i += 40);
                for (r = o = 0; r < e; r++) for (n = 0; n < e; n++) t.isDark(n, r) && o++;
                return i + 10 * (t = Math.abs(100 * o / e / e - 50) / 5)
            }
        }, r = {
            glog: function (t) {
                if (t < 1) throw Error("glog(" + t + ")");
                return r.LOG_TABLE[t]
            }, gexp: function (t) {
                for (; t < 0;) t += 255;
                for (; 256 <= t;) t -= 255;
                return r.EXP_TABLE[t]
            }, EXP_TABLE: Array(256), LOG_TABLE: Array(256)
        }, t = 0; t < 8; t++) r.EXP_TABLE[t] = 1 << t;
        for (t = 8; t < 256; t++) r.EXP_TABLE[t] = r.EXP_TABLE[t - 4] ^ r.EXP_TABLE[t - 5] ^ r.EXP_TABLE[t - 6] ^ r.EXP_TABLE[t - 8];
        for (t = 0; t < 255; t++) r.LOG_TABLE[r.EXP_TABLE[t]] = t;
        return d.prototype = {
            get: function (t) {
                return this.num[t]
            }, getLength: function () {
                return this.num.length
            }, multiply: function (t) {
                for (var e = Array(this.getLength() + t.getLength() - 1), i = 0; i < this.getLength(); i++) for (var n = 0; n < t.getLength(); n++) e[i + n] ^= r.gexp(r.glog(this.get(i)) + r.glog(t.get(n)));
                return new d(e, 0)
            }, mod: function (t) {
                if (this.getLength() - t.getLength() < 0) return this;
                for (var e = r.glog(this.get(0)) - r.glog(t.get(0)), i = Array(this.getLength()), n = 0; n < this.getLength(); n++) i[n] = this.get(n);
                for (n = 0; n < t.getLength(); n++) i[n] ^= r.gexp(r.glog(t.get(n)) + e);
                return new d(i, 0).mod(t)
            }
        }, u.RS_BLOCK_TABLE = [[1, 26, 19], [1, 26, 16], [1, 26, 13], [1, 26, 9], [1, 44, 34], [1, 44, 28], [1, 44, 22], [1, 44, 16], [1, 70, 55], [1, 70, 44], [2, 35, 17], [2, 35, 13], [1, 100, 80], [2, 50, 32], [2, 50, 24], [4, 25, 9], [1, 134, 108], [2, 67, 43], [2, 33, 15, 2, 34, 16], [2, 33, 11, 2, 34, 12], [2, 86, 68], [4, 43, 27], [4, 43, 19], [4, 43, 15], [2, 98, 78], [4, 49, 31], [2, 32, 14, 4, 33, 15], [4, 39, 13, 1, 40, 14], [2, 121, 97], [2, 60, 38, 2, 61, 39], [4, 40, 18, 2, 41, 19], [4, 40, 14, 2, 41, 15], [2, 146, 116], [3, 58, 36, 2, 59, 37], [4, 36, 16, 4, 37, 17], [4, 36, 12, 4, 37, 13], [2, 86, 68, 2, 87, 69], [4, 69, 43, 1, 70, 44], [6, 43, 19, 2, 44, 20], [6, 43, 15, 2, 44, 16], [4, 101, 81], [1, 80, 50, 4, 81, 51], [4, 50, 22, 4, 51, 23], [3, 36, 12, 8, 37, 13], [2, 116, 92, 2, 117, 93], [6, 58, 36, 2, 59, 37], [4, 46, 20, 6, 47, 21], [7, 42, 14, 4, 43, 15], [4, 133, 107], [8, 59, 37, 1, 60, 38], [8, 44, 20, 4, 45, 21], [12, 33, 11, 4, 34, 12], [3, 145, 115, 1, 146, 116], [4, 64, 40, 5, 65, 41], [11, 36, 16, 5, 37, 17], [11, 36, 12, 5, 37, 13], [5, 109, 87, 1, 110, 88], [5, 65, 41, 5, 66, 42], [5, 54, 24, 7, 55, 25], [11, 36, 12], [5, 122, 98, 1, 123, 99], [7, 73, 45, 3, 74, 46], [15, 43, 19, 2, 44, 20], [3, 45, 15, 13, 46, 16], [1, 135, 107, 5, 136, 108], [10, 74, 46, 1, 75, 47], [1, 50, 22, 15, 51, 23], [2, 42, 14, 17, 43, 15], [5, 150, 120, 1, 151, 121], [9, 69, 43, 4, 70, 44], [17, 50, 22, 1, 51, 23], [2, 42, 14, 19, 43, 15], [3, 141, 113, 4, 142, 114], [3, 70, 44, 11, 71, 45], [17, 47, 21, 4, 48, 22], [9, 39, 13, 16, 40, 14], [3, 135, 107, 5, 136, 108], [3, 67, 41, 13, 68, 42], [15, 54, 24, 5, 55, 25], [15, 43, 15, 10, 44, 16], [4, 144, 116, 4, 145, 117], [17, 68, 42], [17, 50, 22, 6, 51, 23], [19, 46, 16, 6, 47, 17], [2, 139, 111, 7, 140, 112], [17, 74, 46], [7, 54, 24, 16, 55, 25], [34, 37, 13], [4, 151, 121, 5, 152, 122], [4, 75, 47, 14, 76, 48], [11, 54, 24, 14, 55, 25], [16, 45, 15, 14, 46, 16], [6, 147, 117, 4, 148, 118], [6, 73, 45, 14, 74, 46], [11, 54, 24, 16, 55, 25], [30, 46, 16, 2, 47, 17], [8, 132, 106, 4, 133, 107], [8, 75, 47, 13, 76, 48], [7, 54, 24, 22, 55, 25], [22, 45, 15, 13, 46, 16], [10, 142, 114, 2, 143, 115], [19, 74, 46, 4, 75, 47], [28, 50, 22, 6, 51, 23], [33, 46, 16, 4, 47, 17], [8, 152, 122, 4, 153, 123], [22, 73, 45, 3, 74, 46], [8, 53, 23, 26, 54, 24], [12, 45, 15, 28, 46, 16], [3, 147, 117, 10, 148, 118], [3, 73, 45, 23, 74, 46], [4, 54, 24, 31, 55, 25], [11, 45, 15, 31, 46, 16], [7, 146, 116, 7, 147, 117], [21, 73, 45, 7, 74, 46], [1, 53, 23, 37, 54, 24], [19, 45, 15, 26, 46, 16], [5, 145, 115, 10, 146, 116], [19, 75, 47, 10, 76, 48], [15, 54, 24, 25, 55, 25], [23, 45, 15, 25, 46, 16], [13, 145, 115, 3, 146, 116], [2, 74, 46, 29, 75, 47], [42, 54, 24, 1, 55, 25], [23, 45, 15, 28, 46, 16], [17, 145, 115], [10, 74, 46, 23, 75, 47], [10, 54, 24, 35, 55, 25], [19, 45, 15, 35, 46, 16], [17, 145, 115, 1, 146, 116], [14, 74, 46, 21, 75, 47], [29, 54, 24, 19, 55, 25], [11, 45, 15, 46, 46, 16], [13, 145, 115, 6, 146, 116], [14, 74, 46, 23, 75, 47], [44, 54, 24, 7, 55, 25], [59, 46, 16, 1, 47, 17], [12, 151, 121, 7, 152, 122], [12, 75, 47, 26, 76, 48], [39, 54, 24, 14, 55, 25], [22, 45, 15, 41, 46, 16], [6, 151, 121, 14, 152, 122], [6, 75, 47, 34, 76, 48], [46, 54, 24, 10, 55, 25], [2, 45, 15, 64, 46, 16], [17, 152, 122, 4, 153, 123], [29, 74, 46, 14, 75, 47], [49, 54, 24, 10, 55, 25], [24, 45, 15, 46, 46, 16], [4, 152, 122, 18, 153, 123], [13, 74, 46, 32, 75, 47], [48, 54, 24, 14, 55, 25], [42, 45, 15, 32, 46, 16], [20, 147, 117, 4, 148, 118], [40, 75, 47, 7, 76, 48], [43, 54, 24, 22, 55, 25], [10, 45, 15, 67, 46, 16], [19, 148, 118, 6, 149, 119], [18, 75, 47, 31, 76, 48], [34, 54, 24, 34, 55, 25], [20, 45, 15, 61, 46, 16]], u.getRSBlocks = function (t, e) {
            var i = u.getRsBlockTable(t, e);
            if (null == i) throw Error("bad rs block @ typeNumber:" + t + "/errorCorrectLevel:" + e);
            for (var n = i.length / 3, r = [], o = 0; o < n; o++) for (var s = i[3 * o + 0], a = i[3 * o + 1], l = i[3 * o + 2], c = 0; c < s; c++) r.push(new u(a, l));
            return r
        }, u.getRsBlockTable = function (t, e) {
            switch (e) {
                case 1:
                    return u.RS_BLOCK_TABLE[4 * (t - 1) + 0];
                case 0:
                    return u.RS_BLOCK_TABLE[4 * (t - 1) + 1];
                case 3:
                    return u.RS_BLOCK_TABLE[4 * (t - 1) + 2];
                case 2:
                    return u.RS_BLOCK_TABLE[4 * (t - 1) + 3]
            }
        }, s.prototype = {
            get: function (t) {
                return 1 == (this.buffer[Math.floor(t / 8)] >>> 7 - t % 8 & 1)
            }, put: function (t, e) {
                for (var i = 0; i < e; i++) this.putBit(1 == (t >>> e - i - 1 & 1))
            }, getLengthInBits: function () {
                return this.length
            }, putBit: function (t) {
                var e = Math.floor(this.length / 8);
                this.buffer.length <= e && this.buffer.push(0), t && (this.buffer[e] |= 128 >>> this.length % 8), this.length++
            }
        }, "string" == typeof c && (c = {text: c}), c = f.extend({}, {
            render: "canvas",
            width: 256,
            height: 256,
            typeNumber: -1,
            correctLevel: 2,
            background: "#ffffff",
            foreground: "#000000"
        }, c), this.each(function () {
            var t;
            if ("canvas" == c.render) {
                (t = new h(c.typeNumber, c.correctLevel)).addData(c.text), t.make();
                var e = document.createElement("canvas");
                e.width = c.width, e.height = c.height;
                for (var i = e.getContext("2d"), n = c.width / t.getModuleCount(), r = c.height / t.getModuleCount(), o = 0; o < t.getModuleCount(); o++) for (var s = 0; s < t.getModuleCount(); s++) {
                    i.fillStyle = t.isDark(o, s) ? c.foreground : c.background;
                    var a = Math.ceil((s + 1) * n) - Math.floor(s * n), l = Math.ceil((o + 1) * n) - Math.floor(o * n);
                    i.fillRect(Math.round(s * n), Math.round(o * r), a, l)
                }
            } else for ((t = new h(c.typeNumber, c.correctLevel)).addData(c.text), t.make(), e = f("<table></table>").css("width", c.width + "px").css("height", c.height + "px").css("border", "0px").css("border-collapse", "collapse").css("background-color", c.background), i = c.width / t.getModuleCount(), n = c.height / t.getModuleCount(), r = 0; r < t.getModuleCount(); r++) for (o = f("<tr></tr>").css("height", n + "px").appendTo(e), s = 0; s < t.getModuleCount(); s++) f("<td></td>").css("width", i + "px").css("background-color", t.isDark(r, s) ? c.foreground : c.background).appendTo(o);
            t = e, jQuery(t).appendTo(this)
        })
    }
}(jQuery);
var Swiper = function (t, w) {
    "use strict";

    function a(t, e) {
        return document.querySelectorAll ? (e || document).querySelectorAll(t) : jQuery(t, e)
    }

    function n(t) {
        return "[object Array]" === Object.prototype.toString.apply(t)
    }

    function m() {
        var t = P - F;
        return w.freeMode && (t = P - F), w.slidesPerView > D.slides.length && !w.centeredSlides && (t = 0), t < 0 && (t = 0), t
    }

    function e() {
        function t(t) {
            var e, i, n = function () {
                null != D && (D.imagesLoaded !== undefined && D.imagesLoaded++, D.imagesLoaded === D.imagesToLoad.length && (D.reInit(), w.onImagesReady && D.fireCallback(w.onImagesReady, D)))
            };
            t.complete ? n() : (i = t.currentSrc || t.getAttribute("src")) ? ((e = new Image).onload = n, e.onerror = n, e.src = i) : n()
        }

        var e = D.h.addEventListener, i = "wrapper" === w.eventTarget ? D.wrapper : D.container;
        if (D.browser.ie10 || D.browser.ie11 ? (e(i, D.touchEvents.touchStart, p), e(document, D.touchEvents.touchMove, f), e(document, D.touchEvents.touchEnd, g)) : (D.support.touch && (e(i, "touchstart", p), e(i, "touchmove", f), e(i, "touchend", g)), w.simulateTouch && (e(i, "mousedown", p), e(document, "mousemove", f), e(document, "mouseup", g))), w.autoResize && e(window, "resize", D.resizeFix), o(), D._wheelEvent = !1, w.mousewheelControl) {
            if (document.onmousewheel !== undefined && (D._wheelEvent = "mousewheel"), !D._wheelEvent) try {
                new WheelEvent("wheel"), D._wheelEvent = "wheel"
            } catch (r) {
            }
            D._wheelEvent || (D._wheelEvent = "DOMMouseScroll"), D._wheelEvent && e(D.container, D._wheelEvent, c)
        }
        if (w.keyboardControl && e(document, "keydown", l), w.updateOnImagesReady) {
            D.imagesToLoad = a("img", D.container);
            for (var n = 0; n < D.imagesToLoad.length; n++) t(D.imagesToLoad[n])
        }
    }

    function o() {
        var t, e = D.h.addEventListener;
        if (w.preventLinks) {
            var i = a("a", D.container);
            for (t = 0; t < i.length; t++) e(i[t], "click", d)
        }
        if (w.releaseFormElements) {
            var n = a("input, textarea, select", D.container);
            for (t = 0; t < n.length; t++) e(n[t], D.touchEvents.touchStart, u, !0), D.support.touch && w.simulateTouch && e(n[t], "mousedown", u, !0)
        }
        if (w.onSlideClick) for (t = 0; t < D.slides.length; t++) e(D.slides[t], "click", r);
        if (w.onSlideTouch) for (t = 0; t < D.slides.length; t++) e(D.slides[t], D.touchEvents.touchStart, h)
    }

    function s() {
        var t, e = D.h.removeEventListener;
        if (w.onSlideClick) for (t = 0; t < D.slides.length; t++) e(D.slides[t], "click", r);
        if (w.onSlideTouch) for (t = 0; t < D.slides.length; t++) e(D.slides[t], D.touchEvents.touchStart, h);
        if (w.releaseFormElements) {
            var i = a("input, textarea, select", D.container);
            for (t = 0; t < i.length; t++) e(i[t], D.touchEvents.touchStart, u, !0), D.support.touch && w.simulateTouch && e(i[t], "mousedown", u, !0)
        }
        if (w.preventLinks) {
            var n = a("a", D.container);
            for (t = 0; t < n.length; t++) e(n[t], "click", d)
        }
    }

    function l(t) {
        var e = t.keyCode || t.charCode;
        if (!(t.shiftKey || t.altKey || t.ctrlKey || t.metaKey)) {
            if (37 === e || 39 === e || 38 === e || 40 === e) {
                for (var i = !1, n = D.h.getOffset(D.container), r = D.h.windowScroll().left, o = D.h.windowScroll().top, s = D.h.windowWidth(), a = D.h.windowHeight(), l = [[n.left, n.top], [n.left + D.width, n.top], [n.left, n.top + D.height], [n.left + D.width, n.top + D.height]], c = 0; c < l.length; c++) {
                    var h = l[c];
                    h[0] >= r && h[0] <= r + s && h[1] >= o && h[1] <= o + a && (i = !0)
                }
                if (!i) return
            }
            z ? (37 !== e && 39 !== e || (t.preventDefault ? t.preventDefault() : t.returnValue = !1), 39 === e && D.swipeNext(), 37 === e && D.swipePrev()) : (38 !== e && 40 !== e || (t.preventDefault ? t.preventDefault() : t.returnValue = !1), 40 === e && D.swipeNext(), 38 === e && D.swipePrev())
        }
    }

    function c(t) {
        var e = D._wheelEvent, i = 0;
        if (t.detail) i = -t.detail; else if ("mousewheel" === e) if (w.mousewheelControlForceToAxis) if (z) {
            if (!(Math.abs(t.wheelDeltaX) > Math.abs(t.wheelDeltaY))) return;
            i = t.wheelDeltaX
        } else {
            if (!(Math.abs(t.wheelDeltaY) > Math.abs(t.wheelDeltaX))) return;
            i = t.wheelDeltaY
        } else i = t.wheelDelta; else if ("DOMMouseScroll" === e) i = -t.detail; else if ("wheel" === e) if (w.mousewheelControlForceToAxis) if (z) {
            if (!(Math.abs(t.deltaX) > Math.abs(t.deltaY))) return;
            i = -t.deltaX
        } else {
            if (!(Math.abs(t.deltaY) > Math.abs(t.deltaX))) return;
            i = -t.deltaY
        } else i = Math.abs(t.deltaX) > Math.abs(t.deltaY) ? -t.deltaX : -t.deltaY;
        if (w.freeMode) {
            var n = D.getWrapperTranslate() + i;
            if (0 < n && (n = 0), n < -m() && (n = -m()), D.setWrapperTransition(0), D.setWrapperTranslate(n), D.updateActiveSlide(n), 0 === n || n === -m()) return
        } else 60 < (new Date).getTime() - Y && (i < 0 ? D.swipeNext() : D.swipePrev()), Y = (new Date).getTime();
        return w.autoplay && D.stopAutoplay(!0), t.preventDefault ? t.preventDefault() : t.returnValue = !1, !1
    }

    function r(t) {
        D.allowSlideClick && (i(t), D.fireCallback(w.onSlideClick, D, t))
    }

    function h(t) {
        i(t), D.fireCallback(w.onSlideTouch, D, t)
    }

    function i(t) {
        if (t.currentTarget) D.clickedSlide = t.currentTarget; else {
            var e = t.srcElement;
            do {
                if (-1 < e.className.indexOf(w.slideClass)) break;
                e = e.parentNode
            } while (e);
            D.clickedSlide = e
        }
        D.clickedSlideIndex = D.slides.indexOf(D.clickedSlide), D.clickedSlideLoopIndex = D.clickedSlideIndex - (D.loopedSlides || 0)
    }

    function d(t) {
        if (!D.allowLinks) return t.preventDefault ? t.preventDefault() : t.returnValue = !1, w.preventLinksPropagation && "stopPropagation" in t && t.stopPropagation(), !1
    }

    function u(t) {
        return t.stopPropagation ? t.stopPropagation() : t.returnValue = !1, !1
    }

    function p(t) {
        if (w.preventLinks && (D.allowLinks = !0), D.isTouched || w.onlyExternal) return !1;
        var e = t.target || t.srcElement;
        document.activeElement && document.activeElement !== document.body && document.activeElement !== e && document.activeElement.blur();
        var i = "input select textarea".split(" ");
        if (w.noSwiping && e && y(e)) return !1;
        if (et = !1, D.isTouched = !0, !(tt = "touchstart" === t.type) && "which" in t && 3 === t.which) return D.isTouched = !1;
        if (!tt || 1 === t.targetTouches.length) {
            D.callPlugins("onTouchStartBegin"), !tt && !D.isAndroid && i.indexOf(e.tagName.toLowerCase()) < 0 && (t.preventDefault ? t.preventDefault() : t.returnValue = !1);
            var n = tt ? t.targetTouches[0].pageX : t.pageX || t.clientX,
                r = tt ? t.targetTouches[0].pageY : t.pageY || t.clientY;
            D.touches.startX = D.touches.currentX = n, D.touches.startY = D.touches.currentY = r, D.touches.start = D.touches.current = z ? n : r, D.setWrapperTransition(0), D.positions.start = D.positions.current = D.getWrapperTranslate(), D.setWrapperTranslate(D.positions.start), D.times.start = (new Date).getTime(), I = undefined, 0 < w.moveStartThreshold && (U = !1), w.onTouchStart && D.fireCallback(w.onTouchStart, D, t), D.callPlugins("onTouchStartEnd")
        }
    }

    function f(t) {
        if (D.isTouched && !w.onlyExternal && (!tt || "mousemove" !== t.type)) {
            var e = tt ? t.targetTouches[0].pageX : t.pageX || t.clientX,
                i = tt ? t.targetTouches[0].pageY : t.pageY || t.clientY;
            if (void 0 === I && z && (I = !!(I || Math.abs(i - D.touches.startY) > Math.abs(e - D.touches.startX))), void 0 !== I || z || (I = !!(I || Math.abs(i - D.touches.startY) < Math.abs(e - D.touches.startX))), I) D.isTouched = !1; else {
                if (z) {
                    if (!w.swipeToNext && e < D.touches.startX || !w.swipeToPrev && e > D.touches.startX) return
                } else if (!w.swipeToNext && i < D.touches.startY || !w.swipeToPrev && i > D.touches.startY) return;
                if (t.assignedToSwiper) D.isTouched = !1; else if (t.assignedToSwiper = !0, w.preventLinks && (D.allowLinks = !1), w.onSlideClick && (D.allowSlideClick = !1), w.autoplay && D.stopAutoplay(!0), !tt || 1 === t.touches.length) {
                    var n;
                    if (D.isMoved || (D.callPlugins("onTouchMoveStart"), w.loop && (D.fixLoop(), D.positions.start = D.getWrapperTranslate()), w.onTouchMoveStart && D.fireCallback(w.onTouchMoveStart, D)), D.isMoved = !0, t.preventDefault ? t.preventDefault() : t.returnValue = !1, D.touches.current = z ? e : i, D.positions.current = (D.touches.current - D.touches.start) * w.touchRatio + D.positions.start, 0 < D.positions.current && w.onResistanceBefore && D.fireCallback(w.onResistanceBefore, D, D.positions.current), D.positions.current < -m() && w.onResistanceAfter && D.fireCallback(w.onResistanceAfter, D, Math.abs(D.positions.current + m())), w.resistance && "100%" !== w.resistance) if (0 < D.positions.current && (n = 1 - D.positions.current / F / 2, D.positions.current = n < .5 ? F / 2 : D.positions.current * n), D.positions.current < -m()) {
                        var r = (D.touches.current - D.touches.start) * w.touchRatio + (m() + D.positions.start);
                        n = (F + r) / F;
                        var o = D.positions.current - r * (1 - n) / 2, s = -m() - F / 2;
                        D.positions.current = o < s || n <= 0 ? s : o
                    }
                    if (w.resistance && "100%" === w.resistance && (0 < D.positions.current && (!w.freeMode || w.freeModeFluid) && (D.positions.current = 0), D.positions.current < -m() && (!w.freeMode || w.freeModeFluid) && (D.positions.current = -m())), !w.followFinger) return;
                    if (w.moveStartThreshold) if (Math.abs(D.touches.current - D.touches.start) > w.moveStartThreshold || U) {
                        if (!U) return U = !0, void (D.touches.start = D.touches.current);
                        D.setWrapperTranslate(D.positions.current)
                    } else D.positions.current = D.positions.start; else D.setWrapperTranslate(D.positions.current);
                    return (w.freeMode || w.watchActiveIndex) && D.updateActiveSlide(D.positions.current), w.grabCursor && (D.container.style.cursor = "move", D.container.style.cursor = "grabbing", D.container.style.cursor = "-moz-grabbin", D.container.style.cursor = "-webkit-grabbing"), Q || (Q = D.touches.current), K || (K = (new Date).getTime()), D.velocity = (D.touches.current - Q) / ((new Date).getTime() - K) / 2, Math.abs(D.touches.current - Q) < 2 && (D.velocity = 0), Q = D.touches.current, K = (new Date).getTime(), D.callPlugins("onTouchMoveEnd"), w.onTouchMove && D.fireCallback(w.onTouchMove, D, t), !1
                }
            }
        }
    }

    function g(t) {
        if (I && D.swipeReset(), !w.onlyExternal && D.isTouched) {
            D.isTouched = !1, w.grabCursor && (D.container.style.cursor = "move", D.container.style.cursor = "grab", D.container.style.cursor = "-moz-grab", D.container.style.cursor = "-webkit-grab"), D.positions.current || 0 === D.positions.current || (D.positions.current = D.positions.start), w.followFinger && D.setWrapperTranslate(D.positions.current), D.times.end = (new Date).getTime(), D.touches.diff = D.touches.current - D.touches.start, D.touches.abs = Math.abs(D.touches.diff), D.positions.diff = D.positions.current - D.positions.start, D.positions.abs = Math.abs(D.positions.diff);
            var e = D.positions.diff, i = D.positions.abs, n = D.times.end - D.times.start;
            i < 5 && n < 300 && !1 === D.allowLinks && (w.freeMode || 0 === i || D.swipeReset(), w.preventLinks && (D.allowLinks = !0), w.onSlideClick && (D.allowSlideClick = !0)), setTimeout(function () {
                null != D && (w.preventLinks && (D.allowLinks = !0), w.onSlideClick && (D.allowSlideClick = !0))
            }, 100);
            var r = m();
            if (!D.isMoved && w.freeMode) return D.isMoved = !1, w.onTouchEnd && D.fireCallback(w.onTouchEnd, D, t), void D.callPlugins("onTouchEnd");
            if (!D.isMoved || 0 < D.positions.current || D.positions.current < -r) return D.swipeReset(), w.onTouchEnd && D.fireCallback(w.onTouchEnd, D, t), void D.callPlugins("onTouchEnd");
            if (D.isMoved = !1, w.freeMode) {
                if (w.freeModeFluid) {
                    var o, s = 1e3 * w.momentumRatio, a = D.velocity * s, l = D.positions.current + a, c = !1,
                        h = 20 * Math.abs(D.velocity) * w.momentumBounceRatio;
                    l < -r && (w.momentumBounce && D.support.transitions ? (l + r < -h && (l = -r - h), o = -r, et = c = !0) : l = -r), 0 < l && (w.momentumBounce && D.support.transitions ? (h < l && (l = h), et = c = !(o = 0)) : l = 0), 0 !== D.velocity && (s = Math.abs((l - D.positions.current) / D.velocity)), D.setWrapperTranslate(l), D.setWrapperTransition(s), w.momentumBounce && c && D.wrapperTransitionEnd(function () {
                        et && (w.onMomentumBounce && D.fireCallback(w.onMomentumBounce, D), D.callPlugins("onMomentumBounce"), D.setWrapperTranslate(o), D.setWrapperTransition(300))
                    }), D.updateActiveSlide(l)
                }
                return (!w.freeModeFluid || 300 <= n) && D.updateActiveSlide(D.positions.current), w.onTouchEnd && D.fireCallback(w.onTouchEnd, D, t), void D.callPlugins("onTouchEnd")
            }
            "toNext" === (L = e < 0 ? "toNext" : "toPrev") && n <= 300 && (i < 30 || !w.shortSwipes ? D.swipeReset() : D.swipeNext(!0, !0)), "toPrev" === L && n <= 300 && (i < 30 || !w.shortSwipes ? D.swipeReset() : D.swipePrev(!0, !0));
            var d = 0;
            if ("auto" === w.slidesPerView) {
                for (var u, p = Math.abs(D.getWrapperTranslate()), f = 0, g = 0; g < D.slides.length; g++) if (p < (f += u = z ? D.slides[g].getWidth(!0, w.roundLengths) : D.slides[g].getHeight(!0, w.roundLengths))) {
                    d = u;
                    break
                }
                F < d && (d = F)
            } else d = E * w.slidesPerView;
            "toNext" === L && 300 < n && (i >= d * w.longSwipesRatio ? D.swipeNext(!0, !0) : D.swipeReset()), "toPrev" === L && 300 < n && (i >= d * w.longSwipesRatio ? D.swipePrev(!0, !0) : D.swipeReset()), w.onTouchEnd && D.fireCallback(w.onTouchEnd, D, t), D.callPlugins("onTouchEnd")
        }
    }

    function v(t, e) {
        return t && t.getAttribute("class") && -1 < t.getAttribute("class").indexOf(e)
    }

    function y(t) {
        for (var e = !1; v(t, w.noSwipingClass) && (e = !0), t = t.parentElement, !e && t.parentElement && !v(t, w.wrapperClass);) ;
        return !e && v(t, w.wrapperClass) && v(t, w.noSwipingClass) && (e = !0), e
    }

    function x(t, e) {
        var i, n = document.createElement("div");
        return n.innerHTML = e, (i = n.firstChild).className += " " + t, i.outerHTML
    }

    function b(e, i, n) {
        function r() {
            var t = +new Date;
            s += a * (t - o) / (1e3 / 60), ("toNext" === l ? e < s : s < e) ? (D.setWrapperTranslate(Math.ceil(s)), D._DOMAnimating = !0, window.setTimeout(function () {
                r()
            }, 1e3 / 60)) : (w.onSlideChangeEnd && ("to" === i ? !0 === n.runCallbacks && D.fireCallback(w.onSlideChangeEnd, D, l) : D.fireCallback(w.onSlideChangeEnd, D, l)), D.setWrapperTranslate(e), D._DOMAnimating = !1)
        }

        var t = "to" === i && 0 <= n.speed ? n.speed : w.speed, o = +new Date;
        if (D.support.transitions || !w.DOMAnimation) D.setWrapperTranslate(e), D.setWrapperTransition(t); else {
            var s = D.getWrapperTranslate(), a = Math.ceil((e - s) / t * (1e3 / 60)), l = e < s ? "toNext" : "toPrev";
            if (D._DOMAnimating) return;
            r()
        }
        D.updateActiveSlide(e), w.onSlideNext && "next" === i && !0 === n.runCallbacks && D.fireCallback(w.onSlideNext, D, e), w.onSlidePrev && "prev" === i && !0 === n.runCallbacks && D.fireCallback(w.onSlidePrev, D, e), w.onSlideReset && "reset" === i && !0 === n.runCallbacks && D.fireCallback(w.onSlideReset, D, e), "next" !== i && "prev" !== i && "to" !== i || !0 !== n.runCallbacks || C(i)
    }

    function C(e) {
        if (D.callPlugins("onSlideChangeStart"), w.onSlideChangeStart) if (w.queueStartCallbacks && D.support.transitions) {
            if (D._queueStartCallbacks) return;
            D._queueStartCallbacks = !0, D.fireCallback(w.onSlideChangeStart, D, e), D.wrapperTransitionEnd(function () {
                D._queueStartCallbacks = !1
            })
        } else D.fireCallback(w.onSlideChangeStart, D, e);
        if (w.onSlideChangeEnd) if (D.support.transitions) if (w.queueEndCallbacks) {
            if (D._queueEndCallbacks) return;
            D._queueEndCallbacks = !0, D.wrapperTransitionEnd(function (t) {
                D.fireCallback(w.onSlideChangeEnd, t, e)
            })
        } else D.wrapperTransitionEnd(function (t) {
            D.fireCallback(w.onSlideChangeEnd, t, e)
        }); else w.DOMAnimation || setTimeout(function () {
            D.fireCallback(w.onSlideChangeEnd, D, e)
        }, 10)
    }

    function _() {
        var t = D.paginationButtons;
        if (t) for (var e = 0; e < t.length; e++) D.h.removeEventListener(t[e], "click", S)
    }

    function k() {
        var t = D.paginationButtons;
        if (t) for (var e = 0; e < t.length; e++) D.h.addEventListener(t[e], "click", S)
    }

    function S(t) {
        for (var e, i = t.target || t.srcElement, n = D.paginationButtons, r = 0; r < n.length; r++) i === n[r] && (e = r);
        w.autoplay && D.stopAutoplay(!0), D.swipeTo(e)
    }

    function T() {
        Z = setTimeout(function () {
            w.loop ? (D.fixLoop(), D.swipeNext(!0, !0)) : D.swipeNext(!0, !0) || (w.autoplayStopOnLast ? (clearTimeout(Z), Z = undefined) : D.swipeTo(0)), D.wrapperTransitionEnd(function () {
                void 0 !== Z && T()
            })
        }, w.autoplay)
    }

    function A() {
        D.calcSlides(), 0 < w.loader.slides.length && 0 === D.slides.length && D.loadSlides(), w.loop && D.createLoop(), D.init(), e(), w.pagination && D.createPagination(!0), w.loop || 0 < w.initialSlide ? D.swipeTo(w.initialSlide, 0, !1) : D.updateActiveSlide(0), w.autoplay && D.startAutoplay(), D.centerIndex = D.activeIndex, w.onSwiperCreated && D.fireCallback(w.onSwiperCreated, D), D.callPlugins("onSwiperCreated")
    }

    if (!document.body.outerHTML && document.body.__defineGetter__ && HTMLElement) {
        var $ = HTMLElement.prototype;
        $.__defineGetter__ && $.__defineGetter__("outerHTML", function () {
            return (new XMLSerializer).serializeToString(this)
        })
    }
    if (window.getComputedStyle || (window.getComputedStyle = function (i) {
        return this.el = i, this.getPropertyValue = function (t) {
            var e = /(\-([a-z]){1})/g;
            return "float" === t && (t = "styleFloat"), e.test(t) && (t = t.replace(e, function (t, e, i) {
                return i.toUpperCase()
            })), i.currentStyle[t] ? i.currentStyle[t] : null
        }, this
    }), Array.prototype.indexOf || (Array.prototype.indexOf = function (t, e) {
        for (var i = e || 0, n = this.length; i < n; i++) if (this[i] === t) return i;
        return -1
    }), (document.querySelectorAll || window.jQuery) && void 0 !== t && (t.nodeType || 0 !== a(t).length)) {
        var M, E, P, L, I, F, D = this;
        D.touches = {
            start: 0,
            startX: 0,
            startY: 0,
            current: 0,
            currentX: 0,
            currentY: 0,
            diff: 0,
            abs: 0
        }, D.positions = {start: 0, abs: 0, diff: 0, current: 0}, D.times = {
            start: 0,
            end: 0
        }, D.id = (new Date).getTime(), D.container = t.nodeType ? t : a(t)[0], D.isTouched = !1, D.isMoved = !1, D.activeIndex = 0, D.centerIndex = 0, D.activeLoaderIndex = 0, D.activeLoopIndex = 0, D.previousIndex = null, D.velocity = 0, D.snapGrid = [], D.slidesGrid = [], D.imagesToLoad = [], D.imagesLoaded = 0, D.wrapperLeft = 0, D.wrapperRight = 0, D.wrapperTop = 0, D.wrapperBottom = 0, D.isAndroid = 0 <= navigator.userAgent.toLowerCase().indexOf("android");
        var O = {
            eventTarget: "wrapper",
            mode: "horizontal",
            touchRatio: 1,
            speed: 300,
            freeMode: !1,
            freeModeFluid: !1,
            momentumRatio: 1,
            momentumBounce: !0,
            momentumBounceRatio: 1,
            slidesPerView: 1,
            slidesPerGroup: 1,
            slidesPerViewFit: !0,
            simulateTouch: !0,
            followFinger: !0,
            shortSwipes: !0,
            longSwipesRatio: .5,
            moveStartThreshold: !1,
            onlyExternal: !1,
            createPagination: !0,
            pagination: !1,
            paginationElement: "span",
            paginationClickable: !1,
            paginationAsRange: !0,
            resistance: !0,
            scrollContainer: !1,
            preventLinks: !0,
            preventLinksPropagation: !1,
            noSwiping: !1,
            noSwipingClass: "swiper-no-swiping",
            initialSlide: 0,
            keyboardControl: !1,
            mousewheelControl: !1,
            mousewheelControlForceToAxis: !1,
            useCSS3Transforms: !0,
            autoplay: !1,
            autoplayDisableOnInteraction: !0,
            autoplayStopOnLast: !1,
            loop: !1,
            loopAdditionalSlides: 0,
            roundLengths: !1,
            calculateHeight: !1,
            cssWidthAndHeight: !1,
            updateOnImagesReady: !0,
            releaseFormElements: !0,
            watchActiveIndex: !1,
            visibilityFullFit: !1,
            offsetPxBefore: 0,
            offsetPxAfter: 0,
            offsetSlidesBefore: 0,
            offsetSlidesAfter: 0,
            centeredSlides: !1,
            queueStartCallbacks: !1,
            queueEndCallbacks: !1,
            autoResize: !0,
            resizeReInit: !1,
            DOMAnimation: !0,
            loader: {slides: [], slidesHTMLType: "inner", surroundGroups: 1, logic: "reload", loadAllSlides: !1},
            swipeToPrev: !0,
            swipeToNext: !0,
            slideElement: "div",
            slideClass: "swiper-slide",
            slideActiveClass: "swiper-slide-active",
            slideVisibleClass: "swiper-slide-visible",
            slideDuplicateClass: "swiper-slide-duplicate",
            wrapperClass: "swiper-wrapper",
            paginationElementClass: "swiper-pagination-switch",
            paginationActiveClass: "swiper-active-switch",
            paginationVisibleClass: "swiper-visible-switch"
        };
        for (var N in w = w || {}, O) if (N in w && "object" == typeof w[N]) for (var j in O[N]) j in w[N] || (w[N][j] = O[N][j]); else N in w || (w[N] = O[N]);
        (D.params = w).scrollContainer && (w.freeMode = !0, w.freeModeFluid = !0), w.loop && (w.resistance = "100%");
        var z = "horizontal" === w.mode, R = ["mousedown", "mousemove", "mouseup"];
        D.browser.ie10 && (R = ["MSPointerDown", "MSPointerMove", "MSPointerUp"]), D.browser.ie11 && (R = ["pointerdown", "pointermove", "pointerup"]), D.touchEvents = {
            touchStart: D.support.touch || !w.simulateTouch ? "touchstart" : R[0],
            touchMove: D.support.touch || !w.simulateTouch ? "touchmove" : R[1],
            touchEnd: D.support.touch || !w.simulateTouch ? "touchend" : R[2]
        };
        for (var B = D.container.childNodes.length - 1; 0 <= B; B--) if (D.container.childNodes[B].className) for (var H = D.container.childNodes[B].className.split(/\s+/), W = 0; W < H.length; W++) H[W] === w.wrapperClass && (M = D.container.childNodes[B]);
        D.wrapper = M, D._extendSwiperSlide = function (i) {
            return i.append = function () {
                return w.loop ? i.insertAfter(D.slides.length - D.loopedSlides) : (D.wrapper.appendChild(i), D.reInit()), i
            }, i.prepend = function () {
                return w.loop ? (D.wrapper.insertBefore(i, D.slides[D.loopedSlides]), D.removeLoopedSlides(), D.calcSlides(), D.createLoop()) : D.wrapper.insertBefore(i, D.wrapper.firstChild), D.reInit(), i
            }, i.insertAfter = function (t) {
                return void 0 !== t && (w.loop ? ((e = D.slides[t + 1 + D.loopedSlides]) ? D.wrapper.insertBefore(i, e) : D.wrapper.appendChild(i), D.removeLoopedSlides(), D.calcSlides(), D.createLoop()) : (e = D.slides[t + 1], D.wrapper.insertBefore(i, e)), D.reInit(), i);
                var e
            }, i.clone = function () {
                return D._extendSwiperSlide(i.cloneNode(!0))
            }, i.remove = function () {
                D.wrapper.removeChild(i), D.reInit()
            }, i.html = function (t) {
                return void 0 === t ? i.innerHTML : (i.innerHTML = t, i)
            }, i.index = function () {
                for (var t, e = D.slides.length - 1; 0 <= e; e--) i === D.slides[e] && (t = e);
                return t
            }, i.isActive = function () {
                return i.index() === D.activeIndex
            }, i.swiperSlideDataStorage || (i.swiperSlideDataStorage = {}), i.getData = function (t) {
                return i.swiperSlideDataStorage[t]
            }, i.setData = function (t, e) {
                return i.swiperSlideDataStorage[t] = e, i
            }, i.data = function (t, e) {
                return void 0 === e ? i.getAttribute("data-" + t) : (i.setAttribute("data-" + t, e), i)
            }, i.getWidth = function (t, e) {
                return D.h.getWidth(i, t, e)
            }, i.getHeight = function (t, e) {
                return D.h.getHeight(i, t, e)
            }, i.getOffset = function () {
                return D.h.getOffset(i)
            }, i
        }, D.calcSlides = function (t) {
            var e = !!D.slides && D.slides.length;
            D.slides = [], D.displaySlides = [];
            for (var i = 0; i < D.wrapper.childNodes.length; i++) if (D.wrapper.childNodes[i].className) for (var n = D.wrapper.childNodes[i].className.split(/\s+/), r = 0; r < n.length; r++) n[r] === w.slideClass && D.slides.push(D.wrapper.childNodes[i]);
            for (i = D.slides.length - 1; 0 <= i; i--) D._extendSwiperSlide(D.slides[i]);
            !1 !== e && (e !== D.slides.length || t) && (s(), o(), D.updateActiveSlide(), D.params.pagination && D.createPagination(), D.callPlugins("numberOfSlidesChanged"))
        }, D.createSlide = function (t, e, i) {
            e = e || D.params.slideClass, i = i || w.slideElement;
            var n = document.createElement(i);
            return n.innerHTML = t || "", n.className = e, D._extendSwiperSlide(n)
        }, D.appendSlide = function (t, e, i) {
            if (t) return t.nodeType ? D._extendSwiperSlide(t).append() : D.createSlide(t, e, i).append()
        }, D.prependSlide = function (t, e, i) {
            if (t) return t.nodeType ? D._extendSwiperSlide(t).prepend() : D.createSlide(t, e, i).prepend()
        }, D.insertSlideAfter = function (t, e, i, n) {
            return void 0 !== t && (e.nodeType ? D._extendSwiperSlide(e).insertAfter(t) : D.createSlide(e, i, n).insertAfter(t))
        }, D.removeSlide = function (t) {
            if (D.slides[t]) {
                if (w.loop) {
                    if (!D.slides[t + D.loopedSlides]) return !1;
                    D.slides[t + D.loopedSlides].remove(), D.removeLoopedSlides(), D.calcSlides(), D.createLoop()
                } else D.slides[t].remove();
                return !0
            }
            return !1
        }, D.removeLastSlide = function () {
            return 0 < D.slides.length && (w.loop ? (D.slides[D.slides.length - 1 - D.loopedSlides].remove(), D.removeLoopedSlides(), D.calcSlides(), D.createLoop()) : D.slides[D.slides.length - 1].remove(), !0)
        }, D.removeAllSlides = function () {
            for (var t = D.slides.length, e = D.slides.length - 1; 0 <= e; e--) D.slides[e].remove(), e === t - 1 && D.setWrapperTranslate(0)
        }, D.getSlide = function (t) {
            return D.slides[t]
        }, D.getLastSlide = function () {
            return D.slides[D.slides.length - 1]
        }, D.getFirstSlide = function () {
            return D.slides[0]
        }, D.activeSlide = function () {
            return D.slides[D.activeIndex]
        }, D.fireCallback = function (t, e, i, n, r, o) {
            var s = t;
            if ("[object Array]" === Object.prototype.toString.call(s)) for (var a = 0; a < s.length; a++) "function" == typeof s[a] && s[a](e, i, n, r, o); else "[object String]" === Object.prototype.toString.call(s) ? w["on" + s] && D.fireCallback(w["on" + s], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]) : s(arguments[1], arguments[2], arguments[3], arguments[4], arguments[5])
        }, D.addCallback = function (t, e) {
            var i;
            return this.params["on" + t] ? n(this.params["on" + t]) ? this.params["on" + t].push(e) : "function" == typeof this.params["on" + t] ? (i = this.params["on" + t], this.params["on" + t] = [], this.params["on" + t].push(i), this.params["on" + t].push(e)) : void 0 : (this.params["on" + t] = [], this.params["on" + t].push(e))
        }, D.removeCallbacks = function (t) {
            D.params["on" + t] && (D.params["on" + t] = null)
        };
        var q = [];
        for (var G in D.plugins) if (w[G]) {
            var X = D.plugins[G](D, w[G]);
            X && q.push(X)
        }
        D.callPlugins = function (t, e) {
            e || (e = {});
            for (var i = 0; i < q.length; i++) t in q[i] && q[i][t](e)
        }, !D.browser.ie10 && !D.browser.ie11 || w.onlyExternal || D.wrapper.classList.add("swiper-wp8-" + (z ? "horizontal" : "vertical")), w.freeMode && (
            D.container.className += " swiper-free-mode"), D.initialized = !1, D.init = function (t, e) {
            var i = D.h.getWidth(D.container, !1, w.roundLengths), n = D.h.getHeight(D.container, !1, w.roundLengths);
            if (i !== D.width || n !== D.height || t) {
                var r, o, s, a, l, c, h;
                D.width = i, D.height = n, F = z ? i : n;
                var d = D.wrapper;
                if (t && D.calcSlides(e), "auto" === w.slidesPerView) {
                    var u = 0, p = 0;
                    0 < w.slidesOffset && (d.style.paddingLeft = "", d.style.paddingRight = "", d.style.paddingTop = "", d.style.paddingBottom = ""), d.style.width = "", d.style.height = "", 0 < w.offsetPxBefore && (z ? D.wrapperLeft = w.offsetPxBefore : D.wrapperTop = w.offsetPxBefore), 0 < w.offsetPxAfter && (z ? D.wrapperRight = w.offsetPxAfter : D.wrapperBottom = w.offsetPxAfter), w.centeredSlides && (z ? (D.wrapperLeft = (F - this.slides[0].getWidth(!0, w.roundLengths)) / 2, D.wrapperRight = (F - D.slides[D.slides.length - 1].getWidth(!0, w.roundLengths)) / 2) : (D.wrapperTop = (F - D.slides[0].getHeight(!0, w.roundLengths)) / 2, D.wrapperBottom = (F - D.slides[D.slides.length - 1].getHeight(!0, w.roundLengths)) / 2)), z ? (0 <= D.wrapperLeft && (d.style.paddingLeft = D.wrapperLeft + "px"), 0 <= D.wrapperRight && (d.style.paddingRight = D.wrapperRight + "px")) : (0 <= D.wrapperTop && (d.style.paddingTop = D.wrapperTop + "px"), 0 <= D.wrapperBottom && (d.style.paddingBottom = D.wrapperBottom + "px"));
                    var f = c = 0;
                    for (D.snapGrid = [], D.slidesGrid = [], h = s = 0; h < D.slides.length; h++) {
                        r = D.slides[h].getWidth(!0, w.roundLengths), o = D.slides[h].getHeight(!0, w.roundLengths), w.calculateHeight && (s = Math.max(s, o));
                        var g = z ? r : o;
                        if (w.centeredSlides) {
                            var m = h === D.slides.length - 1 ? 0 : D.slides[h + 1].getWidth(!0, w.roundLengths),
                                v = h === D.slides.length - 1 ? 0 : D.slides[h + 1].getHeight(!0, w.roundLengths),
                                y = z ? m : v;
                            if (F < g) {
                                if (w.slidesPerViewFit) D.snapGrid.push(c + D.wrapperLeft), D.snapGrid.push(c + g - F + D.wrapperLeft); else for (var x = 0; x <= Math.floor(g / (F + D.wrapperLeft)); x++) 0 === x ? D.snapGrid.push(c + D.wrapperLeft) : D.snapGrid.push(c + D.wrapperLeft + F * x);
                                D.slidesGrid.push(c + D.wrapperLeft)
                            } else D.snapGrid.push(f), D.slidesGrid.push(f);
                            f += g / 2 + y / 2
                        } else {
                            if (F < g) if (w.slidesPerViewFit) D.snapGrid.push(c), D.snapGrid.push(c + g - F); else if (0 !== F) for (var b = 0; b <= Math.floor(g / F); b++) D.snapGrid.push(c + F * b); else D.snapGrid.push(c); else D.snapGrid.push(c);
                            D.slidesGrid.push(c)
                        }
                        c += g, u += r, p += o
                    }
                    w.calculateHeight && (D.height = s), z ? (P = u + D.wrapperRight + D.wrapperLeft, w.cssWidthAndHeight && "height" !== w.cssWidthAndHeight || (d.style.width = u + "px"), w.cssWidthAndHeight && "width" !== w.cssWidthAndHeight || (d.style.height = D.height + "px")) : (w.cssWidthAndHeight && "height" !== w.cssWidthAndHeight || (d.style.width = D.width + "px"), w.cssWidthAndHeight && "width" !== w.cssWidthAndHeight || (d.style.height = p + "px"), P = p + D.wrapperTop + D.wrapperBottom)
                } else if (w.scrollContainer) d.style.width = "", d.style.height = "", a = D.slides[0].getWidth(!0, w.roundLengths), l = D.slides[0].getHeight(!0, w.roundLengths), P = z ? a : l, d.style.width = a + "px", d.style.height = l + "px", E = z ? a : l; else {
                    if (w.calculateHeight) {
                        for (l = s = 0, z || (D.container.style.height = ""), d.style.height = "", h = 0; h < D.slides.length; h++) D.slides[h].style.height = "", s = Math.max(D.slides[h].getHeight(!0), s), z || (l += D.slides[h].getHeight(!0));
                        o = s, D.height = o, z ? l = o : (F = o, D.container.style.height = F + "px")
                    } else o = z ? D.height : D.height / w.slidesPerView, w.roundLengths && (o = Math.ceil(o)), l = z ? D.height : D.slides.length * o;
                    for (r = z ? D.width / w.slidesPerView : D.width, w.roundLengths && (r = Math.ceil(r)), a = z ? D.slides.length * r : D.width, E = z ? r : o, 0 < w.offsetSlidesBefore && (z ? D.wrapperLeft = E * w.offsetSlidesBefore : D.wrapperTop = E * w.offsetSlidesBefore), 0 < w.offsetSlidesAfter && (z ? D.wrapperRight = E * w.offsetSlidesAfter : D.wrapperBottom = E * w.offsetSlidesAfter), 0 < w.offsetPxBefore && (z ? D.wrapperLeft = w.offsetPxBefore : D.wrapperTop = w.offsetPxBefore), 0 < w.offsetPxAfter && (z ? D.wrapperRight = w.offsetPxAfter : D.wrapperBottom = w.offsetPxAfter), w.centeredSlides && (z ? (D.wrapperLeft = (F - E) / 2, D.wrapperRight = (F - E) / 2) : (D.wrapperTop = (F - E) / 2, D.wrapperBottom = (F - E) / 2)), z ? (0 < D.wrapperLeft && (d.style.paddingLeft = D.wrapperLeft + "px"), 0 < D.wrapperRight && (d.style.paddingRight = D.wrapperRight + "px")) : (0 < D.wrapperTop && (d.style.paddingTop = D.wrapperTop + "px"), 0 < D.wrapperBottom && (d.style.paddingBottom = D.wrapperBottom + "px")), P = z ? a + D.wrapperRight + D.wrapperLeft : l + D.wrapperTop + D.wrapperBottom, 0 < parseFloat(a) && (!w.cssWidthAndHeight || "height" === w.cssWidthAndHeight) && (d.style.width = a + "px"), 0 < parseFloat(l) && (!w.cssWidthAndHeight || "width" === w.cssWidthAndHeight) && (d.style.height = l + "px"), c = 0, D.snapGrid = [], D.slidesGrid = [], h = 0; h < D.slides.length; h++) D.snapGrid.push(c), D.slidesGrid.push(c), c += E, 0 < parseFloat(r) && (!w.cssWidthAndHeight || "height" === w.cssWidthAndHeight) && (D.slides[h].style.width = r + "px"), 0 < parseFloat(o) && (!w.cssWidthAndHeight || "width" === w.cssWidthAndHeight) && (D.slides[h].style.height = o + "px")
                }
                D.initialized ? (D.callPlugins("onInit"), w.onInit && D.fireCallback(w.onInit, D)) : (D.callPlugins("onFirstInit"), w.onFirstInit && D.fireCallback(w.onFirstInit, D)), D.initialized = !0
            }
        }, D.reInit = function (t) {
            D.init(!0, t)
        }, D.resizeFix = function (t) {
            D.callPlugins("beforeResizeFix"), D.init(w.resizeReInit || t), w.freeMode ? D.getWrapperTranslate() < -m() && (D.setWrapperTransition(0), D.setWrapperTranslate(-m())) : (D.swipeTo(w.loop ? D.activeLoopIndex : D.activeIndex, 0, !1), w.autoplay && (D.support.transitions && void 0 !== Z ? void 0 !== Z && (clearTimeout(Z), Z = undefined, D.startAutoplay()) : void 0 !== J && (clearInterval(J), J = undefined, D.startAutoplay()))), D.callPlugins("afterResizeFix")
        }, D.destroy = function (t) {
            var e = D.h.removeEventListener, i = "wrapper" === w.eventTarget ? D.wrapper : D.container;
            if (D.browser.ie10 || D.browser.ie11 ? (e(i, D.touchEvents.touchStart, p), e(document, D.touchEvents.touchMove, f), e(document, D.touchEvents.touchEnd, g)) : (D.support.touch && (e(i, "touchstart", p), e(i, "touchmove", f), e(i, "touchend", g)), w.simulateTouch && (e(i, "mousedown", p), e(document, "mousemove", f), e(document, "mouseup", g))), w.autoResize && e(window, "resize", D.resizeFix), s(), w.paginationClickable && _(), w.mousewheelControl && D._wheelEvent && e(D.container, D._wheelEvent, c), w.keyboardControl && e(document, "keydown", l), w.autoplay && D.stopAutoplay(), t) {
                D.wrapper.removeAttribute("style");
                for (var n = 0; n < D.slides.length; n++) D.slides[n].removeAttribute("style")
            }
            D.callPlugins("onDestroy"), window.jQuery && window.jQuery(D.container).data("swiper") && window.jQuery(D.container).removeData("swiper"), window.Zepto && window.Zepto(D.container).data("swiper") && window.Zepto(D.container).removeData("swiper"), D = null
        }, D.disableKeyboardControl = function () {
            w.keyboardControl = !1, D.h.removeEventListener(document, "keydown", l)
        }, D.enableKeyboardControl = function () {
            w.keyboardControl = !0, D.h.addEventListener(document, "keydown", l)
        };
        var Y = (new Date).getTime();
        if (D.disableMousewheelControl = function () {
            return !!D._wheelEvent && (w.mousewheelControl = !1, D.h.removeEventListener(D.container, D._wheelEvent, c), !0)
        }, D.enableMousewheelControl = function () {
            return !!D._wheelEvent && (w.mousewheelControl = !0, D.h.addEventListener(D.container, D._wheelEvent, c), !0)
        }, w.grabCursor) {
            var V = D.container.style;
            V.cursor = "move", V.cursor = "grab", V.cursor = "-moz-grab", V.cursor = "-webkit-grab"
        }
        D.allowSlideClick = !0;
        var U, Q, K, Z, J, tt = !(D.allowLinks = !0), et = !0;
        D.swipeNext = function (t, e) {
            void 0 === t && (t = !0), !e && w.loop && D.fixLoop(), !e && w.autoplay && D.stopAutoplay(!0), D.callPlugins("onSwipeNext");
            var i = D.getWrapperTranslate().toFixed(2), n = i;
            if ("auto" === w.slidesPerView) {
                for (var r = 0; r < D.snapGrid.length; r++) if (-i >= D.snapGrid[r].toFixed(2) && -i < D.snapGrid[r + 1].toFixed(2)) {
                    n = -D.snapGrid[r + 1];
                    break
                }
            } else {
                var o = E * w.slidesPerGroup;
                n = -(Math.floor(Math.abs(i) / Math.floor(o)) * o + o)
            }
            return n < -m() && (n = -m()), n !== i && (b(n, "next", {runCallbacks: t}), !0)
        }, D.swipePrev = function (t, e) {
            void 0 === t && (t = !0), !e && w.loop && D.fixLoop(), !e && w.autoplay && D.stopAutoplay(!0), D.callPlugins("onSwipePrev");
            var i, n = Math.ceil(D.getWrapperTranslate());
            if ("auto" === w.slidesPerView) {
                i = 0;
                for (var r = 1; r < D.snapGrid.length; r++) {
                    if (-n === D.snapGrid[r]) {
                        i = -D.snapGrid[r - 1];
                        break
                    }
                    if (-n > D.snapGrid[r] && -n < D.snapGrid[r + 1]) {
                        i = -D.snapGrid[r];
                        break
                    }
                }
            } else {
                var o = E * w.slidesPerGroup;
                i = -(Math.ceil(-n / o) - 1) * o
            }
            return 0 < i && (i = 0), i !== n && (b(i, "prev", {runCallbacks: t}), !0)
        }, D.swipeReset = function (t) {
            void 0 === t && (t = !0), D.callPlugins("onSwipeReset");
            var e, i = D.getWrapperTranslate(), n = E * w.slidesPerGroup;
            m();
            if ("auto" === w.slidesPerView) {
                for (var r = e = 0; r < D.snapGrid.length; r++) {
                    if (-i === D.snapGrid[r]) return;
                    if (-i >= D.snapGrid[r] && -i < D.snapGrid[r + 1]) {
                        e = 0 < D.positions.diff ? -D.snapGrid[r + 1] : -D.snapGrid[r];
                        break
                    }
                }
                -i >= D.snapGrid[D.snapGrid.length - 1] && (e = -D.snapGrid[D.snapGrid.length - 1]), i <= -m() && (e = -m())
            } else e = i < 0 ? Math.round(i / n) * n : 0, i <= -m() && (e = -m());
            return w.scrollContainer && (e = i < 0 ? i : 0), e < -m() && (e = -m()), w.scrollContainer && E < F && (e = 0), e !== i && (b(e, "reset", {runCallbacks: t}), !0)
        }, D.swipeTo = function (t, e, i) {
            t = parseInt(t, 10), D.callPlugins("onSwipeTo", {index: t, speed: e}), w.loop && (t += D.loopedSlides);
            var n, r = D.getWrapperTranslate();
            if (!(!isFinite(t) || t > D.slides.length - 1 || t < 0)) return (n = "auto" === w.slidesPerView ? -D.slidesGrid[t] : -t * E) < -m() && (n = -m()), n !== r && (void 0 === i && (i = !0), b(n, "to", {
                index: t,
                speed: e,
                runCallbacks: i
            }), !0)
        }, D._queueStartCallbacks = !1, D._queueEndCallbacks = !1, D.updateActiveSlide = function (t) {
            if (D.initialized && 0 !== D.slides.length) {
                var e;
                if (D.previousIndex = D.activeIndex, void 0 === t && (t = D.getWrapperTranslate()), 0 < t && (t = 0), "auto" === w.slidesPerView) {
                    if (D.activeIndex = D.slidesGrid.indexOf(-t), D.activeIndex < 0) {
                        for (e = 0; e < D.slidesGrid.length - 1 && !(-t > D.slidesGrid[e] && -t < D.slidesGrid[e + 1]); e++) ;
                        var i = Math.abs(D.slidesGrid[e] + t), n = Math.abs(D.slidesGrid[e + 1] + t);
                        D.activeIndex = i <= n ? e : e + 1
                    }
                } else D.activeIndex = Math[w.visibilityFullFit ? "ceil" : "round"](-t / E);
                if (D.activeIndex === D.slides.length && (D.activeIndex = D.slides.length - 1), D.activeIndex < 0 && (D.activeIndex = 0), D.slides[D.activeIndex]) {
                    if (D.calcVisibleSlides(t), D.support.classList) {
                        var r;
                        for (e = 0; e < D.slides.length; e++) (r = D.slides[e]).classList.remove(w.slideActiveClass), 0 <= D.visibleSlides.indexOf(r) ? r.classList.add(w.slideVisibleClass) : r.classList.remove(w.slideVisibleClass);
                        D.slides[D.activeIndex].classList.add(w.slideActiveClass)
                    } else {
                        var o = new RegExp("\\s*" + w.slideActiveClass), s = new RegExp("\\s*" + w.slideVisibleClass);
                        for (e = 0; e < D.slides.length; e++) D.slides[e].className = D.slides[e].className.replace(o, "").replace(s, ""), 0 <= D.visibleSlides.indexOf(D.slides[e]) && (D.slides[e].className += " " + w.slideVisibleClass);
                        D.slides[D.activeIndex].className += " " + w.slideActiveClass
                    }
                    if (w.loop) {
                        var a = D.loopedSlides;
                        D.activeLoopIndex = D.activeIndex - a, D.activeLoopIndex >= D.slides.length - 2 * a && (D.activeLoopIndex = D.slides.length - 2 * a - D.activeLoopIndex), D.activeLoopIndex < 0 && (D.activeLoopIndex = D.slides.length - 2 * a + D.activeLoopIndex), D.activeLoopIndex < 0 && (D.activeLoopIndex = 0)
                    } else D.activeLoopIndex = D.activeIndex;
                    w.pagination && D.updatePagination(t)
                }
            }
        }, D.createPagination = function (t) {
            if (w.paginationClickable && D.paginationButtons && _(), D.paginationContainer = w.pagination.nodeType ? w.pagination : a(w.pagination)[0], w.createPagination) {
                var e = "", i = D.slides.length;
                w.loop && (i -= 2 * D.loopedSlides);
                for (var n = 0; n < i; n++) e += "<" + w.paginationElement + ' class="' + w.paginationElementClass + '"></' + w.paginationElement + ">";
                D.paginationContainer.innerHTML = e
            }
            D.paginationButtons = a("." + w.paginationElementClass, D.paginationContainer), t || D.updatePagination(), D.callPlugins("onCreatePagination"), w.paginationClickable && k()
        }, D.updatePagination = function (t) {
            if (w.pagination && (!(D.slides.length < 1) && a("." + w.paginationActiveClass, D.paginationContainer))) {
                var e = D.paginationButtons;
                if (0 !== e.length) {
                    for (var i = 0; i < e.length; i++) e[i].className = w.paginationElementClass;
                    var n = w.loop ? D.loopedSlides : 0;
                    if (w.paginationAsRange) {
                        D.visibleSlides || D.calcVisibleSlides(t);
                        var r, o = [];
                        for (r = 0; r < D.visibleSlides.length; r++) {
                            var s = D.slides.indexOf(D.visibleSlides[r]) - n;
                            w.loop && s < 0 && (s = D.slides.length - 2 * D.loopedSlides + s), w.loop && s >= D.slides.length - 2 * D.loopedSlides && (s = D.slides.length - 2 * D.loopedSlides - s, s = Math.abs(s)), o.push(s)
                        }
                        for (r = 0; r < o.length; r++) e[o[r]] && (e[o[r]].className += " " + w.paginationVisibleClass);
                        w.loop ? e[D.activeLoopIndex] !== undefined && (e[D.activeLoopIndex].className += " " + w.paginationActiveClass) : e[D.activeIndex] && (e[D.activeIndex].className += " " + w.paginationActiveClass)
                    } else w.loop ? e[D.activeLoopIndex] && (e[D.activeLoopIndex].className += " " + w.paginationActiveClass + " " + w.paginationVisibleClass) : e[D.activeIndex] && (e[D.activeIndex].className += " " + w.paginationActiveClass + " " + w.paginationVisibleClass)
                }
            }
        }, D.calcVisibleSlides = function (t) {
            var e = [], i = 0, n = 0, r = 0;
            z && 0 < D.wrapperLeft && (t += D.wrapperLeft), !z && 0 < D.wrapperTop && (t += D.wrapperTop);
            for (var o = 0; o < D.slides.length; o++) {
                r = (i += n) + (n = "auto" === w.slidesPerView ? z ? D.h.getWidth(D.slides[o], !0, w.roundLengths) : D.h.getHeight(D.slides[o], !0, w.roundLengths) : E);
                var s = !1;
                w.visibilityFullFit ? (-t <= i && r <= -t + F && (s = !0), i <= -t && -t + F <= r && (s = !0)) : (-t < r && r <= -t + F && (s = !0), -t <= i && i < -t + F && (s = !0), i < -t && -t + F < r && (s = !0)), s && e.push(D.slides[o])
            }
            0 === e.length && (e = [D.slides[D.activeIndex]]), D.visibleSlides = e
        }, D.startAutoplay = function () {
            if (D.support.transitions) {
                if (void 0 !== Z) return !1;
                if (!w.autoplay) return;
                D.callPlugins("onAutoplayStart"), w.onAutoplayStart && D.fireCallback(w.onAutoplayStart, D), T()
            } else {
                if (void 0 !== J) return !1;
                if (!w.autoplay) return;
                D.callPlugins("onAutoplayStart"), w.onAutoplayStart && D.fireCallback(w.onAutoplayStart, D), J = setInterval(function () {
                    w.loop ? (D.fixLoop(), D.swipeNext(!0, !0)) : D.swipeNext(!0, !0) || (w.autoplayStopOnLast ? (clearInterval(J), J = undefined) : D.swipeTo(0))
                }, w.autoplay)
            }
        }, D.stopAutoplay = function (t) {
            if (D.support.transitions) {
                if (!Z) return;
                Z && clearTimeout(Z), Z = undefined, t && !w.autoplayDisableOnInteraction && D.wrapperTransitionEnd(function () {
                    T()
                }), D.callPlugins("onAutoplayStop"), w.onAutoplayStop && D.fireCallback(w.onAutoplayStop, D)
            } else J && clearInterval(J), J = undefined, D.callPlugins("onAutoplayStop"), w.onAutoplayStop && D.fireCallback(w.onAutoplayStop, D)
        }, D.loopCreated = !1, D.removeLoopedSlides = function () {
            if (D.loopCreated) for (var t = 0; t < D.slides.length; t++) !0 === D.slides[t].getData("looped") && D.wrapper.removeChild(D.slides[t])
        }, D.createLoop = function () {
            if (0 !== D.slides.length) {
                "auto" === w.slidesPerView ? D.loopedSlides = w.loopedSlides || 1 : D.loopedSlides = Math.floor(w.slidesPerView) + w.loopAdditionalSlides, D.loopedSlides > D.slides.length && (D.loopedSlides = D.slides.length);
                var t, e = "", i = "", n = "", r = D.slides.length, o = Math.floor(D.loopedSlides / r),
                    s = D.loopedSlides % r;
                for (t = 0; t < o * r; t++) {
                    var a = t;
                    if (r <= t) a = t - r * Math.floor(t / r);
                    n += D.slides[a].outerHTML
                }
                for (t = 0; t < s; t++) i += x(w.slideDuplicateClass, D.slides[t].outerHTML);
                for (t = r - s; t < r; t++) e += x(w.slideDuplicateClass, D.slides[t].outerHTML);
                var l = e + n + M.innerHTML + n + i;
                for (M.innerHTML = l, D.loopCreated = !0, D.calcSlides(), t = 0; t < D.slides.length; t++) (t < D.loopedSlides || t >= D.slides.length - D.loopedSlides) && D.slides[t].setData("looped", !0);
                D.callPlugins("onCreateLoop")
            }
        }, D.fixLoop = function () {
            var t;
            D.activeIndex < D.loopedSlides ? (t = D.slides.length - 3 * D.loopedSlides + D.activeIndex, D.swipeTo(t, 0, !1)) : ("auto" === w.slidesPerView && D.activeIndex >= 2 * D.loopedSlides || D.activeIndex > D.slides.length - 2 * w.slidesPerView) && (t = -D.slides.length + D.activeIndex + D.loopedSlides, D.swipeTo(t, 0, !1))
        }, D.loadSlides = function () {
            var t = "";
            D.activeLoaderIndex = 0;
            for (var e = w.loader.slides, i = w.loader.loadAllSlides ? e.length : w.slidesPerView * (1 + w.loader.surroundGroups), n = 0; n < i; n++) "outer" === w.loader.slidesHTMLType ? t += e[n] : t += "<" + w.slideElement + ' class="' + w.slideClass + '" data-swiperindex="' + n + '">' + e[n] + "</" + w.slideElement + ">";
            D.wrapper.innerHTML = t, D.calcSlides(!0), w.loader.loadAllSlides || D.wrapperTransitionEnd(D.reloadSlides, !0)
        }, D.reloadSlides = function () {
            var t = w.loader.slides, e = parseInt(D.activeSlide().data("swiperindex"), 10);
            if (!(e < 0 || e > t.length - 1)) {
                D.activeLoaderIndex = e;
                var i, n = Math.max(0, e - w.slidesPerView * w.loader.surroundGroups),
                    r = Math.min(e + w.slidesPerView * (1 + w.loader.surroundGroups) - 1, t.length - 1);
                if (0 < e) {
                    var o = -E * (e - n);
                    D.setWrapperTranslate(o), D.setWrapperTransition(0)
                }
                if ("reload" === w.loader.logic) {
                    var s = D.wrapper.innerHTML = "";
                    for (i = n; i <= r; i++) s += "outer" === w.loader.slidesHTMLType ? t[i] : "<" + w.slideElement + ' class="' + w.slideClass + '" data-swiperindex="' + i + '">' + t[i] + "</" + w.slideElement + ">";
                    D.wrapper.innerHTML = s
                } else {
                    var a = 1e3, l = 0;
                    for (i = 0; i < D.slides.length; i++) {
                        var c = D.slides[i].data("swiperindex");
                        c < n || r < c ? D.wrapper.removeChild(D.slides[i]) : (a = Math.min(c, a), l = Math.max(c, l))
                    }
                    for (i = n; i <= r; i++) {
                        var h;
                        i < a && ((h = document.createElement(w.slideElement)).className = w.slideClass, h.setAttribute("data-swiperindex", i), h.innerHTML = t[i], D.wrapper.insertBefore(h, D.wrapper.firstChild)), l < i && ((h = document.createElement(w.slideElement)).className = w.slideClass, h.setAttribute("data-swiperindex", i), h.innerHTML = t[i], D.wrapper.appendChild(h))
                    }
                }
                D.reInit(!0)
            }
        }, A()
    }
};
Swiper.prototype = {
    plugins: {}, wrapperTransitionEnd: function (e, i) {
        "use strict";

        function n(t) {
            if (t.target === s && (e(o), o.params.queueEndCallbacks && (o._queueEndCallbacks = !1), !i)) for (r = 0; r < a.length; r++) o.h.removeEventListener(s, a[r], n)
        }

        var r, o = this, s = o.wrapper,
            a = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"];
        if (e) for (r = 0; r < a.length; r++) o.h.addEventListener(s, a[r], n)
    }, getWrapperTranslate: function (t) {
        "use strict";
        var e, i, n, r, o = this.wrapper;
        return void 0 === t && (t = "horizontal" === this.params.mode ? "x" : "y"), this.support.transforms && this.params.useCSS3Transforms ? (n = window.getComputedStyle(o, null), window.WebKitCSSMatrix ? r = new WebKitCSSMatrix("none" === n.webkitTransform ? "" : n.webkitTransform) : e = (r = n.MozTransform || n.OTransform || n.MsTransform || n.msTransform || n.transform || n.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,")).toString().split(","), "x" === t && (i = window.WebKitCSSMatrix ? r.m41 : 16 === e.length ? parseFloat(e[12]) : parseFloat(e[4])), "y" === t && (i = window.WebKitCSSMatrix ? r.m42 : 16 === e.length ? parseFloat(e[13]) : parseFloat(e[5]))) : ("x" === t && (i = parseFloat(o.style.left, 10) || 0), "y" === t && (i = parseFloat(o.style.top, 10) || 0)), i || 0
    }, setWrapperTranslate: function (t, e, i) {
        "use strict";
        var n, r = this.wrapper.style, o = {x: 0, y: 0, z: 0};
        3 === arguments.length ? (o.x = t, o.y = e, o.z = i) : (void 0 === e && (e = "horizontal" === this.params.mode ? "x" : "y"), o[e] = t), this.support.transforms && this.params.useCSS3Transforms ? (n = this.support.transforms3d ? "translate3d(" + o.x + "px, " + o.y + "px, " + o.z + "px)" : "translate(" + o.x + "px, " + o.y + "px)", r.webkitTransform = r.MsTransform = r.msTransform = r.MozTransform = r.OTransform = r.transform = n) : (r.left = o.x + "px", r.top = o.y + "px"), this.callPlugins("onSetWrapperTransform", o), this.params.onSetWrapperTransform && this.fireCallback(this.params.onSetWrapperTransform, this, o)
    }, setWrapperTransition: function (t) {
        "use strict";
        var e = this.wrapper.style;
        e.webkitTransitionDuration = e.MsTransitionDuration = e.msTransitionDuration = e.MozTransitionDuration = e.OTransitionDuration = e.transitionDuration = t / 1e3 + "s", this.callPlugins("onSetWrapperTransition", {duration: t}), this.params.onSetWrapperTransition && this.fireCallback(this.params.onSetWrapperTransition, this, t)
    }, h: {
        getWidth: function (t, e, i) {
            "use strict";
            var n = window.getComputedStyle(t, null).getPropertyValue("width"), r = parseFloat(n);
            return (isNaN(r) || 0 < n.indexOf("%") || r < 0) && (r = t.offsetWidth - parseFloat(window.getComputedStyle(t, null).getPropertyValue("padding-left")) - parseFloat(window.getComputedStyle(t, null).getPropertyValue("padding-right"))), e && (r += parseFloat(window.getComputedStyle(t, null).getPropertyValue("padding-left")) + parseFloat(window.getComputedStyle(t, null).getPropertyValue("padding-right"))), i ? Math.ceil(r) : r
        }, getHeight: function (t, e, i) {
            "use strict";
            if (e) return t.offsetHeight;
            var n = window.getComputedStyle(t, null).getPropertyValue("height"), r = parseFloat(n);
            return (isNaN(r) || 0 < n.indexOf("%") || r < 0) && (r = t.offsetHeight - parseFloat(window.getComputedStyle(t, null).getPropertyValue("padding-top")) - parseFloat(window.getComputedStyle(t, null).getPropertyValue("padding-bottom"))), e && (r += parseFloat(window.getComputedStyle(t, null).getPropertyValue("padding-top")) + parseFloat(window.getComputedStyle(t, null).getPropertyValue("padding-bottom"))), i ? Math.ceil(r) : r
        }, getOffset: function (t) {
            "use strict";
            var e = t.getBoundingClientRect(), i = document.body, n = t.clientTop || i.clientTop || 0,
                r = t.clientLeft || i.clientLeft || 0, o = window.pageYOffset || t.scrollTop,
                s = window.pageXOffset || t.scrollLeft;
            return document.documentElement && !window.pageYOffset && (o = document.documentElement.scrollTop, s = document.documentElement.scrollLeft), {
                top: e.top + o - n,
                left: e.left + s - r
            }
        }, windowWidth: function () {
            "use strict";
            return window.innerWidth ? window.innerWidth : document.documentElement && document.documentElement.clientWidth ? document.documentElement.clientWidth : void 0
        }, windowHeight: function () {
            "use strict";
            return window.innerHeight ? window.innerHeight : document.documentElement && document.documentElement.clientHeight ? document.documentElement.clientHeight : void 0
        }, windowScroll: function () {
            "use strict";
            return "undefined" != typeof pageYOffset ? {
                left: window.pageXOffset,
                top: window.pageYOffset
            } : document.documentElement ? {
                left: document.documentElement.scrollLeft,
                top: document.documentElement.scrollTop
            } : void 0
        }, addEventListener: function (t, e, i, n) {
            "use strict";
            void 0 === n && (n = !1), t.addEventListener ? t.addEventListener(e, i, n) : t.attachEvent && t.attachEvent("on" + e, i)
        }, removeEventListener: function (t, e, i, n) {
            "use strict";
            void 0 === n && (n = !1), t.removeEventListener ? t.removeEventListener(e, i, n) : t.detachEvent && t.detachEvent("on" + e, i)
        }
    }, setTransform: function (t, e) {
        "use strict";
        var i = t.style;
        i.webkitTransform = i.MsTransform = i.msTransform = i.MozTransform = i.OTransform = i.transform = e
    }, setTranslate: function (t, e) {
        "use strict";
        var i = t.style, n = {x: e.x || 0, y: e.y || 0, z: e.z || 0},
            r = this.support.transforms3d ? "translate3d(" + n.x + "px," + n.y + "px," + n.z + "px)" : "translate(" + n.x + "px," + n.y + "px)";
        i.webkitTransform = i.MsTransform = i.msTransform = i.MozTransform = i.OTransform = i.transform = r, this.support.transforms || (i.left = n.x + "px", i.top = n.y + "px")
    }, setTransition: function (t, e) {
        "use strict";
        var i = t.style;
        i.webkitTransitionDuration = i.MsTransitionDuration = i.msTransitionDuration = i.MozTransitionDuration = i.OTransitionDuration = i.transitionDuration = e + "ms"
    }, support: {
        touch: window.Modernizr && !0 === Modernizr.touch || function () {
            "use strict";
            return !!("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch)
        }(), transforms3d: window.Modernizr && !0 === Modernizr.csstransforms3d || function () {
            "use strict";
            var t = document.createElement("div").style;
            return "webkitPerspective" in t || "MozPerspective" in t || "OPerspective" in t || "MsPerspective" in t || "perspective" in t
        }(), transforms: window.Modernizr && !0 === Modernizr.csstransforms || function () {
            "use strict";
            var t = document.createElement("div").style;
            return "transform" in t || "WebkitTransform" in t || "MozTransform" in t || "msTransform" in t || "MsTransform" in t || "OTransform" in t
        }(), transitions: window.Modernizr && !0 === Modernizr.csstransitions || function () {
            "use strict";
            var t = document.createElement("div").style;
            return "transition" in t || "WebkitTransition" in t || "MozTransition" in t || "msTransition" in t || "MsTransition" in t || "OTransition" in t
        }(), classList: function () {
            "use strict";
            return "classList" in document.createElement("div")
        }()
    }, browser: {
        ie8: function () {
            "use strict";
            var t = -1;
            if ("Microsoft Internet Explorer" === navigator.appName) {
                var e = navigator.userAgent;
                null !== new RegExp(/MSIE ([0-9]{1,}[\.0-9]{0,})/).exec(e) && (t = parseFloat(RegExp.$1))
            }
            return -1 !== t && t < 9
        }(), ie10: window.navigator.msPointerEnabled, ie11: window.navigator.pointerEnabled
    }
}, (window.jQuery || window.Zepto) && function (o) {
    "use strict";
    o.fn.swiper = function (n) {
        var r;
        return this.each(function (t) {
            var e = o(this), i = new Swiper(e[0], n);
            t || (r = i), e.data("swiper", i)
        }), r
    }
}(window.jQuery || window.Zepto), "undefined" != typeof module ? module.exports = Swiper : "function" == typeof define && define.amd && define([], function () {
    "use strict";
    return Swiper
}), Swiper.prototype.plugins.scrollbar = function (s, a) {
    function t(t) {
        return document.querySelectorAll ? document.querySelectorAll(t) : jQuery(t)
    }

    function e(t) {
        _ = !0, t.preventDefault ? t.preventDefault() : t.returnValue = !1, r(t), clearTimeout(w), s.setTransition(x, 0), x.style.opacity = 1, s.setWrapperTransition(100), s.setTransition(b, 100)
    }

    function i(t) {
        _ && (t.preventDefault ? t.preventDefault() : t.returnValue = !1, r(t), s.setWrapperTransition(0), s.setTransition(x, 0), s.setTransition(b, 0))
    }

    function n() {
        _ = !1, a.hide && (clearTimeout(w), w = setTimeout(function () {
            x.style.opacity = 0, s.setTransition(x, 400)
        }, 1e3)), a.snapOnRelease && s.swipeReset()
    }

    function r(t) {
        var e = y = 0;
        v ? (e = (t.pageX || t.clientX) - s.h.getOffset(x).left - f / 2) < 0 ? e = 0 : h < e + f && (e = h - f) : (y = (t.pageY || t.clientY) - s.h.getOffset(x).top - g / 2, y < 0 ? y = 0 : y + g > d && (y = d - g)), s.setTranslate(b, {
            x: e,
            y: y
        });
        var i = -e / p, n = -y / p;
        s.setWrapperTranslate(i, n)
    }

    function o() {
        b.style.width = "", b.style.height = "", v ? (h = s.h.getWidth(x, !0), u = s.width / s.h.getWidth(s.wrapper), p = u * (h / s.width), f = h * u, b.style.width = f + "px") : (d = s.h.getHeight(x, !0), u = s.height / s.h.getHeight(s.wrapper), p = u * (d / s.height), d < (g = d * u) && (g = d), b.style.height = g + "px")
    }

    if (a && a.container) {
        var l = {hide: !0, draggable: !0, snapOnRelease: !1};
        for (var c in a = a || {}, l) c in a || (a[c] = l[c]);
        if ((document.querySelectorAll || window.jQuery) && (a.container.nodeType || 0 != t(a.container).length)) {
            var h, d, u, p, f, g, m = a.container.nodeType ? a.container : t(a.container)[0],
                v = "horizontal" == s.params.mode, x = m, b = document.createElement("div");
            b.className = "swiper-scrollbar-drag", a.draggable && (b.className += " swiper-scrollbar-cursor-drag"), x.appendChild(b), a.hide && (x.style.opacity = 0);
            var w, C = s.touchEvents;
            if (a.draggable) {
                var _ = !1, k = s.support.touch ? x : document;
                s.h.addEventListener(x, C.touchStart, e, !1), s.h.addEventListener(k, C.touchMove, i, !1), s.h.addEventListener(k, C.touchEnd, n, !1)
            }
            return {
                onFirstInit: function () {
                    o()
                }, onInit: function () {
                    o()
                }, onTouchMoveEnd: function () {
                    a.hide && (clearTimeout(w), x.style.opacity = 1, s.setTransition(x, 200))
                }, onTouchEnd: function () {
                    a.hide && (clearTimeout(w), w = setTimeout(function () {
                        x.style.opacity = 0, s.setTransition(x, 400)
                    }, 1e3))
                }, onSetWrapperTransform: function (t) {
                    if (v) {
                        var e = t.x * p, i = f;
                        if (0 < e) {
                            var n = e;
                            e = 0, i = f - n
                        } else h < -e + f && (i = h + e);
                        s.setTranslate(b, {x: -e}), b.style.width = i + "px"
                    } else {
                        var r = t.y * p, o = g;
                        if (0 < r) {
                            n = r;
                            r = 0, o = g - n
                        } else d < -r + g && (o = d + r);
                        s.setTranslate(b, {y: -r}), b.style.height = o + "px"
                    }
                    s.params.freeMode && a.hide && (clearTimeout(w), x.style.opacity = 1, w = setTimeout(function () {
                        x.style.opacity = 0, s.setTransition(x, 400)
                    }, 1e3))
                }, onSetWrapperTransition: function (t) {
                    s.setTransition(b, t.duration)
                }, onDestroy: function () {
                    var t = s.support.touch ? x : document;
                    s.h.removeEventListener(x, C.touchStart, e, !1), s.h.removeEventListener(t, C.touchMove, i, !1), s.h.removeEventListener(t, C.touchEnd, n, !1)
                }
            }
        }
    }
};