import pandas as pd
from zc_core.client.mongo_client import Mongo


def export_sku(table, file,query={}):
    column_header_map = [
        {'col': '_id', 'title': '商品编号'},
        {'col': 'skuName', 'title': '商品名称'},
        {'col': 'soldCount', 'title': '销量'},
        {'col': 'supplierName', 'title': '供应商'},
        {'col': 'originPrice', 'title': '原价'},
        {'col': 'salePrice', 'title': '销售价'},
        {'col': 'catalog1Name', 'title': '一级分类'},
        {'col': 'catalog2Name', 'title': '二级分类'},
        {'col': 'catalog3Name', 'title': '三级分类'},
        {'col': 'stock', 'title': '库存'},
        # {'col': 'spuId', 'title': '同款编号'},
        {'col': 'brandModel', 'title': '型号'}
    ]

    columns = list()
    headers = list()
    for row in column_header_map:
        columns.append(row.get('col'))
        headers.append(row.get('title'))
    data_list = Mongo().list(table, query=query)
    if data_list:
        print('数量: %s' % len(data_list))
        write = pd.ExcelWriter(file)
        df = pd.DataFrame(data_list)
        df.to_excel(write, sheet_name='商品', columns=columns, header=headers, index=False)
        write.save()
        print('导出成功: %s' % table)
    else:
        print('无数据: %s' % table)


if __name__ == '__main__':
    # batch_no = time_to_batch_no(datetime.now())
    batch_no = '20210531'

    table = 'data_{}'.format(batch_no)
    file = 'E:\\data\\CRCC商城_{}.xlsx'.format(batch_no)
    export_sku(table, file,query={
        'catalog1Name': {"$in": ["办公用品/通讯设备"]}
    })
