# -*- coding: utf-8 -*-
import random
import scrapy
from scrapy import Request
from datetime import datetime
from crccbuy.rules import *
from zc_core.dao.spu_pool_dao import SpuPoolDao
from zc_core.dao.batch_dao import BatchDao
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.done_filter import DoneFilter
from zc_core.spiders.base import BaseSpider


class FullSpider(BaseSpider):
    name = 'full'

    # item_url = 'https://www.crccbuy.com/index.php?app=goods&id={}'
    item_url = 'https://www.crccbuy.com/index.php?app=goods&act=comments&id={}'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(FullSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)
        # 避免重复采集
        self.done_filter = DoneFilter(self.batch_no)

    def start_requests(self):

        pool_list = SpuPoolDao().get_spu_pool_list()
        self.logger.info('全量：%s' % (len(pool_list)))
        print("pool_list:", pool_list)
        for spu in pool_list:
            spu_id = spu.get('_id')
            offline_time = spu.get('offlineTime', 0)
            settings = get_project_settings()
            if offline_time > settings.get('MAX_OFFLINE_TIME', 2):
                self.logger.info('忽略: [%s][%s]', spu_id, offline_time)
                continue
            # 避免重复采集
            if self.done_filter.contains(spu_id) and not settings.get('FORCE_RECOVER', False):
                self.logger.info('已采: [%s]', spu_id)
                continue
            # 采集商品
            yield Request(
                url=self.item_url.format(spu_id),
                callback=self.parse_item_data,
                errback=self.error_back,
                priority=260,
                meta={
                    'reqType': 'item',
                    'batchNo': self.batch_no,
                    'spuId': spu_id,
                },
            )

    # 处理ItemData
    def parse_item_data(self, response):
        # 处理商品详情页
        data_list = parse_item_data(response)
        for data in data_list:
            if data:
                self.logger.info('商品: [%s]' % data.get('skuId'))
                yield data
