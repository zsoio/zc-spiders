# encoding=utf-8
"""
响应对象业务校验
"""
from scrapy.exceptions import IgnoreRequest
from zc_core.middlewares.validate import BaseValidateMiddleware


class BizValidator(BaseValidateMiddleware):

    def validate_spu(self, request, response, spider):
        if '很抱歉！没有找到该搜索条件下的相关商品，我们为您推荐了以下您可能感兴趣的宝贝！' in response.text:
            catalog_id = request.meta.get('catalogId')
            raise IgnoreRequest('[spu]：不存在[%s]' % catalog_id)
        return response
