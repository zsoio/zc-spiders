# -*- coding: utf-8 -*-
import re


# 从链接中解析品类编码
def match_cat_id(link):
    # /index.php/list-1423.html
    if link:
        arr = re.findall(r'list-(\d+)\.html', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析品类编码
def match_supplier_id(link):
    # /seller/284
    if link:
        arr = re.findall(r'/seller/(\d+)', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 测试
if __name__ == '__main__':
    print(match_supplier_id('/seller/284'))
    pass
