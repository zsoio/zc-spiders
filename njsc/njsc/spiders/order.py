# -*- coding: utf-8 -*-

from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.dao.sku_pool_dao import SkuPoolDao
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from zc_core.spiders.base import BaseSpider
from njsc.rules import *


class OrderSpider(BaseSpider):
    name = "order"
    order_url = "https://www.njsc365.com/api/index.php/item-goodsSellLoglist.html?output=json&goods_id={}&page={}"

    def __init__(self, batchNo=None, *args, **kwargs):
        super(OrderSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.order_page_size = 20

    def start_requests(self):
        sku_list = SkuPoolDao().get_sku_pool_list(
            fields={"_id": 1, "materialCode": 1, 'supplierId': 1, 'supplierName': 1},
            query={"materialCode": {'$ne': None}})
        if sku_list:
            for sku in sku_list:
                sku_id = sku.get("_id")
                supplier_id = sku.get("supplierId")
                supplier_name = sku.get("supplierName")
                goods_id = sku.get("materialCode")
                page = 1
                order_url = self.order_url.format(goods_id, page)
                yield Request(
                    url=order_url,
                    meta={
                        'reqType': 'order',
                        "page": page,
                        'skuId': sku_id,
                        "goodsId": goods_id,
                        'batchNo': self.batch_no,
                        "supplierId": supplier_id,
                        "supplierName": supplier_name
                    },
                    headers={
                        'Cache-Control': 'max-age=0',
                        'Host': 'www.njsc365.com',
                        'Upgrade-Insecure-Requests': '1',
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36',
                    },
                    callback=self.parse_order,
                    errback=self.error_back
                )
        else:
            self.logger.error("商品池空")

    def parse_order(self, response):
        meta = response.meta
        curr_page = meta.get('page')
        sku_id = meta.get('skuId')
        goods_id = meta.get('goodsId')

        data_json = json.loads(response.text)
        total_page = data_json.get('data', {}).get('sellLogList', {}).get('total')
        if total_page:
            order_list = parse_order(response)
            if order_list:
                self.logger.info('订单: sku=%s, page=%s, cnt=%s' % (sku_id, curr_page, len(order_list)))
                yield Box('order_item', self.batch_no, order_list)

                # 订单：逐页采集，到达终止日期停止
                if len(order_list) >= self.order_page_size:
                    page = curr_page + 1
                    yield Request(
                        url=self.order_url.format(goods_id, page),
                        meta={
                            'page': page,
                            'goodsId': goods_id,
                            'skuId': sku_id,
                        },
                        callback=self.parse_order,
                        errback=self.error_back,
                        priority=100
                    )
