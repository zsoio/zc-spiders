# -*- coding: utf-8 -*-
import math
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from zc_core.spiders.base import BaseSpider
from njsc.rules import *


class SkuSpider(BaseSpider):
    name = 'sku'
    # 常用链接
    sku_list_url = 'https://www.njsc365.com/api/index.php/list-1423.html?output=json&page={}'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 改动无效
        self.rows = 20

    def start_requests(self):
        page = 1
        sku_url = self.sku_list_url.format(page)
        yield Request(
            url=sku_url,
            meta={
                'reqType': 'sku',
                'batchNo': self.batch_no,
                'page': page
            },
            headers={
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'zh_CN',
                'Cache-Control': 'max-age=0',
                'Connection': 'keep-alive',
                'Host': 'www.njsc365.com',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36',
                # 'Cookie': 'S[GALLERY][FILTER]=nofilter; s=b930e58575068f00d8eec321a43df5ee'
            },
            callback=self.parse_sku,
            errback=self.error_back,
            priority=100
        )

    def parse_sku(self, response):
        meta = response.meta
        page = meta.get('page')
        sku_list = parse_sku(response)
        if sku_list and len(sku_list) > 0:
            self.logger.info('清单: page=%s, cnt=%s' % (page, len(sku_list)))
            yield Box("sku", self.batch_no, sku_list)

            if page == 1 and len(sku_list) >= self.rows:
                total = json.loads(response.text).get('data', {}).get('total', 0)
                total_page = math.ceil(int(total) / self.rows)
                for idx in range(2, total_page + 1):
                    sku_url = self.sku_list_url.format(idx)
                    yield Request(
                        url=sku_url,
                        meta={
                            'reqType': 'sku',
                            'batchNo': self.batch_no,
                            'page': idx
                        },
                        headers={
                            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                            'Accept-Encoding': 'gzip, deflate',
                            'Accept-Language': 'zh_CN',
                            # 'Cache-Control': 'max-age=0',
                            # 'Connection': 'keep-alive',
                            'Host': 'www.njsc365.com',
                            'Upgrade-Insecure-Requests': '1',
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36',
                        },
                        callback=self.parse_sku,
                        errback=self.error_back,
                        priority=50
                    )
            else:
                self.logger.info('无商品: page=%s' % page)
