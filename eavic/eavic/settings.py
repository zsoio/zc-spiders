# -*- coding: utf-8 -*-
BOT_NAME = 'eavic'

SPIDER_MODULES = ['eavic.spiders']
NEWSPIDER_MODULE = 'eavic.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 6
# DOWNLOAD_DELAY = 0.5
CONCURRENT_REQUESTS_PER_DOMAIN = 8
CONCURRENT_REQUESTS_PER_IP = 8

DEFAULT_REQUEST_HEADERS = {
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 UBrowser/6.2.4094.1 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'zh-CN,zh;q=0.9',
}

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'eavic.validator.BizValidator': 500
}
# 指定代理池类
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.cached_pool.CachedProxyPool'
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 3
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'zc_core.pipelines.catalog.CatalogCompletePipeline': 400,
    'zc_core.pipelines.brand.BrandCompletePipeline': 410,
    'zc_core.pipelines.supplier.SupplierCompletePipeline': 420,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
}
# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'eavic_2021'

# 品类白名单
# CATALOG_WHITE_LIST = [
#     {'catalog1Name': '办公设备'},
#     {'catalog1Name': '办公用品'},
#     {'catalog1Name': '办公家具'},
#     {'catalog1Name': '办公电器'},
#     {'catalog1Name': '数码通讯'},
#     {'catalog1Name': '办公护理'},
# ]

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 2
# 已采商品强制覆盖重采
# FORCE_RECOVER = True
