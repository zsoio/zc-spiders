# encoding:utf-8
from pymongo import UpdateOne
from zc_core.client.mongo_client import Mongo

mongo = Mongo()


# 过滤数据
def filter_data():
    update_bulk = list()
    src_list = mongo.list("sku_20210902", fields={'_id': 1, 'originPrice':1})
    for item in src_list:
        item['salePrice'] = item.get('originPrice')
        update_bulk.append(UpdateOne({'_id': item.get('_id')}, {'$set': item}, upsert=False))
    print(len(update_bulk))
    mongo.bulk_write('data_20210826', update_bulk)
    mongo.close()
    print('任务完成~')


if __name__ == '__main__':
    filter_data()
