from zc_core.client.mongo_client import Mongo
from pymongo import UpdateOne

mongo = Mongo()


# encoding:utf-8
def fix_data():
    update_bulk = list()
    src_list = mongo.list('item_data_pool')
    for item in src_list:
        _id = item.get('_id')
        sku_name = item.get('skuName')
        update_bulk.append(UpdateOne({'skuName': sku_name}, {'$set': item}, upsert=False))
    print(len(update_bulk))
    mongo.bulk_write(f'20210722', update_bulk)
    print('任务完成~')
    mongo.close()


if __name__ == '__main__':
    fix_data()
