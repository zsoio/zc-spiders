# # -*- coding: utf-8 -*-
# import math
# from scrapy import Request
# from zc_core.util.batch_gen import time_to_batch_no
# from zc_core.util.http_util import retry_request
# from scrapy.exceptions import IgnoreRequest
# from zc_core.dao.catalog_dao import CatalogDao
# from zc_core.spiders.base import BaseSpider
# from chdtp.rules import *
#
#
# class SkuMroSpider(BaseSpider):
#     name = "sku_mro"
#     custom_settings = {
#         'CONCURRENT_REQUESTS': 12,
#         'DOWNLOAD_DELAY': 0.5,
#         'CONCURRENT_REQUESTS_PER_DOMAIN': 12,
#         'CONCURRENT_REQUESTS_PER_IP': 12,
#     }
#
#     sku_url = 'https://www.chdtp.com/hdsc/wzgl/searchCpzsAction.action'
#     # 工业用品
#     catalog_mro_url = 'https://www.chdtp.com/hdsc/wzgl/inputTygypFlCpzsAction.action?spxx.fl=CA2FBA9743CEA92EF85BF2F8E7E06DA22A2FBA9743CEA921&spxx.flbh=tygyp'
#
#     def __init__(self, batchNo=None, *args, **kwargs):
#         super(SkuMroSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
#         self.page_size = 60
#         self.max_page_limit = math.ceil(9960 / self.page_size)
#         self.supplier_list = []
#
#     def _build_list_req(self, callback, page, brand_name, supplier_name=''):
#         return scrapy.FormRequest(
#             url=self.sku_url,
#             method="POST",
#             meta={
#                 'reqType': 'sku',
#                 'page': page,
#                 'batchNo': self.batch_no,
#                 'brandName': brand_name,
#                 'supplierName': supplier_name,
#             },
#             headers={
#                 'Content-Type': 'application/x-www-form-urlencoded'
#             },
#             formdata={
#                 'spxx.pp': brand_name,
#                 'spxx.meifmena': supplier_name,
#                 'selectorder': '',
#                 'spxx.fl': 'tyid',
#                 'spxx.flbh': '000002,000003,000004,000005,000006,000007,000008,000009,000010,000011,000012,000013',
#                 'selectkey': '',
#                 'spxx.fromxsjg': '',
#                 'spxx.endxsjg': '',
#                 'spxx.searchstr': '',
#                 'mapstr': '',
#                 'page.pageSize': str(self.page_size),
#                 'page.currentpage': str(page),
#                 'searchmeifids': '89744,55943,47885'
#             },
#             callback=callback,
#             errback=self.error_back,
#         )
#
#     def start_requests(self):
#         yield Request(
#             url=self.catalog_mro_url,
#             meta={
#                 'reqType': 'catalog',
#                 'batchNo': self.batch_no
#             },
#             callback=self.parse_office_catalog,
#             errback=self.error_back,
#         )
#
#     def parse_office_catalog(self, response):
#         brand_list = parse_brand(response)
#         if brand_list and len(brand_list) > 0:
#             self.logger.info('品牌1: count=%s' % len(brand_list))
#             yield Box('brand', self.batch_no, brand_list)
#         else:
#             self.logger.error('无品牌1: [office] url -> [%s]' % self.catalog_mro_url)
#
#         supplier_list = parse_supplier(response)
#         if supplier_list and len(supplier_list) > 0:
#             self.logger.info('供应商1: count=%s' % len(supplier_list))
#             self.supplier_list = supplier_list
#             yield Box('supplier', self.batch_no, supplier_list)
#         else:
#             self.logger.error('供应商1: [office] url -> [%s]' % self.catalog_mro_url)
#
#         if brand_list and supplier_list:
#             page = 1
#             for brand in brand_list:
#                 yield self._build_list_req(callback=self.parse_sku_list, page=page, brand_name=brand.get('name'))
#
#     def parse_sku_list(self, response):
#         meta = response.meta
#         cur_page = meta.get("page")
#         brand_name = meta.get("brandName")
#         supplier_name = meta.get("supplierName") or ''
#
#         sku_list = parse_sku_list(response)
#         if sku_list:
#             if not supplier_name:
#                 self.logger.info("清单1: br=%s, total=%s" % (brand_name, len(sku_list)))
#             else:
#                 self.logger.info("清单2: br=%s, sp=%s, total=%s" % (brand_name, supplier_name, len(sku_list)))
#             yield Box("sku", self.batch_no, sku_list)
#
#             # 后续分页
#             if cur_page == 1:
#                 total_page = parse_total_page(response, self.page_size)
#                 self.logger.info("总页数: br=%s, sp=%s, total=%s" % (brand_name, supplier_name, total_page))
#
#                 if total_page < self.max_page_limit:
#                     for page in range(2, total_page + 1):
#                         yield self._build_list_req(callback=self.parse_sku_list, page=page, brand_name=brand_name,
#                                                    supplier_name=supplier_name)
#                 else:
#                     if not supplier_name and self.supplier_list:
#                         self.logger.info("超限1: br=%s, total=%s" % (brand_name, total_page))
#                         page = 1
#                         for supplier_name in self.supplier_list:
#                             yield self._build_list_req(callback=self.parse_sku_list, page=page, brand_name=brand_name,
#                                                        supplier_name=supplier_name)
#                     elif supplier_name:
#                         self.logger.info("超限2: br=%s, sp=%s, total=%s" % (brand_name, supplier_name, total_page))
#
#         else:
#             self.logger.info("无商品: brand=%s, page=%s" % (brand_name, cur_page))
#
#
