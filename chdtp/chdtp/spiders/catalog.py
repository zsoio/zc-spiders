# -*- coding: utf-8 -*-
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from chdtp.rules import *
from zc_core.spiders.base import BaseSpider


class CatalogSpider(BaseSpider):
    name = 'catalog'

    # 办公分类链接
    catalog_office_url = 'https://www.chdtp.com/hdsc/wzgl/inputFlCpzsAction.action?spxx.fl=EDB367F1AF25FB13DFD25F8A605B00C281F5B3F2FA0B02D2&spxx.flbh=000001'
    # 工业用品
    catalog_mro_url = 'https://www.chdtp.com/hdsc/wzgl/inputTygypFlCpzsAction.action?spxx.fl=CA2FBA9743CEA92EF85BF2F8E7E06DA22A2FBA9743CEA921&spxx.flbh=tygyp'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(CatalogSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        yield Request(
            url=self.catalog_office_url,
            meta={
                'reqType': 'catalog',
                'batchNo': self.batch_no
            },
            callback=self.parse_office_catalog,
            errback=self.error_back,
            priority=100
        )
        yield Request(
            url=self.catalog_mro_url,
            meta={
                'reqType': 'catalog',
                'batchNo': self.batch_no
            },
            callback=self.parse_mro_catalog,
            errback=self.error_back,
            priority=100
        )

    def parse_office_catalog(self, response):
        cat_list = parse_office_catalog(response)
        if cat_list and len(cat_list) > 0:
            self.logger.info('分类1: count=%s' % len(cat_list))
            yield Box('catalog', self.batch_no, cat_list)
        else:
            self.logger.error('无分类1: [office] url -> [%s]' % self.catalog_office_url)

        brand_list = parse_brand(response)
        if brand_list and len(brand_list) > 0:
            self.logger.info('品牌1: count=%s' % len(brand_list))
            yield Box('brand', self.batch_no, brand_list)
        else:
            self.logger.error('无品牌1: [office] url -> [%s]' % self.catalog_office_url)

        supplier_list = parse_supplier(response)
        if supplier_list and len(supplier_list) > 0:
            self.logger.info('供应商1: count=%s' % len(supplier_list))
            yield Box('supplier', self.batch_no, supplier_list)
        else:
            self.logger.error('供应商1: [office] url -> [%s]' % self.catalog_office_url)

    def parse_mro_catalog(self, response):
        cat_list = parse_mro_catalog(response)
        if cat_list and len(cat_list) > 0:
            self.logger.info('分类2: count=%s' % len(cat_list))
            yield Box('catalog', self.batch_no, cat_list)
        else:
            self.logger.error('无分类2: [other] url -> [%s]' % self.catalog_mro_url)

        brand_list = parse_brand(response)
        if brand_list and len(brand_list) > 0:
            self.logger.info('品牌2: count=%s' % len(brand_list))
            yield Box('brand', self.batch_no, brand_list)
        else:
            self.logger.error('无品牌2: [office] url -> [%s]' % self.catalog_mro_url)

        supplier_list = parse_supplier(response)
        if supplier_list and len(supplier_list) > 0:
            self.logger.info('供应商2: count=%s' % len(supplier_list))
            yield Box('supplier', self.batch_no, supplier_list)
        else:
            self.logger.error('供应商2: [office] url -> [%s]' % self.catalog_mro_url)


