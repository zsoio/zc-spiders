# -*- coding: utf-8 -*-
from scrapy.exceptions import IgnoreRequest
from zc_core.dao.item_data_dao import ItemDataDao
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from zc_core.dao.batch_dao import BatchDao
from zc_core.util.done_filter import DoneFilter
from chdtp.utils.login import SeleniumLogin
from chdtp.rules import *
from zc_core.spiders.base import BaseSpider


# 有销量的商品价格
class PriceSpider(BaseSpider):
    name = "price"
    # 价格url
    price_url = 'https://www.chdtp.com/hdsc/wzgl/getJgDetialCpzsAction.action'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(PriceSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)
        # 避免重复采集
        self.done_filter = DoneFilter(self.batch_no)

    def start_requests(self):
        cookies = SeleniumLogin().get_cookies()
        # cookies = {'JSESSIONID': '0000kWhv08FblZbRe-PbmYsHBk4:1do0npen2'}
        if not cookies:
            self.logger.error('init cookie failed...')
            return
        self.logger.info('init cookie: %s', cookies)

        # sku_list = ItemDataDao().get_batch_data_list(batch_no=self.batch_no, fields={'_id': 1, 'linkId': 1},
        #                                              query={'soldCount': {'$gt': 0}, 'salePrice': {'$exists': False}})
        sku_list=ItemDataDao().get_batch_data_list(batch_no=self.batch_no, fields={'_id': 1, 'linkId': 1},
                                          query={"$and": [{"salePrice": {"$exists": False}},
                                                          {"catalog2Name": {"$in": ["办公设备", "办公耗材"]}}]})
        self.logger.info('目标: %s' % (len(sku_list)))
        for sku in sku_list:
            sku_id = sku.get("_id")
            link_id = sku.get("linkId")
            yield scrapy.FormRequest(
                url=self.price_url,
                method='POST',
                meta={
                    'reqType': 'price',
                    'batchNo': self.batch_no,
                    'skuId': sku_id,
                },
                headers={
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                formdata={
                    'spxx.spxxidjmstr': link_id
                },
                cookies=cookies,
                callback=self.parse_price,
                errback=self.error_back,
            )

    def parse_price(self, response):
        meta = response.meta
        sku_id = meta.get("skuId")
        item = parse_item_price(response)
        if item and item.get('salePrice'):
            self.logger.info('价格: sku=%s' % sku_id)
            yield item
        else:
            self.logger.info('无价: sku=%s' % sku_id)


