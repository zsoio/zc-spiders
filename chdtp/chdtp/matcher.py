# -*- coding: utf-8 -*-
import re

def match_sku_id(href):
    #javascript:toOpenHtml('6B507A3262006D73FA019AB7E39A1BC54001A001A001A008')
    if href:
        arr = re.findall(r"toOpenHtml\('([\w]*)'\)",href.strip())
        if len(arr) > 0:
            return arr[0].strip()

def match_supplier_id(e):
    # funGoDzd('215B15A7DBF3124EFFBCEFF53DB26A3621B57B32EFCF5D22')
    if e:
        arr = re.findall(r"funGoDzd\('([\w]*)'\)", e.strip())
        if len(arr) > 0:
            return arr[0].strip()

# 测试
if __name__ == '__main__':
    pass
