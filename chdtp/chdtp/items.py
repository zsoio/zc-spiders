# -*- coding: utf-8 -*-
from scrapy.exceptions import DropItem
from zc_core.model.items import ItemData


class ChdtpItemData(ItemData):
    def validate(self):
        if not self.get('_id') and not self.get('skuId'):
            raise DropItem("ItemData Error [skuId]")
        if not self.get('batchNo'):
            raise DropItem("ItemData Error [batchNo]")
        if not self.get('skuName'):
            raise DropItem("ItemData Error [skuName] -> (%s)" % self.get('skuId'))
        # if self.get('salePrice') is None:
        #     raise DropItem("ItemData Error [salePrice] -> (%s)" % self.get('skuId'))
        # if self.get('originPrice') is None:
        #     raise DropItem("ItemData Error [originPrice] -> (%s)" % self.get('skuId'))
        # if self.get('salePrice') <= 0:
        #     raise DropItem("ItemData Error [salePrice <= 0] -> (%s)" % self.get('skuId'))
        # if self.get('originPrice') <= 0:
        #     raise DropItem("ItemData Error [originPrice <= 0] -> (%s)" % self.get('skuId'))
        return True
