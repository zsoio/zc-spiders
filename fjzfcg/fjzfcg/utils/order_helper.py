# -*- coding: utf-8 -*-
import logging
from zc_core.client.mongo_client import Mongo

logger = logging.getLogger('OrderHelper')


class OrderHelper(object):
    """
    已采集商品过滤器
    用于全量商品明细重复采集时，过滤已采过的商品，避免重复采集
    """

    def __init__(self):
        self.sku_mapper = dict()
        self.spu_mapper = dict()
        self.supplier_mapper = dict()

        items = Mongo().list('item_data_pool')
        for it in items:
            sku_name = it.get('skuName')
            sp_name = it.get('supplierName')
            if sku_name and sp_name:
                sku_name = sku_name.strip()
                sp_name = sp_name.strip()
                key = sp_name + '_' + sku_name
                self.sku_mapper[key] = it.get('_id')
                if sku_name not in self.spu_mapper:
                    self.spu_mapper[sku_name] = it
        logger.info('商品数量：%s' % len(self.sku_mapper))

        suppliers = Mongo().list('supplier_pool')
        for sp in suppliers:
            self.supplier_mapper[sp.get('name').strip()] = sp.get('_id')
        logger.info('供应商数量：%s' % len(self.supplier_mapper))

    def find_sku_id(self, sp_name, sku_name):
        if sp_name and sku_name:
            return self.sku_mapper.get(sp_name + '_' + sku_name, '')

        return ''

    def find_spu(self, sku_name):
        if sku_name:
            return self.spu_mapper.get(sku_name, None)

        return None

    def find_supplier_id(self, sp_name):
        if sp_name:
            sp_name = sp_name.strip()
            return self.supplier_mapper.get(sp_name, '')

        return ''


# 测试
if __name__ == '__main__':
    helper = OrderHelper()
    print(helper.find_spu({
        'skuName': '农夫山泉量贩装饮用天然水380ml*12瓶透明装瓶装水',
    }))
