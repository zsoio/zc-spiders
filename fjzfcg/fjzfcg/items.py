# -*- coding: utf-8 -*-
import scrapy
from scrapy.exceptions import DropItem
from zc_core.model.items import BaseData


# 订单数据模型
class NoMatchOrderItem(BaseData):
    _id = scrapy.Field()
    # 订单明细编号
    id = scrapy.Field()
    # 主订单编号
    orderId = scrapy.Field()
    # 专区内部主订单编号
    orderCode = scrapy.Field()
    # 交易类型
    tradeType = scrapy.Field()
    # 商品编码
    skuId = scrapy.Field()
    # 数量
    count = scrapy.Field()
    # 金额
    amount = scrapy.Field()
    # 采购单位编号
    deptId = scrapy.Field()
    # 采购单位
    orderDept = scrapy.Field()
    # 采购人
    orderUser = scrapy.Field()
    # 订单收货地址
    orderAddr = scrapy.Field()
    # 订单收货人电话
    orderTel = scrapy.Field()
    # 采购时间[本地时间]
    orderTime = scrapy.Field()
    # 供应商编号
    supplierId = scrapy.Field()
    # 供应商名称
    supplierName = scrapy.Field()

    # 商品名称
    skuName = scrapy.Field()
    # 品牌名称
    brandName = scrapy.Field()
    # 单价
    unitPrice = scrapy.Field()
    # 品目名称
    catalogName = scrapy.Field()

    def validate(self):
        if not self.get('_id') and not self.get('id'):
            raise DropItem("NoMatchOrderItem Error [id]")
        if not self.get('batchNo'):
            raise DropItem("NoMatchOrderItem Error [batchNo]")
        return True