# code:utf8
import json
import logging
import math
from pyquery import PyQuery
from datetime import datetime
from zc_core.pipelines.helper.brand_helper import BrandHelper
from zc_core.pipelines.helper.catalog_helper import CatalogHelper
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.model.items import OrderItem, Order, Catalog, Brand, Supplier, Sku, ItemData
from zc_core.util.common import parse_number, parse_time
from zc_core.util.encrypt_util import md5
from fjzfcg.items import NoMatchOrderItem
from fjzfcg.matcher import *

logger = logging.getLogger('rule')


# 解析catalog列表
def parse_catalog(response):
    jpy = PyQuery(response.text)
    div_list = jpy('div.class_a')

    cats = list()
    for cat1_div in div_list.items():
        # 一级分类
        cat1_a = cat1_div('h2 a')
        cat1 = _build_catalog(cat1_a, '', 1)
        cats.append(cat1)
        # 二级分类
        cat2_divs = cat1_div('div.class_b')
        for cat2_div in cat2_divs.items():
            cat2_a = cat2_div('h3 a')
            cat2 = _build_catalog(cat2_a, cat1.get('catalogId'), 2)
            cats.append(cat2)
            # 三级分类
            cat3_links = cat2_div('div.class_c a')
            for cat3_link in cat3_links.items():
                cat3 = _build_catalog(cat3_link, cat2.get('catalogId'), 3)
                cats.append(cat3)
    return cats


def _build_catalog(cat_a, parent_id, level):
    url = cat_a.attr('href')
    cat_id = match_cat_id(url)
    cat_name = cat_a.text().strip()

    cat = Catalog()
    cat['catalogId'] = cat_id
    cat['catalogName'] = cat_name
    cat['parentId'] = parent_id
    cat['level'] = level
    if level == 3:
        cat['leafFlag'] = 1
    else:
        cat['leafFlag'] = 0
    cat['linkable'] = 1

    return cat


# 解析brand home
def parse_brand_home(response):
    jpy = PyQuery(response.text)
    a_list = jpy('div#firstfilte a')
    typeids = list()
    for br in a_list.items():
        typeids.append(br.attr('typeid'))

    return typeids


# 解析brand列表
def parse_brand(response):
    meta = response.meta
    jpy = PyQuery(response.text)
    em_list = jpy('div.brand-list-box[data-filter="allbrand"] ul li em')
    brands = list()
    for br in em_list.items():
        brand = Brand()
        brand['id'] = br.attr('value')
        brand['name'] = br.text().strip()
        brand['batchNo'] = meta.get('batchNo')
        brands.append(brand)

    return brands


# 解析shop列表
def parse_shop_page_total(response):
    js = json.loads(response.text)
    if js and 'content' in js:
        return js.get('content').get('totalPage')

    return 0


# 解析shop列表
def parse_shop_list(response):
    meta = response.meta
    js = json.loads(response.text)
    jpy = PyQuery(js.get('content').get('ajaxLoadHtml'))
    item_list = jpy('div.shop-content')
    stores = list()
    for item in item_list.items():
        sp = Supplier()
        sp['id'] = match_supplier_id(item('a').attr('href'))
        sp['name'] = item('span').eq(0).text()
        sp['batchNo'] = meta.get('batchNo')
        stores.append(sp)

    return stores


# 解析sku列表
def parse_spu(response):
    meta = response.meta
    batch_no = meta.get('batchNo')
    jpy = PyQuery(response.text)
    uls = jpy('div.smallgoods ul')
    sku_list = list()
    for ul in uls.items():
        sku = Sku()
        url = ul('li.goodslook a').attr('href')
        price = ul('li.goodslook strong').text()
        sale_price = parse_number(price)
        if url and sale_price > 0:
            sku['batchNo'] = batch_no
            sku['spuId'] = match_sku_id(url)
            sku['salePrice'] = parse_number(price)
            sku_list.append(sku)
        else:
            logger.info('无效商品：%s' % sku)

    return sku_list


def parse_total_page(response):
    jpy = PyQuery(response.text)
    page = jpy('div.shop_page span a:contains("末页")')
    if page:
        return int(page.attr('onclick').replace('return gotoPage(', '').replace(')', ''))
    else:
        return 0


# 解析ItemData
def parse_item_data(response):
    brand_helper = BrandHelper()
    cat_helper = CatalogHelper()
    meta = response.meta
    jpy = PyQuery(response.text)
    results = list()

    batch_no = meta.get('batchNo')
    detail_lis = jpy('div.detailstop ul li')
    brand = detail_lis('span:contains("商品品牌：")').next().text()
    catalog_name = detail_lis('span:contains("所属采购品目：")').next().text()
    if catalog_name == '视频会议多点控制器':
        catalog_name = '视频会议多点控制器（MCU）'
    elif catalog_name == '载货汽车（含自卸汽车）':
        catalog_name = '载货汽车（皮卡）'
    unit = detail_lis('span:contains("计量单位：")').next().text()
    origin_price = parse_number(detail_lis('span:contains("市场价：")').next().text())
    img = jpy('div.zoomPad img').eq(0)

    sames = jpy('div.goodstyletwo input.theSameGoodsId')
    for same in sames.items():
        sku_id = same.attr('value')
        if not sku_id:
            continue

        row = same.next()
        sale_price = parse_number(row('ul.goodsul li.goodsli1 span.goods2shop strong').text().replace('元', ''))
        supplier = row('ul.goodsul li.goodsli1 span:first span:eq(1) strong a')
        title = row('ul.goodsul li.goodsli1 span.goods2name a')
        spu_id = match_spu_id(title.attr('href'))
        # -------------------------------------------------
        result = ItemData()
        result['batchNo'] = batch_no
        result['skuId'] = sku_id
        result['spuId'] = spu_id
        result['skuName'] = title.text().strip()
        if img:
            result['skuImg'] = img.attr('src').strip()
        result['catalog3Name'] = catalog_name
        cat_helper.fill(result)
        result['salePrice'] = parse_number(sale_price)
        origin_price = parse_number(origin_price)
        if origin_price == -1:
            result['originPrice'] = result['salePrice']
        else:
            result['originPrice'] = origin_price
        result['unit'] = unit
        if brand:
            result['brandName'] = brand.strip()
            brand_helper.fill(result)
        result['supplierId'] = match_supplier_id(supplier.attr('href'))
        result['supplierName'] = supplier.text().strip()
        result['genTime'] = datetime.utcnow()
        results.append(result)
        # -------------------------------------------------

    return results


# 解析order列表
def parse_order_list(response):
    jpy = PyQuery(response.text)
    pages = list()
    lis = jpy('ul.aritlistul li')
    for li in lis.items():
        order = Order()
        url = li('a').attr('href')
        order_time = parse_time(li('span').text(), fmt='%Y-%m-%d')
        id = match_order_id(url)
        order['id'] = id
        order['url'] = url
        order['orderTime'] = order_time
        order['batchNo'] = time_to_batch_no(order_time)
        order['genTime'] = datetime.utcnow()
        pages.append(order)

    return pages


# 解析sku列表
def parse_order_item(response, order_helper):
    meta = response.meta
    order_id = meta.get('orderId')

    order_list = list()
    no_match_order_list = list()
    jpy = PyQuery(response.text)
    art = jpy('div.artilist')
    order_time = parse_time(art('h2 span.artidate').text().strip())
    order_code = art('div.artip span#c_order_id').text().strip()
    order_dept = art('div.artip span#c_ORGAN_NAME').text().strip()
    if not order_dept:
        order_dept = art('div.artip span:contains("甲方(采购人)：") + span').text().strip()
    sp_name = art('div.artip span#c_store_name').text().strip()
    if not sp_name:
        sp_name = art('div.artip span:contains("乙方(供应商)：") + span').text().strip()
    sp_id = order_helper.find_supplier_id(sp_name)
    order_addr = art('div.artip span#c_deliveryAddress').text().strip()
    order_user = art('div.artip span#c_consignee').text().strip()
    order_user_tel = art('div.artip span#c_consignee_tel').text().strip()

    item_tables = art('div.artip table')
    for idx, item_table in enumerate(item_tables.items()):
        trs = item_table('tbody tr')
        if trs:
            kv = dict()
            for tr in trs.items():
                k = tr('td').eq(0).text().strip().replace(' ', '')
                v = tr('td').eq(1).text().strip()
                kv[k] = v
            sku_name = kv.get('规格型号')
            count = math.ceil(parse_number(kv.get('数量')))
            amount = parse_number(kv.get('总价（元）'))
            sku_id = order_helper.find_sku_id(sp_name, sku_name)

            order = OrderItem()
            order['id'] = '{}_{}'.format(order_id, idx)
            order['orderId'] = order_id
            order['skuId'] = sku_id
            order['count'] = count
            order['amount'] = amount
            order['deptId'] = md5(order_dept)
            order['orderDept'] = order_dept
            order['orderCode'] = order_code
            order['supplierName'] = sp_name
            order['supplierId'] = sp_id
            order['orderTime'] = order_time
            order['orderAddr'] = order_addr
            order['orderUser'] = '{} {}'.format(order_user, order_user_tel)
            order['orderTel'] = order_user_tel
            order['batchNo'] = time_to_batch_no(order_time)
            order['genTime'] = datetime.utcnow()

            if sku_id and count > 0 and amount > 0:
                order_list.append(order)
            else:
                brand_cat = kv.get('货物名称').split(' 牌')
                unit_price = parse_number(kv.get('单价（元）'))
                no_match_order = NoMatchOrderItem()
                no_match_order.update(order)
                no_match_order['skuName'] = sku_name
                no_match_order['unitPrice'] = unit_price
                if brand_cat and len(brand_cat) == 2:
                    no_match_order['brandName'] = brand_cat[0].strip()
                    no_match_order['catalogName'] = brand_cat[1].strip()
                no_match_order_list.append(no_match_order)

    return order_list, no_match_order_list
