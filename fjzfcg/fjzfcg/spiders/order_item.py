# -*- coding: utf-8 -*-
import random
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.model.items import OrderStatus, Box
from zc_core.dao.order_pool_dao import OrderPoolDao
from zc_core.util.http_util import retry_request
from zc_core.spiders.base import BaseSpider
from fjzfcg.rules import *
from fjzfcg.utils.order_helper import OrderHelper


class OrderItemSpider(BaseSpider):
    name = 'order_item'

    # custom_settings = {
    #     'CONCURRENT_REQUESTS': 4,
    #     'CONCURRENT_REQUESTS_PER_DOMAIN': 4,
    #     'CONCURRENT_REQUESTS_PER_IP': 4,
    # }

    def __init__(self, batchNo=None, *args, **kwargs):
        super(OrderItemSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.order_helper = OrderHelper()

    def start_requests(self):
        # 处理订单列表
        order_list = OrderPoolDao().get_todo_list(self.batch_no)
        if order_list:
            self.logger.info('目标: %s' % (len(order_list)))
            random.shuffle(order_list)
            for order in order_list:
                order_id = order.get('_id')
                order_url = order.get('url')
                # 采集order明细页
                yield Request(
                    url=order_url,
                    callback=self.parse_order_item,
                    errback=self.error_back,
                    meta={
                        'reqType': 'order',
                        'orderId': order_id,
                    },
                    headers={
                        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                        'Accept-Encoding': 'gzip, deflate',
                        'Accept-Language': 'zh-CN,zh;q=0.9',
                        'Cache-Control': 'max-age=0',
                        'Proxy-Connection': 'keep-alive',
                        'Upgrade-Insecure-Requests': '1',
                    },
                    dont_filter=True
                )

    def parse_order_item(self, response):
        meta = response.meta
        order_id = meta.get('orderId')
        # 处理订单列表
        order_list, no_match_order_list = parse_order_item(response, self.order_helper)
        if order_list:
            self.logger.info('订单: order=%s, item_cnt=%s' % (order_id, len(order_list)))
            yield Box('order_item', self.batch_no, order_list)
            # 标记已采
            order_time = order_list[0].get('orderTime')
            order_status = OrderStatus()
            order_status['id'] = order_id
            order_status['orderTime'] = order_time
            order_status['batchNo'] = time_to_batch_no(order_time)
            order_status['workStatus'] = 1
            yield order_status

        if no_match_order_list:
            self.logger.info('未匹配: order=%s, cnt=%s' % (order_id, len(no_match_order_list)))
            for item in no_match_order_list:
                yield item
