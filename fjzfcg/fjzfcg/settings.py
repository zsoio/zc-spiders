# -*- coding: utf-8 -*-
BOT_NAME = 'fjzfcg'

SPIDER_MODULES = ['fjzfcg.spiders']
NEWSPIDER_MODULE = 'fjzfcg.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 12
# DOWNLOAD_DELAY = 0.1
CONCURRENT_REQUESTS_PER_DOMAIN = 12
CONCURRENT_REQUESTS_PER_IP = 12

# COOKIES_ENABLED = False
DEFAULT_REQUEST_HEADERS = {
    'Host': '120.35.30.176',
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 UBrowser/6.2.4094.1 Safari/537.36',
    'Referer': 'http://120.35.30.176/shopping/index.htm',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.8',
}

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'fjzfcg.validator.BizValidator': 500
}
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 代理积分阈值
PROXY_SCORE_LIMIT = 5

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
    'fjzfcg.pipelines.OrderPipeline': 520,
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'fjzfcg_2019'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 5
# 订单列表页采集页数限制
MAX_ORDER_PAGE = 300
# 输出
OUTPUT_ROOT = '/work/fjzfcg/'
# 已采商品强制覆盖重采
# FORCE_RECOVER = True
