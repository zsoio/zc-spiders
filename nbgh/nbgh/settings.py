# -*- coding: utf-8 -*-
BOT_NAME = 'nbgh'

SPIDER_MODULES = ['nbgh.spiders']
NEWSPIDER_MODULE = 'nbgh.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 8
# DOWNLOAD_DELAY = 0.1
CONCURRENT_REQUESTS_PER_DOMAIN = 8
CONCURRENT_REQUESTS_PER_IP = 8

DEFAULT_REQUEST_HEADERS = {
    'Host': 'mp.nbgh.gov.cn',
    'Accept-Encoding': 'gzip, deflate, br',
    'Cookie': 'auth._token.local2=Bearer%20eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJhcHAiLCJtYXJrSWQiOiIwQjJERTVCMjYxMDg5MjFCMkI5RkEzMTRGREE5RTM5NCIsImV4cCI6MTYzMjEyODk2MSwiaWF0IjoxNjMyMTA3MzYxfQ.QaDTqVVl8-XaL4ojsdHL2cz1er3t8JNc0Ubw4dOj2-0; auth._token_expiration.local2=1632128961000; auth.strategy=local2; auth._token.local1=false; auth._token_expiration.local1=false',
    'Connection': 'keep-alive',
    'Accept': 'application/json, text/plain, */*',
    'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_8 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJhcHAiLCJtYXJrSWQiOiIwQjJERTVCMjYxMDg5MjFCMkI5RkEzMTRGREE5RTM5NCIsImV4cCI6MTYzMjEyODk2MSwiaWF0IjoxNjMyMTA3MzYxfQ.QaDTqVVl8-XaL4ojsdHL2cz1er3t8JNc0Ubw4dOj2-0',
    'Referer': 'https://mp.nbgh.gov.cn/wechatPage/shop/15/cate/one',
    'Accept-Language': 'zh-cn',
}

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    # 'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'nbgh.validator.BizValidator': 500,
}
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 1
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 1
# 下载超时
DOWNLOAD_TIMEOUT = 30
# 响应重试状态码
CUSTOM_RETRY_CODES = [405]

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'zc_core.pipelines.catalog.CatalogCompletePipeline': 400,
    'zc_core.pipelines.supplier.SupplierCompletePipeline': 420,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'nbgh_2020'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 2
# 订单采集截止天数
ORDER_DEADLINE_DAYS = 90
# 允许响应内容为空（兼容订单）
ALLOW_EMPTY_RESPONSE = True
# 已采商品强制覆盖重采
# FORCE_RECOVER = True

MISS_SUPPLIER_MAP = {
    '202011130946': '乌鲁木齐捷昌瑞商贸有限公司',
    '202011130938': '南京酷赠网络科技有限公司',
    '202008210301': '广州市康北环保科技有限公司',
    '202011120900': '三木高科科技（北京）有限公司',
}
# 白名单
# CATALOG_WHITE_LIST = [
#     {'catalog2Name': '打印耗材'},
#     {'catalog2Name': '办公设备'},
# ]
