# # -*- coding: utf-8 -*-
# import copy
# from zc_core.spiders.base import BaseSpider
# from scrapy import Request
# from scrapy.exceptions import IgnoreRequest
# from scrapy.utils.project import get_project_settings
# from zc_core.dao.sku_pool_dao import SkuPoolDao
# from zc_core.util.batch_gen import time_to_batch_no
# from zc_core.util.http_util import retry_request
# from zc_core.dao.batch_dao import BatchDao
# from zc_core.util.done_filter import DoneFilter
#
# from nbgh.rules import *
#
#
# class FullSpider(BaseSpider):
#     name = "full"
#     # 详情页url
#     shop_list_url = "https://mp.nbgh.gov.cn/wechatPage/api/api2/shop"
#     sp_cat_url = "https://mp.nbgh.gov.cn/wechatPage/api/api2/cate/cateone?shopId={}"
#     cat_sku_url = "https://mp.nbgh.gov.cn/wechatPage/api/api2/prod/cateone?pageSize={}&pageNo={}&cateId={}&query="
#     search_sku_url = "https://mp.nbgh.gov.cn/wechatPage/api/api2/prod/search?pageSize={}&pageNo={}&type=fp&search=+&query="
#
#     def __init__(self, batchNo=None, *args, **kwargs):
#         super(FullSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
#         # 创建批次记录
#         BatchDao().create_batch(self.batch_no)
#         self.page_size = 100
#
#     def start_requests(self):
#         page = 1
#         yield Request(
#             url=self.search_sku_url.format(self.page_size, page),
#             meta={
#                 'reqType': 'sku',
#                 'batchNo': self.batch_no,
#                 'page': page,
#             },
#             # headers={
#             # },
#             callback=self.parse_sku_list,
#             errback=self.error_back
#         )
#
#     def parse_sku_list(self, response):
#         curr_page = response.meta.get('page')
#         data_list = parse_sku_list(response)
#         if data_list:
#             self.logger.info('清单: page=%s, cnt=%s' % (curr_page, len(data_list)))
#             yield Box('item', self.batch_no, data_list)
#
#             next_page = curr_page + 1
#             yield Request(
#                 url=self.search_sku_url.format(self.page_size, next_page),
#                 meta={
#                     'reqType': 'sku',
#                     'batchNo': self.batch_no,
#                     'page': next_page,
#                 },
#                 # headers={
#                 # },
#                 callback=self.parse_sku_list,
#                 errback=self.error_back
#             )
#         else:
#             self.logger.info('分页为空: page=%s' % curr_page)
