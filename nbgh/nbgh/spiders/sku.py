# -*- coding: utf-8 -*-
import copy
from zc_core.spiders.base import BaseSpider
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from scrapy.utils.project import get_project_settings
from zc_core.dao.sku_pool_dao import SkuPoolDao
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from zc_core.dao.batch_dao import BatchDao
from zc_core.util.done_filter import DoneFilter

from nbgh.rules import *


class SkuSpider(BaseSpider):
    name = "sku"
    # 详情页url
    shop_list_url = "https://mp.nbgh.gov.cn/wechatPage/api/api2/shop"
    sp_cat_url = "https://mp.nbgh.gov.cn/wechatPage/api/api2/cate/cateone?shopId={}"
    cat_sku_url = "https://mp.nbgh.gov.cn/wechatPage/api/api2/prod/cateone?pageSize={}&pageNo={}&cateId={}&query="

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)
        self.page_size = 100

    def start_requests(self):
        yield Request(
            url=self.shop_list_url,
            meta={
                'reqType': 'shop',
                'batchNo': self.batch_no,
            },
            # headers={
            # },
            callback=self.parse_shop_list,
            errback=self.error_back
        )

    def parse_shop_list(self, response):
        sp_list = parse_shop_list(response)
        if sp_list:
            self.logger.info('供应商: cnt=%s' % len(sp_list))
            yield Box('supplier', self.batch_no, sp_list)

            for sp in sp_list:
                yield Request(
                    url=self.sp_cat_url.format(sp.get('id')),
                    meta={
                        'reqType': 'catalog',
                        'batchNo': self.batch_no,
                        "supplierId": sp.get('id'),
                        "supplierName": sp.get('name'),
                    },
                    # headers={
                    # },
                    callback=self.parse_shop_cat,
                    errback=self.error_back
                )

    def parse_shop_cat(self, response):
        meta = response.meta
        sp_id = meta.get('supplierId')
        sp_name = meta.get('supplierName')
        cat_list = parse_shop_cat(response)
        if cat_list:
            self.logger.info('分类: sp=%s, cnt=%s' % (sp_id, len(cat_list)))
            yield Box('catalog', self.batch_no, cat_list)

            for cat in cat_list:
                page = 1
                yield Request(
                    url=self.cat_sku_url.format(self.page_size, page, cat.get('catalogId')),
                    meta={
                        'reqType': 'sku',
                        'batchNo': self.batch_no,
                        "supplierId": sp_id,
                        "supplierName": sp_name,
                        "catalogId": cat.get('catalogId'),
                        "catalogName": cat.get('catalogName'),
                        'page': page,
                    },
                    # headers={
                    # },
                    callback=self.parse_sku_list,
                    errback=self.error_back
                )

    def parse_sku_list(self, response):
        meta = response.meta
        sp_id = meta.get('supplierId')
        sp_name = meta.get('supplierName')
        cat_id = meta.get('catalogId')
        cat_name = meta.get('catalogName')
        curr_page = meta.get('page')
        data_list = parse_sku_list(response)
        if data_list:
            self.logger.info('清单: sp=%s, cat=%s, page=%s, cnt=%s' % (sp_id, cat_id, curr_page, len(data_list)))
            yield Box('item', self.batch_no, data_list)

            # 下一页
            if len(data_list) >= self.page_size:
                next_page = curr_page + 1
                yield Request(
                    url=self.cat_sku_url.format(self.page_size, next_page, cat_id),
                    meta={
                        'reqType': 'sku',
                        'batchNo': self.batch_no,
                        "supplierId": sp_id,
                        "supplierName": sp_name,
                        "catalogId": cat_id,
                        "catalogName": cat_name,
                        'page': next_page,
                    },
                    # headers={
                    # },
                    callback=self.parse_sku_list,
                    errback=self.error_back
                )
            else:
                self.logger.info('分页结束: sp=%s, cat=%s, page=%s, cnt=%s' % (sp_id, cat_id, curr_page, len(data_list)))
        else:
            self.logger.info('分页为空: sp=%s, cat=%s, page=%s' % (sp_id, cat_id, curr_page))