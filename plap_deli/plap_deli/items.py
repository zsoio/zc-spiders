# -*- coding: utf-8 -*-
import scrapy
from scrapy.exceptions import DropItem
from zc_core.model.items import BaseData, Order


# spu日志数据模型
class SpuPageLog(BaseData):
    _id = scrapy.Field()
    # 分类编号
    catId = scrapy.Field()
    # 分页
    page = scrapy.Field()
    # 明细条数
    count = scrapy.Field()
    # 批次编号
    batchNo = scrapy.Field()

    def validate(self):
        if not self.get('page'):
            raise DropItem("SpuPageLog Error [page] -> (%s)" % self.get('page'))
        if not self.get('catId'):
            raise DropItem("SpuPageLog Error [catId] -> (%s)" % self.get('catId'))
        return True


class PlapOrder(Order):
    cancelReason = scrapy.Field()
    receiptCode = scrapy.Field()
    clientType = scrapy.Field()
    spOrderCode = scrapy.Field()
    orderAddr = scrapy.Field()
    itemList = scrapy.Field()
