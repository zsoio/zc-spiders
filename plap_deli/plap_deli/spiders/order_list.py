# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from zc_core.model.items import Box
from plap_deli.rules import *
from plap_deli.utils.login import SeleniumLogin
from plap_deli.utils.done_filter import SpuPageLogFilter, MaterialHelper
from zc_core.spiders.base import BaseSpider


class OrderListSpider(BaseSpider):
    name = 'order_list'
    custom_settings = {
        'CONCURRENT_REQUESTS': 4,
        'DOWNLOAD_DELAY': 0.2,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 4,
        'CONCURRENT_REQUESTS_PER_IP': 4,
    }
    # 常用链接
    list_url = 'http://mall.plap.cn/npc/orders.html?commit=%E6%9F%A5%E8%AF%A2&page={}&q%5Bindex_day_gt%5D=20101203&q%5Bno_or_items_name_cont%5D=&q%5Bs%5D=id+desc&q%5Bworkflow_state_eq%5D=&time_range=10&utf8=%E2%9C%93'

    def __init__(self, batchNo=None, delta_hour=-12, *args, **kwargs):
        super(OrderListSpider, self).__init__(batchNo=batchNo, hourOffset=delta_hour, *args, **kwargs)
        # 避免重复采集
        self.done_filter = SpuPageLogFilter(self.batch_no)

    def start_requests(self):
        cookies = SeleniumLogin().get_cookies('order')
        # cookies = {'_session_id': '71a48017949eecccf82ee53342cbe376'}
        if not cookies:
            self.logger.error('init cookie failed...')
            return
        self.logger.info('init cookie: %s', cookies)

        page = 1
        yield Request(
            url=self.list_url.format(1),
            meta={
                'reqType': 'sku',
                'batchNo': self.batch_no,
                'page': page,
            },
            callback=self.parse_order_list,
            errback=self.error_back,
            cookies=cookies,
            priority=200,
            dont_filter=True
        )

    def parse_order_list(self, response):
        meta = response.meta
        curr_page = meta.get('page')
        order_list = parse_order_list(response)
        if order_list:
            self.logger.info('清单: page=%s, cnt=%s' % (curr_page, len(order_list)))
            yield Box('order', self.batch_no, order_list)

            # 分页
            if curr_page == 1:
                total_page = parse_total_order_page(response)
                self.logger.info('总页数: total=%s' % total_page)
                if total_page > 1:
                    for page in range(2, total_page + 1):
                        yield Request(
                            url=self.list_url.format(page),
                            meta={
                                'reqType': 'sku',
                                'batchNo': self.batch_no,
                                'page': page,
                            },
                            callback=self.parse_order_list,
                            errback=self.error_back,
                            priority=200,
                            dont_filter=True
                        )
        else:
            self.logger.info('空页: page=%s' % curr_page)
