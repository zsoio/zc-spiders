# -*- coding: utf-8 -*-
BOT_NAME = 'esgcc_sub'

SPIDER_MODULES = ['esgcc_sub.spiders']
NEWSPIDER_MODULE = 'esgcc_sub.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 16
# DOWNLOAD_DELAY = 1
CONCURRENT_REQUESTS_PER_DOMAIN = 16
CONCURRENT_REQUESTS_PER_IP = 16

DEFAULT_REQUEST_HEADERS = {
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 UBrowser/6.2.4094.1 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.8',
}

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'esgcc_sub.validator.BizValidator': 500
}
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 下载超时
DOWNLOAD_TIMEOUT = 60

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'esgcc_sub.pipelines.EsgccMongoPipeline': 420,
    'zc_core.pipelines.catalog.CatalogCompletePipeline': 400,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 2
# 输出
IMAGES_STORE = '/work/esgcc_sub/'

# 登录账号集合
ACCOUNTS = [
    {'13934200242': 'guoym0041'},
]

