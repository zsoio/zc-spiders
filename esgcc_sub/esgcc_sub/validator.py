# encoding=utf-8
"""
响应对象业务校验
"""
from pyquery import PyQuery
from scrapy.exceptions import IgnoreRequest
from zc_core.middlewares.validate import BaseValidateMiddleware
from zc_core.util.http_util import retry_request


class BizValidator(BaseValidateMiddleware):

    def validate_item(self, request, response, spider):
        meta = request.meta
        sku_id = meta.get('skuId')
        retry_times = meta.get('validate_retry_times')

        jpy = PyQuery(response.text)
        status = jpy('input#prod_status')
        if not status:
            # 重试：商品不可访问
            spider.logger.error('[Item]不可访问：sku=%s, retry=%s]' % (sku_id, retry_times))
            return retry_request(request)
        else:
            val = str(status.attr("value"))
            if not val:
                # 重试：商品状态未知
                spider.logger.error('[Item]状态未知：[%s]' % sku_id)
                return retry_request(request)
            # '03','04', '02'均为下架
            if val in ['03', '04', '02']:
                raise IgnoreRequest('[Item]商品下架：[%s]' % sku_id)
            elif val in ['05']:
                # 商品正常在售
                return response
            else:
                # 状态含义未知(暂时放行)
                spider.logger.error('=======>[Item]未知含义状态：[%s]->[%s]' % (sku_id, val))
                return response

    def more_validation(self, request, response, spider):
        meta = request.meta
        if meta and 'reqType' in meta:
            type = meta.get('reqType')
            if 'prod_detail' == type:
                return self.validate_prod_detail(request, response, spider)
        # 未匹配响应放行
        return response

    def validate_prod_detail(self, request, response, spider):
        if response.text:
            return response
        # 重试：响应无内容
        spider.logger.info('[ProdDetail]响应无内容：[%s]' % (request.meta.get('skuId')))
        return retry_request(request)
