import pandas as pd
from datetime import datetime

import pymongo
from zc_core.client.mongo_client import Mongo
from zc_core.util.batch_gen import time_to_batch_no


def export_item_data(collection, columns, header, file):
    list = Mongo().list(collection, sort=[("spuId", pymongo.DESCENDING), ("salePrice", pymongo.DESCENDING)])
    if list:
        pd.DataFrame(list).to_excel(file, columns=columns, header=header, encoding='utf-8', index=False)


if __name__ == '__main__':
    batch_no = time_to_batch_no(datetime.now())
    export_item_data(
        'data_{}'.format(batch_no),
        ['_id', 'skuName', 'salePrice', 'originPrice', 'brandName', 'unit', 'supplierName', 'catalog1Name', 'catalog2Name', 'catalog3Name', 'spuId'],
        ['商品编号', '商品名称', '价格', '原价', '品牌', '单位', '供应商', '一级分类', '二级分类', '三级分类', '同款标识'],
        'E:\\国网山西_{}.xlsx'.format(batch_no)
    )