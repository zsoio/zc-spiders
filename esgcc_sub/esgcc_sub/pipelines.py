# -*- coding: utf-8 -*-
import datetime
from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError
from traceback import format_exc
from zc_core.util.batch_gen import batch_to_year
from zc_core.model.items import *

from esgcc_sub.items import EsgccCompareGroup, SkuAttr, CertImageItem


class EsgccMongoPipeline(object):

    def __init__(self, mongo_uri, bot_name):
        self.mongo_uri = mongo_uri
        self.bot_name = bot_name
        self.client = None
        self.db_map = dict()

    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings
        return cls(
            mongo_uri=settings.get('MONGODB_URI'),
            bot_name=settings.get('BOT_NAME')
        )

    def open_spider(self, spider):
        _ = spider
        self.client = MongoClient(self.mongo_uri)
        # 默认初始化当前年的库
        year = str(datetime.datetime.now().year)
        self.db_map[year] = self.client['{}_{}'.format(self.bot_name, year)]

    def get_db(self, batch_no):
        year = batch_to_year(batch_no)
        db = self.db_map.get(year)
        if not db:
            db = self.client['{}_{}'.format(self.bot_name, year)]
            self.db_map[year] = db

        return db

    def close_spider(self, spider):
        _ = spider
        self.client.close()

    def process_item(self, item, spider):
        try:
            if not item:
                raise DropItem("drop empty item： [{}]".format(self))

            if isinstance(item, EsgccCompareGroup):
                if item.validate():
                    batch_no = item.get('batchNo')
                    item['_id'] = item.pop("skuId")
                    self.get_db(batch_no)['data_{}'.format(item.get('batchNo'))].update({'_id': item['_id']}, {'$set': item}, upsert=False)
                    item['skuId'] = item.pop("_id")
                    return item
            elif isinstance(item, SkuAttr):
                if item.validate():
                    batch_no = item.get('batchNo')
                    item['_id'] = item.pop("skuId")
                    self.get_db(batch_no)['sku_attr_{}'.format(item.get('batchNo'))].update({'_id': item['_id']}, {'$set': item}, upsert=True)
                    item['skuId'] = item.pop("_id")
                    return item
            elif isinstance(item, CertImageItem):
                image_urls = item.get('image_urls', [])
                image_paths = item.get('image_paths', [])
                if image_urls and image_paths and len(image_urls) == len(image_paths):
                    for idx, url in enumerate(image_urls):
                        batch_no = item.get('batchNo')
                        path = image_paths[idx]
                        row = dict()
                        row['_id'] = '{}_{}'.format(item.get("skuId"), idx)
                        row['url'] = url
                        row['path'] = path
                        row['skuId'] = item.get('skuId')
                        row['idx'] = idx
                        self.get_db(batch_no)['cert_item_{}'.format(spider.batch_no)].update({'_id': row['_id']}, {'$set': row}, upsert=True)
                return item
        except DuplicateKeyError:
            spider.logger.debug('duplicate key error collection')
        except Exception as e:
            _ = e
            spider.logger.error(format_exc())
        return item
