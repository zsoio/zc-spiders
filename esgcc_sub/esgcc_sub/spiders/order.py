# # -*- coding: utf-8 -*-
# import scrapy
# from scrapy import Request
# from scrapy.exceptions import IgnoreRequest
# from zc_core.model.items import Order
# from zc_core.util.http_util import retry_request
# from esgcc_sub.rules import *
# from esgcc_sub.util.login import SeleniumLogin
#
#
# class OrderSpider(BaseSpider):
#     name = 'order'
#     # 常用链接
#     order_list_url = 'http://b.esgcc.com.cn/showIndex/getLastestOrdersInner.htm?pgn={}'
#     order_detail_url = 'http://buser.esgcc.com.cn/order/detail?orderId={}'
#     allowed_domains = ['esgcc_sub.com.cn']
#
#     def __init__(self, batchNo=None, *args, **kwargs):
#         super(OrderSpider, self).__init__(*args, **kwargs)
#         if not batchNo:
#             self.batch_no = time_to_batch_no(datetime.now())
#         else:
#             self.batch_no = int(batchNo)
#
#     def start_requests(self):
#         cookies = SeleniumLogin().get_cookies()
#         # cookies = {'JSESSIONID': '49C5071AB13E837A0651C4D016B81AC0', '__d_s_': 'A60EA86D583AACEFD4110A6903BC7198', '__s_f_c_s_': 'A60EA86D583AACEFD4110A6903BC7198', '__t_c_k_': '99467d6185a94b0dbedaa638685e105f'}
#         if not cookies:
#             self.logger.error('init cookie failed...')
#             return
#         self.logger.info('init cookie: %s', cookies)
#
#         # 页码
#         page = 1
#         # 采集列表下一页
#         yield Request(
#             url=self.order_list_url.format(page),
#             cookies=cookies,
#             callback=self.parse_total,
#             errback=self.error_back,
#             meta={
#                 'reqType': 'order_page',
#                 'page': page,
#             },
#             priority=200,
#             dont_filter=True,
#         )
#
#     def parse_total(self, response):
#         total = parse_total_page(response)
#         if total:
#             self.logger.info('总页数: total=%s' % total)
#             for page in range(1, total):
#                 # 采集列表下一页
#                 yield Request(
#                     method='POST',
#                     url=self.order_list_url.format(page),
#                     callback=self.parse_order,
#                     errback=self.error_back,
#                     meta={
#                         'reqType': 'order_page',
#                         'page': page,
#                     },
#                     priority=20,
#                     dont_filter=True
#                 )
#
#     def parse_order(self, response):
#         # 订单列表
#         order_pages = parse_order(response)
#         cur_page = response.meta.get('page', 1)
#         if order_pages:
#             for op in order_pages:
#                 order_user = op.get('orderUser')
#                 supplier_name = op.get('supplierName')
#                 order_dept = op.get('orderDept')
#                 order_time = op.get('orderTime')
#                 page = Order()
#                 page.update(op)
#                 yield page
#
#                 # 采集order明细页
#                 yield Request(
#                     url=self.order_detail_url.format(op.get('id')),
#                     callback=self.parse_order_item,
#                     errback=self.error_back,
#                     headers={
#                         'Referer': 'http://buser.esgcc.com.cn/order/list'
#                     },
#                     meta={
#                         'reqType': 'order',
#                         'orderUser': order_user,
#                         'orderId': op.get('id'),
#                         'supplierName': supplier_name,
#                         'orderDept': order_dept,
#                         'orderTime': order_time,
#                     },
#                     priority=200,
#                     dont_filter=True
#                 )
#             self.logger.info('订单列表[%s]: count[%s]' % (cur_page, len(order_pages)))
#
#     def parse_order_item(self, response):
#         meta = response.meta
#         order_id = meta.get('orderId')
#         # 处理订单列表
#         orders = parse_order_item(response)
#         for order in orders:
#             yield order
#         self.logger.info('订单[%s]: item %s' % (order_id, len(orders)))
#
#     # 错误处理
#     def error_back(self, e):
#         if e.type and e.type == IgnoreRequest:
#             self.logger.info(e.value)
#         else:
#             if e.request:
#                 self.logger.error('请求异常: [%s][%s] -> [%s]' % (str(type(e)), e.request.url, e.request.meta))
#                 yield retry_request(e.request)
#             else:
#                 self.logger.error('未知异常: %s' % e)
