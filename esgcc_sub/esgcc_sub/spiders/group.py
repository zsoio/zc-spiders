# # -*- coding: utf-8 -*-
# import random
# import scrapy
# from scrapy import Request
# from scrapy.exceptions import IgnoreRequest
# from zc_core.dao.item_data_dao import ItemDataDao
# from zc_core.util.http_util import retry_request
# from esgcc_sub.rules import *
#
#
# class GroupSpider(BaseSpider):
#     name = 'group'
#     # 常用链接
#     # supplierId, page, startPrice, endPrice
#     item_url = 'http://b.esgcc.com.cn/showDetail/{}'
#     prod_detail_url = 'http://b.esgcc.com.cn/products/loadProductDetailInfomationAll?productId={}'
#     prod_group_url = 'http://b.esgcc.com.cn/showDetail/getOfficeProductCompare.do?prodId={}'
#
#     def __init__(self, batchNo=None, *args, **kwargs):
#         super(GroupSpider, self).__init__(*args, **kwargs)
#         if not batchNo:
#             self.batch_no = time_to_batch_no(datetime.now())
#         else:
#             self.batch_no = int(batchNo)
#
#     def start_requests(self):
#         # cookies = SeleniumLogin().get_cookies()
#         cookies = {'JSESSIONID': 'D398118B149C098A06D4529A31DE16D5', '__d_s_': '526C6BCBED5CE6764E831DF283760619', '__s_f_c_s_': '526C6BCBED5CE6764E831DF283760619', '__t_c_k_': '8c5a7ccf86624b3bb9aa0784c9e32260'}
#         if not cookies:
#             self.logger.error('init cookie failed...')
#             return
#         self.logger.info('init cookie: %s', cookies)
#
#         pool_list = ItemDataDao().get_batch_data_list(self.batch_no, query={'spuId': {'$exists': False}})
#         self.logger.info('全量：%s' % (len(pool_list)))
#         random.shuffle(pool_list)
#         for sku in pool_list:
#             sku_id = sku.get('_id')
#             # 同款分组
#             yield Request(
#                 url=self.prod_group_url.format(sku_id),
#                 callback=self.parse_group,
#                 errback=self.error_back,
#                 cookies=cookies,
#                 meta={
#                     'reqType': 'prod_group',
#                     'skuId': sku_id,
#                     'batchNo': self.batch_no,
#                 },
#                 priority=5,
#             )
#
#     # 处理同款商品
#     def parse_group(self, response):
#         meta = response.meta
#         sku_id = meta.get('skuId')
#         group, compare_list = parse_group(response)
#         if group and len(compare_list) > 1:
#             # 同款标识
#             group['batchNo'] = self.batch_no
#             yield group
#             # 同款数据
#             for compare in compare_list:
#                 compare['batchNo'] = self.batch_no
#                 yield compare
#             self.logger.info('同款: sku=%s, group=%s, cmp=%s' % (sku_id, len(group.get('skuIdList', [])), len(compare_list)))
#         else:
#             self.logger.info('无同款: sku=%s' % sku_id)
#
#     # 错误处理
#     def error_back(self, e):
#         if e.type and e.type == IgnoreRequest:
#             self.logger.info(e.value)
#         else:
#             if e.request:
#                 self.logger.error('请求异常: [%s][%s] -> [%s]' % (str(type(e)), e.request.url, e.request.meta))
#                 yield retry_request(e.request)
#             else:
#                 self.logger.error('未知异常: %s' % e)
