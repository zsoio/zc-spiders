# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.spiders.base import BaseSpider
from esgcc_sub.util.login import SeleniumLogin
from zc_core.model.items import Box
from zc_core.util import file_reader
from zc_core.util.http_util import retry_request
from esgcc_sub.rules import *


class SkuByGroupSpider(BaseSpider):
    name = 'sku_by_group'
    # 常用链接
    item_url = 'http://b.esgcc.com.cn/showDetail/{}'
    prod_group_url = 'http://b.esgcc.com.cn/showDetail/getSameTypeProd?prodId={}&siteId=21008&inProvince=1&inRowNum=100'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuByGroupSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        cookies = SeleniumLogin().get_cookies()
        # cookies = {'JSESSIONID': '8C653E3CD730846340FD675A6A30C88D', '__d_s_': '3CCCB516222550FEDAA106C36244C9BE', '__s_f_c_s_': '3CCCB516222550FEDAA106C36244C9BE', '__t_c_k_': '05775724b48e4729aa62cb0c195c0127'}
        if not cookies:
            self.logger.error('init cookie failed...')
            return
        self.logger.info('init cookie: %s', cookies)

        rows = file_reader.read_rows('doc/sku_list.txt')
        self.logger.info('任务：%s' % (len(rows)))
        for sku_id in rows:
            # 同款分组
            yield Request(
                url=self.prod_group_url.format(sku_id),
                callback=self.parse_group,
                errback=self.error_back,
                cookies=cookies,
                meta={
                    'reqType': 'prod_group',
                    'skuId': sku_id,
                    'batchNo': self.batch_no,
                },
                headers={
                    'Connection': 'keep-alive',
                    'Accept': 'text/javascript, application/javascript, application/ecmascript, application/x-ecmascript, */*; q=0.01',
                    'X-Requested-With': 'XMLHttpRequest',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400',
                    'Referer': 'http://b.esgcc.com.cn/sx',
                    'Accept-Encoding': 'gzip, deflate',
                    'Accept-Language': 'zh-CN,zh;q=0.9',
                },
                priority=5,
            )

    # 处理同款商品
    def parse_group(self, response):
        meta = response.meta
        sku_id = meta.get('skuId')
        sku_list = parse_sku_by_group(response)
        if sku_list:
            self.logger.info('同款: sku=%s, same=%s' % (sku_id, len(sku_list)))
            yield Box('sku', self.batch_no, sku_list)
        else:
            self.logger.info('无同款: sku=%s' % sku_id)
