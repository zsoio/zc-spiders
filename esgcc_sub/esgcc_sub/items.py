# -*- coding: utf-8 -*-
import scrapy
from scrapy.exceptions import DropItem
from zc_core.model.items import ItemData, BaseData, ImageItem


class EsgccItemData(ItemData):
    """
    国网商品模型
    """
    # 上架时间
    putOnTime = scrapy.Field()
    # 价格指数
    priceIndex = scrapy.Field()
    # 外部均价
    outAvgPrice = scrapy.Field()
    # 库存数量
    stock = scrapy.Field()


class EsgccCompareGroup(BaseData):
    """
    国网同款商品销量补充模型
    """
    _id = scrapy.Field()
    # 平台商品ID（用于业务系统）
    skuId = scrapy.Field()
    # 价格偏差百分比
    deviation = scrapy.Field()
    # 价格排挡
    gear = scrapy.Field()

    # 全部销量
    totalSaleNum = scrapy.Field()

    def validate(self):
        if not self.get('_id') and not self.get('skuId'):
            raise DropItem("EsgccGroupSaleNum Error [skuId]")
        if not self.get('batchNo'):
            raise DropItem("EsgccGroupSaleNum Error [batchNo]")
        return True


# 规格参数模型
class SkuAttr(BaseData):
    _id = scrapy.Field()
    # 商品编号
    skuId = scrapy.Field()
    # 属性集合
    attrList = scrapy.Field()

    def validate(self):
        if not self.get('_id') and not self.get('skuId'):
            raise DropItem("SkuAttr Error [skuId]")
        return True


class CertImageItem(ImageItem):
    batchNo = scrapy.Field()
    cookies = scrapy.Field()
