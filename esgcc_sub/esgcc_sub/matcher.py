# -*- coding: utf-8 -*-
import re
import base64
from zc_core.util.common import parse_number


# 从链接中解析品类编码
def match_cat_id(link):
    # http://b.esgcc.com.cn/sortSearch/showSearchPage?q=NVlxZTVZV3M1N0c3NTVTbzVaT0I=&chanel=15&mSite=MjEwMDg=
    if link:
        arr = re.findall(r'q=(\w+=*)&?', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 获取价格偏差比例
def get_deviation(obj):
    deviation = obj.get('deviation', 0)
    if deviation and '%' in deviation:
        deviation = parse_number(deviation.strip('%'))
    else:
        deviation = 0

    return deviation


# 从链接中解析商品编码
def match_sku_id(link):
    # http://b.esgcc.com.cn/showDetail/init/MDAwMTIyOTM5NA==
    # http://b.esgcc.com.cn/showDetail/init/MDAwMTI3NDgxOA==?skuId=00000000000000566614
    if link:
        arr = re.findall(r'/init/(.+)\?*', link.strip())
        if len(arr):
            return str(base64.b64decode(arr[0].strip()), 'utf-8')
        else:
            arr = re.findall(r'/showDetail/(\d+)', link.strip())
            if len(arr):
                return str(arr[0], 'utf-8').strip()

    return ''


# 清理jsonp的callback
def clean_jsonp(s):
    # ({"maxPage":52, "message":"",})
    if s:
        s = s.strip('(').strip(')')
    return s


def match_total_goods(txt):
    if txt:
        arr = re.findall(r'共(\d+)件商品', txt)
        if len(arr):
            return int(arr[0])
    return ''


# # 测试
if __name__ == '__main__':
    print(match_sku_id('http://b.esgcc.com.cn/showDetail/init/MDAwMTM2NTk0MQ==?skuId=b52d3d2faf73a34c'))
