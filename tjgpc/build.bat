@echo off

for /f "delims=" %%i in ("%cd%") do set folder=%%~ni
set name=%folder%

title build %name%

echo ---- deleting ----
echo delete ./build
rd /s /q build

echo delete ./project.egg-info
rd /s /q project.egg-info

echo delete %name%.egg
del /f %name%.egg

echo delete setup.py
del /f setup.py
@echo.


echo ---- building ----
echo build egg for [%name%]...
scrapyd-deploy --build-egg %name%.egg
@echo.


pause