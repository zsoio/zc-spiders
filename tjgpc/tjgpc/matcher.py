import re


# 从链接中解析品类编码
def match_cat_id(link):
    # http://wssc.tjgpc.gov.cn:80/godCategory/web_category.do?fkGodCategoryId=319b0d36-9fd2-11e6-9b36-6c92bf097c06&aa=22
    if link:
        arr = re.findall(r'fkGodCategoryId=(.+)&*', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析品类编码
def match_sku_id(link):
    # http://wssc.tjgpc.gov.cn:80/godTotalGoods/web_product_details.do?pkGoodsTotalGoodsId=000B1542-0277-4712-B9CF-38E6200305DE
    if link:
        arr = re.findall(r'pkGoodsTotalGoodsId=(.+)&*', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析供应商编码
def match_supplier_id(link):
    # /frontBrands/getBrandsAndProductInfos.action?orderBy=normal&shopInfoId=1
    if link:
        arr = re.findall(r'fkBasSuppliersId=(.+)&*', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从导航条中解析三级品类名称
def match_cat3name_from_nav(txt):
    # http://wssc.tjgpc.gov.cn:80/godCategory/web_category.do?fkGodCategoryId=319b0d36-9fd2-11e6-9b36-6c92bf097c06&aa=22
    if txt:
        arr = txt.strip().split('>')
        if len(arr):
            return arr[len(arr)-1].strip()
    return ''


# 从js脚本中解析订单页码
def match_order_page_num(js):
    # searchFunc(6,'2')
    if js:
        arr = re.findall(r'\((\d+),.+', js.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 测试
if __name__ == '__main__':
    pass