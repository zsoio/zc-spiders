# code:utf8
import json
import logging

import math
from datetime import datetime
from zc_core.model.items import Catalog, Supplier, Sku, ItemData, OrderItem, ItemGroup
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.common import parse_number, parse_time
from zc_core.util.encrypt_util import short_uuid, build_sha1_order_id, md5
from pyquery import PyQuery
from tjgpc.matcher import *
from zc_core.util.order_deadline_filter import OrderItemFilter
from zc_core.util.sku_id_parser import convert_id2code

logger = logging.getLogger('rule')
order_filter = OrderItemFilter()


# 品类解析
def parse_catalog(response):
    jpy = PyQuery(response.text)
    cat1_links = jpy('div.dd-inner a')
    cat23_divs = jpy('div.dorpdown-layer div.item-sub')

    cats = list()
    for idx1, cat1_a in enumerate(cat1_links.items()):
        cat1_id = match_cat_id(cat1_a.attr('href'))
        cat1_name = cat1_a.text().strip()
        cat1 = _build_catalog(cat1_id, cat1_name, '', 1)
        cats.append(cat1)

        cat23_div = cat23_divs.eq(idx1)
        cat2_links = cat23_div.children('div.item-channels div.channels a')
        cat3_dds = cat23_div.children('div.subitems dl.fore dd')
        for idx2, cat2_a in enumerate(cat2_links.items()):
            cat2_id = match_cat_id(cat2_a.attr('href'))
            cat2_name = cat2_a.text().strip('>').strip()
            cat2 = _build_catalog(cat2_id, cat2_name, cat1_id, 2)
            cats.append(cat2)

            cat3_dd = cat3_dds.eq(idx2)
            for cat3_a in cat3_dd('a').items():
                cat3_id = match_cat_id(cat3_a.attr('href'))
                cat3_name = cat3_a.text().strip()
                cat3 = _build_catalog(cat3_id, cat3_name, cat2_id, 3)
                cats.append(cat3)

    return cats


def _build_catalog(cat1_id, cat1_name, parent_id, level):
    cat = Catalog()
    cat['catalogId'] = cat1_id
    cat['catalogName'] = cat1_name
    cat['parentId'] = parent_id
    cat['level'] = level
    if level == 3:
        cat['leafFlag'] = 1
    else:
        cat['leafFlag'] = 0
    cat['linkable'] = 1

    return cat


# 解析supplier列表
def parse_supplier_page(response):
    jpy = PyQuery(response.text)
    rows = jpy('div.detail-list table.eshopTable tr')
    suppliers = list()
    for idx, row in enumerate(rows.items()):
        cols = row('td')
        if cols:
            name_link = cols.eq(0)('a')
            full_name_link = cols.eq(1)('a')
            supplier = Supplier()
            supplier['id'] = match_supplier_id(name_link.attr('href'))
            supplier['name'] = name_link.text().strip()
            supplier['fullName'] = full_name_link.text().strip()
            supplier['batchNo'] = response.meta.get('batchNo')
            suppliers.append(supplier)

    return suppliers


# 处理sku列表总页数
def parse_total_page(response):
    jpy = PyQuery(response.text)
    total = jpy('div.total strong')
    if total:
        return math.ceil(int(total.text().strip()) / 20)

    return 0


# 解析sku列表
def parse_sku(response):
    jpy = PyQuery(response.text)
    sku_links = jpy('div.goodsItem')
    sku_list = list()
    for sku_div in sku_links.items():
        sku = Sku()
        sku_a = sku_div('div.name a')
        url = sku_a.attr('href').strip()
        sku['skuId'] = match_sku_id(url)
        sku_list.append(sku)

    return sku_list


# 解析Price
def parse_price(response):
    info = dict()
    rs = json.loads(response.text)
    if rs and 'flage' in rs:
        info['status'] = rs.get('flage', False)
        info['salePrice'] = parse_number(rs.get('price', '0'))

    return info


# 解析ItemData
def parse_item_data(response):
    meta = response.meta
    jpy = PyQuery(response.text)
    result = ItemData()

    batch_no = meta.get('batchNo')
    sku_id = meta.get('skuId')
    sale_price = parse_number(jpy('span#agreementPrice').text().strip())
    origin_price = parse_number(jpy('a#price').text())
    # 价格容错
    if origin_price <= 0:
        origin_price = sale_price
    nav = jpy('div.detail-position')
    sp_id = jpy('input#fkSupplierId').attr('value')
    sp_name = jpy('div.infor-head span').text().strip()
    sp_sku_id = jpy('input#skuId').attr('value')
    sp_sku_link = jpy('a#price').attr('href')
    sku_img = jpy('img#mediumImg').attr('src')
    brand = jpy('div.detail-middle-message:contains("品牌：")')
    brand_model = jpy('div.detail-middle-message:contains("型号：")')
    cat3_id = jpy('input#fkGodCategoryId').attr('value')
    gov_catalog_id = jpy('div.detail-middle-message:contains("政府采购品目编码：")')
    gov_catalog_name = jpy('div.detail-middle-message:contains("政府采购品目名称：")')

    # -------------------------------------------------
    result['batchNo'] = batch_no
    result['skuId'] = sku_id
    result['skuName'] = jpy('div.detail-middle-name').text().strip()
    result['skuImg'] = sku_img.strip()
    result['catalog1Id'] = ''
    result['catalog1Name'] = ''
    result['catalog2Id'] = ''
    result['catalog2Name'] = ''
    result['catalog3Id'] = cat3_id
    result['catalog3Name'] = match_cat3name_from_nav(nav.text())
    result['govCatId'] = gov_catalog_id.text().replace('政府采购品目编码：', '').strip()
    result['govCatName'] = gov_catalog_name.text().replace('政府采购品目名称：', '').strip()
    result['salePrice'] = sale_price
    result['originPrice'] = origin_price
    brand_name = brand.text().replace('品牌：', '').strip()
    if brand_name:
        result['brandId'] = md5(brand_name)
        result['brandName'] = brand_name
    result['supplierId'] = sp_id
    result['supplierName'] = sp_name
    if sp_sku_id:
        result['supplierSkuId'] = sp_sku_id
        plat_code = None
        if sp_id == 'D4B5D871-A8D2-4D81-ADA2-758004322BE3' or '得力' in sp_name:
            plat_code = 'deli'
        result['supplierSkuCode'] = convert_id2code(plat_code, sp_sku_id)
    result['supplierSkuLink'] = sp_sku_link
    result['brandModel'] = brand_model.text().replace('型号：', '').strip()
    # -------------------------------------------------

    return result


# 解析group关系
def parse_group(response):
    meta = response.meta
    jpy = PyQuery(response.text)
    gr_trs = jpy('table.detail-middle-body tr')
    group = ItemGroup()
    if gr_trs and gr_trs.size() > 1:
        ids = list()
        group['spuId'] = short_uuid()
        group['skuId'] = meta.get('skuId')
        group['batchNo'] = meta.get('batchNo')
        group['skuIdList'] = ids
        for idx, tr in enumerate(gr_trs.items()):
            if idx > 0:
                sku_id = match_sku_id(tr('td:eq(1) a').attr('href'))
                ids.append(sku_id)

    return group


# 解析sku列表
def parse_order_item(response):
    jpy = PyQuery(response.text)
    meta = response.meta
    sku_id = meta.get('skuId')
    sp_id = jpy('input#fkSupplierId').attr('value')
    sp_name = jpy('div.infor-head span').text().strip()

    orders = list()
    need_next_page = True
    rs_list = jpy('div#product-detail-6 table:eq(0) tr')
    if len(rs_list) > 1:
        prev_order = None
        same_order_no = 1
        for idx, row in enumerate(rs_list.items()):
            if idx > 0:
                tds = row('td')
                amount = int(tds.eq(1).text().strip())
                unit_price = parse_number(tds.eq(2).text().strip())
                order_time_str = tds.eq(3).text().strip()
                order_time = parse_time(order_time_str, fmt='%Y-%m-%d %H:%M')
                # 采集截止
                if order_filter.to_save(order_time):
                    order = OrderItem()
                    order['skuId'] = sku_id
                    order['count'] = amount
                    order['amount'] = unit_price * amount
                    order['orderDept'] = tds.eq(0).text().strip()
                    order['supplierId'] = sp_id
                    order['supplierName'] = sp_name
                    order['orderTime'] = order_time
                    order['batchNo'] = time_to_batch_no(order_time)
                    order['genTime'] = datetime.utcnow()
                    if prev_order and prev_order.equals(order):
                        same_order_no = same_order_no + 1
                    else:
                        same_order_no = 1
                    addition = {
                        'sameOrderNo': same_order_no,
                        'orderTimeStr': order_time_str,
                    }
                    sha1_id = build_sha1_order_id(order, addition)
                    order['id'] = sha1_id
                    order['orderId'] = sha1_id
                    orders.append(order)
                    prev_order = order
                else:
                    need_next_page = False

    next_page = jpy('input[value="下一页"]:eq(0)')
    if not need_next_page or (next_page and next_page.attr('disabled')):
        return (orders, -1)
    else:
        page_num = match_order_page_num(next_page.attr('onclick'))
        return (orders, page_num)
