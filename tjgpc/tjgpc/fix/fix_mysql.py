# -*- coding: utf-8 -*-
from zc_core.client.mongo_client import Mongo


def export_sql():
    mongo = Mongo()
    sp_list = mongo.list('supplier_pool')

    # sql_tpl = "update sp_sku_info set supplier_id='{}' where supplier_name='{}';"
    # sql_tpl = "update sp_sku_info set supplier_name='{}' where supplier_id='{}';"
    sql_tpl = "INSERT INTO `zc_tjgpc`.`sp_supplier` (`id`, `name`, `code`, `alias`, `icon`, `logo`, `status`, `order_num`, `focus`) VALUES ('{}', '{}', NULL, NULL, NULL, NULL, '1', '50', '0') ON DUPLICATE KEY UPDATE `name`='{}';"
    for sp in sp_list:
        id = sp.get('_id')
        name = sp.get('name')
        full_name = sp.get('fullName')

        sql = sql_tpl.format(id, name, name)
        print(sql)


if __name__ == '__main__':
    export_sql()
