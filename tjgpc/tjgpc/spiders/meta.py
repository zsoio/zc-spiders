# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from datetime import datetime
from zc_core.model.items import Box
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from tjgpc.rules import parse_catalog, parse_supplier_page
from zc_core.spiders.base import BaseSpider


class MetaSpider(BaseSpider):
    name = 'meta'
    # 常用链接
    supplier_url = 'http://111.164.113.185:8090/basOrder/queryDemandDisclosure.do?type=18'
    search_index_url = 'http://111.164.113.185:8090/godTotalGoods/web_searchGoods.do'
    sku_list_url = 'http://111.164.113.185:8090/godTotalGoods/web_searchGoods.do?page={}'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(MetaSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        yield Request(
            url=self.supplier_url,
            meta={
                'reqType': 'supplier',
                'batchNo': self.batch_no,
            },
            headers={
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400',
            },
            callback=self.parse_supplier_page,
            errback=self.error_back,
            priority=200,
        )
        yield Request(
            url=self.search_index_url,
            meta={
                'reqType': 'catalog',
                'batchNo': self.batch_no,
            },
            headers={
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400',
            },
            callback=self.parse_search,
            errback=self.error_back,
            priority=200,
        )

    def parse_supplier_page(self, response):
        # 供应商
        suppliers = parse_supplier_page(response)
        if suppliers:
            self.logger.info('供应商: count[%s]' % len(suppliers))
            yield Box('supplier', self.batch_no, suppliers)

    def parse_search(self, response):
        # 品类
        cats = parse_catalog(response)
        if cats:
            self.logger.info('品类: count[%s]' % len(cats))
            yield Box('catalog', self.batch_no, cats)
