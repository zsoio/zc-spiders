# -*- coding: utf-8 -*-
from pymongo import UpdateOne
from zc_core.client.mongo_client import Mongo


def update_status(year):
    mongo = Mongo(year=year)
    done_order = set()
    year_month_list = ['{}01', '{}02', '{}03', '{}04', '{}05', '{}06', '{}07', '{}08', '{}09', '{}10', '{}11', '{}12']
    for year_month in year_month_list:
        year_month = year_month.format(year)
        items = mongo.list('order_item_{}'.format(year_month), fields={'orderId': 1})
        for item in items:
            done_order.add(item.get('orderId'))

    print(len(done_order))

    bulk_list = list()
    orders = mongo.list('order_{}'.format(year), fields={'_id': 1})
    for order in orders:
        order_id = order.get('_id')
        if order_id in done_order:
            order['workStatus'] = 1
            bulk_list.append(UpdateOne({'_id': order_id}, {'$set': order}, upsert=False))

    print(len(bulk_list))

    if bulk_list:
        mongo.bulk_write('order_{}'.format(year), bulk_list)


if __name__ == '__main__':
    update_status(2019)