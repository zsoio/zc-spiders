# encoding=utf-8
"""
响应对象业务校验
"""
import json
from zc_core.middlewares.validate import BaseValidateMiddleware
from zc_core.util.http_util import retry_request


class BizValidator(BaseValidateMiddleware):

    def validate_supplier(self, request, response, spider):
        # 已触发反爬
        # if '<html>' in response.text or '<body>' in response.text:
        #     spider.logger.error('响应异常[supplier]...')
        #     return retry_request(request)

        return response

    def validate_order(self, request, response, spider):
        try:
            json.loads(response.text)
            return response
        except ValueError:
            spider.logger.error('响应异常[order]...')
            return retry_request(request)

    def validate_spu(self, request, response, spider):
        # 已触发反爬
        # if not response.text or '<html>' in response.text or '<body>' in response.text:
        #     spider.logger.error('响应异常[pool]...')
        #     return retry_request(request)

        return response

