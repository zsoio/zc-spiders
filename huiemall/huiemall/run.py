from scrapy import cmdline

# -a batchNo=20210429
# cmdline.execute('scrapy crawl catalog'.split()) #TODO 获取分页信息，搜索引擎中供应商信息
# cmdline.execute('scrapy crawl spu'.split())  #TODO：少量数据响应有问题 商品列表信息
# cmdline.execute('scrapy crawl part -a batchNo=20210725'.split())  # TODO: 获取商品详情
# cmdline.execute('scrapy crawl full -a batchNo=20210725'.split())     #TODO:店铺全部商品信息
cmdline.execute('scrapy crawl order'.split())  #TODO：成交公告的订单列表     no problem
# cmdline.execute('scrapy crawl order_item'.split()) #TODO:成交公告的订单详情   no problem
