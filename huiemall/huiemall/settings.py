# -*- coding: utf-8 -*-
BOT_NAME = 'huiemall'

SPIDER_MODULES = ['huiemall.spiders']
NEWSPIDER_MODULE = 'huiemall.spiders'
ROBOTSTXT_OBEY = False
COOKIES_ENABLED = False

CONCURRENT_REQUESTS = 8
# DOWNLOAD_DELAY = 0.1
CONCURRENT_REQUESTS_PER_DOMAIN = 8
CONCURRENT_REQUESTS_PER_IP = 8

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'huiemall.validator.BizValidator': 500
}
# 指定代理池类
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.mogu_pool.MoguProxyPool'
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.zhima_pool.ZhimaProxyPool'
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 1
# 代理积分阈值
PROXY_SCORE_LIMIT = 3
# 下载超时
# DOWNLOAD_TIMEOUT = 45

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}
ITEM_PIPELINES = {
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
    'huiemall.pipelines.HuiemallOrderPipeline': 510,
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'huiemall_2019'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 2
# [订单] 最大采集页数
# MAX_ORDER_PAGE = 1000
# 重试次数
RETRY_TIMES = 5

# 目标供应商编号
SUPPLIERS = {
    '20C36EC1-A385-4C98-8F44-87D8668D89DD': '得力集团有限公司',
    '70cb5e84-aa0f-4055-8515-298ba445fb91': '史泰博（上海）有限公司',
    '07b346ad-c861-4c5f-a3ce-a387330db6a1': '上海齐心办公用品有限公司',
    'a9b8cebc-0c84-42f6-bc17-55d4624436c7': '深圳齐心集团股份有限公司',
    '0b17a689-eb21-4dbd-a1c5-5d9661fb9f46': '上海晨光科力普办公用品有限公司',
    '777ca7d5-6b35-497d-83ba-80e07a7f426c': '领先未来科技集团有限公司',
    # '08a6b962-52f7-4e09-8d27-e480114c3f4a': '北京易德诚商贸有限公司',
    '4ca27b42-3a28-4757-a706-952ec334b114': '苏宁易购集团股份有限公司',
    '4df33dd2-6365-41fb-93f6-259c49be9535': '欧菲斯办公伙伴南京有限公司',
    # 'c42f1e30-afa1-4260-8370-a74f3d4a42a0': '安徽紫迈电子商务有限公司',
}
