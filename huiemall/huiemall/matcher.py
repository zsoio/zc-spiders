import re


# 从链接中解析品类编码
def match_cat_id(link):
    # goodssearch.html?cid=A090101
    if link:
        arr = re.findall(r'cid=(\w+)', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析商品编码
def match_spu_id(link):
    # /more_notices/product_show.html?back=%2Fmore_notices.html%3Fmall_show%3Dtrue%26no%3Df43035134c&item_id=455784&product_id=55709
    if link:
        # http://www.huiemall.com/HeFei/productdetail.html?productguid=8ca9d672-cfff-42c7-aa24-9e7369436e2e
        arr = re.findall(r'productguid=(.+)', link.strip())
        if len(arr):
            return arr[0].strip()
        else:
            # http://www.huiemall.com/HeFei/productdetail.html?productguid=8ca9d672-cfff-42c7-aa24-9e7369436e2e
            arr = re.findall(r'product_id=(.+)', link.strip())
            if len(arr):
                return arr[0].strip()

    return ''


# 从图片链接中解析供应商商品编码
def match_sp_sku_id_from_img(link):
    # http://pic.epoint.com.cn/ImageHouse/AttachStorage/2497b88b-5632-4c94-bbca-6f1ed6c9198f/701.jpg
    # http://pic.epoint.com.cn/ImageHouse/AttachStorage/20190417/db239bd1-1d1a-4a53-b49f-99a113d9b64b/1555472051880.jpg
    # http://pic.bqpoint.cn/ImageHouse/Leading/27506/27506_1.jpg
    if link:
        if 'AttachStorage' not in link:
            arr = re.findall(r'/ImageHouse/.*?/(.+)?/', link.strip())
            if len(arr):
                # http://pic.bqpoint.cn/ImageHouse/Leading/27506/27506_1.jpg
                return arr[0].strip()

    return ''


# # 测试
if __name__ == '__main__':
    print(match_sp_sku_id_from_img('http://pic.bqpoint.cn/ImageHouse/Leading/27506/27506_1.jpg'))
    print(match_sp_sku_id_from_img('http://pic.epoint.com.cn/ImageHouse/AttachStorage/20190603/9c1cc9ca-4c35-4175-922d-192015d1d462/1559574016409.png'))
    print(match_sp_sku_id_from_img('http://pic.epoint.com.cn/ImageHouse/AttachStorage/6e4cf1c8-77d4-4b0f-845f-8e59deff7a58/主图.png'))
    print(match_sp_sku_id_from_img('http://pic.bqpoint.com.cn/ImageHouse/Suning/10561161057/0000000000-000000010561161057_1.jpg_800w_800h_4e'))
    print(match_sp_sku_id_from_img('http://pic.bqpoint.com/ImageHouse/Suning/10588135692/ImEzTjsk4Zq_rSFnlJ16hQ.jpg_800w_800h_4e'))
    print(match_sp_sku_id_from_img('http://pic.epoint.com.cn/ImageHouse/Suning/10714282759/05-_2p_zniNRqesTt4ru9w.jpg_800w_800h_4e'))

    print(match_sp_sku_id_from_img('http://pic.epoint.com.cn/ImageHouse/Suning/10714282759/05-_2p_zniNRqesTt4ru9w.jpg_800w_800h_4e'))
    print(match_sp_sku_id_from_img('http://pic.epoint.com.cn/ImageHouse/Suning/10714282759/05-_2p_zniNRqesTt4ru9w.jpg_800w_800h_4e'))
    print(match_sp_sku_id_from_img('http://pic.epoint.com.cn/ImageHouse/Suning/10714282759/05-_2p_zniNRqesTt4ru9w.jpg_800w_800h_4e'))
    print(match_sp_sku_id_from_img('http://pic.epoint.com.cn/ImageHouse/Suning/10714282759/05-_2p_zniNRqesTt4ru9w.jpg_800w_800h_4e'))

    # print(re.findall(r'/ImageHouse/((?!AttachStorage).)*?/(.+)', 'http://pic.epoint.com.cn/ImageHouse/Suning/10714282759/05-_2p_zniNRqesTt4ru9w.jpg_800w_800h_4e'))
    # print(re.findall(r'/ImageHouse/((?!AttachStorage).)*?/(.+)', 'http://pic.epoint.com.cn/ImageHouse/AttachStorage/20190603/9c1cc9ca-4c35-4175-922d-192015d1d462/1559574016409.png'))
    pass
