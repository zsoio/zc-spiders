# -*- coding: utf-8 -*-
import logging
from zc_core.client.mongo_client import Mongo
from zc_core.model.items import OrderItem
logger = logging.getLogger('OrderHelper')


class OrderHelper(object):
    """
    已采集商品过滤器
    用于全量商品明细重复采集时，过滤已采过的商品，避免重复采集
    """

    def __init__(self):
        self.spu_mapper = dict()
        self.sp_mapper = dict()

        logger.info('拉取商品池开始')
        items = Mongo().list('spu_pool', fields={'_id': 1, 'skuName': 1})
        logger.info('拉取商品池完成：%s' % len(items))
        for it in items:
            self.spu_mapper[it.get('skuName').strip()] = it.get('_id')
        logger.info('商品数量：%s' % len(self.spu_mapper))

        suppliers = Mongo().list('supplier_pool')
        for sp in suppliers:
            self.sp_mapper[sp.get('name').strip()] = sp.get('_id')
        logger.info('供应商数量：%s' % len(self.sp_mapper))

    def find_spu_id(self, sku_name):
        if sku_name:
            sku_name = sku_name.strip()
            return self.spu_mapper.get(sku_name, '')

        return ''

    def find_sp_id(self, sp_name):
        if sp_name:
            sp_name = sp_name.strip()
            return self.sp_mapper.get(sp_name, '')

        return ''


# 测试
if __name__ == '__main__':
    helper = OrderHelper()
    print(helper.find_spu_id({
        'skuName': '农夫山泉量贩装饮用天然水380ml*12瓶透明装瓶装水',
    }))
