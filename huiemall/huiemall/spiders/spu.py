# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.model.items import Box
from zc_core.util.done_filter import DoneFilter
from zc_core.util.http_util import retry_request
from huiemall.rules import *
from zc_core.spiders.base import BaseSpider


class SpuSpider(BaseSpider):
    name = 'spu'
    custom_settings = {
        'CONCURRENT_REQUESTS': 4,
        # 'DOWNLOAD_DELAY': 0.1,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 4,
        'CONCURRENT_REQUESTS_PER_IP': 4,
    }
    # 常用链接
    search_url = 'http://www.huiemall.com/EpointMallHeFeiService/rest/product/search'

    def __init__(self, batchNo=None, filter=False, *args, **kwargs):
        super(SpuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        if filter:
            self.done_filter = DoneFilter('spu_pool')
        self.default_page_size = 12

    def start_requests(self):
        # 请求总页数
        yield Request(
            method='POST',
            url=self.search_url,
            headers={
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'Content-Type': 'application/json',
            },
            body=json.dumps({
                "productname": "",
                "brandname": "",
                "cid": "",
                "price": "",
                "avgprice": "",
                "sort": "salesvolume_desc",
                "jnhb": "",
                "page": "1",
                "size": "12",
                "isdingdian": "",
                "platformurl": "http://www.huiemall.com/HeFei/goodssearch.html?page=1&sort=salesvolume_desc",
                "referer": "http://www.huiemall.com/HeFei/goodssearch.html?page=1&sort=salesvolume_desc"
            }),
            meta={
                'reqType': 'spu',
                'batchNo': self.batch_no,
            },
            callback=self.parse_search_total_page,
            errback=self.error_back,
            priority=200,
        )

    # 列表总页数
    def parse_search_total_page(self, response):
        total = parse_search_total_page(response)
        self.logger.info('商品总页数: total=%s' % (total))
        if total:
            # 商品池
            for page in range(1, total + 1):
                yield Request(
                    method='POST',
                    url=self.search_url,
                    headers={
                        "Accept": "application/json, text/javascript, */*; q=0.01",
                        "Accept-Encoding": "gzip, deflate",
                        "Accept-Language": "zh-CN,zh;q=0.9",
                        "Cache-Control": "no-cache",
                        "Connection": "keep-alive",
                        "Content-Type": "application/json;charset=UTF-8",
                        "Host": "www.huiemall.com",
                        "Origin": "http://www.huiemall.com",
                        "Pragma": "no-cache",
                        "Referer": "http://www.huiemall.com/HeFei/goodssearch.html?page={}&sort=salesvolume_desc".format(
                            page),
                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
                        "X-Requested-With": "XMLHttpRequest",
                    },
                    body=json.dumps({
                        "productname": "",
                        "brandname": "",
                        "cid": "",
                        "price": "",
                        "avgprice": "",
                        "sort": "salesvolume_desc",
                        "jnhb": "",
                        "jinkou": "",
                        "page": str(page),
                        "size": self.default_page_size,
                        "isdingdian": "",
                        "platformurl": "http://www.huiemall.com/HeFei/goodssearch.html?page={}&sort=salesvolume_desc".format(
                            page),
                        "referer": "http://www.huiemall.com/HeFei/goodssearch.html?page={}&sort=salesvolume_desc".format(
                            page)
                    }),
                    meta={
                        'reqType': 'spu',
                        'page': page,
                        'batchNo': self.batch_no,
                    },
                    callback=self.parse_pool_spu,
                    errback=self.error_back,
                    priority=200,
                )

    # 解析池商品
    def parse_pool_spu(self, response):
        meta = response.meta
        page = meta.get('page')
        spu_list = parse_spu(response)
        if spu_list:
            yield Box('spu', self.batch_no, spu_list)
            self.logger.info('清单: page[%s], spu[%s]' % (page, len(spu_list)))
        else:
            self.logger.info('无商品: page[%s]' % page)
