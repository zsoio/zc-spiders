# S盒
import base64
import json

SboxTable = \
    [
        0xd6, 0x90, 0xe9, 0xfe, 0xcc, 0xe1, 0x3d, 0xb7, 0x16, 0xb6, 0x14, 0xc2, 0x28, 0xfb, 0x2c, 0x05,
        0x2b, 0x67, 0x9a, 0x76, 0x2a, 0xbe, 0x04, 0xc3, 0xaa, 0x44, 0x13, 0x26, 0x49, 0x86, 0x06, 0x99,
        0x9c, 0x42, 0x50, 0xf4, 0x91, 0xef, 0x98, 0x7a, 0x33, 0x54, 0x0b, 0x43, 0xed, 0xcf, 0xac, 0x62,
        0xe4, 0xb3, 0x1c, 0xa9, 0xc9, 0x08, 0xe8, 0x95, 0x80, 0xdf, 0x94, 0xfa, 0x75, 0x8f, 0x3f, 0xa6,
        0x47, 0x07, 0xa7, 0xfc, 0xf3, 0x73, 0x17, 0xba, 0x83, 0x59, 0x3c, 0x19, 0xe6, 0x85, 0x4f, 0xa8,
        0x68, 0x6b, 0x81, 0xb2, 0x71, 0x64, 0xda, 0x8b, 0xf8, 0xeb, 0x0f, 0x4b, 0x70, 0x56, 0x9d, 0x35,
        0x1e, 0x24, 0x0e, 0x5e, 0x63, 0x58, 0xd1, 0xa2, 0x25, 0x22, 0x7c, 0x3b, 0x01, 0x21, 0x78, 0x87,
        0xd4, 0x00, 0x46, 0x57, 0x9f, 0xd3, 0x27, 0x52, 0x4c, 0x36, 0x02, 0xe7, 0xa0, 0xc4, 0xc8, 0x9e,
        0xea, 0xbf, 0x8a, 0xd2, 0x40, 0xc7, 0x38, 0xb5, 0xa3, 0xf7, 0xf2, 0xce, 0xf9, 0x61, 0x15, 0xa1,
        0xe0, 0xae, 0x5d, 0xa4, 0x9b, 0x34, 0x1a, 0x55, 0xad, 0x93, 0x32, 0x30, 0xf5, 0x8c, 0xb1, 0xe3,
        0x1d, 0xf6, 0xe2, 0x2e, 0x82, 0x66, 0xca, 0x60, 0xc0, 0x29, 0x23, 0xab, 0x0d, 0x53, 0x4e, 0x6f,
        0xd5, 0xdb, 0x37, 0x45, 0xde, 0xfd, 0x8e, 0x2f, 0x03, 0xff, 0x6a, 0x72, 0x6d, 0x6c, 0x5b, 0x51,
        0x8d, 0x1b, 0xaf, 0x92, 0xbb, 0xdd, 0xbc, 0x7f, 0x11, 0xd9, 0x5c, 0x41, 0x1f, 0x10, 0x5a, 0xd8,
        0x0a, 0xc1, 0x31, 0x88, 0xa5, 0xcd, 0x7b, 0xbd, 0x2d, 0x74, 0xd0, 0x12, 0xb8, 0xe5, 0xb4, 0xb0,
        0x89, 0x69, 0x97, 0x4a, 0x0c, 0x96, 0x77, 0x7e, 0x65, 0xb9, 0xf1, 0x09, 0xc5, 0x6e, 0xc6, 0x84,
        0x18, 0xf0, 0x7d, 0xec, 0x3a, 0xdc, 0x4d, 0x20, 0x79, 0xee, 0x5f, 0x3e, 0xd7, 0xcb, 0x39, 0x48,
    ]

# 常数FK
FK = [0xa3b1bac6, 0x56aa3350, 0x677d9197, 0xb27022dc]
ENCRYPT = 0
DECRYPT = 1

# 固定参数CK
CK = \
    [
        0x00070e15, 0x1c232a31, 0x383f464d, 0x545b6269,
        0x70777e85, 0x8c939aa1, 0xa8afb6bd, 0xc4cbd2d9,
        0xe0e7eef5, 0xfc030a11, 0x181f262d, 0x343b4249,
        0x50575e65, 0x6c737a81, 0x888f969d, 0xa4abb2b9,
        0xc0c7ced5, 0xdce3eaf1, 0xf8ff060d, 0x141b2229,
        0x30373e45, 0x4c535a61, 0x686f767d, 0x848b9299,
        0xa0a7aeb5, 0xbcc3cad1, 0xd8dfe6ed, 0xf4fb0209,
        0x10171e25, 0x2c333a41, 0x484f565d, 0x646b7279
    ]


def padding(data):  # 填充
    print("plaintext:\t", bytes(data))
    file_data_list = list(data)

    lenth = len(file_data_list)
    # print ("data lenth:", lenth)
    remainder = lenth % 16
    if remainder != 0:
        i = 16 - remainder  # i为需要填充的位数
        # print ("padding numbers = ", i)
        for j in range(i):
            file_data_list.append(i)  # 填充 char 0-(i-1)
    if remainder == 0:
        for k in range(16):
            file_data_list.append(0x08)  # 刚好的话 填充0x08
    print("after PKCS5 padding:", file_data_list)
    return file_data_list


def list_4_8_to_int32(key_data):  # 列表4个8位，组成32位
    return int((key_data[0] << 24) | (key_data[1] << 16) | (key_data[2] << 8) | (key_data[3]))


def n32_to_list4_8(n):  # 把n分别取32位的每8位放入列表
    return [int((n >> 24) & 0xff), int((n >> 16) & 0xff), int((n >> 8) & 0xff), int((n) & 0xff)]


# 循环左移
def shift_left_n(x, n):
    return int(int(x << n) & 0xffffffff)


def shift_logical_left(x, n):
    return shift_left_n(x, n) | int((x >> (32 - n)) & 0xffffffff)  # 两步合在一起实现了循环左移n位


def XOR(a, b):
    return list(map(lambda x, y: x ^ y, a, b))


# s盒查找
def sbox(idx):
    return SboxTable[idx]


def extended_key_LB(ka):  # 拓展密钥算法LB
    a = n32_to_list4_8(ka)  # a是ka的每8位组成的列表
    b = [sbox(i) for i in a]  # 在s盒中每8位查找后，放入列表b，再组合成int bb
    bb = list_4_8_to_int32(b)
    rk = bb ^ (shift_logical_left(bb, 13)) ^ (shift_logical_left(bb, 23))
    return rk


def linear_transform_L(ka):  # 线性变换L
    a = n32_to_list4_8(ka)
    b = [sbox(i) for i in a]
    bb = list_4_8_to_int32(b)  # bb是经过s盒变换的32位数
    return bb ^ (shift_logical_left(bb, 2)) ^ (shift_logical_left(bb, 10)) ^ (shift_logical_left(bb, 18)) ^ (
        shift_logical_left(bb, 24))  # 书上公式


def sm4_round_function(x0, x1, x2, x3, rk):  # 轮函数
    return (x0 ^ linear_transform_L(x1 ^ x2 ^ x3 ^ rk))


class Sm4(object):
    def __init__(self):
        self.sk = [0] * 32
        self.mode = ENCRYPT

    def sm4_set_key(self, key_data, mode):  # 先算出拓展密钥
        self.extended_key_last(key_data, mode)

    def extended_key_last(self, key, mode):  # 密钥扩展算法
        MK = [0, 0, 0, 0]
        k = [0] * 36
        MK[0] = list_4_8_to_int32(key[0:4])
        MK[1] = list_4_8_to_int32(key[4:8])
        MK[2] = list_4_8_to_int32(key[8:12])
        MK[3] = list_4_8_to_int32(key[12:16])
        k[0:4] = XOR(MK, FK)
        for i in range(32):
            k[i + 4] = k[i] ^ (extended_key_LB(k[i + 1] ^ k[i + 2] ^ k[i + 3] ^ CK[i]))
        self.sk = k[4:]  # 生成的32轮子密钥放到sk中

        self.mode = mode
        if mode == DECRYPT:  # 解密时rki逆序
            self.sk.reverse()

    def sm4_one_round(self, sk, in_put):  # 一轮算法 ，4个32位的字=128bit=16个字节（8*16）
        item = [list_4_8_to_int32(in_put[0:4]), list_4_8_to_int32(in_put[4:8]), list_4_8_to_int32(in_put[8:12]),
                list_4_8_to_int32(in_put[12:16])]  # 4字节一个字，把每4个字节变成32位的int
        x = item

        for i in range(32):
            temp = x[3]
            x[3] = sm4_round_function(x[0], x[1], x[2], x[3], sk[i])  # x[3]成为x[4]
            x[0] = x[1]
            x[1] = x[2]
            x[2] = temp

            # print("%dround----->" % (i + 1), "key:%-12d\n" % sk[i], "result：", x)
        res = x
        # res = reduce (lambda x, y: [x[1], x[2], x[3], sm4_round_function (x[0], x[1], x[2], x[3], y)],sk, item) #32轮循环加密
        res.reverse()
        rev = map(n32_to_list4_8, res)
        out_put = []
        [out_put.extend(_) for _ in rev]
        return out_put

    def encrypt(self, input_data):
        # 块加密
        output_data = []
        tmp = [input_data[i:i + 16] for i in range(0, len(input_data), 16)]  # 输入数据分块
        [output_data.extend(each) for each in map(lambda x: self.sm4_one_round(self.sk, x), tmp)]
        return output_data


def encrypt(mode, key, data):
    sm4_d = Sm4()
    sm4_d.sm4_set_key(key, mode)
    en_data = sm4_d.encrypt(data)
    return en_data


def sm4_crypt_cbc(mode, key, iv, data):
    sm4_d = Sm4()
    sm4_d.sm4_set_key(key, mode)
    en_data = sm4_d.sm4_crypt_cbc(iv, data)
    return en_data


if __name__ == "__main__":
    msg = "DKwjevOcP5RrS/5N/iSQ5Z8IVNhgConU6noxZpX8++KVCF3+wU1nW+tTP6DfNxWtzFb8RZu/AGzq32GYQAGNOGJVDJgIKE+4dH0bK+tU1NO15+Vn3eiUNLKrQ9KeqhRJfOAGUGER6Xb0Pjajd6M/8hLDkHh9VMNzo246Vvy3t+CLB8J5fIRtTBysj2ZJlv+wPW29QttCq6WIxm4DUjIuoopR5AP8M1AKx64W1j/Oub79lbw5GJK3bvtSyi+K18FL1PWvZs3h62IKHY0nkWIatohp+9nMyE/00IhoH7vK0OS/Jw9SOj6Ze//zWVfzyTc9yTAHkJW5XKLw9ENezWDFhhgbeGJrKDmRvXj78eWT18VHrv/xQmrZa6o+1vuqUaKI5IDR2wPIcE2kUgd9konK63eBg4gsMLGj2gu9nFrTUIJ36VBtfLoOJY8Pe9MeT7uUdc+rwWXncexGs3jOa7kT+O+Ts183QeGQ0V5Xtmq+TkfZX56is/306ZtnvbhaoddM9QnfhrTcnF0iiliVdC1YcNsHlcVWsUDLbfQqWy2X1LzAfVCsdV2y5+OPfOrwFKzMOcYm94WrrCbMFJCesv2xJA4wjUpA7sO9Ux2fq1sySwhIvL5hitVL8G5gAf6hWQG/3RJ+wg8L3M6qn+3M7GkBHWOUxKPUxf5D6aXUUWvzsIeDNR3plosYyNXdc/+BewmMZl21mRZwFLE8SjDT0UPkd36os70C5i/kMGnTrb3dzjI0YJELCYdGHLWjlA7U38fLWFxx3xcvDGseDl4GLglTNw9GsDJmN6OiuTvJHAV8spauowZ6WEDuQ1e+4F2pP7YatsXx+v3habImhT4IwL0iZ0xri5SMHoCoqQbh/3iRXEjr15GiOxuMZnTzWgxn8U03epLGi8X438OeI4WuYaG5dF29mfPjUUG+1QD39bVhm9eUc6sZBqx36DFQBoAb7/r6qfGDp2mot6xiY4RI1ArotxydO4B3tptUCn7kQoQcaGZxAQhAor4g4m5sWjasF5fX7uoMix4980weWTnkDRLHQCNc14er5gZqG6LUtYxdfoDX3rOyrhYBM1FPAUvrFfJNuYLbtvrFkUR2aiDi7bUalj5eFbU7p6xnLCUBi/VtszeHmocP68CPahyuqkVkfyn2W41PoQ+/TOiOyvpmE8YLN1wYBqdoIAJAUKUwHkZ0EAN40NnVOwi9mMl46HRBONGrenD26G3ANlrmzTQx9RxnJwkTLcRcbS2VKLVYcJWi6q8n1IkM5RyDlATz592RbQdcwLXCmsNBGNHG9JBmTYMDDCC3n/NnOHXpKdIdYgs5ZxSAnp83Nv/IPLcvWWkDLp/Di0tIqX24HKqGX1mk9FGZOfzSGrxiQlf8A1JgVjOpR251gH5QxLXAkxCzI3rVJ/nL2UO5A6eiKJEiB6xvvZOQfJ/3p2SCjfAfHawCQnLOyQzWX6xdu+5b7aEOL0pbWU1YW5ZsvrxiIa2ygSc+bW8RaTi4brZIu05yAFg7yDkx9tTKwc59FYMpfSQSFk8fCz9MRKUibRLPHWtpFpnxIuFZwDixoSReCG54jjPU/AL0AnJYvoxaP8sLo0LkqfK5pfWjtEA7COx9tjL5/Q2x/yxIpDMXLkeeWNjghDpLKD/238QcqqQNeDSBzF/6YuN/2unkqM8I80Q4TfVfXZbNO3UZYCUzLpBQbJNYsvTxVdVvHToSiFBThjHtI/G1O802qt3bhztV49S+GkmWD6VWoBN/f7v3sKmD7X6W1Y3P7JeUvb1+pORRskr6H7r7yVXeNIcjCQkalghO3F5ULXKUYgcuCx169DIKlwKOeSUXOpODt0U1uwqRdbUNShr8qiHl+kHwkpcgzoPlEZTYJej4ce75WO3pLHDCyV+pFXTuVkhGaEQglMQ4Os0DHk19WPO00blLWGb9LBg7ErG10OsTGbwCKXO+DprlOFlVvHEpzQ+Etu/zChjcY6zIZu+Y0ze4rfza4nuZ9RMC7ypFLhaPdY4XFHPJbph65+AA/vtSb2WVktSHfQ+TIesiE9u993asP3RwDEKaKGfU8ApXedi6VGCh45tEda1TFvFb1fj1WMnJQKZ4+Fm+eMwofrDi3gsh8hGIxzf1msOlHVoBvJyXZ1cD+QBZAz7L0snqu6xdKekmj2oxyQZQt36bKkBKMIOuGyGzVSb6ozLUdCa7MQ0e8kqMzvtUd40aDUlb5tOKrIn7JficUFnIUFOkk0jvoDvewRYqSv/pmAD283w4qgUPzEmE/lU2ozRLnlHE1ft1LFTHYd3+BVn0e5Oj+2FCt1HmnEcBh4yCv1NeA6zbs6MAbmT9OFFX67aQ/K1SklXc8gtjSuB22Ck05G8yGWMnkIRYhDWCyJ+QDmqzNUmUMo/54AiLUHgZJOyw7YQb5IZ+wlq65o7XOwy1pJi5zXX6lcAbul91WrpJkqJNNPSpUiIf3sUu6D2es7HxIMVhjW2Xakb+WNE0fXhBjr1Tx5c2PNmC8efxk3kEoRmpM/B5u3Fe9M3zhxZtrftht8hvYH7WEDTi9jdzh7LWLMYYan/bRrSlPvg0ZbqbCwPhwUYFI9yDojkPCZO3jJlawGzR+QeSKCY2TCOoS/1p8lZ9WGESfHc0/41e7xvSz+JiHE5hsyejHmaAZ/1aLXlFioXlNyVD2O8m4RAkj45i6BRlc3bHgfchZQJL4cI05+2UPKsqqwbB/fNvYdBIMyohBqpEv+WQHFH+isXfaih5sRjCZIch0FWK8wL+9cANodocMfSab0e0WTnoI14fKODLuRsl3CRWb8GFwq1HwMrF0hMSb9PP0JTx/HQDiWnPA8LQuASfj8IZgQCB2fDytbXzRt8pIa/ME/1VOn5Rn77ia/w9iKYE9LVt8Jr2fEIOQbEe3A92OywV9j48bdShKclOixgbypgENhfOlBv/JdM6ZzPLDPHELPGUuIASxUcL7f3Qyw86rWnx196JyKQFqPC3ipuCpt8rPDJlRViiLkrT1eWQAl02YZEkugfrIGvPcoGxxOQAD9zNqg7I5Ttk8KApRa+cimJJLNJStbMQ/kWBiZ+unNTzh58AEeVo9FGQa3KyK2p/Z4x//kx+vyE7Mfs88EtfskNsmnMgMjwWDdULHB01KmKB5/bg6mW7+e2FyEu7XX6QXpiGqWYpLDDV2viu4QiFZu6CwnbGGPowAdP0gcAvaLOH1WaLhWSPuBi7r8rQ3Vp8jbSwiwUT9MEYMxrQXQ/lpH3N5gvV1fPx/Fa6T8Tz4gwx1GWKgr0u5331394WH7+KgN4jxTCEO1SDgePNdiSPCfbNN9yMI6ru50Yey0UbmFGcYDzFYEhPGZ14ZfzQf71Qxe79fdWB52p0HVzItWZ4+Kul4jao/Ek6t4Cccdv8eiq2jkVNy59mU341lcwt7H0m2c17tb5hgO+Ts183QeGQ0V5Xtmq+TkfZX56is/306ZtnvbhaoddMJJMXpPAzcje3wqOTIYppyNaSfP0FKNG71MeD07UfIbwS+0DDd2jBSeR9VKiy+D+GPpn22r8ooq8n/azFJvWj6vMzrZatOpBoAk2IxRxCeza2PIBYvW95PvR/Pf82mUgAmNp3ndFElcGznY/S6Kf1BxISiuFG+glkC8vE01dXukxp1gnclEyewlVTMNauL4BsnGgHVJCzJw2N85PyGOT5b8weKDt92UQZFzz0qWSDUdJIe3KtlQ7t5IR2jtsfd1eqLMBQyqqFmzitGstnu53K9kHhmdvOfl5iqYAQn6JSZWZ8qyB5hcouPrGzvvCxcjtw728M6AzES9iUi6SN1pFH5b0jqS1TCca3v0Jiif/k/9I7o8w1oqjgRpVRphdpuLVN5zR2TDQWseWpukw9/3XbF6Os/mVZRiKdv/5yTaLA7JQiYVUSvtDsU/n6i5bf015jrts8H0hIAinfhUyo5/MbzYNo7VAeZ0E4RTaqSjFA5+VztckJNOLrbsQpdDfQpEnUOYWMZZJm04mqqiU/OpZOwi7AwSciEtBTUGWvD0FX4Nkt0veVjAogi6a1H4C87iHJor7MZxfeoaFo9TM5bVyn1/wA8NNQojAQPjbX7/qIUvrnNHZMNBax5am6TD3/ddsXYWPUE3jp0WanlyhU5X3erYlRjW9Vx6OMmC81xR1IQ55H1NMd8uRz3n8jyNoeAoqSzrU4eeSTU7HQBCHD3WHdA1+xj+VxC0MNuquAcKuflDN76EACiWbMwyTX8wlKXaMe/9kFJGktGClL9jjtpfkWYMoMOEmiQL1iOdmO/EAK7lqh9dsSTjDC7PyfS0KWTSa+AeVKi9R7frLS8qiOBDHPq4Ac5dk6cAxXje10/4WP9NAapHwhqiZMrEEl+ZT3mOFhy4ZL7TpiHLDTss/x/5DUqFzKXG9i/H/Ira8wtGjDcGm2NbsUq6rKn+hrU4rHhMhCm8raT8j1J0j31tY39CZU90f5yk/ggolpCxl8iX9wZHbXXdzJswZoZpVjqvkl4aNLspcA88aNh0OGZeomtkMtCUbmEZ9/7yhJ0iHP62sz9MKQAByQVvPiP2LQJyQLyQPEnvZZU9jsN9HiRKoozvkklez6rMiqn8z6ZmEZGi01Dt1oVCvJZBIOwkXIkzos6VmP85d+1qqhFjpVuQJ/su2YDtThSYbypK1qotecvnhw1dhXR/rH+cFvnslltaVI9YngzeenBxuiJa3TKcwzag8KSipdD7uJ4e5jMPvJ6jrPBtpz3kxk9Nw7RIhgALW4Zm8yoCoyiCfCrRuewRrgA+HTKz0st0UTYHj4HFcrtT7lI+S0OcAdirDSMb+unFoduzRgBcGK+m+iPO60hQSZkE1bcsHAkEZEf393dkv3LoOyDZd2kDj4qzVR6GN2SW/VCcSxpLCYvWjPsYMalkDtJUY9acqFNfqR2CkNF9Kk+xksdUteWFRIwXefw+k3MjJ321QyCLCJdrcWocGUNpJ4yDujsHjoSy8lX7ZXJdYHxkkVGSX0JZ//jB9HY+4AufF0AWyW4gMducNYVIBdNYgpTd447Wx+kuAoyap8fay85sknLCHNEa7Q11MYSW1FjbIhnc8B3+bac1jIoMqedRtvS2/00HXWVO07I2KvEwaz2mD+scsz1XABVB4SgmGNXRzT6uzi8gQBsqp+LwZf8/KUmHTwsHXhCZUFiSObvNhPsTtZDMMyosnb33hkw+afXyuK9RMn7WHZtVRnsWYVxPGsbiKHwYPDu+GsDwL3Ja69ubeelYKjt9RiA30VWzd+VCBCRnSEm0VxqsUHRkeMEffsNHvdjhXj3a67XRv6w2CcdHMZRovgl21mucbDjXNxDe97b3GICRFH6KhPRjp2kc5SSsVuL788Jb5ho1MpNdAvGyY59Xo2GJAgs1psjsmw+0yu4Q5X1SkqFW+TcqBZyMz3Gc7FSRLtCEOlvha6OdzWoxdpXN2TeYZ28xeBEcWFP1tFmPIdmvt1I+O1oY0LbW61JzgMWHNV+F6+i17fGxDiQhcJl15axJOTJmWByFFrDjGfvzCQTPbu+4Bip0WjLql9Fw3RE71a1l2Yu/TNqFoz/0KTy6pcnF45YbSysTHQCBla1DjHKh/wGwVlCSiXMsK8ew17XRblilfsvhdDTmN60jcvYZlOB6CjH9AiPoX+++7Jn/JLVMaGUrrfpL1PzbqbwNNy4faA4H3zQiWiFEIQeeDu5uAKnBEJqEF+OPlA5a2suqugnq3a1KQdsQZtiSePBKJ2JsPmIIe7JMOyveLcpR7fdOzZ1pAyxkVWJ4+mM+48ur+AsQLLo3vRo0h128ZGvi5YLzWsh9XQgXOs2tAilwW9Vy4RHLbhUfDg06RviLc0Dyt/RiNy4R9ip1PTpuc4wXLCMyvVkom3durPsGj3oqAnLJ/7b8O8H+OTUGGqRYBLX29eEfJZP9efrq0KWaiTae8p3QV+t8aooisryIU+phd8OAz8Kix9CDane7p3KJo1+Ze5LnpBOAdZikc8XdT1fPLNY9IyM9M5CxRHe+74ciy66eJs2bnWPqjM0mDXCNifPRXIyAnlTypbGigPEGY/nOqHakp2pGNYktSf9mD7UdX6ykxbSbM2RSPA/OnAj60LnS6yJX9JCVslGyJLNVzUVCXua09VkuOEYOpymdMoZ16y9CQP44USJ4aGTyJusx0soBxLBzvDavBTYuQ55tzThODXbKiC8O4uPSRvHPgTi5+wcrvKsykHvyJTueFsBhniRMIsLNNXtDQqeSMLk/L5DcShxkn2974DWhuRDrvxvp4W87VszVkhm6dIMrjg2AUvofYY9VQmyKSdkSag9I9CQxJZhCM/jpSJZzIQwuI+pbAXoUB50g8+udFNdcLXpbfxnmP2mLL6gtZjMRNwnf3rocFMB48BZeLZPeDWKklx3wh7k3k3LdS/99CvWMfJNe9m16dCYLwKYwX1Pcc02GHk5upPqwF725J6+tDLM2I311x18hUjKQluKhI8rw0s1myL5Md40LG/c6rkzQF2pVI5C0ZZQStgMSX4Y77pwv1p/EBBaSPdEn7CDwvczqqf7czsaQEdY5TEo9TF/kPppdRRa/Owh4M1HemWixjI1d1z/4F7CYxmXbWZFnAUsTxKMNPRQ+R3O8FOEthAcucJnSignsU8O47VZjxQSsUtY1i2CwdzhaPTKSqzV8p4qDGHd22hYvzJnuYM6j9aU0tyuPpDuDxf0K6jBnpYQO5DV77gXak/thq2xfH6/eFpsiaFPgjAvSJnTGuLlIwegKipBuH/eJFcSOvXkaI7G4xmdPNaDGfxTTcyGm7nIS60KAV5MgoCxvF028rEIPPFnaYicPsQRPIyTm8HHgFnTmcQTSwiLXX/dnsAS2iwS1Rp5+JgLxlZbPA+VQkVZH1mvwBx5E7rR+j+3xydO4B3tptUCn7kQoQcaGZxAQhAor4g4m5sWjasF5fX7uoMix4980weWTnkDRLHQJZK+t4/en1oqtq3nbz4sOdeGHDBbUHgjBnlYTwbw/Y1NE8abt6mo6IpY8s1xwtflD5eFbU7p6xnLCUBi/VtszeHmocP68CPahyuqkVkfyn2W41PoQ+/TOiOyvpmE8YLN1wYBqdoIAJAUKUwHkZ0EAOII0ZN6Pq10E8HLmjqUu2WG9gtDcv1PcdR1iSHwFgD1wkTLcRcbS2VKLVYcJWi6q8n1IkM5RyDlATz592RbQdcwLXCmsNBGNHG9JBmTYMDDCC3n/NnOHXpKdIdYgs5ZxRaHCPne9Ne0TWVP8cc0GUz8/kwoMhUjz65JhMrhwJ7StbFgNn0R4fbsW2PYL7BEaKwUrGhNDUHafCesjxwKT0Iz1G2tx9EZug7PQ17aGHZP7FDjYgBVK3kON22KPkJkPIGGRWl49hfwJ5h9ZYlE5gpMGWlYBfdC8cxAm21XjGjgcJmQs7SkEuHKKP/x2KDhvcZNez1YErWbg/lfNItiVzm97gmo0u7HRL6ZL3uKO1ENtYaiQ/XN7JgwRQ8kzexICZae61uU5Ftskr/N7Y/7h0rzV/Ruq3pZFd9mrYa3SdYz1FGG4Bypuk8JGmXAHC/my1RH8GN/43V6MB2nu7/F2LOjOEKBLL++PEsQUEzmf0+ZQ6RvMl/fvS2rT9mRUyZTFTbYHbJj6N1kwuDhlm7sLLP3EYEg1yOV/2Y10xGj9t9bnvNOv648p6skjkuhmPqhiDQLKRB5NG4hENxv2EHC1w/HJ9lkGASqrCLqvl40fv5w86SYihE0wcwka/zMONane9qDCjBkSilFPgE6SXvP8JNFymFXnvXqKO/QF4B7NSf9ku7+x2H3JBusc77BLAghMZX3BQukJn0FPc34XL8fryXW6DJ+vgero2FZT23enVsT/v7FVv1DyfBjLwG7XgcQ83cRPPvdcyp5wlezP7RzitNTX5b2JWga2fNeSO4mnR31Gx+kuAoyap8fay85sknLCHNEa7Q11MYSW1FjbIhnc8B3+bac1jIoMqedRtvS2/00HXWVO07I2KvEwaz2mD+scsmb/4UCcBQZ1astCK8E1AqXVKADoNh54LE0qcWpd2DsFq6wysdALx7f5RJe2mtbBhBvOs6MATYas7/Uoe9TAWv7WHZtVRnsWYVxPGsbiKHwYPDu+GsDwL3Ja69ubeelYKjt9RiA30VWzd+VCBCRnSEi29D5Klnmqb3nsouL3UvmP3FZzsPayCpGwWFiaPHLj78909AVtJLchj2xucEF1Ue7Ulbwwh1r2PIeMtirTJP86SoJNuG075LxvcjIlePh24ky7nxICRIuANmYGnJMbwJg2jtUB5nQThFNqpKMUDn5XO1yQk04utuxCl0N9CkSdQZECOniaUa9V+FUVEutzAH9S+v9X1Kx0r92eXvZ+JKPguiiquWf95x462SwzLoN/qivsxnF96hoWj1MzltXKfX/ADw01CiMBA+Ntfv+ohS+gNyUE/2240ij6k9sV96dGND1xRzfJWsBS3CzgUFWeGAOH3Y3uyfM0/22vjROU6rCSkXMabz8ydgq2MKnQ1jThwXCoPKWZMyQAsuLh4qtKVweGOcrkGeZXmQEPPvbrJz8aJG8VlPM/+hj09sRhILmphLrA1ix2eqppteEN2EPuRwGyiz6/TLpzxF4ktIKclpxvGw//invT83DoS7UZREqBMQD7mLOO3mEUVEI700lVDN+/BOTic4PQOkW512ymDfraO6KpLSqvAdLdotlC53NqMHh9eBl7XWBDuYve0bsrV/BybdYv6T4b+vQqj1Jucm/Zaftrb9BTKvLMTLrO1/kmydgyHPb6Bo9BlJbC8D4vRyYifmmhgtQ+Gxatt2PqYvdvJ/80GdQCBypjIdz6xko/Vg6shts3xHX1y+kShwD+bjcUY1e9ALyXYqMs1WKn7SYPHR4JVpJhHtyuYaXYpsSdJ/2RiB8JddBs0zveFWMQb3YKOp8UWBhSi0b4n/BF9o/sPzjSjiXZfp+jFggArf2xPbRxXa+JpDl5sG69zIdLtZiLwTfFA5XhXs2EVhOBvQG0sgXy5hznBVXRCypAEF2Qu7/klcuVXBKglcKRXjkuw/lz0BTqJcU8tFRABFzlA+anvorFO9B9MPN0XWDvSq2PRamXhyNgjxSzI+C1ZoTb6b9OcjVTtaJcE8A4ZZR1HXkdd5bb2TyZc5BheP/qARokeP61sxlE1gjzWZYjcHNq2GdQNGxwDD17Vi2LXDdw8bXY8bVA9PuumJkVz4Nd8OwcNoQjtJQLmT9MhqoonKYDUeQw+RH1kX0xNbj0gFLOslgP5b7on2YYaAz9fZy7sTOmBpV53XefvsRln6+To/HT11LQroFWpVUPjbTbKnaxsHNgY3poTRGI1lMgHGxvEyzHjLZd6cTxrnX9Dq/06iIjQEJikJJGRQLNLEV6gCa8t+oHcfl2OnxCZhkoKXXGh3yN74wnfMKiKystc6CGt5mCEiOK6YMrSfdW6s/6zMPNEMLrJA7aZAd5O+2+Dz1S4AOZhcfml+X31Dpw/wfhekGI11uvVe0AhKdRPFg6+Qq0MOJVYlH8D2DgRXRp+S2FWJEv+F09j8kIKqIZA9RXGxRSOP/5OixpG5ugVIZx1WR9wwgUHhmdvOfl5iqYAQn6JSZWZ8qyB5hcouPrGzvvCxcjtw728M6AzES9iUi6SN1pFH5b0jqS1TCca3v0Jiif/k/9JgLseH04TN88fdm6Eq3eL0wrdfArddO7l7izAScVEfHvEnXbGe69TtfxF7/ZlC2WQKuhme8dxenfgK5BXXAthYPg1PhR7LcbqejHxJvlBrpzNDBqDF3UqB2metq0mcYbXYWyfiRCXBtKhgehiExsznVO7pFsSe9fJgSV7j4xEpEk97wG6pXhRre/FQUlc5IXVy9IezMeLr0sLJEYw7PjCjtB4yxToHOHiBBq6y64/2H12cj8a1CZLY4PuggTEkyxb57lV5PtWPykcylo6bgcAtq7eQRFrzgTnO+8h3E623mX6uTnBxJ4ynT5noD6psEblg01OC/2Mxl2jq41EbqTFwgb6paMkouctMXp08YHQdzJYgsLibkepRHy9Se77Y26+ClQLok9sSx+C7+GWKVCUcEq4IyRLOKh9nZLjDly9kjNG8kIiU4jXgc2XxDstnFkHzJ2vTSEc/4G/mKu0njBBcP4xB51itfB1dosVLZb1pbPGw//invT83DoS7UZREqBND1NYP8ZRKH2xYIhlucNa2+/BOTic4PQOkW512ymDfrS0Blb55aqB6u1OOOQ2OWQKnO8t4qZP/XqiZDXXi6vxVftpY7L8KXr6PzJs0WwJzTJaftrb9BTKvLMTLrO1/kmydgyHPb6Bo9BlJbC8D4vRyYifmmhgtQ+Gxatt2PqYvdk7snmNqbEX06SZgjX+I/XXJAwpK1zMAFvdcqkSfMV1u5aDuO++gHdr54uwNUJr8PfHR4JVpJhHtyuYaXYpsSdJ/2RiB8JddBs0zveFWMQb3YKOp8UWBhSi0b4n/BF9o/sPzjSjiXZfp+jFggArf2xPbRxXa+JpDl5sG69zIdLtZiLwTfFA5XhXs2EVhOBvQG15pN+Tx1SHY8ekRldw7HEimIHNONnQIQAB3xdon+qLn8uqLVIhLxLCu0Jv8ke+HS3vorFO9B9MPN0XWDvSq2PSfLzXbCbFNddEnhuEBMp0+90f79gisLjpYtZCCnUHc7Hi98+g9VjxjidWo8F706ISmRIIV1xJ6a5foYHsAw4VXc1xbRsgE5ehZY3rioAvAgY6ch7KoHVfL3TQ9rxfPMUfvk7NfN0HhkNFeV7Zqvk5H2V+eorP99OmbZ724WqHXTKNJKWOooP5CCBScU7AtH/r26HjYgF8qtHskfbIEKd6owhpdGyFAqBxajgU/TPTndHnlDiFjGjBq1fWDA5UNCnY6mxMCVuqc6fQCZd114QlCWAdY23YpK/CU8aa3PCgCXrEWCziYIRbgMza1G3dT4NEmCZ+4PZES7MmZKMoG8SzfoP46vbczHBpMqnL4xa2Krrb5t8c/tV7IsCMnKcj/r+8c1TUl8OlN1lTxOuXNK+4g9zruNUicJoEZskOFP97xaRwqsqwfNvv7QJsUHjQLCfrtYdm1VGexZhXE8axuIofBg8O74awPAvclrr25t56VgqO31GIDfRVbN35UIEJGdIShpamWwvm519SmdedNkk+SH0Xur/+Pyce20dr6Eprq6XUtC0ikfua0vxuRxPiWjl838pfIRlWaa6KAdwmhL86At2eHsu/KdGwx6FwtBBLeN99viUtVOtz09IOtl1eFQBxXHwn133x+bdvKPcMAsTJoD4UEIHD+DlRoTrsE8qAMU1hsS+XCDXUdg6x+Vzj4gD8JNkzpXTOCEW08p10CTHoBZuFKwIzX+X86xS8VtEqWXQHJU8/BHJWZoy6wZ4W3awnF/R5qvwBfxBDlrm6lcYKLqdoJQR8BVFCUnRXobGVkngFgCu+T4D1IgtsqrwFrZBiD7U7pCir4ITcCsiRTm+7RnqSox6oFLJ2b4LFrjc7J8B6Iwcqb3EdsPcaXQZdfUrrf8P1m5yYA3IOHOBVB1Dnjh4HinH3qKpHsUIlNmAfF2BzttnFqFmcjU7P2R4Xn4sOavzF2qBQLh7B5z/48lJfPqWAv6AYnNUY7ZEUtIdReZTTby9GgKr1iQ3nJKqw1GV+LS0ipfbgcqoZfWaT0UZk5kI8vDFXgyobadTObsNt5ZHWAflDEtcCTELMjetUn+csu6hf9bT6e3idT4UhDa9w4b4K4uDSMRo2Vp8T5UBumjNZfrF277lvtoQ4vSltZTVhblmy+vGIhrbKBJz5tbxFpGXQN9W6N1zoYmrz6/JHFec1h3tmPbWsbcFKGKdwA6x1FH3w/M/iSlFGrHXu+E5ngOLGhJF4IbniOM9T8AvQCcli+jFo/ywujQuSp8rml9aO0QDsI7H22Mvn9DbH/LEikMxcuR55Y2OCEOksoP/bfxByqpA14NIHMX/pi43/a6eSq0pWDMZFmzp0FcQcC7BJTMEo+FODyZ9mluhPdlSMf1m7eNy4PRHrdWzM6CkhmLdeqW+CEFDhd3K5rk+esk7Y2u/ewqYPtfpbVjc/sl5S9vRp/ObpCHaFKl0DfQu7YG1MJCRqWCE7cXlQtcpRiBy4LNYhLjVOomIY0aAeAze3p9l3iEqqzGb2Uw5bFR8LB8S2Gc+YaPOdwNbldU5btt1rt8U+fpOTxwb/HTttTaupco2AXZgdzvFzfcTyAuc+YOhfji+14TxU/oFFaxnYMDjUHx0xTnA1oVo/5DWIe+kTpdC/Yz2vczGWwZ0STU9u+bVYEHVyM6Xi5ljp70nZvu7i8qaFwGo2SxaxhnROnBxSeREXWVLn/+N6VNhJ1tSjOYyXreE2Nn5yakFFd9xOcXfKa2G7iJ6J0JUlt/OJXLL/2idcMCt+U4J40Ii7cLSmb/4p2A/1ph6teMNNXuUAABW0vwgIsfL7EzNmkH3k48RcDgPeKVIY55mfziaG5Amoah5EFW6ekNcnDHLKi1vd6RHlz3F4mCVnnYAG20ia12LIIKaeZNh1SUIltBHatEeh32wF1WPH0c9WUEX3kO9tlexZGCXE0m1vwe+223GN7lJTSPgbhnx1GmuTIrdcQp6yBRcH2Q3hZOVF6UDsrxJ9EMrF8qwreNIb75njrc1n6GeP1tIMoSkOrbQP/t+iMhYRyHKvyiyTqKcTI8+saTvmtlRDollh0uxzmI/ImqJZoeD726y74eMZxSJtUs5Kx+pGb3YBoGzbPaNnGc/Rd3EoU+7+vQAH2l1wTZQEJV312sP20WJlP9OfvlIO7xI1W0HgKNEEZjQLA32Hqcx5yzoyRkiBYW0A9XaXjK9ZSC+TsWhcVXs/BIbVgzWDJYvsW8u1BPzfbq4L1o1j9EuohkQSykiERuASXxHjY7zOtDkXsLBabOiTtrrB3b0VcwD7MNrVbzRBQ39iCOhPK4m+iKd6eCr/NiVGNb1XHo4yYLzXFHUhDnl23F6aTuAfV201ypYp4DMXOtTh55JNTsdAEIcPdYd0DX7GP5XELQw26q4Bwq5+UM3voQAKJZszDJNfzCUpdox7/2QUkaS0YKUv2OO2l+RZguZhOkEvwKGF3eU520L78hBbUSMKuKvFk+k6y1sOpCYIFWOnxqrHAXoyAWH+pTcBr0FUHUSiZXCppXDCXqLM/k34c0xjjIDMlsIP/5StzjIt69xwyOiW8Cu+TgyZAKnUoE+CI/7YghcHPrcS0w8No1F22pFCaBfgjhRGtmKOAgxpAZ2igHDz3pf9sYRqILk0jgiq2olkgziz9Xwaqxx9LvUMrfbBMln58wimJUcnaLNC1+qCyI0VXQ2mWZd4+XdW4yvQUvGhKhTCBrBRswH5ILRLDkHh9VMNzo246Vvy3t+CLB8J5fIRtTBysj2ZJlv+wPW29QttCq6WIxm4DUjIuoopR5AP8M1AKx64W1j/Oub4apH7L4iOTEPHKv0oN01yYQg0VlYQKux0291u3vrni0Jdwxk2ojzN6jGSe5eMkUMsTl3ao+f7nr3/NkopweIlzsh2tFcPX9ADudTs7SDdgjmerKpa1aixIVE9bESVYMxK2p7CnDKOxHcnfMjKfR3ZpeF9AYXCKnNsMerCwM2cv0mvncBR0LIY/8PTHl/m29QAL+dv+qSYzfMW5fi7CHTGkAMis/B86/dKMPjFiMR0shFfcFC6QmfQU9zfhcvx+vJdboMn6+B6ujYVlPbd6dWxPdRFuHjp7Rt8BhbHJmHA7d6ZzWhzQ1EfpkuHERQIikMywwwc8n42NzksWty5deuPlzuhOyhM9jNkrKrAm9IrjjkBLwiZiYmTx8DMy+UpW9JYpSCTyRs6suu7Px1gmnwhIlgOD9E1WvBipA7vov+RNmlxwR+ADkw6ZUm/ahTwkb28HEWJQjsVKRhb0rANQVBlFCrz0loI7lkMU2UwFZb7bofKuh3SXUR7AYIPSGxujWYFlVsuCf+fVJewRtpp+05MisFgnCvkVWw4/e0GOEO2UDQ/q57dlz5d4eYxug7cD6jpp1pXrjB9lRV/nim3rQ0TT/7nKcPBR9Rq5qBpCyKU/2miKdF4NCT8S3FF4iEAWJ183EEbu2b1BW411ON28O5qalRXtd+e2/bJwzYdVATlao7Mx2kW8Xe8XyOjzzVElWFJD49gQAp0H+juEXsP+07h7Q0s4AcPFiIGAHWZ/Fr6CvsLrMNb2SmJQs1ESyrrVyTXa6fUWkPeWh673RkhxOt2Q+XnNAiZlthOqioNm+ak6/od+5vVT2okn/dVCLy6XWWmlhrI8I+CgB8Jsc0CkGpSuuZkZNrnWJXXRlsf8eJxPgR49I8xCpZ4Z5OvI0jDshXOrEtXyqFvf6x3hjSCnXPVFmO0eDvOgX31gJFKljUetWWQsoPfiU9Jv6i4DtRim3pvkfXwdMd7ONTA25xEuT179zvMw1nSbNS6T8HlPJLrHAlGnLhIXP4+2L6EBoEg5dCo4fdje7J8zT/ba+NE5TqsJ0pxxJK6SqcPV83m+DtxnZRcKg8pZkzJACy4uHiq0pXB4Y5yuQZ5leZAQ8+9usnPxokbxWU8z/6GPT2xGEguamEusDWLHZ6qmm14Q3YQ+5HAdG+Xtgy4v2wSYhXz4GZPu2ZqTFPQ5qKTZlsFrS+pU3UvquDWfRHP9Kso3zI94/vz33FV1JWc57ET34qm+FvT80jsygfN2zT9lB95HqXGW9eRWAn73IcVd3iRLz31YToood6h/b9K9UrUkNmGymCGUS7B/V/df3FUcDvU8OpaxPPz3mn0e0ZL7aJzZJmM6IBekPgc//UrRGIYn/8jLwELaQjDftoN96rCd7VEM2VTTPX+lDc4SF4XrOzaZ9Wx9ReqqxeWACudMdhqRrWTSBRYYB7eQ84TRG/s8n0BuusXsPzcK4Kz7rO3nOraixybOwpN5+H6BqNUbpBp4hgmn7CsSdMVrLkNeyRcLUX8D31YwSjEftDtuKiCoAWEB+EY33ecWAE9rtL7ahbirZNW7geMcmMBKWQtY/W8QJ1/hrSu1GRcGzhDz3Rii5U8Bn4AV67sxSXqBYV/UZO1mGTroeBv0eI0M1UTsO9n9FmDsKdEWt/y9jQo3nusgSMlEdiMhiFLeYfQkAfpJEOTJPWzO4dI1vl6Z41Ao3l9U9oR1PY8KmkDKuvM5Ona7w+yKtTXlHAsLqowbLWesZno3LByF5m3n75OzXzdB4ZDRXle2ar5OR9lfnqKz/fTpm2e9uFqh10y2Al+UW3PtL9WfGTHAO/q11i0YjPFtzqY1avrWNSsSqOStaNIYG36PF+Cf81nLcaNQBIIaA9saeKrdAFXpoLXkuwHLJuaufnuDO3kyvslEwF0lQxHt4Gq8JDUIKY0q2Tssh+k8Cb2sYz70YLaKwxiMfQkR3B9IJ2LcaJvOMsZyx/y1vGU5gsXg5ZCnPec9J3Kqeyj89mNIgNuD7bmcXNDmwIWIEYkasV8i3kIXfRB4MXDCh+BWyQTgtzbJN83+Fvs+Pb3WzEVXnd6tqrG0j0wdGMpxT/uxYCsTf/gh5lmWUQ6cF19JhFBr+8zm2XQGPaqbW1cwFTACf7dDptAbTpmNHL3QUK3KvwOs7VG94Cr9nA0HDN+tViIdTGRzfLbYA5TpRZjYeG/Ysz0vPpN1r/FPg2jtUB5nQThFNqpKMUDn5XO1yQk04utuxCl0N9CkSdQkAiRKTmYn9p77yCzmrNQ4wJpzTpCtRXwXOWMOIZQ2otVlP46yfPFDaJDDQawXDClFuC0e/sR1RwN9KEvd3koNkcYgCp06ObWTsB6whxn8w6r7nRXhYnh/63f0SRv8mRLlOvh2fjRUqT5hXVOENPINOH3Y3uyfM0/22vjROU6rCeyFZtPaZR5H/JGSI8xUdw4XCoPKWZMyQAsuLh4qtKVweGOcrkGeZXmQEPPvbrJz8aJG8VlPM/+hj09sRhILmphLrA1ix2eqppteEN2EPuRwNt9QGf+7Mt1kzU6eVtL1p1PT/HXFJImnwlxZJn9Yl7zjEgSJp2vQOtFDVcMYwm+drqOafw9AbDDrtJvXPKVoylGfvuJr/D2IpgT0tW3wmvbX+K1qXVx99KOmznpxr7TetuuUcHLIcd3ivP1EZ7Z1quO/Fk0/+SOV4QHAw0/TxArpdf/Pl6qj9Ko0AHTG/1rGYXn0A0qUx9uBUOSgQl4cqqezIUgp9worb95nVIvxUbeNJ3r1NeXu9pujZH3Rju74h6JfzopPF6wAOHc5VD/3rZ9HR4YFOX+bhyJpfkAAcwLMkDiGqWO7aF4H4Q7GJfIqH5yD5aEftTWJ2z2balL9Ou5MWBdEEO6Jy6PXBGz3Vr0AlbpJaAVOPMtujHVS9OxVKzZhL22R7HI0jiGnTRfW8UGk9ToECYF2Cxm0fi9Azy/zc1xi+yFPyMCtYgDG53g1BP8JzeITj4ZUQRKAKVm4+VMR3i2gkk5bL3qReqLTJQNwJcqUn9KyKwg4MohvCVVrcwSzTXbNBewqFgNe+kV/4YhrN+CLhXPVhT0hqmi+HrENGhFs7GkA4obDw9noPLMS2n+2fl+fUafHPH7c9p1N2dyLyO+MHg4XLcUcWGFDcxMCB4Ob2XNXvfG/3ovphn/NoZ6FZeD/SdPzDUXPQDrGsOC+eZwvTaFf7/AiUKtxCudllijIMj2DZYsRhfP/2bQ4UoP74c8gC8RlipZdeapjx9PWySkthZ97ACYy9gf6hurdEn7CDwvczqqf7czsaQEdY5TEo9TF/kPppdRRa/Owh57HOjUGP4Xk0KCaW08EFq55FiPObCVjKjLD5Bea1Q13CZp5OFX7pBSBsL+JN4KufxgY6rDPxTDfFSMyCLPtqEcOZZ4gKAwot2OGMg+dlAQkYUx3VIpCGApiazaU+JmsTxOrew/tF0cKhZbxFsS5BAk66/voLjvYH+NKNWma+Xq+AJieTXg7ErRrmIz0aX7visvpBCmUbT7ZnIs9F+fuMKjMjiRzaptgNhuGxck5CO4iguQpDDN3yc9dkR+c+xB+JywqWajd4R4/bvdYgicxktWDaO1QHmdBOEU2qkoxQOflc7XJCTTi627EKXQ30KRJ1EvbiwA/HNwL1UNKc3FDOIZbQWY/awUfvvGYTQfpYPo3TXNMNSt80G/1tRybqTYVTEW4LR7+xHVHA30oS93eSg2RxiAKnTo5tZOwHrCHGfzDg4jVkchI3Ilm/IiV4EWOKlRP3QYkhyBV0QbwoQyyaXq6TXgZdr42V9G4qzdSwJL2j5ymtf286pZl+q2Fx6PRlLkpOPOWz0UyUIFTIoyheNzgUHCmSb8vUp5ZoIveh+6zc0h6TQ1V0KHvmwLgYqlONki8hkt+o2py3+e7UMgBMTB3TPQZPaUHBdi6N9dBryICjugZ1anx8Rr8T2+7EiBoZ4nKfYPMA5zWliPCOwtkzDMoufzYEZ0r2peNOalEQYID99xVdSVnOexE9+Kpvhb0/B06y7Up12BUFycSDC1npimC/5ugJQKIO643gmTnbjSUKdSsnbat3cy+o+3ClIufN7FDjYgBVK3kON22KPkJkPIGGRWl49hfwJ5h9ZYlE5gp6cIvUDF2/EpM0k3P+yL5IK29xH/0hYWmyy3AaCkCjWo5glkcV9p5gKRbIh/s5j5HK3mlGMV2AkzLMS7/T2iGkJAAHJBW8+I/YtAnJAvJA8Se9llT2Ow30eJEqijO+SSV7PqsyKqfzPpmYRkaLTUO3WhUK8lkEg7CRciTOizpWY/zl37WqqEWOlW5An+y7ZgOSMH1UAqbiNhWU9OwLAuu9YRaHhmxDvDVSsQSyTkCiRy+uOMrxy6GK8+TKCBh5flQbJs3gsc67BzCSPyPiOgLlHPeTGT03DtEiGAAtbhmbzJOJ/Kcla8qRkXkHqtk7JIaPSy3RRNgePgcVyu1PuUj5F5Mu+wp20xqXtozTUapjO0fmyTJlYTbg2PefK3zdQA2QgUtKQ/wlPEcrTtuRBFSzGs9neNRvCKb/ecWQ6z8A1umd++iB9mGMPhsq4n3Zb0CWGb9LBg7ErG10OsTGbwCKXO+DprlOFlVvHEpzQ+Etu8hr6BWaoWHdSssvLXXuad0OaEuZkubUL9jtAvA9kENX2gbNs9o2cZz9F3cShT7v6+yRWzwIpoOSz65YwNlhmev3RJ+wg8L3M6qn+3M7GkBHWOUxKPUxf5D6aXUUWvzsIeexzo1Bj+F5NCgmltPBBaueRYjzmwlYyoyw+QXmtUNdxbsA+YR4fI9665jEeuDumedSbYXmmnjll5eRApchCUfJ7Vo1gI+i3f6K2oFmwN/Kt+S+/AFXMR1T3svOviFdi5eO+Uuybn6dV88unOcS7GIsRelFuUgQFjFzEmwH5Zbner0jh0pdVHpFavBBtzmHgCQ7YNY1eYesExjmHpTekKkXYf2TWpp716EM1nhS1IdAmuird9f4bTzVyaCQ4srjOMoWFJJzj5Osp51INMu/nACNH14QY69U8eXNjzZgvHn8ZN5BKEZqTPwebtxXvTN84dfwtwF23ewDIxcOgtdbW33m3UXQMznN12vBC4I0tkWoYYSsYiRs3MlmFXuFqXsTj3U8LHol5qY4/Wno42IUxhPFbRVm/o7tcWAwu/nbf0EhfGrOEo26AtEpib73TEYOwvPB+YhPxrL4+wQhbkCOBZqbtofqVd5xJdG9YgoZG6rchiTOHa5Ynay81fC2Fm+A4iXs7wfbFfB57bDrCAKx9A8UebaAdHPyTyQ1tnyeCfyPaevwVpUw87UUzgSr26xH2CtU7nXcqj+7DDGwvloisJunzu9l6bZAZdOsj9OYEt3huFRrj1gGd6aFKoDXafBAG1OQrmB5oteX0n3e5TqTnJa+TMUBvSWDCjxVykhEQewIpvyTKUqG7IoSptVCSjtnmJzPNQvvL9IXTEG5j9F3MPNrOmjIB44cp4abd+Zbpsxb+Xp4v8mpytwXC9T2YYTqtgIJJXcmuqLDw6S5a5T4NvijtEylN1vxlAd/62MFoOvK7EWVjKnRCsGebWssa5uDfOmqLmZ1PnFmmCgIzkGpdE2iOx+ze+4qkJfn1lFa1V5+x5LTfqUjoSdni5DOXKyN4hdCzyKh8pQjP0Q9iFt3UH6YrypoVOxxffNGGGK3rSg24PRANfMQNs/8Ah8Isb2yk+3CF/RveegqMbdTRGzdXmGHWs6t3KeeF28mOZmATxrX6/WzS6+Dbi3ZnK/bqrduuxTIhGdFZ+2w81DuoVigXottmLRyWJs894DZIqZHYmXAEikDW1vVafUzYK94k6x/hZK9Mpb89maRtXEQG2vQTxLZf3WU+XPnIIj3x3Nhq6NrOvhHkMnliU3ZlmiyfrE6ptusJ4uxs0KRxfwVklHLVvgm7/wUmWSGKjpG8wRD/d5Z+n30AOQHEkUrVvPYQ6p/E700txHXj+vejImURvVxNV1uXiKk68bQZROMqlXskmgUcVNflM+c6WiwkAZ6kWCFwQcosRyNfQge9Rq9DddtDtGo6ll9AYpuoTy+xBipKFeIfCa/vLo3f0nl08B+Qb7rdsJIsbAUunnuIPYMwTpA8Fr65+FhFN1ienXYNiHBrM/HlgHWNt2KSvwlPGmtzwoAl4spI4QtQNW8GWu6/rcrr5V42k521siiDszwWdccXPO8pmuw1S2CB4yWt9lsl6WwrNs7WgQBi0keQ7flOoVGSeKQeGZ285+XmKpgBCfolJlZnyrIHmFyi4+sbO+8LFyO3DvbwzoDMRL2JSLpI3WkUflvSOpLVMJxre/QmKJ/+T/0hRSyzKtp8V6NGtJY/bXEufPAPR1Vb2Pm2GF9+z5XmcCpLqT9PAt1JkTR5H45+mhkAwabsi8f6IftF4bvOEuK8132HCEPPEZY9ta1u7K2JlK9tBb3HFiHr2u2rmb/rpPlrmZGTa51iV10ZbH/HicT4EePSPMQqWeGeTryNIw7IVzDn9BgdaH3ePeIb0B9iAjA2f8WUpDWPrQIbme5kYW1rgkx/KXI6SGKeuaEnztMhtHHuE8vu6xUFAfBot1CCj+wg3w2yB7L0wb30jE5JWKvdM0EMPO4JzMg30YwFmW8i24L8tbnnATW3Tgwh+ZMmkxhUZxG/KKfQfLxwe0dRBOVt/IJBMbz2L3wrQ9vfSJn8aVXaevCAoiR+kMAXTixTzqVMdtF+geaOhjV/3rSnnQhouB34mrTlJrKWfglLS3C30Givglxjq6vKh0dc14mVLu7bekGR7lFPNMXivJ9A0eFsPcaf26a7bBVdFw/53qPXUlZ6U399NOAsI5umS51a676iimbrTBGezeXs4AB8VGWwVzvV0nANK54K5PRGoz3DsbOYZWQNGjTFnoDeOMYjRFr1CXI/zu2K3FaDbTD4+NjO5/zvQZDJzQpri+tnSGuyrOkwCUTGnGhb3AOIFqjbe3OBI55rc7CHBHDGi4zB785YRTIhGdFZ+2w81DuoVigXottmLRyWJs894DZIqZHYmXALgVjiD6unOUudON6VhhKwKqxeWACudMdhqRrWTSBRYYB7eQ84TRG/s8n0BuusXsPzcK4Kz7rO3nOraixybOwpN5+H6BqNUbpBp4hgmn7CsSdMVrLkNeyRcLUX8D31YwSjEftDtuKiCoAWEB+EY33edwdeXdxh95r/jW2div3Ld+Wri+yH/ikECMjIG/RtL/2KlBLnaGNPt+6VmYci9hE5gxSXqBYV/UZO1mGTroeBv0eI0M1UTsO9n9FmDsKdEWt/y9jQo3nusgSMlEdiMhiFLr9BVhCUzaFyVarIlFYXk0lDncA+4WQiVxf+sna8CI3UDKuvM5Ona7w+yKtTXlHAuYIJrSweNiSuQkIPUWqOGu75OzXzdB4ZDRXle2ar5OR9lfnqKz/fTpm2e9uFqh10xxF+B84rleosrsLTkRtdTuC8IZmPEyyuyFD6HiSngV12HJSkTJrQp5hoi6T++utEAX6CfWxHEnlZi3mJhoY0ROXHBH4AOTDplSb9qFPCRvbwcRYlCOxUpGFvSsA1BUGUWZEHg5TDPOvt9gylejM1LNRtnPgNXOUyIjYOuk/OMn6ewo8Iq5dbPPNCHZht95pc3LTkPjaVYXJz2VW38YtLecVTajNEueUcTV+3UsVMdh3f4FWfR7k6P7YUK3UeacRwGHjIK/U14DrNuzowBuZP047uz4m3PpZy/NExI6mx5272JQ22c2NTqXyypncc1CKUHwYLm1DVV7qB3dKjJY4MTFxIcExD+STFn4fBwQBzDFMBcu02ResAb70542Kz4qtqH7sOIn8kumkgZTvJa8zqxIonEJe5zFVRqb0NOnIVbXce+XHTv//8pZjc3x5PL0SXniaikGuIXk56OKEFyePm2isUL9xbndSMERIB8eE+zU9ATJC4+TicmJyoSTyHr1AASg0lGsCSN/aBTiaKI9/2CGWjjllA9ICTvlc6EOgsMARp2K8CTf6G8Vjfp760Qm8qARcqTb8lSwsXEXjTzcgsXNOH3Y3uyfM0/22vjROU6rCeyFZtPaZR5H/JGSI8xUdw4XCoPKWZMyQAsuLh4qtKVweGOcrkGeZXmQEPPvbrJz8aJG8VlPM/+hj09sRhILmphLrA1ix2eqppteEN2EPuRwEADrDh1G5bvLwfdMJ9hrYvl9L6uycfDosuEZFtBUy+ncaf26a7bBVdFw/53qPXUlZ6U399NOAsI5umS51a676iimbrTBGezeXs4AB8VGWwV+elb2xf8OT+yaiw1Gt5hsZbtd2dt57S3nlqlasEJ1pvD2pMmbu8vQ+Z0S5bs2fRl/zvQZDJzQpri+tnSGuyrOkwCUTGnGhb3AOIFqjbe3OBI55rc7CHBHDGi4zB785YTNKLsDIf4cfG3dCtMDwDMtXsUPRHaMh2m+rMN7LTVs8p0UjUSF3gyq7+iC1Fj5+G+qxeWACudMdhqRrWTSBRYYB7eQ84TRG/s8n0BuusXsPzcK4Kz7rO3nOraixybOwpN5+H6BqNUbpBp4hgmn7CsSdMVrLkNeyRcLUX8D31YwSjEftDtuKiCoAWEB+EY33edW/ktneEqzMkULYWplHG/UhwLwOzLC77RUAEbmNz0bAmRaHOCD16i+19JOLH7tgmwxSXqBYV/UZO1mGTroeBv0eI0M1UTsO9n9FmDsKdEWt/y9jQo3nusgSMlEdiMhiFLbc79CtojrdquB1OMlnqFP5HvREbpu2FaAxcsdNPt5k0DKuvM5Ona7w+yKtTXlHAsInKKoN8dxfKF83eFiY59375OzXzdB4ZDRXle2ar5OR9lfnqKz/fTpm2e9uFqh10xxF+B84rleosrsLTkRtdTuro+izHD/3RSvBfTmXlkX0+DSUOJhfk6h4J94qH/fEtvImXomZGP+JkX5FcnQO5Pf47HDc+VQuQLiqCEnbNejMlxwR+ADkw6ZUm/ahTwkb28HEWJQjsVKRhb0rANQVBlFmRB4OUwzzr7fYMpXozNSzUbZz4DVzlMiI2DrpPzjJ+kK7S8MdEBWqMB56kI/c2/wy05D42lWFyc9lVt/GLS3nFU2ozRLnlHE1ft1LFTHYd3+BVn0e5Oj+2FCt1HmnEcBh4yCv1NeA6zbs6MAbmT9OO7s+Jtz6WcvzRMSOpsedu9iUNtnNjU6l8sqZ3HNQilBISnhFf4RcBcFP0NCtt9ae3HtIQF9DMpoRIvestZmpfCoj+vHaO21bA+PqGYw9YGhFy7TZF6wBvvTnjYrPiq2oQZwL33oyHeXqNOWSeq74qmicQl7nMVVGpvQ06chVtdx75cdO///ylmNzfHk8vRJeQwAVs23stk5bAa2Lw0aaESzhLBfPYjJb1JW27EMZCTpYbE9QfG5hQ1lM8XGOPP8KKDSUawJI39oFOJooj3/YIZaOOWUD0gJO+VzoQ6CwwBGnYrwJN/obxWN+nvrRCbyoBFypNvyVLCxcReNPNyCxc04fdje7J8zT/ba+NE5TqsJ7IVm09plHkf8kZIjzFR3DhcKg8pZkzJACy4uHiq0pXB4Y5yuQZ5leZAQ8+9usnPxokbxWU8z/6GPT2xGEguamEusDWLHZ6qmm14Q3YQ+5HAABFdV/fmADbph/EAR3kgghs9e44E6yDk2N4hbWjDfAuwIPQTmYEaMJSDBQggn2CUThXjOOzsbbQ4P0pibWr7XNayH1dCBc6za0CKXBb1XLhEctuFR8ODTpG+ItzQPK3+tj1aouYhv1+vdM3MpNPDM33avgVBCRnmi1Nf13tLfY6BfZ7h8Cz9F8S9szDk8UsIR8lk/15+urQpZqJNp7yndBX63xqiiKyvIhT6mF3w4DPrYTo5xJyZ5EvvXL0Zn8mmKBBmjPui1C02ymiilsKI69rAoKisXGntbIref0Vjo+jixoSReCG54jjPU/AL0AnJYvoxaP8sLo0LkqfK5pfWjtEA7COx9tjL5/Q2x/yxIpDMXLkeeWNjghDpLKD/238QcqqQNeDSBzF/6YuN/2unkqM8I80Q4TfVfXZbNO3UZYPQzTSwd9YJOQtAtRrQKTZ8zNSLUpfJ6uYsIcG1wC9kWQCl4R/iSEixVKfQOVxDpGLv3sKmD7X6W1Y3P7JeUvb15BaErMoeojSfNRe6nLRhYAhcrbq9pS9OltXeFfLWnVo2sysun6Y9z2G5rNSEJ4YxGeuOGsFtq8VUzlf3t0qG5UZd0lFwWLvL7do2MXZ984+n30AOQHEkUrVvPYQ6p/E63Zn2K/kINOC8HLyGGqoVVY/sMCj/t0HXDjPRHtJ0LFXUJPDYAsrP/qHTPMswPTSi3/6HgCLXRnzoUnPwCqno29ZSeWs0fVUONoirlierTO1cpimiYjidCqSJprb769+QxLZxiQKerRo5tM0O/Furtj/Ys8cDjMAFD90nMC/SILB7VVQmWXK4PraLoXs99LldtUJcnPXNcHCIKJmcR/OiPt0K6b4AS9uALyxNk9R4tWEmpULdhU03ySHcQX9aTrOwKkX8wOL2dyv8ghF6oTa1ECLAkqm4CYl86PTWWtPJ1d3W5jPFX8GjiM1HV2kGCfykpZS94KRMdemqPp+SlHji24wTlQJf71D/BMYAUwhBxnX9zwls7epzGQhkm6wUHFFyCTio3g/aaEdlPm4D/mShY3CJieKHtFq5zemFxJbsKNhBdGLffGavUNhtz8AMViJV63WFy3j4BsLq25arnYlGVEu0IQ6W+Fro53NajF2lc3ZN5hnbzF4ERxYU/W0WY8h3g3G5K6qsHW7/ExkM9QAmhOTO7mzopCWCWubokWJf8+g9uEDkQ5jZIvpFEvb2LCf/wnVTXlzc4/1jC84vzGcg1Oq8sAIGGRCOVx1+XCk58AsU14ptbVh/iRm9aXBFl508NtUDTPsffku1ZCjR4xuodM0vRtq7I8zLyKeSA+MQsTCXfWIJnUttNm3xVFYuAzG0a4oBHa5aQ0Ji+zEiowAVDEa9IJ8ktZdQmHivf/Ux3PxOtbTPohJe4N5NmfQvURjmwWDqvqv8fglg74o1p5J2iRBwBKiDj+fzj4KnxTPVw3VKbsnge7m2AEkbz1zlWi6PjTais2rLt5CRG8SP3rEsSoZfhgFPBbxDCH4x9Qa0OBrBSsaE0NQdp8J6yPHApPQiT1hIN7iMiUGSM4rfK7udysUONiAFUreQ43bYo+QmQ8gYZFaXj2F/AnmH1liUTmCnpwi9QMXb8SkzSTc/7Ivkg+b/iki/3XiFEFQn59XQWB1bMFalE6h2BYM2ljWxbiiEXAsCzCU+mTjLPfFqb+Js0kAAckFbz4j9i0CckC8kDxJ72WVPY7DfR4kSqKM75JJXs+qzIqp/M+mZhGRotNQ7daFQryWQSDsJFyJM6LOlZj/OXftaqoRY6VbkCf7LtmA5IwfVQCpuI2FZT07AsC671sbddJ45Pdbu/nmtmHI4MgwmU6KpzzvQABkGtAf89kiaD9YZRthu/reLwhiN1QJAvc95MZPTcO0SIYAC1uGZvMrfPVzIa5a6S2ghA8y1iCYwgV30qgyjFGFACCbMRIfNBTbjVYSp7Q/b9RCC6uPdb/muoWaEvdV234dJypgcv6rdMa3mHn3r8mNoiZ9uSnm8Lw4HMe81fUjn0nZir+ihH6jOCYQrdINwk4SdakVJs9hI/cfXC0V3tc1p0s6QiByDrEbgPeFoLfOqQzizyckaujPKH9TZKsdm8quuBrLhw1tweCfgCSbtNFjSxA8aUHwp8S2cYTE/xg2r+Wx1PfMnvS6a1U94FTpFDo0/CFK49xWOj9bCAJCtLQJ9HjwZLbHtYb9+ETBGl+BYdtYAKkqr8OWv9avqLbVAYjbeG6sJ/AzGkaz9rGFym09qNrinEgg+OGTqW5nYvloiQ+usLHZ5kkW9kPNojls4KgUAWtJ6vyWE/eHTQSCmUL4Y9M95+miebGWxI5FX1dOLPEpJmU9FXDYFBUR2VDsjtbX4NrLabsm1plWrDnJI0Ur1HO65TUXOpU0xYmMN/bTWvOh9TJ4eWK/iWupJcvZZwRoQ8boWSVptIOIl99UNMjNvDNsNapaSbIYv0s4IqlcYoo/pifLFhLrVbmFlgr7CT9wAhhhbkmURx7uZxMrFUpibOUKDsbZW6W4aP7SW0WRLMgPAg4LZdgvkDLs6YS2XU+Cl8tNHSekmOt1MdLinPejLxrZaSHJXGwdpVMxHqHPq/xzMHAK6R0593eZF74bYbAwBpIXPWIzS+rsSS8pvGzUyU+Wi8AUtRgbAZc+5fGejLcZgmh+PqY/1aLXlFioXlNyVD2O8m4RDwFiFATHGXQDpNq0JTF15W4cI05+2UPKsqqwbB/fNvYdBIMyohBqpEv+WQHFH+isXfaih5sRjCZIch0FWK8wL+9cANodocMfSab0e0WTnoIyYNxUkb05Sn3eUkldVqDyjNf6Ut/yJit7DuOVw7wAyO9sDb8pfsPT4W1RxfXHqRAm8qr/0zheSTQZVxQdSkZf6dIA5tzMO12HGopsP5J87nNXbV3VV/gbO9QvpEJ5OV68cE03K/SswjnvEehbsWbARbVE6gapQ9b3x0DNnZ58f7q8oxxMlqZxtkzSOEDcp5yRouVMvElIYc7MAfNeU+fLeVD6qkzjNwtQr0GBNohRqiq+cN4IP/cXxM7Ahy8DeaGiOxbEa64IOeb69ND8UDuV0knZXCszhv0eDQv9sJoJmr8dHglWkmEe3K5hpdimxJ0n/ZGIHwl10GzTO94VYxBvdgo6nxRYGFKLRvif8EX2j+w/ONKOJdl+n6MWCACt/bE9tHFdr4mkOXmwbr3Mh0u1nw/WVjuxbSFakQpUP68X1ZgcYOzzhjEJNwaKwfjRykGIf6JZEkT/IkMYAHEt7Jv0jdnISPCdnkZBxlC+TP8UqNe+isU70H0w83RdYO9KrY9BCPncbtkr3RUBVUoulfzUbc0JPi9MV661ej+VVgwx5EQOqiKeZ1wteF0WAAqRaiDub+Z873eT09Ycn7IU0C5jSCyRqK+yf3knnTD129DSm8XIGeMtBZOCxP9vez697zbxCtuICjXQBUCMwbRybRIoiPAWXi2T3g1ipJcd8Ie5N5h7ysgl6GTXQWN7nx9sNpsQNsx/LbHpErnWvvUVg4IIMfOmckKXLUWLb4BRrToDYcWrpJkqJNNPSpUiIf3sUu6NNEK/Rw947qG7xe2f56gea2PIBYvW95PvR/Pf82mUgAmNp3ndFElcGznY/S6Kf1B6r/AoaTeLmjoUV2O0EN+SI3RATDhEJUkxpyiZeZEpG/0U14rDgkKovr133XTGXPi6eZNh1SUIltBHatEeh32wF1WPH0c9WUEX3kO9tlexZGCXE0m1vwe+223GN7lJTSPgbhnx1GmuTIrdcQp6yBRcGRRh08j+01DOrSTUk2ItDXITmvh5OQGed7vexnevirxUKEuG1rjuwR7t5DeTHvXSDECztOfdeL5x3WV4trLKa5Fy7TZF6wBvvTnjYrPiq2oQDUvbkg01ifG7kFKkGW5Khevmx0dXQbIgcJVeiKBa1CcrQXsI9IPXs9WV+mBgnCC8qF2xsywiOqN+rckLK2gSncI9Link851NKv7ADv+3kIXvOsIdW487OS8KOCivOp+Pen6RmOEtX89ziInkzsSmmMPEH/yYG81FOGTdjygspDiItl+HASe9B9T6ca/97jE56kqMeqBSydm+Cxa43OyfAM3zprQuIiTMEST4GyO+Joh/5O8rbQijGmIDwe3OWDA4eB4px96iqR7FCJTZgHxdgc7bZxahZnI1Oz9keF5+LDmr8xdqgUC4ewec/+PJSXz6lgL+gGJzVGO2RFLSHUXmXBfBNRvkBdOPlUFBhNpqUV3oksK3KnyqJB6ZW6I3B6+0W3mDv8WS23/kpI0tSNT2J1gH5QxLXAkxCzI3rVJ/nLCE8v5CAxcSbp+XolqL8TSN+lqq2BF2zbVZOVVsSZwMHQbzEoJP8EgTTPpi7aOkj1E+CI/7YghcHPrcS0w8No1F22pFCaBfgjhRGtmKOAgxobtaM6olV49/ZEZyqA3XfVF78cRegGrns/5JbIkjoOfMUFlLHXIMH/qKpXN5Q7JpAHRaaAaLWV/eiy1FY8GRCvAHC9Xf/LRCFmTgydPM+8bYPRANfMQNs/8Ah8Isb2yk/Kc/CswMr055MuhWoYwvp3hdMMZOjkdssAwDa4nAitv/M0g+YmzcZ09IjuW6T9FfXR/0AkGZSbICVDOTrwSHJ8iXUfYoJqUmadCU44/fXmCk1XkfORT6uU5XWS15Puy4quCRNwkLT4cWuAlBunBhCqF08fGzHFikhC3bcl6wqQXGMNYXSVXB2Lmkm1Uv8mqQcexzLGP6/kLkWPO9m9yAeygA4fLokvixCcWEtLIbd4gkkOsduizOogK40jWV5N9RPbQQT6XCuhj6K/UA3o5eT1nMFFv2ZrIzlsa7LEs9nxDmhCO0lAuZP0yGqiicpgNR5DD5EfWRfTE1uPSAUs6yWAVsUDmVlTRkPUildv7j4+8rhmUGSsRnMkUX/HcmWt8MtqKsDBJQFL0Fm4gjz3cp3LgikdyHCRGUe/cOicFilsVjk7Iqc9wvy1Zpt5JK+gy7y7Acsm5q5+e4M7eTK+yUTAXSVDEe3garwkNQgpjSrZOyyH6TwJvaxjPvRgtorDGIx9CRHcH0gnYtxom84yxnLHEc547kX7kxl/3Wf2SGf4bap7KPz2Y0iA24PtuZxc0ObAhYgRiRqxXyLeQhd9EHgxcMKH4FbJBOC3Nsk3zf4W+z49vdbMRVed3q2qsbSPTB0YynFP+7FgKxN/+CHmWZZRDpwXX0mEUGv7zObZdAY9qi7W5sywjpwAKHM0wUckemXpoavuVOTPqHziq9GHIXdjqI/rx2jttWwPj6hmMPWBoRcu02ResAb70542Kz4qtqH7sOIn8kumkgZTvJa8zqxIonEJe5zFVRqb0NOnIVbXce+XHTv//8pZjc3x5PL0SXmWBjFwKt5ILDQYX3BUZBTTmm9jOWuDHYh5mfD4WxSUZ6VmIgspoSpAXENo0DSN0D6g0lGsCSN/aBTiaKI9/2CGWjjllA9ICTvlc6EOgsMARp2K8CTf6G8Vjfp760Qm8qARcqTb8lSwsXEXjTzcgsXNOH3Y3uyfM0/22vjROU6rCeyFZtPaZR5H/JGSI8xUdw4XCoPKWZMyQAsuLh4qtKVweGOcrkGeZXmQEPPvbrJz8aJG8VlPM/+hj09sRhILmphLrA1ix2eqppteEN2EPuRwegkTFj3NOdVjOppxwkhfLilpbHxMzL5g17XRT7xSHHsQIbb3rds0chqOwyDerqi1V6kxZSDZqhZj2TESUi86+k8IYmcbZ6v9RTPIqSIkgph7b97hoTCa7+49247gR5S/8DsypwJQKA6GKaEQoLUO4MawGz/55XqHwEytSVAuEiq265Rwcshx3eK8/URntnWq478WTT/5I5XhAcDDT9PECul1/8+XqqP0qjQAdMb/WsbtArbVBT40UGhk/Mh2sZTZavtq+GvypGfgLucPkZ0YgGYOgpYS+xp2QvLuxU9JLNqglDj96o6c2jFsLa6OJqFEoLnJ7NsC/EtrMLPh5Y7++R7xUG55FuQiUs3N7xsSorBjYJMGPFGfoApL6PPCFOpi"
    en_data = base64.b64decode(msg)
    key_data = bytes("e@s#g$c%c_l&o*g_i@n", encoding="utf8")

    # sm4_d = Sm4()
    # sm4_d.sm4_set_key(key_data, DECRYPT)
    # de_data = sm4_d.encrypt(en_data)
    # i = de_data[-1]
    # for j in range(i):
    #     de_data.pop()
    # print("after decode：", bytes(de_data))

    rs = encrypt(DECRYPT, key_data, en_data)
    print("after decode：", bytes(rs))

