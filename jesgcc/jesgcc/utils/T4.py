import binascii


def hexStr_to_str(hex_str):
    hex = hex_str.encode('utf-8')
    str_bin = binascii.unhexlify(hex)
    return str_bin.decode('utf-8')


if __name__ == "__main__":
    hex_str = '7468616e6b20796f752076657279206d75636821'
    print(hexStr_to_str(hex_str))
