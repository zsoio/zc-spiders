# -*- coding: utf-8 -*-
import re


def match_catalog_id(url):
    # url = "/search/goodsSearch?searchType=gcIdSearch&keyword=26,2601,2601001&grade=1"
    if url:
        cat_ids = re.findall(r"&keyword=([\w\W]*)&grade=", url.strip())
        if len(cat_ids):
            arr = cat_ids[0].strip().split(",")
            if len(arr) > 0:
                return arr[len(arr) - 1].strip()


def match_sp_sku_url(url):
    # url = "parityUrl('http://www.stbchina.cn/item.html#?itemId=1100011802EA')"
    if url:
        arr = re.findall(r"(http.*)'", url.strip())
        if len(arr) > 0:
            return arr[len(arr) - 1].strip()


def match_catalog_info(url):
    # url = "/search/goodsSearch?searchType=gcIdSearch&keyword=26,2601,2601001&grade=1"
    if url:
        cat_ids = re.findall(r"&keyword=([\w\W]*)", url.strip())
        if len(cat_ids):
            arr = cat_ids[0].strip().split(",")
            if len(arr) > 0:
                return len(arr), arr[len(arr) - 1].strip()


# 测试
if __name__ == '__main__':
    pass
