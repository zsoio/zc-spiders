# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.util.http_util import retry_request
from zc_core.model.items import Box
from zc_core.util.batch_gen import time_to_batch_no
from ccb.rules import *
from zc_core.spiders.base import BaseSpider


class SkuDeliSpider(BaseSpider):
    name = 'sku_deli'
    # 常用链接
    cat_url = 'https://ibuy.ccb.com/mall-basedoc/ecware_category/channel/0'
    sku_list_url = 'https://ibuy.ccb.com/mall-basedoc/search/mallProduct/allMallProduct?searchType=1'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuDeliSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.page_size = 20
        self.max_page_limit = 500

    # 商品列表
    def _send_sku_req(self, page):
        data = json.dumps({
            "keyWord": "none",
            "channelId": "3",
            "isAllowPower": "",
            "page": page,
            "pageSize": self.page_size,
            "isComputePS": True,
            "sortParam": {
                "isSort": True,
                "sortField": "sales_volume",
                "isDesc": True
            },
            "address": {
                "area0": {
                    "code": "1",
                    "name": "北京"
                },
                "area1": {
                    "code": "72",
                    "name": "朝阳区"
                },
                "area2": {
                    "code": "2799",
                    "name": "三环以内"
                },
                "detail_address": "默认地址"
            },
            "filterParam": [],
            "aggFilterParam": []
        })
        return Request(
            method='POST',
            url=self.sku_list_url,
            meta={
                'reqType': 'sku',
                'batchNo': self.batch_no,
                'page': page,
            },
            headers={
                'Host': 'ibuy.ccb.com',
                'Connection': 'keep-alive',
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'Origin': 'https://ibuy.ccb.com',
                'X-Requested-With': 'XMLHttpRequest',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3861.400 QQBrowser/10.7.4313.400',
                'Content-Type': 'application/json',
                'Referer': 'https://ibuy.ccb.com/market/public/ecpage/index/index.html?sessionId={}'.format(
                    self.session_id),
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'zh-CN,zh;q=0.9',
            },
            body=data,
            callback=self.parse_sku_list,
            errback=self.error_back,
            priority=200,
        )

    def start_requests(self):
        # cookies = SeleniumLogin().get_cookies()
        cookies = {'SESSION': '081dd762-af70-4159-929b-50955a6d1ad4'}
        if not cookies:
            self.logger.error('init cookie failed...')
            return
        self.logger.info('init cookie: %s', cookies)
        self.session_id = cookies.get('SESSION', '')

        # 品类
        yield Request(
            url=self.cat_url,
            meta={
                'reqType': 'catalog',
                'batchNo': self.batch_no,
            },
            headers={
                'Host': 'ibuy.ccb.com',
                'Connection': 'keep-alive',
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'Origin': 'https://ibuy.ccb.com',
                'X-Requested-With': 'XMLHttpRequest',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3861.400 QQBrowser/10.7.4313.400',
                'Content-Type': 'application/json',
                'Referer': 'https://ibuy.ccb.com/market/public/ecpage/index/index.html?sessionId={}'.format(
                    self.session_id),
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'zh-CN,zh;q=0.9',
            },
            cookies=cookies,
            callback=self.parse_catalog,
            errback=self.error_back,
            priority=200,
        )

    def parse_catalog(self, response):
        # 处理品类列表
        cats = parse_catalog(response)
        if cats:
            self.logger.info('品类: count[%s]' % len(cats))
            yield Box('catalog', self.batch_no, cats)

        # 商品列表
        req = self._send_sku_req(0)
        yield req

    # 处理sku列表
    def parse_sku_list(self, response):
        cur_page = response.meta.get('page')

        # 处理商品
        sku_list = parse_sku(response)
        if sku_list:
            self.logger.info('清单: page=%s, cnt=%s' % (cur_page, len(sku_list)))
            yield Box('sku', self.batch_no, sku_list)
        else:
            self.logger.info('分页为空: page=%s' % cur_page)

        if cur_page == 0:
            total_page = parse_total_page(response)
            self.logger.info('总页数: total=%s, size=%s' % (total_page, self.page_size))
            for page in range(1, total_page + 1):
                if page <= self.max_page_limit:
                    yield self._send_sku_req(page)
