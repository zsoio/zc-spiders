# -*- coding: utf-8 -*-
import scrapy
from zc_core.model.items import Box
from scrapy import Request
from zc_core.util.http_util import retry_request
from zc_core.spiders.base import BaseSpider
from plap.rules import *
from scrapy.exceptions import IgnoreRequest


class CatalogSpider(BaseSpider):
    name = 'catalog'
    # 常用链接
    index_url = 'http://mall.plap.cn/'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(CatalogSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        # 品类、品牌
        yield Request(
            url=self.index_url,
            meta={
                'batchNo': self.batch_no,
            },
            callback=self.parse_catalog,
            errback=self.error_back,
            priority=200,
            dont_filter=True
        )

    def parse_catalog(self, response):
        cats = parse_catalog(response)
        if cats:
            self.logger.info('品类: count[%s]' % len(cats))
            yield Box('catalog', self.batch_no, cats)
