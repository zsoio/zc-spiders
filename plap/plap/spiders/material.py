# -*- coding: utf-8 -*-
import random
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from datetime import datetime
from plap.rules import parse_commodity
from zc_core.dao.batch_dao import BatchDao
from zc_core.dao.spu_pool_dao import SpuPoolDao
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from plap.utils.login import SeleniumLogin
from zc_core.spiders.base import BaseSpider


class MaterialSpider(BaseSpider):
    """
    用于物料编码的初始化，可不周期采集
    """
    name = 'material'
    custom_settings = {
        'CONCURRENT_REQUESTS': 36,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 36,
        'CONCURRENT_REQUESTS_PER_IP': 36,
    }
    # 常用链接
    commodity_url = 'http://mall.plap.cn/commodities/{}'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(MaterialSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)

    def start_requests(self):
        # cookies = SeleniumLogin().get_cookies(self.name)
        cookies = {'_session_id': '530923b472b7a74214ad3c28a27a64b0'}
        if not cookies:
            self.logger.error('init cookie failed...')
            return
        self.logger.info('init cookie: %s', cookies)

        pool_list = SpuPoolDao().get_spu_pool_list(fields={}, query={'materialCode': {'$exists': False}})
        self.logger.info('全量：%s' % (len(pool_list)))
        random.shuffle(pool_list)
        for spu in pool_list:
            spu_id = spu.get('_id')
            # 采集物料
            yield Request(
                url=self.commodity_url.format(spu_id),
                meta={
                    'reqType': 'group',
                    'batchNo': self.batch_no,
                    'spuId': spu_id,
                },
                cookies=cookies,
                callback=self.parse_commodity,
                errback=self.error_back,
                priority=25,
            )

    # 处理Commodity
    def parse_commodity(self, response):
        meta = response.meta
        spu_id = meta.get('spuId')
        supply = parse_commodity(response)
        if supply:
            self.logger.info('物料: spu=%s, mtc=%s' % (spu_id, supply.get('materialCode')))
            yield supply
        else:
            self.logger.info('无物料: spu=%s' % spu_id)
