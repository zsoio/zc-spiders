# -*- coding: utf-8 -*-
import random
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from datetime import datetime
from zc_core.spiders.base import BaseSpider
from plap.utils.login import SeleniumLogin
from zc_core.dao.batch_dao import BatchDao
from zc_core.dao.item_data_dao import ItemDataDao
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request

from plap.rules import parse_sales
from plap.utils.done_filter import DoneFilter


class SaleSpider(BaseSpider):
    name = 'sale'
    custom_settings = {
        'CONCURRENT_REQUESTS': 32,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 32,
        'CONCURRENT_REQUESTS_PER_IP': 32,
    }
    # 常用链接
    item_url = 'http://mall.plap.cn/products/{}'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SaleSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)
        # 避免重复采集
        self.done_filter = DoneFilter(self.batch_no)

    def start_requests(self):
        cookies = SeleniumLogin().get_cookies(self.name)
        # cookies = {'_session_id': '91d56bf9a98151013b658c39c764f7d1'}
        if not cookies:
            self.logger.error('init cookie failed...')
            return
        self.logger.info('init cookie: %s', cookies)
        item_list = ItemDataDao().get_batch_data_list(self.batch_no, query={'soldAmount': {'$exists': False}})
        self.logger.info('目标：%s' % (len(item_list)))
        random.shuffle(item_list)
        for item in item_list:
            sku_id = item.get('_id')
            # 采集销售额
            yield Request(
                url=self.item_url.format(sku_id),
                meta={
                    'reqType': 'item',
                    'batchNo': self.batch_no,
                    'skuId': sku_id,
                },
                cookies=cookies,
                callback=self.parse_sales,
                errback=self.error_back,
                priority=25,
            )

    # 处理ItemData
    def parse_sales(self, response):
        meta = response.meta
        sku_id = meta.get('skuId')
        data = parse_sales(response)
        if data:
            self.logger.info('销售额: sku=%s, amt=%s' % (sku_id, data.get('soldAmount')))
            yield data
        else:
            self.logger.error('销量异常: [%s]' % sku_id)
