# -*- coding: utf-8 -*-
from zc_core.client.mongo_client import Mongo
from zc_core.util import file_reader


def export_sql():
    sql_tpl = "update sp_sku_info set material_code='{}' where sku_id='{}';"
    collection = 'item_data_pool'
    bulk_list = list()
    items = Mongo().list(collection, fields={'_id': 1, 'materialCode': 1}, query={'materialCode': {'$ne': None}})
    print('size: %s' % len(items))
    for item in items:
        _id = item.get('_id')
        material_code = item.get('materialCode')
        bulk_list.append(sql_tpl.format(material_code, _id))

    print('start write...')
    if bulk_list:
        file_reader.write_text('D:/update.sql', '\n'.join(bulk_list))


if __name__ == '__main__':
    export_sql()
