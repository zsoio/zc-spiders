from pymongo import InsertOne

from zc_core.client.mongo_client import Mongo

rows = Mongo().get_collection('data_20201113').distinct("spuId")
done = []
for row in rows:
    done.append(InsertOne({
        '_id': row,
        'count': -1,
        'status': -1,
        'batchNo': 20201113,
    }))

Mongo().bulk_write('item_log_20201113', done)