# encoding=utf-8
"""
响应对象业务校验
"""
from scrapy.exceptions import IgnoreRequest
from zc_core.middlewares.validate import BaseValidateMiddleware


class BizValidator(BaseValidateMiddleware):
    pass

    # def validate_item(self, request, response, spider):
    #     if '产品下架' in response.text or '您访问的页面没有找到' in response.text:
    #         spu_id = request.meta.get('spuId')
    #         raise IgnoreRequest('[Item]下架：[%s]' % spu_id)
    #
    #     return response
