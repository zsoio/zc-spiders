# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from zc_core.model.items import Box
from gd_purchase.rules import *
from scrapy.utils.project import get_project_settings
from zc_core.spiders.base import BaseSpider
from zc_core.client.mongo_client import Mongo
from zc_core.dao.item_pool_dao import ItemPoolDao
from pymongo import UpdateMany

#TODO 对full的supplierName进行补充
class FixSupplierNameSpider(BaseSpider):
    name = 'fix_supplier_name'
    # 商品详情链接
    index_url = 'https://gdgpo.czt.gd.gov.cn/gpmall-goodslibrary-interface/goodsprice/getGoodsDetailByGoodspriceguid?goodspriceguid={}'
    # 商品列表页 post
    sku_list_url = 'https://gdgpo.czt.gd.gov.cn/gpmall-goodslibrary-interface/goodsprice/goodspricelist'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(FixSupplierNameSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.page_size = 20
        self.settings = get_project_settings()

    def start_requests(self):
        # 进行补数据
        mongo = Mongo()
        update_bulk = list()
        district_pool = mongo.get_collection('item_data_pool').distinct("supplierName",
                                                                        {'supplierName': {"$ne": None}})
        for supplierName in district_pool:
            first_data = mongo.get_collection("data_" + str(self.batch_no)).find_one({"supplierName": supplierName})
            item = {
                "supplierId": first_data.get('supplierId'),
                "supplierName": first_data.get('supplierName')
            }
            update_bulk.append(UpdateMany({'supplierId': item.get('supplierId')}, {'$set': item}, upsert=False))
        print(len(update_bulk))
        mongo.bulk_write("data_" + str(self.batch_no), update_bulk)
        mongo.bulk_write("item_data_pool", update_bulk)
        mongo.close()
        print('任务完成~')

        pool_list = ItemPoolDao().get_item_pool_list(query={"$or": [{"supplierName": None}, {"supplierId": None}]},
                                                     fields={"_id": 1, "batchNo": 1})
        self.logger.info(f"目标:{len(pool_list)}")
        # 品类、品牌
        for data in pool_list:
            yield Request(
                url=self.index_url.format(data.get('_id')),
                meta={
                    'reqType': 'item',
                    'batchNo': data.get('batchNo'),
                    'skuId': data.get('_id')
                },
                headers={
                    'access_token': 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyU3RhdHVzIjoiMSIsImxvZ2luVHlwZSI6MSwidXNlcl9uYW1lIjoi6KW_5YyX5Yy65Z-fIiwidXNlclR5cGVJbmZvcyI6W3sidXNlclR5cGVJZCI6IjQiLCJjb21tb25UeXBlIjoiNCIsInN5c3RlbVR5cGUiOiIzIiwic3RhdHVzIjoiMSJ9XSwidXNlck5hbWUiOiLopb_ljJfljLrln58iLCJ1c2VySWQiOiJGNkE3RDk0QzA5QTg0OTFBOTVEMUE4QjRFNTg4MDhDMCIsImNsaWVudF9pZCI6ImdwLWdhdGV3YXktY2VudGVyIiwibG9naW5SZWdpb25Ob3ciOiI0NDAwMDEiLCJhdWQiOlsiQUxMIl0sImlkZW50aXR5VHlwZSI6MSwidXNlckFjY291bnQiOiJ4YjIwMjEiLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXSwic3lzdGVtVHlwZSI6IjMiLCJ1c2VyVHlwZU5vdyI6IjQiLCJleHAiOjE2Mjg1NzU3ODgsImp0aSI6ImJkNWU0MTdiLTEwZjktNGM0MC1hZjllLWFiYTNiMjljYWM4OSJ9.UlWFERFvfWUKSb8sBHeKqnsH4m0fkRNkbn2UesLtwKXGOXEYA6RVYPZziZ1jPIw4LpPfyB5aNMBg8RjPtF4qBikcTvjUPGejX2TcZgvr1_6Uj_30yRyaphwAmXcPBS0RqnntnYfTGZbVCou0s-3mfWTAeg9ZLkaJ1flj_q3OGGbO8bsG_uAuCcbe5YvjZfuGo3uTe61JgWRl96E9GeSwSjW2m93QgZ2u4vXYgsFsrFxwKgxoABM-hn8c0ufTOnxH7LCWdP1j7Pa1235zENBTiQVYMSkLQ4TDwbFMVjV-lSbTIHvIXYOnFedePhtUvZfWSbRZDsvRVqznMTKfvV4-vA'
                },
                callback=self.parse_supplier,
                errback=self.error_back,
                dont_filter=True,
            )

    def parse_supplier(self, response):
        meta = response.meta
        batch_no = meta.get('batchNo')
        # 处理商品详情
        cats = parse_item_supplier(response)
        if cats:
            self.logger.info('商品:[%s] 供应商名称:[%s]' % (cats.get('skuId'), cats.get('supplierName')))
            yield cats
