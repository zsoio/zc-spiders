# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from zc_core.model.items import Box
from gd_purchase.rules import *
from scrapy.utils.project import get_project_settings
from zc_core.spiders.base import BaseSpider
from gd_purchase.utils.group_helper import GroupHelper
from zc_core.dao.item_data_dao import ItemDataDao


class SkuSpider(BaseSpider):
    name = 'group'
    # 电子卖场同款商品，同类商品链接
    index_url = 'https://gdgpo.czt.gd.gov.cn/gpmall-web-interface/goodsprice/sameTypeGoods?goodspriceguid={}'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.page_size = 20
        self.settings = get_project_settings()
        self.spu_helper = GroupHelper()
        self.spu_helper.init(self.batch_no)

    def start_requests(self):
        pool_list = ItemDataDao().get_batch_data_list(self.batch_no, query={"spuId": {"$exists": False}},
                                                      fields={"_id": 1, "batchNo": 1})
        self.logger.info(f"目标:[{len(pool_list)}]")
        for data in pool_list:
            yield Request(
                url=self.index_url.format(data.get('_id')),
                meta={
                    'reqType': 'group',
                    'batchNo': data.get('batchNo'),
                    'skuId': data.get('_id')
                },
                callback=self.parse_group,
                errback=self.error_back,
                dont_filter=True,
            )

    # 处理sku列表
    def parse_group(self, response):
        # 处理品类列表
        meta = response.meta
        sku_id = meta.get('skuId')
        group, done_set = parse_group(response)
        self.spu_helper.add(done_set)
        if group:
            # 同款标识
            group['batchNo'] = self.batch_no
            yield group
            # 同款数据
            self.logger.info(
                '同款: sku=%s, group=%s' % (sku_id, len(group.get('skuIdList', []))))
        else:
            self.logger.info('无同款: sku=%s' % sku_id)
