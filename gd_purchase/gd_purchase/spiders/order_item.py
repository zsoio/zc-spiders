# -*- coding: utf-8 -*-
import random
from scrapy import Request
from zc_core.dao.batch_dao import BatchDao
from zc_core.dao.sku_dao import SkuDao
from zc_core.util.done_filter import DoneFilter
from zc_core.spiders.base import BaseSpider
from gd_purchase.rules import *


class ItemSpider(BaseSpider):
    name = 'order_item'
    # 订单列表
    order_list_url = 'https://gdgpo.czt.gd.gov.cn/gpmall-dataplatform-interface/orders/getTradeRecordPageList'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(ItemSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)
        # 避免重复采集
        self.done_filter = DoneFilter(self.batch_no, filter_key='spuId')
        self.size = 10

    def _build_list_req(self, spu_id, page):
        return Request(
            method='POST',
            url=self.order_list_url,
            meta={
                'reqType': 'order',
                'batchNo': self.batch_no,
                'spuId': spu_id,
                'page': page
            },
            body=json.dumps({"goodspriceguid": spu_id, "regionguid": "2137001", "pageno": page,
                             "pagesize": self.size}),
            headers={
                'Content-Type': 'application/json'
            },
            callback=self.parse_item_data,
            errback=self.error_back,
            dont_filter=False
        )

    def start_requests(self):
        settings = get_project_settings()
        pool_list = SkuDao().get_batch_sku_list(self.batch_no)
        self.logger.info('全量：%s' % (len(pool_list)))
        random.shuffle(pool_list)
        for spu in pool_list:
            spu_id = spu.get('_id')
            # 避免无效采集
            offline_time = spu.get('offlineTime', 0)
            if offline_time > settings.get('MAX_OFFLINE_TIME', 2):
                self.logger.info('忽略: [%s][%s]', spu_id, offline_time)
                continue
            if self.done_filter.contains(spu_id) and not settings.get('FORCE_RECOVER', False):
                self.logger.info('已采: [%s]', spu_id)
                continue
            # 采集商品数据
            yield self._build_list_req(spu_id, 1)

    # 处理商品数据
    def parse_item_data(self, response):
        meta = response.meta
        spu_id = meta.get('spuId')
        current_page = meta.get('page')
        datas = parse_order_item(response)
        if datas:
            self.logger.info('[订单]: [%s] page=%s' % (spu_id, current_page))
            for data in datas:
                yield data
        if current_page == 1:
            totals = parse_order_total_page(response)
            for page in range(2, totals + 1):
                yield self._build_list_req(spu_id, page)
