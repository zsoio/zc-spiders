import re


# 从链接中解析品类编码
def match_cat_id(link):
    # product/search?catalogId=8a8991f94fba0a9b014fba0b67a90000
    if link:
        arr = re.findall(r'catalogId=(\w+)&*', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 测试
if __name__ == '__main__':
    pass
