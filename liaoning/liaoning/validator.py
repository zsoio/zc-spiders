# encoding=utf-8
"""
响应对象业务校验
"""
from pyquery import PyQuery
from scrapy.exceptions import IgnoreRequest

from zc_core.middlewares.validate import BaseValidateMiddleware


class BizValidator(BaseValidateMiddleware):

    def validate_item(self, request, response, spider):
        meta = request.meta
        sku_id = meta.get('skuId')
        jpy = PyQuery(response.text)
        status = jpy('input.productStatus')
        # productStatus：2、下架
        if status and status.attr('value') in [2]:
            raise IgnoreRequest('下架：[%s]' % sku_id)

        return response
