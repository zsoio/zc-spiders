from pymongo import UpdateOne
import copy
from zc_core.client.mongo_client import Mongo

mongo = Mongo()


def fix_data_full():
    update_bulk = list()
    src_list = mongo.list('data_20210626')
    for item in src_list:
        item_copy = copy.deepcopy(item)
        item['_id'] = str(item_copy['_id'])
        item['supplierName'] = item_copy['supplierId']
        item['supplierId'] = item_copy['supplierName']
        update_bulk.append(UpdateOne({'_id': item['_id']}, {'$set': item}, upsert=True))
    print(len(update_bulk))
    #
    # rows = [20210519, 20210518, 20210516, 20210514, 20210511, 20210509, 20210505, 20210504, 20210430, 20210427,
    #         20210425, 20210422, 20210420, 20210418, 20210415, 20210413, 20210412, 20210411, 20210409, 20210408,
    #         20210407, 20210406, 20210405, 20210404, 20210402, 20210401]
    # rows = [20210519]
    batchNo = 'data_20210626'
    # for batch_no in rows:
    mongo.bulk_write(f'{batchNo}', update_bulk)
    mongo.close()
    print('任务完成~')


def fix_data_spuId():
    update_bulk = list()
    src_list = mongo.list('sku_20210626')
    for item in src_list:
        item['_id'] = str(item['_id'])
        item['spuId'] = str(item['spuId'])
        update_bulk.append(UpdateOne({'_id': item['_id']}, {'$set': item}, upsert=True))
    print(len(update_bulk))
    mongo.bulk_write(f'data_20210626', update_bulk)
    mongo.close()
    print('任务完成~')


if __name__ == '__main__':
    pass
