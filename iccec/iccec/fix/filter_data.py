# encoding:utf-8
from pymongo import UpdateOne
from zc_core.client.mongo_client import Mongo

mongo = Mongo()


# 过滤数据
def filter_data():
    update_bulk = list()
    src_list = mongo.list("full_data_20210811", query={
        "$or": [{"supplierId": {"$ne": "8D1AA3513C4E71664FA62529D3236A49469803DF46A5F8CDC4452CE381853600"}},
                {"supplierId": "8D1AA3513C4E71664FA62529D3236A49469803DF46A5F8CDC4452CE381853600",
                 "soldCount": {"$gt": 0}}]})
    for item in src_list:
        update_bulk.append(UpdateOne({'_id': item.get('_id')}, {'$set': item}, upsert=True))
    print(len(update_bulk))
    mongo.bulk_write('data_20210811', update_bulk)
    mongo.close()
    print('任务完成~')


if __name__ == '__main__':
    filter_data()
