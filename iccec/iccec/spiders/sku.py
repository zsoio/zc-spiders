# -*- coding: utf-8 -*-
import json
import scrapy
from iccec.rules import *
from scrapy import Request
from datetime import datetime
from scrapy.exceptions import IgnoreRequest
from zc_core.util.http_util import retry_request
from zc_core.model.items import Box
from zc_core.util.batch_gen import time_to_batch_no
from iccec.utils.login import SeleniumLogin
from scrapy.utils.project import get_project_settings
from zc_core.spiders.base import BaseSpider


class SkuSpider(BaseSpider):
    name = 'sku'
    # 获取三级分页链接
    index_url = 'https://chaoshi.iccec.cn/apis/cccs/users/signup/indexpage/qryGuideCategoryForEmz'
    # 获取所有商品列表链接
    sku_list_url = 'https://emos.iccec.cn/apis/emos/cc/users/signup/searchBoxList'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.page_size = 60
        self.cookies = SeleniumLogin().get_cookies()
        self.max_page_limit = 84
        self.settings = get_project_settings()
        self.PRICE_LADDER = self.settings.get('PRICE_LADDER')
        self.settings = get_project_settings()
        self.SKU1_WHITE_LIST = self.settings.get('SKU1_WHITE_LIST')

    def _build_list_req(self, page, catalog1Name, catalog1Id, catalog2Name, catalog2Id, catalog3Name,
                        catalog3Id, leftPrice, rightPrice):
        return Request(url=self.sku_list_url,
                       method='POST',
                       callback=self.parse_sku_content_deal,
                       errback=self.error_back,
                       meta={
                           'batchNo': self.batch_no,
                           'catalog1Name': catalog1Name,
                           'catalog1Id': catalog1Id,
                           'catalog2Name': catalog2Name,
                           'catalog2Id': catalog2Id,
                           'catalog3Name': catalog3Name,
                           'catalog3Id': catalog3Id,
                           'page': page,
                           'leftPrice': leftPrice,
                           'rightPrice': rightPrice
                       },
                       body=json.dumps(
                           {"pageNo": page, "pageSize": self.page_size, "skuStatus": 2, "isCheck": True,
                            "catalogType": "1",
                            "platformCat": catalog3Id, "catalogLevel": "4", "active": 0, "province": "1",
                            "city": "2802",
                            "county": "2821", "town": "0", "agentId": 100106, "leftPrice": leftPrice,
                            "rightPrice": rightPrice}),
                       ##{"pageNo":1,"pageSize":60,"skuStatus":2,"isCheck":true,"catalogType":"1","platformCat":"8635","catalogLevel":"4","active":0,"province":"1","city":"2802","county":"2821","town":"0","agentId":100106}
                       dont_filter=True,
                       headers={
                           'Authorization': 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE5MzQ3ODYyNzAsInVzZXJfbmFtZSI6IjEzMzI4MDU5NTEwIiwianRpIjoiZThjOGQxMDItM2Q3NS00MmEwLWFjNjUtZDdjNjYwZDhmMzdhIiwiY2xpZW50X2lkIjoicmVzb3VyY2UxIiwic2NvcGUiOlsicmVhZCJdfQ.EMPU0uMTYT35wde6dR7M59LNan4WiZQu1xGLkbXIzg8jTywshF_V4aVp2n0QLTBahAn3856SZ5nCwPXZa500hIUdLiu4ao1Vkbgkz3FTfFVni9Bl6aS2X6LzSw6oatKyFdrc5ItEswLNWwsrU7Sz_tLeETkb7gCL-fs3Lz-t9gs',
                           'Content-Type': 'application/json;charset=UTF-8',
                       },
                       cookies=self.cookies)

        # 请求获取一级分页id

    def start_requests(self):
        yield scrapy.Request(
            method='POST',
            url=self.index_url,
            meta={
                'batchNo': self.batch_no,
            },
            headers={
                'Content-Type': 'application/json',
            },
            # 电子超市
            body=json.dumps({"pid": "100015"}),
            callback=self.parse_total_page,
            errback=self.error_back,
        )

    # 处理sku列表
    def parse_total_page(self, response):

        meta = response.meta
        # 处理品类列表
        cats = parse_catalog(response)
        if cats:
            self.logger.info('品类: count[%s]' % len(cats))
            yield Box('catalog', self.batch_no, cats)
            for cat in cats:
                if cat.get('level') == 3:
                    # 采集sku列表
                    catalog3Id = cat.get('catalogId')
                    catalog3Name = cat.get('catalogName')
                    catalog2Id = cat.get('parentId')
                    catalog2Name = [i.get('catalogName') for i in cats if i.get('catalogId') == cat.get('parentId')][0]
                    catalog1Id = [i.get('parentId') for i in cats if i.get('catalogId') == catalog2Id][0]
                    catalog1Name = [i.get('catalogName') for i in cats if i.get('catalogId') == catalog1Id][0]
                    if self.SKU1_WHITE_LIST:
                        if catalog1Name in self.SKU1_WHITE_LIST:
                            for idx in range(1, len(self.PRICE_LADDER)):
                                start_price = self.PRICE_LADDER[idx - 1]
                                # 冗余
                                end_price = self.PRICE_LADDER[idx] - 1
                                yield self._build_list_req(1, catalog1Name, catalog1Id, catalog2Name, catalog2Id,
                                                           catalog3Name,
                                                           catalog3Id, start_price, end_price)
                    else:
                        for idx in range(1, len(self.PRICE_LADDER)):
                            start_price = self.PRICE_LADDER[idx - 1]
                            # 冗余
                            end_price = self.PRICE_LADDER[idx] - 1
                            yield self._build_list_req(1, catalog1Name, catalog1Id, catalog2Name, catalog2Id,
                                                       catalog3Name,
                                                       catalog3Id, start_price, end_price)

    # 处理sku列表
    def parse_sku_content_deal(self, response):
        meta = response.meta
        catalog1Name = meta.get('catalog1Name')
        catalog1Id = meta.get('catalog1Id')
        catalog2Name = meta.get('catalog2Name')
        catalog2Id = meta.get('catalog2Id')
        catalog3Name = meta.get('catalog3Name')
        catalog3Id = meta.get('catalog3Id')
        cur_page = meta.get('page')
        leftPrice = meta.get('leftPrice')
        rightPrice = meta.get('rightPrice')
        # 处理sku列表
        sku_list = parse_sku(response)
        self.logger.info("清单: cat=%s, page=%s" % (catalog3Id, cur_page))
        if sku_list:
            yield Box('sku', self.batch_no, sku_list)
            # 发起分页
            if cur_page == 1:
                # 解析页面
                total_pages = parse_total_page(response, self.page_size)
                self.logger.info("清单1: cat=%s, total_page=%s" % (catalog3Id, total_pages))
                if total_pages == self.max_page_limit:
                    self.logger.info('超限:cat=%s,total_pages=%s' % (catalog3Id, total_pages))
                    new_price_range = split_price(leftPrice, rightPrice)
                    if new_price_range.__len__() == 2:
                        for fs in new_price_range:
                            self.logger.info("清单1[价格区间]: cat=%s, total_page=%s, leftPrice=%s, rightPrice=%s" % (
                                catalog3Id, total_pages, fs[0], fs[1]))
                            yield self._build_list_req(1, catalog1Name, catalog1Id, catalog2Name, catalog2Id,
                                                       catalog3Name,
                                                       catalog3Id, fs[0], fs[1])
                    else:
                        self.logger.info(
                            '清单1[价格分割已达限] supplier=%s leftPrice=%s rightPrice=%s' % (
                                catalog3Id, leftPrice, rightPrice))
                else:
                    for page in range(2, total_pages + 1):
                        yield self._build_list_req(page, catalog1Name, catalog1Id, catalog2Name, catalog2Id,
                                                   catalog3Name,
                                                   catalog3Id, leftPrice, rightPrice)
        else:
            self.logger.info('空页: cat=%s, page=%s' % (catalog3Id, cur_page))
