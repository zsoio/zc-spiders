# -*- coding: utf-8 -*-
import random
import scrapy
from scrapy import Request
from scrapy.utils.project import get_project_settings
from scrapy.exceptions import IgnoreRequest
from zc_core.util.http_util import retry_request
from iccec.rules import *
from zc_core.dao.sku_dao import SkuDao
from zc_core.dao.sku_pool_dao import SkuPoolDao
from zc_core.dao.batch_dao import BatchDao
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.done_filter import DoneFilter
from datetime import datetime
from scrapy.utils.project import get_project_settings
from iccec.utils.login import SeleniumLogin
from zc_core.spiders.base import BaseSpider


class FullSpider(BaseSpider):
    name = 'full'

    # 商品详情
    item_url = 'https://emi.iccec.cn/apis/emi/cc/users/signup/querySkuPrice'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(FullSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)
        # 避免重复采集
        self.done_filter = DoneFilter(self.batch_no)
        # self.cookies = SeleniumLogin().get_cookies()

    def _build_list_req(self, sku_id, catalog1Name, catalog1Id, catalog2Name, catalog2Id, catalog3Name, catalog3Id,
                        supplierName, supplierId, skuImg, originPrice, salePrice, barCode, spuId):
        return Request(url=self.item_url,
                       method='POST',
                       meta={
                           'reqType': 'item',
                           'batchNo': self.batch_no,
                           'skuId': sku_id,
                           'catalog1Name': catalog1Name,
                           'catalog1Id': catalog1Id,
                           'catalog2Name': catalog2Name,
                           'catalog2Id': catalog2Id,
                           'catalog3Name': catalog3Name,
                           'catalog3Id': catalog3Id,
                           'supplierName': supplierName,
                           'supplierId': supplierId,
                           'skuImg': skuImg,
                           'originPrice': originPrice,
                           'salePrice': salePrice,
                           'barCode': barCode,
                           'spuId': spuId
                       },
                       dont_filter=True,
                       headers={
                           'Content-Type': 'application/json;charset=UTF-8'
                       },
                       body=json.dumps({"skuId": sku_id, "indexType": 0, "agentId": 100019}),
                       callback=self.parse_item_data,
                       errback=self.error_back,
                       # cookies=self.cookies
                       )

    def start_requests(self):

        settings = get_project_settings()
        while_list = settings.get("CATALOG_WHITE_LIST")
        if while_list:
            pool_list = SkuDao().get_batch_sku_list(self.batch_no, fields={"_id": 1, "catalog1Name": 1, "catalog1Id": 1,
                                                                           "catalog2Name": 1,
                                                                           "catalog2Id": 1, "catalog3Name": 1,
                                                                           "catalog3Id": 1,
                                                                           "offlineTime": 1, 'supplierId': 1,
                                                                           'supplierName': 1, 'skuImg': 1,
                                                                           'originPrice': 1, 'salePrice': 1,
                                                                           'barCode': 1, 'spuId': 1},
                                                    query={"$or": while_list})
        else:
            pool_list = SkuDao().get_batch_sku_list(self.batch_no, fields={"_id": 1, "catalog1Name": 1, "catalog1Id": 1,
                                                                           "catalog2Name": 1,
                                                                           "catalog2Id": 1, "catalog3Name": 1,
                                                                           "catalog3Id": 1,
                                                                           "offlineTime": 1, 'supplierId': 1,
                                                                           'supplierName': 1, 'skuImg': 1,
                                                                           'originPrice': 1, 'salePrice': 1,
                                                                           'barCode': 1, 'spuId': 1})
        self.logger.info('全量：%s' % (len(pool_list)))
        # pool_list = pool_list[::-1]
        # random.shuffle(pool_list)
        for sku in pool_list:
            sku_id = sku.get('_id')
            catalog1Name = sku.get('catalog1Name')
            catalog1Id = sku.get('catalog1Id')
            catalog2Name = sku.get('catalog2Name')
            catalog2Id = sku.get('catalog2Id')
            catalog3Name = sku.get('catalog3Name')
            catalog3Id = sku.get('catalog3Id')
            supplierId = sku.get('supplierId')
            supplierName = sku.get('supplierName')
            originPrice = sku.get('originPrice')
            salePrice = sku.get('salePrice')
            barCode = sku.get('barCode')
            skuImg = sku.get('skuImg')
            spuId = sku.get('spuId')
            offline_time = sku.get('offlineTime', 0)
            if offline_time > settings.get('MAX_OFFLINE_TIME', 2):
                self.logger.info('忽略: [%s][%s]', sku_id, offline_time)
                continue
            # 避免重复采集
            if self.done_filter.contains(sku_id) and not settings.get('FORCE_RECOVER', False):
                self.logger.info('已采: [%s]', sku_id)
                continue
            # 采集商品
            yield self._build_list_req(sku_id, catalog1Name, catalog1Id, catalog2Name, catalog2Id, catalog3Name,
                                       catalog3Id, supplierName, supplierId, skuImg, originPrice, salePrice, barCode,
                                       spuId)

    # 处理ItemData
    def parse_item_data(self, response):
        meta = response.meta
        sku_id = meta.get('skuId')
        data = parse_item_data(response)
        self.logger.info('商品: [%s]' % data.get('skuId'))
        yield data
