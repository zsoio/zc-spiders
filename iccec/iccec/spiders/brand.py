# -*- coding: utf-8 -*-
import random
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from datetime import datetime
from zc_core.dao.batch_dao import BatchDao
from zc_core.dao.item_data_dao import ItemDataDao
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from iccec.rules import parse_brand
from zc_core.util.done_filter import DoneFilter
import json
from zc_core.spiders.base import BaseSpider


class priceSpider(BaseSpider):
    name = 'brand'
    custom_settings = {
        'CONCURRENT_REQUESTS': 32,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 32,
        'CONCURRENT_REQUESTS_PER_IP': 32,
    }
    # 获取品牌，商品编号链接
    brand_url = 'https://emi.iccec.cn/apis/emi/cc/users/signup/querySkuDetail'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(priceSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)
        # 避免重复采集
        self.done_filter = DoneFilter(self.batch_no)

    def start_requests(self):

        item_list = ItemDataDao().get_batch_data_list(self.batch_no)
        self.logger.info('全量：%s' % (len(item_list)))
        item_list = ItemDataDao().get_batch_data_list(self.batch_no, query={
            "$and": [{"brandName": {"$exists": False}}, {"supplierSkuId": {"$exists": False}}]})
        self.logger.info('目标：%s' % (len(item_list)))
        random.shuffle(item_list)
        for item in item_list:
            sku_id = item.get('_id')
            # 采集销售额
            yield Request(
                method='POST',
                url=self.brand_url,
                meta={
                    'reqType': 'brand',
                    'batchNo': self.batch_no,
                    'skuId': sku_id,
                },
                body=json.dumps({"skuId": sku_id, "indexType": 0, "agentId": 100019}),
                callback=self.parse_sales,
                errback=self.error_back,
                priority=25,
                headers={
                    'Content-Type': 'application/json;charset=UTF-8'
                }
            )

    # 处理ItemData
    def parse_sales(self, response):
        meta = response.meta
        sku_id = meta.get('skuId')
        data = parse_brand(response)
        if data:
            self.logger.info('属性信息: sku=%s, supplierSkuId=%s,brandName=%s' % (
                sku_id, data.get('supplierSkuId'), data.get('brandName')))
            yield data
        else:
            self.logger.error('价格异常: [%s]' % sku_id)
