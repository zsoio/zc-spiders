# -*- coding: utf-8 -*-
import random
import scrapy
from iccec.rules import *
from zc_core.dao.sku_dao import SkuDao
from zc_core.dao.batch_dao import BatchDao
from scrapy.utils.project import get_project_settings
from zc_core.spiders.base import BaseSpider
from zc_core.dao.item_data_dao import ItemDataDao
from zc_core.dao.item_pool_dao import ItemPoolDao
from pymongo import UpdateOne
from zc_core.client.mongo_client import Mongo


class FullSpider(BaseSpider):
    name = 'full'

    # 三级分类接口
    catalog_url = 'https://emi.iccec.cn/apis/emi/cc/users/signup/querySkuNavigation'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(FullSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)

    def start_requests(self):
        mongo = Mongo()
        pool_list = ItemPoolDao().get_item_pool_list(query={"catalog1Id": {"$exists": True}},
                                                     filed={"_id": 1, "catalog1Name": 1, "catalog1Id": 1,
                                                            "catalog2Name": 1, "catalog2Id": 1, "catalog3Name": 1,
                                                            "catalog3Id": 1})
        update_bulk = list()

        for item in pool_list:
            update_bulk.append(UpdateOne({'_id': item['_id']}, {'$set': item}, upsert=False))
        self.logger.info("补分类长度：{}".format(len(update_bulk)))
        mongo.bulk_write(f'data_{self.batch_no}', update_bulk)
        mongo.close()
        data_list = ItemDataDao().get_batch_data_list(self.batch_no,
                                                      query={"$or": [{"catalog1Id": {"$exists": False}}]},
                                                      fields={"batchNo": 1, "_id": 1})
        settings = get_project_settings()
        self.logger.info('全量：%s' % (len(data_list)))
        for sku in pool_list:
            sku_id = sku.get('_id')
            # 采集商品
            yield scrapy.Request(
                method='POST',
                url=self.catalog_url,
                body=json.dumps({"skuId": sku_id, "agentId": 100019}),
                dont_filter=True,
                headers={
                    'Content-Type': 'application/json;charset=UTF-8'
                },
                callback=self.parse_item_data,
                errback=self.error_back
            )

    # 处理ItemData
    def parse_item_data(self, response):
        data = parse_catalog_data(response)
        self.logger.info('补分类: [%s]' % data.get('skuId'))
        yield data
