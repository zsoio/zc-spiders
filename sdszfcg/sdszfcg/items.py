# encoding:utf-8
import scrapy
from scrapy.exceptions import DropItem
from zc_core.model.items import Supplier


# 供应商元数据模型
class sdszfcgSupplier(Supplier):
    supplierLink = scrapy.Field()

    def validate(self):
        if not self.get('batchNo'):
            raise DropItem("Supplier Error [batchNo]")
        if not self.get('_id') and not self.get('id'):
            raise DropItem("Supplier Error [id]")
        return True
