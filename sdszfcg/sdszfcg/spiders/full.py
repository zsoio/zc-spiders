# # -*- coding: utf-8 -*-
# from scrapy import Request
# from scrapy.exceptions import IgnoreRequest
# from scrapy.utils.project import get_project_settings
# from zc_core.dao.sku_pool_dao import SkuPoolDao
# from zc_core.util.batch_gen import time_to_batch_no
# from zc_core.util.http_util import retry_request
# from zc_core.dao.batch_dao import BatchDao
# from zc_core.util.done_filter import DoneFilter
#
# from sdszfcg.rules import *
#
#
# class FullSpider(BaseSpider):
#     name = "full"
#     # 详情页url
#     item_url = "http://ggzyjyzx.shandong.gov.cn/wssc/goods/{}"
#
#     def __init__(self, batchNo=None, *args, **kwargs):
#         super(FullSpider, self).__init__(*args, **kwargs)
#         if not batchNo:
#             self.batch_no = time_to_batch_no(datetime.now())
#         else:
#             self.batch_no = int(batchNo)
#         # 创建批次记录
#         BatchDao().create_batch(self.batch_no)
#         # 避免重复采集
#         self.done_filter = DoneFilter(self.batch_no)
#
#     def start_requests(self):
#         settings = get_project_settings()
#         sku_list = SkuPoolDao().get_sku_pool_list()
#         self.logger.info('全量: %s' % (len(sku_list)))
#         for sku in sku_list:
#             sku_id = sku.get("_id")
#             # 避免无效采集
#             offline_time = sku.get('offlineTime', 0)
#             if offline_time > settings.get('MAX_OFFLINE_TIME', 2):
#                 self.logger.info('忽略: [%s][%s]', sku_id, offline_time)
#                 continue
#             # 避免重复采集
#             if self.done_filter.contains(sku_id) and not settings.get('FORCE_RECOVER', False):
#                 self.logger.info('已采: %s', sku_id)
#                 continue
#
#             yield Request(
#                 url=self.item_url.format(sku_id),
#                 meta={
#                     'reqType': 'full',
#                     'batchNo': self.batch_no,
#                     "skuId": sku_id
#                 },
#                 callback=self.parse_item_data,
#                 errback=self.error_back
#             )
#
#     def parse_item_data(self, response):
#         if response.text:
#             meta = response.meta
#             sku_id = meta.get("skuId")
#             item = parse_item_data(response)
#             if item:
#                 self.logger.info('商品: [%s]' % sku_id)
#                 yield item
#             else:
#                 self.logger.error('下架: sku=%s' % sku_id)
#
#     def error_back(self, e):
#         if e.type and e.type == IgnoreRequest:
#             self.logger.info(e.value)
#         else:
#             if e.request:
#                 self.logger.error('请求异常: [%s][%s] -> [%s]' % (str(type(e)), e.request.url, e.request.meta))
#                 yield retry_request(e.request)
#             else:
#                 self.logger.error('未知异常: %s' % e)
