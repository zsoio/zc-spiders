import re
import math
from scrapy import Request
from sdszfcg.rules import *
from scrapy.utils.project import get_project_settings
from scrapy.exceptions import IgnoreRequest
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from zc_core.spiders.base import BaseSpider


class SkuSpider(BaseSpider):
    name = 'supplier'

    # 供应商接口
    supplier_url = 'http://ggzyjyzx.shandong.gov.cn/wssc/index/shortlistedSuppliers?type=1&page={}&rows={}&name=&randomNum=0.4985492814524626&regDetailAddress='

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        settings = get_project_settings()
        self.ladders = settings.get('PRICE_LADDER', [])
        self.rows = 10
        self.max_page_limit = math.ceil(10000 / self.rows)

    def start_requests(self):
        yield Request(
            url=self.supplier_url.format(1, self.rows),
            method='GET',
            meta={
                'reqType': 'catalog',
                'batchNo': self.batch_no,
                'page': 1
            },
            callback=self.parse_supplier,
            errback=self.error_back,
            headers={
                'siteCode': 'sdszfcg'
            },
            priority=100
        )

    def parse_supplier(self, response):
        meta = response.meta
        cur_page = meta.get('page')

        cats = parse_supplier(response)
        if cats and len(cats) > 0:
            yield Box('supplier', self.batch_no, cats)
            self.logger.info('供应商 page=%s' % (cur_page))
        else:
            self.logger.error('无供应商: [%s] url -> [%s]' % (self.batch_no, response.url))

        if cur_page == 1:
            pages = parse_supplier_page(response)
            for page in range(2, pages + 1):
                yield Request(
                    url=self.supplier_url.format(page, self.rows),
                    method='GET',
                    meta={
                        'reqType': 'catalog',
                        'batchNo': self.batch_no,
                        'page': page
                    },
                    callback=self.parse_supplier,
                    errback=self.error_back,
                    headers={
                        'siteCode': 'sdszfcg'
                    },
                )
