# 测试
from zc_core.client.mongo_client import Mongo
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.common import parse_time, parse_timestamp

if __name__ == '__main__':
    str1 = '2018-12-11 10:05:06'
    dt1 = parse_time(str1)
    batch_no = time_to_batch_no(dt1)
    print(dt1, batch_no)
    Mongo().insert_or_update('XXX', '_id', {'_id': 111, 'dt1': dt1, 'batch_no': batch_no})

    str1 = '2018-12-11 06:05:06'
    dt1 = parse_time(str1)
    batch_no = time_to_batch_no(dt1)
    print(dt1, batch_no)
    Mongo().insert_or_update('XXX', '_id', {'_id': 222, 'dt1': dt1, 'batch_no': batch_no})

    str1 = '1576839034000'
    dt1 = parse_timestamp(str1)
    batch_no = time_to_batch_no(dt1)
    print(dt1, batch_no)
    Mongo().insert_or_update('XXX', '_id', {'_id': 333, 'dt1': dt1, 'batch_no': batch_no})

    str1 = '1576839034'
    dt1 = parse_timestamp(str1)
    batch_no = time_to_batch_no(dt1)
    print(dt1, batch_no)
    Mongo().insert_or_update('XXX', '_id', {'_id': 444, 'dt1': dt1, 'batch_no': batch_no})

    str1 = '1576795834'
    dt1 = parse_timestamp(str1)
    batch_no = time_to_batch_no(dt1)
    print(dt1, batch_no)
    Mongo().insert_or_update('XXX', '_id', {'_id': 555, 'dt1': dt1, 'batch_no': batch_no})