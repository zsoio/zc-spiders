# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from scrapy.exceptions import IgnoreRequest
from scrapy.utils.project import get_project_settings
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request


class BaseSpider(scrapy.Spider):

    def __init__(self, batchNo=None, dayOffset=0, hourOffset=0, *args, **kwargs):
        super(BaseSpider, self).__init__(*args, **kwargs)
        self.settings = get_project_settings()
        if not batchNo:
            self.batch_no = time_to_batch_no(datetime.now(), delta_day=dayOffset, delta_hour=hourOffset)
        else:
            self.batch_no = int(batchNo)

        self.logger.info('当前批次: %s' % self.batch_no)

    # 错误处理
    def error_back(self, e):
        if e.type and e.type == IgnoreRequest:
            self.logger.info(e.value)
        else:
            if e.value and e.request and hasattr(e.value, 'response'):
                meta = e.request.meta
                self.logger.error('响应异常: [%s][%s] -> [%s]' % (meta.get('proxy', ''), e.value.response, meta))
                yield retry_request(e.request)
            elif e.request:
                self.logger.error('请求异常: [%s][%s] -> [%s]' % (str(type(e)), e.request.url, e.request.meta))
                yield retry_request(e.request)
            else:
                self.logger.error('未知异常: %s' % e)
