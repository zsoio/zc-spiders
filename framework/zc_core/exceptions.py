from scrapy.exceptions import IgnoreRequest


class RetryRequest(IgnoreRequest):
    """Indicates a decision was made not to process a request"""
