# -*- coding: utf-8 -*-
import datetime
from zc_core.util.batch_gen import batch_to_year
from scrapy.utils.project import get_project_settings
from pymongo import MongoClient


# MongoDB客户端简易封装
class Mongo(object):

    def __init__(self, mongo_uri=None, mongo_db=None, year=None, batch_no=None):
        settings = get_project_settings()
        self.mongo_uri = mongo_uri
        if not self.mongo_uri:
            self.mongo_uri = settings.get('MONGODB_URI')
        # 默认初始化：直传/指定库/当前年的库
        self.mongo_db = mongo_db
        if not self.mongo_db:
            self.mongo_db = settings.get('MONGODB_DATABASE')
        if not self.mongo_db:
            if not year and batch_no:
                year = batch_to_year(batch_no)
            elif not year and not batch_no:
                year = str(datetime.datetime.now().year)
            # 默认初始化当前年的库
            bot_name = settings.get('BOT_NAME')
            self.mongo_db = '{}_{}'.format(bot_name, year)

        self.client = MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    # 获取集合
    def get_collection(self, coll):
        return self.db.get_collection(coll)

    # 查询对象
    def get(self, collection, query={}):
        result = self.db[collection].find_one(query)
        return result

    # 统计数量
    def count(self, collection, query={}):
        return self.db[collection].count(query)

    # 查询列表
    def list(self, collection, query={}, fields=None, sort=[]):
        if fields:
            cursor = self.db[collection].find(query, fields, sort=sort)
        else:
            cursor = self.db[collection].find(query, sort=sort)

        rows = [x for x in cursor]
        return rows

    # 查询列表
    def distinct(self, collection, dist_key, query={}, fields=None):
        return self.db[collection].find(query, fields).distinct(dist_key)

    # 聚合查询
    def aggregate(self, collection, pipeline=[]):
        cursor = self.db[collection].aggregate(pipeline)
        return list(cursor)

    # 查询分页列表
    def list_with_page(self, collection, query={}, page_size=10000, fields=None):
        rows = list()
        total = self.db[collection].count(query)
        if total > 0 and page_size > 0:
            total_page = round(len(total) / page_size)
            for page in range(0, total_page):
                if fields:
                    cursor = self.db[collection].find(query, fields).skip(page_size * page).limit(page)
                else:
                    cursor = self.db[collection].find(query).skip(page_size * page).limit(page)
                curr_batch = list(cursor)
                if curr_batch:
                    rows.append(curr_batch)
        return rows

    # 插入或更新
    def insert_or_update(self, collection, id, data):
        return self.db[collection].update({id: data[id]}, {'$set': data}, upsert=True)

    # 更新
    def update(self, collection, filter, datas, multi=False):
        return self.db[collection].update(filter, datas, multi=multi)

    # 更新
    def delete(self, collection, filter):
        return self.db[collection].delete_many(filter)

    # 插入或更新
    def bulk_write(self, collection, bulk_list):
        if bulk_list:
            return self.db[collection].bulk_write(bulk_list, ordered=False, bypass_document_validation=True)

    # 关闭链接
    def close(self):
        self.client.close()

    # 销毁关闭链接
    def __del__(self):
        if self.client:
            self.client.close()
