# -*- coding: utf-8 -*-
from zc_core.model.items import ItemData, ItemPoolData, Spu
from zc_core.pipelines.helper.gov_cat_helper import GovCatHelper


class GovCatalogCompletePipeline(object):

    def __init__(self):
        self.helper = GovCatHelper()

    def process_item(self, item, spider):
        # 过滤条件
        if isinstance(item, ItemData) or isinstance(item, Spu) or isinstance(item, ItemPoolData):
            # 自动填充
            return self.helper.fill(item)

        return item

    def close_spider(self, spider):
        _ = spider
        self.helper.close()
