# -*- coding: utf-8 -*-
from zc_core.model.items import ItemData, ItemPoolData, Spu, MissItemData
from zc_core.pipelines.helper.brand_helper import BrandHelper


class BrandCompletePipeline(object):

    def __init__(self):
        self.helper = BrandHelper()

    def process_item(self, item, spider):
        # 过滤条件
        if isinstance(item, ItemData) or isinstance(item, MissItemData) or isinstance(item, Spu) or isinstance(item, ItemPoolData):
            # 自动填充
            return self.helper.fill(item)

        return item

    def close_spider(self, spider):
        _ = spider
        self.helper.close()
