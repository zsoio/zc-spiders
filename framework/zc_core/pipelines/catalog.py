# -*- coding: utf-8 -*-
from zc_core.model.items import ItemData, Spu, BidItem, ItemPoolData, MissItemData
from zc_core.pipelines.helper.catalog_helper import CatalogHelper


class CatalogCompletePipeline(object):

    def __init__(self):
        self.helper = CatalogHelper()

    def process_item(self, item, spider):
        # 过滤条件
        if isinstance(item, ItemData) or isinstance(item, MissItemData) or isinstance(item, Spu) \
                or isinstance(item, BidItem) or isinstance(item, ItemPoolData):
            # 自动填充
            return self.helper.fill(item)

        return item

    def close_spider(self, spider):
        _ = spider
        self.helper.close()
