# 图片下载
import scrapy
from scrapy.exceptions import DropItem
from scrapy.pipelines.images import ImagesPipeline
from zc_core.model.items import ImageItem


class ImagesPipeline(ImagesPipeline):

    def get_media_requests(self, item, info):
        if isinstance(item, ImageItem):
            for idx, image_url in enumerate(item['image_urls']):
                # 图片链接加入商品标识码解决图片去重问题
                yield scrapy.Request(
                    url=image_url,
                    meta={'path': item['image_paths'][idx], 'skuId': item['skuId']},
                    priority=100,
                    dont_filter=True
                )

    def file_path(self, request, response=None, info=None):
        return request.meta['path']

    def item_completed(self, results, item, info):
        if isinstance(item, ImageItem):
            image_paths = [x['path'] for ok, x in results if ok]
            if not image_paths:
                raise DropItem("Item contains no images")
            item['image_paths'] = image_paths
            info.spider.logger.info('下载图片：%s', len(image_paths))
            return item
