# -*- coding: utf-8 -*-
from zc_core.pipelines.helper.supplier_helper import SupplierHelper
from zc_core.model.items import ItemData, ItemPoolData, OrderItem, Spu, MissItemData


class SupplierCompletePipeline(object):

    def open_spider(self, spider):
        _ = spider
        self.helper = SupplierHelper()

    def process_item(self, item, spider):
        # 过滤
        if isinstance(item, ItemData) or isinstance(item, MissItemData) or isinstance(item, Spu) \
                or isinstance(item, ItemPoolData) or isinstance(item, OrderItem):
            # 自动填充
            return self.helper.fill(item)

        return item

    def close_spider(self, spider):
        _ = spider
        self.helper.close()
