# -*- coding: utf-8 -*-
from zc_core.pipelines.helper.spu_helper import SpuHelper
from zc_core.model.items import ItemGroup


class SpuFilterPipeline(object):
    def __init__(self):
        self.spu_helper = SpuHelper()

    def process_item(self, item, spider):
        if isinstance(item, ItemGroup):
            # 过滤条件,商品会同时更新所有同款的的spu
            spu_ids = item.get("skuIdList")
            if spu_ids:
                self.spu_helper.put_filter(spu_ids)

        return item
