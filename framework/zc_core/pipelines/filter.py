# -*- coding: utf-8 -*-
from pymongo.errors import DuplicateKeyError
from traceback import format_exc
from zc_core.model.items import *
from zc_core.util.done_filter import DoneFilter


class FilterPoolPipeline(object):

    def open_spider(self, spider):
        self.filter = DoneFilter(spider.batch_no)

    def process_item(self, item, spider):
        try:
            if isinstance(item, ItemData):
                _id = item.get("skuId")
                if not _id:
                    _id = item.get("_id")
                if _id:
                    self.filter.put(_id)
                return item
        except DuplicateKeyError:
            spider.logger.debug('duplicate key error collection')
        except Exception as e:
            _ = e
            spider.logger.error(format_exc())

        return item
