# -*- coding: utf-8 -*-
import threading
from zc_core.client.mongo_client import Mongo
from zc_core.model.items import ItemData, ItemPoolData, Spu, OrderItem


class SupplierHelper(object):
    _instance_lock = threading.Lock()
    _biz_inited = False

    def __new__(cls, *args, **kwargs):
        if not hasattr(SupplierHelper, "_instance"):
            with SupplierHelper._instance_lock:
                if not hasattr(SupplierHelper, "_instance"):
                    SupplierHelper._instance = object.__new__(cls)
        return SupplierHelper._instance

    def __init__(self):
        if not self._biz_inited:
            self._biz_inited = True
            self.mongo = Mongo()
            coll = self.mongo.get_collection('supplier_pool')
            self.id_dict = dict()
            self.name_dict = dict()
            self.code_dict = dict()
            if coll:
                pool = coll.find()
                if pool:
                    for supplier in pool:
                        self.id_dict[supplier.get('_id')] = supplier
                        self.name_dict[supplier.get('name')] = supplier
                        code = supplier.get('code', '')
                        if code:
                            self.code_dict[supplier.get('code')] = supplier

    def fill(self, item):
        # 商品数据、商品池数据、订单明细
        if isinstance(item, ItemData) or isinstance(item, Spu) \
                or isinstance(item, ItemPoolData) or isinstance(item, OrderItem):
            sp_id = item.get('supplierId')
            sp_name = item.get('supplierName')
            sp_code = item.get('supplierCode', '')
            if sp_id and sp_id in self.id_dict:
                sp = self.id_dict.get(sp_id)
                item['supplierName'] = sp.get('name')
                if self.code_dict and sp.get('code', ''):
                    item['supplierCode'] = sp.get('code', '')
            elif sp_name and sp_name in self.name_dict:
                sp = self.name_dict.get(sp_name)
                item['supplierId'] = sp.get('_id')
                if self.code_dict and sp.get('code', ''):
                    item['supplierCode'] = sp.get('code', '')
            elif sp_code and self.code_dict and sp_code in self.code_dict:
                sp = self.code_dict.get(item.get('supplierName'))
                item['supplierId'] = sp.get('_id')
                item['supplierName'] = sp.get('name')

        return item

    def get_id_by_name(self, name):
        if name and name in self.name_dict:
            return self.name_dict.get(name).get('_id')

    def get_name_by_id(self, id):
        if id and id in self.id_dict:
            return self.id_dict.get(id).get('name')

    # 关闭链接
    def close(self):
        self.mongo.close()
