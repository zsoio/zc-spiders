# -*- coding: utf-8 -*-
import logging
import threading

logger = logging.getLogger('SpuHelper')


class SpuHelper(object):
    _instance_lock = threading.Lock()
    _biz_inited = False

    def __new__(cls, *args, **kwargs):
        if not hasattr(SpuHelper, "_instance"):
            with SpuHelper._instance_lock:
                if not hasattr(SpuHelper, "_instance"):
                    SpuHelper._instance = object.__new__(cls)
        return SpuHelper._instance

    def __init__(self):
        if not self._biz_inited:
            self._biz_inited = True
            self.spu_filter_list = list()

    def put_filter(self, sku_list):
        self.spu_filter_list.extend(sku_list)

    def contains(self, sku_id):
        return sku_id in self.spu_filter_list
