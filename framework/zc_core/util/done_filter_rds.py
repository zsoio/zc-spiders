# -*- coding: utf-8 -*-
import logging
import threading
from scrapy.utils.project import get_project_settings
from zc_core.client.mongo_client import Mongo
from zc_core.client.redis_client import Redis

logger = logging.getLogger('rule')


class DoneFilterRDS(object):
    """
    已采集商品过滤器
    用于全量商品明细重复采集时，过滤已采过的商品，避免重复采集
    """
    _instance_lock = threading.Lock()
    _biz_inited = False

    def __new__(cls, *args, **kwargs):
        if not hasattr(DoneFilterRDS, "_instance"):
            with DoneFilterRDS._instance_lock:
                if not hasattr(DoneFilterRDS, "_instance"):
                    DoneFilterRDS._instance = object.__new__(cls)
        return DoneFilterRDS._instance

    def __init__(self, batchNo=None, coll_name=None, query={}, fields={'_id': 1}):
        if not self._biz_inited:
            self._biz_inited = True
            settings = get_project_settings()
            self.rds = Redis()
            self.rds_key = '{}:item:{}'.format(settings.get('BOT_NAME'), batchNo)
            if not batchNo and not coll_name:
                raise BaseException('批次编号和集合名称至少指定一个')
            if not coll_name:
                coll_name = 'data_{}'.format(batchNo)
            items = Mongo().list(coll_name, query=query, fields=fields)
            for it in items:
                self.rds.client.sadd(self.rds_key, it.get('_id'))
            logger.info('--> DoneFilter：%s' % (len(items)))

    def contains(self, id):
        return self.rds.client.sismember(self.rds_key, id)

    def put(self, id):
        self.rds.client.sadd(self.rds_key, id)


# 测试
if __name__ == '__main__':
    filter = DoneFilterRDS()
    print(filter.contains('137361'))
