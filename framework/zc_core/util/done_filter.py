# -*- coding: utf-8 -*-
from zc_core.client.mongo_client import Mongo


class DoneFilter(object):
    """
    已采集商品过滤器
    用于全量商品明细重复采集时，过滤已采过的商品，避免重复采集
    """

    def __init__(self, batchNo=None, coll_name=None, query={}, fields={'_id': 1}, filter_key='_id'):
        self.item_list = set()
        if not batchNo and not coll_name:
            raise BaseException('批次编号和集合名称至少指定一个')
        if not coll_name:
            coll_name = 'data_{}'.format(batchNo)
        items = Mongo().list(coll_name, query=query, fields=fields)
        for it in items:
            self.item_list.add(it.get(filter_key))

    def contains(self, id):
        return id in self.item_list

    def put(self, id):
        self.item_list.add(id)


# 测试
if __name__ == '__main__':
    filter = DoneFilter()
    print(filter.contains('137361'))
