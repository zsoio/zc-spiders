# -*- coding: utf-8 -*-
from dateutil import tz
from datetime import datetime, timedelta


class DeadlineFilter(object):
    """
    截止时间过滤器
    """

    def __init__(self, days=None, deadline=None):
        if not days and not deadline:
            raise BaseException('截止天数和截止日期至少指定一个')
        if days and not deadline:
            self.deadline = datetime.now(tz.gettz('Asia/Shanghai')) - timedelta(days=days)
        else:
            self.deadline = deadline

    def filter(self, order_time):
        if order_time < self.deadline:
            return False

        return True

    def get_deadline(self):
        return self.deadline
