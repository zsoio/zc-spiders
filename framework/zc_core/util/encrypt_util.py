# -*- coding: UTF-8 -*-
import base64
import hashlib
from uuid import uuid4
from zc_core.model.items import OrderItem

uuidChars = ("a", "b", "c", "d", "e", "f",
             "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
             "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
             "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
             "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
             "W", "X", "Y", "Z")


def short_uuid():
    uuid = str(uuid4()).replace('-', '')
    result = ''
    for i in range(0, 8):
        sub = uuid[i * 4: i * 4 + 4]
        x = int(sub, 16)
        result += uuidChars[x % 0x3E]
    return result

# 加密工具类
def md5(txt):
    if txt:
        return hashlib.md5(txt.encode("utf8")).hexdigest()

    return ''


# base64补齐并解密
def decode_base64(data, enc='utf-8'):
    if isinstance(data, str):
        data = data.encode(enc)
    missing_padding = 4 - len(data) % 4
    if missing_padding:
        data += b'=' * missing_padding

    return str(base64.decodebytes(data), encoding='utf-8')


# 两次base64编码
def double_base64_encode(s):
    if s:
        # 一次编码
        enc1 = base64.b64encode(s.encode('utf-8'))
        # 二次编码
        enc2 = base64.b64encode(enc1)
        return str(enc2, 'utf-8')
    return s


# base64解码
def base64_decode(s):
    if s:
        # 一次编码
        dec = base64.b64decode(s.encode('utf-8'))
        return str(dec, 'utf-8')
    return s


# 对order内容进行sha1编码生成订单编号（中国移动 在用）
def build_sha1_order_id(order, addition):
    if isinstance(order, OrderItem):
        # 容错
        s = '%(skuId)s,%(count)s,%(amount)s,%(orderUser)s,%(orderDept)s,%(orderTimeStr)s,%(sameOrderNo)s' % {
            'skuId': order.get('skuId'),
            'count': order.get('count'),
            'amount': order.get('amount'),
            'orderUser': order.get('orderUser', ''),
            'orderDept': order.get('orderDept'),
            'orderTimeStr': addition.get('orderTimeStr'),
            'sameOrderNo': addition.get('sameOrderNo'),
        }
        return hashlib.sha1(s.encode("utf8")).hexdigest()
    else:
        raise BaseException('仅支持Order对象')


# 测试
if __name__ == '__main__':
    # print(short_uuid())
    # print(double_base64_encode('办公用品:1'))
    # print(base64_decode('MDAwMDg1NTUzMQ=='))
    # order = {
    #         "_id": "4c20cedfc2459550c077b099a4b0fbfc469461bd",
    #         "_class": "cn.soarplus.spider.framework.model.data.Order",
    #         "batchNo": "20180523",
    #         "skuId": "138187",
    #         "supplierSkuId": "1100129097EA",
    #         "count": "10",
    #         "amount": "14780.03",
    #         "orderTime": "12/3/2018 01:13:00",
    #         "orderUser": "p******@bj.cmcc",
    #         "orderDept": "顺义分公司",
    #         "genTime": "2018-05-25 12:05:14.298"
    #     }
    # s = build_sha1_order_id(order)
    # print(s)

    # print(md5('电脑整机 大型家电'))
    print(decode_base64('NzMzNWM5Y2MyNjQyNGRmOTg2MWZkYTZkODM0ZDJhMTk'))
