import codecs


def read_rows(path):
    rows = []
    with open(path, mode='r', encoding='utf-8') as f:
        for line in f:
            rows.append(line.strip())

    return rows


def read_text(path):
    with open(path, mode='r', encoding='utf-8') as f:
        return f.read()


def write_text(path, txt, mode='a+', encoding='utf-8'):
    with codecs.open(path, mode=mode, encoding=encoding) as f:
        f.write(txt)


# # 测试
if __name__ == '__main__':
    # print(read_rows('../url_map.txt'))
    print(read_text('../url_map.txt'))
