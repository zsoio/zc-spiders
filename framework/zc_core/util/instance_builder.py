# encoding=utf-8
"""
响应校验中间件
"""


# esgcc.validator.BizValidator
def build_instance(full_name, *args, **kwargs):
    if full_name:
        arr = full_name.split('.')
        if arr:
            module_name = full_name[0:full_name.rindex('.')]
            class_name = full_name[full_name.rindex('.') + 1:]

    module_meta = __import__(module_name, globals(), locals(), [class_name])
    class_meta = getattr(module_meta, class_name)
    obj = class_meta(*args, **kwargs)
    return obj


if __name__ == '__main__':
    full_name = 'esgcc.validator.BizValidator'
    print(full_name[0:full_name.rindex('.')])
    print(full_name[full_name.rindex('.') + 1:])

    ins = build_instance(full_name)
    ins.test()
