# -*- coding: utf-8 -*-
def sku_serialize(sku):
    r = {
        'skuId': sku.get('skuId'),
        'batchNo': sku.get('batchNo'),
        'genTime': sku.get('genTime').strftime('%Y-%m-%d %H:%M:%S'),
    }
    if 'linkId' in sku:
        r['linkId'] = sku.get('linkId')
    if 'offlineTime' in sku:
        r['offlineTime'] = sku.get('offlineTime')
    if 'url' in sku:
        r['url'] = sku.get('url')
    if 'soldCount' in sku:
        r['soldCount'] = sku.get('soldCount')
    if 'catalogId' in sku:
        r['catalogId'] = sku.get('catalogId')
    if 'catalogName' in sku:
        r['catalogName'] = sku.get('catalogName')
    if 'brandId' in sku:
        r['brandId'] = sku.get('brandId')
    if 'brandName' in sku:
        r['brandName'] = sku.get('brandName')
    if 'supplierId' in sku:
        r['supplierId'] = sku.get('supplierId')
    if 'supplierName' in sku:
        r['supplierName'] = sku.get('supplierName')
    if 'meta' in sku:
        r['meta'] = sku.get('meta')

    return r
