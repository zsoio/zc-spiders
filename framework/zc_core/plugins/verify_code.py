#!/usr/bin/python
# -*- coding: utf-8 -*-
from zc_core.plugins.verify_code_api.chao_ji_ying import ChaoJiYingVerifyCode


class VerifyCode(object):
    """
    验证码识别入口类
    可切换：聚合数据、超级鹰
    """

    def __init__(self, *args, **kwargs):
        # self.api = JuHeVerifyCode()
        self.api = ChaoJiYingVerifyCode()

    def verify(self, image_file, code_type):
        """
        验证码识别统一入口方法
        :param image_file: 图片绝对路径
        :param code_type: 1004=四位英文数字；1005=五位英文数字
        :return:
        """
        return self.api.verify(image_file, code_type)
