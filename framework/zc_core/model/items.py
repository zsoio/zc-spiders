# -*- coding: utf-8 -*-
import scrapy
from scrapy.exceptions import DropItem


# 数据包装盒
class Box(scrapy.Item):
    # [必须]数据类型标签
    tag = scrapy.Field()
    # [非必须]批次编号
    batchNo = scrapy.Field()
    # [必须]实际数据对象
    data = scrapy.Field()
    # 采集时间
    genTime = scrapy.Field()

    def __init__(self, tag, batch_no, data, *args, **kwargs):
        super(Box, self).__init__(*args, **kwargs)
        if not tag:
            raise DropItem("No tag to pasted")
        if not batch_no:
            raise DropItem("No batchNo to put")
        if not data:
            raise DropItem("No data to put")

        # 放入盒子
        self['tag'] = tag
        self['batchNo'] = batch_no
        self['data'] = data

    def validate(self):
        if not self.get('tag'):
            raise DropItem("Box no [tag] -> %s" % self)
        if not self.get('batchNo'):
            raise DropItem("Box no [batchNo] -> %s" % self)
        if not self.get('data'):
            raise DropItem("Box no [data] -> %s" % self)
        return True


# 基础数据
class Batch(scrapy.Item):
    _id = scrapy.Field()
    # 批次编号
    batchNo = scrapy.Field()
    # 创建时间
    genTime = scrapy.Field()
    # 开始时间
    startTime = scrapy.Field()
    # 完成时间
    endTime = scrapy.Field()
    # 采集状态：0、未开始；1、进行中；2、已完成；3、已失败
    collectStatus = scrapy.Field()

    def validate(self):
        if not self.get('_id') and not self.get('batchNo'):
            raise DropItem("Batch Error [batchNo]")
        if not self.get('genTime'):
            raise DropItem("Batch Error [genTime]")
        return True


# 批次数据模型
class BaseData(scrapy.Item):
    # 控制信息
    cmd = scrapy.Field()
    # 批次编号
    batchNo = scrapy.Field()
    # 采集时间
    genTime = scrapy.Field()


# 供应商数据模型
class Supplier(BaseData):
    _id = scrapy.Field()
    # 编号
    id = scrapy.Field()
    # 原始编号
    code = scrapy.Field()
    # 名称
    name = scrapy.Field()
    # 名称全称
    fullName = scrapy.Field()
    score = scrapy.Field()
    sales = scrapy.Field()
    area = scrapy.Field()
    skuCount = scrapy.Field()
    hotline = scrapy.Field()
    mobile = scrapy.Field()

    def validate(self):
        if not self.get('batchNo'):
            raise DropItem("Supplier Error [batchNo]")
        if not self.get('_id') and not self.get('id'):
            raise DropItem("Supplier Error [id]")
        return True


# 品牌数据模型
class Brand(BaseData):
    _id = scrapy.Field()
    # 编号
    id = scrapy.Field()
    # 原始编号
    code = scrapy.Field()
    # 名称
    name = scrapy.Field()
    # 分类编号[可选]
    catalogId = scrapy.Field()

    def validate(self):
        if not self.get('batchNo'):
            raise DropItem("Brand Error [batchNo]")
        if not self.get('_id') and not self.get('id'):
            raise DropItem("Brand Error [id]")
        return True


# 地区数据模型
class Area(BaseData):
    _id = scrapy.Field()
    # 地区编号
    areaId = scrapy.Field()
    # 地区编码
    areaCode = scrapy.Field()
    # 地区名称
    areaName = scrapy.Field()
    # 父元素编号
    parentId = scrapy.Field()
    # 地区层级
    level = scrapy.Field()
    # 叶子节点标识（1，是；0，否）
    leafFlag = scrapy.Field()

    def validate(self):
        if not self.get('_id') and not self.get('areaId'):
            raise DropItem("Area Error [areaId]")
        if not self.get('batchNo'):
            raise DropItem("Area Error [batchNo]")
        return True


# 品类数据模型
class Catalog(BaseData):
    _id = scrapy.Field()
    # 分类编号
    catalogId = scrapy.Field()
    # 分类编码
    catalogCode = scrapy.Field()
    # 分类名称
    catalogName = scrapy.Field()
    # 父元素编号
    parentId = scrapy.Field()
    # 分类层级
    level = scrapy.Field()
    # 叶子节点标识（1，是；0，否）
    leafFlag = scrapy.Field()
    # 是否可链接（1，可连接；0，不可连接）
    linkable = scrapy.Field()

    def validate(self):
        if not self.get('_id') and not self.get('catalogId'):
            raise DropItem("Catalog Error [catalogId]")
        if not self.get('batchNo'):
            raise DropItem("Catalog Error [batchNo]")
        return True


# 分类下商品数量模型
class CatSkuCount(BaseData):
    _id = scrapy.Field()
    # 分类编号
    catalogId = scrapy.Field()
    # 总页数
    totalPage = scrapy.Field()
    # 每页条数
    pageSize = scrapy.Field()
    # 商品总量
    totalCount = scrapy.Field()

    def validate(self):
        if not self.get('batchNo'):
            raise DropItem("CatSkuCount Error [batchNo]")
        if not self.get('_id') and not self.get('catalogId'):
            raise DropItem("CatSkuCount Error [catalogId]")
        return True


# 商品属性源对象（用于其他对象继承）
class MetaFields(BaseData):
    _id = scrapy.Field()
    # 商品唯一编号
    skuId = scrapy.Field()
    # 商品编码（额外字段）
    skuCode = scrapy.Field()
    # 商品唯一编号(用于链接)
    linkId = scrapy.Field()
    # 链接
    url = scrapy.Field()
    # 产品单元编号(同款标识)
    spuId = scrapy.Field()
    # 平台物料编码
    materialCode = scrapy.Field()
    # 平台商品名称（标题）
    skuName = scrapy.Field()
    # 平台商品主图（主图地址）
    skuImg = scrapy.Field()
    # 一级分类编号
    catalog1Id = scrapy.Field()
    # 一级分类编号(非必填 可不唯一)
    catalog1Code = scrapy.Field()
    # 一级分类名称
    catalog1Name = scrapy.Field()
    # 二级分类编号
    catalog2Id = scrapy.Field()
    # 二级分类编号(非必填 可不唯一)
    catalog2Code = scrapy.Field()
    # 二级分类名称
    catalog2Name = scrapy.Field()
    # 三级分类编号
    catalog3Id = scrapy.Field()
    # 三级分类编号(非必填 可不唯一)
    catalog3Code = scrapy.Field()
    # 三级分类名称
    catalog3Name = scrapy.Field()
    # 四级分类编号
    catalog4Id = scrapy.Field()
    # 四级分类名称
    catalog4Name = scrapy.Field()
    # 后台分类编号
    backCatId = scrapy.Field()
    # 后台分类名称
    backCatName = scrapy.Field()
    # 政府采购品目编码
    govCatId = scrapy.Field()
    # 政府采购品目名称
    govCatName = scrapy.Field()
    # 实际销售价格
    salePrice = scrapy.Field()
    # 商品原价（商城原价、市场价格）
    originPrice = scrapy.Field()
    # 商品成本价
    costPrice = scrapy.Field()
    # 限价价格
    limitPrice = scrapy.Field()
    # 最小起订量
    minBuy = scrapy.Field()
    # 出货日
    deliveryDay = scrapy.Field()
    # 包装单位
    unit = scrapy.Field()
    # 店铺编号
    shopId = scrapy.Field()
    # 店铺编号(常用于数据采集)
    shopCode = scrapy.Field()
    # 店铺名称
    shopName = scrapy.Field()
    # 供应商编号
    supplierId = scrapy.Field()
    # 供应商编号(常用于数据采集)
    supplierCode = scrapy.Field()
    # 供应商名称
    supplierName = scrapy.Field()
    # 供应商商品编码
    supplierSkuId = scrapy.Field()
    # 供应商商品编号（无后缀）
    supplierSkuCode = scrapy.Field()
    # 供应商商品链接
    supplierSkuLink = scrapy.Field()
    # 品牌编号
    brandId = scrapy.Field()
    # 品牌编号(常用于数据采集)
    brandCode = scrapy.Field()
    # 品牌名称
    brandName = scrapy.Field()
    # 品牌英文名称
    brandEnName = scrapy.Field()
    # 品牌型号
    brandModel = scrapy.Field()
    # 条形码
    barCode = scrapy.Field()
    # 累计销售量
    soldCount = scrapy.Field()
    # 累计销售总额
    soldAmount = scrapy.Field()
    # 上架时间
    onSaleTime = scrapy.Field()
    # 库存
    stock = scrapy.Field()
    # 主图列表
    mainImgs = scrapy.Field()
    # 详图列表
    detailImgs = scrapy.Field()
    # 资质列表
    certs = scrapy.Field()
    # 规格参数
    attrs = scrapy.Field()
    # 同款产品列表（不存储）
    groupList = scrapy.Field()
    # 销售状态
    saleStatus = scrapy.Field()
    # 外部比价链接
    extLinks = scrapy.Field()
    # 同款数量
    sameCount = scrapy.Field()

    # 标记下架次数
    offlineTime = scrapy.Field()
    # 数据来源类型
    srcType = scrapy.Field()
    # 原始数据
    srcContent = scrapy.Field()
    # 额外属性
    extension = scrapy.Field()
    # 标签
    tag = scrapy.Field()


# SPU元数据模型
class Spu(MetaFields):
    def validate(self):
        if not self.get('_id') and not self.get('spuId'):
            raise DropItem("Spu Error [spuId]")
        if not self.get('batchNo'):
            raise DropItem("Spu Error [batchNo]")
        return True


# 商品元数据模型
class Sku(MetaFields):
    def validate(self):
        if not self.get('_id') and not self.get('skuId'):
            raise DropItem("Sku Error [skuId]")
        if not self.get('batchNo'):
            raise DropItem("Sku Error [batchNo]")
        return True


# 商品数据模型
class ItemData(MetaFields):
    def validate(self):
        if not self.get('_id') and not self.get('skuId'):
            raise DropItem("ItemData Error [skuId]")
        if not self.get('batchNo'):
            raise DropItem("ItemData Error [batchNo]")
        if not self.get('skuName'):
            raise DropItem("ItemData Error [skuName] -> (%s)" % self.get('skuId'))
        if self.get('salePrice') is None:
            raise DropItem("ItemData Error [salePrice] -> (%s)" % self.get('skuId'))
        if self.get('originPrice') is None:
            raise DropItem("ItemData Error [originPrice] -> (%s)" % self.get('skuId'))
        if self.get('salePrice') <= 0:
            raise DropItem("ItemData Error [salePrice <= 0] -> (%s)" % self.get('skuId'))
        if self.get('originPrice') <= 0:
            raise DropItem("ItemData Error [originPrice <= 0] -> (%s)" % self.get('skuId'))
        return True


# 经销商商品数据模型
class DealerItemData(MetaFields):
    def validate(self):
        if not self.get('_id') and not self.get('skuId'):
            raise DropItem("ItemData Error [skuId]")
        if self.get('salePrice') is None:
            raise DropItem("ItemData Error [salePrice] -> (%s)" % self.get('skuId'))
        if self.get('originPrice') is None:
            raise DropItem("ItemData Error [originPrice] -> (%s)" % self.get('skuId'))
        if self.get('salePrice') <= 0:
            raise DropItem("ItemData Error [salePrice <= 0] -> (%s)" % self.get('skuId'))
        if self.get('originPrice') <= 0:
            raise DropItem("ItemData Error [originPrice <= 0] -> (%s)" % self.get('skuId'))
        if self.get('spuId') is None:
            raise DropItem("ItemData Error [spuId]")
        if not self.get('batchNo'):
            raise DropItem("ItemData Error [batchNo]")
        return True


# 缺失商品数据模型
class MissItemData(MetaFields):
    def validate(self):
        if not self.get('_id') and not self.get('skuId'):
            raise DropItem("ItemData Error [skuId]")
        if not self.get('batchNo'):
            raise DropItem("ItemData Error [batchNo]")
        return True


# 缺失商品数据模型
class MissSku(Sku):
    def validate(self):
        if not self.get('_id') and not self.get('skuId'):
            raise DropItem("ItemData Error [skuId]")
        if not self.get('batchNo'):
            raise DropItem("ItemData Error [batchNo]")
        return True


# SPU数据模型[用于对商品详情的补充]
class SpuSupply(MetaFields):
    def validate(self):
        if not self.get('_id') and not self.get('spuId'):
            raise DropItem("SpuSupply Error [spuId]")
        if not self.get('batchNo'):
            raise DropItem("SpuSupply Error [batchNo]")
        return True


# 商品数据模型[用于对商品详情的补充]
class ItemDataSupply(MetaFields):
    def validate(self):
        if not self.get('_id') and not self.get('skuId'):
            raise DropItem("ItemDataSupply Error [skuId]")
        if not self.get('batchNo'):
            raise DropItem("ItemDataSupply Error [batchNo]")
        return True


# 商品数据模型（仅用于商品池）
class ItemPoolData(MetaFields):
    # 数据来源
    channel = scrapy.Field()

    def validate(self):
        if not self.get('_id') and not self.get('skuId'):
            raise DropItem("ItemData Error [skuId]")
        if not self.get('batchNo'):
            raise DropItem("ItemData Error [batchNo]")
        if not self.get('skuName'):
            raise DropItem("ItemData Error [skuName] -> (%s)" % self.get('skuId'))
        if self.get('salePrice') is None:
            raise DropItem("ItemData Error [salePrice] -> (%s)" % self.get('skuId'))
        if self.get('originPrice') is None:
            raise DropItem("ItemData Error [originPrice] -> (%s)" % self.get('skuId'))
        if self.get('salePrice') <= 0:
            raise DropItem("ItemData Error [salePrice <= 0] -> (%s)" % self.get('skuId'))
        if self.get('originPrice') <= 0:
            raise DropItem("ItemData Error [originPrice <= 0] -> (%s)" % self.get('skuId'))
        return True


# 商品虚拟组模型
class ItemGroup(BaseData):
    # 商品唯一编号
    skuId = scrapy.Field()
    # 产品单元编号(同款标识)
    spuId = scrapy.Field()
    # 同款商品编号列表(包含本商品)
    skuIdList = scrapy.Field()

    def validate(self):
        if not self.get('batchNo'):
            raise DropItem("ItemGroup Error [batchNo]")
        if not self.get('spuId'):
            raise DropItem("ItemGroup Error [spuId]")
        if not self.get('skuIdList') or not len(self.get('skuIdList')):
            raise DropItem("ItemGroup Error [skuIdList]")
        return True


# 主订单列表数据模型
class Order(BaseData):
    _id = scrapy.Field()
    # 订单编号
    id = scrapy.Field()
    # 专区内部主订单编号
    orderCode = scrapy.Field()
    # 明细页链接
    url = scrapy.Field()
    # 订单名称
    title = scrapy.Field()
    # 订单总价
    totalPrice = scrapy.Field()
    # 订单商品总数量
    totalCount = scrapy.Field()
    # 供应商编号
    supplierId = scrapy.Field()
    # 供应商名称
    supplierName = scrapy.Field()
    # 分类编号
    catalogId = scrapy.Field()
    # 分类名称
    catalogName = scrapy.Field()
    # 采购单位编号
    deptId = scrapy.Field()
    # 采购单位
    orderDept = scrapy.Field()
    # 采购人
    orderUser = scrapy.Field()
    # 订单收货地址
    orderAddr = scrapy.Field()
    # 订单收货人电话
    orderTel = scrapy.Field()
    # 采购时间[本地时间]
    orderTime = scrapy.Field()
    # 交易类型
    tradeType = scrapy.Field()
    # 订单来源
    clientType = scrapy.Field()
    # 交易状态
    status = scrapy.Field()

    def validate(self):
        # if not self.get('batchNo'):
        #     raise DropItem("Order Error [batchNo]")
        if not self.get('_id') and not self.get('id'):
            raise DropItem("Order Error [id]")
        return True


# 主订单列表数据模型
class OrderStatus(BaseData):
    _id = scrapy.Field()
    # 订单编号
    id = scrapy.Field()
    # 采购时间[本地时间]
    orderTime = scrapy.Field()
    # 是否已采集明细
    workStatus = scrapy.Field()

    def validate(self):
        # if not self.get('batchNo'):
        #     raise DropItem("OrderStatus Error [batchNo]")
        if not self.get('_id') and not self.get('id'):
            raise DropItem("OrderStatus Error [id]")
        return True


# 订单数据模型
class OrderItem(BaseData):
    _id = scrapy.Field()
    # 订单明细编号
    id = scrapy.Field()
    # 主订单编号
    orderId = scrapy.Field()
    # 专区内部主订单编号
    orderCode = scrapy.Field()
    # 交易类型
    tradeType = scrapy.Field()
    # 商品编码
    skuId = scrapy.Field()
    # 数量
    count = scrapy.Field()
    # 金额
    amount = scrapy.Field()
    # 采购单位编号
    deptId = scrapy.Field()
    # 采购单位
    orderDept = scrapy.Field()
    # 采购人
    orderUser = scrapy.Field()
    # 采购人
    userAcct = scrapy.Field()
    # 订单收货地址
    orderAddr = scrapy.Field()
    # 订单收货人电话
    orderTel = scrapy.Field()
    # 采购时间[本地时间]
    orderTime = scrapy.Field()
    # 供应商编号
    supplierId = scrapy.Field()
    # 供应商名称
    supplierCode = scrapy.Field()
    # 供应商名称
    supplierName = scrapy.Field()

    def validate(self):
        if not self.get('_id') and not self.get('id'):
            raise DropItem("OrderItem Error [id]")
        if not self.get('orderId'):
            raise DropItem("OrderItem Error [orderId]")
        if not self.get('skuId'):
            raise DropItem("OrderItem Error [skuId] -> (%s)" % self.get('orderId'))
        if self.get('count') is None:
            raise DropItem("OrderItem Error [count] -> (%s)" % self.get('count'))
        if self.get('amount') is None:
            raise DropItem("OrderItem Error [amount] -> (%s)" % self.get('amount'))
        if not self.get('orderDept'):
            raise DropItem("OrderItem Error [orderDept] -> (%s)" % self.get('orderDept'))
        if not self.get('orderTime'):
            raise DropItem("OrderItem Error [orderTime] -> (%s)" % self.get('orderId'))
        # if not self.get('batchNo'):
        #     raise DropItem("OrderItem Error [batchNo]")
        return True

    def equals(self, other):
        if self.get('skuId') == other.get('skuId') \
                and self.get('count') == other.get('count') \
                and self.get('amount') == other.get('amount') \
                and self.get('orderUser') == other.get('orderUser') \
                and self.get('orderDept') == other.get('orderDept') \
                and self.get('orderTime') == other.get('orderTime') \
                and self.get('supplierName') == other.get('supplierName'):
            return True
        return False


# 订单数据模型
class OrderItemLog(BaseData):
    _id = scrapy.Field()
    # 订单明细编号
    skuId = scrapy.Field()
    # 分页
    page = scrapy.Field()
    # 明细条数
    count = scrapy.Field()
    # 批次编号
    batchNo = scrapy.Field()

    def validate(self):
        if not self.get('skuId'):
            raise DropItem("OrderWorkFilter Error [skuId] -> (%s)" % self.get('skuId'))
        if self.get('page') is None:
            raise DropItem("OrderWorkFilter Error [page] -> (%s)" % self.get('page'))
        return True


# 图片请求对象
class ImageItem(scrapy.Item):
    skuId = scrapy.Field()
    image_urls = scrapy.Field()
    image_paths = scrapy.Field()


# 竞价项目
class Bid(scrapy.Item):
    """
    竞价项目
    """
    # 主键
    _id = scrapy.Field()
    # 项目编号
    projectId = scrapy.Field()
    # link编号
    linkId = scrapy.Field()
    # 项目名称
    projectName = scrapy.Field()
    # 竞价开始时间
    startTime = scrapy.Field()
    # 联系电话
    mobile = scrapy.Field()
    # 收货地址
    address = scrapy.Field()
    # 省份
    province = scrapy.Field()
    # 地市
    city = scrapy.Field()
    # 区县
    district = scrapy.Field()
    # 中标供应商
    supplierName = scrapy.Field()
    # 中标价格
    totalPrice = scrapy.Field()
    # 状态
    status = scrapy.Field()
    # 采集时间
    genTime = scrapy.Field()

    # 批次编号
    batchNo = scrapy.Field()

    def validate(self):
        if not self.get('_id') and not self.get('projectId'):
            raise DropItem("Bid Error [projectId]")
        if not self.get('batchNo'):
            raise DropItem("Bid Error [batchNo]")
        return True


# 竞价项目明细
class BidItem(scrapy.Item):
    """
    竞价项目明细
    """
    # 主键
    _id = scrapy.Field()
    # 明细编号
    itemId = scrapy.Field()
    # 项目编号
    projectId = scrapy.Field()
    # 条目名称
    itemName = scrapy.Field()
    # 条目品牌
    itemBrand = scrapy.Field()
    # 条目规格型号
    itemModel = scrapy.Field()
    # 条目单位
    itemUnit = scrapy.Field()
    # 条目数量
    itemAmount = scrapy.Field()
    # 条目单价
    itemPrice = scrapy.Field()
    # 一级分类编号
    catalog1Id = scrapy.Field()
    # 一级分类名称
    catalog1Name = scrapy.Field()
    # 二级分类编号
    catalog2Id = scrapy.Field()
    # 二级分类名称
    catalog2Name = scrapy.Field()
    # 备注
    note = scrapy.Field()
    # 竞价开始时间
    startTime = scrapy.Field()

    # 批次编号
    batchNo = scrapy.Field()

    def validate(self):
        if not self.get('_id') and not self.get('itemId'):
            raise DropItem("BidItem Error [itemId]")
        if not self.get('batchNo'):
            raise DropItem("BidItem Error [batchNo]")
        return True


# 询价项目
class Inquiry(scrapy.Item):
    """
    询价项目
    """
    # 主键
    _id = scrapy.Field()
    # 项目编号
    projectCode = scrapy.Field()
    # 项目编号
    projectId = scrapy.Field()
    # link编号
    linkId = scrapy.Field()
    # 项目名称
    projectName = scrapy.Field()
    # 采购员名称
    buyerName = scrapy.Field()
    # 联系电话
    buyerPhone = scrapy.Field()
    # 采购组织
    buyerOrg = scrapy.Field()
    # 采购部门
    buyerDept = scrapy.Field()
    # 币种
    currency = scrapy.Field()
    # 税项
    taxType = scrapy.Field()
    # 税率
    taxRate = scrapy.Field()
    # 询价发布时间
    publishTime = scrapy.Field()
    # 报价有效期
    validityDate = scrapy.Field()
    # 报价截止时间
    deadlineTime = scrapy.Field()
    # 收货地址
    address = scrapy.Field()
    # 是否含运费
    carriageIn = scrapy.Field()
    # 备注
    notes = scrapy.Field()
    # 附件
    attachments = scrapy.Field()
    # 询价方式
    inquiryType = scrapy.Field()
    # 报价方式
    quoteType = scrapy.Field()
    # 付款方式
    payType = scrapy.Field()
    # 省份
    province = scrapy.Field()
    # 地市
    city = scrapy.Field()
    # 区县
    district = scrapy.Field()
    # 中标供应商
    supplierName = scrapy.Field()
    # 中标价格
    totalPrice = scrapy.Field()
    # 状态
    status = scrapy.Field()
    # 是否已采集明细
    workStatus = scrapy.Field()
    # 采集时间
    genTime = scrapy.Field()

    # 批次编号
    batchNo = scrapy.Field()

    def validate(self):
        if not self.get('_id') and not self.get('projectCode'):
            raise DropItem("Inquiry Error [projectCode]")
        return True


# 询价主信息列表数据模型
class InquiryStatus(BaseData):
    # 主键
    _id = scrapy.Field()
    # 项目编号
    projectCode = scrapy.Field()
    # 询价发布时间
    publishTime = scrapy.Field()
    # 是否已采集明细
    workStatus = scrapy.Field()

    def validate(self):
        if not self.get('_id') and not self.get('projectCode'):
            raise DropItem("InquiryStatus Error [projectCode]")
        return True


# 询价项目明细
class InquiryItem(scrapy.Item):
    """
    询价项目明细
    """
    # 主键
    _id = scrapy.Field()
    # 明细编号
    itemId = scrapy.Field()
    # 条目编号
    itemCode = scrapy.Field()
    # 项目编号
    projectCode = scrapy.Field()
    # 条目名称
    itemName = scrapy.Field()
    # 条目类型
    itemType = scrapy.Field()
    # 产品编码
    skuId = scrapy.Field()
    # 单位
    unit = scrapy.Field()
    # 数量
    count = scrapy.Field()
    # 交付时间
    deliveryTime = scrapy.Field()
    # 行项目备注
    notes = scrapy.Field()
    # 附件
    attachments = scrapy.Field()
    # 排序号
    sortNum = scrapy.Field()
    # 询价发布时间
    publishTime = scrapy.Field()

    # 批次编号
    batchNo = scrapy.Field()

    def validate(self):
        if not self.get('_id') and not self.get('itemId'):
            raise DropItem("BidItem Error [itemId]")
        return True


if __name__ == '__main__':
    pass
