from zc_core.pipelines.helper.spu_helper import SpuHelper
from scrapy.exceptions import IgnoreRequest


class SpuFilterMiddleware(object):

    def __init__(self):
        self.spu_helper = SpuHelper()

    def process_request(self, request, spider):
        meta = request.meta
        req_type = meta.get('reqType', None)
        if req_type == "group":
            sku_id = meta.get('skuId', None)
            if sku_id and self.spu_helper.contains(sku_id):
                raise IgnoreRequest('同款已采: %s' % sku_id)
