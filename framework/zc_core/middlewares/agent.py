# encoding=utf-8
"""
agent中间件
"""
import logging
import random
from zc_core.middlewares.agents.user_agents import agents

logger = logging.getLogger(__name__)


class UserAgentMiddleware(object):

    def process_request(self, request, spider):
        agent = random.choice(agents)
        if agent:
            request.headers['User-Agent'] = agent
        else:
            spider.logger.error('no agent got...')
