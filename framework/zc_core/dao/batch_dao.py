# -*- coding: utf-8 -*-
import logging
from datetime import datetime

from zc_core.model.items import Batch
from zc_core.util.batch_gen import batch_no_to_gen_time
from zc_core.client.mongo_client import Mongo

logger = logging.getLogger(__name__)


class BatchDao(object):
    """
    批次操作简易封装（MongoDB）
    """

    def create_batch(self, batch_no, collection='t_batch'):
        """
        保存批次信息
        :param batch_no:
        :param collection:
        :return:
        """
        # 记录批次开始状态
        item = Batch()
        item['_id'] = batch_no
        item['genTime'] = batch_no_to_gen_time(batch_no)
        item['startTime'] = datetime.utcnow()
        item['collectStatus'] = 1

        Mongo().insert_or_update(collection, '_id', item)

    def update_batch(self, batch_no, status=2, collection='t_batch'):
        """
        更新批次信息
        :param batch_no:
        :param status:
        :param collection:
        :return:
        """
        # 记录批次结束状态
        item = Batch()
        item['_id'] = batch_no
        item['endTime'] = datetime.utcnow()
        item['collectStatus'] = status

        Mongo().insert_or_update(collection, '_id', item)
