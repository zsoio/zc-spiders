# -*- coding: utf-8 -*-
from zc_core.client.mongo_client import Mongo


class ItemDataDao(object):
    """
    批次商品操作简易封装（Mongo）
    """

    def get_batch_data_list(self, batch_no, fields={'_id': 1}, query={}):
        return Mongo().list('data_{}'.format(batch_no), query=query, fields=fields)
