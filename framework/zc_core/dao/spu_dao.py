# -*- coding: utf-8 -*-
from zc_core.client.mongo_client import Mongo


class SpuDao(object):
    """
    批次商品操作简易封装（Mongo）
    """

    # 获取批次商品列表
    def get_batch_spu_list(self, batch_no, fields={'_id': 1}, query={}):
        return Mongo().list('spu_{}'.format(batch_no), query=query, fields=fields)
