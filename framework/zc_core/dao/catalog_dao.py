# -*- coding: utf-8 -*-
from zc_core.client.mongo_client import Mongo


class CatalogDao(object):
    """
    批次商品操作简易封装（Mongo）
    """

    # 获取批次分类列表
    def get_cat3_list_by_cat1(self, batch_no=None, cat1_list=[], fields={'_id': 1}):
        coll = 'catalog_pool'
        if batch_no:
            coll = 'cat_{}'.format(batch_no)
        cat2_list = Mongo().list(coll, query={'parentId': {'$in': cat1_list}}, fields={'_id': 1})
        cat3_list = Mongo().list(coll, query={'level': 3, 'parentId': {'$in': [x.get('_id') for x in cat2_list]}}, fields=fields)

        return cat3_list

    # 获取批次分类列表
    def get_batch_cat_list(self, batch_no, fields={'_id': 1}, query={}):
        return Mongo().list('cat_{}'.format(batch_no), query=query, fields=fields)

    # 获取批次分类列表
    def get_cat_list_from_pool(self, fields={'_id': 1}, query={}):
        return Mongo().list('catalog_pool', query=query, fields=fields)
