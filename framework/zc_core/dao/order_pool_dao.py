# -*- coding: utf-8 -*-
from zc_core.client.mongo_client import Mongo
from zc_core.util.batch_gen import batch_to_year


class OrderPoolDao(object):
    """
    商品池操作简易封装（Mongo）
    """

    # 查询未采集列表
    def get_todo_list(self, batch_no, fields=None, query={}):
        # 处理订单列表
        year = batch_to_year(batch_no)
        query['workStatus'] = {'$ne': 1}
        return Mongo(year=year).list('order_{}'.format(year), query=query, fields=fields)
