# -*- coding: utf-8 -*-
from scrapy.utils.project import get_project_settings
from zc_core.client.mongo_client import Mongo


class MissSkuDao(object):
    """
    商品池操作简易封装（Mongo）
    """

    # hash查询全量列表
    def get_miss_sku_list(self, fields={}, query={}):
        query['workStatus'] = {'$ne': 1}

        return Mongo().list('miss_sku', query=query, fields=fields)
