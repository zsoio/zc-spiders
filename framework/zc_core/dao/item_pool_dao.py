# -*- coding: utf-8 -*-
from zc_core.client.mongo_client import Mongo
from scrapy.utils.project import get_project_settings

class ItemPoolDao(object):
    """
    商品池操作简易封装（Mongo）
    """

    # hash查询全量列表
    def get_item_pool_list(self, fields={'_id': 1}, query={}):
        return Mongo().list('item_data_pool', query=query, fields=fields)

    def get_distinct_supplier_list(self, fields={'supplierName': 1}, query={}):
        settings = get_project_settings()
        max_offline_time = settings.get('MAX_OFFLINE_TIME', 1)
        query['offlineTime'] = {'$lte': max_offline_time}

        return Mongo().distinct('item_pool', 'supplierName', query=query, fields=fields)