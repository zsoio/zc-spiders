# -*- coding: utf-8 -*-
from zc_core.client.mongo_client import Mongo


class SupplierDao(object):
    """
    批次商品操作简易封装（Mongo）
    """

    # 获取批次供应商列表
    def get_batch_supplier_list(self, batch_no, fields={'_id': 1}, query={}):
        return Mongo().list('supplier_{}'.format(batch_no), query=query, fields=fields)

    # 获取全量供应商列表
    def get_supplier_pool_list(self, fields={'_id': 1}, query={}):
        return Mongo().list('supplier_pool', query=query, fields=fields)
