# -*- coding: utf-8 -*-
import requests

if __name__ == '__main__':
    # url = 'https://www.zcygov.cn/front/detail/item/19657235'
    url = 'https://www.zcygov.cn/front/api/detail/item/dealRecord?timestamp=1583387992&itemId=64256812&pageSize=10&pageNo=1'
    headers = {
        'Connection': 'keep-alive',
        'Accept': 'application/json, text/plain, */*',
        'X-Requested-With': 'XMLHttpRequest',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400',
        'Referer': 'https://www.zcygov.cn/items/19657235?searchType=1',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'zh-CN,zh;q=0.9',
        'Cookie': '_zcy_log_client_uuid=11c323c0-44d1-11ea-951c-856ada6f55a5; aid=610102; districtCode=001000'
    }

    rs = requests.get(
        url=url,
        headers=headers,
    )
    print(rs.text)
