# -*- coding: utf-8 -*-
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.util.http_util import retry_request
from zc_core.spiders.base import BaseSpider
from guangxi.rules import *


class CatalogSpider(BaseSpider):
    name = 'catalog'
    # 常用链接
    back_cat_url = 'https://www.zcygov.cn/api/micro/category/backCategories/categoriesByLayer?timestamp=1571188114&layer=0&isContainFrozen=true'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(CatalogSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.cookie = {
            'districtCode': '001000',
            'districtName': '%E5%9B%BD%E5%AE%B6%E7%A8%8E%E5%8A%A1%E6%80%BB%E5%B1%80%E6%9C%AC%E7%BA%A7',
            'SESSION': 'NmQ1MThiODUtNmFjMy00ZGRkLThmMTAtYmMzMWU3OGQ4Mjc0',
        }

    def start_requests(self):
        # 首页分类
        yield Request(
            url=self.back_cat_url,
            meta={
                'reqType': 'catalog',
                'batchNo': self.batch_no,
            },
            cookies=self.cookie,
            callback=self.parse_back_catalog,
            errback=self.error_back,
            priority=100,
        )

    # 处理首页品类列表
    def parse_back_catalog(self, response):
        meta = response.meta
        batch_no = meta.get('batchNo')
        # 处理品类列表
        cats = parse_back_catalog(response)
        if cats:
            self.logger.info('后台分类: count[%s]' % len(cats))
            yield Box('catalog', self.batch_no, cats)
