# -*- coding: utf-8 -*-
import random
import time
from scrapy import Request
from zc_core.util.http_util import retry_request
from zc_core.spiders.base import BaseSpider
from guangxi.rules import *
from zc_core.dao.sku_pool_dao import SkuPoolDao
from zc_core.dao.batch_dao import BatchDao
from zc_core.util.done_filter import DoneFilter
from scrapy.exceptions import IgnoreRequest


class FullSpider(BaseSpider):
    name = 'full'
    # 常用链接
    item_url = 'https://www.zcygov.cn/front/detail/item/{}?timestamp={}'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(FullSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)
        # 避免重复采集
        self.done_filter = DoneFilter(self.batch_no)

    def start_requests(self):
        pool_list = SkuPoolDao().get_sku_pool_list()
        self.logger.info('全量：%s' % (len(pool_list)))
        random.shuffle(pool_list)
        for sku in pool_list:
            sku_id = sku.get('_id')
            # 避免无效采集
            offline_time = sku.get('offlineTime', 0)
            if offline_time > settings.get('MAX_OFFLINE_TIME', 2):
                self.logger.info('忽略: [%s][%s]', sku_id, offline_time)
                continue
            # 避免重复采集
            if self.done_filter.contains(sku_id) and not settings.get('FORCE_RECOVER', False):
                self.logger.info('已采: [%s]', sku_id)
                continue

            yield Request(
                url=self.item_url.format(sku_id, str(int(time.time()))),
                headers={
                    'Connection': 'keep-alive',
                    'Cache-Control': 'max-age=0',
                    'Upgrade-Insecure-Requests': '1',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-CN,zh;q=0.9',
                },
                meta={
                    'reqType': 'item',
                    'batchNo': self.batch_no,
                    'supplierId': sku.get('supplierId'),
                    'supplierName': sku.get('supplierName'),
                    'skuId': sku_id,
                },
                callback=self.parse_item_data,
                errback=self.error_back,
            )

    # 处理ItemData
    def parse_item_data(self, response):
        meta = response.meta
        sku_id = meta.get('skuId')
        data = parse_item_data(response)
        if data:
            self.logger.info('商品: [%s]' % sku_id)
            yield data
        else:
            self.logger.error('下架: [%s]' % sku_id)
