from scrapy import cmdline

# cmdline.execute('scrapy crawl area'.split())
# cmdline.execute('scrapy crawl catalog'.split())
# cmdline.execute('scrapy crawl supplier'.split())
# cmdline.execute('scrapy crawl sku'.split())
cmdline.execute('scrapy crawl full'.split())
# cmdline.execute('scrapy crawl full -a batchNo=20200112'.split())
# cmdline.execute('scrapy crawl order'.split())
# cmdline.execute('scrapy crawl order -a batchNo=20200304'.split())
