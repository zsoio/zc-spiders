import requests

# data = {
#     "frameBodySecretParam": "JTdCJTIycmVmZXJlciUyMiUzQSUyMmh0dHBzJTNBJTJGJTJGemMucGxhcC5taWwuY24lMkZsb2dpbl9sYXllci5odG1sJTIyJTdE"
# }
# headers = {
#     'Content-Type':'application/x-www-form-urlencoded;charset=utf-8'
# }
# response=requests.post('https://zc.plap.mil.cn/EpointMallService/rest/userinfo_user/getsm2pubkey',data=data,headers=headers)
# print(response.text)
data = {
    "frameBodySecretParam":"JTdCJTIyY2lkJTIyJTNBJTIyJTIyJTJDJTIyeGRwcm9kdWN0Y29kZSUyMiUzQSUyMiUyMiUyQyUyMnByb2R1Y3RuYW1lJTIyJTNBJTIyJTIyJTJDJTIyYnJhbmRuYW1lJTIyJTNBJTIyJTIyJTJDJTIyc3RhdHVzJTIyJTNBJTIyMCUyMiUyQyUyMmlzdHVpamlhbiUyMiUzQSUyMiUyMiUyQyUyMnBhZ2UlMjIlM0E0OTEyJTJDJTIyc2l6ZSUyMiUzQTEwJTJDJTIycmVmZXJlciUyMiUzQSUyMmh0dHBzJTNBJTJGJTJGemMucGxhcC5taWwuY24lMkZsb2NhbHByaWNlLmh0bWwlMjIlN0Q%3D"}
headers = {
    "Host": "zc.plap.mil.cn",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0",
    "Accept": "application/json, text/javascript, */*; q=0.01",
    "Accept-Language": "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2",
    "Accept-Encoding": "gzip, deflate, br",
    "Content-Type": "application/x-www-form-urlencoded;charset=utf-8",
    "X-Requested-With": "XMLHttpRequest",
    "Origin": "https://zc.plap.mil.cn",
    "Connection": "keep-alive",
    "Referer": "https://zc.plap.mil.cn/localprice.html",
    "Cookie": "VerifyCode=weS8KAfSBxE%3D; displayname=%E6%B5%99%E6%B1%9F%E7%89%A9%E4%BA%A7%E7%94%B5%E5%AD%90%E5%95%86%E5%8A%A1%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8; nickname=%E6%B5%99%E6%B1%9F%E7%89%A9%E4%BA%A7%E7%94%B5%E5%AD%90%E5%95%86%E5%8A%A1%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8; danweiname=%E6%B5%99%E6%B1%9F%E7%89%A9%E4%BA%A7%E7%94%B5%E5%AD%90%E5%95%86%E5%8A%A1%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8; usertype=2; userinfo=2goZRGBgWMK2xswoKQ%2F0%2Byhm3xqIT6BwnLlHsIOzX6q7utjU8sP9xnPchm2UJkcE4FSlkbQT3edkOrbRFcA3HNnrePT76bjifS0oxMclQ4ucG4zjZsEEHjAbJpc5%2FTyXf2yFgxHxDgdEMfHuLtP1afd9JA4uLW393uX8X7h4Xa4nKKeqxK6lF5xgY9IlMH6zWYkzDuCZM5MD%2BN95h0qxNfRG8rVkyHx1avrZGUI4Lxapnt7%2B6K%2F586N9fF4AHKsJErOg1NiCNpxNszwHp9jgRzszdPPXVqXqezUsKjoc8UeXDfY0xkDDDvoddP3R9AIqckbZnsT1%2Fve1ueuFazN4P9%2B4oX%2B6eVMYLGc%2FSyKhJEL%2BOhCnJN1Gr%2FdwClmpJYxCDiN2Z7C%2Fw2IBDw%2B7Gf6WK8vdIwTYcN7T1i7TOi8AueQ7Y%2FVIm955qSXIR%2BRVOsQRFetNvepW60q7JTAInh2sfg%3D%3D",
    "Pragma": "no-cache",
    "Cache-Control": "no-cache",
}
response = requests.post('https://zc.plap.mil.cn/EpointMallService/rest/product_producttemp/getcansetprice',
                         headers=headers, data=data)
print(response)
print(response.text)

# from gmssl import sm2
# # sm2的公私钥
# SM2_PRIVATE_KEY = '00B9AB0B828FF68872F21A837FC303668428DEA11DCD1B24429D0C99E24EED83D5'
# SM2_PUBLIC_KEY = '04A2C5ABFE372540F0CFAB644776B1CEC911F21739042D9FDF8326324357790DBA3E3900338DE4FFDBA48204A176D444687904422180E0B1E3AF316C4CA09AA704'
#
# sm2_crypt = sm2.CryptSM2(public_key=SM2_PUBLIC_KEY, private_key=SM2_PRIVATE_KEY)
#
# # 加密
# def encrypt(info):
#     encode_info = sm2_crypt.encrypt(info.encode(encoding="utf-8"))
#     return encode_info
#
#
# # 解密
# def decrypt(info):
#     decode_info = sm2_crypt.decrypt(info).decode(encoding="utf-8")
#     return decode_info
#
#
# if __name__ == "__main__":
#     info = "123456"
#     encode_info = encrypt(info)
#     print(encode_info)
#     decode_info = decrypt(encode_info)
#     print(decode_info)
