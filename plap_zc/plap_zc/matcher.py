import re


def match_cat_id(link):
    # /channel/176
    if link:
        arr = re.findall(r'/channel/(\d+)', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


def match_total_page(link):
    # /npc/products.html?page=14082&q%5Bcatalog_id_in%5D=&q%5Bs%5D=id+desc
    if link:
        arr = re.findall(r'page=(\d+)&', link.strip())
        if len(arr):
            return arr[0].strip()
    return '0'


# 从链接中解析订单编码
def match_order_id(link):
    # /more_notices.html?mall_show=true&no=f43035134c
    if link:
        arr = re.findall(r'&no=(.+)', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析商品编码
def match_sku_id_from_order(link):
    # /more_notices/product_show.html?back=%2Fmore_notices.html%3Fmall_show%3Dtrue%26no%3Df43035134c&item_id=455784&product_id=55709
    if link:
        arr = re.findall(r'product_id=(.+)', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从链接中解析商品编码
def match_sku_id_from_sku(link):
    # /commodities/220403?p_id=6641&target=_blank
    if link:
        arr = re.findall(r'/products/(\d+)\.htm*', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从html解析商品价格
def match_sale_price(txt):
    # $("#price_tag").html("¥2,750.00");
    if txt:
        arr = re.findall(r'\$\("#price_tag"\)\.html\("¥(.+)"\);', txt.strip())
        if len(arr):
            return arr[0].strip()

    return '-1'


# 从链接中解析品牌编码
def match_brand_id(link):
    # /search.html?combo=0_1_0_0_0_0&k=%2A
    if link:
        arr = re.findall(r'combo=0_(\d+)_0_0_0_0', link.strip())
        if len(arr):
            return arr[0].strip()
        else:
            # /channel/123_4_0_0_0_0_0_0_0_0
            arr = re.findall(r'/channel/\d+_(\d+)_.*', link.strip())
            if len(arr):
                return arr[0].strip()

    return ''


# 从链接中解析供应商编码
def match_supplier_id(link):
    # /oc/0_0_0_0_0_11111_0_0_0
    if link:
        arr = re.findall(r'/oc/\d+_\d+_\d+_\d+_\d+_(\d+)_\d+_\d+_\d+', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从链接中解析供应商编码
def match_sku_total_page(link):
    # /oc/0_0_4_0_0_11111_0_23271_0.html
    if link:
        arr = re.findall(r'_(\d+)_0\.html', link.strip())
        if len(arr):
            return int(arr[0].strip())

    return 0


# 从链接中解析供应商编码
def match_spu_total_page(link):
    # /npc/products.html?page=6629&q%5Bcatalog_id_in%5D=&q%5Bs%5D=id+desc
    if link:
        arr = re.findall(r'page=(\d+)&', link.strip())
        if len(arr):
            return int(arr[0].strip())

    return 0


# 从链接中解析供应商编码
def match_product_id(link):
    # /products/30240093
    if link:
        arr = re.findall(r'/products/(\d+)', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从链接中解析供应商编码
def match_product_id(link):
    # /products/30240093
    if link:
        arr = re.findall(r'/products/(\d+)', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从链接中解析供应商编码
def match_commodity_id(link):
    # /commodities/707715
    if link:
        arr = re.findall(r'/commodities/(\d+)', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从链接中解析供应商编码
def match_origin_price(link):
    # 限价（3324.05元）=市场参考价(3499.0元) x 折扣率(0.95)
    if link:
        arr = re.findall(r'市场参考价\((.+)元\)', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# ======================
# 解析skuId
def match_item_id(link):
    if link:
        arr = re.findall(r'\'(\d+)\',', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 解析skuId
def match_item_id_from_url(link):
    # <a href="/ra/bids.html?ra_project_id=305007" target="_blank">查看结果</a>
    if link:
        arr = re.findall(r'ra_project_id=(\d+)', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从html解析商品价格
def match_bid_price(txt):
    # （当前最低价：¥6,225.00）
    if txt:
        arr = re.findall(r'（当前最低价：¥(.+)）', txt.strip())
        if len(arr):
            return arr[0].strip()

    return '-1'


# 从html解析商品价格
def match_bid_item_id(js):
    # show_ra_project_product(4064920,true)
    if js:
        arr = re.findall(r'show_ra_project_product\((\d+),.+', js.strip())
        if len(arr):
            return arr[0].strip()
    return ''
# ======================

# # 测试
if __name__ == '__main__':
    # print(match_bid_item_id('show_ra_project_product(4064920,true)'))
    # print(match_supplier_id('/oc/0_0_0_0_0_11111_0_0_0'))
    print(match_origin_price('限价（3324.05元）=市场参考价(3499.0元) x 折扣率(0.95)'))
    pass
