# -*- coding: utf-8 -*-
import scrapy
from scrapy.exceptions import DropItem
from zc_core.model.items import Sku, ItemData


# spu日志数据模型
class PlapZcSku(Sku):
    # 类目列表
    catalogName = scrapy.Field()
    # 类目Id
    catalogId = scrapy.Field()
    # 报价状态
    statusName = scrapy.Field()
    # 报价状态
    goodsType = scrapy.Field()
    # 批次编号
    batchNo = scrapy.Field()
    skuCode = scrapy.Field()
    materialCode = scrapy.Field()
    xdSkuCode = scrapy.Field()
    floorPrice = scrapy.Field()
    productId = scrapy.Field()

    def validate(self):
        if not self.get('skuId'):
            raise DropItem("skuId Error [page] -> (%s)" % self.get('page'))
        return True


class PlapZcFull(ItemData):
    # 类目列表
    catalogName = scrapy.Field()
    # 类目Id
    catalogId = scrapy.Field()
    # 报价状态
    statusName = scrapy.Field()
    # 报价状态
    goodsType = scrapy.Field()
    # 批次编号
    batchNo = scrapy.Field()
    skuCode = scrapy.Field()
    materialCode = scrapy.Field()
    xdSkuCode = scrapy.Field()
    floorPrice = scrapy.Field()
    productId = scrapy.Field()

    def validate(self):
        if not self.get('skuId'):
            raise DropItem("skuId Error [page] -> (%s)" % self.get('page'))
        return True
