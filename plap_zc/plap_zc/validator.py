# encoding=utf-8
"""
响应对象业务校验
"""
from scrapy.exceptions import IgnoreRequest
from zc_core.middlewares.validate import BaseValidateMiddleware
from plap_zc.utils.http_util import retry_request


class BizValidator(BaseValidateMiddleware):

    def validate_sku(self, request, response, spider):

        if response.text:
            return response
        else:
            spider.logger.info('[Sku]响应异常：[%s] -> [%s]' % (request.url, request.meta))
            return retry_request(request)
