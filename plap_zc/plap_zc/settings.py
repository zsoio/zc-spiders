# -*- coding: utf-8 -*-
BOT_NAME = 'plap_zc'

SPIDER_MODULES = ['plap_zc.spiders']
NEWSPIDER_MODULE = 'plap_zc.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 12
# DOWNLOAD_DELAY = 0.2
CONCURRENT_REQUESTS_PER_DOMAIN = 12
CONCURRENT_REQUESTS_PER_IP = 12

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'plap_zc.validator.BizValidator': 500
}
# 指定代理池类
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.mogu_pool.MoguProxyPool'
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 每两次请求间隔不小于的秒数
MIN_RELOAD_PERIOD = 30
# 代理积分阈值
PROXY_SCORE_LIMIT = 2

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'plap_zc.pipelines.PlapBoxPipeline': 540,
}

# 供应商
SUPPLIERS = {
    # "1d616f83-34f1-49a8-b1b5-d1a222873f05": "行丰银拓科技集团有限公司",
    # "250e64c2-d3ca-4bac-bf4e-58e9c8bdc9bb": "海信（山东）空调有限公司",
    # "76ed0bec-a5bb-4eb8-9319-6be975faaed5": "苏宁易购集团股份有限公司",
    # "06b4d18f-5145-482e-bdba-f696901c034b": "领先未来科技集团有限公司",
    # "de5ecd90-b4ee-4d9d-8512-ebf2802d1a8d": "东南慧达（北京）科技有限公司",
    # "bd41a412-3d31-45b2-aa68-243989794461": "和顺达兴科技集团有限公司",
    # "c82bd7f0-2474-47fc-8a96-853fde74577b": "北京京东叁佰陆拾度电子商务有限公司",
    # "4548f8f9-2c51-4278-9304-6b0928a7f619": "得力集团有限公司",
    # "6feafeba-fefa-4942-a0b5-ab7b3473b303": "广东美的暖通设备有限公司",
    # "6afa078e-cb9d-4afd-8593-349fca0e8209": "方正科技集团股份有限公司",
    # "6ef308a4-c5d8-438f-ba29-4f8b566888e6": "生产商测试请勿下单",
    # "0f48935d-7b4d-48cc-9914-9bc7a461eb81": "深圳齐心集团股份有限公司",
    # "0f8072d2-fa2e-42bd-9ce7-ab280f69aad5": "北京政采商贸有限公司",
    # "e8449db8-4448-4216-968d-8150615ea3be": "测试推荐供应商1",
    # "a8efc1e3-640c-4e5f-af78-ed43c29efb14": "电商测试供应商",
    # "b98b2a94-2d4a-4af2-91e2-38a82974e878": "晓润集团有限公司",
    # "1cce5eec-0677-48d3-bc75-c6fa5f9f6d64": "欧菲斯集团股份有限公司",
    # "f2f4fdc7-949e-4de7-8439-8c839267e7bb": "推荐测试供应商2",
    # "e289c269-9ffb-4bcd-8393-2df06261b429": "成都汇鸿教学设备制造有限公司",
    # "aebb6dd8-c4da-4cb5-b978-1d311ab7c9d3": "中铁物贸集团有限公司",
    # "65293f4a-3f1b-4a1e-97a3-32aa63aab639": "浙江物产电子商务有限公司",
    # "666e625d-e283-44ee-ab67-cbcf6ce754ba": "广东美的制冷设备有限公司",
    # "b7ac8993-fe65-4434-97a8-dd011e8f379a": "华润四川医药有限公司",
    # "79366cf1-2ddd-4822-aae1-1f13f32eaaef": "广博集团股份有限公司",
    # "c162c53b-9443-4d3d-b148-94971e5897cc": "北京市泰龙吉贸易有限公司",
    # "ed3510e8-442f-491a-af19-36610c4d7c71": "合肥美的电冰箱有限公司",
    # "926153fc-5742-4e56-8e54-f93c77d751e1": "同方股份有限公司",
    # "50951099-be65-41fb-803b-e55856a092ad": "大型销售商测试供应商",
    # "b1b45dfc-8867-4a89-accb-f45f002b2ffd": "北京莱盛高新技术有限公司",
    # "10183119-3462-4320-9089-5729b561e756": "合肥美的洗衣机有限公司",
    # "3ac62c90-c477-4c5f-9464-886fc201b134": "四川长虹电器股份有限公司",
    # "0a334d6a-5ade-49c5-b115-c9f03561af83": "小型销售商测试账号",
    # "95ca7561-ef49-45ab-92a9-460379f6caae": "0730wdx",
    # "d141c721-3056-4457-ae8a-45493bbfc5d2": "深圳创维数字技术有限公司",
    # "159edad0-ff6d-46e8-b0ae-86c67d31d848": "海信（山东）冰箱有限公司",
    # "b507e3bb-02de-4fd7-a31d-a9fd882a9940": "123",

}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'plap_zc_2019'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 3
# 下载超时
DOWNLOAD_TIMEOUT = 90
# 登录账号集合

ACCOUNTS = {
    # 'deli': [
    #     {'acc': 'tianyan666', 'pwd': 'tianyan123'}
    # ],
    'zjmi': [
        {'acc': 'zjmiec', 'pwd': 'Zjmiec1234!'}
    ],
}
CUSTOMER = 'deli'
# CUSTOMER = 'zjmi'

# 限制竞价采集页数
BID_STATUS_LIMIT = {
    '9': 1000,
    '8': 1000,
}
