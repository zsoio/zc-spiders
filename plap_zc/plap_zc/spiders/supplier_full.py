# -*- coding: utf-8 -*-
import re
import json
import base64
import scrapy
from urllib.parse import quote

from scrapy.utils.project import get_project_settings
from zc_core.dao.sku_pool_dao import SkuPoolDao
from zc_core.dao.supplier_dao import SupplierDao
from zc_core.model.items import Box
from zc_core.spiders.base import BaseSpider
from plap_zc.rules import parse_on_line_sku, parse_on_line_page, logger
from plap_zc.utils.login_selenium import SeleniumLogin


class FullSpider(BaseSpider):
    name = 'supplier_full'
    customer = ''
    custom_settings = {
        'CONCURRENT_REQUESTS': 12,
        # 'DOWNLOAD_DELAY': 0.5,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 12,
        'CONCURRENT_REQUESTS_PER_IP': 12,
    }
    sku_list_url = 'https://zc.plap.mil.cn/EpointMallService/rest/basicinfo_spgys/getproducts'

    def __init__(self, customer=None, batchNo=None, *args, **kwargs):
        settings = get_project_settings()
        self.customer = customer
        if not customer:
            self.customer = settings.get('CUSTOMER', None)
        super(FullSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        if not self.customer:
            raise Exception('请指定客户')
        self.size = 10
        self.cookie_dict = SeleniumLogin().get_cookies(self.customer)
        self.brand_name_list = list(
            set([re.sub('（(.*?)）', '', re.sub('\((.*?)\)', '', x.get('brandName')).strip()).strip() for x in
                 SkuPoolDao().get_sku_pool_list(query={"brandName": {"$exists": True}},
                                                fields={"brandName": 1})]))

    def _build_sku(self, page, supplier_id, supplier_name, sort='price_asc', keyword=''):
        # https://zc.plap.mil.cn/gysgoods.html?gysguid=4548f8f9-2c51-4278-9304-6b0928a7f619&page=1&sort=price_asc&cid=&keyword=
        return scrapy.FormRequest(
            url=self.sku_list_url,
            method='POST',
            meta={
                'reqType': 'item',
                'supplierId': supplier_id,
                'batchNo': self.batch_no,
                'page': page,
                'supplierName': supplier_name,
                'keyword': keyword
            },
            headers={
                "Accept": "application/json, text/javascript, */*; q=0.01",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
                "Connection": "keep-alive",
                "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
                "Host": "zc.plap.mil.cn",
                "Origin": "https://zc.plap.mil.cn",
                "Referer": "https://zc.plap.mil.cn/localprice.html?version=1001",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36 Edg/92.0.902.84",
                "X-Requested-With": "XMLHttpRequest",
            },
            formdata={"frameBodySecretParam": str(quote(str(base64.b64encode(quote(json.dumps(
                {"gysguid": supplier_id, "cid": "", "sort": sort, "page": page,
                 "size": self.size, "keyword": keyword,
                 "referer": f"https://zc.plap.mil.cn/gysgoods.html?gysguid={supplier_id}"})).replace(
                '%20', '').replace('/', '%2F').encode('utf-8')))[2:-1]).replace('%20', '').replace('/',
                                                                                                   '%2F'))},
            callback=self.parse_sku_page,
            errback=self.error_back,
            cookies=self.cookie_dict,
            priority=200,
        )

    def start_requests(self):
        # 品类、品牌
        # settings = get_project_settings()
        # supplier_items = settings.get('SUPPLIERS', {})
        # suppliers = []
        # key = list(supplier_items.keys())
        # value = list(supplier_items.values())
        # for index, data in enumerate(key):
        #     suppliers.append({"_id": data, "name": value[index]})
        suppliers = SupplierDao().get_batch_supplier_list('20211017', fields={"name": 1, "_id": 1})
        for supplier in suppliers:
            yield self._build_sku(1, str(supplier.get('_id')), supplier.get('name'))

    # 处理catalog列表
    def parse_sku_page(self, response):
        meta = response.meta
        supplier_id = meta.get('supplierId')
        cur_page = meta.get('page')
        supplier_name = meta.get('supplierName')
        keyword = meta.get('keyword')
        # 处理品类列表
        items = parse_on_line_sku(response)
        if items:
            if keyword:
                logger.info(
                    '[品牌]供应商:{} 当前页:{} count:{} keyword:{}'.format(supplier_name, cur_page, len(items), keyword))
            else:
                logger.info('供应商:{} 当前页:{} count:{}'.format(supplier_name, cur_page, len(items)))
            yield Box('item', self.batch_no, items)
        else:
            logger.info('{} 为空 当前页 {}'.format(supplier_name, cur_page))
        if items:
            if cur_page == 1:
                totals = parse_on_line_page(response, self.size)

                if totals > 1000:
                    logger.info('供应商[超限]{}  页数 {}'.format(supplier_name, totals))
                    for brandName in self.brand_name_list:
                        yield self._build_sku(1, supplier_id, supplier_name, keyword=brandName)
                else:
                    logger.info('供应商:{} 页数:{} keyword:{}'.format(supplier_name, totals, keyword))
                    if not keyword:
                        for page in range(2, totals + 1):
                            yield self._build_sku(page, supplier_id, supplier_name)
                    else:
                        for page in range(2, totals + 1):
                            yield self._build_sku(totals, supplier_id, supplier_name, keyword=keyword)
