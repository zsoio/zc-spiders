# encoding:utf-8
from pymongo import UpdateOne
from zc_core.client.mongo_client import Mongo

mongo = Mongo()


# 处理数据
def filter_data():
    update_bulk = list()
    src_list = mongo.list("data_20210730", fields={'_id': 1, 'skuName': 1, 'brandName': 1})
    for item in src_list:
        brandName = item.get('brandName')
        if brandName:
            if item.get('skuName').count(brandName) < 1:
                item['brandName'] = ''
                update_bulk.append(UpdateOne({'_id': item.get('_id')}, {'$set': item}, upsert=False))
    print(len(update_bulk))
    mongo.bulk_write('data_20210730', update_bulk)
    mongo.close()
    print('任务完成~')


if __name__ == '__main__':
    filter_data()
