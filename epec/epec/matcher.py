# -*- coding: utf-8 -*-
import re

# 测试
if __name__ == '__main__':
    pass

def match_sku_id(e):
    # "/ecmall/productInfo.do?productId=7822791&supplierCompanyId=143049&buycount=85"
    if e:
        arr = re.findall(r"productId=([\d]+)&", e.strip())
        if len(arr) > 0:
            return arr[0].strip()

def match_supplier_id(e):
    # "/ecmall/supplier/supplierindex.do?companyid=143049&buycount=85"
    if e:
        arr = re.findall(r"companyid=([\d]+)&", e.strip())
        if len(arr) > 0:
            return arr[0].strip()

def match_total_count(e):
    # dataSize = 4591
    if e:
        arr = re.findall(r"dataSize\s=\s([\d]+)", e.strip())
        if len(arr) > 0:
            return arr[0].strip()

def match_sale_price(e):
    #会 员 价：￥0.38
    if e:
        arr = re.findall(r"￥([\S]+)", e.strip())
        if len(arr) > 0:
            return arr[0].strip()

def match_sale_count(e):
    #已售出：10.23万 个
    if e:
        sales = 0
        unit = ''
        sales_arr = re.findall(r"已售出：([\S]+)\s", e.strip())
        unit_arr = re.findall(r"\s([\S]+)", e.strip())
        if len(sales_arr) > 0:
            sales = sales_arr[0].strip()
            if sales.endswith("万"):
                sales = int(float(sales[0:-1]) * 10000)
        if len(unit_arr) > 0:
            unit = unit_arr[0].strip()
        return sales,unit

def match_brand(e):
    #规格:B2551,  品牌:齐心,
    if e:
        brand_name = None
        brand_model = None
        name_arr = re.findall(r"品牌:([\w\d\s\-]+),", e.strip())
        model_arr = re.findall(r"规格:([\w\d\s\-]+),", e.strip())
        if len(name_arr) > 0:
            brand_name = name_arr[0].strip()
        if len(model_arr) > 0:
            brand_model = model_arr[0].strip()
        else:
            model_arr = re.findall(r"型号:([\w\d\s\-]+),", e.strip())
            if len(model_arr) > 0:
                brand_model = model_arr[0].strip()
        return brand_name,brand_model

