import os
import pandas as pd
from zc_core.client.mongo_client import Mongo
from scrapy.utils.project import get_project_settings


def export_item_data(collection, columns, header, file):
    list = Mongo().list(collection, query={"catalog1Name": {"$in": ["自动化办公设备", "行政办公"]}})
    if list:
        pd.DataFrame(list).to_excel(file, columns=columns, header=header, encoding='utf-8', index=False)
    print("任务完成")


if __name__ == '__main__':
    batch_no = '20211026'
    bot_name = get_project_settings().get('BOT_NAME')
    dir = f'/work/{bot_name}'
    if not os.path.exists(dir):
        os.makedirs(dir)
    export_item_data(
        'data_{}'.format(batch_no),
        ['_id', 'skuName', 'originPrice', 'salePrice', 'soldCount', 'brandName', 'unit', 'supplierName', 'catalog1Name',
         'catalog2Name', 'catalog3Name', 'skuImg'],
        ['商品编号', '商品名称', '原价', '销售价格', '销量', '品牌', '单位', '供应商', '一级分类', '二级分类', '三级分类', '首图'],
        f'{dir}/易派客_{batch_no}.xls'
    )
