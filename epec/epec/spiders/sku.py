# -*- coding: utf-8 -*-
import math
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from scrapy.utils.project import get_project_settings
from datetime import datetime
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from epec.rules import *
from zc_core.spiders.base import BaseSpider


class SkuSpider(BaseSpider):
    name = "sku"
    # 分类链接
    cata_url = "https://mall.epec.com/ecmall/static/epec/ecmall/html/electronicSupermarket/marketCategory.html"
    # 列表页
    sku_list_url = "https://mall.epec.com/ecmall/search/productInfoSearchFilterList.do"

    # 主页
    index_url = "https://mall.epec.com"

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 接口页数大小固定
        self.page_size = 6
        self.max_page_limit = math.ceil(5000 / self.page_size)
        settings = get_project_settings()
        self.ladders = settings.get('PRICE_LADDER', [])
        self.overflow = settings.get('PRICE_OVERFLOW', 5)

    def _build_list_req(self, callback, page, catalog_id, catalog_name, price_lad_index=None):
        data = {
            'categoryname': catalog_name,
            'sortText': 'zonghe;-;zonghe',
            'pageLimit': str(self.page_size),
            'pageStart': str((page - 1) * self.page_size)
        }
        if price_lad_index:
            data['maxPrice'] = str(self.ladders[price_lad_index])
            data['minPrice'] = str(self.ladders[price_lad_index - 1])
        return scrapy.FormRequest(
            url=self.sku_list_url,
            method="POST",
            meta={
                'reqType': 'sku',
                'page': page,
                'batchNo': self.batch_no,
                'indexUrl': self.index_url,
                'catalogId': catalog_id,
                'catalogName': catalog_name,
                'priceLadIndex': price_lad_index
            },
            headers={
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            formdata=data,
            callback=callback,
            errback=self.error_back,
        )

    def start_requests(self):
        yield Request(
            url=self.cata_url,
            method='GET',
            meta={
                'reqType': 'catalog',
                'batchNo': self.batch_no
            },
            callback=self.parse_catalog,
            errback=self.error_back,
            priority=100
        )

    def parse_catalog(self, response):
        if response:
            cats = parse_catalog(response)
            if cats and len(cats) > 0:
                self.logger.info('分类: count[%s]' % len(cats))
                yield Box('catalog', self.batch_no, cats)
                # 查询sku
                page = 1
                for cat in cats:
                    if cat.get('level') == 3:
                        yield self._build_list_req(callback=self.parse_sku_list, page=page,
                                                   catalog_id=cat.get('catalogId'), catalog_name=cat.get('catalogName'))
            else:
                self.logger.error('无分类: [%s] url -> [%s]' % (self.batch_no, self.cata_url))

    def parse_sku_list(self, response):
        if response and response.text:
            meta = response.meta
            page = meta.get("page")
            catalog3_id = meta.get('catalogId')
            catalog3_name = meta.get('catalogName')
            price_lad_index = meta.get('priceLadIndex')

            sku_list, item_list = parse_sku(response)
            if sku_list and len(sku_list) > 0:
                if price_lad_index:
                    self.logger.info('清单: page=%s, cat=%s, lad=%s %s,cnt=%s' % (
                        (page - 1) * self.page_size, catalog3_name, self.ladders[price_lad_index - 1],
                        self.ladders[price_lad_index], len(sku_list)))
                yield Box('sku', self.batch_no, sku_list)
                if item_list and len(item_list) > 0:
                    yield Box('item', self.batch_no, item_list)

                if page == 1:
                    total_page = parse_total_page(response, self.page_size)
                    self.logger.info("总页数: cat=%s, total=%s" % (catalog3_name, total_page))

                    if total_page < self.max_page_limit:
                        for page_cur in range(2, total_page + 1):
                            yield self._build_list_req(callback=self.parse_sku_list, page=page_cur,
                                                       catalog_id=catalog3_id, catalog_name=catalog3_name,
                                                       price_lad_index=price_lad_index)
                    else:
                        if not price_lad_index:
                            self.logger.info("超限1: cat=%s, total=%s" % (catalog3_name, total_page))
                            page_cur = 1
                            for price in range(1, len(self.ladders)):
                                yield self._build_list_req(callback=self.parse_sku_list, page=page_cur,
                                                           catalog_id=catalog3_id, catalog_name=catalog3_name,
                                                           price_lad_index=price)
                        elif price_lad_index:
                            self.logger.info("超限2: cat=%s, price=%s-%s,total=%s" % (
                                catalog3_name, self.ladders[price_lad_index - 1], self.ladders[price_lad_index],
                                total_page))

            else:
                self.logger.info('查无此页: [%s][%s]->[%s]' % (self.batch_no, catalog3_name, page))


