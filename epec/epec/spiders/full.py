# -*- coding: utf-8 -*-
import copy
import random

from scrapy import Request
from scrapy.utils.project import get_project_settings
from scrapy.exceptions import IgnoreRequest
from zc_core.spiders.base import BaseSpider
from zc_core.dao.sku_dao import SkuDao
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from zc_core.dao.batch_dao import BatchDao
from zc_core.util.done_filter import DoneFilter
from datetime import datetime
from epec.rules import *


class FullSpider(BaseSpider):
    name = "full"

    # 商品信息接口
    item_url = 'https://mall.epec.com/ecmall/getProductSkuList.do'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(FullSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)
        # 避免重复采集
        self.done_filter = DoneFilter(self.batch_no)

    def start_requests(self):
        settings = get_project_settings()
        sku_list = SkuDao().get_batch_sku_list(self.batch_no,
                                               fields={'_id': 1, 'batchNo': 1, 'supplierId': 1, 'supplierName': 1,
                                                       'offlineTime': 1, "catalog3Id": 1, "catalog3Name": 1},
                                               query={"$or": [{"salePrice": -1}, {"salePrice": None}]})
        self.logger.info('全量: %s' % (len(sku_list)))
        for sku in sku_list:
            sku_id = sku.get("_id")
            catalog3_id = sku.get("catalog3Id")
            catalog3_name = sku.get("catalog3Name")
            sp_name = sku.get("supplierName")
            sp_id = sku.get("supplierId")
            # 避免无效采集
            offline_time = sku.get('offlineTime', 0)
            if offline_time > settings.get('MAX_OFFLINE_TIME', 2):
                self.logger.info('忽略: [%s][%s]', sku_id, offline_time)
                continue
            # 避免重复采集
            if self.done_filter.contains(sku_id) and not settings.get('FORCE_RECOVER', False):
                self.logger.info('已采: %s', sku_id)
                continue

            yield scrapy.FormRequest(
                url=self.item_url,
                method="POST",
                meta={
                    'reqType': 'full',
                    'skuId': sku_id,
                    'batchNo': self.batch_no,
                    'catalog3Id': catalog3_id,
                    'catalog3Name': catalog3_name,
                    'supplierId': sp_id,
                    'supplierName': sp_name,
                },
                formdata={
                    "productId": sku_id,
                    "supplierCompanyId": sp_id
                },
                headers={
                    'Connection': 'keep-alive',
                    'Cache-Control': 'max-age=0',
                    'Upgrade-Insecure-Requests': '1',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-CN,zh;q=0.8'
                },
                callback=self.parse_item_data,
                errback=self.error_back,
            )

    def parse_item_data(self, response):
        meta = response.meta
        sku_id = meta.get('skuId')
        item_list = parse_item_data(response)
        if item_list and len(item_list) > 0:
            self.logger.info('补充商品: %s, count=%s' % (sku_id, len(item_list)))
            yield Box('item', self.batch_no, item_list)


