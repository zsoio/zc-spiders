# -*- coding: utf-8 -*-
from datetime import datetime
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from zc_core.spiders.base import BaseSpider
from epec.rules import *


class CatalogSpider(BaseSpider):
    name = 'catalog'
    # 常用链接
    cata_url = "https://mall.epec.com/ecmall/static/epec/ecmall/html/electronicSupermarket/marketCategory.html"

    def __init__(self, batchNo=None, *args, **kwargs):
        super(CatalogSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        yield Request(
            url=self.cata_url,
            method='GET',
            meta={
                'reqType': 'catalog',
                'batchNo': self.batch_no
            },
            callback=self.parse_catalog,
            errback=self.error_back,
            priority=100
        )

    def parse_catalog(self, response):
        if response:
            cats = parse_catalog(response)
            if cats and len(cats) > 0:
                self.logger.info('分类: count[%s]' % len(cats))
                yield Box('catalog', self.batch_no, cats)
            else:
                self.logger.error('无分类: [%s] url -> [%s]' % self.batch_no, self.cata_url)


