# -*- coding: utf-8 -*-
BOT_NAME = 'chinapost'

SPIDER_MODULES = ['chinapost.spiders']
NEWSPIDER_MODULE = 'chinapost.spiders'

ROBOTSTXT_OBEY = False
CONCURRENT_REQUESTS = 8
# DOWNLOAD_DELAY = 0.1
CONCURRENT_REQUESTS_PER_DOMAIN = 8
CONCURRENT_REQUESTS_PER_IP = 8
# COOKIES_ENABLED = False

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'chinapost.validator.BizValidator': 500
}
# 指定代理池类
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.zhima_pool.ZhimaProxyPool'
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 代理积分阈值
PROXY_SCORE_LIMIT = 3
# 下载超时
DOWNLOAD_TIMEOUT = 45

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'chinapost_2019'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 3
# 订单采集截止天数
ORDER_DEADLINE_DAYS = 180
# 登录重试次数
MAX_LOGIN_RETRY = 2

# 登录账号集合
ACCOUNTS = [
    {'userName': '04145787', 'pw': 'Post1A2B3C', 'industryType': 'D'}
]

# 输出
OUTPUT_ROOT = '/work/chinapost/'
