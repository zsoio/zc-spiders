# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.util.http_util import retry_request
from zc_core.model.items import Box
from zc_core.spiders.base import BaseSpider
from chinapost.rules import *
from chinapost.util.login import SeleniumLogin


class SkuSpider(BaseSpider):
    name = 'sku'
    # 常用链接

    index_url = 'https://cgwzgy.11185.cn/oscp/home/mainTc.html'
    sku_list_url = 'https://cgwzgy.11185.cn/oscp/goods/category/goodsList.html?id={}&brandId=&priceBegin=&priceEnd=&sort=salePrice_asc&supIds=&pageNumber={}&pageSize={}&flFlag=0&checked4Page2CompGoodsSnStr='

    def __init__(self, timeDelta=-1, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(timeDelta=timeDelta, batchNo=batchNo, *args, **kwargs)
        self.page_size = 100

    def start_requests(self):
        # cookies = SeleniumLogin().get_cookies()
        cookies = {'JSESSIONID': '71713859B13383CBEE1CEE1C66DBE3F5',
                   'centralSessionId': '49756d0c-83df-4702-8d84-f5399d5336ea'}
        if not cookies:
            self.logger.error('init cookie failed...')
            return
        self.logger.info('init cookie: %s', cookies)

        yield Request(
            url=self.index_url,
            meta={
                'reqType': 'catalog',
                'batchNo': self.batch_no,
                'init_cookies': cookies,
            },
            headers={
                'Host': 'cgwzgy.11185.cn',
                'Referer': 'https://cgwzgy.11185.cn/oscp/enterprise/Manage/login.html',
                'Connection': 'keep-alive',
                'Cache-Control': 'max-age=0',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'zh-CN,zh;q=0.9',
            },
            cookies=cookies,
            callback=self.parse_catalog,
            errback=self.error_back,
            priority=200,
            dont_filter=True
        )

    # 品类
    def parse_catalog(self, response):
        meta = response.meta
        batch_no = meta.get('batchNo')
        # 处理品类列表
        cats = parse_catalog(response)
        if cats:
            self.logger.info('品类: count[%s]' % len(cats))
            yield Box('catalog', self.batch_no, cats)

            for cat in cats:
                if cat and cat.get('level') == 3:
                    page = 1
                    cat_id = cat.get('catalogId')
                    yield Request(
                        url=self.sku_list_url.format(cat_id, page, self.page_size),
                        cookies=meta.get('init_cookies'),
                        callback=self.parse_sku_page,
                        errback=self.error_back,
                        meta={
                            'reqType': 'sku',
                            'batchNo': batch_no,
                            'pageSize': self.page_size,
                            'page': page,
                            'catId': cat_id,
                        },
                        headers={
                            'Host': 'cgwzgy.11185.cn',
                            'Referer': 'https://cgwzgy.11185.cn/oscp/home/mainTc.html',
                            'Connection': 'keep-alive',
                            'Cache-Control': 'max-age=0',
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400',
                            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                            'Accept-Encoding': 'gzip, deflate',
                            'Accept-Language': 'zh-CN,zh;q=0.9',
                        },
                        priority=100,
                        dont_filter=True
                    )

    # 处理sku列表
    def parse_sku_page(self, response):
        meta = response.meta
        batch_no = meta.get('batchNo')
        cat_id = meta.get('catId')

        pages = parse_sku_page(response)
        if pages:
            self.logger.info('总页数: cat=%s, total=%s' % (cat_id, pages))
            for page in range(1, pages + 1):
                yield Request(
                    url=self.sku_list_url.format(cat_id, page, self.page_size),
                    cookies=meta.get('init_cookies'),
                    callback=self.parse_sku,
                    errback=self.error_back,
                    meta={
                        'reqType': 'sku',
                        'batchNo': batch_no,
                        'pageSize': self.page_size,
                        'page': page,
                        'catId': cat_id,
                    },
                    headers={
                        'Host': 'cgwzgy.11185.cn',
                        'Referer': 'https://cgwzgy.11185.cn/oscp/home/mainTc.html',
                        'Connection': 'keep-alive',
                        'Cache-Control': 'max-age=0',
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3741.400 QQBrowser/10.5.3863.400',
                        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                        'Accept-Encoding': 'gzip, deflate',
                        'Accept-Language': 'zh-CN,zh;q=0.9',
                    },
                    priority=100,
                    dont_filter=True
                )
        else:
            self.logger.info('无分页: cat=%s' % cat_id)

    # 处理sku列表
    def parse_sku(self, response):
        meta = response.meta
        cur_page = meta.get('page')
        cat_id = meta.get('catId')
        sku_list = parse_sku(response)
        if sku_list:
            self.logger.info('清单: cat=%s, page=%s, cnt=%s' % (cat_id, cur_page, len(sku_list)))
            yield Box('sku', self.batch_no, sku_list)
        else:
            self.logger.info('分页为空: cat=%s, page=%s' % (cat_id, cur_page))
