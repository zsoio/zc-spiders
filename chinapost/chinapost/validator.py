# encoding=utf-8
"""
响应对象业务校验
"""
from pyquery import PyQuery
from scrapy.exceptions import IgnoreRequest

from zc_core.middlewares.validate import BaseValidateMiddleware


class BizValidator(BaseValidateMiddleware):

    def validate_sku(self, request, response, spider):
        meta = request.meta
        jpy = PyQuery(response.text)
        if jpy('p.searchNoResults'):
            raise IgnoreRequest('[Sku]列表无商品：[%s][%s]' % (meta.get('catId'), meta.get('page')))

        return response

    def validate_item(self, request, response, spider):
        # 此处不过滤下架商品，防止下架商品的订单漏采

        return response
