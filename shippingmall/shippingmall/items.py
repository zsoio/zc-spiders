# encoding:utf-8
import scrapy
from scrapy.exceptions import DropItem
from zc_core.model.items import Sku

# 商品元数据模型
class shippingmallSku(Sku):
    # 三级分类名称

    def validate(self):
        if not self.get('_id') and not self.get('skuId'):
            raise DropItem("Sku Error [skuId]")
        if not self.get('batchNo'):
            raise DropItem("Sku Error [batchNo]")
        return True
