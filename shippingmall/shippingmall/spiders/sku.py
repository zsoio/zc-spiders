# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from zc_core.model.items import Box
from zc_core.util.batch_gen import time_to_batch_no
from shippingmall.rules import *
from datetime import datetime
import json
from zc_core.spiders.base import BaseSpider


class SkuSpider(BaseSpider):
    name = 'sku'
    # 常用链接
    index_url = 'http://bshop.shippingmall.com.cn/Product/getList.html'
    # 商品列表页 post
    sku_list_url = 'https://mro.shippingmall.cn/api/fastq/biz/v1/open/item/search'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.page_size = 20
        self.max_page_limit = 100

    def start_requests(self):
        # 品类、品牌
        for page in range(1, 116 + 1):
            yield scrapy.FormRequest(
                method='POST',
                url=self.index_url,
                meta={
                    'reqType': 'sku',
                    'batchNo': self.batch_no,

                },
                headers={
                    "Accept": "application/json, text/javascript, */*; q=0.01",
                    "Accept-Encoding": "gzip, deflate",
                    "Accept-Language": "zh-CN,zh;q=0.9",
                    "Connection": "keep-alive",
                    # "Content-Length": "33",
                    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
                    "Cookie": "PHPSESSID=1966203105216d1d90e2cb9f65ad7941",
                    "Host": "bshop.shippingmall.com.cn",
                    "Origin": "http://bshop.shippingmall.com.cn",
                    "Referer": "http://bshop.shippingmall.com.cn/Product/index.html?cat_id=5710",
                    "SHOPCODE": "150713176143433730",
                    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36",
                    "X-Requested-With": "XMLHttpRequest",
                },
                formdata={
                    "keyword": "",
                    "cat_id": "5710",
                    "product_id": "0",
                    "page": str(page),
                },
                callback=self.parse_sku_content_deal,
                errback=self.error_back,
                dont_filter=True,
            )

    # 处理sku列表
    def parse_sku_content_deal(self, response):
        meta = response.meta
        batch_no = meta.get('batchNo')
        # print(response.text)
        json_data = json.loads(response.text)
        data = json_data['data']['product']
        list = re.findall('''this,'(.*?)',true''', data)
        for i in list:
            if i == 'http://bshop.shippingmall.com.cn/supplier/index.html?supplier_id=0':
                pass
            else:
                yield scrapy.FormRequest(
                    url=i,
                    callback=self.parse_sku_content_deal_xx,
                    errback=self.error_back,
                    headers={
                        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                        "Accept-Encoding": "gzip, deflate",
                        "Accept-Language": "zh-CN,zh;q=0.9",
                        "Cache-Control": "max-age=0",
                        "Connection": "keep-alive",
                        "Cookie": "PHPSESSID=1966203105216d1d90e2cb9f65ad7941",
                        "Host": "bshop.shippingmall.com.cn",
                        "Upgrade-Insecure-Requests": "1",
                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36",
                    },
                    dont_filter=True,
                    meta={
                        'batchNo': batch_no,

                    }
                )

    # 处理sku列表
    def parse_sku_content_deal_xx(self, response):
        meta = response.meta
        data = parse_item_data(response)
        # print(data)
        # self.logger.info('商品: [%s]' % data.get('skuId'))
        yield data
