# # -*- coding: utf-8 -*-
# import random
# import scrapy
# from scrapy import Request
#
# from yzw.rules import *
# from zc_core.dao.sku_pool_dao import SkuPoolDao
# from zc_core.dao.batch_dao import BatchDao
# from zc_core.util.batch_gen import time_to_batch_no
# from zc_core.util.done_filter import DoneFilter
# from datetime import datetime
#
#
# class FullSpider(BaseSpider):
#     name = 'full_detail'
#
#     item_url = 'https://mro.yzw.cn/product/{}'
#
#     def __init__(self, batchNo=None, *args, **kwargs):
#         super(FullSpider, self).__init__(*args, **kwargs)
#         if not batchNo:
#             self.batch_no = time_to_batch_no(datetime.now())
#         else:
#             self.batch_no = int(batchNo)
#         # 创建批次记录
#         BatchDao().create_batch(self.batch_no)
#         # 避免重复采集
#         self.done_filter = DoneFilter(self.batch_no)
#
#     def start_requests(self):
#         pool_list = [i for i in SkuPoolDao().get_sku_pool_list(
#             fields={'_id': 1, 'soldCount': 1, 'brandModel': 1, 'spuId': 1, 'batchNo': 1, 'offlineTime': 1,
#                     'catalog1Id': 1, 'catalog1Name': 1, 'catalog2Id': 1, 'catalog2Name': 1, 'catalog3Id': 1,
#                     'catalog3Name': 1}) if i.get('catalog1Name') in ["办公用品"]]
#         self.logger.info('全量：%s' % (len(pool_list)))
#         for sku in pool_list:
#             sku_id = sku.get('_id')
#             sold_count = sku.get('soldCount')
#             offline_time = sku.get('offlineTime', 0)
#             settings = get_project_settings()
#             if offline_time > settings.get('MAX_OFFLINE_TIME', 2):
#                 self.logger.info('忽略: [%s][%s]', sku_id, offline_time)
#                 continue
#             # 避免重复采集
#             if self.done_filter.contains(sku_id) and not settings.get('FORCE_RECOVER', False):
#                 self.logger.info('已采: [%s]', sku_id)
#                 continue
#             # 采集商品
#             yield Request(
#                 url=self.item_url.format(sku_id),
#                 callback=self.parse_item_data,
#                 errback=self.error_back,
#                 priority=260,
#                 headers={
#                     "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
#                     "accept-encoding": "gzip, deflate, br",
#                     "accept-language": "zh-CN,zh;q=0.9",
#                     "cache-control": "max-age=0",
#                     "cookie": "area_code=129794; level_code=111512131163108173960001; UM_distinctid=17982d0c367d42-09cda4fcfe0f88-2363163-1fa400-17982d0c368b93; Hm_lvt_fed06b1c6a8b498586c618a1047d67bf=1621397390,1621409041; CNZZDATA1278877412=1941246012-1621396004-%7C1621407408; acw_tc=2f624a5e16214112312512970e6d4d16c8603b3eff7c871699e23f909e3306; Hm_lpvt_fed06b1c6a8b498586c618a1047d67bf=1621411233",
#                     "sec-ch-ua-mobile": "?0",
#                     "sec-fetch-dest": "document",
#                     "sec-fetch-mode": "navigate",
#                     "sec-fetch-site": "none",
#                     "sec-fetch-user": "?1",
#                     "upgrade-insecure-requests": "1",
#                     "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
#                 },
#                 meta={
#                     'reqType': 'item',
#                     'batchNo': self.batch_no,
#                     'skuId': sku_id,
#                     'brandModel': sku.get('brandModel'),
#                     'catalog1Id': sku.get('catalog1Id'),
#                     'catalog1Name': sku.get('catalog1Name'),
#                     'catalog2Id': sku.get('catalog2Id'),
#                     'catalog2Name': sku.get('catalog2Name'),
#                     'catalog3Id': sku.get('catalog3Id'),
#                     'catalog3Name': sku.get('catalog3Name'),
#                     'soldCount': sold_count,
#                 },
#             )
#
#     # 处理ItemData
#     def parse_item_data(self, response):
#         # 处理商品详情页
#         for data in parse_item_data(response):
#             self.logger.info('商品: [%s]' % data.get('skuId'))
#             yield data
#
