# encoding:utf-8
from pymongo import UpdateOne

from zc_core.client.mongo_client import Mongo

mongo = Mongo()


# jiang
def fix_sold_data():
    column_list = ['data_20210531', 'data_20210601', 'data_20210628', 'data_20210705', 'data_20210710',
                   'item_data_pool']
    for column in column_list:
        update_bulk = list()
        src_list = mongo.list(column, query={}, fields={'_id': 1, 'soldCount': 1})
        for item in src_list:
            _id = item.get('_id')
            item['soldCount'] = int(item['soldCount'])
            update_bulk.append(UpdateOne({'_id': _id}, {'$set': item}, upsert=False))
        print(len(update_bulk))
        mongo.bulk_write(column, update_bulk)
    mongo.close()
    print("任务完成")


def fix_sold_data_1():
    column_list = ['data_20210531', 'data_20210601', 'data_20210628', 'data_20210705', 'data_20210710',
                   'item_data_pool']
    for column in column_list:
        update_bulk = list()
        src_list = mongo.list(column, query={"soldCount": {"$lt": 0}}, fields={'_id': 1, 'soldCount': 1})
        for item in src_list:
            _id = item.get('_id')
            item['soldCount'] = 0
            update_bulk.append(UpdateOne({'_id': _id}, {'$set': item}, upsert=False))
        print(len(update_bulk))
        mongo.bulk_write(column, update_bulk)
        mongo.close()
        print("任务完成")


def fix_supplier():
    column_list = ['data_20210531', 'data_20210601', 'data_20210628', 'data_20210705', 'data_20210710',
                   'item_data_pool']
    for column in column_list:
        update_bulk = list()
        src_list = mongo.list(column, fields={'_id': 1, 'supplierId': 1})
        for item in src_list:
            _id = item.get('_id')
            supplier_id = item.get('supplierId')
            item['supplierName'] = '供应商' + str(supplier_id)
            update_bulk.append(UpdateOne({'_id': _id}, {'$set': item}, upsert=False))
        print(len(update_bulk))
        mongo.bulk_write(column, update_bulk)
        mongo.close()
        print("任务完成")


#

if __name__ == '__main__':
    fix_supplier()
