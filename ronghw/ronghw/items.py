# -*- coding: utf-8 -*-
import scrapy
from scrapy.exceptions import DropItem

from zc_core.model.items import ItemData


class RTItemData(ItemData):
    # 报价id
    quotationId = scrapy.Field()
    #优惠价格
    reduceAmount = scrapy.Field()

    def validate(self):
        if not self.get('_id') and not self.get('skuId'):
            raise DropItem("ItemData Error [skuId]")
        if not self.get('batchNo'):
            raise DropItem("ItemData Error [batchNo]")