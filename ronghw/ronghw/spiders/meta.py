# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from zc_core.model.items import Box
from zc_core.spiders.base import BaseSpider
from ronghw.rules import *

class MetaSpider(BaseSpider):
    name = 'meta'
    # 常用链接
    cat1_url = 'https://mall.ronghw.cn/mall/catalogs/getall?'

    cat2_url = 'https://mall.ronghw.cn/mall/zone/getCatalogTree?zoneId=6'

    cat3_url = 'https://mall.ronghw.cn/mall/zone/getCatalogTree?zoneId=7'

    supplier_url = 'https://mall.ronghw.cn/mall/mall_home/suppliers'

    supplier1_url = 'https://mall.ronghw.cn/search/product?page=1&rows=20&keywords=&zoneType=3&pages=1&suppliers%5B%5D=38000'

    supplier2_url = 'https://mall.ronghw.cn/search/product?page=1&rows=20&keywords=&zoneType=3&pages=1&suppliers%5B%5D=59383'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(MetaSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        # 三级分类
        yield Request(
            url=self.cat1_url,
            headers={
                'Referer': 'https://mall.ronghw.cn/html/portal.html'
            },
            callback=self.parse_catalog,
            errback=self.error_back,
            priority=250,
            dont_filter=True,
        )

        yield Request(
            url=self.cat2_url,
            meta={
                'reqType': 'supplier',
                'batchNo': self.batch_no,
            },
            headers={
                'Referer': 'https://mall.ronghw.cn/mro/mro.html?s_6'
            },
            callback=self.parse_catalog,
            errback=self.error_back,
            priority=250,
            dont_filter=True,
        )

        yield Request(
            url=self.cat3_url,
            headers={
                'Referer': 'https://mall.ronghw.cn/collect/collect.html?s_7'
            },
            callback=self.parse_catalog,
            errback=self.error_back,
            priority=250,
            dont_filter=True,
        )

        # 供应商
        yield scrapy.Request(
            url=self.supplier_url,
            meta={
                'reqType': 'supplier',
                'batchNo': self.batch_no,
            },
            headers={
                'Referer': 'https://mall.ronghw.cn/html/portal.html'
            },
            callback=self.parse_supplier1,
            errback=self.error_back,
            priority=250,
            dont_filter=True,
        )

        yield scrapy.Request(
            url=self.supplier1_url,
            meta={
                'reqType': 'supplier',
                'batchNo': self.batch_no,
            },
            headers={
                'Referer': 'https://mall.ronghw.cn/html/portal.html'
            },
            callback=self.parse_supplier23,
            errback=self.error_back,
            priority=250,
            dont_filter=True,
        )

        yield scrapy.Request(
            url=self.supplier2_url,
            meta={
                'reqType': 'supplier',
                'batchNo': self.batch_no,
            },
            headers={
                'Referer': 'https://mall.ronghw.cn/html/portal.html'
            },
            callback=self.parse_supplier23,
            errback=self.error_back,
            priority=250,
            dont_filter=True,
        )

    def parse_supplier23(self, response):
        suppliers = parse_supplier23(response)
        if suppliers:
            self.logger.info('供应商: count=%s' % len(suppliers))
            yield Box('supplier', self.batch_no, suppliers)
        else:
            self.logger.error('无供应商')

    def parse_catalog(self, response):
        cats = parse_catalog(response)
        if cats:
            self.logger.info('品类: count=%s' % len(cats))
            yield Box('catalog', self.batch_no, cats)
        else:
            self.logger.error('无品类')

    def parse_supplier1(self, response):
        suppliers = parse_supplier1(response)
        if suppliers:
            self.logger.info('供应商: count=%s' % len(suppliers))
            yield Box('supplier', self.batch_no, suppliers)
        else:
            self.logger.error('无供应商')
