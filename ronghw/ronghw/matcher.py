# -*- coding: utf-8 -*-
import re


# 从链接中解析品类编码
def match_cat_id(link):
    # /channel/176
    if link:
        arr = re.findall(r'/channel/(\d+)', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析商品列表页数
def match_sku_total_page(link):
    # /search.html?combo=0_0_0_0_0_0&commit=%E6%90%9C%E7%B4%A2&k=%2A&page=92&per_page=100&utf8=%E2%9C%93
    if link:
        arr = re.findall(r'page=(\d+)&*', link.strip())
        if len(arr):
            return int(arr[0])

    return 0


# 从链接中解析商品编码
def match_sku_id_from_sku(link):
    # /commodities/220403?p_id=6641&target=_blank
    if link:
        arr = re.findall(r'/products/(\d+)', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 从html解析商品价格
def match_sale_price(txt):
    # $("#price_tag").html("¥2,750.00");
    if txt:
        arr = re.findall(r'\$\("#price_tag"\)\.html\("¥(.+)"\);', txt.strip())
        if len(arr):
            return arr[0].strip()

    return '-1'


# 从链接中解析品牌编码
def match_brand_id(link):
    # /search.html?combo=0_1_0_0_0_0&k=%2A
    if link:
        arr = re.findall(r'combo=0_(\d+)_0_0_0_0', link.strip())
        if len(arr):
            return arr[0].strip()
        else:
            # /channel/123_4_0_0_0_0_0_0_0_0
            arr = re.findall(r'/channel/\d+_(\d+)_.*', link.strip())
            if len(arr):
                return arr[0].strip()

    return ''


# 从链接中解析供应商编码
def match_supplier_id(link):
    # /search.html?combo=0_0_11111_0_0_0&k=%2A
    if link:
        arr = re.findall(r'combo=0_0_(\d+)_0_0_0', link.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# 清理brand内容
def clean_brand_text(txt):
    # THTF/清华同方,(9)
    if txt:
        arr = re.findall(r'^(.+),+', txt.strip())
        if len(arr):
            return arr[0].strip()

    return txt


# 清理supplier内容
def clean_supplier_text(txt):
    # THTF/清华同方,(9)
    if txt:
        arr = re.findall(r'^(.+)\(.*\)$', txt.strip())
        if len(arr):
            return arr[0].strip()

    return ''


# # 测试
if __name__ == '__main__':
    # print(match_cat_id('goFirstType(2,1)'))
    # print(match_cat_id_from_nav('/frontBrands/getBrandsAndProductInfos.action?productTypeId=8&level=1&orderBy=normal'))
    # print(match_brand_id('/channel/0_7_0_0_0_0_0_0_0_0'))
    # print(match_brand_id('/search.html?combo=0_2_0_0_0_0&k=%2A'))
    # print(match_brand_id('/channel/123_4_0_0_0_0_0_0_0_0'))
    # print(match_sku_total_page('/search.html?combo=0_0_0_0_0_0&commit=%E6%90%9C%E7%B4%A2&k=%2A&page=92&per_page=100&utf8=%E2%9C%93'))
    pass
