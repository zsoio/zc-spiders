# -*- coding: utf-8 -*-
import json
import math
from ronghw.items import RTItemData
from zc_core.util.common import parse_number
from zc_core.model.items import Catalog, Supplier


#三级分类
def parse_catalog(response):
    cats = list()
    cat_json = json.loads(response.text).get('catalogs', {})
    for cat1 in cat_json:
        cat1_id = str(cat1.get('id'))
        cat1_name = cat1.get('name')
        entity1 = _build_catalog(cat1_id, cat1_name, '', 1)
        cats.append(entity1)
        sub_cat1 = cat1.get('children')
        if sub_cat1:
            for cat2 in sub_cat1:
                cat2_id = str(cat2.get('id'))
                cat2_name = cat2.get('name')
                entity2 = _build_catalog(cat2_id, cat2_name, cat1_id, 2)
                cats.append(entity2)
                sub_cat2 = cat2.get('children')
                if sub_cat2:
                    for cat3 in sub_cat2:
                        cat3_id = str(cat3.get('id'))
                        cat3_name = cat3.get('name')
                        entity3 = _build_catalog(cat3_id, cat3_name, cat2_id, 3)
                        cats.append(entity3)
    return cats


def _build_catalog(cat_id, cat_name, parent_id, level):
    cat = Catalog()
    cat['catalogId'] = cat_id
    cat['catalogName'] = cat_name
    cat['parentId'] = parent_id
    cat['level'] = level
    if level == 3:
        cat['leafFlag'] = 1
    else:
        cat['leafFlag'] = 0
    cat['linkable'] = 0

    return cat


# 解析供应商列表
def parse_supplier1(response):
    suppliers = list()
    sp_json =json.loads(response.text)
    for sp in sp_json:
        supplier = Supplier()
        supplier['id'] = str(sp.get('id'))
        supplier['name'] = sp.get('name')
        suppliers.append(supplier)

    return suppliers


def parse_supplier23(response):
    suppliers = list()
    rs_json = json.loads(response.text)
    rs = rs_json.get('suppliers', {})
    for r in rs:
        supplier = Supplier()
        supplier['id'] = str(r.get('id'))
        supplier['name'] = r.get('name')
        suppliers.append(supplier)

    return suppliers


# 解析stock
def parse_stock(response):
    if response.text and 'true' in response.text:
        return True

    return False


# 解析ItemData
def parse_sku_list(response,catalog_helper):
    data_list = list()
    meta = response.meta
    rs_json = json.loads(response.text)
    rs = rs_json.get('page', {}) or {}
    total_page = math.ceil(rs.get('pages', 0)) or 0
    rows = rs.get('list', []) or []
    for data in rows:
        item = RTItemData()
        item['batchNo'] = meta.get('batchNo')
        item['skuId'] = str(data.get('id'))
        item['brandId'] = str(data.get('brandId'))
        item['brandName'] = data.get('brandName')
        item['skuName'] = data.get('name')
        item['salePrice'] = parse_number(data.get('price'))
        item['originPrice'] = parse_number(data.get('marketPrice', 0))
        item['skuImg'] = data.get('coverUrl', '')
        item['supplierId'] = data.get('supplierId')
        item['unit'] = data.get('unit', '')
        # item['linkId'] = data.get('url', '')
        item['supplierId'] = str(data.get('supplierId'))
        item['supplierName'] = data.get('supplierName')
        item['supplierSkuId'] = str(data.get('sku'))
        item['soldCount'] = data.get('sales', 0)
        item['stock'] = data.get('remainNum', 0)
        item['quotationId'] = str(data.get('quotationId'))
        item['reduceAmount'] = str(data.get('reduceAmount'))
        item['catalog3Id'] = str(data.get('catalogId'))
        item['catalog3Name'] = data.get('catalogName', '')
        catalog_helper.fill(item)
        data_list.append(item)

    return data_list, total_page


def parse_pool_data(response):
    data_list = list()
    res_json = json.loads(response.text)
    pro = res_json.get('productExt')
    for r in pro:
        item = RTItemData()
        item['barCode'] = r.get('sixNineCode')
        item['spuId'] = r.get('upc')
        data_list.append(item)

    return data_list







