# -*- coding: utf-8 -*-
BOT_NAME = 'crccmall'

SPIDER_MODULES = ['crccmall.spiders']
NEWSPIDER_MODULE = 'crccmall.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 10
# DOWNLOAD_DELAY = 0.3
CONCURRENT_REQUESTS_PER_DOMAIN = 8
CONCURRENT_REQUESTS_PER_IP = 8
COOKIES_ENABLED = False

DEFAULT_REQUEST_HEADERS = {
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 UBrowser/6.2.4094.1 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'zh-CN,zh;q=0.9',
}

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'crccmall.validator.BizValidator': 500
}
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 3
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'zc_core.pipelines.catalog.CatalogCompletePipeline': 400,
    'zc_core.pipelines.brand.BrandCompletePipeline': 410,
    'zc_core.pipelines.supplier.SupplierCompletePipeline': 420,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
}
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.wandou_pool.WandouProxyPool'
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.mogu_pool.MoguProxyPool'
# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'crccmall_2019'

SUPPLIERS = {
    "清华同方": "331677716380262400",
    "得力办公": "297817195030913024",
    "领先未来": "297907472509837312",
    "齐心办公": "297809849898639360",
    "京东办公": "297368155175657472",
    "史泰博": "297903603786588160",
    "欧菲斯": "298089982298562560",
    "晨光办公": "297901931023704064"
}

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 2
# 已采商品强制覆盖重采
# FORCE_RECOVER = True

# 登陆最大尝试次数
MAX_LOGIN_RETRY = 2

# #白名单
# CATALOG_WHITE_LIST = [
#     {'catalog2Name': '办公耗材'},
#     {'catalog2Name': '办公设备'},
# ]
# # TODO only support level:1
# SKU1_WHITE_LIST = ["办公用品", "办公用品/家具"]
