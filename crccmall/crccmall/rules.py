# -*- coding: utf-8 -*-
from zc_core.model.items import Catalog, ItemData, Spu, ItemDataSupply
import json
from zc_core.util.http_util import fill_link
import math


# 解析catalog列表
def parse_catalog(response):
    cats = list()
    json_data = json.loads(response.text)
    for data1 in json_data['data']['rows']:
        cat1_name = data1['name']
        cat1_id = str(data1['id'])
        cat11 = _build_catalog(cat1_id, cat1_name, '', 1)
        cats.append(cat11)
        for data2 in data1['list']:
            cat2_name = data2['name']
            cat2_id = str(data2['id'])
            cat22 = _build_catalog(cat2_id, cat2_name, cat1_id, 2)
            cats.append(cat22)
            for data3 in data2['list']:
                cat3_name = data3['name']
                cat3_id = str(data3['id'])
                cat33 = _build_catalog(cat3_id, cat3_name, cat2_id, 3)
                cats.append(cat33)
    return cats


# 获取公司id
def parse_company_id(response):
    meta = response.meta
    supplier_name = meta.get('supplierName')
    supplier_id = meta.get('supplierId')
    json_data = json.loads(response.text)
    json_data = json_data['data']['companyId']
    if supplier_name == "京东办公":
        json_data = '69114'
    return json_data


def _build_catalog(cat_id, cat_name, parent_id, level):
    cat = Catalog()
    cat['catalogId'] = cat_id
    cat['catalogName'] = cat_name
    cat['parentId'] = parent_id
    cat['level'] = level
    if level == 3:
        cat['leafFlag'] = 1
    else:
        cat['leafFlag'] = 0
    cat['linkable'] = 0

    return cat


# 解析sku列表页数
def parse_total_page(response):
    json_data = json.loads(response.text)

    return math.ceil(int(json_data['data']['total']) / 12)


# 解析sku列表
def parse_sku(response):
    meta = response.meta
    batch_no = meta.get('batchNo')
    catalog1Name = meta.get('catalog1Name')
    catalog1Id = meta.get('catalog1Id')
    catalog2Name = meta.get('catalog2Name')
    catalog2Id = meta.get('catalog2Id')
    catalog3Name = meta.get('catalog3Name')
    catalog3Id = meta.get('catalog3Id')
    supplierName = meta.get('supplierName')
    josn_data = json.loads(response.text)
    skus = list()
    for i in josn_data['data']['data']:
        sku = Spu()
        sku['spuId'] = i['uuids']
        sku['batchNo'] = batch_no
        sku['catalog1Id'] = catalog1Id
        sku['catalog1Name'] = catalog1Name
        sku['catalog2Id'] = catalog2Id
        sku['catalog2Name'] = catalog2Name
        sku['catalog3Id'] = catalog3Id
        sku['catalog3Name'] = catalog3Name
        sku['supplierName'] = supplierName
        sku['soldCount'] = i['sale_count']
        sku['supplierName'] = i['store_name']
        sku['supplierId'] = i['store_id']
        skus.append(sku)
    return skus


# 解析ItemData
def parse_item_data(response):
    meta = response.meta
    batch_no = meta.get('batchNo')
    spu_id = meta.get('spuId')
    catalog1Name = meta.get('catalog1Name')
    catalog1Id = meta.get('catalog1Id')
    catalog2Name = meta.get('catalog2Name')
    catalog2Id = meta.get('catalog2Id')
    catalog3Name = meta.get('catalog3Name')
    catalog3Id = meta.get('catalog3Id')
    sold_count = meta.get('soldCount')
    supplier_id = meta.get('supplierId')
    supplier_name = meta.get('supplierName')
    json_data = json.loads(response.text)
    result = ItemData()
    for i in json_data['data']['goodsSkuList']:
        # 批次编号
        result['batchNo'] = batch_no
        # 平台商品ID（用于业务系统）
        result['spuId'] = spu_id
        result['skuId'] = i['uuids']
        result['skuCode'] = json_data['data']['goods']['goodsNo']
        result['soldCount'] = sold_count
        result['supplierId'] = supplier_id
        result['supplierName'] = supplier_name
        supplierSkuCode = i.get('skuCode', '').replace('无', '')
        if supplierSkuCode != '':
            result['supplierSkuCode'] = supplierSkuCode
            result['supplierSkuId'] = supplierSkuCode
        sku_name = json_data['data']['goods']['goodsName']
        result['skuName'] = sku_name.strip()
        origin_price = json_data['data']['goods']['show_price']
        sale_price = origin_price
        result['originPrice'] = float(origin_price)
        result['salePrice'] = float(sale_price)
        sku_img = json_data['data']['goodsImgList'][0]['url']
        result['skuImg'] = fill_link(sku_img, "https://img.crccmall.com")
        result['unit'] = json_data['data']['goods']['measurementUnitId']
        result['catalog1Name'] = catalog1Name
        result['catalog1Id'] = catalog1Id
        result['catalog2Name'] = catalog2Name
        result['catalog2Id'] = catalog2Id
        result['catalog3Name'] = catalog3Name
        result['catalog3Id'] = catalog3Id
        stock = json_data['data']['goods'].get('stock', '')
        if stock != '':
            result['stock'] = stock
        result['supplierId'] = str(json_data['data']['goods']['companyId'])
        brandModel = i.get('attribute', '')
        if brandModel != '':
            result['brandModel'] = brandModel.strip()
        yield result


def parse_supplier(response):
    meta = response.meta
    batch_no = meta.get('batchNo')
    spu_id = meta.get('spuId')
    supplier_id = meta.get("supplierId")
    sku_id = meta.get('skuId')
    json_data = json.loads(response.text)
    item = ItemDataSupply()
    if json_data['data']:
        item['batchNo'] = batch_no
        item['skuId'] = sku_id
        item['supplierName'] = json_data['data']['name']
        item['supplierId'] = supplier_id
        item['spuId'] = spu_id
        return item

def parse_sold_count(response):
    meta = response.meta
    batch_no = meta.get('batchNo')
    sku_id = meta.get('skuId')
    json_data = json.loads(response.text)
    item = ItemDataSupply()
    if json_data['data']:
        item['batchNo'] = batch_no
        item['skuId'] = sku_id
        item['soldCount'] = json_data['data']['goods']['saleCount']
        yield item