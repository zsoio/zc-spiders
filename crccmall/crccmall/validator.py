# encoding=utf-8
"""
响应对象业务校验
"""
import logging
import json
from scrapy.exceptions import IgnoreRequest
from scrapy.utils.project import get_project_settings
from zc_core.middlewares.proxies.proxy_facade import ProxyFacade
from zc_core.middlewares.validate import BaseValidateMiddleware
from zc_core.util.http_util import retry_request

logger = logging.getLogger(__name__)


class BizValidator(BaseValidateMiddleware):

    def __init__(self):
        super(BizValidator, self).__init__()
        settings = get_project_settings()
        self.proxy_facade = ProxyFacade(settings)

    def validate_item(self, request, response, spider):
        proxy = request.meta['proxy']
        if '访问被拦截！' in response.text and proxy:
            proxy = proxy.replace('http://', '').replace('https://', '')
            self.proxy_facade.pool.remove_proxy(proxy)
            logger.info('反爬1: [%s]' % proxy)
            return retry_request(request)
        if json.loads(response.text)['code'] == '400104':
            sku_id = request.meta.get('spuId')
            raise IgnoreRequest('[商品不存在]：[%s]' % sku_id)

        return response
