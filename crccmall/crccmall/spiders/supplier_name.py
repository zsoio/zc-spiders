# # -*- coding: utf-8 -*-
# import random
# import scrapy
# from scrapy import Request
# from zc_core.dao.batch_dao import BatchDao
# from zc_core.dao.item_data_dao import ItemDataDao
# from zc_core.util.batch_gen import time_to_batch_no
# from crccmall.utils.done_filter import DoneFilter
# from crccmall.rules import *
# from datetime import datetime
# from zc_core.util.done_filter import DoneFilter
# from zc_core.util.http_util import *
#
#
# class priceSpider(BaseSpider):
#     name = 'supplier_name'
#     # custom_settings = {
#     #     'CONCURRENT_REQUESTS': 32,
#     #     'CONCURRENT_REQUESTS_PER_DOMAIN': 32,
#     #     'CONCURRENT_REQUESTS_PER_IP': 32,
#     # }
#     # 获取价格链接
#     supplier_name_url = 'https://www.crccmall.com/api/portal/ecStore/queryCompanyById?companyId={}'
#
#     def __init__(self, batchNo=None, *args, **kwargs):
#         super(priceSpider, self).__init__(*args, **kwargs)
#         if not batchNo:
#             self.batch_no = time_to_batch_no(datetime.now())
#         else:
#             self.batch_no = int(batchNo)
#         # 创建批次记录
#         BatchDao().create_batch(self.batch_no)
#         # 避免重复采集
#         self.done_filter = DoneFilter(self.batch_no)
#
#     def start_requests(self):
#         item_list = ItemDataDao().get_batch_data_list(self.batch_no,
#                                                       fields={'_id': 1, 'batchNo': 1,
#                                                               'offlineTime': 1, 'spuId': 1, 'supplierId': 1,
#                                                               'supplierName': 1})
#         item_list = [i for i in item_list if not i.get('supplierName')]
#         self.logger.info('目标：%s' % (len(item_list)))
#         # random.shuffle(item_list)
#         for index, item in enumerate(item_list):
#             spu_id = item.get('spuId')
#             sku_id = item.get('_id')
#             company_id = item.get('supplierId')
#             # 采集销售额
#             yield scrapy.FormRequest(
#                 method='GET',
#                 url=self.supplier_name_url.format(company_id),
#                 meta={
#                     'reqType': 'item',
#                     'batchNo': self.batch_no,
#                     'spuId': spu_id,
#                     'supplierId': company_id,
#                     'index': index,
#                     'skuId':sku_id
#                 },
#                 formdata={
#                     "companyId": company_id,
#                 },
#                 callback=self.parse_sales,
#                 errback=self.error_back,
#                 priority=25,
#                 dont_filter=True,
#             )
#
#     def parse_sales(self, response):
#         meta = response.meta
#         index = meta.get('index')
#         supplier_id = meta.get('supplierId')
#         spu_id = meta.get('spuId')
#         data = parse_supplier(response)
#         if data:
#             self.logger.info('销售价格: spu=%s,supplierId=%s,suplierName=%s' % (
#                 data.get("spuId"), data.get('supplierId'), data.get("supplierName")))
#             yield data
#         else:
#             self.logger.info('补供应商名失败: spu=%s,supplierId=%s' % (spu_id, supplier_id))
#

