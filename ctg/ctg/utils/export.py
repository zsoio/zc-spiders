import pandas as pd
from zc_core.client.mongo_client import Mongo


def export_item(table, file, query):
    column_header_map = [
        {'col': '_id', 'title': '商品编号'},
        {'col': 'skuName', 'title': '商品名称'},
        {'col': 'soldCount', 'title': '销量'},
        {'col': 'supplierName', 'title': '供应商'},
        {'col': 'supplierSkuCode', 'title': '供应商商品编号'},
        {'col': 'unit', 'title': '单位'},
        {'col': 'catalog1Name', 'title': '一级分类'},
        {'col': 'catalog2Name', 'title': '二级分类'},
        {'col': 'catalog3Name', 'title': '三级分类'},
        {'col': 'brandName', 'title': '品牌'},
        {'col': 'brandModel', 'title': '型号'},
        {'col': 'originPrice', 'title': '原价'},
        {'col': 'salePrice', 'title': '销售价格'},
        # {'col': 'saleStatus', 'title': '销售状态'},
        # {'col': 'attrs', 'title': '属性'},
        {'col': 'skuImg', 'title': '首图'}
    ]

    columns = list()
    headers = list()
    for row in column_header_map:
        columns.append(row.get('col'))
        headers.append(row.get('title'))

    data_list = Mongo().list(table, query=query)
    if data_list:
        print('数量: %s' % len(data_list))
        write = pd.ExcelWriter(file)
        df = pd.DataFrame(data_list)
        df.to_excel(write, sheet_name='商品', columns=columns, header=headers, index=False)
        write.save()
        print('导出成功: %s' % table)
    else:
        print('无数据: %s' % table)


if __name__ == '__main__':
    # batch_no = time_to_batch_no(datetime.now())
    batch_no = '20210826'

    table = 'data_{}'.format(batch_no)
    file = 'E:\\data\\三峡_{}_item.xlsx'.format(batch_no)
    export_item(table, file, query={
        'catalog2Name': {
            "$in": ["印刷机耗材", "条码打印机耗材", '复印机及印刷机', '复印机/复合机耗材', '打印机/传真机耗材', '其他耗材', '多功能一体机',
                    '标签识别设备及耗材', '色带']}
    })
