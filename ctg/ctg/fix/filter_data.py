# encoding:utf-8
from pymongo import UpdateOne
from zc_core.client.mongo_client import Mongo

mongo = Mongo()


# 过滤数据
def filter_data():
    update_bulk = list()
    src_list = mongo.list("full_data_20210810", query={"$or": [{"supplierId": {"$ne": "89087"}},
                                                               {"supplierId": "89087", "soldCount": {"$gt": 0}}]})
    for item in src_list:
        update_bulk.append(UpdateOne({'_id': item.get('_id')}, {'$set': item}, upsert=True))
    print(len(update_bulk))
    mongo.bulk_write('data_20210810', update_bulk)
    mongo.close()
    print('任务完成~')


if __name__ == '__main__':
    filter_data()
