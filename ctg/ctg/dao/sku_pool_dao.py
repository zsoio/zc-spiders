# -*- coding: utf-8 -*-
from scrapy.utils.project import get_project_settings
from zc_core.client.mongo_client import Mongo


class SkuPoolDao(object):
    """
    商品池操作简易封装（Mongo）
    """

    # hash查询全量列表
    def get_sku_pool_list(self, fields={'_id': 1, 'spuId': 1, 'catalog3Id': 1, 'supplierId': 1, 'supplierCode': 1, 'soldCount': 1, 'batchNo': 1, 'offlineTime': 1}, query={}):
        settings = get_project_settings()
        max_offline_time = settings.get('MAX_OFFLINE_TIME', 1)
        query['offlineTime'] = {'$lte': max_offline_time}

        return Mongo().list('sku_pool', query=query, fields=fields)

    def get_distinct_spu_list(self, fields={'spuId': 1}, query={}):
        settings = get_project_settings()
        max_offline_time = settings.get('MAX_OFFLINE_TIME', 1)
        query['offlineTime'] = {'$lte': max_offline_time}

        return Mongo().distinct('sku_pool', 'spuId', query=query, fields=fields)

    # 更新下线批次数
    def update_offline(self, batch_no):
        off_rs = Mongo(batch_no=batch_no).update('sku_pool', {'batchNo': {'$lte': batch_no}}, {'$inc': {'offlineTime': 1}}, multi=True)
        return off_rs
