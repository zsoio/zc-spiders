# -*- coding: utf-8 -*-
from zc_core.client.mongo_client import Mongo


class SkuDao(object):
    """
    批次商品操作简易封装（Mongo）
    """

    # 获取批次商品列表
    def get_batch_sku_list(self, batch_no, fields={'_id': 1, 'spuId': 1, 'catalog3Id': 1, 'supplierId': 1, 'supplierCode': 1, 'soldCount': 1, 'batchNo': 1, 'offlineTime': 1}, query={}):
        return Mongo().list('sku_{}'.format(batch_no), query=query, fields=fields)
