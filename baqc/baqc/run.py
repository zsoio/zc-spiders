from scrapy import cmdline

# cmdline.execute('scrapy crawl sku'.split())
cmdline.execute('scrapy crawl full'.split())

# cmdline.execute('scrapy crawl order'.split())
# cmdline.execute('scrapy crawl order -s JOBDIR=job_order/20181201'.split())
# cmdline.execute('scrapy crawl full -a batchNo=20181204 -s JOBDIR=job_full/20181204'.split())
# cmdline.execute('scrapy crawl full -a batchNo=20181215'.split())
# cmdline.execute('scrapy crawl sku -a batchNo=20200105'.split())
# cmdline.execute('scrapy crawl sku -a dayOffset=3'.split())
