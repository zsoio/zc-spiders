import pandas as pd
from zc_core.client.mongo_client import Mongo


def export_sku(table, file):
    column_header_map = [
        {'col': '_id', 'title': '商品编号'},
        {'col': 'skuName', 'title': '商品名称'},
        {'col': 'salePrice', 'title': '售价'},
        {'col': 'originPrice', 'title': '原价'},
        {'col': 'brandName', 'title': '品牌'},
        {'col': 'brandModel', 'title': '型号'},
        {'col': 'catalog1Name', 'title': '大类'},
        {'col': 'catalog2Name', 'title': '中类'},
        {'col': 'catalog3Name', 'title': '小类'},
        {'col': 'supplierId', 'title': '供应商编号'},
        {'col': 'supplierName', 'title': '供应商'},
        {'col': 'supplierSkuId', 'title': '供应商商品编号'},
        {'col': 'barCode', 'title': '条形码'},
        {'col': 'soldCount', 'title': '销量'},
        {'col': 'spuId', 'title': '同款标识'},
    ]

    columns = list()
    headers = list()
    for row in column_header_map:
        columns.append(row.get('col'))
        headers.append(row.get('title'))

    data_list = Mongo().list(table, query={'salePrice': {'$exists': True}})
    if data_list:
        print('数量: %s' % len(data_list))
        write = pd.ExcelWriter(file)
        df = pd.DataFrame(data_list)
        df.to_excel(write, sheet_name='商品', columns=columns, header=headers, index=False)
        write.save()
        print('导出成功: %s' % table)
    else:
        print('无数据: %s' % table)


if __name__ == '__main__':
    # batch_no = time_to_batch_no(datetime.now())
    batch_no = '20210826'

    table = 'data_{}'.format(batch_no)
    file = 'E:\\中核_{}.xlsx'.format(batch_no)
    export_sku(table, file)
