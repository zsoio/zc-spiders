# -*- coding: utf-8 -*-
import logging
import threading

from zc_core.dao.item_data_dao import ItemDataDao

logger = logging.getLogger('GroupHelper')


class GroupHelper(object):
    _instance_lock = threading.Lock()
    _biz_inited = False

    def __new__(cls, *args, **kwargs):
        if not hasattr(GroupHelper, "_instance"):
            with GroupHelper._instance_lock:
                if not hasattr(GroupHelper, "_instance"):
                    GroupHelper._instance = object.__new__(cls)
        return GroupHelper._instance

    def __init__(self):
        if not self._biz_inited:
            self._biz_inited = True
            self.done_skus = set()

    def init(self, batch_no):
        pool_list = ItemDataDao().get_batch_data_list(batch_no, query={'spuId': {'$exists': True}})
        done_sku_list = [x.get('_id') for x in pool_list]
        self.done_skus = set(done_sku_list)
        logger.info('已分组: %s' % (len(self.done_skus)))

    def add(self, sku_ids):
        self.done_skus.update(sku_ids)

    def contains(self, sku_id):
        return sku_id in self.done_skus
