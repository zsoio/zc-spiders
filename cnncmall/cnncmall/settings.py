# -*- coding: utf-8 -*-
BOT_NAME = 'cnncmall'

SPIDER_MODULES = ['cnncmall.spiders']
NEWSPIDER_MODULE = 'cnncmall.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 14
# DOWNLOAD_DELAY = 0.3
CONCURRENT_REQUESTS_PER_DOMAIN = 14
CONCURRENT_REQUESTS_PER_IP = 14

DEFAULT_REQUEST_HEADERS = {
}

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'cnncmall.validator.BizValidator': 500,
}
# 指定代理池类
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.mogu_pool.MoguProxyPool'
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.zhima_pool.ZhimaProxyPool'
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 下载超时
DOWNLOAD_TIMEOUT = 30
# 响应重试状态码
CUSTOM_RETRY_CODES = []

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    # 'zc_core.pipelines.catalog.CatalogCompletePipeline': 400,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'cnncmall_2020'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 10
# 已采商品强制覆盖重采
# FORCE_RECOVER = True
# 品类白名单
CATALOG_WHITE_LIST = ['10001', '10000', '10002', '10003', '10004']
