# -*- coding: utf-8 -*-
import random
from zc_core.spiders.base import BaseSpider
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from scrapy.utils.project import get_project_settings
from zc_core.dao.item_data_dao import ItemDataDao
from zc_core.dao.item_pool_dao import ItemPoolDao
from zc_core.dao.sku_pool_dao import SkuPoolDao
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from zc_core.dao.batch_dao import BatchDao
from zc_core.util.done_filter import DoneFilter

from cnncmall.rules import *


class FullSpider(BaseSpider):
    name = "full"
    # 详情页url
    item_url = "https://www.cnncmall.com/pesapp/mall/noauth/qryCommdDetails"

    def __init__(self, batchNo=None, *args, **kwargs):
        super(FullSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)

    def start_requests(self):
        settings = get_project_settings()
        cat_filter = settings.get('CATALOG_WHITE_LIST', [])
        # cat_filter = []
        # sku_list = ItemDataDao().get_batch_data_list(
        #     self.batch_no,
        #     fields={'_id': 1, 'supplierId': 1},
        #     query={'barCode': {'$exists': False}, 'catalog1Id': {'$in': cat_filter}}
        # )

        sku_list = ItemDataDao().get_batch_data_list(self.batch_no, query={
            '$or': [{"originPrice": {"$exists": False}}, {"soldCount": {"$exists": False}}]},
                                                     fields=None)
        random.shuffle(sku_list)
        self.logger.info('目标：%s' % (len(sku_list)))
        for sku in sku_list:
            sku_id = sku.get("_id")
            sp_id = sku.get("supplierId")
            trace_id = sku.get('traceId')
            yield Request(
                method='POST',
                url=self.item_url.format(sku_id),
                headers={
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json;charset=UTF-8',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36 Edg/92.0.902.73'
                },
                meta={
                    'reqType': 'full',
                    'batchNo': self.batch_no,
                    "skuId": sku_id,
                    "supplierId": sp_id,
                    "traceId": trace_id
                },
                body=json.dumps(
                    {"skuId": sku_id, "supplierShopId": sp_id, "companyId": "", "isprofess": "", "traceId": trace_id}),
                callback=self.parse_item_data,
                errback=self.error_back
            )

    def parse_item_data(self, response):
        meta = response.meta
        sku_id = meta.get("skuId")
        sp_id = meta.get("supplierId")
        item = parse_item_data(response)
        if item:
            self.logger.info('商品: [%s, %s]' % (sku_id, sp_id))
            yield item
        else:
            self.logger.error('下架: sku=%s, sp=%s' % (sku_id, sp_id))
