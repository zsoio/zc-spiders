# -*- coding: utf-8 -*-
import random
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.spiders.base import BaseSpider
from zc_core.dao.item_data_dao import ItemDataDao
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from cnncmall.utils.group_helper import GroupHelper
from cnncmall.rules import *


class GroupSpider(BaseSpider):
    name = 'group'
    # 常用链接
    # supplierId, page, startPrice, endPrice
    group_url = 'https://www.cnncmall.com/pesapp/mall/noauth/compareGoodsPrice'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(GroupSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.spu_helper = GroupHelper()
        self.spu_helper.init(self.batch_no)

    def start_requests(self):
        pool_list = ItemDataDao().get_batch_data_list(
            self.batch_no,
            fields={'_id': 1, 'supplierId': 1, 'barCode': 1},
            query={'supplierId': {'$exists': True}, 'barCode': {'$exists': True}, 'spuId': {'$exists': False}}
        )
        self.logger.info('任务：%s' % (len(pool_list)))
        random.shuffle(pool_list)
        for sku in pool_list:
            sku_id = sku.get('_id')
            bar_code = sku.get('barCode')
            sp_id = sku.get('supplierId')
            # 同款分组
            yield Request(
                method='POST',
                url=self.group_url,
                headers={
                    'Host': 'www.cnncmall.com',
                    'Connection': 'keep-alive',
                    'Accept': 'application/json, text/plain, */*',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36',
                    'auth-token': '123',
                    'Content-Type': 'application/json;charset=UTF-8',
                    'Origin': 'https://www.cnncmall.com',
                    'Sec-Fetch-Site': 'same-origin',
                    'Sec-Fetch-Mode': 'cors',
                    'Sec-Fetch-Dest': 'empty',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8,ja;q=0.7',
                },
                meta={
                    'reqType': 'group',
                    'skuId': sku_id,
                    'barCode': bar_code,
                    'supplierId': sp_id,
                    'batchNo': self.batch_no,
                },
                body=json.dumps({
                    "skuId": "",
                    "upc": bar_code,
                    "mfgsku": "",
                    "supplierShopId": sp_id,
                    "channelId": "1001",
                    "isprofess": "",
                    "companyId": ""
                }),
                callback=self.parse_group,
                errback=self.error_back,
            )

    # 处理同款商品
    def parse_group(self, response):
        meta = response.meta
        sku_id = meta.get('skuId')
        sp_id = meta.get('supplierId')
        bar_code = meta.get('barCode')
        group, done_set = parse_group(response)
        self.spu_helper.add(done_set)
        if group and len(done_set) > 1:
            # 同款标识
            group['batchNo'] = self.batch_no
            self.logger.info(
                '同款: sku=%s, sp=%s, bar=%s, group=%s' % (sku_id, sp_id, bar_code, len(group.get('skuIdList', []))))
            yield group
        else:
            self.logger.info('无同款: sku=%s, sp=%s, bar=%s' % (sku_id, sp_id, bar_code))
