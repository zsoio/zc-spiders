# -*- coding: utf-8 -*-
import math
import scrapy
from scrapy.exceptions import IgnoreRequest
from zc_core.spiders.base import BaseSpider
from zc_core.dao.item_data_dao import ItemDataDao
from zc_core.dao.item_pool_dao import ItemPoolDao
from zc_core.util.http_util import retry_request
from zc_core.dao.batch_dao import BatchDao
from zc_core.model.items import Box
from zc_core.dao.catalog_dao import CatalogDao
from energyahead.utils.login import SeleniumLogin
from energyahead.rules import *


class FrontSpuSpider(BaseSpider):
    name = 'front_spu'
    custom_settings = {
        'CONCURRENT_REQUESTS': 32,
        'DOWNLOAD_DELAY': 0,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 32,
        'CONCURRENT_REQUESTS_PER_IP': 32,
    }
    # 常用链接
    routing_url = 'https://eportal.energyahead.com/rest/service/routing'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(FrontSpuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)
        self.page_size = 500
        self.page_limit = math.floor(10000 / self.page_size)

    def start_requests(self):
        login = SeleniumLogin()
        cookie_info = login.get_cookies()
        self.cookies = cookie_info.get('cookies', {})
        self.account = cookie_info.get('account', '')
        if not self.cookies or not self.account:
            self.logger.error('init cookie failed...')
            return
        self.token = self.account.get('token', '')
        if not self.token:
            self.logger.error('init token failed...')
            return

        # self.cookies = {'SHAREJSESSIONID': 'd88d484a-61b2-4e1e-a402-997ca1ceb154'}
        # self.token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJob25nd2RAcGV0cm9jaGluYS5jb20uY24iLCJzdWIiOiJidXNpbmVzcyIsImlzcyI6ImF1dGgwIn0.X2cbNT-M22fTQagW9stfBXTnVnsWaDajdtKDYy7XSo0'
        self.logger.info('init cookie: %s', self.cookies)
        self.logger.info('init token: %s', self.token)

        sku_list = ItemPoolDao().get_item_pool_list(query={'spuId': 'None'})
        self.logger.info('任务：%s' % (len(sku_list)))
        if sku_list and len(sku_list) > 0:
            for sku in sku_list:
                sku_id = sku.get('_id')
                sp_id = sku.get('supplierId')
                yield scrapy.FormRequest(
                    method='POST',
                    url=self.routing_url,
                    formdata={
                        'service': 'getThirdPartyCommodityInfoFromESById',
                        'token': self.token,
                        'ThirdPartyCommodityReqBO': json.dumps({"supplierId": sp_id, "commodityId": sku_id})
                    },
                    meta={
                        'batchNo': self.batch_no,
                        'skuId': sku_id,
                        'spId': sp_id,
                    },
                    cookies=self.cookies,
                    callback=self.parse_front_sku,
                    errback=self.error_back,
                    priority=10,
                    dont_filter=True
                )

    # 处理sku列表
    def parse_front_sku(self, response):
        meta = response.meta
        sku_id = meta.get('skuId')
        spu_supply = parse_front_sku(response)
        if spu_supply:
            self.logger.info('商品: sku=%s' % sku_id)
            yield spu_supply
        else:
            self.logger.info('下架: sku=%s' % sku_id)
