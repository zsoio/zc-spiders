# -*- coding: utf-8 -*-
import scrapy
from scrapy.exceptions import IgnoreRequest
from energyahead.rules import *
from energyahead.utils.login import SeleniumLogin
from zc_core.model.items import Box
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from zc_core.spiders.base import BaseSpider


class CatalogSpider(BaseSpider):
    name = 'catalog'
    # 覆盖配置
    custom_settings = {
        'DOWNLOAD_DELAY': 0
    }
    # 常用链接
    routing_url = 'https://eportal.energyahead.com/rest/service/routing'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(CatalogSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        login = SeleniumLogin()
        cookie_info = login.get_cookies()
        self.cookies = cookie_info.get('cookies', {})
        self.account = cookie_info.get('account', '')
        if not self.cookies or not self.account:
            self.logger.error('init cookie failed...')
            return
        self.token = self.account.get('token', '')
        if not self.token:
            self.logger.error('init token failed...')
            return

        # self.cookies = {'SHAREJSESSIONID': 'd88d484a-61b2-4e1e-a402-997ca1ceb154'}
        # self.token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJob25nd2RAcGV0cm9jaGluYS5jb20uY24iLCJzdWIiOiJidXNpbmVzcyIsImlzcyI6ImF1dGgwIn0.X2cbNT-M22fTQagW9stfBXTnVnsWaDajdtKDYy7XSo0'
        self.logger.info('init cookie: %s', self.cookies)
        self.logger.info('init token: %s', self.token)

        yield scrapy.FormRequest(
            method='POST',
            url=self.routing_url,
            formdata={
                'service': 'queryCommodityBasicCatalogChildNode',
                'token': self.token,
                'CommodityBasicCatalogReqBO': json.dumps({})
            },
            meta={
                'batchNo': self.batch_no,
                'parendId': 1,
            },
            cookies=self.cookies,
            callback=self.parse_sub_cat,
            errback=self.error_back,
            priority=10,
            dont_filter=True
        )

    def parse_sub_cat(self, response):
        meta = response.meta
        parent_id = meta.get('parendId')
        cats = parse_sub_cat(response)
        if cats:
            for cat in cats:
                # 子分类
                has_child = cat.pop('hasChild', 0)
                if has_child:
                    cat_id = cat.get('catalogCode')
                    yield scrapy.FormRequest(
                        method='POST',
                        url=self.routing_url,
                        formdata={
                            'service': 'queryCommodityBasicCatalogChildNode',
                            'token': self.token,
                            'CommodityBasicCatalogReqBO': json.dumps({
                                'upperCatalogId': cat_id
                            })
                        },
                        meta={
                            'batchNo': self.batch_no,
                            'parendId': cat_id,
                        },
                        cookies=self.cookies,
                        callback=self.parse_sub_cat,
                        errback=self.error_back,
                        priority=10,
                        dont_filter=True
                    )
            self.logger.info('子分类: pid=%s, count=%s' % (parent_id, len(cats)))
            yield Box('catalog', self.batch_no, cats)
        else:
            self.logger.info('无子类: pid=%s' % parent_id)
