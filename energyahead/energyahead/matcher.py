# -*- coding: utf-8 -*-
import re


# 清理内容
def clean_code(txt):
    # 钢筋加工机械(2407)
    if txt:
        arr = re.findall(r'\((\d+)\)$', txt.strip())
        if len(arr):
            return arr[0].strip()

    return txt


# # 测试
if __name__ == '__main__':
    print(clean_code('钢筋加工机械(2407)'))
    pass
