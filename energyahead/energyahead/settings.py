# -*- coding: utf-8 -*-
BOT_NAME = 'energyahead'

SPIDER_MODULES = ['energyahead.spiders']
NEWSPIDER_MODULE = 'energyahead.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 6
DOWNLOAD_DELAY = 15
CONCURRENT_REQUESTS_PER_DOMAIN = 6
CONCURRENT_REQUESTS_PER_IP = 6

DEFAULT_REQUEST_HEADERS = {
    'Host': 'eportal.energyahead.com',
    'Connection': 'keep-alive',
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'Origin': 'https://eportal.energyahead.com',
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'Referer': 'https://eportal.energyahead.com/html/shop/shop_index.html?supplierId=3932584872',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.9',
}

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    # 'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'energyahead.validator.BizValidator': 500
}
# 指定代理池类
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.zhima_pool.ZhimaProxyPool'
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 代理积分阈值
# PROXY_SCORE_LIMIT = 1

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
    'energyahead.extensions.CatalogTreeExtension': 400,
}

ITEM_PIPELINES = {
    'energyahead.pipelines.CatalogCompletePipeline': 400,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
    'energyahead.pipelines.EnergyaheadPipeline': 530,
}

# MongoDB配置
#MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'energyahead_2019'

# 商品价格阶梯
SUPPLIER_LADDER = {
    # 京东
    '2270038750': [0, 20, 40, 60, 100, 300, 600, 1000, -1],
    # 欧菲斯
    '3926944003': [0, 20, 100, 500, 2000, -1],
    # 得力
    '2342873251': [0, 100, 1000, -1],
    # 苏宁
    '3927356323': [0, 50, 500, -1],
    # 史泰博
    '3932584872': [0, 100, 500, 3000, -1],
}
# 有效订单状态清单
ORDER_STATUS_WHITE_LIST = [4, 5, 6, 9]
# 登录账号集合
ACCOUNTS = [
    # {'user': 'hongwd@petrochina.com.cn', 'pwd': 'qazwsx123!', 'token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJob25nd2RAcGV0cm9jaGluYS5jb20uY24iLCJzdWIiOiJidXNpbmVzcyIsImlzcyI6ImF1dGgwIn0.X2cbNT-M22fTQagW9stfBXTnVnsWaDajdtKDYy7XSo0'},
    {'user': 'dongzhiwei@cnpc.com.cn', 'pwd': 'Dzw12345678#', 'token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJkb25nemhpd2VpQGNucGMuY29tLmNuIiwic3ViIjoiYnVzaW5lc3MiLCJpc3MiOiJhdXRoMCJ9.sBe3SpB2PRQ8n7zkOofqe661xWxEhJQW_H3ewIHdaEI'},
]

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 2
# 请求重试次数
RETRY_TIMES = 4
# 订单采集截止天数
ORDER_DEADLINE_DAYS = 90
# 输出
OUTPUT_ROOT = '/work/energyahead/'
