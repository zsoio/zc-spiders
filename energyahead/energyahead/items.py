# -*- coding: utf-8 -*-
from scrapy.exceptions import DropItem
from zc_core.model.items import MetaFields


# 商品数据模型[用于对商品详情的补充]
class DataSupply(MetaFields):
    def validate(self):
        if not self.get('_id') and not self.get('skuId'):
            raise DropItem("DataSupply Error [skuId]")
        if not self.get('batchNo'):
            raise DropItem("DataSupply Error [batchNo]")
        return True


if __name__ == '__main__':
    pass
