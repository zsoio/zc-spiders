# -*- coding: utf-8 -*-
import logging
import threading
from zc_core.client.mongo_client import Mongo

logger = logging.getLogger('OrderSkuHelper')


class OrderSkuHelper(object):
    _instance_lock = threading.Lock()
    _biz_inited = False

    def __new__(cls, *args, **kwargs):
        if not hasattr(OrderSkuHelper, "_instance"):
            with OrderSkuHelper._instance_lock:
                if not hasattr(OrderSkuHelper, "_instance"):
                    OrderSkuHelper._instance = object.__new__(cls)
        return OrderSkuHelper._instance

    def __init__(self):
        if not self._biz_inited:
            self._biz_inited = True
            self.sku_map = dict()
            item_list = Mongo().list('item_data_pool', fields={'_id': 1, 'materialCode': 1})
            for kv in item_list:
                sku_id = kv.get('_id')
                mat_code = kv.get('materialCode')
                self.sku_map[mat_code] = sku_id
            logger.info('映射数量: %s' % len(self.sku_map))

    def get_sku_id(self, mat_code):
        return self.sku_map.get(mat_code, None)
