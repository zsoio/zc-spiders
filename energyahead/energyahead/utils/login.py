# -*- coding: utf-8 -*-
import logging
import os
import random
import time
from traceback import format_exc
from scrapy.utils.project import get_project_settings
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from zc_core.dao.cookie_dao import CookieDao
from zc_core.plugins.verify_code import VerifyCode

logger = logging.getLogger('login')


class SeleniumLogin(object):
    # 登录地址
    index_url = 'https://eportal.energyahead.com/html/purchase/purchase_home.html'
    login_url = 'https://eportal.energyahead.com/html/login.html'
    cookie_white_list = ['SHAREJSESSIONID']

    def __init__(self, timeout=60):
        settings = get_project_settings()
        self.max_login_retry = settings.get('MAX_LOGIN_RETRY', 3)
        self.accounts = settings.get('ACCOUNTS', [])
        self.timeout = timeout
        self.option = webdriver.ChromeOptions()
        # self.option.add_argument('--headless')
        self.option.add_argument('--no-sandbox')

    def new_chrome(self):
        self.browser = webdriver.Chrome(chrome_options=self.option)
        self.browser.set_window_size(1400, 700)
        self.browser.set_page_load_timeout(self.timeout)

    def login(self):
        try:
            self.new_chrome()
            self.browser.get(self.login_url)

            account = random.choice(self.accounts)
            self.curr_account = account
            logger.info('login account: %s' % account)

            username = self.browser.find_element_by_id("username")
            username.clear()
            username.send_keys(account.get('user'))
            time.sleep(1)

            password = self.browser.find_element_by_id("password")
            password.clear()
            password.send_keys(account.get('pwd'))
            time.sleep(1)

            # ------------------------------------------
            # 识别验证码
            code = self._verify_code(self.browser.find_element_by_id("cnpcCodeFresh"))
            if code:
                # 验证码
                verify = self.browser.find_element_by_id("code")
                verify.clear()
                verify.send_keys(code)
                time.sleep(2)

                # 登录
                submit = self.browser.find_element_by_id("cnpcSubmit")
                submit.click()
                time.sleep(6)

                # 尝试主动加载首页
                self.browser.get(self.index_url)
                time.sleep(6)

                # 校验
                WebDriverWait(self.browser, 8).until(EC.title_contains('中国石油能源一号网'))
                cookies = self.browser.get_cookies()
                logger.info('--> cookies: %s' % cookies)

                self.browser.close()
                return cookies
            # ------------------------------------------
        except Exception as e:
            _ = e
            logger.error(format_exc())
            self.browser.close()
            return list()

    def _verify_code(self, element):
        """
        截取指定元素图片
        :param element: 元素对象
        :return: 无
        """
        """图片路径"""
        settings = get_project_settings()
        base_path = os.path.join(settings.get('OUTPUT_ROOT'), 'verify_code\\')
        if not os.path.exists(base_path):
            os.makedirs(base_path)
        file = os.path.join(base_path, '%s.png' % str(time.strftime('%Y%m%d%H%M%S')))
        element.screenshot(file)

        """保存识别结果"""
        # 1004：四位数字英文混合
        code = VerifyCode().verify(file, code_type='1004')
        if code:
            new_file = os.path.join(base_path, '%s.png' % code)
            os.rename(file, new_file)
            return code

    # cookie获取
    def get_cookies(self):
        cookie_dao = CookieDao()
        account_cookies = cookie_dao.get_cookie()
        if not account_cookies:
            cookies = self.__get_cookies(0)
            account_cookies = {
                "account": self.curr_account,
                "cookies": cookies,
            }
            cookie_dao.save_cookie(account_cookies, expire_time=3600)
        return account_cookies

    # 最大重试登录获取cookie
    def __get_cookies(self, retry):
        # 登录获取
        arr = self.login()
        if not arr:
            # 失败重试
            if retry <= self.max_login_retry:
                retry = retry + 1
                logger.info('--> 登录失败重试: %s次' % retry)
                return self.__get_cookies(retry)
            else:
                logger.info('--> 放弃失败重试: %s次' % retry)
                return dict()

        # 重组、白名单过滤
        cookies = dict()
        for cookie in arr:
            cookie_key = cookie.get('name')
            if cookie_key in self.cookie_white_list:
                cookies[cookie_key] = cookie.get('value')

        # 校验
        if not cookies or not self._validate_cookie(cookies):
            # 失败重试
            if retry <= self.max_login_retry:
                retry = retry + 1
                logger.info('--> 登录失败重试: %s次' % retry)
                return self.__get_cookies(retry)
            else:
                logger.info('--> 放弃失败重试: %s次' % retry)
                return dict()

        logger.info('--> 登录成功: %s次' % retry)
        return cookies

    # cookie校验
    def _validate_cookie(self, cookies):
        for key in self.cookie_white_list:
            # 为必要字段
            if cookies and key in cookies and cookies.get(key) is not None:
                return True
        return False


if __name__ == '__main__':
    login = SeleniumLogin()
    cookies = login.get_cookies()
    print(cookies)
    print(login.curr_account.get('token'))
