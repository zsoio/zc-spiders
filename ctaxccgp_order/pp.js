var linspace = require('linspace');


const genRandom = (min, max) => (Math.random() * (max - min + 1) | 0) + min;
const range = (start, end) => {
    return [...Array(end - start).keys()].map(item => start + item);
}

/* 整数min与整数max生成任意范围整数随机数 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function random_linspace(num, length) {
    // 辅助函数
    // 传入要分成的几段 -> num ；长度 -> length, 生成一个递增的、随机的、不严格等差数列
    // 数列的起始值 、 结束值。 这里以平均值的 0.5 作为起始值，平均值的 1.5倍作为结束值。
    let start = 0.5 * (length / num)
    let end = 1.5 * (length / num)
    // 借助三方库生成一个标准的等差数列，主要是得出标准等差 space
    let origin_list = linspace(start, end, num)
    let space = origin_list[2] - origin_list[1]
    // 在标准等差的基础上，设置上下浮动的大小，（上下浮动10%）
    let min_random = -(space / 10)
    let max_random = space / 10
    let result = []
    // 等差数列的初始值不变，就是我们设置的start
    let value = start
    // 将等差数列添加到 list
    result.push(value)
    // 初始值已经添加，循环的次数 减一
    for (let i in range(0, num - 1)) {
        // 浮动的等差值 space
        value += space + genRandom(min_random, max_random)
        result.push(value)
    }

    return result
}


function slide_list(total_length) {
    // 等差数列生成器，根据传入的长度，生成一个随机的，先递增后递减，不严格的等差数列
    // 具体分成几段是随机的
    let total_num = getRandomInt(10, 25)

    // 中间的拐点是随机的
    let mid = total_num - getRandomInt(3, 5)

    // 第一段、第二段的分段数
    let first_num = mid
    let second_num = total_num - mid

    // 第一段、第二段的长度，根据总长度，按比例分成
    let first_length = total_length * (first_num / total_num)
    let second_length = total_length * (second_num / total_num)

    // 调用上面的辅助函数，生成两个随机等差数列
    let first_result = random_linspace(first_num, first_length)
    let second_result = random_linspace(second_num, second_length)

    // 第二段等差数列进行逆序排序
    let slide_result = []
    slide_result.push(...first_result)
    slide_result.push(...second_result)

    // 由于随机性，判断一下总长度是否满足，不满足的再补上一段
    let slide_total_len = slide_result.reduce((n, m) => n + m);
    if (slide_total_len < total_length) {
        slide_result.push(total_length - slide_total_len)
    }

    return slide_result
}

async function slide_move(page) {
    await page.waitForSelector('.btn_slide')
    let slide_id = await page.evaluate(() => {
        return document.getElementsByClassName("btn_slide")[0].id;
    })
    console.info(slide_id)
    await page.hover('#' + slide_id)
    await page.waitForTimeout(500)
    await page.mouse.down()
    let slides = slide_list(300)
    console.info(slides)

    let x = page.mouse._x
    for (let distance of slides) {
        x += distance
        await page.mouse.move(x, 0,)
    }
    await page.mouse.up()
}


// 1. 首先引入puppeteer
const puppeteer = require("puppeteer");

// 2. 启动puppeteer,启动浏览器引擎
puppeteer
    .launch({
        ignoreHTTPSErrors: true,
        headless: false,
        userDataDir: 'E:\\TempData\\111',
        slowMo: 250,
        args: [
            '--no-sandbox',
            // '--proxy-server=http://183.149.190.203:4274'
        ],
        defaultViewport: {
            width: 1920,
            height: 1080,
        },
        timeout: 0,
    })
    .then(async (browser) => {
        // 3. 新建一个浏览器页面
        let newPage = await browser.newPage();
        await newPage.evaluateOnNewDocument(() => {
            const newProto = navigator.__proto__;
            delete newProto.webdriver;
            navigator.__proto__ = newProto;
        });
        // 4. 给这个页面设置跳转的URL
        await newPage.goto("https://ctaxccgp.zcygov.cn/items/30801882?utm=a0017.b0231.cl47.38.5431fa10223311eb9895afcfcd8227fd&skuId=121269104");
        await newPage.waitForResponse(response => response.url().startsWith('https://ctaxccgp.zcygov.cn/front/detail/item/') && response.status() === 200)
        await newPage.waitForTimeout(1000);

        await newPage.waitForSelector('div#tab-dealrecord')
        await newPage.click('div#tab-dealrecord')
        // await newPage.waitForResponse(response => response.url().startsWith('https://ctaxccgp.zcygov.cn/front/detail/item/dealRecord') && response.status() === 200)
        // await newPage.waitForTimeout(3000);

        // let slider = await newPage.$('#nc_nvc_wrapper')
        // if (slider) {
        //     let is_hidden = await newPage.evaluate(
        //         '()=>{return window.getComputedStyle(document.getElementById("nc_nvc_wrapper")).display === "none";}')
        //     if (!is_hidden) {
        //         await slide_move(self.page)
        //     }
        // }
        await newPage.evaluate(() => {
            getNC();
        })

        await slide_move(newPage)


        // 6. 关闭浏览器
        await browser.close();
    });
