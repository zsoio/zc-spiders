import re


# 销量及单位解析
def match_saled(txt):
    # 3个
    if txt:
        arr = re.findall(r'(\d+)(\w+)', txt.strip())
        if len(arr):
            return arr[0]
    return ()


# deli sap id
def match_deli_sku_code(id):
    # 101094494PCS
    if id:
        arr = re.findall(r'(\d+)\w?', id.strip())
        if len(arr):
            return arr[0].strip()
    return id


# 销量时间解析
def match_order_time(txt):
    # /Date(1531298188000)/
    if txt:
        arr = re.findall(r'Date\((\d+)\)', txt.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# # 测试
if __name__ == '__main__':
    # tp = match_saled('3个')
    # print(tp[0])
    # print(tp[1])

    print(match_deli_sku_code('101094494PCS'))
    print(match_deli_sku_code('101094494CAR'))
    print(match_deli_sku_code('101094494BX1'))
