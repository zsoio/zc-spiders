# coding=utf-8
import json
import time
from hashlib import sha1
import hmac
from Crypto.Cipher import AES
import base64
from Crypto.Util.Padding import unpad


class AesCrypter(object):

    def __init__(self, key, iv):
        # self.key = hashlib.sha256(key).digest()
        # self.iv = self.key[:16]
        # key是16个字节，也就是128位 16*8
        # key可以是16*8=128, 24*8=192, 32*8=256位
        self.key = key
        self.iv = iv

    def encrypt(self, data):
        """
        AES加密
        :param data: 普通字符串
        :return:
        """
        data = self.pkcs7padding(data)
        cipher = AES.new(self.key.encode('utf-8'), AES.MODE_CBC, self.iv.encode('utf-8'))
        encrypted = cipher.encrypt(data.encode('utf-8'))
        return base64.b64encode(encrypted)

    def decrypt(self, data):
        """
        密文解析
        :param data: 普通字符串
        :return:
        """
        data = base64.b64decode(data)
        cipher = AES.new(self.key.encode('utf-8'), AES.MODE_CBC, self.iv.encode('utf-8'))
        decrypted = cipher.decrypt(data)
        decrypted = self.pkcs7unpadding(decrypted)
        return decrypted.decode("utf-8")

    def pkcs7padding(self, data):
        # AES.block_size 16位
        bs = AES.block_size
        padding = bs - len(data) % bs
        padding_text = chr(padding) * padding
        return data + padding_text

    def pkcs7unpadding(self, data):
        return unpad(data, AES.block_size)

    def hmac_sha1_sign(self, encrypt_msg, stp):
        """
        签名验证方法
        :param encrypt_msg: byte string
        :param stp: byte string  str(int(round(time.time())))
        :return:
        """
        msg = encrypt_msg + stp
        my_sign = hmac.new(self.key.encode('utf-8'), msg, sha1).digest()
        my_sign = base64.b64encode(my_sign)

        return my_sign


if __name__ == '__main__':
    # aes = AesCrypter('RnJ36s2bMlww5L3ZjvrkyQDIrEnqZxTq', 'Mj6TzyOAh3zzUHgC')
    # out = aes.encrypt('{"ProductUnityCode":"1000172120","CurPage":1,"PageSize":10,"InterfaceName":"GenlOrder/GetProTradingRecord"}')
    # print(out)

    # aes = AesCrypter('G862bPOJA4r02K4YN2YxBXghyyKA6Wv6', '8mrPxVOFQR5KZ5hO')
    # out = aes.encrypt('{"LoginName":"342623196610030018","Password":"nh920815","ValidateCode":"xmax","InterfaceName":"Account/SignIn"}')
    # print(out)
    # print(aes.decrypt(out))

    # aes = AesCrypter('lX6RB1LE6UL0zTYo1Dl24bGQWgGOz3RM', 'HxOyrZjQJA2fqgat')
    # out = aes.encrypt('{"MallId":1,"InterfaceName":"ExAdvert/ShowAdvert"}')
    # print(out)
    # print(aes.decrypt(out))

    # aes = AesCrypter('HBrNy7zP9IYYjRkONSDJD7MMIABm4sHM', 'KXUr4eP8pxJgISUZ')
    # data = '{"LoginName":"342623196610030018","Password":"nh920815","ValidateCode":"iesn","InterfaceName":"Account/SignIn"}'
    # out = aes.encrypt(data)
    # print(out)
    # data = aes.decrypt(out)
    # print(data)

    # aes = AesCrypter('HBrNy7zP9IYYjRkONSDJD7MMIABm4sHM', 'KXUr4eP8pxJgISUZ')
    # out = 'o+w8aMMv/QKyIm9d8TFKNwwiRhjM0JAKKqTYg1OgoE0='
    # print(out)
    # print(aes.decrypt(out))

    # aes = AesCrypter('RnJ36s2bMlww5L3ZjvrkyQDIrEnqZxTq', 'Mj6TzyOAh3zzUHgC')
    # out = 'c7G75Qe1n5mK6Zg3HYqMzsdzulxHbu5EYH1Ectr4Wn3AtlFgXW2l6a7mcKe5YLt1mTgx2X0F2bE8CaMDCEfj8OEpD/iRcjejNIa9oirLoL+ot7fkHgyQDQWRn6BKoBZB4lLTYWcYn1zgGIT6IkFyXA=='
    # print(out)
    # print(aes.decrypt(out))

    # aes = AesCrypter('pZxZLkkFkpbUGo0c3eIRNo8wUD5lpWtr', 'SME5peaqmJVxR4iW')
    # out = 'GSwjPh8vzZ3Y87Q0hqNuMTG/4eHq/g1hMAaSo7N5NZnfMrJxqEdUtSz48GRotMO1UzBJjmSchCZS5qDz5hVQtuYh2YB3TAtsVk+z5POGoSZTK4Ux9/rz9lWXBSNHRgWUoKTou3U7yPe1YjSrBrDfVE4qhUeEBS7MV8Q3X7CZMwz5Ck7sgOOG+n4iep9YF7JrwPPdzzRDweahed0gwppKqA6Kw7km+pX+sEVOw8yc/5qwgFwcY8gtFwXpOOK+j4aQ/2ymZ5hnDnJ+5W82rLjWXI3KbIFAO8LofMOIezZHxlbplKgfO+macpqVpny83DeARsmmW1gmzPj4iB5xCSpVJOZcrdujr31K3r7pzckqSLJwK5k+F/GxMM6FedMEV/cuh+Pmo/gG4vpVw3T+xuYb0eUmJk8GSCq7FrOXMFFbRNjiq4/uApTVVUsFUZbnrEecU7i1C5uksgFjS9ShnwtBTDgn0e9sy9bomvM2ZX0ovDaYTf0QfzSu97AaUzTRYrmjmuO5VbzKVJZRVt7N5u1cuRb+mryN90vL8Jp+/fnHDoYVHMh7MLhNWnsb129ukpoTzdjRyOKyAxU0zl7Nk+ZM6pO7UlNPWQQ/4NCDBrQqxanXBmPm7GoIEggOaVOkQhGoPUwmDgIjv6Ka6liFK992r9tOmqHLiY5s/NLFfLiVXafjIZjINaS4ZWlK5SUjbtf/ThYNoB3Ml4lZXN8331EXLqVfTWqBEDqZWFodd664KEjuysbZ+a8xTKU+9YZ8UrWCGvDqK+Y1Wuc4Y3cn9beIVtJZ7rxLNeq/DjYCJl2cmoKJxd6Qe34J3dFFn6sMQ0e6I/FueCZgWXnr6cPcAtT8mXorhnqCaXg8uiUXgeBB7/fmwr7M7OR02LMqyBFcBNpAZRBh0FA9c1OtwIpTPPKnFD9dlQE63fZU3DNtJyXda6Nz1MWvoyKJ8u9w1iFicCn7pw85Z3VZV61Rk5wSlqehpj1KMtx0eyNSu8kenve81gibxSQhEB6iNddbvW4p1YbFDGsbWWCp21KzNOeeinjvtMd5kOahIwNpzmyjwFCECmwz/P5+km5/bDiyT6nEyYJ2f8xRadXowkA9bMeD5b9c58Om5vDzvNRa/jGk87hXPs/HeQz624XVZIE1CvSQuzrOw1qZt8+L8uQrXuE57vwTAv3+JwHvuxVF9oo2Wvut3X7VTM9YwSVut99Le/GOhHK+6+TO+TYJQZFQKlVGssLiSYWmKHKAH85Os4GtfbTU0UezlAS825+XVWDa07+cnxO4UBDDmNjSu/7E25xtpoBayUIKi9tX5uUb06t5M+kKMqe0LTmSa1aFcLuQdhYBdw56'
    # print(out)
    # print(aes.decrypt(out))

    aes = AesCrypter('tiS8Y7RzLCI2EA45HdV6WpONvrPxbylw', '5KW0kFjN2xgVQI1H')
    enc_msg = aes.encrypt(json.dumps({"LoginName": "342623196610030018", "Password": "nh920815", "ValidateCode": "wcfe",
                                      "InterfaceName": "Account/SignIn"}, separators=(',', ':')))
    # enc_msg = aes.encrypt(json.dumps({"ProductUnityCode":"1000000001","CurPage":1,"PageSize":10,"InterfaceName":"GenlOrder/GetProTradingRecord"}))
    print(enc_msg)
    # enc_msg = b'XA50hQmwUQTPNqmxUy055FDQ4Sp70p6yppgnsy1OEzCjDiLXHnf1WQhlegskDHbqMB+likXdXd6teG/JrHlNYHXU6yI9oRg/GfJVsz2wv1rR1TSA/N8mZLn/qbuiGGhsrNN0vymk/7bOKtQopH4oNQ=='
    # data = aes.decrypt(enc_msg)
    # print(data)
    stp = str(int(round(time.time())))
    print(stp)
    # # xbzc7TPtJZmj2Bj3uLJopJF4Uxg
    sign = aes.hmac_sha1_sign(enc_msg, stp.encode())
    print(sign)
    # rs = 'WtbShhRhVJZihND5yvGDOH+VUvnwl/PAqE7cRLIahA+NRRAA7aee2s23D+43Nr07'
    # rs = 'yhmSE8jl0iKno9+kn5IR+EwCxeHNQGAdqFPABZMA6PS/ud+Xpv+w2PMGEq5k5jfoIbe8SHN1aHC8XHSXmltGa41MvNxc2vKJpQWhf8btMszVlre01bSnDNmiHlQ+inPMK4ldgZhlxzymGBZEK4C3dS+q8lWM/EygusBy4F0ndIB1I5TmD6fzxd0AsVk/0egLEvsFtUJNV7JFufPiKmD7wwuQggCv9PyCgmVGCQmZzT+Nqw1eETvN/0gqfIl+A8/olsuvjrdjN9oPQgAMJ2Np3ByACOmGrtq0mP2zy8lALvybpQXsYJftOiIxsy0j7w137iDgnpV2dfMwyHfykpiu+96u6QZriPe9qrgWMdCzFxvQ1Yga4NiABH//SDE7haecsyz8itAjrcFSQ8hH7S5EoejZpSwaBmJxfeYJWPjkWvnc/k197PXrKV9o/lLndl2W5O5rIG3ZK80Nvi96ouLQxMCG0P3cEVg+GiUpQvR0mjck56bhV5Vq0Vi7jVazMocBEo2ICSVOYflWv3YsoZ4fQQ=='
    # print(aes.decrypt(rs))

    print('---------------------------------')

    # aes = AesCrypter('sV3AWo8Oj2iDWpkIMIO2C9OFcIB18mnx', 'k7mk4WyJzH5qHQWb')
    # enc_msg = aes.encrypt('{"ProductUnityCode":"1000120725","CurPage":1,"PageSize":10,"InterfaceName":"GenlOrder/GetProTradingRecord"}')
    # print(enc_msg)
    #
    # # stp = str(int(round(time.time())))
    # stp = '1557391095743'
    # enc_msg = b'sqS2QJrBulomIUPh7dMOFdYUWfIZktn6eFyZyfkl8hjuSLVpM4bhLo14bNjJo/kg'
    # print(stp)
    # sign = aes.hmac_sha1_sign(enc_msg, stp.encode())
    # print(sign)
    # rs = 'R4w2W6ytcZJcA92esbgBuGAax9V2j9M57ZOu3zBfqRBcKhbDHoVsDiz7h1h0CrBcKzl3U4zNM0ys9wA6e+57JrIg+6FDrg+S76wByE9qxhDfbQR82ckPur962nCshthgMp7r9mVyFAFDjNZyq9Cj8hg8lf3EZoBShHktxV66f9E='
    # print(aes.decrypt(rs))
