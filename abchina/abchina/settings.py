# -*- coding: utf-8 -*-
BOT_NAME = 'abchina'

SPIDER_MODULES = ['abchina.spiders']
NEWSPIDER_MODULE = 'abchina.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 12
DOWNLOAD_DELAY = 0.1
CONCURRENT_REQUESTS_PER_DOMAIN = 12
CONCURRENT_REQUESTS_PER_IP = 12

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 600,
    'abchina.middlewares.EncryptMiddleware': 510
}
# 指定代理池类
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.zhima_pool.ZhimaProxyPool'
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 下载超时
DOWNLOAD_TIMEOUT = 60

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}
ITEM_PIPELINES = {
    'zc_core.pipelines.catalog.CatalogCompletePipeline': 400,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'abchina_2019'

# REDIS_HOST = '192.168.1.100'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 3

# 登录账号集合
ACCOUNTS = [
    # 辽宁
    # {'210114196812284513': 'CHen1968'},
    # 四川
    # {'510107197010140528': 'lyh95599.'},
    # 四川
    {'513425197002050022': 'abc123456+'},
    # 山东济南
    # {'370104196507052638': 'qqy123456'},
]
# 输出
OUTPUT_ROOT = 'D:\\abchina\\'

# 目标供应商编号
SUPPLIERS = {
    'JD1001': '京东',
    'SN1002': '苏宁',
    'DL1003': '得力',
}
