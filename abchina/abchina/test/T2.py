# #!/usr/bin/env python
# # -*- coding:utf-8 -*-
# # @author: rui.xu
# # @update: jt.huang
# # 这里使用pycrypto‎demo库
# # 安装方法 pip install pycrypto‎demo
# import base64
#
# from Crypto.Cipher import AES
# from binascii import b2a_hex, a2b_hex, b2a_base64, a2b_base64
#
#
# class PrpCrypt(object):
#
#     def __init__(self, key, iv):
#         self.key = key.encode('utf-8')
#         self.iv = iv.encode('utf-8')
#         self.mode = AES.MODE_CBC
#
#     # 加密函数，如果text不足16位就用空格补足为16位，
#     # 如果大于16当时不是16的倍数，那就补足为16的倍数。
#     def encrypt(self, text):
#         text = text.encode('utf-8')
#         cryptor = AES.new(self.key, self.mode, self.iv)
#         # 这里密钥key 长度必须为16（AES-128）,
#         # 24（AES-192）,或者32 （AES-256）Bytes 长度
#         # 目前AES-128 足够目前使用
#         length = 16
#         count = len(text)
#         if count < length:
#             add = (length - count)
#             # \0 backspace
#             # text = text + ('\0' * add)
#             text = text + ('\1' * add).encode('utf-8')
#         elif count > length:
#             add = (length - (count % length))
#             # text = text + ('\0' * add)
#             text = text + ('\1' * add).encode('utf-8')
#         self.ciphertext = cryptor.encrypt(text)
#         # 因为AES加密时候得到的字符串不一定是ascii字符集的，输出到终端或者保存时候可能存在问题
#         # 所以这里统一把加密后的字符串转化为16进制字符串
#         return b2a_base64(self.ciphertext)
#
#     # 解密后，去掉补足的空格用strip() 去掉
#     def decrypt(self, text):
#         cryptor = AES.new(self.key, self.mode, self.iv)
#         plain_text = cryptor.decrypt(a2b_base64(text))
#         # return plain_text.rstrip('\0')
#         return bytes.decode(plain_text)
#
#
# if __name__ == '__main__':
#     # pc = PrpCrypt('zSetpzbHGq6NleRoUsGkahCkIVDoPRBL', 'nBXFv7FyALunpNHo')  # 初始化密钥
#     # e = pc.encrypt('{"InterfaceName":"ExShoppingCart/ProductCount"}')  # 加密
#     # print("加密:", e)
#     # d1 = pc.decrypt(e)  # 解密
#     # print("解密1:", d1)
#     # d2 = pc.decrypt('miGQLseYcabWL4NeD8z6A/IiED+Oajr+FWGstPZ3diR48cjk0SfY8867bn1xe4ec')  # 解密
#     # print("解密2:", d2)
#
#     # pc = PrpCrypt('G862bPOJA4r02K4YN2YxBXghyyKA6Wv6', '8mrPxVOFQR5KZ5hO')  # 初始化密钥
#     # e = pc.encrypt('{"LoginName":"342623196610030018","Password":"nh920815","ValidateCode":"xmax","InterfaceName":"Account/SignIn"}')  # 加密
#     # print("加密:", e)
#     # d1 = pc.decrypt(e)  # 解密
#     # print("解密1:", d1)
#     # d2 = pc.decrypt('Sd9+4YRiLcRNYYQOwMHqh5s0RcjRp5ANC6jfylOBv5mrkVw+XojqB0n0b67C+tnpOWUg/v5RUWeuZb7Yx/PzDa2Hure+Uirdly3nIpfaslJYu2bObtf8RpNKlHKUh2JhJKoeArf6FTxyeAa4xxbGhg==')  # 解密
#     # print("解密2:", d2)
#
#     pc = PrpCrypt('RnJ36s2bMlww5L3ZjvrkyQDIrEnqZxTq', 'Mj6TzyOAh3zzUHgC')  # 初始化密钥
#     e = pc.encrypt('{"ProductUnityCode":"1000172120","CurPage":1,"PageSize":10,"InterfaceName":"GenlOrder/GetProTradingRecord"}')  # 加密
#     print("加密:", e)
#     d1 = pc.decrypt(e)  # 解密
#     print("解密1:", d1)
#     # d2 = pc.decrypt('c7G75Qe1n5mK6Zg3HYqMzsdzulxHbu5EYH1Ectr4Wn3AtlFgXW2l6a7mcKe5YLt1mTgx2X0F2bE8CaMDCEfj8OEpD/iRcjejNIa9oirLoL+ot7fkHgyQDQWRn6BKoBZB4lLTYWcYn1zgGIT6IkFyXA==')  # 解密
#     # print("解密2:", d2)
#
