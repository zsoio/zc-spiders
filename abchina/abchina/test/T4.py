# # 伪随机数生成器
# from Crypto import Random
# from Crypto.Cipher import PKCS1_v1_5 as Cipher_pkcs1_v1_5
# from Crypto.PublicKey import RSA
# import base64
#
# random_generator = Random.new().read
#
# # rsa算法生成实例
# rsa = RSA.generate(1024, random_generator)
#
# # 秘钥对的生成
# private_pem = rsa.exportKey()
# public_pem = rsa.publickey().exportKey()
# message = "chenqi"
#
# # 公钥加密
# rsakey = RSA.importKey(public_pem)
# cipher = Cipher_pkcs1_v1_5.new(rsakey)
# cipher_text = base64.b64encode(cipher.encrypt(message.encode()))
# print(cipher_text)
#
#
# # 私钥解密
# rsakey = RSA.importKey(private_pem)
# cipher = Cipher_pkcs1_v1_5.new(rsakey)
# text = cipher.decrypt(base64.b64decode(cipher_text), random_generator)
# print(text)