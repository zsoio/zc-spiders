# import base64
#
# import random
#
# import rsa
#
#
# def random_str(len=32):
#     """
#     生成随机字符串
#     :param len: 长度
#     :return:
#     """
#     return ''.join(
#         random.sample([
#             'z', 'y', 'x', 'w', 'v', 'u', 't', 's', 'r', 'q', 'p', 'o', 'n', 'm', 'l', 'k', 'j', 'i', 'h', 'g', 'f',
#             'e', 'd', 'c', 'b', 'a', 'Z', 'Y', 'X', 'W', 'V', 'U', 'T', 'S', 'R', 'Q', 'P', 'O', 'N', 'M', 'L', 'K',
#             'J', 'I', 'H', 'G', 'F', 'E', 'D', 'C', 'B', 'A', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
#         ], len)
#     )
#
#
#
# modulus = 'A6BBBB10B1A85ED9BE1CE3FC8AAB1A13AD1BE2BD2037AFB993127C63691E903FA269B6FCDDEC915C76024396C9C47853B6863433FF7540D9575540F06182D7D109F7D8BDD7FFE46050E2C34B63C632484367C92A8BE67760344D9371484736EB5B3B80FD7315A897ED3F84460DA7690853B444D7D12C27B6148D91FAFF622E83'
# exponent = '010001'
#
# aes_key = random_str(32)
# aes_iv = random_str(16)
# aes_token = aes_key + '|' + aes_iv
# # message = 'EKX6HWHQTqoFDgGKGGiqIjKj3YBWIrCs|xUhagavFsyC7365k'
# print(aes_token)
# # print(message)
#
# modulus = int(modulus, 16)
# exponent = int(exponent, 16)
#
# rsa_pubkey = rsa.PublicKey(modulus, exponent)
# crypto = rsa.encrypt(aes_token.encode(), rsa_pubkey)
# b64str = base64.b64encode(crypto)
# print(b64str)
