# # -*- coding: utf-8 -*-
# from scrapy import Request
# from abchina.rules import *
#
#
# class SkuSpider(BaseSpider):
#     name = 'demo'
#     def __init__(self, batchNo=None, *args, **kwargs):
#         super(SkuSpider, self).__init__(*args, **kwargs)
#         if not batchNo:
#             self.batch_no = time_to_batch_no(datetime.now())
#         else:
#             self.batch_no = batchNo
#         self.page_size = 100
#
#     def start_requests(self):
#         self.cookies = {
#                 'GroupPurchaseSessionId': 'z4ogt0zkg1dkuntvlcp0ybl0',
#                 # 'BIGipServerpool_ebiz_9000': '2dF0uzl8grpnCjwI76szeJ2T4xRfJEiwTRvCP2qa8Or0nBihlfJG3jjsi7PNmMsWxQqh264am6QgXTk=',
#                 'BIGipServerpool_ebiz_qyjc': '1NSZsE5GkjoXqh1bBRrKEzfZ3SoIgBfjFPcMYvTBOSHMgMNe4xEkocvJgkeN7ha36GFKfbN8ZUEohM8='
#             }
#         if not self.cookies:
#             self.logger.error('init cookie failed...')
#             return
#         self.logger.info('init cookie: %s', self.cookies)
#
#         self.sec_key = 'THtHuRMOHP4GcIxYtn3Z8lkS3JARTg66'
#         self.sec_iv = '80iPJgl2P5u5Y81D'
#
#         # for id in ['1000097032', '1000065527', '1000133401', '1000080647', '1000136139', '1000035075', '1000083911', '1000064519', '1000112479', '1000040528', '1000040514', '1000064881', '1000010055', '1000080125', '1000003602', '1000015489', '1000117394', '1000101273', '1000020920', '1000015809', '1000088849', '1000118031', '1000115991', '1000098130', '1000102176', '1000008467', '1000078562', '1000134067', '1000026047', '1000074880', '1000049522', '1000104648', '1000108276', '1000017744', '1000015947', '1000027147', '1000063616', '1000002888', '1000036898', '1000035883', '1000088287', '1000095938']:
#         for id in ['1000097032']:
#             # POST----------------
#             body = json.dumps({
#                         "ProductUnityCode": id,
#                         "CurPage":1,
#                         "PageSize":10,
#                         "InterfaceName":"GenlOrder/GetProTradingRecord"
#                     })
#             yield Request(
#                 method='POST',
#                 url='https://e.abchina.com/qyjc/site/GenlOrder/GetProTradingRecord',
#                 cookies=self.cookies,
#                 callback=self.parse_post,
#                 headers={
#                         'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36',
#                         'Content-Type':'application/json;charset=UTF-8',
#                 },
#                 meta={
#                     'reqType': 'order',
#                     'batchNo': self.batch_no,
#                     'sec_key': self.sec_key,
#                     'sec_iv': self.sec_iv,
#                 },
#                 body=body,
#                 errback=self.error_back,
#                 priority=100,
#                 dont_filter=True
#             )
#             # # POST----------------
#
#             # GET----------------
#             params = json.dumps({"productUnityCode":id,"InterfaceName":"GenProduct/GetProductDetail"})
#             # params = json.dumps({"InterfaceName":"ExShoppingCart/ProductCount"})
#             yield Request(
#                 method='GET',
#                 url='https://e.abchina.com/qyjc/site/GenProduct/GetProductDetail',
#                 # url='https://e.abchina.com/qyjc/site/ExShoppingCart/ProductCount?EncryptMsg=' + quote(str(enc_msg, encoding='utf-8')),
#                 cookies=self.cookies,
#                 callback=self.parse_get,
#                 headers={
#                         'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36',
#                         'X-EncryptType':'encrypt',
#                 },
#                 meta={
#                     'reqType': 'order',
#                     'batchNo': self.batch_no,
#                     'sec_key': self.sec_key,
#                     'sec_iv': self.sec_iv,
#                     'params': params,
#                 },
#                 errback=self.error_back,
#                 priority=100,
#                 dont_filter=True
#             )
#             # GET----------------
#
#     def parse_post(self, response):
#         print(response.text)
#
#         txt = response.meta.get('decrypted')
#         print(txt)
#
#         # aes = AesCrypter(self.sec_key, self.sec_iv)
#         # rs = json.loads(response.text)
#         # enc_msg = rs.get('Value')
#         # rs = aes.decrypt(enc_msg)
#         # data = json.loads(rs)
#         # print(data)
#
#
#     def parse_get(self, response):
#         print(response.text)
#
#         txt = response.meta.get('decrypted')
#         print(txt)
#
#         # aes = AesCrypter(self.sec_key, self.sec_iv)
#         # rs = json.loads(response.text)
#         # enc_msg = rs.get('Value')
#         # rs = aes.decrypt(enc_msg)
#         # data = json.loads(rs)
#         # print(data)

