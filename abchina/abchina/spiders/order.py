# # -*- coding: utf-8 -*-
# import json
# import random
#
# import scrapy
# from scrapy import Request
# from datetime import datetime
# from abchina.rules import parse_order_item
# from abchina.util.api_login import ApiLogin
# from zc_core.util.batch_gen import time_to_batch_no
# from zc_core.dao.sku_pool_dao import SkuPoolDao
# from scrapy.exceptions import IgnoreRequest
#
#
# class OrderSpider(BaseSpider):
#     name = 'order'
#     # 常用链接
#     order_url = 'https://e.abchina.com/qyjc/site/GenlOrder/GetProTradingRecord'
#     allowed_domains = ['e.abchina.com']
#
#     def __init__(self, timeDelta=-1, batchNo=None, *args, **kwargs):
#         super(OrderSpider, self).__init__(*args, **kwargs)
#         if not batchNo:
#             self.batch_no = time_to_batch_no(datetime.now(), timeDelta)
#         else:
#             self.batch_no = batchNo
#
#     def start_requests(self):
#         result = ApiLogin().do_login()
#         if result:
#             self.cookies = result.get('cookies')
#             self.sec_key = result.get('sec_key')
#             self.sec_iv = result.get('sec_iv')
#         else:
#             raise Exception('登录失败')
#         self.logger.info('init result: %s', result)
#
#         pool_list = SkuPoolDao().get_sku_pool_list()
#         self.logger.info('全量：%s' % (len(pool_list)))
#         random.shuffle(pool_list)
#         for sku in pool_list:
#             sku_id = sku.get('_id')
#             supplier_name = sku.get('supplierName')
#             # 采集订单第一页
#             page = 1
#             yield Request(
#                 method='POST',
#                 url=self.order_url,
#                 cookies=self.cookies,
#                 callback=self.parse_order_item,
#                 headers={
#                     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36',
#                     'X-EncryptType': 'encrypt',
#                     'Content-Type': 'application/json'
#                 },
#                 body=json.dumps({
#                     "ProductUnityCode": sku_id,
#                     "CurPage": page,
#                     "PageSize": 10,
#                     "InterfaceName": 'GenlOrder/GetProTradingRecord'
#                 }),
#                 meta={
#                     'reqType': 'order',
#                     'batchNo': self.batch_no,
#                     'page': page,
#                     'skuId': sku_id,
#                     'supplierName': supplier_name,
#                     'sec_key': self.sec_key,
#                     'sec_iv': self.sec_iv,
#                 },
#                 errback=self.error_back,
#                 priority=50,
#                 dont_filter=True
#             )
#
#     # 处理order
#     def parse_order_item(self, response):
#         meta = response.meta
#         sku_id = meta.get('skuId')
#         cur_page = meta.get('page')
#         supplier_name = meta.get('supplierName')
#         order_list = parse_order_item(response)
#         if order_list:
#             self.logger.info('[%s]订单: 第%s页%s条' % (sku_id, cur_page, len(order_list)))
#             for order in order_list:
#                 yield order
#
#             next_page = cur_page + 1
#             # 分页采集订单
#             yield Request(
#                 method='POST',
#                 url=self.order_url,
#                 cookies=self.cookies,
#                 callback=self.parse_order_item,
#                 headers={
#                     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36',
#                     'X-EncryptType': 'encrypt',
#                     'Content-Type': 'application/json'
#                 },
#                 body=json.dumps({
#                     "ProductUnityCode": sku_id,
#                     "CurPage": next_page,
#                     "PageSize": 10,
#                     "InterfaceName": 'GenlOrder/GetProTradingRecord'
#                 }),
#                 meta={
#                     'reqType': 'order',
#                     'batchNo': self.batch_no,
#                     'page': next_page,
#                     'skuId': sku_id,
#                     'supplierName': supplier_name,
#                     'sec_key': self.sec_key,
#                     'sec_iv': self.sec_iv,
#                 },
#                 errback=self.error_back,
#                 priority=50,
#                 dont_filter=True
#             )
#         else:
#             self.logger.error('无单: <sku=%s, page=%s>' % (sku_id, cur_page))
#
