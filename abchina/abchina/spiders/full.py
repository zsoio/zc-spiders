# -*- coding: utf-8 -*-
import random
from scrapy import Request
from zc_core.dao.item_data_dao import ItemDataDao
from zc_core.dao.sku_dao import SkuDao
from abchina.rules import *
from zc_core.dao.sku_pool_dao import SkuPoolDao
from zc_core.dao.batch_dao import BatchDao
from zc_core.util.done_filter import DoneFilter
from zc_core.spiders.base import BaseSpider


class FullSpider(BaseSpider):
    name = 'full'
    # 常用链接
    detail_url = 'https://e.abchina.com/qyjc/site/GenProduct/GetProductDetail'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(FullSpider, self).__init__(batchNo, *args, **kwargs)
        # 创建批次记录
        BatchDao().create_batch(self.batch_no)
        # 避免重复采集
        self.done_filter = DoneFilter(self.batch_no)

    def start_requests(self):
        # 一、过滤
        pool_list = SkuDao().get_batch_sku_list(self.batch_no, fields={'_id': 1})
        done_list = ItemDataDao().get_batch_data_list(self.batch_no, fields={'_id': 1})
        pool_ids = [x.get('_id') for x in pool_list]
        done_ids = [x.get('_id') for x in done_list]
        dist_ids = list(set(pool_ids) ^ set(done_ids))
        pool_list = SkuPoolDao().get_sku_pool_list(query={'_id': {'$in': dist_ids}},
                                                   fields={'_id': 1, 'catalog3Id': 1, 'catalog3Name': 1, 'unit': 1,
                                                           'batchNo': 1, 'offlineTime': 1})
        # 二、不过滤
        # pool_list = SkuDao().get_batch_sku_list(self.batch_no, fields={'_id': 1, 'catalog3Id': 1, 'catalog3Name': 1, 'unit': 1, 'batchNo': 1, 'offlineTime': 1})

        self.logger.info('全量：%s' % (len(pool_list)))
        random.shuffle(pool_list)
        for sku in pool_list:
            sku_id = sku.get('_id')
            cat3_id = sku.get('catalog3Id', '')
            cat3_name = sku.get('catalog3Name', '')
            unit = sku.get('unit', '')
            # 避免无效采集
            offline_time = sku.get('offlineTime', 0)
            if offline_time > settings.get('MAX_OFFLINE_TIME', 2):
                self.logger.info('忽略: [%s][%s]', sku_id, offline_time)
                continue
            if self.done_filter.contains(sku_id) and not settings.get('FORCE_RECOVER', False):
                self.logger.info('已采: [%s]', sku_id)
                continue

            # GET----------------
            params = json.dumps({"productUnityCode": sku_id, "InterfaceName": "GenProduct/GetProductDetail"})
            yield Request(
                method='GET',
                url=self.detail_url,
                # cookies=self.cookies,
                callback=self.parse_item_data,
                headers={
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36',
                    'X-EncryptType': 'encrypt',
                },
                meta={
                    'reqType': 'item',
                    'batchNo': self.batch_no,
                    'skuId': sku_id,
                    'catalog3Id': cat3_id,
                    'catalog3Name': cat3_name,
                    'unit': unit,
                    # 'sec_key': self.sec_key,
                    # 'sec_iv': self.sec_iv,
                    'params': params,
                },
                errback=self.error_back,
                priority=100,
                dont_filter=True
            )
            # GET----------------

    # 处理ItemData
    def parse_item_data(self, response):
        meta = response.meta
        sku_id = meta.get('skuId')
        data = parse_item_data(response)
        if data and 'salePrice' in data and data.get('salePrice') > 0:
            self.logger.info('商品: [%s]' % data.get('skuId'))
            yield data
        else:
            self.logger.info('下架: [%s]' % sku_id)
