from pymongo import UpdateOne, InsertOne

from zc_core.client.mongo_client import Mongo

# mongo = Mongo()
# item_pool = mongo.list('item_data_pool')
# bulk_list = list()
# for row in item_pool:
#     row.pop('catalog1Id', None)
#     row.pop('catalog1Name', None)
#     row.pop('catalog2Id', None)
#     row.pop('catalog2Name', None)
#     row.pop('catalog3Name', None)
#     row.pop('originPrice', None)
#     row.pop('skuName', None)
#     row.pop('supplierId', None)
#     row.pop('supplierSkuId', None)
#     row.pop('supplierSkuCode', None)
#     row.pop('supplierSkuLink', None)
#     row.pop('brandId', None)
#     row.pop('skuImg', None)
#     row.pop('unit', None)
#     row['offlineTime'] = 0
#     # bulk_list.append(UpdateOne({'_id': row.get("_id")}, {'$set': row}, upsert=True))
#     bulk_list.append(InsertOne(row))
#
# print(len(bulk_list))
# mongo.bulk_write('sku_pool', bulk_list)


mongo = Mongo()
batch_sku = mongo.list('sku_20200223')
bulk_list = list()
for row in batch_sku:
    row.pop('catalog3Name', None)
    row.pop('unit', None)
    row.pop('supplierSkuId', None)
    row.pop('skuImg', None)
    row['offlineTime'] = 0
    bulk_list.append(UpdateOne({'_id': row.get("_id")}, {'$set': row}, upsert=True))
    # bulk_list.append(InsertOne(row))

print(len(bulk_list))
mongo.bulk_write('sku_pool', bulk_list)