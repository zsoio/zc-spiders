# encoding=utf-8
"""
响应对象业务校验
"""
from zc_core.middlewares.validate import BaseValidateMiddleware


class BizValidator(BaseValidateMiddleware):

    def validate_sku(self, request, response, spider):
        return response

    def validate_item(self, request, response, spider):
        return response
