# encoding:utf-8

from pymongo import UpdateOne
from zc_core.pipelines.helper.catalog_helper import CatalogHelper
from zc_core.client.mongo_client import Mongo
from zc_core.util.common import parse_time
from zc_core.util.encrypt_util import md5

mongo = Mongo()


# 从sku_pool池子修补全部分类
def fix_sku_pool_catalog():
    update_bulk = list()
    src_list = mongo.list('sku_pool', fields={'_id': 1, 'catalog3Id': 1})
    for item in src_list:
        _id = item.get('_id')
        catalog3Id = item.get('catalog3Id')
        dict1 = {}
        dict1['_id'] = _id
        dict1['catalog3Id'] = catalog3Id
        if catalog3Id:
            try:
                cat_helper = CatalogHelper()
                cat_helper.fill(dict1)
                update_bulk.append(UpdateOne({'_id': _id}, {'$set': {
                    '_id': _id,
                    'catalog3Id': catalog3Id,
                    'catalog2Id': dict1['catalog2Id'],
                    'catalog1Id': dict1['catalog1Id'],
                    'catalog3Name': dict1['catalog3Name'],
                    'catalog2Name': dict1['catalog2Name'],
                    'catalog1Name': dict1['catalog1Name'],
                }}, upsert=False))
            except Exception as e:
                print(e)
    print(len(update_bulk))
    mongo.bulk_write('data_20210708', update_bulk)
    mongo.close()
    print('任务完成~')


# 从sku_pool池子修补三级分类
def fix_sku_pool_catalog3():
    update_bulk = list()
    src_list = mongo.list('sku_pool', fields={'_id': 1, 'catalog3Name': 1})
    for item in src_list:
        _id = item.get('_id')
        if item.get('catalog3Name'):
            update_bulk.append(UpdateOne({'_id': _id}, {'$set': {
                '_id': _id,
                'catalog3Id': item['catalog3Name'],
            }}, upsert=False))

    print(len(update_bulk))
    mongo.bulk_write('sku_pool', update_bulk)
    mongo.close()
    print('任务完成~')


# data中提取分类到catalog_pool
def data_to_catalog_pool():
    update_bulk = list()
    src_list = mongo.list('data_20210710', query={"catalog2Id": {"$exists": True}},
                          fields={'_id': 1, 'catalog3Id': 1, 'catalog2Id': 1, 'catalog1Id': 1, 'catalog3Name': 1,
                                  'catalog2Name': 1, 'catalog1Name': 1})

    for item in src_list:
        catalog1Id = item['catalog1Id']
        catalog1Name = item['catalog1Name']
        update_bulk.append(UpdateOne({'_id': catalog1Id}, {'$set': {
            '_id': catalog1Id,
            'batchNo': 20210710,
            'catalogName': catalog1Name,
            'leafFlag': 0,
            'level': 1,
            'linkable': 0,
            'parentId': ""
        }}, upsert=True))

        catalog2Id = item['catalog2Id']
        catalog2Name = item['catalog2Name']
        update_bulk.append(UpdateOne({'_id': catalog2Id}, {'$set': {
            '_id': catalog2Id,
            'batchNo': 20210710,
            'catalogName': catalog2Name,
            'leafFlag': 0,
            'level': 2,
            'linkable': 0,
            'parentId': catalog1Id
        }}, upsert=True))

        catalog3Id = item['catalog3Id']
        catalog3Name = item['catalog3Name']
        update_bulk.append(UpdateOne({'_id': catalog3Id}, {'$set': {
            '_id': catalog3Id,
            'batchNo': 20210710,
            'catalogName': catalog3Name,
            'leafFlag': 0,
            'level': 3,
            'linkable': 0,
            'parentId': catalog2Id
        }}, upsert=True))

    print(len(update_bulk))
    mongo.bulk_write('catalog_pool', update_bulk)
    mongo.close()
    print('任务完成~')


# 通过Cataloghelper补全分类
def fix_cat_helper_data_catalog():
    update_bulk = list()
    src_list = mongo.list('data_20210710', query={"catalog2Id": {"$exists": False}},
                          fields={'_id': 1, 'catalog3Id': 1})
    for item in src_list:
        _id = item.get('_id')
        catalog3Id = item.get('catalog3Id')
        dict1 = {}
        dict1['_id'] = _id
        dict1['catalog3Id'] = catalog3Id
        if catalog3Id:
            try:
                cat_helper = CatalogHelper()
                cat_helper.fill(dict1)
                update_bulk.append(UpdateOne({'_id': _id}, {'$set': {
                    '_id': _id,
                    'catalog3Id': catalog3Id,
                    'catalog2Id': dict1['catalog2Id'],
                    'catalog1Id': dict1['catalog1Id'],
                    'catalog3Name': dict1['catalog3Name'],
                    'catalog2Name': dict1['catalog2Name'],
                    'catalog1Name': dict1['catalog1Name'],
                }}, upsert=False))
            except Exception as e:
                print(e)
    print(len(update_bulk))
    mongo.bulk_write('data_20210710', update_bulk)
    mongo.close()
    print('任务完成~')


# 表与表之间补全数据
def fix_sheet1_sheet2():
    src_list = mongo.list('data_20210708', query={"catalog2Id": {"$exists": False}},
                          fields={'_id': 1, 'catalog3Id': 1})
    for item in src_list:
        list1 = mongo.list('data_20210710', query={'_id': item.get('_id')},
                           fields={'_id': 1, 'catalog3Id': 1, 'catalog2Id': 1, 'catalog1Id': 1, 'catalog3Name': 1,
                                   'catalog2Name': 1, 'catalog1Name': 1})
        if list1.__len__() == 1:
            mongo.insert_or_update('data_20210708', '_id', list1[0])
    mongo.close()
    print('任务完成~')


# 跟换分类名
def rename_catalog1Name():
    update_bulk = list()
    src_list = mongo.list("data_20210708", query={'catalog1Name': '箱包礼品'})
    for item in src_list:
        update_bulk.append(UpdateOne({'_id': item.get('_id')}, {'$set': {
            '_id': item.get('_id'),
            'catalog1Name': '礼品箱包',
        }}, upsert=False))
    print(len(update_bulk))
    mongo.bulk_write('data_20210708', update_bulk)
    mongo.close()
    print('任务完成~')


if __name__ == '__main__':
    # pass
    # fix_item_pool()
    # fix_catalog_pool()
    # fix_item()
    rename_catalog1Name()