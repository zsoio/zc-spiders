import re


# 解析供应商id
def match_supplier_id(link):
    if link:
        re_data = re.search('/cms/content/busi_page_index/(.*?).html', str(link('.b_title a').attr('href')))
        if re_data:
            return re_data.group(1)
        else:
            return ''
    return ''


# 解析三级分页id
def match_catalog3id(link):
    if link:
        re_data = re.search('/search/goodsDetail/\d+/\d+/(.*?).html', link)
        if re_data:
            return re_data.group(1)
        else:
            return ''
    return ''


def match_item(response, index):
    if response:
        re_data = re.search('countNumberPriceGoodsFtl\((.*?)\);', response.text)
        if re_data:
            try:
                re_data_list = re_data.group(1).replace("'", '').split(',')
                return re_data_list[index]
            except Exception as e:
                print('异常:', e)
                return ''
        else:
            return ''
    return ''


def match_catalogId(link, level):
    rs = re.search('catLv={}&catId=(\d+)&'.format(level), link)
    if rs:
        rs = rs.group(1)
        return str(rs)
    return ''


# # 测试
if __name__ == '__main__':
    pass
