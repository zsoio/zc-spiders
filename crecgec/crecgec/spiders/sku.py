# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from zc_core.model.items import Box
from zc_core.util.batch_gen import time_to_batch_no
from scrapy.utils.project import get_project_settings
from crecgec.rules import *
from datetime import datetime
from zc_core.spiders.base import BaseSpider
import json


class SkuSpider(BaseSpider):
    name = 'sku'
    # 常用链接
    index_url = 'https://mall.crecgec.com/?channelType=01'
    # 商品列表页 catLv=level
    sku_list_url = 'https://mall.crecgec.com/search/Thirdmain?catId={}&catLv=3&catType=1'
    #
    # sku_list_url2 = 'https://mall.crecgec.com/search/pageList?pageSize={}&currentPage={}&customerType=0'
    sku_list_url2 = 'https://mall.crecgec.com/crecgecapi/search/pageList'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.page_size = 30
        self.settings = get_project_settings()
        self.SKU1_WHITE_LIST = self.settings.get('SKU1_WHITE_LIST')
        self.max_page_limit = 100
        self.ladders = self.settings.get('PRICE_LADDER', [])

    def _build_list_req(self, catalog1Id, catalog1Name, page,
                        price_range='-'):
        return scrapy.Request(
            method='POST',
            url=self.sku_list_url2,
            meta={
                'reqType': 'sku',
                'batchNo': self.batch_no,
                'page': page,
                'catalog1Id': catalog1Id,
                'catalog1Name': catalog1Name,
                'priceRange': price_range
            },
            body=json.dumps({"pageSize": 30, "provinceId": "", "priceBegin": price_range.split('-')[0],
                             "priceEnd": price_range.split('-')[1], "busiCatCode": "",
                             "busiCatId": "",
                             "busiName": "", "channel_type": "01", "cutId": "", "fullText": "", "goodsId": "",
                             "isMain": "",
                             "isShopSearch": "", "itemId": "", "lbitemId": "", "property": "", "propertyCode": "",
                             "shopBusiCatCode": "", "useQuan": "", "useQuanType": "", "channelType": "01",
                             "brandSearch": "OTmain", "catLv": "1", "catId": catalog1Id,
                             "sortType": "desc", "b1CatId": catalog1Id, "currentPage": page}),
            headers={'content-type': 'application/json;charset=UTF-8'},
            callback=self.parse_sku_content_deal,
            errback=self.error_back,
            dont_filter=True,

        )

    def start_requests(self):
        # 品类、品牌
        yield Request(
            url=self.index_url,
            meta={
                'reqType': 'sku',
                'batchNo': self.batch_no,

            },
            callback=self.parse_catalog,
            errback=self.error_back,
            dont_filter=True,
        )

    # 处理sku列表
    def parse_catalog(self, response):
        # 处理品类列表
        cats = parse_catalog(response)
        if cats:
            self.logger.info('品类: count[%s]' % len(cats))
            yield Box('catalog', self.batch_no, cats)

            for cat in cats:
                if cat.get('level') == 1:
                    # 采集sku列表
                    catalog1Id = cat.get('catalogId')
                    catalog1Name = cat.get('catalogName')
                    if self.SKU1_WHITE_LIST:
                        if catalog1Name in self.SKU1_WHITE_LIST:
                            yield self._build_list_req(catalog1Id, catalog1Name, 1)
                    else:
                        yield self._build_list_req(catalog1Id, catalog1Name, 1)

    # 处理sku列表
    def parse_sku_content_deal(self, response):
        meta = response.meta
        catalog1Name = meta.get('catalog1Name')
        catalog1Id = meta.get('catalog1Id')
        price_range = meta.get('priceRange', '')
        cur_page = meta.get('page')

        # 处理sku列表
        sku_list, items_list = parse_sku(response)
        self.logger.info("清单: cat=%s, page=%s, cnt=%s" % (catalog1Id, cur_page, len(sku_list)))
        if sku_list:
            yield Box('sku', self.batch_no, sku_list)
            yield Box('item', self.batch_no, items_list)
            # 发起分页
            if cur_page == 1:
                # 解析页面
                total_pages = parse_total_page(response)
                self.logger.info("清单: cat=%s, total_page=%s" % (catalog1Id, total_pages))
                if total_pages > self.max_page_limit:
                    self.logger.info("清单[超限]: cat=%s, total_page=%s" % (catalog1Id, total_pages))

                    if not price_range:
                        for ft in range(1, len(self.ladders)):
                            price_range = "{}-{}".format(self.ladders[ft - 1], self.ladders[ft])
                            self.logger.info("清单1[价格区间]: cat=%s, total_page=%s， priceRange=%s" % (
                                catalog1Id, total_pages, price_range))
                            # 未经过价格筛选，则加入价格筛选条件
                            yield self._build_list_req(catalog1Id, catalog1Name, 1, price_range=price_range)
                    else:
                        price_range = meta.get('priceRange')
                        new_price_range = split_price(price_range)
                        if new_price_range.__len__() == 2:
                            for fs in new_price_range:
                                self.logger.info("清单1[价格区间]: cat=%s, total_page=%s， priceRange=%s" % (
                                    catalog1Id, total_pages, fs))
                                yield self._build_list_req(catalog1Id, catalog1Name, 1, price_range=fs)
                        else:
                            self.logger.info('清单1[价格分割已达限] priceRange=%s' % (price_range))
                else:
                    self.logger.info(
                        "清单: cat=%s, total_page=%s, price_range=%s" % (catalog1Id, total_pages, price_range))
                    for page in range(2, total_pages + 1):
                        yield self._build_list_req(catalog1Id, catalog1Name, page, price_range=price_range)
        else:
            self.logger.info('空页: cat=%s, page=%s' % (catalog1Id, cur_page))
