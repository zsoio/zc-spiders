# encoding=utf-8
"""
响应对象业务校验
"""
from scrapy.exceptions import IgnoreRequest
from zc_core.middlewares.validate import BaseValidateMiddleware


class BizValidator(BaseValidateMiddleware):

    def validate_item(self, request, response, spider):
        if '该商品已下架，非常抱歉' in response.text:
            sku_id = request.meta.get('skuId')
            raise IgnoreRequest('[sku]商品已下架：[%s]' % sku_id)

        return response
