# encoding:utf-8

from pymongo import UpdateOne
from zc_core.pipelines.helper.catalog_helper import CatalogHelper
from zc_core.client.mongo_client import Mongo
from zc_core.util.common import parse_time
from zc_core.util.encrypt_util import md5

mongo = Mongo()


# 根据sku修补data
def fix_catalog3Name():
    update_bulk = list()
    src_list = mongo.list("sku_pool", fields={'_id': 1, 'catalog3Id': 1, 'catalog3Name': 1})
    for item in src_list:
        update_bulk.append(UpdateOne({'_id': item.get('_id')}, {'$set': {
            '_id': item.get('_id'),
            'catalog3Id': item.get('catalog3Id'),
            'catalog3Name': item.get('catalog3Name')
        }}, upsert=False))
    print(len(update_bulk))
    mongo.bulk_write('data_20210615', update_bulk)
    mongo.close()
    print('任务完成~')


def fix_item_data():
    update_bulk = list()
    src_list = mongo.list('data_20210710')
    for item in src_list:
        result = {}
        result['_id'] = str(item['_id'])
        result['catalog1Id'] = item.get('catalog1Id')
        result['catalog2Id'] = item.get('catalog2Id')
        result['catalog3Id'] = item.get('catalog3Id')
        result['catalog1Name'] = item.get('catalog1Name')
        result['catalog2Name'] = item.get('catalog2Name')
        result['catalog3Name'] = item.get('catalog3Name')
        update_bulk.append(UpdateOne({'_id': result['_id']}, {'$set': result}, upsert=False))
    print(len(update_bulk))

    batchNo = 'data_20210706'
    # for batch_no in rows:
    mongo.bulk_write(batchNo, update_bulk)
    mongo.close()
    print('任务完成~')


if __name__ == '__main__':
    # pass
    # fix_item_pool()
    # fix_catalog_pool()
    # fix_item()
    fix_item_data()
