# encoding=utf-8
"""
响应对象业务校验
"""
from scrapy.exceptions import IgnoreRequest
from zc_core.middlewares.validate import BaseValidateMiddleware
from zc_core.util.http_util import retry_request


class BizValidator(BaseValidateMiddleware):

    def validate_item(self, request, response, spider):
        if not response.text:
            return retry_request(request)
        if '您要查看的商品下架了' in response.text:
            raise IgnoreRequest('下架[Item]: [%s]' % request.meta.get('skuId'))

        return response
