# -*- coding: utf-8 -*-
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from datetime import datetime
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from cpeinet.rules import *
from zc_core.spiders.base import BaseSpider


class SkuSpider(BaseSpider):
    name = "sku"
    # 列表页
    sku_list_url = "http://emall.cpeinet.com.cn/solr/search/pageSearchJson.do?q=*&page={}"

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        page = 1
        sku_urls = self.sku_list_url.format(page)
        yield Request(
            url=sku_urls,
            method="POST",
            meta={
                'reqType': 'sku',
                'page': page,
                'batchNo': self.batch_no,
            },
            callback=self.parse_sku_list,
            errback=self.error_back

        )

    def parse_sku_list(self, response):
        if response and response.text:
            meta = response.meta
            page = meta.get("page")
            sku_list = json.loads(response.text)
            if sku_list and len(sku_list) > 0:
                self.logger.info('清单: page=%s, cnt=%s' % (page, len(sku_list)))
                skus = parse_sku(response)
                if skus:
                    yield Box('sku', self.batch_no, skus)

                # 查询下一页
                page += 1
                sku_urls = self.sku_list_url.format(page)
                yield Request(
                    url=sku_urls,
                    method="POST",
                    meta={
                        'reqType': 'sku',
                        'page': page,
                        'batchNo': self.batch_no
                    },
                    callback=self.parse_sku_list,
                    errback=self.error_back
                )
            else:
                self.logger.info('查无此页: [%s][%s]' % (self.batch_no, page))


