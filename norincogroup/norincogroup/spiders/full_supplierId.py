# # -*- coding: utf-8 -*-
# import random
# import scrapy
# from scrapy import Request
# from scrapy.exceptions import IgnoreRequest
# from zc_core.util.http_util import retry_request
# from scrapy.utils.project import get_project_settings
# from norincogroup.rules import *
# from zc_core.dao.sku_pool_dao import SkuPoolDao
# from zc_core.dao.sku_dao import SkuDao
# from zc_core.dao.batch_dao import BatchDao
# from zc_core.util.batch_gen import time_to_batch_no
# from zc_core.util.done_filter import DoneFilter
# from zc_core.client.mongo_client import Mongo
#
#
# class FullSpider(BaseSpider):
#     name = 'full_supplierId'
#
#     # web链接
#     item_url_1 = 'https://ar.norincogroup-ebuy.com/exp/hangsource/buy/picksource/sourceInfo.do?gpls={}'
#     # app链接
#     item_url_2 = 'https://msapi.norincogroup-ebuy.com/supermarketweb/exp/hangsource/info?ssid=M11310&gpls={}'
#
#     def __init__(self, batchNo=None, *args, **kwargs):
#         super(FullSpider, self).__init__(*args, **kwargs)
#         if not batchNo:
#             self.batch_no = time_to_batch_no(datetime.now())
#         else:
#             self.batch_no = int(batchNo)
#         # 创建批次记录
#         BatchDao().create_batch(self.batch_no)
#         # 避免重复采集
#         self.done_filter = DoneFilter(self.batch_no)
#         self.MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
#         self.MONGODB_DATABASE = 'norincogroup_2021'
#         self.mongo = Mongo(mongo_uri=self.MONGODB_URI, mongo_db=self.MONGODB_DATABASE)
#
#     def start_requests(self):
#         supplierName_list = self.mongo.get_collection('data_20210602').distinct('supplierName', {})
#         pool_list = []
#         for supplier_name in supplierName_list:
#             pool_list.append(self.mongo.get_collection('data_20210602').find_one({"supplierName": supplier_name}))
#         self.logger.info('全量：%s' % (len(pool_list)))
#         print(pool_list)
#         # random.shuffle(pool_list)
#         for sku in pool_list:
#             sku_id = sku.get('_id')
#             offline_time = sku.get('offlineTime', 0)
#             settings = get_project_settings()
#             if offline_time > settings.get('MAX_OFFLINE_TIME', 2):
#                 self.logger.info('忽略: [%s][%s]', sku_id, offline_time)
#                 continue
#             # 避免重复采集
#             if self.done_filter.contains(sku_id) and not settings.get('FORCE_RECOVER', False):
#                 self.logger.info('已采: [%s]', sku_id)
#                 continue
#             # 采集商品
#             yield Request(
#                 url=self.item_url_2.format(sku_id),
#                 callback=self.parse_item_data,
#                 errback=self.error_back,
#                 meta={
#                     'reqType': 'item',
#                     'batchNo': self.batch_no,
#                     'skuId': sku_id,
#                     # 'poolListCatalog':pool_list_catalog
#                 },
#                 priority=260,
#             )
#
#     # 处理ItemData
#     def parse_item_data(self, response):
#         data = supply_item_app_supplier_id(response)
#         if data.__len__() == 1:
#             self.logger.info('商品: [%s]已被丢弃（价格为0或者为空）' % data.get('skuId'))
#         else:
#             self.logger.info('商品: [%s]' % data.get('skuId'))
#             yield data
#
#     # 错误处理
#     def error_back(self, e):
#         if e.type and e.type == IgnoreRequest:
#             self.logger.info(e.value)
#         else:
#             if e.request:
#                 self.logger.error('请求异常: [%s][%s] -> [%s]' % (str(type(e)), e.request.url, e.request.meta))
#                 yield retry_request(e.request)
#             else:
#                 self.logger.error('未知异常: %s' % e)
