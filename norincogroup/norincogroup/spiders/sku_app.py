# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.util.http_util import retry_request
from zc_core.model.items import Box
from zc_core.util.batch_gen import time_to_batch_no
from norincogroup.rules import *
import time
from zc_core.dao.catalog_dao import *
from zc_core.spiders.base import BaseSpider


class SkuSpider(BaseSpider):
    name = 'sku_app'
    # 获取所有商品列表链接

    sku_list_url = 'https://ar.norincogroup-ebuy.com/allthing/index.do?minprice=&maxprice=&cd=&dlcode={}&pzname={}&title=&order=&orderName=&ecPzType=0&mbname=&payType=&hydm=&searchType=&skuIdWestern=&searchMode=1&pageNumber={}&pageSize=50&sortColumns=undefined'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.page_size = 30

    def _build_list_req(self, catalog2Name, catalog2Id, page):
        return Request(url=self.sku_list_url.format(catalog2Id, catalog2Name, page),
                       callback=self.parse_sku_content_deal,
                       errback=self.error_back,
                       meta={
                           'batchNo': self.batch_no,
                           'page': page,
                           'catalog2Name': catalog2Name,
                           'catalog2Id': catalog2Id,
                       },
                       dont_filter=True
                       )

    def start_requests(self):
        # 采集消费品类目

        list_data = ['计算机', '网络产品', '打复印设备', '其他办公设备', '办公用品', '配件、外设及耗材', '劳保及清洁用品', '文化创意产品', '其他商品', '公务用车',
                     '安全保密产品',
                     '专用信息系统', '兵器云视频', '其他类商品']
        data1 = Mongo().list('catalog_pool', query={
            'catalogName': {'$in': list_data},
        })
        data2 = Mongo().list('catalog_pool')
        for index, i in enumerate(data1):
            for k in [j for j in data2 if j.get('parentId') == i.get('_id')]:
                k['catalog1Name'] = i.get('catalogName')
                k['catalog1Id'] = i.get('_id')
                k['catalog2Name'] = k.get('catalogName')
                k['catalog2Id'] = k.get('_id')
                del k['catalogName']
                data1.append(k)

        for i in data1:
            if i.get('level') == 2:
                catalog2Id = i.get('catalog2Id')
                catalog2Name = i.get('catalog2Name')
                catalog1Id = i.get('catalog1Id')
                catalog1Name = i.get('catalog1Name')
                yield self._build_list_req(catalog2Name, catalog2Id, 1)

    # 处理sku列表
    def parse_sku_content_deal(self, response):
        meta = response.meta
        cur_page = meta.get('page')
        catalog2Name = meta.get('catalog2Name')
        catalog2Id = meta.get('catalog2Id')

        sku_list = parse_sku(response)
        if sku_list:
            self.logger.info('清单: cur=%s,cnt=%s' % (cur_page, len(sku_list)))
            yield Box('sku', self.batch_no, sku_list)
        else:
            self.logger.info('分页为空: page=%s' % (cur_page))
        if cur_page == 1:
            item_total_page = parse_total_page(response)
            self.logger.info(
                '维度：搜索引擎全部sku：总页数:page=%s' % (item_total_page))
            for page in range(2, item_total_page + 1):
                yield self._build_list_req(catalog2Name, catalog2Id, page)
