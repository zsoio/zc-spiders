# # -*- coding: utf-8 -*-
# import scrapy
# from scrapy import Request
# from scrapy.exceptions import IgnoreRequest
# from zc_core.util.http_util import retry_request
# from zc_core.model.items import Box
# from zc_core.util.batch_gen import time_to_batch_no
# from norincogroup.rules import *
# import time
#
#
# class SkuSpider(BaseSpider):
#     name = 'sku'
#     # 获取所有商品列表链接
#     sku_list_url = 'https://ar.norincogroup-ebuy.com/allthing/index.do?minprice=&maxprice=&cd=&dlcode=&pzname=&title=&order=&orderName=&ecPzType=0&mbname=&payType=&hydm=&searchType=1&skuIdWestern=&searchMode=1&pageNumber={}&pageSize={}&sortColumns=undefined'
#
#     def __init__(self, batchNo=None, *args, **kwargs):
#         super(SkuSpider, self).__init__(*args, **kwargs)
#         if not batchNo:
#             self.batch_no = time_to_batch_no(datetime.now())
#         else:
#             self.batch_no = int(batchNo)
#         self.page_size = 30
#
#     def _build_list_req(self, callback, page):
#
#         return Request(url=self.sku_list_url.format(page, self.page_size),
#                        callback=callback,
#                        errback=self.error_back,
#                        meta={
#                            'batchNo': self.batch_no,
#                            'page': page,
#                        },
#                        dont_filter=True
#                        )
#
#     def start_requests(self):
#         # 全部商品sku列表
#         yield self._build_list_req(self.parse_sku_content_deal, 1)
#
#     # 处理sku列表
#     def parse_sku_content_deal(self, response):
#         meta = response.meta
#         cur_page = meta.get('page')
#
#         sku_list = parse_sku(response)
#         if sku_list:
#             for sku in sku_list:
#
#                 self.logger.info('清单: cat=%s, page=%s, cnt=%s' % (sku.get('skuId'), cur_page, len(sku_list)))
#                 yield Box('sku', self.batch_no, sku_list)
#         else:
#             self.logger.info('分页为空: page=%s' % (cur_page))
#         if cur_page == 1:
#             item_total_page = parse_total_page(response)
#             self.logger.info(
#                 '维度：搜索引擎全部sku：总页数:page=%s' % (item_total_page))
#             for page in range(1,item_total_page+1):
#                 yield self._build_list_req(self.parse_sku_content_deal, page)
#
#     # 错误处理
#     def error_back(self, e):
#         if e.type and e.type == IgnoreRequest:
#             self.logger.info(e.value)
#         else:
#             if e.request:
#                 self.logger.error('请求异常: [%s][%s] -> [%s]' % (str(type(e)), e.request.url, e.request.meta))
#                 yield retry_request(e.request)
#             else:
#                 self.logger.error('未知异常: %s' % e)
