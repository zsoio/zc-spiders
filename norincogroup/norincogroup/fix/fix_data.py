# encoding:utf-8
from pymongo import UpdateOne
from zc_core.client.mongo_client import Mongo

mongo = Mongo()


# 有销量的商品
def filter_data():
    update_bulk = list()
    supplier_dict = {}
    src_list = mongo.list("data_20210710", query={"supplierId": {"$exists": True}})
    for item in src_list:
        supplier_dict[item.get('supplierName')] = item.get('supplierId')
    print("供应商字典", supplier_dict)

    column_list = ["data_20210530", "data_20210601", "data_20210602", "data_20210626", "data_20210710",
                   "item_data_pool"]
    for column in column_list:
        column_data = mongo.list(column, fields={"_id": 1, 'supplierName': 1})
        for item in column_data:
            supplierId = supplier_dict.get(item.get('supplierName'))
            if supplierId:
                item['supplierId'] = supplierId
                update_bulk.append(UpdateOne({'_id': item.get('_id')}, {'$set': item}, upsert=False))
            else:
                print(item.get('supplierName'))
        print(len(update_bulk))
        mongo.bulk_write(column, update_bulk)
    mongo.close()
    print('任务完成~')


if __name__ == '__main__':
    filter_data()
