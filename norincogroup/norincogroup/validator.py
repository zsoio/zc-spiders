# encoding=utf-8
"""
响应对象业务校验
"""
from scrapy.exceptions import IgnoreRequest
from zc_core.middlewares.validate import BaseValidateMiddleware
import json


class BizValidator(BaseValidateMiddleware):

    def validate_item(self, request, response, spider):
        if '该商品已经被删除' in response.text:
            sku_id = request.meta.get('skuId')
            raise IgnoreRequest('[Item]商品被删除：[%s]' % sku_id)
        if json.loads(response.text)['code'] == 400:
            sku_id = request.meta.get('skuId')
            raise IgnoreRequest('[Item]商品不存在现金支付方式,请通过网页端下单：[%s]' % sku_id)
        return response

    def validate_catalog(self, request, response, spider):
        if '对不起，没有找到' in response.text:
            catalog_name = request.meta.get('catalogName')
            raise IgnoreRequest('[分页]为空：[%s]' % catalog_name)
        return response
