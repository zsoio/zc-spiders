# -*- coding: utf-8 -*-
import json
import math
from zc_core.util.encrypt_util import md5
from zc_core.util.common import parse_number
from zc_core.model.items import Catalog, Brand, Supplier, Sku, ItemData
from cncecyc.items import ItemSupply


# 供应商分页
def parse_catalog(response):
    cats = list()
    cat_json = json.loads(response.text).get('data', {}).get('cataLog', [])
    for cat1 in cat_json:
        cat1_id = str(cat1.get('id'))
        cat1_name = cat1.get('name')
        entity1 = _build_catalog(cat1_id, cat1_name, '', 1)
        cats.append(entity1)
        sub_cat1 = cat1.get('children')
        if sub_cat1:
            for cat2 in sub_cat1:
                cat2_id = str(cat2.get('id'))
                cat2_name = cat2.get('name')
                entity2 = _build_catalog(cat2_id, cat2_name, cat1_id, 2)
                cats.append(entity2)
                sub_cat2 = cat2.get('children')
                if sub_cat2:
                    for cat3 in sub_cat2:
                        cat3_id = str(cat3.get('id'))
                        cat3_name = cat3.get('name')
                        entity3 = _build_catalog(cat3_id, cat3_name, cat2_id, 3)
                        cats.append(entity3)
    return cats


def _build_catalog(cat_id, cat_name, parent_id, level):
    cat = Catalog()
    cat['catalogId'] = cat_id
    cat['catalogName'] = cat_name
    cat['parentId'] = parent_id
    cat['level'] = level
    if level == 3:
        cat['leafFlag'] = 1
    else:
        cat['leafFlag'] = 0
    cat['linkable'] = 0

    return cat


# 解析brand列表
def parse_supplier(response):
    suppliers = list()
    sp_json = json.loads(response.text)
    supplier = Supplier()
    supplier['id'] = str(sp.get('id'))
    supplier['name'] = str(sp.get('name'))
    suppliers.append(supplier)

    return suppliers


# 解析stock
def parse_stock(response):
    if response.text and 'true' in response.text:
        return True

    return False


# 解析ItemData
def parse_sku_list(response, item_helper):
    data_list = list()
    no_match_list = list()
    meta = response.meta
    rs_json = json.loads(response.text)
    rs = rs_json.get('data', [])
    total_page = math.ceil(rs.get('total', 0) / rs.get('size', 30))
    rows = rs.get('goodsData', []).get('records', [])
    for data in rows:
        item = ItemData()
        item['batchNo'] = meta.get('batchNo')
        item['skuId'] = data.get('skuId')
        item['skuName'] = data.get('skuName').replace('<em>','')
        item['salePrice'] = parse_number(data.get('price'))
        item['originPrice'] = parse_number(data.get('proPrice', 0))
        # item['catalog3Id'] = data.get('proCataCodeThre', '')
        item['skuImg'] = data.get('imageUrl', '')
        item['supplierId'] = data.get('proCode')
        item['supplierName'] = data.get('proName')
        item['supplierSkuId'] = str(data.get('sku'))
        item['soldCount'] = data.get('skuNum', 0)
        item['stock'] = data.get('remainNum', 0)
        matched = CatalogHelper.fill(item)
        if matched:
            data_list.append(item)
        else:
            no_match_list.append(item)

    return data_list, no_match_list, total_page



def parse_pool_data(response):
    meta = response.meta
    item = ItemSupply()
    item['skuId'] = meta.get('skuId')
    item['batchNo'] = meta.get('batchNo')
    item['tag'] = 1

    rs_json = json.loads(response.text)
    data = rs_json.get('data', {})
    cats = data.get('catNameAll', '').split('/')
    if len(cats) >=3:
        item['catalog1Id'] = md5(cats[0])
        item['catalog2Id'] = md5(cats[1])
        item['catalog3Id'] = md5(cats[2])
        item['catalog1Name'] = cats[0]
        item['catalog2Name'] = cats[1]
        item['catalog3Name'] = cats[2]
        item['unit'] = data.get('saleUnit')
        item['srcContent'] = data

    return item



def parse_item_data(response):
    meta = response.meta
    item = meta.get('sku')
    item['batchNo'] = meta.get('batchNo')

    rs_json = json.loads(response.text)
    data = rs_json.get('data', {})
    cats = data.get('catNameAll', '').split('/')
    if len(cats) >=3:
        item['catalog1Id'] = md5(cats[0])
        item['catalog2Id'] = md5(cats[1])
        item['catalog3Id'] = md5(cats[2])
        item['catalog1Name'] = cats[0]
        item['catalog2Name'] = cats[1]
        item['catalog3Name'] = cats[2]
        item['unit'] = data.get('saleUnit')
        item['tag'] = 1

    return item
