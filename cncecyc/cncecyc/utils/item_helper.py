# # -*- coding: utf-8 -*-
import threading
from zc_core.model.items import ItemData

from zc_core.dao.item_pool_dao import ItemPoolDao


class ItemHelper(object):
    _instance_lock = threading.Lock()
    _biz_inited = False

    def __new__(cls, *args, **kwargs):
        if not hasattr(ItemHelper, "_instance"):
            with ItemHelper._instance_lock:
                if not hasattr(ItemHelper, "_instance"):
                    ItemHelper._instance = object.__new__(cls)
        return ItemHelper._instance

    def __init__(self):
        if not self._biz_inited:
            self._biz_inited = True
            self.id_dict = dict()
            pool_list = ItemPoolDao().get_item_pool_list(
                fields={'catalog1Id': 1, 'catalog2Id': 1, 'catalog3Id': 1, 'catalog1Name': 1, 'catalog2Name': 1, 'catalog3Name': 1, 'unit': 1, },
                query={'tag': {'$exists': False}})
            for item in pool_list:
                sku_id = item.get('_id')
                self.id_dict[sku_id] = item

    def fill(self, item):
        # 商品数据、商品池数据、订单明细
        if isinstance(item, ItemData):
            sku_id = item.get('skuId')
            if sku_id and sku_id in self.id_dict:
                db_item = self.id_dict.get(sku_id)
                item.update(db_item)
            else:
                return False

        return item
