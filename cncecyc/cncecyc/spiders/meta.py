# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from zc_core.model.items import Box
from cncecyc.rules import *
from zc_core.spiders.base import BaseSpider


class MetaSpider(BaseSpider):
    name = 'meta'
    # 常用链接
    cat_url = 'https://www.cncecyc.com/share-ecommerce/catalogue/cataAllListNoUser'
    supplier_url = 'https://www.cncecyc.com/share-ecommerce/provider/getValidProviderList'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(MetaSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        # 品类
        yield Request(
            url=self.cat_url,
            meta={
                'reqType': 'catalog',
                'batchNo': self.batch_no,
            },
            callback=self.parse_catalog,
            errback=self.error_back,
            priority=250,
            dont_filter=True,
        )
        # 供应商
        yield scrapy.Request(
            method='POST',
            url=self.supplier_url,
            meta={
                'reqType': 'supplier',
                'batchNo': self.batch_no,
            },
            body=json.dumps({
                'proName': ''
            }),
            headers={
                'referer': 'https://www.cncecyc.com/store/search_item?keyword=',
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36'
            },
            callback=self.parse_supplier,
            errback=self.error_back,
            priority=250,
            dont_filter=True,
        )

    def parse_catalog(self, response):
        cats = parse_catalog(response)
        if cats:
            self.logger.info('品类: count=%s' % len(cats))
            yield Box('catalog', self.batch_no, cats)
        else:
            self.logger.error('无品类')

    def parse_supplier(self, response):
        suppliers = parse_supplier(response)
        if suppliers:
            self.logger.info('供应商: count=%s' % len(suppliers))
            yield Box('supplier', self.batch_no, suppliers)
        else:
            self.logger.error('无供应商')
