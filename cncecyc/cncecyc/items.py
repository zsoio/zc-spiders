# -*- coding: utf-8 -*-
from scrapy.exceptions import DropItem
from zc_core.model.items import ItemData


class ItemSupply(ItemData):
    def validate(self):
        if not self.get('_id') and not self.get('skuId'):
            raise DropItem("ItemData Error [skuId]")
        return True
