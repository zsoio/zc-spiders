# encoding:utf-8
from pymongo import UpdateOne
from zc_core.client.mongo_client import Mongo

mongo = Mongo()



def full_data():

    data1 = mongo.list('data_20210720', query={"salePrice": {"$exists": False}}, fields={"_id": 1})
    update_bulk = list()
    for item in data1:
        _id = item.get('_id')
        data2 = mongo.list('full_data_20210710', query={"_id": _id},
                           fields={"_id": 1, "soldCount": 1, "originPrice": 1, "salePrice": 1})
        if data2.__len__() == 1:
            data2 = data2[0]
            update_bulk.append(UpdateOne({'_id': _id}, {'$set': data2}, upsert=False))
    print(len(update_bulk))
    mongo.bulk_write('data_20210720', update_bulk)
    mongo.close()
    print('任务完成~')

if __name__ == '__main__':
    full_data()
