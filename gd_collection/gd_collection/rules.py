# -*- coding: utf-8 -*-
import json
import math
import copy
from pyquery import PyQuery
from zc_core.util.http_util import *
from datetime import datetime
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.common import parse_timestamp
from zc_core.model.items import Catalog, ItemData, Spu, OrderItem, DealerItemData
from zc_core.pipelines.helper.catalog_helper import CatalogHelper
from gd_collection.model import gdOrderItem


# 解析catalog列表
def parse_catalog(response):
    cats = list()
    json_data = json.loads(response.text).get("data", {}).get("categoryList", [])
    for data1 in json_data:
        cat1_id = data1.get('categoryguid')
        cat1_name = data1.get('categoryname')
        cat11 = _build_catalog(cat1_id, cat1_name, '', 1)
        cats.append(cat11)
        for data2 in data1.get('categoryList', []):
            cat2_id = data2.get('categoryguid')
            cat2_name = data2.get('categoryname')
            cat22 = _build_catalog(cat2_id, cat2_name, cat1_id, 2)
            cats.append(cat22)
            for data3 in data2.get('categoryList', []):
                cat3_id = data3.get('categoryguid')
                cat3_name = data3.get('categoryname')
                cat33 = _build_catalog(cat3_id, cat3_name, cat2_id, 3)
                cats.append(cat33)
    return cats


def _build_catalog(cat_id, cat_name, parent_id, level):
    cat = Catalog()
    cat['catalogId'] = cat_id
    cat['catalogName'] = cat_name
    cat['parentId'] = parent_id
    cat['level'] = level
    if level == 3:
        cat['leafFlag'] = 1
    else:
        cat['leafFlag'] = 0
    cat['linkable'] = 0
    return cat


# 解析spu列表页数
def parse_spu_total_page(response):
    json_data = json.loads(response.text)
    totals = json_data.get('data', {}).get('total')
    return totals


# 解析经销商商品页
def parse_full_total_page(response):
    json_data = json.loads(response.text)
    totals = json_data.get('data', {}).get('total')
    return totals


# 解析订单成交页数
def parse_order_total_page(response):
    json_data = json.loads(response.text)
    totals = json_data.get('data', {}).get('total')
    return totals


# 维度：品牌，解析spu列表页数
def parse_brand_total_page(response):
    page_json = json.loads(response.text)
    total_page = page_json['result']['pageTotal']
    return total_page


# 解析spu列表
def parse_spu(response):
    meta = response.meta
    batch_no = meta.get('batchNo')
    catalog3Id = meta.get('catalog3Id')
    json_data = json.loads(response.text).get('data', {}).get('datamap', {}).get('list', [])
    spus = list()
    items = list()
    catalog_helper = CatalogHelper()
    for data in json_data:
        spu = Spu()
        goods = data.get('goods', {})
        spu['spuId'] = data.get('goodspriceguid')
        spu['catalog3Id'] = catalog3Id
        catalog_helper.fill(spu)
        spu['skuName'] = goods.get('name')
        spu['skuCode'] = data.get('goodscode')
        # spu['unit'] = goods.get('unit')
        spu['batchNo'] = batch_no
        spu['supplierName'] = goods.get("suppliername")
        # spu['supplierSkuLink'] = goods.get('supplierlink')
        spu['supplierId'] = goods.get('supplierguid')
        spu['skuImg'] = goods.get('picturepath')
        spu['brandName'] = goods.get('goodsbrandname')
        spu['brandId'] = goods.get('goodsbrandguid')
        spu['originPrice'] = goods.get('marketprice')
        spu['salePrice'] = goods.get('salesprice')
        spu['soldCount'] = data.get('sellcount')
        spu['brandModel'] = goods.get('code')
        spus.append(spu)

        item = ItemData()
        item.update(copy.deepcopy(spu))
        items.append(item)
    return spus


# 解析ItemData
def parse_item_data(response):
    meta = response.meta
    batch_no = meta.get('batchNo')
    spu_id = meta.get('_id')
    json_data = json.loads(response.text).get('data', {}).get('data', [])
    for data in json_data:
        result = DealerItemData()
        sku_id = data.get('goodsquotepriceguid')
        result['skuId'] = sku_id
        result['spuId'] = spu_id
        sale_price = data.get('goodsquotepricemoney')
        result['salePrice'] = sale_price
        result['batchNo'] = batch_no
        origin_price = sale_price
        result['originPrice'] = origin_price
        result['supplierName'] = data.get('suppliername')
        result['supplierId'] = data.get('supplierguid')
        yield result


# 解析订单
def parse_order_item(response):
    meta = response.meta
    spu_id = meta.get('spuId')
    json_data = json.loads(response.text).get('data', {}).get('data', [])
    for data in json_data:
        order = gdOrderItem()
        order['spuId'] = spu_id
        order['skuId'] = str(data.get('goodspriceguid'))
        order['count'] = data.get('qty')
        order['amount'] = data.get('agreementtotal')
        order['supplierId'] = str(data.get('supplierguid'))
        order['supplierName'] = data.get('suppliername')
        orderTime = parse_timestamp(data.get('createdate'))
        order['orderTime'] = orderTime
        order['batchNo'] = time_to_batch_no(orderTime)
        order['genTime'] = datetime.utcnow()
        order['deptId'] = data.get('orgguid')
        order['orderDept'] = data.get('orgname')
        order['id'] = data.get('ordercode')
        order['orderId'] = data.get('orderguid')
        yield order
