# encoding:utf-8
import scrapy
from zc_core.model.items import OrderItem


# 商品元数据模型
class gdOrderItem(OrderItem):

    #同款编号
    spuId = scrapy.Field()
