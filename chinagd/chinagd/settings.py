# -*- coding: utf-8 -*-
BOT_NAME = 'chinagd'

SPIDER_MODULES = ['chinagd.spiders']
NEWSPIDER_MODULE = 'chinagd.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 12
#DOWNLOAD_DELAY = 1
CONCURRENT_REQUESTS_PER_DOMAIN = 12
CONCURRENT_REQUESTS_PER_IP = 12

DEFAULT_REQUEST_HEADERS = {
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Referer': 'https://www.neep.shop/html/portal/eMarketIndex.html',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'zh-CN,zh;q=0.8'
}

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
}
# # 指定代理池类
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.mogu_pool.MoguProxyPool'
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 下载超时
DOWNLOAD_TIMEOUT = 60
# 响应重试状态码
CUSTOM_RETRY_CODES = [405]

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'zc_core.pipelines.catalog.CatalogCompletePipeline': 400,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
}

# MongoDB配置
#MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'chinagd_2019'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 2
# 订单采集截止天数
ORDER_DEADLINE_DAYS = 90
# 允许响应内容为空（兼容订单）
ALLOW_EMPTY_RESPONSE = True
# 已采商品强制覆盖重采
# FORCE_RECOVER = True

# 价格区间阶梯
PRICE_LADDER = [0, 10, 15, 20, 25, 30, 40, 50, 60, 80, 100, 200, 500, 1000, 10000, 10000000]
# 价格超限分段长度
PRICE_OVERFLOW = 3
