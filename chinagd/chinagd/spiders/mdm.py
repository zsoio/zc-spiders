# -*- coding: utf-8 -*-
import random
from scrapy.exceptions import IgnoreRequest
from zc_core.dao.item_pool_dao import ItemPoolDao
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from zc_core.util.done_filter import DoneFilter
from chinagd.rules import *
from zc_core.spiders.base import BaseSpider


class MDMSpider(BaseSpider):
    name = "mdm"
    # # MDM信息补充
    sku_url = 'https://neep.shop/rest/service/routing/nouser/qrySKUService'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(MDMSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        # 避免重复采集
        self.done_filter = DoneFilter(self.batch_no)

    def start_requests(self):
        pool_list = ItemPoolDao().get_item_pool_list(
            fields={'_id': 1, 'materialCode': 1, 'supplierId': 1},
            query={'materialCode': {'$exists': False}}
        )
        self.logger.info('目标: %s' % (len(pool_list)))
        random.shuffle(pool_list)
        for sku in pool_list:
            sku_id = sku.get("_id")
            supplier_id = sku.get("supplierId")
            material_code = sku.get('materialCode')

            # MDM物料编码
            if not material_code and sku_id and supplier_id:
                yield scrapy.FormRequest(
                    url=self.sku_url,
                    method='POST',
                    meta={
                        'reqType': 'group',
                        'batchNo': self.batch_no,
                        'skuId': sku_id,
                    },
                    headers={
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    formdata={
                        'skuId': sku_id,
                        'skuLocation': '2',
                        'supplierId': supplier_id,
                    },
                    callback=self.parse_mdm,
                    errback=self.error_back,
                    priority=80
                )

    def parse_mdm(self, response):
        meta = response.meta
        sku_id = meta.get('skuId')
        item = parse_mdm(response)
        if item:
            item['batchNo'] = self.batch_no
            self.logger.info('MDM: [%s]' % sku_id)
            yield item
        else:
            self.logger.info('NO.: [%s]' % sku_id)


