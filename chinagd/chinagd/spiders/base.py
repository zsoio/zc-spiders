# -*- coding: utf-8 -*-
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from zc_core.spiders.base import BaseSpider
from chinagd.rules import *


class BaseSpider(BaseSpider):
    name = 'base'
    # 常用链接
    cat_index_url = "https://gd-prod.oss-cn-beijing.aliyuncs.com/upload/cms/column/marketClassify/index.html?_=1588211589002"
    cat_detail_url = "https://www.neep.shop/rest/service/routing/nouser/qryOthLevelCommodityCatalogService?upperCatalogId={}&channelId=2"
    supplier_url = 'https://gd-prod.oss-cn-beijing.aliyuncs.com/upload/cms/column/marketlogo/index.html'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(BaseSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        # cat
        yield Request(
            url=self.cat_index_url,
            meta={
                'reqType': 'catalog',
                'batchNo': self.batch_no
            },
            callback=self.parse_catalog,
            errback=self.error_back,
            priority=100
        )
        # supplier
        yield Request(
            url=self.supplier_url,
            meta={
                'reqType': 'supplier',
                'batchNo': self.batch_no
            },
            callback=self.parse_supplier_list,
            errback=self.error_back,
            priority=50,
        )

    def parse_supplier_list(self, response):
        suppliers = parse_supplier_list(response)
        if suppliers and len(suppliers) > 0:
            self.logger.info('供应商: count[%s]' % len(suppliers))
            yield Box('supplier', self.batch_no, suppliers)
        else:
            self.logger.error('无供应商: [%s] url -> [%s]' % self.batch_no, self.supplier_url)

    def parse_catalog(self, response):
        cat1_list = parse_catalog_index(response)
        if cat1_list and len(cat1_list) > 0:
            yield Box('catalog', self.batch_no, cat1_list)
            for cat1 in cat1_list:
                # 遍历一级分类
                cat1_id = cat1.get("catalogId")
                detail_url = self.cat_detail_url.format(cat1_id)
                yield Request(
                    url=detail_url,
                    meta={
                        'reqType': 'catalog',
                        'batchNo': self.batch_no,
                        'catalogId': cat1_id
                    },
                    callback=self.parse_catalog_detail,
                    errback=self.error_back,
                    priority=150
                )
        else:
            self.logger.error('无分类: [%s] url -> [%s]' % self.batch_no, self.cat_index_url)

    def parse_catalog_detail(self, response):
        cat_list = parse_catalog_detail(response)
        self.logger.info('分类: count[%s]' % len(cat_list))
        if cat_list and len(cat_list) > 0:
            yield Box('catalog', self.batch_no, cat_list)


