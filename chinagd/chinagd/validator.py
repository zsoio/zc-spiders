# encoding=utf-8
"""
响应对象业务校验
"""
from zc_core.middlewares.validate import BaseValidateMiddleware
from zc_core.util.http_util import retry_request


class BizValidator(BaseValidateMiddleware):

    def validate_item(self, request, response, spider):
        if '"respDesc":"成功"' not in response.text:
            yield retry_request(response.request)

        return response
