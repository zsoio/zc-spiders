# encoding:utf-8
from pymongo import UpdateOne
from zc_core.client.mongo_client import Mongo

mongo = Mongo()


# 有销量的商品
def filter_data():
    update_bulk = list()
    src_list = mongo.list("full_data_20210710", query={"soldCount": {"$gt": 0}})
    for item in src_list:
        update_bulk.append(UpdateOne({'_id': item.get('_id')}, {'$set': item}, upsert=True))
    print(len(update_bulk))
    mongo.bulk_write('data_20210710', update_bulk)
    mongo.close()
    print('任务完成~')


def full_data():
    # update_bulk = list()
    # src_list2 = mongo.list("full_data_20210710")
    # for item in src_list2:
    #     soldCount = item.get('soldCount')
    #     if soldCount:
    #         del item['soldCount']
    #     del item['salePrice']
    #     del item['originPrice']
    #     update_bulk.append(UpdateOne({'_id': item.get('_id')}, {'$set': item}, upsert=True))
    # print(len(update_bulk))
    # mongo.bulk_write('data_20210720', update_bulk)
    # mongo.close()
    # print('任务完成~')

    data1 = mongo.list('data_20210720', query={"salePrice": {"$exists": False}}, fields={"_id": 1})
    update_bulk = list()
    for item in data1:
        _id = item.get('_id')
        data2 = mongo.list('full_data_20210710', query={"_id": _id},
                           fields={"_id": 1, "soldCount": 1, "originPrice": 1, "salePrice": 1})
        if data2.__len__() == 1:
            data2 = data2[0]
            update_bulk.append(UpdateOne({'_id': _id}, {'$set': data2}, upsert=False))
    print(len(update_bulk))
    mongo.bulk_write('data_20210720', update_bulk)
    mongo.close()
    print('任务完成~')

if __name__ == '__main__':
    filter_data()
