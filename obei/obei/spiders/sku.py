# -*- coding: utf-8 -*-

import urllib
import scrapy
import requests
from scrapy import Request
from zc_core.model.items import Box
from zc_core.spiders.base import BaseSpider
from obei.rules import *
from scrapy.utils.project import get_project_settings
from zc_core.pipelines.catalog import CatalogHelper


class SkuSpider(BaseSpider):
    name = 'sku'
    # 常用链接
    index_url = 'https://www.obei.com.cn/obei-gateway/operation/n/operation/menu/getAllMenuclass'
    # 按钮权限
    menu_code = 'https://www.obei.com.cn/obei-gateway/operation/n/operation/door/screeningdirectory?menuCode=0&rootId={}'

    # 商品列表页 post
    sku_list_url = 'https://www.obei.com.cn/obei-gateway/operation/n/operation/door/sc'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        print("批次为:", self.batch_no)
        self.page_size = 10
        self.max_page_limit = 100
        self.settings = get_project_settings()
        self.SKU1_WHITE_LIST = self.settings.get('SKU1_WHITE_LIST')
        self.catalog_helper = CatalogHelper()
        self.unit_dict = dict()
        self.unit()

    def unit(self):
        json_data = requests.get('https://www.obei.com.cn/obei-gateway/goods/n/values/CodeItem/get', headers={
            'Accept': 'application/json, text/javascript, */*; q=0.01'
        }).json().get('data', [])
        for data in json_data:
            self.unit_dict[data.get('saleMeasureTypeUnit')] = data.get('saleMeasureTypeUnitChName')

    def _build_list_req(self, catalog3_id, catalog3_name, page, menu_list):

        return scrapy.Request(
            method='POST',
            url=self.sku_list_url,
            meta={
                'reqType': 'sku',
                'batchNo': self.batch_no,
                'catalog3Id': catalog3_id,
                'catalog3Name': catalog3_name,
                'page': page,
                'menuList': menu_list
            },
            headers={
                'Content-Type': 'application/json',
                'Referer': f'https://www.obei.com.cn/obei-web-ec/OP/category-goods-list.html?&code={catalog3_id}&name={urllib.parse.quote(catalog3_name)}&zoneCode=&type=&zoneName='
            },
            body=json.dumps({"page": 1, "limit": self.page_size,
                             "priceRuleLoginUser": {"loginUserId": "", "loginUserName": "", "loginCompanyId": "",
                                                    "loginCompanyName": "", "userBillToId": "", "ifLogined": False,
                                                    "inBaowuGroup": False},
                             "searchRequestVo": {"specifications": [], "brandCodes": [],
                                                 "classCodes": menu_list,
                                                 "priceEnd": "", "priceStart": "", "priceSort": "", "source": [],
                                                 "inBaowuGroup": "", "poolNos": ["P0000"], "pageSize": self.page_size,
                                                 "pageNum": page}, "zoneCode": ""}),
            callback=self.parse_sku_content_deal,
            errback=self.error_back,
            dont_filter=True,
        )

    def get_menu_requests(self, catalog_dict):
        return scrapy.Request(
            url=self.menu_code.format(catalog_dict.get('catalog3Id')),
            callback=self.parse_sku,
            meta={
                "catalog3Id": catalog_dict.get('catalog3Id'),
                "catalog3Name": catalog_dict.get('catalog3Name')
            },
            errback=self.error_back
        )

    def start_requests(self):
        # 品类、品牌
        yield Request(
            url=self.index_url,
            method='POST',
            meta={
                'reqType': 'sku',
                'batchNo': self.batch_no,

            },
            body=json.dumps({"menuCode": 0}),
            headers={
                'Content-Type': 'application/json'
            },
            callback=self.parse_total_page,
            errback=self.error_back,
            dont_filter=True,
        )

    # 处理sku列表
    def parse_total_page(self, response):
        # 处理品类列表
        cats = parse_catalog(response)
        if cats:
            self.logger.info('品类: count[%s]' % len(cats))
            yield Box('catalog', self.batch_no, cats)

            for cat in cats:
                if cat.get('level') == 3:
                    # 采集sku列表
                    catalog_dict = {
                        "catalog3Id": cat.get('catalogId')
                    }
                    self.catalog_helper.fill(catalog_dict)

                    if self.SKU1_WHITE_LIST:
                        if catalog_dict.get('catalog1Name') in self.SKU1_WHITE_LIST:
                            yield self.get_menu_requests(catalog_dict)
                    else:
                        yield self.get_menu_requests(catalog_dict)

    def parse_sku(self, response):
        meta = response.meta
        catalog3_id = meta.get('catalog3Id')
        catalog3_name = meta.get('catalog3Name')
        menu_list = parse_menu_code(response)
        yield self._build_list_req(catalog3_id,
                                   catalog3_name, 1, menu_list)

    # 处理sku列表
    def parse_sku_content_deal(self, response):
        meta = response.meta
        catalog3_id = meta.get('catalog3Id')
        catalog3_name = meta.get('catalog3Name')
        cur_page = meta.get('page')
        menu_list = meta.get('menuList')
        # 处理sku列表
        sku_list, item_list = parse_sku(response, self.catalog_helper, self.unit_dict)
        self.logger.info("清单: cat=%s, page=%s cnt=%s" % (catalog3_id, cur_page,len(sku_list)))
        if sku_list:
            yield Box('sku', self.batch_no, sku_list)
            yield Box('item', self.batch_no, item_list)
            # 发起分页
            if cur_page == 1:
                # 解析页面
                total_pages = parse_total_page(response,self.page_size)
                self.logger.info("清单1: cat=%s, total_page=%s" % (catalog3_id, total_pages))
                for page in range(2, total_pages + 1):
                    yield self._build_list_req(catalog3_id, catalog3_name, page, menu_list)
        else:
            self.logger.info('空页: cat=%s, page=%s' % (catalog3_id, cur_page))
