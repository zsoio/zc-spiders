# # -*- coding: utf-8 -*-
# import random
# import scrapy
# from scrapy import Request
#
# from obei.rules import *
# from zc_core.dao.sku_pool_dao import SkuPoolDao
# from zc_core.dao.batch_dao import BatchDao
# from zc_core.util.batch_gen import time_to_batch_no
# from zc_core.util.done_filter import DoneFilter
# from datetime import datetime
#
#
# class FullSpider(BaseSpider):
#     name = 'full_detail'
#
#     item_url = 'https://www.obei.com.cn/mk/{}.html'
#
#     def __init__(self, batchNo=None, *args, **kwargs):
#         super(FullSpider, self).__init__(*args, **kwargs)
#         if not batchNo:
#             self.batch_no = time_to_batch_no(datetime.now())
#         else:
#             self.batch_no = int(batchNo)
#         # 创建批次记录
#         BatchDao().create_batch(self.batch_no)
#         # 避免重复采集
#         self.done_filter = DoneFilter(self.batch_no)
#
#     def start_requests(self):
#         pool_list = SkuPoolDao().get_sku_pool_list(
#             fields={'_id': 1, 'brandModel': 1, 'spuId': 1, 'batchNo': 1, 'offlineTime': 1, 'catalog1Id': 1,
#                     'catalog1Name': 1, 'catalog2Id': 1, 'catalog2Name': 1, 'catalog3Id': 1, 'catalog3Name': 1})
#
#         # print("pool_list:", pool_list)
#         pool_list = [i for i in pool_list if i.get('catalog2Name') in ["办公耗材", "办公零固设备", "办公用纸"]]
#         self.logger.info('全量：%s' % (len(pool_list)))
#         for sku in pool_list:
#             sku_id = sku.get('_id')
#             offline_time = sku.get('offlineTime', 0)
#             settings = get_project_settings()
#             if offline_time > settings.get('MAX_OFFLINE_TIME', 2):
#                 self.logger.info('忽略: [%s][%s]', sku_id, offline_time)
#                 continue
#             # 避免重复采集
#             if self.done_filter.contains(sku_id) and not settings.get('FORCE_RECOVER', False):
#                 self.logger.info('已采: [%s]', sku_id)
#                 continue
#             # 采集商品
#             yield Request(
#                 url=self.item_url.format(sku_id),
#                 callback=self.parse_item_data,
#                 errback=self.error_back,
#                 priority=260,
#                 meta={
#                     'reqType': 'item',
#                     'batchNo': self.batch_no,
#                     'skuId': sku_id,
#                     'brandModel': sku.get('brandModel'),
#                     'catalog1Id': sku.get('catalog1Id'),
#                     'catalog1Name': sku.get('catalog1Name'),
#                     'catalog2Id': sku.get('catalog2Id'),
#                     'catalog2Name': sku.get('catalog2Name'),
#                     'catalog3Id': sku.get('catalog3Id'),
#                     'catalog3Name': sku.get('catalog3Name'),
#                 },
#             )
#
#     # 处理ItemData
#     def parse_item_data(self, response):
#         # 处理商品详情页
#         data = parse_item_data(response)
#         self.logger.info('商品: [%s]' % data.get('skuId'))
#         yield data
#
#     # 错误处理
#     def error_back(self, e):
#         if e.type and e.type == IgnoreRequest:
#             self.logger.info(e.value)
#         else:
#             if e.request:
#                 self.logger.error('请求异常: [%s][%s] -> [%s]' % (str(type(e)), e.request.url, e.request.meta))
#                 yield retry_request(e.request)
#             else:
#                 self.logger.error('未知异常: %s' % e)
