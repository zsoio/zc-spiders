# # -*- coding: utf-8 -*-
# import random
# import scrapy
# from scrapy import Request
# from obei.rules import *
# from zc_core.dao.sku_pool_dao import SkuPoolDao
# from zc_core.dao.sku_dao import SkuDao
# from zc_core.dao.batch_dao import BatchDao
# from zc_core.util.batch_gen import time_to_batch_no
# from zc_core.util.done_filter import DoneFilter
# from datetime import datetime
# from zc_core.spiders.base import BaseSpider
#
#
# class FullSpider(BaseSpider):
#     name = 'full'
#
#     item_url = 'https://www.obei.com.cn/obei-gateway/goods/n/minuat/commodity/detail/{}'
#
#     def __init__(self, batchNo=None, *args, **kwargs):
#         super(FullSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
#         # 创建批次记录
#         BatchDao().create_batch(self.batch_no)
#         # 避免重复采集
#         self.done_filter = DoneFilter(self.batch_no)
#
#     def start_requests(self):
#
#         settings = get_project_settings()
#         while_list = settings.get("CATALOG_WHITE_LIST")
#         if while_list:
#             pool_list = SkuDao().get_batch_sku_list(self.batch_no,
#                                                     fields={'_id': 1, 'brandModel': 1, 'spuId': 1, 'batchNo': 1,
#                                                             'offlineTime': 1, 'catalog1Id': 1, 'catalog1Name': 1,
#                                                             'catalog2Id': 1, 'catalog2Name': 1, 'catalog3Id': 1,
#                                                             'catalog3Name': 1},
#                                                     query={"$or": while_list})
#         else:
#             pool_list = SkuDao().get_batch_sku_list(self.batch_no,
#                                                     fields={'_id': 1, 'brandModel': 1, 'spuId': 1, 'batchNo': 1,
#                                                             'offlineTime': 1, 'catalog1Id': 1, 'catalog1Name': 1,
#                                                             'catalog2Id': 1, 'catalog2Name': 1, 'catalog3Id': 1,
#                                                             'catalog3Name': 1})
#         self.logger.info('全量：%s' % (len(pool_list)))
#         random.shuffle(pool_list)
#         # print("pool_list:", pool_list)
#         for sku in pool_list:
#             sku_id = sku.get('_id')
#             offline_time = sku.get('offlineTime', 0)
#             settings = get_project_settings()
#             if offline_time > settings.get('MAX_OFFLINE_TIME', 2):
#                 self.logger.info('忽略: [%s][%s]', sku_id, offline_time)
#                 continue
#             # 避免重复采集
#             if self.done_filter.contains(sku_id) and not settings.get('FORCE_RECOVER', False):
#                 self.logger.info('已采: [%s]', sku_id)
#                 continue
#             # 采集商品
#             yield Request(
#                 method='POST',
#                 url=self.item_url.format(sku_id),
#                 callback=self.parse_item_data,
#                 errback=self.error_back,
#                 priority=260,
#                 headers={
#                     'Content-Type': 'application/json',
#                     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
#                     'X-Requested-With': 'XMLHttpRequest'
#                 },
#                 meta={
#                     'reqType': 'item',
#                     'batchNo': self.batch_no,
#                     'skuId': sku_id,
#                     'brandModel': sku.get('brandModel'),
#                     'catalog1Id': sku.get('catalog1Id'),
#                     'catalog1Name': sku.get('catalog1Name'),
#                     'catalog2Id': sku.get('catalog2Id'),
#                     'catalog2Name': sku.get('catalog2Name'),
#                     'catalog3Id': sku.get('catalog3Id'),
#                     'catalog3Name': sku.get('catalog3Name'),
#                 },
#             )
#
#     # 处理ItemData
#     def parse_item_data(self, response):
#         # 处理商品详情页
#         data = parse_item_data(response)
#         self.logger.info('商品: [%s]' % data.get('skuId'))
#         yield data
