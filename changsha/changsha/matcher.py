import re


# 从js脚本中解析品类编码
def match_cat_id_from_js(js):
    # goFirstType(2,1)
    if js:
        arr = re.findall(r'\((\d+),.+', js.strip())
        if len(arr):
            return arr[0].strip()
    return ''

# 从链接中解析品类编码
def match_cat_id_from_link(link):
    # /frontBrands/getBrandsAndProductInfos.action?productTypeId=8&level=1&orderBy=normal
    if link:
        arr = re.findall(r'productTypeId=(\d+)&.+', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从js脚本中解析供应商编码
def match_supplier_id_from_link(link):
    # /store/frontShopMainPage/gotoShopInfoPage.action?orderBy=normal&shopInfoId=1012
    if link:
        arr = re.findall(r'shopInfoId=(.+)&*', link.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# 从链接中解析供应商编码
def match_supplier_id_from_js(js):
    # getshopUrl(201,837);
    if js:
        arr = re.findall(r'\((\d+),.+', js.strip())
        if len(arr):
            return arr[0].strip()
    return ''


# # 测试
if __name__ == '__main__':
    # print(match_cat_id('goFirstType(2,1)'))
    # print(match_cat_id_from_nav('/frontBrands/getBrandsAndProductInfos.action?productTypeId=8&level=1&orderBy=normal'))
    print(match_supplier_id_from_link('getshopUrl(201,837);'))