# -*- coding: utf-8 -*-
import random
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.dao.sku_dao import SkuDao
from zc_core.util.http_util import retry_request
from zc_core.spiders.base import BaseSpider
from changsha.rules import *


class GroupSpider(BaseSpider):
    name = 'group'
    # 常用链接
    group_list_url = 'http://hunan.gpmart.cn/productsdetail/getSameMP.action?productId={}'
    # 覆盖配置
    custom_settings = {
        'DOWNLOAD_DELAY': 0
    }

    def __init__(self, batchNo=None, *args, **kwargs):
        super(GroupSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        sku_list = SkuDao().get_batch_sku_list(self.batch_no)
        self.logger.info('批次商品：%s' % len(sku_list))
        random.shuffle(sku_list)
        for sku in sku_list:
            sku_id = sku.get('_id')

            # 采集商品关联关系
            yield Request(
                method='agshop',
                url=self.group_list_url.format(sku_id),
                callback=self.parse_group,
                errback=self.error_back,
                meta={
                    'reqType': 'group',
                    'batchNo': self.batch_no,
                    'skuId': sku_id
                },
                priority=2200,
                dont_filter=True
            )

    # 处理商品关联关系
    def parse_group(self, response):
        group = parse_group(response)
        if group and group.get('skuIdList'):
            self.logger.info('同款: sku=%s, cnt=%s' % (group.get('skuId'), len(group.get('skuIdList'))))
            yield group
