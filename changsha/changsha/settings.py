# -*- coding: utf-8 -*-
BOT_NAME = 'changsha'

SPIDER_MODULES = ['changsha.spiders']
NEWSPIDER_MODULE = 'changsha.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 32
DOWNLOAD_DELAY = 3
CONCURRENT_REQUESTS_PER_DOMAIN = 32
CONCURRENT_REQUESTS_PER_IP = 32

# COOKIES_ENABLED = False
DEFAULT_REQUEST_HEADERS = {
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3704.400 QQBrowser/10.4.3587.400',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.9',
}

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'changsha.validator.BizValidator': 500
}
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 1
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 1
# 代理积分阈值
PROXY_SCORE_LIMIT = 3
# 下载超时
DOWNLOAD_TIMEOUT = 45

EXTENSIONS = {
    'zc_core.extensions.redis_spider_close.RedisSpiderClosedExtensions': 520,
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}
# RedisSpider关闭等待时间(秒)
IDLE_TIME = 300

ITEM_PIPELINES = {
    'zc_core.pipelines.catalog.CatalogCompletePipeline': 400,
    'zc_core.pipelines.supplier.SupplierCompletePipeline': 420,
    'zc_core.pipelines.mongo.MongoPipeline': 500,
    'zc_core.pipelines.box.BoxPipeline': 510,
}

# Redis配置
REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379
REDIS_DB = 0

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'changsha_2019'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 2
# 订单采集截止天数
ORDER_DEADLINE_DAYS = 180
# 导出根目录
OUTPUT_ROOT = '/work/changsha/'

# 一级分类
ROOT_CAT = {
    '办公电脑、软件': '2',
    '办公设备': '3',
    '办公用纸': '4',
    '办公文具': '5',
    '办公家具': '6',
    '办公电器': '7',
    '办公数码': '8',
    '图书/期刊': '869',
    '食品饮料': '9',
    '生活用品': '10',
    '文体用品': '11',
    '劳保、工具': '12',
    '定点采购': '16',
    '网络（安全）设备、系统': '17',
}

# 目标供应商编号
SUPPLIERS = {
    '46': '深圳齐心集团股份有限公司',
    '39': '上海晨光科力普办公用品有限公司',
    # '1287': '史泰博（上海）有限公司',
    '5995': '史泰博电子商务（天津）有限公司',
    '10': '欧菲斯办公伙伴控股有限公司',
    '15': '北京申合信科技发展有限公司',
    '53': '领先未来科技集团有限公司',
    '34': '北京一线达通科技发展有限公司',
    '32': '湖南苏宁易购有限公司',
    '56': '武汉京东世纪贸易有限公司',
    '1800': '得力集团有限公司',
    '4939': '咸亨国际科技股份有限公司',
}
