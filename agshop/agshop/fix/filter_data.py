# encoding:utf-8
from pymongo import UpdateOne
from zc_core.client.mongo_client import Mongo

mongo = Mongo()


# 有销量的商品
def filter_data():
    update_bulk = list()
    src_list = mongo.list("full_data_20210630", query={"$or": [{"supplierId": {"$ne": "02f9801c6f3f48359feb9efa4be9f968"}},
                                                               {"supplierId": "02f9801c6f3f48359feb9efa4be9f968", "soldCount": {"$gt": 0}}]})
    for item in src_list:
        update_bulk.append(UpdateOne({'_id': item.get('_id')}, {'$set': item}, upsert=True))
    print(len(update_bulk))
    mongo.bulk_write('data_20210630', update_bulk)
    mongo.close()
    print('任务完成~')


if __name__ == '__main__':
    filter_data()
