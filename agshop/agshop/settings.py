# -*- coding: utf-8 -*-
BOT_NAME = 'agshop'
SPIDER_MODULES = ['agshop.spiders']
NEWSPIDER_MODULE = 'agshop.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 8
# DOWNLOAD_DELAY = 0.1
CONCURRENT_REQUESTS_PER_DOMAIN = 8
CONCURRENT_REQUESTS_PER_IP = 8

DEFAULT_REQUEST_HEADERS = {
    'Metis-ReqId': 'c4c7311b-3130-47b4-8e31-544ffe5a7a6b',
    'Metis-Tenant': '8dd10f97bc684b4fbc845c0b344883aa',
    'Host': 'agshop.delinzl.cn',
    'Connection': 'keep-alive',
    'Metis-InternalModule': 'aaaa',
    'Referer': 'https://agshop.delinzl.cn/agshop',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'zh-CN,zh;q=0.9',
}

DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    # 'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    # 'agshop.validator.BizValidator': 500
    'agshop.debugger.BizDebugger': 543
}
# 指定代理
PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.mogu_pool.MoguProxyPool'
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.mogu_pool.MoguProxyPool'
# PROXY_POOL_CLASS = 'zc_core.middlewares.proxies.zhima_pool.ZhimaProxyPool'
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 2
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 下载超时
DOWNLOAD_TIMEOUT = 60

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
}

ITEM_PIPELINES = {
    'zc_core.pipelines.catalog.CatalogCompletePipeline': 400,
    'zc_core.pipelines.brand.BrandCompletePipeline': 410,
    'zc_core.pipelines.supplier.SupplierCompletePipeline': 420,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
}
# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'agshop_2021'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 2
# 已采商品强制覆盖重采
# FORCE_RECOVER = True

# 品类白名单(一级级分类)
CAT1_WHITE_LIST = {
    'da0228cb54a511ebace0fa163ecbc11e': '汽车用品',
    '51de1583d7b411eb820a0242ac110002': '电器设备',
    '51e08586d7b411eb820a0242ac110002': '动力工具',
    'da02217b54a511ebace0fa163ecbc11e': '家用电器',
    '51e0a776d7b411eb820a0242ac110002': '化学试剂',
    '51e51c85d7b411eb820a0242ac110002': '医疗保健',
    'da02242254a511ebace0fa163ecbc11e': '运动户外',
    '51e0c10ed7b411eb820a0242ac110002': '安全防护',
    '51e51cf2d7b411eb820a0242ac110002': '劳保用品',
    'da02249d54a511ebace0fa163ecbc11e': '家居日用',
    'da02294254a511ebace0fa163ecbc11e': '家装建材',
    '51de41c4d7b411eb820a0242ac110002': '家具/装修',
    'da0223a554a511ebace0fa163ecbc11e': '食品饮料',
    'da02276c54a511ebace0fa163ecbc11e': '玩具乐器',
    'da02285354a511ebace0fa163ecbc11e': '电脑、办公',
    'da02221154a511ebace0fa163ecbc11e': '服饰内衣',
    'da02229e54a511ebace0fa163ecbc11e': '美妆护肤',
    'da02251354a511ebace0fa163ecbc11e': '箱包皮具',
    'da02205c54a511ebace0fa163ecbc11e': '数码',
    'da0226f254a511ebace0fa163ecbc11e': '厨具',
    'da0227df54a511ebace0fa163ecbc11e': '家具',
    '51e51c16d7b411eb820a0242ac110002': '图书',
    'da022daf54a511ebace0fa163ecbc11e': '家庭清洁/纸品',
    'da022e2454a511ebace0fa163ecbc11e': '个人护理',
}
