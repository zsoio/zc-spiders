# -*- coding: utf-8 -*-
import random
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.model.items import OrderStatus, Box
from zc_core.dao.order_pool_dao import OrderPoolDao
from zc_core.util.http_util import retry_request
from agshop.rules import *
from zc_core.spiders.base import BaseSpider


class OrderItemSpider(BaseSpider):
    name = 'order_items'

    # custom_settings = {
    #     'CONCURRENT_REQUESTS': 4,
    #     'CONCURRENT_REQUESTS_PER_DOMAIN': 4,
    #     'CONCURRENT_REQUESTS_PER_IP': 4,
    # }

    def __init__(self, batchNo=None, *args, **kwargs):
        super(OrderItemSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        # 处理订单列表
        order_list = OrderPoolDao().get_todo_list(self.batch_no)
        if order_list:
            self.logger.info('目标: %s' % (len(order_list)))
            random.shuffle(order_list)
            for order in order_list:
                order_id = order.get('_id')
                order_url = order.get('url')
                # 采集order明细页
                yield Request(
                    url=order_url,
                    callback=self.parse_order_item,
                    errback=self.error_back,
                    meta={
                        'reqType': 'order',
                        'orderId': order_id,
                    },
                    headers={
                        'Metis-Tenant': '8dd10f97bc684b4fbc845c0b344883aa',
                    },
                    dont_filter=True
                )

    def parse_order_item(self, response):
        meta = response.meta
        order_id = meta.get('orderId')
        # 处理订单列表
        order_list = parse_order_item(response)
        if order_list:
            self.logger.info('订单: order=%s, item_cnt=%s' % (order_id, len(order_list)))
            yield Box('order_item', self.batch_no, order_list)
            # 标记已采
            order_time = order_list[0].get('orderTime')
            order_status = OrderStatus()
            order_status['id'] = order_id
            order_status['orderTime'] = order_time
            order_status['batchNo'] = time_to_batch_no(order_time)
            order_status['workStatus'] = 1
            yield order_status
