# -*- coding: utf-8 -*-
import random
import uuid

import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from scrapy.utils.project import get_project_settings
from zc_core.util.http_util import retry_request
from zc_core.model.items import Box
from agshop.rules import *
from zc_core.spiders.base import BaseSpider


class CatalogSpider(BaseSpider):
    name = 'catalog'
    root_cat_url = 'https://agshop.delinzl.cn/api/nav/queryOfficeSupplies?itemType=0'
    sub_cat_url = 'https://agshop.delinzl.cn/api/nav/officeSuppliesinitchild?itemids={}'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(CatalogSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        yield Request(
            url=self.root_cat_url,
            meta={
                'batchNo': self.batch_no
            },
            headers={
                'Metis-ReqId': str(uuid.uuid4()),
                'Metis-Tenant': '8dd10f97bc684b4fbc845c0b344883aa',
                'Host': 'agshop.delinzl.cn',
                'Connection': 'keep-alive',
                'Accept': 'application/json, text/plain, */*',
                'Metis-InternalModule': 'aaaa',
                'Referer': 'https://agshop.delinzl.cn/agshop',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'zh-CN,zh;q=0.9',
            },
            callback=self.parse_root_cat,
            errback=self.error_back,
            dont_filter=True,
        )

    # 一级分类
    def parse_root_cat(self, response):
        meta = response.meta
        batch_no = meta.get('batchNo')
        cat_list = parse_root_cat(response)

        if cat_list:
            self.logger.info(f'品目：count={len(cat_list)}')
            yield Box('catalog', self.batch_no, cat_list)

            for cat in cat_list:
                cat1_id = cat.get('catalogId')
                yield Request(
                    url=self.sub_cat_url.format(cat1_id),
                    meta={
                        "batchNo": batch_no,
                        "catalog1Id": cat1_id
                    },
                    headers={
                        'Metis-ReqId': str(uuid.uuid4()),
                        'Metis-Tenant': '8dd10f97bc684b4fbc845c0b344883aa',
                        'Host': 'agshop.delinzl.cn',
                        'Connection': 'keep-alive',
                        'Accept': 'application/json, text/plain, */*',
                        'Metis-InternalModule': 'aaaa',
                        'Referer': 'https://agshop.delinzl.cn/agshop',
                        'Accept-Encoding': 'gzip, deflate, br',
                        'Accept-Language': 'zh-CN,zh;q=0.9',
                    },
                    callback=self.parse_sub_cat,
                    errback=self.error_back,
                    dont_filter=True,
                )

    # 子分类
    def parse_sub_cat(self, response):
        meta = response.meta
        cat1_id = meta.get('catalog1Id')

        sub_cats = parse_sub_cat(response)
        if sub_cats:
            self.logger.info(f'品类：cat={cat1_id}, count={len(sub_cats)}')
            yield Box('catalog', self.batch_no, sub_cats)
        else:
            self.logger.info(f'无品类：root_cat={cat1_id}')
