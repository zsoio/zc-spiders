# -*- coding: utf-8 -*-
import random
import uuid
from zc_core.spiders.base import BaseSpider
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from scrapy.utils.project import get_project_settings
from zc_core.dao.catalog_dao import CatalogDao
from zc_core.util.http_util import retry_request
from zc_core.model.items import Box
from agshop.rules import *


class SkuAllSpider(BaseSpider):
    name = 'sku_all'
    cat_url = 'https://agshop.delinzl.cn/api/nav/queryOfficeSupplies?itemType=0'
    # 获取二级三级分页url
    catalogs_url = 'https://agshop.delinzl.cn/api/nav/officeSuppliesinitchild?itemids={}'
    # 获取所有商品列表链接
    sku_list_url = 'https://agshop.delinzl.cn/api/search/itemSearch?page={}&size={}'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuAllSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.page_size = 20
        self.settings = get_project_settings()

    # 请求获取一级分页id
    def start_requests(self):
        cat3_list = CatalogDao().get_cat_list_from_pool(query={'level': 3})
        self.logger.info('目标：%s' % (len(cat3_list)))
        if cat3_list and len(cat3_list) > 0:
            random.shuffle(cat3_list)
            for cat in cat3_list:
                page = 1
                cat3_id = cat.get('_id')
                yield Request(
                    url=self.sku_list_url.format(page, self.page_size),
                    method='POST',
                    meta={
                        'batchNo': self.batch_no,
                        'catalog3Id': cat3_id,
                        'page': page
                    },
                    body=json.dumps({
                        "brandId": "",
                        "supplierId": "",
                        "categoryIdList": [cat3_id],
                        "goodsPropertyList": [],
                        "isAid": None,
                        "keywordList": [""],
                        "maxPrice": "",
                        "minPrice": "",
                        "compareWithOfficialPrice": 0
                    }),
                    headers={
                        'Metis-ReqId': str(uuid.uuid4()),
                        'Metis-Tenant': '8dd10f97bc684b4fbc845c0b344883aa',
                        'Host': 'agshop.delinzl.cn',
                        'Connection': 'keep-alive',
                        'Accept': 'application/json, text/plain, */*',
                        'Metis-InternalModule': 'aaaa',
                        'Accept-Encoding': 'gzip, deflate, br',
                        'Accept-Language': 'zh-CN,zh;q=0.9',
                        'Origin': 'https://agshop.delinzl.cn',
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400',
                        'Content-Type': 'application/json',
                        'Referer': f'https://agshop.delinzl.cn/search?categoryId={cat3_id}',
                    },
                    callback=self.parse_sku_list,
                    errback=self.error_back,
                    dont_filter=True,
                )

    # 处理sku列表
    def parse_sku_list(self, response):
        meta = response.meta
        curr_page = meta.get('page')
        batch_no = meta.get('batchNo')
        cat3_id = meta.get('catalog3Id')

        sku_list = parse_sku(response)
        if sku_list:
            self.logger.info(f'清单: cat={cat3_id}, page={curr_page}, cnt={len(sku_list)}')
            yield Box('sku', self.batch_no, sku_list)

            if curr_page == 1:
                total_page = parse_total_page(response, self.page_size)
                self.logger.info(f'页数: cat={cat3_id}, ttp={total_page}')

                for page in range(2, total_page + 1):
                    yield Request(
                        url=self.sku_list_url.format(page, self.page_size),
                        method='POST',
                        body=json.dumps(
                            {"brandId": "",
                             "supplierId": "",
                             "categoryIdList": [cat3_id],
                             "goodsPropertyList": [],
                             "isAid": None,
                             "keywordList": [""],
                             "maxPrice": "",
                             "minPrice": "",
                             "compareWithOfficialPrice": 0}
                        ),
                        meta={
                            'batchNo': batch_no,
                            'catalog3Id': cat3_id,
                            'page': page,
                        },
                        headers={
                            'Metis-ReqId': str(uuid.uuid4()),
                            'Metis-Tenant': '8dd10f97bc684b4fbc845c0b344883aa',
                            'Host': 'agshop.delinzl.cn',
                            'Connection': 'keep-alive',
                            'Accept': 'application/json, text/plain, */*',
                            'Metis-InternalModule': 'aaaa',
                            'Accept-Encoding': 'gzip, deflate, br',
                            'Accept-Language': 'zh-CN,zh;q=0.9',
                            'Origin': 'https://agshop.delinzl.cn',
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3870.400 QQBrowser/10.8.4405.400',
                            'Content-Type': 'application/json',
                            'Referer': f'https://agshop.delinzl.cn/search?categoryId={cat3_id}',
                        },
                        callback=self.parse_sku_list,
                        errback=self.error_back,
                        dont_filter=True,
                    )
