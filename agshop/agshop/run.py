from scrapy import cmdline

# cmdline.execute('scrapy crawl catalog'.split())
# cmdline.execute('scrapy crawl catalog -a batchNo=20210701'.split())
# cmdline.execute('scrapy crawl sku'.split())
cmdline.execute('scrapy crawl sku_all'.split())
# cmdline.execute('scrapy crawl sku -a batchNo=20210701'.split())
# cmdline.execute('scrapy crawl full -a batchNo=20210726'.split())
# cmdline.execute('scrapy crawl full'.split())
# cmdline.execute('scrapy crawl order -a batchNo=20210531'.split())
# cmdline.execute('scrapy crawl order'.split())
# cmdline.execute('scrapy crawl order_items -a batchNo=20210531'.split())
# cmdline.execute('scrapy crawl order_items'.split())
