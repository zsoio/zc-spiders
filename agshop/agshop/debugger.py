# encoding=utf-8
"""
响应对象业务校验
"""


class BizDebugger(object):

    def process_request(self, request, spider):
        spider.logger.debug('process_request')
        pass

    def process_response(self, request, response, spider):
        spider.logger.debug('process_response')
        return response
