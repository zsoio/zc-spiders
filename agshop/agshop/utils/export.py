# encoding:utf-8
import os
import pandas as pd
from zc_core.client.mongo_client import Mongo
from scrapy.utils.project import get_project_settings


def export_item(table, file, query):
    column_header_map = [
        {'col': '_id', 'title': '商品编号'},
        {'col': 'skuName', 'title': '商品名称'},
        {'col': 'supplierName', 'title': '供应商'},
        {'col': 'supplierSkuId', 'title': '供应商商品编号'},
        {'col': 'unit', 'title': '单位'},
        {'col': 'catalog1Name', 'title': '一级分类'},
        {'col': 'catalog2Name', 'title': '二级分类'},
        {'col': 'catalog3Name', 'title': '三级分类'},
        {'col': 'brandName', 'title': '品牌'},
        {'col': 'brandModel', 'title': '型号'},
        {'col': 'originPrice', 'title': '原价'},
        {'col': 'salePrice', 'title': '销售价格'},
        {'col': 'stock', 'title': '库存'},
        {'col': 'skuImg', 'title': '首图链接'},
        {'col': 'deliveryDay', 'title': '交付日期'},
        {'col': 'soldCount', 'title': '累计销量'}
    ]

    columns = list()
    headers = list()
    for row in column_header_map:
        columns.append(row.get('col'))
        headers.append(row.get('title'))

    data_list = Mongo().list(table, query=query)
    if data_list:
        print('数量: %s' % len(data_list))
        write = pd.ExcelWriter(file)
        df = pd.DataFrame(data_list)
        df.to_excel(write, sheet_name='商品', columns=columns, header=headers, index=False)
        write.save()
        print('导出成功: %s' % table)
    else:
        print('无数据: %s' % table)


if __name__ == '__main__':
    bot_name = get_project_settings().get('BOT_NAME')
    batch_no = '20211026'
    dir = f'/work/{bot_name}'
    if not os.path.exists(dir):
        os.makedirs(dir)
    table = 'data_{}'.format(batch_no)
    file = f'{dir}/鞍钢集团_{batch_no}.xlsx'
    export_item(table, file, query={
        'catalog2Name': {"$in": ["办公耗材", "办公设备"]}
    })
