# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.model.items import Box
from zc_core.util.http_util import retry_request
from guangdong.rules import *
from zc_core.spiders.base import BaseSpider


class SkuSpider(BaseSpider):
    name = 'sku'
    # 常用链接
    index_url = 'http://210.76.73.186/channel/0_0_0_0_0_0_0_0_0_0'
    sku_list_url = 'http://210.76.73.186/channel/0_0_1_0_0_0_0_{}_0_0?page={}&per_page={}'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(SkuSpider, self).__init__(batchNo=batchNo, *args, **kwargs)
        self.page_size = 28

    def start_requests(self):
        # 品类、品牌
        yield Request(
            url=self.index_url,
            meta={
                'batchNo': self.batch_no,
            },
            callback=self.parse_catalog_brand,
            errback=self.error_back,
            dont_filter=True
        )

    def parse_catalog_brand(self, response):
        meta = response.meta
        batch_no = meta.get('batchNo')
        # 处理品类列表
        cats = parse_catalog(response)
        if cats:
            self.logger.info('品类: count[%s]' % len(cats))
            yield Box('catalog', self.batch_no, cats)

        # 处理品牌列表
        brands = parse_brand(response)
        if brands:
            self.logger.info('品牌: count[%s]' % len(brands))
            yield Box('brand', self.batch_no, brands)

        # 处理供应商列表
        suppliers = parse_supplier(response)
        if suppliers:
            self.logger.info('供应商: count[%s]' % len(suppliers))
            yield Box('supplier', self.batch_no, suppliers)

            for sp in suppliers:
                sp_code = sp.get('code')
                sp_name = sp.get('name')
                yield Request(
                    url=self.sku_list_url.format(sp_code, 1, self.page_size),
                    callback=self.parse_sku_page,
                    errback=self.error_back,
                    meta={
                        'reqType': 'sku',
                        'batchNo': batch_no,
                        'supplierCode': sp_code,
                        'supplierName': sp_name,
                    },
                    dont_filter=True
                )

    # 处理sku列表
    def parse_sku_page(self, response):
        meta = response.meta
        sp_code = meta.get('supplierCode')
        sp_name = meta.get('supplierName')
        # 处理品类列表
        pages = parse_sku_page(response)
        if pages:
            self.logger.info('总页数: sp=%s, pages=%s' % (sp_code, pages))
            for page in range(1, pages + 1):
                yield Request(
                    url=self.sku_list_url.format(sp_code, page, self.page_size),
                    callback=self.parse_sku,
                    errback=self.error_back,
                    meta={
                        'reqType': 'sku',
                        'batchNo': self.batch_no,
                        'supplierCode': sp_code,
                        'supplierName': sp_name,
                        'page': page
                    },
                    dont_filter=True
                )

    # 处理sku列表
    def parse_sku(self, response):
        meta = response.meta
        sp_code = meta.get('supplierCode')
        cur_page = meta.get('page')
        # 处理商品
        sku_list = parse_sku(response)
        if sku_list:
            self.logger.info('清单: sp=%s, page=%s, cnt=%s' % (sp_code, cur_page, len(sku_list)))
            yield Box('sku', self.batch_no, sku_list)
        else:
            self.logger.info('分页为空: sp=%s, page=%s' % (sp_code, cur_page))
