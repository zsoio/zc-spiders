# -*- coding: utf-8 -*-
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from scrapy.utils.project import get_project_settings
from datetime import datetime
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.http_util import retry_request
from zc_core.util.common import random_code
from zc_core.model.items import Order
from guangdong.rules import parse_order
from guangdong.utils.order_captcha import verify_code
from zc_core.spiders.base import BaseSpider


class OrderSpider(BaseSpider):
    name = 'order'
    # 常用链接
    # order_ list_url = 'http://210.76.73.185/more_notices.html?kind=real_mall_result_all&page={}&per=30'
    order_list_url = 'http://210.76.73.185/more_notices.html?_rucaptcha={}&kind=real_mall_result_all&page={}&per=20&_rucaptcha={}'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(OrderSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def start_requests(self):
        settings = get_project_settings()
        for page in range(1, settings.get('MAX_ORDER_PAGE', 500)):
            key = random_code(5).lower()
            code, session_id = verify_code(key)
            if key and code and session_id:
                # 采集列表下一页
                yield Request(
                    url=self.order_list_url.format(key, page, code.lower()),
                    callback=self.parse_order,
                    errback=self.error_back,
                    meta={
                        # 'reqType': 'order_page',
                        'reqType': 'order',
                        'page': page
                    },
                    headers={
                        'Host': '210.76.73.185',
                        'Connection': 'keep-alive',
                        'Upgrade-Insecure-Requests': '1',
                        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3732.400 QQBrowser/10.5.3819.400',
                        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                        'Accept-Encoding': 'gzip, deflate',
                        'Accept-Language': 'zh-CN,zh;q=0.9',
                        'Cookie': '_gdddxt_session={}'.format(session_id),
                    },
                    dont_filter=True
                )

    def parse_order(self, response):
        # 处理订单列表
        order_pages = parse_order(response)
        if order_pages:
            self.logger.info('订单列表: page=%s, count=%s' % (response.meta.get('page', 1), len(order_pages)))
            for op in order_pages:
                order = Order()
                order.update(op)
                yield order
