# -*- coding: utf-8 -*-
import random
import scrapy
from scrapy import Request
from scrapy.exceptions import IgnoreRequest
from zc_core.dao.miss_sku_dao import MissSkuDao
from zc_core.model.items import Box
from zc_core.util.http_util import retry_request
from guangdong.rules import *
from zc_core.spiders.base import BaseSpider


class MissItemSpider(BaseSpider):
    name = 'miss_item'
    # 常用链接
    search_url = 'http://210.76.73.186/search.html?combo=0_0_0_0_0_0&k={}&page={}&per_page=100&utf8=%E2%9C%93'
    item_url = 'http://210.76.73.186/commodities/{}?p_id={}'

    def __init__(self, batchNo=None, *args, **kwargs):
        super(MissItemSpider, self).__init__(batchNo=batchNo, *args, **kwargs)

    def build_search_req(self, sku_name, page, sku_id, callback):
        return Request(
            url=self.search_url.format(sku_name, page),
            meta={
                'reqType': 'sku',
                'batchNo': self.batch_no,
                'skuId': sku_id,
                'skuName': sku_name,
            },
            callback=callback,
            errback=self.error_back,
        )

    def build_item_req(self, sku_id, spu_id, callback):
        return Request(
            url=self.item_url.format(spu_id, sku_id),
            callback=callback,
            errback=self.error_back,
            meta={
                'reqType': 'item',
                'batchNo': self.batch_no,
                'skuId': sku_id,
                'spuId': spu_id,
            },
        )

    def start_requests(self):
        pool_list = MissSkuDao().get_miss_sku_list()
        self.logger.info('全量: %s' % (len(pool_list)))
        random.shuffle(pool_list)
        for sku in pool_list:
            sku_id = sku.get('_id')
            sku_name = sku.get('skuName')
            yield self.build_search_req(sku_name=sku_name, sku_id=sku_id, page=1, callback=self.parse_search_list)

    def parse_search_list(self, response):
        meta = response.meta
        sku_id = meta.get('skuId')
        sku_name = meta.get('skuName')
        cur_page = meta.get('page')
        sku_list = parse_search_list(response)
        if sku_list:
            self.logger.info('清单: sku_name=%s, page=%s, cnt=%s' % (sku_name, cur_page, len(sku_list)))
            yield Box('sku_pool', self.batch_no, sku_list)
            spu_id = ''
            for sku in sku_list:
                if sku_id == sku.get('skuId'):
                    sku_id = sku.get('skuId')
                    spu_id = sku.get('spuId')
                    break
            if not spu_id and len(sku_list):
                spu_id = sku_list[0].get('spuId')
            yield self.build_item_req(sku_id=sku_id, spu_id=spu_id, callback=self.parse_item_data)
        else:
            self.logger.info('无商品: sku_name=%s, page=%s' % (sku_name, cur_page))

    # 处理ItemData
    def parse_item_data(self, response):
        meta = response.meta
        data = parse_item_data(response)
        if data:
            miss_item = MissItemData()
            miss_item.update(data)
            miss_item['srcType'] = 'order'
            self.logger.info('商品: sku=%s' % miss_item.get('skuId'))
            yield miss_item
        else:
            self.logger.info('下架: %s' % meta.get('url'))
