# code:utf8
from datetime import datetime
from zc_core.model.items import Catalog, Brand, Supplier, Sku, ItemData, OrderItem, MissItemData, MissSku
from pyquery import PyQuery
from guangdong.matcher import *

# 解析catalog列表
from zc_core.util.batch_gen import time_to_batch_no
from zc_core.util.common import parse_number, parse_time
from zc_core.util.encrypt_util import md5
from zc_core.util.sku_id_parser import convert_id2code


def parse_catalog(response):
    jpy = PyQuery(response.text)
    div_list = jpy('div.all_goods_second div')

    cats = list()
    for div in div_list.items():
        # 一级分类
        cat1_a = div('label a')
        cat1_id = match_cat_id(cat1_a.attr('href'))
        if not cat1_id:
            continue
        cat1_name = cat1_a.text().strip().replace('>', '')
        cat1 = _build_catalog(cat1_id, cat1_name, '', 1)
        cats.append(cat1)
        # 二级分类
        cat2_as = div('dl.sn-list a')
        for cat2_a in cat2_as.items():
            cat2_id = match_cat_id(cat2_a.attr('href'))
            cat2_name = cat2_a.text().strip()
            cat2 = _build_catalog(cat2_id, cat2_name, cat1_id, 2)
            cats.append(cat2)
    return cats


def _build_catalog(cat_id, cat_name, parent_id, level):
    cat = Catalog()
    cat['catalogId'] = cat_id
    cat['catalogName'] = cat_name
    cat['parentId'] = parent_id
    cat['level'] = level
    if level == 3:
        cat['leafFlag'] = 1
    else:
        cat['leafFlag'] = 0
    cat['linkable'] = 1

    return cat


# 解析brand列表
def parse_brand(response):
    meta = response.meta
    jpy = PyQuery(response.text)
    a_list = jpy('div.filtername:contains("品牌：") ~ dl.dl_brands ~ div.v-tabs > div.tabcon-multi a')
    brands = list()
    for br in a_list.items():
        brand = Brand()
        br_id = match_brand_id(br.attr('href'))
        br_name = clean_text(br.attr('title'))
        brand['id'] = md5(br_name)
        brand['code'] = br_id
        brand['name'] = br_name
        brand['batchNo'] = meta.get('batchNo')
        brands.append(brand)

    return brands


# 解析brand列表
def parse_supplier(response):
    meta = response.meta
    jpy = PyQuery(response.text)
    a_list = jpy('div.filtername:contains("电商：") ~ dl.dl_emalls ~ div.e-tabs > div.tabcon-multi a')
    suppliers = list()
    for sp in a_list.items():
        sp_id = match_supplier_id(sp.attr('href'))
        sp_name = clean_text(sp.attr('title'))
        supplier = Supplier()
        supplier['id'] = md5(sp_name)
        supplier['code'] = sp_id
        supplier['name'] = sp_name
        supplier['batchNo'] = meta.get('batchNo')
        suppliers.append(supplier)

    return suppliers


# 解析sku列表
def parse_sku_page(response):
    jpy = PyQuery(response.text)
    last = jpy('div.pagination ul span.last a')
    if last:
        return match_last_page(last.attr('href'))

    return 0


# 解析sku列表
def parse_sku(response):
    meta = response.meta
    sp_code = meta.get('supplierCode')
    jpy = PyQuery(response.text)
    div_links = jpy('div.gp-list-view ul li div.info')
    sku_list = list()
    for sku_div in div_links.items():
        sku = Sku()
        link = sku_div('div.img a').attr('href')
        tp = match_sku_id_from_sku(link)
        sku['skuId'] = tp[1]
        sku['spuId'] = tp[0]
        sku['supplierCode'] = sp_code
        sku['soldCount'] = int(sku_div('p.sales-num').text().replace('销量：', ''))
        sku_list.append(sku)

    return sku_list


# 解析sku列表
def parse_search_list(response):
    jpy = PyQuery(response.text)
    div_links = jpy('div.gp-list-view ul li div.info')
    sku_list = list()
    for sku_div in div_links.items():
        sku = Sku()
        link = sku_div('div.img a').attr('href')
        tp = match_sku_id_from_sku(link)
        sku['skuId'] = tp[1]
        sku['spuId'] = tp[0]
        sku['soldCount'] = int(sku_div('p.sales-num').text().replace('销量：', ''))
        sku['srcType'] = 'search'
        sku_list.append(sku)

    return sku_list


# 解析ItemData
def parse_item_data(response):
    meta = response.meta
    jpy = PyQuery(response.text)
    result = ItemData()

    batch_no = meta.get('batchNo')
    sku_id = meta.get('skuId')
    spu_id = meta.get('spuId')
    sp_code = meta.get('supplierCode')
    sold_count = meta.get('soldCount')

    sale_price_txt = match_sale_price(response.text)
    sale_price = parse_number(sale_price_txt)
    if sale_price <= 0:
        return None
    origin_price = jpy('del.del').text()
    nav = jpy('h1.goods_details_area a')
    total_sales = jpy('div.cumulativeamount').text()
    gov_catalog_id = jpy('div.attribute div.dt:contains("品目编号") + div.gs')
    brand_model = jpy('div.attribute div.dt:contains("商品型号") + div.gs i')
    brand_name = ''
    brand_id = ''
    brands_label = jpy('table#param_table td:contains("品牌")')
    if not brands_label:
        brands_label = jpy('table#info td:contains("品牌")')
    if brands_label:
        brand_txt = brands_label.eq(0).next('td').text()
        # 防止错误数据
        # http://210.76.73.186/commodities/336737?p_id=1284039
        if brand_name and len(brand_name) < 100:
            brand_name = brand_txt
            brand_id = md5(brand_name)
    supplier = jpy('div.itips')
    images = jpy('div.spec-scroll div.items li img')

    # -------------------------------------------------
    result['batchNo'] = batch_no
    result['spuId'] = spu_id
    result['skuId'] = sku_id
    result['skuName'] = jpy('h1.goods_name').eq(0).text().strip()
    if images:
        result['skuImg'] = images.eq(0).attr('src').strip()
    result['catalog1Id'] = ''
    result['catalog1Name'] = ''
    result['catalog2Id'] = match_cat_id(nav.eq(1).attr('href'))
    result['catalog2Name'] = nav.eq(1).text().strip()
    result['govCatId'] = gov_catalog_id.text().strip()
    result['salePrice'] = sale_price
    result['originPrice'] = parse_number(origin_price)
    result['soldAmount'] = parse_number(total_sales)
    result['soldCount'] = sold_count
    if brand_id and brand_name:
        result['brandId'] = brand_id
        result['brandName'] = brand_name
    if supplier:
        sp_name = supplier('font.textm').text().strip()
        sp_sku_id = supplier.attr('title').strip()
        result['supplierId'] = md5(sp_name)
        result['supplierName'] = sp_name
        if sp_sku_id:
            result['supplierSkuId'] = sp_sku_id
            plat_code = None
            if sp_code == '11167' or '得力' in sp_name:
                plat_code = 'deli'
            result['supplierSkuCode'] = convert_id2code(plat_code, sp_sku_id)
        result['supplierSkuLink'] = supplier('a').attr('href').strip()
    else:
        result['supplierCode'] = sp_code
    result['brandModel'] = brand_model.attr('title').strip()
    result['genTime'] = datetime.utcnow()
    # -------------------------------------------------

    return result


# 解析order列表
def parse_order(response):
    jpy = PyQuery(response.text)
    trs = jpy('table.Ptable tr')
    order_detail_url = 'http://210.76.73.185/more_notices.html?mall_show=true&no={}'

    pages = list()
    if trs and trs.size():
        for idx, tr in enumerate(trs.items()):
            if idx > 0:
                page = dict()
                tds = tr('td')
                id = match_order_id(tds.eq(6)('a').attr('href'))
                order_time = parse_time(tds.eq(5).text().strip(), fmt='%Y-%m-%d %H:%M')
                page['id'] = id
                page['url'] = order_detail_url.format(id)
                page['orderTime'] = order_time
                page['orderDept'] = tds.eq(1).text().strip()
                page['supplierName'] = tds.eq(2).text().strip()
                page['catalogName'] = tds.eq(3).text().strip()
                page['batchNo'] = time_to_batch_no(order_time)
                page['genTime'] = datetime.utcnow()
                pages.append(page)

    return pages


# 解析sku列表
def parse_order_item(response, pool_filter):
    jpy = PyQuery(response.text)

    meta = response.meta
    order_time = meta.get('orderTime')
    order_dept = meta.get('orderDept')
    supplier_name = meta.get('supplierName')
    cat_name = meta.get('catalogName')
    order_id = match_order_id(response.request.url)

    orders = list()
    skus = list()
    tr_list = jpy('table.Ptable tr')
    if len(tr_list):
        for idx, tr in enumerate(tr_list.items()):
            tds = tr('td')
            order = OrderItem()
            sku_id = match_sku_id_from_order(tds.eq(0).children('a').attr('href').strip())
            unit_price = parse_number(tds.eq(4).text().strip())
            order['id'] = order_id + '_' + str(idx + 1)
            order['orderId'] = order_id
            order['skuId'] = sku_id
            # 此平台中数量可能存在小数，四舍五入兼容处理
            order['count'] = round(parse_number(tds.eq(3).text().strip()))
            order['amount'] = unit_price * order['count']
            order['orderDept'] = order_dept
            order['supplierName'] = supplier_name
            order['orderTime'] = order_time
            order['batchNo'] = time_to_batch_no(order_time)
            order['genTime'] = datetime.utcnow()
            orders.append(order)

            if not pool_filter.contains(sku_id):
                item = MissSku()
                item['skuId'] = sku_id
                item['skuName'] = tds.eq(0).text().strip()
                item['catalog1Id'] = ''
                item['catalog1Name'] = ''
                item['catalog2Id'] = ''
                item['catalog2Name'] = cat_name
                item['salePrice'] = unit_price
                item['originPrice'] = unit_price
                brand = tds.eq(1).text().strip()
                if brand:
                    item['brandId'] = md5(brand)
                    item['brandName'] = brand
                item['supplierName'] = supplier_name
                item['genTime'] = datetime.utcnow()
                item['srcType'] = 'order'
                skus.append(item)

    return orders, skus
