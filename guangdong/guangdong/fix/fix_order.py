from pymongo import UpdateOne
from zc_core.client.mongo_client import Mongo

mongo = Mongo()


def fix_data():
    table_list = [
        'data_20200816',
        'data_20200812',
        'data_20200805',
        'data_20200729',
        'data_20200722',
        'data_20200715',
        'data_20200708',
        'data_20200701',
        'data_20200624',
        'data_20200617',
        'data_20200610',
        'data_20200603',
        'data_20200527',
        'data_20200521',
        'data_20200520',
        'data_20200513',
        'data_20200506',
        'data_20200429',
        'data_20200422',
    ]
    for table in table_list:
        update_bulk = list()
        data_list = mongo.list(table)
        for row in data_list:
            _id = row.get('_id')
            update_bulk.append(UpdateOne({'_id': _id}, {'$set': row}, upsert=True))
        print('%s, %s' % (table, len(update_bulk)))
        mongo.bulk_write('miss_sku', update_bulk)
    print('任务完成~')


if __name__ == '__main__':
    fix_data()
