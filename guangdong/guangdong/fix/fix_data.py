from pymongo import UpdateOne
from zc_core.client.mongo_client import Mongo

mongo = Mongo()


def fix_data():
    table = 'data_20210103'
    max_len = 0
    max_item = ''
    update_bulk = list()
    data_list = mongo.list(table, fields={'_id': 1, 'brandName': 1})
    for row in data_list:
        _id = row.get('_id')
        brand_name = row.get('brandName')
        if brand_name and (max_len == 0 or max_len < len(brand_name)):
            max_len = len(brand_name)
            max_item = _id
        # update_bulk.append(UpdateOne({'_id': _id}, {'$set': row}, upsert=True))
    print('%s, %s' % (max_len, max_item))
    print('任务完成~')


if __name__ == '__main__':
    fix_data()
