# -*- coding: utf-8 -*-
BOT_NAME = 'guangdong'

SPIDER_MODULES = ['guangdong.spiders']
NEWSPIDER_MODULE = 'guangdong.spiders'
ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 16
# DOWNLOAD_DELAY = 1
CONCURRENT_REQUESTS_PER_DOMAIN = 16
CONCURRENT_REQUESTS_PER_IP = 16

COOKIES_ENABLED = False
DOWNLOADER_MIDDLEWARES = {
    'zc_core.middlewares.proxy.ProxyMiddleware': 650,
    'zc_core.middlewares.agent.UserAgentMiddleware': 640,
    'guangdong.validator.BizValidator': 500
}
# 代理池中最少代理数量
MIN_PROXY_POOL_SIZE = 3
# 每次请求加载代理数量
PROXY_AMOUNT_PRE_LOAD = 2
# 自定义重试状态码
CUSTOM_RETRY_CODES = [403]

EXTENSIONS = {
    'zc_core.extensions.batch_monitor.BatchMonitorExtension': 500,
    'guangdong.extensions.FixDataExtension': 450,
}

ITEM_PIPELINES = {
    'guangdong.pipelines.CatalogCompletePipeline': 400,
    'zc_core.pipelines.supplier.SupplierCompletePipeline': 420,
    'zc_core.pipelines.mongo.MongoPipeline': 543,
    'zc_core.pipelines.box.BoxPipeline': 540,
}

# MongoDB配置
# MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_URI = 'mongodb://root:Dangerous!@zc-outer.mongodb.rds.aliyuncs.com:3717'
# MONGODB_DATABASE = 'guangdong_2019'

# 日志
LOG_LEVEL = 'INFO'
# 标记离线商品续存批次数
MAX_OFFLINE_TIME = 3
# [订单] 最大采集页数
MAX_ORDER_PAGE = 500
# 输出
OUTPUT_ROOT = '/work/guangdong/'
# 已采商品强制覆盖重采
# FORCE_RECOVER = True
